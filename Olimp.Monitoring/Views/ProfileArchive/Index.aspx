﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<ProfileArchiveViewModel>"  MasterPageFile="~/Views/Shared/Monitoring.master" %>
<%@ Import Namespace="I18N=Olimp.Monitoring.I18N" %>

<asp:Content ID="Content" ContentPlaceHolderID="Olimp_Monitoring_Content" runat="server">
    <% Html.RenderPartial("ProfileResultInfo"); %>
    
    <% using (Html.BeginForm("Generate", "ReportGenerator", FormMethod.Post)) { %>
    <% if (Model.AllowGenerateReport) { %>
        <div id="report-block">
            <h1>Формирование отчёта (<a href="#" class="link-button" data-bind="text: toggleReportLinkText, click: toggleReportCollapse"></a>)</h1>
	        <div class="block">
		        <table class="form-table" data-bind="visible: reportExpanded">	
		    	    <tr class="first">	
		    		    <th><%= I18N.Report.ProtocolTemplate %></th> 
		    		    <td>
                            <% Html.RenderAction("InPlace", "ProfilesProtocolManager"); %>
		    		    </td>
		    	    </tr>
		    	    <tr class="last">
		    		    <td colspan="2">
		    			    <button type="submit">Сформировать</button>
		    		    </td>
		    	    </tr>
		        </table>
            </div>

	    </div>    
    <% } %>            		
    
    <h1>Результаты</h1>    
    <div class="block">
        <div id="search-form">
            <table class="form-table">
                <tr class="first">
                    <th><%= I18N.Monitoring.FullName %></th>
                    <td>
                        <input id="employee-name" type="text" data-bind="value: searchData.EmployeeFullName" /></td>
                </tr>
                <tr>
                    <th><%= I18N.Monitoring.Profile %></th>
                    <td>
                        <%= Html.DropDownList( "ProfileName", Model.Profiles, "Выберите профиль...", new
						        {
						            data_bind = "value: searchData.ProfileName",
						            id = "profile-list"
						        }) %>
                    </td>
                </tr>
                <tr id="period">
                    <th><%= I18N.Monitoring.Period %></th>
                    <td class="date-span">
                        <span><%= I18N.Monitoring.PeriodBegin %></span>
                        <input type="text" data-bind="value: searchData.DateBegin" />
                        <span><%= I18N.Monitoring.PeriodEnd %></span>
                        <input type="text" data-bind="value: searchData.DateEnd" />
                    </td>
                </tr>
                <tr>
                    <th style="white-space: normal; width: 200px;"><%= I18N.Monitoring.ShowEmployeeWithoutGroup %></th>
                    <td class="checkbox">
                        <input type="checkbox" data-bind="checked: searchData.ShowEmployeeWithoutGroup" />
                    </td>
                </tr>
                <tr class="last">
                    <td colspan="2">
                        <button id="search" type="button" data-bind="click: applyFilter"><%= I18N.Monitoring.Search %></button>
                        <button type="button" data-bind="click: clearFilter"><%= I18N.Monitoring.Clear %></button>
                    </td>
                </tr>
            </table>
        </div>
    <%  
        var columns = new[]
        {
	        new ColumnOptions("DatePassed", I18N.Monitoring.DatePassed),
	        new ColumnOptions("ProfileName", I18N.Monitoring.Profile),
	        new ColumnOptions("ExamArea", I18N.Monitoring.ExamArea)
        };
        
        if (!Model.ShowExamArea)
            columns = columns.Where(c => c != columns[2]).ToArray();
            
        Html.Table(
            new TableOptions
            {
                SelectUrl = Url.Action("GetProfileResults"),
                GroupBy = "EmployeeFullName",
                PagingDefaultSize = 50,
                Columns = columns,
                Checkboxes = Model.AllowGenerateReport,
                RowsHighlighting = true,
                CheckboxesName = "ProfilesProtocolModel.ProfileResults",
                Actions = new RowAction[]
                {
                    new TemplateAction("profile-result-info"),
                    new DeleteAction(Url.Action("Delete"),  I18N.Monitoring.ConfirmDelete) { Disabled = !Model.AllowSetResults }
                }
            }, new { id = "search-results" });%>
    </div>

    <% } %>
    <script type="text/javascript">
        $(function () {
            $("#search-form").collapsibleBlock({ headerText: "<%= I18N.Monitoring.ResultsSearch %>" });
            $("#search-form button, #report-block button").olimpbutton();
            $("#protocolDate, #period input").datepicker({ changeMonth: true, changeYear: true });
            $("#profile-list").combobox();

            $('#protocol-commission').combobox();
            $('#protocol-company').combobox({
                source: function (request, response) {
                    $.post('<%= Url.Action("LookupCompany", "Employee") %>', { filter: request.term }, response)
			    }
		    });

            var searchResultsModel = $('#search-results').data('viewModel');

            var filterViewModel = {
                searchData: {
                    EmployeeFullName: ko.observable(),
                    ProfileName: ko.observable(),
                    ShowEmployeeWithoutGroup: ko.observable(),
                    DateBegin: ko.observable(),
                    DateEnd: ko.observable(),
                    __callback: function () {
                        if (!this.groups().length) {
                            $.Olimp.showWarning('По заданным параметрам не найдено ни одного результата ');
                        }
                    }
                },
                applyFilter: function () {
                    var data = ko.mapping.toJS(this.searchData);
                    searchResultsModel.requestData(data);
                },
                clearFilter: function () {
                    this.searchData.EmployeeFullName("");
                    $('#profile-list').combobox('select');
                    this.searchData.ShowEmployeeWithoutGroup(false);
                    this.searchData.DateBegin("");
                    this.searchData.DateEnd("");
                    searchResultsModel.requestData({});
                }
            };

            ko.applyBindings(filterViewModel, $('#search-form')[0]);

            var reportViewModel = {
                reportExpanded: ko.observable(false),
                toggleReportCollapse: function () {
                    this.reportExpanded(!this.reportExpanded());
                    return false;
                }
            };

            reportViewModel.toggleReportLinkText = ko.computed(function () {
                return this.reportExpanded() ? 'скрыть' : 'показать';
            }, reportViewModel);

            if ($('#report-block').length > 0)
                ko.applyBindings(reportViewModel, $('#report-block')[0]);
        });
	</script>
</asp:Content>