﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="I18N=Olimp.Monitoring.I18N" %>
<script id="passed-content" type="text/html">
	<!-- ko if: IsPassed -->
	    <%=I18N.Monitoring.Passed %>
	<!-- /ko -->

	<!-- ko ifnot: IsPassed -->
	    <%=I18N.Monitoring.Failed %>
	<!-- /ko -->
</script>
<script type="text/javascript">
    $(function () {
        var employeeInfoViewModel = {
            EmployeeFullName: ko.observable(),
            ProfileName: ko.observable(),
            CompanyName: ko.observable(),
            AppointmentNames: ko.observable(),
        };
        $('#employee-info').data('employeeInfoViewModel', employeeInfoViewModel);
        ko.applyBindings(employeeInfoViewModel, $('#employee-info')[0]);

        $('#profile-results-dialog').olimpdialog({
            autoOpen: false,
            draggable: false,
            resizable: false,
            modal: true,
            width: '60%',
            title: 'Результаты по профилю'
        });
        window.showProfileResults = function (profileResultId) {
            $.post('<%= Url.Action("GetEmployeeProfileResultCoursesAndEvents") %>', { ProfileResultId: profileResultId }, function (json) {
                var employeeInfoViewModel = $('#employee-info').data('employeeInfoViewModel');
                employeeInfoViewModel.EmployeeFullName(json.employeeFullName);
                employeeInfoViewModel.ProfileName(json.profileName);
                employeeInfoViewModel.CompanyName(json.companyName);
                employeeInfoViewModel.AppointmentNames(json.appointmentNames);
                $('#courses-table').data('viewModel').setRows(json.courses);
                $('#additional-event-table').data('viewModel').setRows(json.events);
                $('#profile-results-dialog').olimpdialog('open');
            }, 'json');

        };
    })
</script>

<div id="profile-results-dialog" title="Результаты по профилю">
    <div id="employee-info">
        <h1 data-bind="text: EmployeeFullName"></h1>
        <div class="block">
            <table class="form-table">
                <tr class="first">
                    <th>Организация</th>
                    <td data-bind="text: CompanyName"></td>
                </tr>
                <tr>
                    <th>Должность</th>
                    <td data-bind="text: AppointmentNames"></td>
                </tr>
                <tr class="last">
                    <th>Профиль</th>
                    <td data-bind="text: ProfileName"></td>
                </tr>
            </table>
        </div>
    </div>
    <h1 class="title">Результаты по курсам</h1>
    <div class="block">
        <% Html.Table(new TableOptions
           {
               OmitTemplates = true,
               DeferredInitializationScriptRendering = true,
               Sorting = false,
               Paging = false,
               Columns = new[] { 
				    new ColumnOptions("CourseName", "Наименование курса"),
				    new ColumnOptions("Date", "Дата сдачи"),
                    new ColumnOptions("IsPassed", "Результат")  { TemplateId = "passed-content" }}
           }, new { id = "courses-table" }); %>
    </div>
    <h1 class="title">Дополнительные мероприятия</h1>
    <div class="block">
        <% Html.Table(new TableOptions
           {
               OmitTemplates = true,
               DeferredInitializationScriptRendering = true,
               Sorting = false,
               Paging = false,
               Columns = new[] {
				    new ColumnOptions("AdditionalEventName", "Наименование мероприятия"),
                    new ColumnOptions("Timestamp", "Дата сдачи"),
                    new ColumnOptions("IsPassed", "Результат")  { TemplateId = "passed-content" }}
           }, new { id = "additional-event-table" }); %>
    </div>
</div>

<% Html.TableInitializationScript("courses-table"); %>
<% Html.TableInitializationScript("additional-event-table"); %>

<script type="text/html" id="profile-result-info">
    <a href="#" class="icon icon-stats action" title="Просмотр профиля" data-bind="click: function () { showProfileResults($data.Id()); }"></a>
</script>