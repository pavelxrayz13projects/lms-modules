﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<MonitoringIndexViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Monitoring.I18N" %>

<script id="passed-event" type="text/html">
    <!-- ko if: IsPassed -->
        <input type="checkbox" name="AdditionalEvents" <% if (!Model.AllowSetResults) { %>disabled="disabled"<% } %> checked="checked" data-bind="value: AdditionalEventId"/>
    <!-- /ko -->

    <!-- ko ifnot: IsPassed -->
        <input type="checkbox" name="AdditionalEvents" <% if (!Model.AllowSetResults) { %>disabled="disabled"<% } %>  data-bind="value: AdditionalEventId"/>
    <!-- /ko -->
</script>

<script type="text/javascript">
    $(function () {
        var employeeInfoViewModel = {
            EmployeeFullName: ko.observable(),
            ProfileName: ko.observable(),
            CompanyName: ko.observable(),
            AppointmentNames: ko.observable(),
        };
        $('#employee-info').data('employeeInfoViewModel', employeeInfoViewModel);
        ko.applyBindings(employeeInfoViewModel, $('#employee-info')[0]);
        $("#finish-datetime").datepicker(
            {
                altField: '#FinishDateTime',
                changeMonth: true,
                changeYear: true,
                defaultDate: new Date()
            });
        $('#profile-finish-dialog').olimpdialog({
            autoOpen: false,
            draggable: false,
            resizable: false,
            modal: true,
            width: 267,
            title: '<%= I18N.Monitoring.ChooseFinishDate %>',
            buttons: {
                '<%= Olimp.I18N.PlatformShared.Save%>': function () {
                    $.post('<%= Url.Action("MonitoringEmployeeProfileFinish") %>',
                        $("#profile-results-dialog form").serialize(),
                        function () {
                            $('#search-results').data('viewModel').reload();
                            $('#profile-finish-dialog').olimpdialog('close');
                            $('#profile-results-dialog').olimpdialog('close');
                            $.Olimp.showSuccess('<%= I18N.Monitoring.ProfileFinished%>');
                        });
                },
                '<%= Olimp.I18N.PlatformShared.Cancel %>': function () { $(this).olimpdialog('close'); }
            }
        });
        $('#profile-results-dialog').olimpdialog({
            autoOpen: false,
            draggable: false,
            resizable: false,
            modal: true,
            width: '60%',
            title: '<%= I18N.Monitoring.ProfileResults %>',
            <% if (Model.AllowSetResults) { %>
            buttons: {
                '<%= I18N.Monitoring.PassEvents %>': function () {
                    if (!confirm('<%= I18N.Monitoring.ConfirmPassEvents %>'))
                        return;
                    $.post('<%= Url.Action("MonitoringProfilePassEvents") %>', $("#profile-results-dialog form").serialize(), function () {
                        $.Olimp.showSuccess('<%= I18N.Monitoring.EventsUpdated %>');
                    });
                },
                '<%= I18N.Monitoring.FinishProfile %>': function () {
                    $('#profile-finish-dialog').olimpdialog('open');
                },
                '<%= Olimp.I18N.PlatformShared.Cancel %>': function() { $(this).olimpdialog('close'); }
            }
            <% } %>
        });
        window.showProfileResults = function (employeeId, profileId) {
            $('#EmployeeId').val(employeeId);
            $('#ProfileId').val(profileId);
            $.post('<%= Url.Action("GetEmployeeProfileCoursesAndEvents") %>', { EmployeeId: employeeId, ProfileId: profileId }, function (json) {
                var employeeInfoViewModel = $('#employee-info').data('employeeInfoViewModel');
                employeeInfoViewModel.EmployeeFullName(json.employeeFullName);
                employeeInfoViewModel.ProfileName(json.profileName);
                employeeInfoViewModel.CompanyName(json.companyName);
                employeeInfoViewModel.AppointmentNames(json.appointmentNames);
                $('#courses-table').data('viewModel').setRows(json.courses);
                $('#additional-event-table').data('viewModel').setRows(json.events);
                $('#profile-results-dialog').olimpdialog('open');
            }, 'json');

        };
    })
</script>

<div id="profile-results-dialog">
    <% using (Html.BeginForm("MonitoringEmployeeProfileFinish", "Monitoring")) { %>
        <%= Html.Hidden("EmployeeId") %>
        <%= Html.Hidden("ProfileId") %>
        <%= Html.Hidden("FinishDateTime") %>
        <div id="employee-info">
            <h1 data-bind="text: EmployeeFullName"></h1>
            <div class="block">
                <table class="form-table">
                    <tr class="first">
                        <th><%= I18N.Monitoring.Company %></th>
                        <td data-bind="text: CompanyName"></td>
                    </tr>
                    <tr>
                        <th><%= I18N.Monitoring.Appointment %></th>
                        <td data-bind="text: AppointmentNames"></td>
                    </tr>
                    <tr class="last">
                        <th><%= I18N.Monitoring.Profile %></th>
                        <td data-bind="text: ProfileName"></td>
                    </tr>
                </table>
            </div>
        </div>
        <h1><%=I18N.Monitoring.CourseResults %></h1>
        <div class="block">
            <% Html.Table(new TableOptions {
                OmitTemplates = true,
                DeferredInitializationScriptRendering = true,
                Sorting = false,
                Paging = false,
                Columns = new[] { 
                    new ColumnOptions("CourseName", I18N.Monitoring.CourseName),
                    new ColumnOptions("DatePassed", I18N.Monitoring.DateSuccessPassed),
                    new ColumnOptions("LastFinishedDate", I18N.Monitoring.LastFinishedDate)}
            }, new { id = "courses-table" }); %>
        </div>
        <h1><%= I18N.Monitoring.AdditionalEvents %></h1>
        <div class="block">
            <% Html.Table(new TableOptions {
                OmitTemplates = true,
                DeferredInitializationScriptRendering = true,
                Sorting = false,
                Paging = false,
                Columns = new[] {
                    new ColumnOptions("IsPassed", I18N.Monitoring.IsPassed)  { TemplateId = "passed-event" }, 
                    new ColumnOptions("AdditionalEventName", I18N.Monitoring.AdditionalEventName),
                    new ColumnOptions("Timestamp", I18N.Monitoring.AdditionalEventTimestamp)}
            }, new { id = "additional-event-table" }); %>
        </div>
    <% } %>
</div>

<div id="profile-finish-dialog">
    <center>
            <div id="finish-datetime"></div>
    </center>
</div>

<% Html.TableInitializationScript("courses-table"); %>
<% Html.TableInitializationScript("additional-event-table"); %>

<script type="text/html" id="profile-results-action">
    <a href="#" class="icon icon-stats action" title="<%= I18N.Monitoring.ShowProfileTitle %>" data-bind="click: function () { showProfileResults($data.EmployeeId(), $data.ProfileId()); }"></a>
</script>