﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<MonitoringIndexViewModel>"  MasterPageFile="~/Views/Shared/Monitoring.master" %>
<%@ Import Namespace="I18N=Olimp.Monitoring.I18N" %>

<asp:Content ID="Content" ContentPlaceHolderID="Olimp_Monitoring_Content" runat="server">
    <% Html.RenderPartial("EmployeeProfileResultInfo"); %>
    
    <%if (Model.AllowShiftProfiles)
          Html.RenderPartial("EmployeeProfilesShift", new EmployeeProfileShiftViewModel { ShiftDate = DateTime.Now });%>

    <% using (Html.BeginForm("Generate", "ReportGenerator", FormMethod.Post)) { %>
        <% if (Model.AllowGenerateReport) { %>
            <div id="generate-schedule">
                <h1><%= I18N.Monitoring.ScheduleForming %> (<a href="#" class="link-button" data-bind="text: toggleScheduleLinkText, click: toggleScheduleCollapse"></a>)</h1>
                <div class="block" >
                    <table class="form-table" data-bind="visible: scheduleExpanded">
                        <tr class="first">
                            <th><%= I18N.Monitoring.TemplateHeader %></th>
                            <td>
                                <% Html.RenderAction("InPlace", "ProfilesReportManager"); %>
                            </td>
                        </tr>
                        <tr class="last">
                            <td colspan="2">
                                <button type="submit"><%= I18N.Monitoring.GenerateSchedule %></button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        <% } %>

        <h1><%= I18N.Monitoring.Schedule %></h1>
        <div class="block">
            <div id="search-form">
                <table class="form-table">
                    <tr class="first">
                        <th><%= I18N.Monitoring.FullName %></th>
                        <td>
                            <input id="employee-name" type="text" data-bind="value: searchData.EmployeeFullName" />

                        </td>
                    </tr>
                    <tr>
                        <th><%= I18N.Monitoring.Profile %></th>
                        <td>
                            <%= Html.DropDownList( "ProfileName", Model.Profiles, I18N.Monitoring.ChooseProfile, new
                                {
                                    data_bind = "value: searchData.ProfileName",
                                    id = "profile-list"
                                }) %>
                        </td>
                    </tr>
                    <tr id="period">
                        <th><%= I18N.Monitoring.Period %></th>
                        <td class="date-span">
                            <span><%= I18N.Monitoring.PeriodBegin %></span>
                            <input type="text" data-bind="value: searchData.DateBegin" />
                            <span><%= I18N.Monitoring.PeriodEnd %></span>
                            <input type="text" data-bind="value: searchData.DateEnd" />
                        </td>
                    </tr>
                    <tr class="last">
                        <td colspan="2">
                            <button type="button" data-bind="click: applyFilter"><%= I18N.Monitoring.Search %></button>
                            <button type="button" data-bind="click: clearFilter"><%= I18N.Monitoring.Clear %></button>
                        </td>
                    </tr>
                </table>
            </div>
            <%
                var tableOptions = new TableOptions
                {
                    SelectUrl = Url.Action("GetProfileResults"),
                    GroupBy = "EmployeeFullName",
                    PagingDefaultSize = 50,
                    Checkboxes = Model.AllowGenerateReport || Model.AllowShiftProfiles,
                    RowsHighlighting = true,
                    CheckboxesName = "ScheduleModel.KnowledgeControlSchedule",
                    PagingAllowAll = true,
                    Columns = new[]
                    {
                        new ColumnOptions("ProfileName", I18N.Monitoring.Profile),
                        new ColumnOptions("Periodicity", I18N.Monitoring.Periodicity),
                        new ColumnOptions("DatePassed", I18N.Monitoring.DatePassed),
                        new ColumnOptions("NextDate", I18N.Monitoring.NextDate)
                    },
                    Actions = new RowAction[] {new TemplateAction("profile-results-action")}
                };
                if (Model.AllowShiftProfiles)
                    tableOptions.TableActions = new TableAction[]
                    {
                        new CustomTableAction("shiftProfileNextDate", "", I18N.Monitoring.Shift, new {@class = "shiftProfile"}) {Disabled = false}
                    };
                Html.Table(tableOptions, new { id = "search-results" }); %>
        </div>
    <% } %>
    <script type="text/javascript">
        $(function () {
            $("#search-form").collapsibleBlock({ headerText: "<%= I18N.Monitoring.ResultsSearch %>" });
            $("#search-form button, #generate-schedule button").olimpbutton();
            $("#generate-schedule select").combobox();
            $("#period input").datepicker({ changeMonth: true, changeYear: true });
            $("#profile-list").combobox();

            var searchResultsModel = $('#search-results').data('viewModel');

            var filterViewModel = {
                searchData: {
                    EmployeeFullName: ko.observable(),
                    ProfileName: ko.observable(),
                    DateBegin: ko.observable(),
                    DateEnd: ko.observable(),
                    __callback: function () {
                        if (!this.groups().length) {
                            $.Olimp.showWarning('<%= I18N.Monitoring.NothingFound %>');
                        }
                    }
                },

                applyFilter: function () {
                    var data = ko.mapping.toJS(this.searchData);
                    searchResultsModel.requestData(data);
                },
                clearFilter: function () {
                    this.searchData.EmployeeFullName("");
                    $('#profile-list').combobox('select');
                    this.searchData.DateBegin("");
                    this.searchData.DateEnd("");
                    searchResultsModel.requestData({});
                }
            };

            ko.applyBindings(filterViewModel, $('#search-form')[0]);

            var scheduleViewModel = {
                scheduleExpanded: ko.observable(false),
                toggleScheduleCollapse: function () {
                    this.scheduleExpanded(!this.scheduleExpanded());
                    return false;
                }
            };

            scheduleViewModel.toggleScheduleLinkText = ko.computed(function () {
                return this.scheduleExpanded() ? '<%= I18N.Monitoring.Hide %>' : '<%= I18N.Monitoring.Show %>';
            }, scheduleViewModel);
            if ($('#generate-schedule').length > 0)
                ko.applyBindings(scheduleViewModel, $('#generate-schedule')[0]);
        });
    </script>
</asp:Content>