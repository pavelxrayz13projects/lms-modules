﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<EmployeeProfileShiftViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Monitoring.I18N" %>

<script type="text/javascript">
    $(function () {
        window.shiftProfileNextDate = function (model) {
            if (model.items.length > 0) {
                var ids = $.map(model.items, function (r) { return 'objects=' + ko.utils.unwrapObservable(r.Id); });
                $("#employee-with-profile-ids").val(ids.join('&'));
                $('#profile-shift-dialog').olimpdialog("open");
            } else {
                $.Olimp.showWarning('<%= I18N.Monitoring.NoProfilesChosen %>');
            }
        };
        
        $("#datepicker").datepicker({ altField: "#profile-shiftdate", changeMonth: true, changeYear: true });
        $('#profile-shift-dialog').olimpdialog({
            autoOpen: false,
            draggable: false,
            resizable: false,
            modal: true,
            width: 267,
            title: '<%= I18N.Monitoring.ChooseShiftDate %>',
            buttons: {
                '<%= Olimp.I18N.PlatformShared.Save%>': function () {
                    $.post('<%= Url.Action("MonitoringProfilesShift") %>', "ShiftDate=" + $("#profile-shiftdate").val() + "&" + $("#employee-with-profile-ids").val(), function () {
                        $('#search-results').data('viewModel').reload();
                        $('#profile-shift-dialog').olimpdialog('close');
                        $.Olimp.showSuccess('<%= I18N.Monitoring.DataChanged %>');
                    });
                },
                '<%= Olimp.I18N.PlatformShared.Cancel %>': function () { $(this).olimpdialog('close'); }
            }
        });
    })
</script>
<% Html.EnableClientValidation(); %>
<div id="profile-shift-dialog">
    <center>
        <% using (Html.BeginForm("MonitoringProfilesShift", "Monitoring")) { %>
            <%= Html.HiddenFor(m => m.objects, new {id = "employee-with-profile-ids"}) %>
            <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ShiftDate) %></div>
            <div id="datepicker"></div>
            <%= Html.HiddenFor(m => m.ShiftDate, new {id = "profile-shiftdate"}) %>
        <% } %>
     </center>
</div>