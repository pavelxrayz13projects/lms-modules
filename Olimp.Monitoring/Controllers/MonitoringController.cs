﻿using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Common;
using Olimp.Domain.Exam.ProfileResults;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Monitoring.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Olimp.Monitoring.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.KnowledgeControl.Profiles.View, Order = 2)]
    public class MonitoringController : PController
    {
        private IProfileResultsProvider _profileResultsProvider;
        private IProfileShiftingService _profileShiftingService;
        private IProfileResultService _profileResultService;

        public MonitoringController(
            IProfileResultsProvider profileResultsProvider,
            IProfileShiftingService profileShiftingService,
            IProfileResultService profileResultService)
        {
            if (profileResultsProvider == null)
                throw new ArgumentNullException("profileResultsProvider");

            if (profileShiftingService == null)
                throw new ArgumentNullException("profileShiftingService");

            if (profileResultService == null)
                throw new ArgumentNullException("profileResultService");

            _profileResultsProvider = profileResultsProvider;
            _profileShiftingService = profileShiftingService;
            _profileResultService = profileResultService;
        }

        public ActionResult Index()
        {
            using (UnitOfWork.CreateRead())
            {
                //TODO: Wrap with provider
                var profilesList = Repository.Of<Profile>()
                    .OrderBy(a => a.Name)
                    .Select(a => new SelectListItem { Text = a.Name, Value = a.Name });

                return View(new MonitoringIndexViewModel
                {
                    Profiles = profilesList,
                    AllowGenerateReport = Roles.IsUserInRole(Permissions.Reports.Generate) || Roles.IsUserInRole(Permissions.Reports.ManageProfilesReportTemplates),
                    AllowShiftProfiles = Roles.IsUserInRole(Permissions.KnowledgeControl.Profiles.ScheduleMove),
                    AllowSetResults = Roles.IsUserInRole(Permissions.KnowledgeControl.Profiles.SetResults)
                });
            }
        }

        public ActionResult GetProfileResults(MonitoringSearchViewModel model)
        {
            var filter = new ProfileResultsFilter
            {
                DateBegin = model.DateBegin,
                DateEnd = model.DateEnd,
                EmployeeFullName = model.EmployeeFullName,
                ProfileName = model.ProfileName
            };

            var paging = new GroupingPagingValues
            {
                GroupByColumn = "EmployeeFullName",
                PageNumber = model.CurrentPage,
                PageSize = model.PageSize,
                SortAsc = model.SortAsc,
                SortByColumn = model.SortColumn
            };

            var results = _profileResultsProvider.Filter(filter, paging);
            var json = new
            {
                rowsCount = results.TotalCount,
                groups = results.Groups.Select(g => new
                {
                    name = g.GroupName,
                    rows = g.Entities.Select(e => new
                    {
                        Id = e.EmployeeId.ToString() + "_" + e.ProfileId,
                        e.EmployeeId,
                        Periodicity = e.Periodicity == 0 ? "&mdash;" : e.Periodicity.ToString(),
                        e.ProfileId,
                        e.ProfileName,
                        DatePassed = e.DatePassed.HasValue ? e.DatePassed.Value.ToShortDateString() : I18N.Monitoring.HasNotPassedYet,
                        NextDate = e.Periodicity == 0 && e.PlannedDate == e.NextDate ? "&mdash;" : e.NextDate.ToShortDateString()
                    })
                })
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeProfileCoursesAndEvents(EmployeeProfileCoursesAndEventsViewModel model)
        {
            var profileEmployeeInfo = _profileResultsProvider.GetEmployeeProfileInfo(model.EmployeeId, model.ProfileId);
            var json = new
            {
                employeeFullName = profileEmployeeInfo.EmployeeFullName,
                profileName = profileEmployeeInfo.ProfileName,
                companyName = profileEmployeeInfo.CompanyName,
                appointmentNames = profileEmployeeInfo.AppointmentNames,
                courses = new
                {
                    rows = profileEmployeeInfo.Courses.Select(e => new
                    {
                        e.Id,
                        e.CourseName,
                        DatePassed =
                            e.DatePassed.HasValue
                                ? e.DatePassed.Value.ToShortDateString()
                                : I18N.Monitoring.HasNotPassedYet,
                        LastFinishedDate =
                            e.LastFinishedDate.HasValue
                                ? e.LastFinishedDate.Value.ToShortDateString()
                                : I18N.Monitoring.HasNotPassedYet,
                    })
                },
                events = new
                {
                    rows = profileEmployeeInfo.Events.Select(e => new
                    {
                        e.AdditionalEventName,
                        e.AdditionalEventId,
                        e.EmployeeId,
                        e.IsPassed,
                        e.ProfileId,
                        Timestamp =
                            e.Timestamp.HasValue
                                ? e.Timestamp.Value.ToShortDateString()
                                : I18N.Monitoring.HasNotChangedYet
                    })
                }
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeForAdmin(Roles = Permissions.KnowledgeControl.Profiles.ScheduleMove, Order = 2)]
        public ActionResult MonitoringProfilesShift(EmployeeProfileShiftViewModel model)
        {
            foreach (var employeeProfileIdsString in model.objects)
            {
                var employeeProfileIdsArr = employeeProfileIdsString.Split('_');
                Guid employeeId;
                int profileId;

                if (Guid.TryParse(employeeProfileIdsArr[0], out employeeId) &&
                    int.TryParse(employeeProfileIdsArr[1], out profileId))
                    _profileShiftingService.ShiftProfile(new ShiftProfileBase
                    {
                        EmployeeId = employeeId,
                        ProfileId = profileId,
                        ShiftDate = model.ShiftDate
                    });
            }
            return null;
        }

        [AuthorizeForAdmin(Roles = Permissions.KnowledgeControl.Profiles.SetResults, Order = 2)]
        public ActionResult MonitoringProfilePassEvents(EmployeeProfileActionViewModel model)
        {
            _profileResultService.PassEvents(model.EmployeeId, model.ProfileId, model.AdditionalEvents);
            return null;
        }

        [AuthorizeForAdmin(Roles = Permissions.KnowledgeControl.Profiles.SetResults, Order = 2)]
        public ActionResult MonitoringEmployeeProfileFinish(MonitoringFinishProfileViewModel model)
        {
            _profileResultService.FinishProfile(
                model.EmployeeId, 
                model.ProfileId, 
                model.AdditionalEvents, 
                true,
                model.FinishDateTime);
            return null;
        }
    }
}