﻿using System.Globalization;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Common;
using Olimp.Domain.Exam.ProfileResults;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Monitoring.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Olimp.Monitoring.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.KnowledgeControl.Profiles.View, Order = 2)]
    public class ProfileArchiveController : PController
    {
        private IProfileResultArchiveProvider _profileResultArchiveProvider;
        private IExamAreasListProvider _examAreasListProvider;
        private IProfileResultService _profileResultService;

        public ProfileArchiveController(IProfileResultArchiveProvider profileResultArchiveProvider, IExamAreasListProvider examAreasListProvider, IProfileResultService profileResultService)
        {
            if (profileResultArchiveProvider == null)
                throw new ArgumentNullException("profileResultArchiveProvider");
            if (examAreasListProvider == null)
                throw new ArgumentNullException("examAreasListProvider");
            if (profileResultService == null)
                throw new ArgumentNullException("profileResultService");

            _profileResultArchiveProvider = profileResultArchiveProvider;
            _examAreasListProvider = examAreasListProvider;
            _profileResultService = profileResultService;
        }

        public ActionResult Index()
        {
            using (UnitOfWork.CreateRead())
            {
                //TODO: Wrap with provider
                var profilesList = Repository.Of<Profile>()
                    .OrderBy(a => a.Name)
                    .Select(a => new SelectListItem { Text = a.Name, Value = a.Name });

                var commissionsList = Repository.Of<Commission>().OrderBy(c => c.Name)
                    .Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString(CultureInfo.InvariantCulture) });

                return View(new ProfileArchiveViewModel
                {
                    Commissions = commissionsList,
                    ShowExamArea = _examAreasListProvider.IsAvailable,
                    Profiles = profilesList,
                    AllowGenerateReport = Roles.IsUserInRole(Permissions.Reports.Generate) || Roles.IsUserInRole(Permissions.Reports.ManageProfilesProtocolTemplates),
                    AllowSetResults = Roles.IsUserInRole(Permissions.KnowledgeControl.Profiles.SetResults)
                });
            }
        }

        public ActionResult GetProfileResults(ProfileArchiveSearchViewModel model)
        {
            var filter = new ProfileResultsFilter
            {
                DateBegin = model.DateBegin,
                DateEnd = model.DateEnd,
                EmployeeFullName = model.EmployeeFullName,
                ProfileName = model.ProfileName,
                ShowEmployeeWithoutGroup = model.ShowEmployeeWithoutGroup
            };

            var paging = new GroupingPagingValues
            {
                GroupByColumn = "EmployeeFullName",
                PageNumber = model.CurrentPage,
                PageSize = model.PageSize,
                SortAsc = model.SortAsc,
                SortByColumn = model.SortColumn
            };

            var results = _profileResultArchiveProvider.Filter(filter, paging);
            var json = new
            {
                rowsCount = results.TotalCount,
                groups = results.Groups.Select(g => new
                {
                    name = g.GroupName,
                    rows = g.Entities.Select(e => new
                    {
                        Id = e.ProfileResultId,
                        e.ProfileName,
                        DatePassed = e.DatePassed.ToShortDateString(),
                        ExamArea = e.ExamAreas != null  ? String.Join(", ",e.ExamAreas) : ""
                    })
                })
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeProfileResultCoursesAndEvents(int profileResultId)
        {
            var profileResultInfo = _profileResultArchiveProvider.GetProfileResultInfo(profileResultId);
            var json = new
            {
                employeeFullName = profileResultInfo.EmployeeFullName,
                profileName = profileResultInfo.ProfileName,
                companyName = profileResultInfo.CompanyName,
                appointmentNames = profileResultInfo.AppointmentNames,
                courses = new
                {
                    rows = profileResultInfo.Courses.Select(e => new
                    {
                        e.Id,
                        Date =
                            e.DatePassed.HasValue
                                ? e.DatePassed.Value.ToShortDateString()
                                : I18N.Monitoring.HasNotPassedYet,
                        e.CourseName,
                        e.IsPassed
                    })
                },
                events = new
                {
                    rows = profileResultInfo.Events.Select(e => new
                    {
                        e.AdditionalEventName,
                        e.AdditionalEventId,
                        e.IsPassed,
                        Timestamp =
                            e.Timestamp.HasValue
                                ? e.Timestamp.Value.ToShortDateString()
                                : I18N.Monitoring.HasNotChangedYet
                    })
                }
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.KnowledgeControl.Profiles.SetResults, Order = 2)]
        public ActionResult Delete(int Id)
        {
            _profileResultService.DeleteProfileResult(Id);
            return null;
        }
    }
}