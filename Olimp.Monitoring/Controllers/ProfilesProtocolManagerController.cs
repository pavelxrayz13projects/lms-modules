﻿using Olimp.Domain.Catalogue.Security;
using Olimp.Reporting;
using Olimp.Web.Controllers.Reporting;
using Olimp.Web.Controllers.Security;
using System.Web.Mvc;

namespace Olimp.Monitoring.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    public class ProfilesProtocolManagerController : ReportManagerControllerBase
    {
        protected override ReportType ReportType { get { return Reporting.ReportType.ProfilesProtocol; } }

        public override string AllowManagePermission { get { return Permissions.Reports.ManageProfilesProtocolTemplates; } }

        public ActionResult Library()
        {
            return View();
        }
    }
}