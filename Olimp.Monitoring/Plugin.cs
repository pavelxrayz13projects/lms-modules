using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Catalogue;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Exam.NHibernate.ProfileResults;
using Olimp.Domain.Exam.ProfileResults;
using Olimp.Monitoring.Controllers;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Monitoring.Plugin))]

namespace Olimp.Monitoring
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        { }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(MonitoringController),
                typeof(ProfilesReportManagerController),
                typeof(ProfilesProtocolManagerController),
                typeof(ProfileArchiveController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (this.Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;
            var materialsModule = PContext.ExecutingModule as OlimpMaterialsModule;
            if (materialsModule == null)
                throw new InvalidOperationException("Executing module must be OlimpMaterialsModule");

            var facadeRepository = materialsModule.Materials.CourseRepository;
            var connFactoryFactory = new PerHttpRequestConnectionFactoryFactory(
                new SqliteConnectionFactoryFactory());

            var connFactory = connFactoryFactory.Create();

            var nodeRepository = new OrmLiteNodeRepository(connFactory);
            var courseRepository = new OrmLiteCourseRepository(this.Context.NodeData, connFactory, materialsModule.Materials.CourseRepository, nodeRepository);

            var profileResultsProvider = new NHibernateProfileResultsProvider(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                courseRepository);
            var profileShiftingService = new NHibernateProfileShiftingService(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory);
            var profileResultsService = new NHibernateProfileResultService(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                facadeRepository,
                courseRepository);

            binder.Controller<MonitoringController>(b => b
                .Bind<IProfileResultsProvider>(() => profileResultsProvider)
                .Bind<IProfileShiftingService>(() => profileShiftingService)
                .Bind<IProfileResultService>(() => profileResultsService));

            var profileResultArchiveProvider = new NHibernateProfileResultArchiveProvider(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory);
            var examAreasListProvider = new CachedExamAreasListProvider(
                new XmlExamAreasListProvider(OlimpPaths.Default.ExamAreasListFile));

            binder.Controller<ProfileArchiveController>(b => b
                .Bind<IProfileResultArchiveProvider>(() => profileResultArchiveProvider)
                .Bind<IExamAreasListProvider>(() => examAreasListProvider)
                .Bind<IProfileResultService>(() => profileResultsService));
        }
    }
}