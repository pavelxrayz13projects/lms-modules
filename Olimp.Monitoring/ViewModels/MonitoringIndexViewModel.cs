﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Olimp.Monitoring.ViewModels
{
    public class MonitoringIndexViewModel
    {
        public IEnumerable<SelectListItem> Profiles { get; set; }

        public bool AllowGenerateReport { get; set; }

        public bool AllowShiftProfiles { get; set; }

        public bool AllowSetResults { get; set; }
    }
}