﻿using System;

namespace Olimp.Monitoring.ViewModels
{
    public class MonitoringFinishProfileViewModel : EmployeeProfileActionViewModel
    {
        public DateTime FinishDateTime { get; set; }
    }
}