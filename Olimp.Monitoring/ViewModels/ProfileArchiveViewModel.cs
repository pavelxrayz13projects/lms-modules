﻿using Olimp.Reporting;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Olimp.Monitoring.ViewModels
{
    public class ProfileArchiveViewModel
    {
        public class CustomProfilesProtocolModel : ProfilesProtocolModel
        {
            public new string Date { get; set; }
        }

        public ProfileArchiveViewModel()
        {
            this.ProfilesProtocolModel = new ProfileArchiveViewModel.CustomProfilesProtocolModel
            {
                Date = DateTime.Today.ToShortDateString()
            };
        }

        public bool ShowExamArea { get; set; }

        public bool AllowGenerateReport { get; set; }

        public bool AllowSetResults { get; set; }

        public IEnumerable<SelectListItem> Profiles { get; set; }

        public CustomProfilesProtocolModel ProfilesProtocolModel { get; set; }

        public IEnumerable<SelectListItem> Commissions { get; set; }

        public IEnumerable<SelectListItem> Companies { get; set; }
    }
}