﻿using Olimp.UI.ViewModels;
using System;

namespace Olimp.Monitoring.ViewModels
{
    public class MonitoringSearchViewModel : TableViewModel
    {
        public string EmployeeFullName { get; set; }

        public string ProfileName { get; set; }

        public DateTime? DateBegin { get; set; }

        public DateTime? DateEnd { get; set; }
    }
}