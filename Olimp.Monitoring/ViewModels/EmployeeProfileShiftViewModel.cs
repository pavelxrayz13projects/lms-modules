﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Olimp.Monitoring.ViewModels
{
    public class EmployeeProfileShiftViewModel
    {
        public ICollection<string> objects { get; set; }

        [Required(ErrorMessageResourceType = typeof(I18N.Monitoring), ErrorMessageResourceName = "DateRequired")]
        public DateTime ShiftDate { get; set; }
    }
}