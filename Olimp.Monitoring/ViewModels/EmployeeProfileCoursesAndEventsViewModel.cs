﻿using System;

namespace Olimp.Monitoring.ViewModels
{
    public class EmployeeProfileCoursesAndEventsViewModel
    {
        public Guid EmployeeId { get; set; }

        public int ProfileId { get; set; }
    }
}