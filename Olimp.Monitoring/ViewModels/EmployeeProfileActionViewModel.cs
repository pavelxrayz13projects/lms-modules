﻿using System;

namespace Olimp.Monitoring.ViewModels
{
    public class EmployeeProfileActionViewModel
    {
        public Guid EmployeeId { get; set; }

        public int ProfileId { get; set; }

        public int[] AdditionalEvents { get; set; }
    }
}