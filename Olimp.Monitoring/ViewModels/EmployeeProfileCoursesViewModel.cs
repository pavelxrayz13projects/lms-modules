﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Olimp.Monitoring.ViewModels
{
    public class EmployeeProfileCoursesViewModel
    {
        public Guid EmployeeId { get; set; }
        public int ProfileId { get; set; }
    }
}