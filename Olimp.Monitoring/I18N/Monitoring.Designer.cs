﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Olimp.Monitoring.I18N {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Monitoring {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Monitoring() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Olimp.Monitoring.I18N.Monitoring", typeof(Monitoring).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Наименование мероприятия.
        /// </summary>
        public static string AdditionalEventName {
            get {
                return ResourceManager.GetString("AdditionalEventName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дополнительные мероприятия.
        /// </summary>
        public static string AdditionalEvents {
            get {
                return ResourceManager.GetString("AdditionalEvents", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дата последнего изменения.
        /// </summary>
        public static string AdditionalEventTimestamp {
            get {
                return ResourceManager.GetString("AdditionalEventTimestamp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Должность.
        /// </summary>
        public static string Appointment {
            get {
                return ResourceManager.GetString("Appointment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Архив.
        /// </summary>
        public static string Archive {
            get {
                return ResourceManager.GetString("Archive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите дату завершения.
        /// </summary>
        public static string ChooseFinishDate {
            get {
                return ResourceManager.GetString("ChooseFinishDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите профиль....
        /// </summary>
        public static string ChooseProfile {
            get {
                return ResourceManager.GetString("ChooseProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите дату переноса.
        /// </summary>
        public static string ChooseShiftDate {
            get {
                return ResourceManager.GetString("ChooseShiftDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Очистить.
        /// </summary>
        public static string Clear {
            get {
                return ResourceManager.GetString("Clear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Организация.
        /// </summary>
        public static string Company {
            get {
                return ResourceManager.GetString("Company", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Результат будет удален. Продолжить?.
        /// </summary>
        public static string ConfirmDelete {
            get {
                return ResourceManager.GetString("ConfirmDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Сдать дополнительные мероприятия работника?.
        /// </summary>
        public static string ConfirmPassEvents {
            get {
                return ResourceManager.GetString("ConfirmPassEvents", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Наименование курса.
        /// </summary>
        public static string CourseName {
            get {
                return ResourceManager.GetString("CourseName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Результаты по курсам.
        /// </summary>
        public static string CourseResults {
            get {
                return ResourceManager.GetString("CourseResults", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Данные изменены..
        /// </summary>
        public static string DataChanged {
            get {
                return ResourceManager.GetString("DataChanged", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дата предыдущей сдачи.
        /// </summary>
        public static string DatePassed {
            get {
                return ResourceManager.GetString("DatePassed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дата необходима для заполнения.
        /// </summary>
        public static string DateRequired {
            get {
                return ResourceManager.GetString("DateRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дата последней успешной сдачи.
        /// </summary>
        public static string DateSuccessPassed {
            get {
                return ResourceManager.GetString("DateSuccessPassed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Мероприятия обновлены.
        /// </summary>
        public static string EventsUpdated {
            get {
                return ResourceManager.GetString("EventsUpdated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Область аттестации.
        /// </summary>
        public static string ExamArea {
            get {
                return ResourceManager.GetString("ExamArea", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Не сдано.
        /// </summary>
        public static string Failed {
            get {
                return ResourceManager.GetString("Failed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Завершить профиль.
        /// </summary>
        public static string FinishProfile {
            get {
                return ResourceManager.GetString("FinishProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ФИО.
        /// </summary>
        public static string FullName {
            get {
                return ResourceManager.GetString("FullName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Сформировать график.
        /// </summary>
        public static string GenerateSchedule {
            get {
                return ResourceManager.GetString("GenerateSchedule", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Не было изменений.
        /// </summary>
        public static string HasNotChangedYet {
            get {
                return ResourceManager.GetString("HasNotChangedYet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Не было сдачи.
        /// </summary>
        public static string HasNotPassedYet {
            get {
                return ResourceManager.GetString("HasNotPassedYet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Библиотека шаблонов.
        /// </summary>
        public static string Header {
            get {
                return ResourceManager.GetString("Header", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to скрыть.
        /// </summary>
        public static string Hide {
            get {
                return ResourceManager.GetString("Hide", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Сдано.
        /// </summary>
        public static string IsPassed {
            get {
                return ResourceManager.GetString("IsPassed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дата последней сдачи.
        /// </summary>
        public static string LastFinishedDate {
            get {
                return ResourceManager.GetString("LastFinishedDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Мониторинг.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дата следующей сдачи.
        /// </summary>
        public static string NextDate {
            get {
                return ResourceManager.GetString("NextDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Нет выбранных профилей..
        /// </summary>
        public static string NoProfilesChosen {
            get {
                return ResourceManager.GetString("NoProfilesChosen", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to По заданным параметрам не найдено ни одного результата..
        /// </summary>
        public static string NothingFound {
            get {
                return ResourceManager.GetString("NothingFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Сдано.
        /// </summary>
        public static string Passed {
            get {
                return ResourceManager.GetString("Passed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Сдать мероприятия.
        /// </summary>
        public static string PassEvents {
            get {
                return ResourceManager.GetString("PassEvents", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Период.
        /// </summary>
        public static string Period {
            get {
                return ResourceManager.GetString("Period", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to начало.
        /// </summary>
        public static string PeriodBegin {
            get {
                return ResourceManager.GetString("PeriodBegin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to окончание.
        /// </summary>
        public static string PeriodEnd {
            get {
                return ResourceManager.GetString("PeriodEnd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Периодичность (в месяцах).
        /// </summary>
        public static string Periodicity {
            get {
                return ResourceManager.GetString("Periodicity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Профиль.
        /// </summary>
        public static string Profile {
            get {
                return ResourceManager.GetString("Profile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Профиль завершен..
        /// </summary>
        public static string ProfileFinished {
            get {
                return ResourceManager.GetString("ProfileFinished", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Результаты по профилю.
        /// </summary>
        public static string ProfileResults {
            get {
                return ResourceManager.GetString("ProfileResults", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поиск по результатам.
        /// </summary>
        public static string ResultsSearch {
            get {
                return ResourceManager.GetString("ResultsSearch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to График.
        /// </summary>
        public static string Schedule {
            get {
                return ResourceManager.GetString("Schedule", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Формирование графика.
        /// </summary>
        public static string ScheduleForming {
            get {
                return ResourceManager.GetString("ScheduleForming", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поиск.
        /// </summary>
        public static string Search {
            get {
                return ResourceManager.GetString("Search", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Перенести.
        /// </summary>
        public static string Shift {
            get {
                return ResourceManager.GetString("Shift", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to показать.
        /// </summary>
        public static string Show {
            get {
                return ResourceManager.GetString("Show", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Показывать работников, не прошедших предварительную регистрацию.
        /// </summary>
        public static string ShowEmployeeWithoutGroup {
            get {
                return ResourceManager.GetString("ShowEmployeeWithoutGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Просмотр профиля.
        /// </summary>
        public static string ShowProfileTitle {
            get {
                return ResourceManager.GetString("ShowProfileTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Шаблон графика.
        /// </summary>
        public static string TemplateHeader {
            get {
                return ResourceManager.GetString("TemplateHeader", resourceCulture);
            }
        }
    }
}
