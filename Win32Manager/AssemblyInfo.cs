using PAPI.Core;
using System.Reflection;
using Win32Manager;

[assembly: AssemblyTitle("Win32Manager")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: EncryptedAssemblyReference("Olimp.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=bd83c7d5d855788a")]
[assembly: Bind(
    Context = typeof(ModuleFactory),
    PropertyName = "Runner",
    TargetType = typeof(Runner))]