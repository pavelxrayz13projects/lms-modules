using PAPI.Core;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Win32Manager
{
    public class Runner : IModuleRunner
    {
        public void Run(IModule module, IDictionary<string, string> moduleParams)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ManageForm());
        }
    }
}