using System;
using System.Net;
using System.Windows.Forms;

namespace Win32Manager
{
    public class DataGridViewEx : DataGridView
    {
        protected override void SetSelectedCellCore(int columnIndex, int rowIndex, bool selected)
        {
            this.Rows[rowIndex].Cells[columnIndex].Selected = selected;
        }

        protected override void SetSelectedRowCore(int rowIndex, bool selected)
        {
            base.SetSelectedRowCore(rowIndex, selected);

            this.OnSelectionChanged(EventArgs.Empty);
        }

        protected override void OnColumnAdded(DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
            e.Column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            base.OnColumnAdded(e);
        }

        protected override void OnCellValidating(DataGridViewCellValidatingEventArgs e)
        {
            base.OnCellValidating(e);

            IPAddress ip;
            if (this.Columns[e.ColumnIndex].ValueType.Equals(typeof(IPAddress)) && !IPAddress.TryParse(e.FormattedValue.ToString(), out ip))
                e.Cancel = true;
        }
    }
}