﻿using Olimp.Core;
using Olimp.LocalNodes.Contracts.LocalNodeController;
using Olimp.LocalNodes.Contracts.LocalNodeController.DataContracts;
using Olimp.LocalNodes.Core.Cluster;
using Olimp.LocalNodes.Core.Services.Clients;
using PAPI.Core.Networking;
using PAPI.Core.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using Win32Manager.Properties;

namespace Win32Manager
{
    public partial class ManageForm : Form
    {
        private readonly DataTable _table;
        private readonly Timer _timer;

        private bool _isDirty;
        private readonly string _serviceUrl;

        public ManageForm()
        {
            InitializeComponent();

            _serviceUrl = OlimpPaths.OlimpServiceUri + "/Cluster";

            _table = new DataTable();
            _table.Columns.AddRange(new[] {
				new DataColumn("Id", typeof(string)) { Caption = "Имя", },
				new DataColumn("Address", typeof(IPAddress)) { Caption = "Интерфейс" },
				new DataColumn("Port", typeof(int)) { Caption = "Порт" },
				new DataColumn("Status", typeof(bool)) { Caption = "Статус" }
			});

            _table.PrimaryKey = new[] { _table.Columns[0] };

            dataGridView1.ColumnAdded += HandleDataGridViewColumnAdded;
            dataGridView1.SelectionChanged += HandleDataGridViewSelectionChanged;
            dataGridView1.CellValidating += HandleDataGridViewCellValidating;
            dataGridView1.CellEndEdit += HandleDataGridViewCellEndEdit;

            dataGridView1.DataSource = _table;

            _timer = new Timer { Interval = 15000 };
            _timer.Tick += delegate
                               {
                                   if (!_isDirty) UpdateTable();
                               };

            startButton.Click += HandleStartButtonClick;
            stopButton.Click += HandleStopButtonClick;
            saveButton.Click += HandleSaveButtonClick;
        }

        private void HandleDataGridViewCellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            _isDirty = true;
        }

        private void HandleDataGridViewCellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex != 2) return;
            if (e.FormattedValue.ToString() == _table.Rows[e.RowIndex][e.ColumnIndex].ToString()) return;

            int port;
            if (!int.TryParse(e.FormattedValue.ToString(), out port) || port > 65536 || port < 0)
            {
                e.Cancel = true;
                return;
            }

            if (EqualsOneOfOtherNodesPorts(port, e.RowIndex))
            {
                e.Cancel = true;
                return;
            }

            if (!IsNoRestrictedAndAvailablePort(port)) e.Cancel = true;
        }

        private void HandleSaveButtonClick(object sender, EventArgs e)
        {
            if (!_isDirty) return;

            dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);

            var sc = new DefaultLocalNodeControllerServiceClient(_serviceUrl);

            var failed = false;

            var nInfos = sc.GetAllNodes().ToList();

            foreach (DataRow r in _table.Rows)
            {
                var nodeName = r["Id"].ToString();
                var nodeStateInfo = nInfos.FirstOrDefault(nInfo => nInfo.Node.Name == nodeName);

                if (nodeStateInfo == null)
                {
                    MessageBox.Show(string.Format("Не найдена конфигурация для узла [{0}].", r["Id"]));
                    return;
                }

                nodeStateInfo.Node.Port = (int)r["Port"];
                nodeStateInfo.Node.Params["ip"] = r["Address"].ToString();

                var result = sc.UpdateNodeInfo(nodeStateInfo.Node);

                if (result != NodeControllerResult.Success)
                {
                    failed = true;

                    MessageBox.Show(
                        string.Concat(
                            "Не удалось сохранить натсройки для ",
                            nodeStateInfo.Node.Name,
                            Environment.NewLine,
                            result.GetMessage(nodeStateInfo.Node.Name)));

                    break;
                }
            }

            if (!failed)
            {
                _isDirty = false;
                MessageBox.Show(Resources.SettingsSavedSuccessfully);
            }
        }

        private void HandleDataGridViewSelectionChanged(object sender, EventArgs e)
        {
            ToggleButtonsEnabled();
        }

        private void ToggleButtonsEnabled()
        {
            startButton.Enabled = false;
            stopButton.Enabled = false;

            foreach (DataGridViewRow r in dataGridView1.SelectedRows)
            {
                if ((bool)_table.Rows[r.Index]["Status"]) stopButton.Enabled = true;
                else startButton.Enabled = true;

                if (stopButton.Enabled && startButton.Enabled) break;
            }
        }

        private void HandleStartButtonClick(object sender, EventArgs e)
        {
            ToggleNodes(true);
        }

        private void HandleStopButtonClick(object sender, EventArgs e)
        {
            ToggleNodes(false);
        }

        private void HandleDataGridViewColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.HeaderText = _table.Columns[e.Column.Index].Caption;

            if (e.Column.Name == "Status" || e.Column.Name == "Id") e.Column.ReadOnly = true;
        }

        private void ToggleNodes(bool on)
        {
            var sc = new DefaultLocalNodeControllerServiceClient(_serviceUrl);
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                var r = _table.Rows[row.Index];
                var name = r["Id"].ToString();

                var result = on ? sc.Activate(name) : sc.Deactivate(name);

                if (result == NodeControllerResult.Success)
                {
                    r["Status"] = on;
                }
                else
                {
                    MessageBox.Show(string.Concat("Не удалось изменить статус ", name, ": ", result.GetMessage(name)));
                    break;
                }
            }

            ToggleButtonsEnabled();
        }

        private void BindDataTable(IEnumerable<NodeStateContract> nodes)
        {
            foreach (var n in nodes)
            {
                string ip;
                var address = n.Node.Params.TryGetValue("ip", out ip) ? IPAddress.Parse(ip) : IPAddress.Any;

                var row = _table.Rows.Find(n.Node.Name);
                if (row != null)
                {
                    row["Address"] = address;
                    row["Port"] = n.Node.Port;
                    row["Status"] = n.IsAlive;
                }
                else
                    _table.Rows.Add(n.Node.Name, address, n.Node.Port, n.IsAlive);
            }

            ToggleButtonsEnabled();
        }

        private void UpdateTable()
        {
            try
            {
                using (var sc = new ServiceClient<ILocalNodeControllerService>(_serviceUrl))
                {
                    var nodes = sc.Service.GetAllNodes();
                    BindDataTable(nodes);
                }
            }
            catch (Exception ee)
            {
                Console.WriteLine(ee);
            }
        }

        private bool EqualsOneOfOtherNodesPorts(int port, int rowIndex)
        {
            for (var i = 0; i < _table.Rows.Count; i++)
            {
                int otherNodePort;
                if (!int.TryParse(_table.Rows[i]["Port"].ToString(), out otherNodePort)) continue;

                if (otherNodePort == port && rowIndex != i)
                {
                    MessageBox.Show(Resources.NodeValidatorNodePortIsNotUnique);
                    return true;
                }
            }

            return false;
        }

        private static bool IsNoRestrictedAndAvailablePort(int port)
        {
            if (PortScanner.IsRestricted(port))
            {
                MessageBox.Show(Resources.NodeValidatorNodePortIsNotSupportedByBrowsers);
                return false;
            }

            if (PortScanner.IsAvailable(port)) return true;

            MessageBox.Show(Resources.NodeValidatorNodePortIsBusy);
            return false;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            UpdateTable();

            foreach (DataGridViewRow r in dataGridView1.Rows) r.Selected = false;

            _timer.Start();
        }
    }
}