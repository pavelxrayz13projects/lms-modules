﻿using Olimp.Core;
using Olimp.Core.Watchdog;
using PAPI.Core;
using PAPI.Core.Services;
using PAPI.Core.Web;
using PAPI.Core.Web.Hosting;
using PAPI.Core.Web.Hosting.PHttp;
using System;
using System.Collections.Generic;

namespace Olimp
{
    public class WatchdogOlimpRunner : OlimpRunner
    {
        public override void Run(IModule module, IDictionary<string, string> moduleParams)
        {
            PHttpServer.DontLog = path => path.StartsWith("/!/watchdog", StringComparison.OrdinalIgnoreCase);

            base.Run(module, moduleParams);
        }

        protected override IHttpServer ConfigureHttpServerProvider(AspNetModule aspNetModule, IDictionary<string, string> moduleParams)
        {
            var server = base.ConfigureHttpServerProvider(aspNetModule, moduleParams);

            server.ServerStateChanged += server_ServerStateChanged;

            return server;
        }

        private void server_ServerStateChanged(object sender, HttpServerStateChangedEventArgs e)
        {
            if (e.State != HttpServerState.Started && e.State != HttpServerState.Stopped)
                return;

            try
            {
                using (var sc = new ServiceClient<IWatchdogServiceContract>(OlimpPaths.OlimpWatchdogServiceUri))
                {
                    if (e.State == HttpServerState.Started)
                        sc.Service.Pause(PContext.ExecutingModule.Context.NodeData.Name, false);
                    else
                        sc.Service.Pause(PContext.ExecutingModule.Context.NodeData.Name, true);
                }
            }
            catch (Exception ee)
            { }
        }
    }
}