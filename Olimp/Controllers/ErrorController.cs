using Olimp.Web.Controllers.Base;
using System.Web.Mvc;

namespace Olimp.Controllers
{
    public class ErrorController : PController
    {
        public ActionResult Index()
        {
            return View("Error");
        }

        public ActionResult Error404()
        {
            return View();
        }

        public ActionResult Error403()
        {
            return View();
        }
    }
}