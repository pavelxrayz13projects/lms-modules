﻿using Funq;
using Olimp.Core;
using Olimp.Core.Web;
using Olimp.Courses.Facades.Course;
using Olimp.Courses.Facades.Normative;
using Olimp.Courses.Generation.Generators.Course;
using Olimp.Courses.Generation.InfoPath;
using Olimp.Courses.Generation.Tickets;
using Olimp.Courses.Model.Dto;
using Olimp.Courses.Security;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.Ldap;
using Olimp.Domain.Catalogue.Normatives;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Catalogue;
using Olimp.Domain.Catalogue.OrmLite.GlobalStorage;
using Olimp.Domain.Catalogue.OrmLite.Ldap;
using Olimp.Domain.Catalogue.OrmLite.Normatives;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Catalogue.OrmLite.Repositories.Lazy;
using Olimp.Domain.Catalogue.OrmLite.Security;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Common;
using Olimp.Domain.Common.Nodes;
using Olimp.Domain.Common.Security;
using Olimp.Domain.Exam.NHibernate.Catalogue;
using Olimp.Domain.Exam.NHibernate.ProfileResults;
using Olimp.Domain.Exam.ProfileResults;
using Olimp.Ldap;
using Olimp.Ldap.Web;
using Olimp.Reporting;
using PAPI.Core;
using PAPI.Core.Security;
using ServiceStack.OrmLite;
using System;
using System.IO;
using FacadeRepos = Olimp.Core.Model.Repositories;
using PAPIData = PAPI.Core.Data;

namespace Olimp
{
    public class Container
    {
        public static Funq.Container Instance { get { return _container; } }

        private static readonly Funq.Container _container;

        static Container()
        {
            _container = Create();
        }

        public static Funq.Container Create()
        {
            return Create(PContext.ExecutingModule);
        }

        private static DistributionInfo GetDistributionInfo(Funq.Container container)
        {
            var distributionInfo = container.Resolve<IDistributionInfoProvider>().Get();
            if (distributionInfo == null)
                throw new FileNotFoundException("Distribution info file wasn't found");

            return distributionInfo;
        }

        private static IReportEngine GetReportEngine(Funq.Container container)
        {
            var nodeData = container.Resolve<NodeData>();

            return new ReportEngine(
                nodeData,
                container.Resolve<PAPIData.IUnitOfWorkFactory>(),
                container.Resolve<IWholeCatalogueProviderFactory>()
                    .Create(CatalogueLockingPolicy.NoLocking, nodeData.Id));
        }

        public static Funq.Container Create(IModule module)
        {
            if (module == null)
                throw new ArgumentNullException("module");

            //TODO: remove this
            var materialsModule = module as OlimpMaterialsModule;
            if (materialsModule == null)
                throw new InvalidOperationException("Executing module must be OlimpMaterialsModule");

            //just adapter
            var activationNumberProvider = new OlimpModuleActivationNumberProvider(
                materialsModule.Activation.ActivationNumberProvider);

            return Create(
                module.Context.NodeData,
                module.Context.DistributionInfoProvider,
                materialsModule.Materials.CourseRepository,
                materialsModule.Materials.NormativeRepository,
                module.Database.RepositoryFactory,
                module.Database.UnitOfWorkFactory,
                activationNumberProvider);
        }

        public static Funq.Container Create(
            NodeData nodeData,
            IDistributionInfoProvider distributionInfoProvider,
            FacadeRepos.IMaterialRepository<ICourseFacade> courseFacadeRepo,
            FacadeRepos.IMaterialRepository<INormativeFacade> normativeFacadeRepo,
            PAPIData.IRepositoryFactory repoFactory,
            PAPIData.IUnitOfWorkFactory uowFactory,
            IActivationNumberProvider activationNumberProvider)
        {
            if (nodeData == null)
                throw new ArgumentNullException("nodeData");

            if (distributionInfoProvider == null)
                throw new ArgumentNullException("distributionInfoProvider");

            if (courseFacadeRepo == null)
                throw new ArgumentNullException("courseFacadeRepo");

            if (normativeFacadeRepo == null)
                throw new ArgumentNullException("normativeFacadeRepo");

            if (repoFactory == null)
                throw new ArgumentNullException("repoFactory");

            if (uowFactory == null)
                throw new ArgumentNullException("uowFactory");

            var container = new Funq.Container();

            container.Register<NodeData>(nodeData);

            container.Register<PAPIData.IRepositoryFactory>(repoFactory);
            container.Register<PAPIData.IUnitOfWorkFactory>(uowFactory);

            container.Register<FacadeRepos.IMaterialRepository<ICourseFacade>>(courseFacadeRepo);
            container.Register<FacadeRepos.IMaterialRepository<INormativeFacade>>(normativeFacadeRepo);

            container.RegisterAs<SqliteConnectionFactoryFactory, IDbConnectionFactoryFactory>();
            container.Register<IDbConnectionFactory>(c => c.Resolve<IDbConnectionFactoryFactory>().Create());

            container.RegisterAs<OrmLiteNodeRepository, INodesRepository>();
            container.RegisterAs<OrmLiteNodeRepository, IRepository<Node, Guid>>();

            container.RegisterAs<IntegratedRolesProvider, IRolesProvider>();

            container.RegisterAs<OrmLiteUserRepository, IUserRepository>();

            container.RegisterAs<OrmLiteCourseRepository, ICourseRepository>();

            container.RegisterAs<NHibernateProfileResultsProvider, IProfileResultsProvider>();

            container.RegisterAs<OrmLitePersonalCatalogueProvider, IPersonalCatalogueProvider>();
            container.RegisterAs<DefaultCatalogueTreeBuilder, ICatalogueTreeBuilder>();
            container.RegisterAs<NHibernateProfileBranchesProvider, IProfileBranchesProvider>();

            container.RegisterAs<OrmLiteWholeCatalogueProviderFactory, IWholeCatalogueProviderFactory>();

            container.Register<IReportEngine>(GetReportEngine);

            container.RegisterAs<NHibernateProfileResultArchiveProvider, IProfileResultArchiveProvider>();

            container.RegisterAs<NHibernateProfileResultService, IProfileResultService>();

            container.RegisterAs<OrmLiteNormativeRepository, INormativeRepository>();
            container.RegisterAs<OrmLiteNormativeExcerptTopicRepository, INormativeExcerptTopicRepository>();

            container.RegisterAs<OrmLiteNormativeExcerptRepository, INormativeExcerptRepository>();

            container.Register<Olimp.Courses.Security.ICryptoService>(c => new CryptoService(c.Resolve<DistributionInfo>().GetDistributionIdBytes()));
            container.Register<IDistributionInfoProvider>(distributionInfoProvider);
            container.Register<DistributionInfo>(GetDistributionInfo);

            container.RegisterAs<RoleProviderPermissionChecker, IPermissionChecker>();

            container.RegisterAs<OrmLitePredefinedCatalogueBranchesProvider, IPredefinedCatalogueBranchesProvider>();
            container.RegisterAs<OrmLiteContainerBranchRepository, IContainerBranchRepository>();
            container.RegisterAs<LazyGeneratedFromNormativesUserMaterialBranchFactory, IGeneratedFromNormativesUserMaterialBranchFactory>();
            container.RegisterAs<SimpletTicketGeneratorFactory, ITicketGeneratorFactory<TopicDto, Guid, TicketDto>>();
            container.RegisterAs<EcpCourseService, ICourseService>();
            container.RegisterAs<OrmLiteNormativeTestGenerator, INormativeTestGenerator>();
            container.Register<IGeneratedFromNormativesBranchRepository>(c =>
                new PermissionInsuredGeneratedFromNormativesBranchRepository(
                    new OrmLiteGeneratedFromNormativesBranchRepository(
                        c.Resolve<NodeData>(),
                        c.Resolve<IDbConnectionFactory>(),
                        c.Resolve<INormativeRepository>(),
                        c.Resolve<IContainerBranchRepository>(),
                        c.Resolve<IGeneratedFromNormativesUserMaterialBranchFactory>(),
                        c.Resolve<INormativeTestGenerator>()),
                    c.Resolve<IPermissionChecker>()));

            container.RegisterAs<OrmLiteNormativeExcerptImporter, INormativeExcerptImporter>();
            container.RegisterAs<OrmLiteNormativesNodeIsolatedAccessorFactory, INormativesNodeIsolatedAccessorFactory>();

            container.RegisterAs<OrmLiteUpdateEventLogger, IUpdateEventLogger>();

            container.RegisterAs<OrmLiteNormativeExcerptTreeProvider, INormativeExcerptTreeProvider>();

            container.RegisterAs<OrmLiteNormativeExcerptTopicQuestionRepositoryFactory, INormativeExcerptTopicQuestionRepositoryFactory>();

            container.RegisterAs<OrmLiteNormativeContentsProvider, INormativeContentsProvider>();

            container.RegisterAs<OrmLiteNormativeQuestionsProvider, INormativeQuestionsProvider>();

            container.RegisterAs<OrmLiteProfessionRepository, IProfessionRepository>();

            container.RegisterAs<OrmLiteNormativeExcerptSerializer, INormativeExcerptSerializer>();

            container.Register<InfoPathCatalogueSerializer>(c => new InfoPathCatalogueSerializer(MaterialType.None));
            container.Register<IExcerptCatalogueExporter>(c =>
                new OrmLiteExcerptCatalogueExporter(c.Resolve<InfoPathCatalogueSerializer>()));

            container.Register<IActivationNumberProvider>(activationNumberProvider);

            container.RegisterAs<OrmLiteLdapPermissionRuleRepository, ILdapPermissionRulesRepository>();

            container.Register<UsersRepositoryRoleProvider>(c =>
                new UsersRepositoryRoleProvider(c.Resolve<IUserRepository>(), c.Resolve<IRolesProvider>()));

            container.Register<UsersRepositoryMembershipProvider>(c =>
                new UsersRepositoryMembershipProvider(c.Resolve<IUserRepository>()));

            container.Register<ILdapUsersAccessor>(c => new LdapUsersAccessor(LdapScheme.CurrentSchemeForSystemUsers))
                .ReusedWithin(ReuseScope.Request);

            container.RegisterAs<LdapMembershipProvider, LdapMembershipProvider>().ReusedWithin(ReuseScope.Request);
            
            container.RegisterAs<LdapRoleProvider, LdapRoleProvider>().ReusedWithin(ReuseScope.Request);

            container.RegisterAs<OrmLiteLdapAuthSchemeRepository, ILdapAuthSchemeRepository>();

            container.RegisterAs<OrmLiteJsonStorage, IGlobalConfigurationStorage>();
            GlobalConfiguration.SetStorage(container.Resolve<IGlobalConfigurationStorage>());

            return container;
        }
    }
}