using Olimp;
using Olimp.Core;
using Olimp.Domain.LearningCenter;
using PAPI.Core;
using PAPI.Core.Diagnostics;
using System.Reflection;

[assembly: AssemblyVersion("4.1.7.*")]
[assembly: DebugMode(true)]
[assembly: EncryptedAssemblyReference("Olimp.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=bd83c7d5d855788a")]
[assembly: EncryptedAssemblyReference("Olimp.Courses, Version=1.0.0.0, Culture=neutral, PublicKeyToken=bd83c7d5d855788a")]
[assembly: EncryptedAssemblyReference("Olimp.Domain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=bd83c7d5d855788a")]
[assembly: ModuleFactory(typeof(LearningCenterModuleFactory))]
[assembly: Bind(
    Context = typeof(OlimpModuleFactory),
    BindingName = "Runner",
    TargetType = typeof(WatchdogOlimpRunner))]
/*[assembly: Bind(
	Context = typeof(OlimpRunner),
	PropertyName = "FormFactory",
	TargetType = typeof(HttpServerFormFactory))]*/