﻿using System.Web.Mvc;
using Olimp.Core;

namespace Olimp
{
    class OlimpDefaultHandleErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            OlimpApplication.Logger.Error(filterContext.Exception.ToString());
            base.OnException(filterContext);
        }
    }
}