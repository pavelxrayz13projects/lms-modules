﻿using PAPI.Core;
using PAPI.Core.Initialization;

namespace Olimp
{
    public static class BinderExtensions
    {
        public static IBindingBuilder Bind<T>(this IBindingBuilder binder, Funq.Container container)
        {
            return binder.Bind<T>(() => container.Resolve<T>());
        }
    }
}