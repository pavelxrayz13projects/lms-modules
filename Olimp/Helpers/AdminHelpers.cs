﻿using Olimp.Domain.Catalogue.Security;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Security;

namespace Olimp.Helpers
{
    public static class AdminHelpers
    {
        public static void RenderLogoutLink(this HtmlHelper html)
        {
            var writer = html.ViewContext.Writer;

            html.BeginForm("Logout", "Auth", new { area = "" }, FormMethod.Post, new { id = "logout-form" })
                .Dispose();

            writer.Write(@"<script type=""text/javascript"">");
            writer.Write(@"$(function(){");
            writer.Write(@"$('#logout-link').click(function(){");
            writer.Write(@"$('#logout-form')[0].submit();");
            writer.Write(@"});");
            writer.Write(@"});");
            writer.Write(@"</script>");

            writer.Write(@"<a id=""logout-link"" class=""status-link"" href=""javascript: logout()"">");
            writer.Write(@"<span></span>");
            writer.Write(@"<table><tr><td>");
            writer.Write(@"<div class=""icon icon-exit""></div>");
            writer.Write(@"</td><td class=""text"">{0}</td></tr></table>", I18N.Admin.Logout);
            writer.Write(@"</a>");
        }

        public static void RenderAboutLink(this HtmlHelper html, bool ignoreRoles = false)
        {
            if (!ignoreRoles && !Roles.IsUserInRole(Permissions.About.Client) && !Roles.IsUserInRole(Permissions.About.Server))
                return;

            var writer = html.ViewContext.Writer;

            var url = new UrlHelper(html.ViewContext.RequestContext).Action("Index", "About", new { area = "Admin" });

            writer.Write(@"<script type=""text/javascript"">");
            writer.Write(@"$(function(){");
            writer.Write(@"$('#about-link').click(function(){");
            writer.Write(@"window.open($(this).attr('data-href'),'_blank');");
            writer.Write(@"});");
            writer.Write(@"});");
            writer.Write(@"</script>");

            writer.Write(@"<a id=""about-link"" class=""status-link"" style=""margin-right:20px;"" data-href=""{0}"" href=""javascript:void(0)"">", url);
            writer.Write(@"<table><tr><td>");
            writer.Write(@"<div class=""icon icon-about""></div>");
            writer.Write(@"</td><td class=""text"">{0}</td></tr></table>", I18N.Admin.About);
            writer.Write(@"</a>");
        }
    }
}