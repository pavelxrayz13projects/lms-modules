﻿using Olimp.Configuration;
using System.Web.Mvc;
using Olimp.Domain.LearningCenter;
using Olimp.Web.Controllers;

namespace Olimp.Helpers
{
    public static class LmsCssHelper
    {
        public static void RenderLmsStyles(this HtmlHelper helper)
        {
            var config = Config.Get<OlimpConfiguration>();
            var lmsCss = config.LmsCss;

            if (!config.AppliesLmsCssToWholeSystem)
                return;

            if (!string.IsNullOrWhiteSpace(lmsCss))
                lmsCss = lmsCss.Replace("{olimp-path}", helper.ViewContext.RequestContext.HttpContext.Request.Url.GetRequestUrlBase());

            var writer = helper.ViewContext.Writer;

            writer.Write(@"<style type=""text/css"">");
            writer.Write(lmsCss);
            writer.Write(@"</style>");
        }

    }
}