﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<System.Web.Mvc.HandleErrorInfo>" MasterPageFile="~/Views/Shared/PopupWithoutToken.master" %>
<%@ Import Namespace="I18N=Olimp.I18N" %>

<asp:Content  ContentPlaceHolderID="Olimp_Js" runat="server">
    <% if (Model != null && Model.Exception is CookiesDisabledException && Request.HttpMethod.Equals("GET", StringComparison.OrdinalIgnoreCase)) { %>
        <script>$.cookie('WorkplaceToken') && window.location.reload()</script>	
    <% } %>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Body" runat="server">
	
	<%	
		string errorCode = "500";
		string message = I18N.Error.Error500Message;
		string instructions = null;		
        
		bool allowGoToActivation = false;
		bool showStackTrace = true;
	
		if (Model != null)
		{	
			var olimpException = Model.Exception as OlimpHttpException;
			var httpException = Model.Exception as HttpException;
			
			if (olimpException != null)
			{
				errorCode = olimpException.OlimpCode;
				message = olimpException.Message;
				instructions = olimpException.Instructions;
				showStackTrace = false;
			}
			else if (httpException != null)
			{
				errorCode = httpException.GetHttpCode().ToString();
				message = httpException.Message;							
			}			
			else
			{			
				if (Model.Exception != null)
					message = String.Format("{0}: {1}", message, Model.Exception.Message);
	
				allowGoToActivation = Model.Exception is Olimp.Core.Mvc.Security.NotActivatedException;
			}
            
            if (Model.Exception is CookiesDisabledException && Request.HttpMethod.Equals("POST", StringComparison.OrdinalIgnoreCase))
            {
                this.Writer.Write(@"<form id=""resubmit-form"" action=""{0}"" method=""post"">", Request.RawUrl);
                foreach(string p in Request.Form)
                    this.Writer.Write(@"<input type=""hidden"" name=""{0}"" value=""{1}""/>", p, Request.Form[p]);
                this.Writer.Write("</form>");
                this.Writer.Write("<script>$.cookie('WorkplaceToken') && $('#resubmit-form').submit()</script>");
            }
		}
	%>
	
	<center>
		<table id="error">
			<tbody>
				<tr>
					<td class="error-img">
						<img src="<%: Url.Content("~/Content/Images/attention.gif") %>" alt="<%: I18N.Error.ErrorTitle %> <%: errorCode %>" />
					</td>
					<td class="error-content">
						<h1>
							<span class="error-title"><%: I18N.Error.ErrorTitle %></span>
							<span class="error-code"><%: errorCode %></span>
						</h1>
						<h2><%: message %></h2>
						<% if (!String.IsNullOrWhiteSpace(instructions)) { %>
							<div style="width: 500px;"><%: instructions %></div>
						<% } %>
						<h3>Вы можете:</h3>					
						<ul class="error-options">
							<% if (allowGoToActivation) { %>
								<li>Перейти <a href="<%= Url.Action("Generate", "GenerateBinding", new { area = "Admin" }) %>">на страницу активации &gt;&gt;</a></li>
							<% } %>
							<li>Вернуться <a href="javascript:history.go(-1);">на предыдущую страницу &gt;&gt;</a></li>
							<li>Перейти <a href="/">на главную страницу &gt;&gt;</a></li>
							<% if (Model != null && Model.Exception != null && errorCode != "401" && errorCode != "403" && errorCode != "404" && !allowGoToActivation && showStackTrace) { %>
							<li>
								<div>Сообщить об ошибке разработчику</div>
								<div class="tip">для отправки сообщения необходимо подключение к Интернет</div>
								<form action="http://errors.olimpoks.ru" target="_blank">
									<textarea name="errorData" readonly="readonly"><%: Model.Exception %></textarea>
									<div class="buttons">
										<button type="submit">Сообщить об ошибке</button>
									</div>
								</form>
								<script type="text/javascript">
								$(function() { $('button').olimpbutton() })
								</script>
							</li>
							<% } %>
						</ul>
					</td>
				</tr>
			</tbody>
		</table>
	</center>
</asp:Content>