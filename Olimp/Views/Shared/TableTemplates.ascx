﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="I18N=Olimp.I18N" %>

<script id="tableHeadTemplate" type="text/html">
	<tr>
        <!-- ko if: $root.rowHighlighting -->
            <th class="highlight"></th>
        <!-- /ko -->

		<!-- ko if: actions().length -->
			<!-- ko if: $root.headerActions -->
				<th class="min-size" data-bind="foreach: $root.actions">
					<!-- ko ifnot: $data.templateId -->&nbsp;<!-- /ko -->
	
					<!-- ko if: $data.templateId -->
					<!-- ko template: { name: templateId, data: { _header:true }, templateEngine: $root.tbodyEngine } --> <!-- /ko -->
					<!-- /ko -->
				</th>
			<!-- /ko -->
	
			<!-- ko ifnot: $root.headerActions -->
				<th class="min-size">&nbsp;</th>
			<!-- /ko -->
		<!-- /ko -->
	
		<!-- ko if: $root.checkboxes -->
			<th class="min-size checkbox"><input type="checkbox" data-bind="checked: allChecked" /></th>
		<!-- /ko  -->
	
		<!-- ko foreach: columns -->
		<th data-bind="style: style">
			<a data-bind="text: caption, attr: _attrs, click: $root._sort"></a>
	
			<!-- ko if: $root.sorting -->
			<span data-bind="attr: { 'class': sortClass }"></span>
			<!-- /ko -->
		</th>
		<!-- /ko -->
	</tr>	
</script>

<script id="defaultRowLockedTemplate" type="text/html">
    <a class="icon icon-lock-small-active action" title="<%= I18N.Shared.Locked %>"></a>
</script>

<script id="tableBodyTemplate" type="text/html">		
	<!-- ko foreach: rows -->
	<tr data-bind="attr: { 'class': _className }, click: $root._rowHighlight.bind($data)">
        <!-- ko if: $root.rowHighlighting -->
            <td class="highlight"></td>
        <!-- /ko -->
		<!-- ko if: $root.actions().length -->
			<!-- ko ifnot: _isLocked -->
			<td class="actions" data-bind="foreach: $root.actions">
				<!-- ko ifnot: $data.templateId -->
				<a data-bind="attr: attrs, click: $root._rowAction.bind($data, $parent)"></a>
				<!-- /ko -->
	
				<!-- ko if: $data.templateId -->
				<!-- ko template: { name: templateId, data: $parent } --> <!-- /ko -->
				<!-- /ko -->
			</td>
			<!-- /ko -->
	
			<!-- ko if: _isLocked -->
			<td class="actions" data-bind="template: { name: $root.rowLockedTemplate, data: $data}"></td>
			<!-- /ko -->
		<!-- /ko -->
	
		<!-- ko if: $root.checkboxes -->
			<td class="checkbox">
				<input type="checkbox" data-bind="checked: _checked" />
			</td>
		<!-- /ko  -->
	
		<!-- ko foreach: $root.columns -->
			<!-- ko if: $data.templateId -->
				<td data-bind="template: { name: templateId, data: $parent}"></td>
			<!-- /ko -->
	
			<!-- ko ifnot: $data.templateId -->					
				<td data-bind="html: $parent[$data.name]"></td>
			<!-- /ko -->
		<!-- /ko -->
	</tr>
	<!-- /ko -->	
</script>	

<script id="tableGroupsTemplate" type="text/html">		
	<!-- ko foreach: groups -->
		<tr class="odd">
			<td class="grouping" data-bind="attr: { colSpan: $root.columnsCount }, text: name"></td>		
		</tr>
		<!-- ko template: { name: 'tableBodyTemplate', data: $data } -->	
		<!-- /ko -->	
	<!-- /ko -->
</script>		

<script id="tablePagingTemplate" type="text/html">	
    <!-- ko if: checkboxes -->
        <div class="selected-items-count">
            <span>выбрано:&nbsp;</span>
            <span data-bind="text: selectedItemsCount"></span>
        </div>

        <!-- ko foreach: selectedIds -->
            <input type="hidden" data-bind="attr: { name: $root.checkboxesName, value: $data }" />
        <!-- /ko -->

    <!-- /ko -->
	<div class="pages">
		<a href="#" class="icon icon-left" data-bind="click: paging.leftPage"></a>
		<div class="links">
			<!-- ko foreach: paging.pages -->				
				<!-- ko if: isSeparator -->
				<span class="separator">...</span>
				<!-- /ko -->
	
				<!-- ko ifnot: isSeparator -->
				<a href="#" data-bind="text: number, attr: { 'class': selectedClass }, click: $root.paging.select"></a>
				<!-- /ko -->	
			<!-- /ko -->
		</div>
		<a href="#" class="icon icon-right" data-bind="click: paging.rightPage"></a>	
	</div>
	<div class="total">
		<span>на странице:</span>
		<select class="paging-size-select" 
                data-bind="
			optionsText: function(item) { return item === 99999 ? '<%= I18N.Shared.TableShowAll %>' : item },
			options: paging.rowAmounts, 
			value: paging.size">				
		</select>
	</div>
	<div class="status">
		<span>результаты</span>
		<span data-bind="text: paging.rowsSpan"></span><span>&nbsp;из</span>
		<span data-bind="text: rowsCount"></span>
	</div>	
</script>	

<script id="tableButtonsTemplate" type="text/html">		
	<!-- ko if: adding -->
	<button type="button" data-bind="text: adding.text, click: adding.action, disable: adding.disabled"></button>			
	<!-- /ko -->
		
	<!-- ko foreach: tableActions -->
	<button type="button" data-bind="text: text, style: style, click: $root._tableAction.bind($data), disable: disabled"></button>			
	<!-- /ko -->	
</script>	

<script id="tableTemplate" type="text/html">
	<div class="body">
		<table cellpadding="0" cellspacing="0">
			<thead data-bind="template: { name: 'tableHeadTemplate', data: $data }"></thead>
			<tbody data-bind="template: { name: $root.tbodyTemplate, data: $data, templateEngine: $root.tbodyEngine }"></tbody>
		</table>
	</div>
	
	<!-- ko if: paging -->
	<div class="paging" data-bind="template: { name: 'tablePagingTemplate', data: $data, afterRender: paging.reset }"></div>
	<!-- /ko -->
	
	<!-- ko if: buttons -->
	<div class="buttons" data-bind="template: { name: 'tableButtonsTemplate', data: $data }"></div>
	<!-- /ko -->
	
</script>	

