﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>


<script id="defaultMaterialNameTemplate_doTEngine" type="text/html">
	<span>{{= it.$data.code() }}&nbsp;{{= it.$data.name() }}</span>
</script>	
		
<script id="materialTemplate_doTEngine" type="text/html">	
	{{~ it.$data.materials() :material:mi }}
		{{ var materialContext = it.createChildContext(material); }}
		<li class="-has-dot-binding" data-dot-context="{{= materialContext.$$contextId }}" data-dot-bind="attr: { 'class': listItemClass }">
			<div class="gap">&nbsp;</div>
			<div class="node-caption">
				{{? it.$root.showCheckBoxes }}			
				<input type="checkbox" class="checker -has-dot-binding" name="{{= it.$root.checkBoxesName }}" value="{{= material[it.$root.checkBoxesValue] }}" data-dot-context="{{= materialContext.$$contextId }}" data-dot-bind="checked: selected" />
				{{?}}
			
				<div class="{{= material.iconClass() }}"></div>
	
				{{? !it.$root.materialTemplate }}
					<div class="material-name">{{= material.caption }}</div>
				{{??}}
					<div class="material-name">
						{{= materialContext.renderTemplate(it.$root.materialTemplate, material) }}
					</div>
				{{?}}
			</div>
		</li>	
	{{~}}
</script>	

<script id="nodeTemplate_doTEngine" type="text/html">	
	{{~ it.$data.nodes() :node:ni }}	
		{{ var nodeContext = it.createChildContext(node); }}
		<li class="{{= ko.utils.unwrapObservable(node.listItemClass) }}">			
			<div class="node-caption">
				{{? node.available }}
				<div class="-has-dot-binding" data-dot-context="{{= nodeContext.$$contextId }}" data-dot-bind="attr: { 'class': expandeeClass }, click: $root.toggleExpand"></div>
				{{?}}
	
				<div class="gap">&nbsp;</div>				
				{{? node.available }}
					{{? it.$root.showCheckBoxes }}									
					<input type="checkbox" class="checker -has-dot-binding" data-dot-context="{{= nodeContext.$$contextId }}" data-dot-bind="checked: checked" />
					{{?}}
	
					<div class="-has-dot-binding" data-dot-context="{{= nodeContext.$$contextId }}" data-dot-bind="attr: { 'class': nodeNameClass }">
						<span class="-has-dot-binding" data-dot-context="{{= nodeContext.$$contextId }}" data-dot-bind="click: $root.toggleExpand">{{= ko.utils.unwrapObservable(node.name) }}</span>
					</div>
				{{??}}			
					<div class="-has-dot-binding" data-dot-context="{{= nodeContext.$$contextId }}" data-dot-bind="attr: { 'class': nodeNameClass }">
						<span>{{= ko.utils.unwrapObservable(node.name) }}</span>
					</div>
				{{?}}
			</div>
		
			<div style="clear:both;"></div>
	
			{{? !node.dynamicContent }}		
				<ul class="{{= node.subListClass() }} -has-dot-binding" data-dot-context="{{= nodeContext.$$contextId }}" data-dot-bind="visible: expanded">						
					{{= nodeContext.renderTemplate(node.template(), node) }}				
				</ul>
			{{??}}					
				<div class="-has-dot-binding" data-dot-context="{{= nodeContext.$$contextId }}" data-dot-bind="if: dynamicData">	
					<div data-bind="visible: expanded">		
						<div data-bind="template: { name: $root.dynamicContentTemplate, data: dynamicData }"></div>
					</div>	
				</div>
			{{?}}				
		</li>
	{{~}}
</script>	