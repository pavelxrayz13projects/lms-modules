﻿<%@ Control Language="C#" Inherits="IntegratableUserControl<object>" %>

<% if (IntegrationContext.IntegrationState == IntegrationContextState.InIntegrationContext) { %>
    <a class="logo" >
        <img src="<%= Url.Content("~/Content/Images/logo.gif") %>" alt="<%= Olimp.I18N.Shared.Title %>" />
    </a>
<% } else { %>
    <a href="<%= Url.RouteUrl("Default") %>" class="logo" >
	    <img src="<%= Url.Content("~/Content/Images/logo.gif") %>" alt="<%= Olimp.I18N.Shared.Title %>" />
    </a>
<% } %>