﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<Boolean>" %>

<% if (Model) { %>
	<center class="ui-state-default" style="border: 0">
		<div class="ui-icon ui-icon-circle-check"></div>
	</center>
<% } else {%>
	<center class="ui-state-error" style="border: 0">
		<div class="ui-icon ui-icon-circle-close"></div>
	</center>
<% } %>