﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<System.Web.Mvc.HandleErrorInfo>" MasterPageFile="~/Views/Shared/PopupWithoutToken.master" %>
<%@ Import Namespace="I18N=Olimp.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Body" runat="server">
	
	<center>
		<table id="error">
			<tbody>
				<tr>
					<td class="error-img">
						<img src="<%: Url.Content("~/Content/Images/error.gif") %>" alt="<%: I18N.Error.ErrorTitle %> 404" />
					</td>
					<td class="error-content">
						<h1>
							<span class="error-title"><%: I18N.Error.ErrorTitle %></span>
							<span class="error-code">404</span>
						</h1>
						<h2><%: I18N.Error.Error404Message %></h2>
						<h3>Вы можете:</h3>					
						<ul class="error-options">
							<li>Вернуться <a href="javascript:history.go(-1);">на предыдущую страницу &gt;&gt;</a></li>
							<li>Перейти <a href="/">на главную страницу &gt;&gt;</a></li>							
						</ul>
					</td>
				</tr>
			</tbody>
		</table>
	</center>

</asp:Content>
