﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script id="tableGroupsTemplate_doTEngine" type="text/html">		
	{{~ it.$data.groups() :group:gi }}
		{{ var groupContext = it.createChildContext(group); }}
		<tr class="odd">
			<td class="grouping -has-dot-binding" data-dot-context="{{= groupContext.$$contextId }}" data-dot-bind="attr: { colSpan: $root.columnsCount }">{{= ko.utils.unwrapObservable(group.name) }}</td>		
		</tr>
	
		{{= groupContext.renderTemplate('tableBodyTemplate_doTEngine', group) }}	
	{{~}}
</script>

<script id="tableBodyTemplate_doTEngine" type="text/html">
	{{~ it.$data.rows() :row:ri }}		
		{{ var rowContext = it.createChildContext(row); }}
		<tr class="-has-dot-binding" data-dot-context="{{= rowContext.$$contextId }}" data-dot-bind="click: $root._rowHighlight.bind($data), attr: { 'class': _className }">
            {{? it.$root.rowHighlighting }}
                <td class="highlight"></td>
            {{?}}
			{{? it.$root.actions().length }}			
				{{? !row._isLocked() }}			
				<td class="actions">				
					{{~ it.$root.actions() :action:ai }}
						{{ var actionContext = rowContext.createChildContext(action); }}
						{{? !action.templateId }}				
							<a class="-has-dot-binding" data-dot-context="{{= actionContext.$$contextId }}" data-dot-bind="attr: attrs, click: $root._rowAction.bind($data, $parent)"></a>
						{{??}}		
							{{= actionContext.renderTemplate(action.templateId, row) }}										
						{{?}}
					{{~}}
				</td>
				{{??}}
				<td class="actions">
					{{= rowContext.renderTemplate(it.$root.rowLockedTemplate, row) }}
				</td>
				{{?}}
			{{?}}
	
			{{? it.$root.checkboxes }}
				<td class="checkbox">
					<input class="-has-dot-binding" data-dot-context="{{= rowContext.$$contextId }}" type="checkbox" data-dot-bind="checked: _checked" />
				</td>
			{{?}}
		
		{{~ it.$root.columns() :col:ci }}			
			{{? col.templateId }}
				{{ var colContext = rowContext.createChildContext(col); }}
				<td>{{= colContext.renderTemplate(col.templateId, row) }}</td>				
			{{??}}					
				<td>{{= row[col.name]() }}</td>
			{{?}}
		{{~}}
	</tr>
	{{~}}	
</script>	