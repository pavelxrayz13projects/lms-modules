﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>


<script id="defaultMaterialNameTemplate" type="text/html">
	<span data-bind="text: code"></span>&nbsp;<span data-bind="text: name"></span>
</script>	
		
<script id="materialTemplate" type="text/html">
	<li data-bind="attr: { 'class': listItemClass }">
		<div class="gap">&nbsp;</div>
		<div class="node-caption">
			<!-- ko if: $root.showCheckBoxes -->
			<input type="checkbox" class="checker" data-bind="attr: { name: $root.checkBoxesName, value: $data[$root.checkBoxesValue] }, checked: selected" />
			<!-- /ko -->
			<div data-bind="attr: { 'class': iconClass }"></div>
	
			<!-- ko ifnot: $root.materialTemplate -->
			<div data-bind="html: caption" class="material-name"></div>
			<!-- /ko -->
	
			<!-- ko if: $root.materialTemplate -->
			<div data-bind="template: { name: $root.materialTemplate, data: $data }" class="material-name"></div>
			<!-- /ko -->
		</div>
	</li>	
</script>	

<script id="nodeTemplate" type="text/html">
	<li data-bind="attr: { 'class': listItemClass }">			
		<div class="node-caption">
			<!-- ko if: available -->
			<div data-bind="attr: { 'class': expandeeClass }, click: $root.toggleExpand"></div>
			<!-- /ko -->
			<div class="gap">&nbsp;</div>
	
			<!-- ko if: available -->
				<!-- ko if: $root.showCheckBoxes -->
				<input type="checkbox" class="checker" data-bind="checked: checked" />
				<!-- /ko -->
				<div data-bind="attr: { 'class': nodeNameClass }">
					<span data-bind="text: name, click: $root.toggleExpand"></span>
				</div>
			<!-- /ko -->
	
			<!-- ko ifnot: available -->
			<div data-bind="attr: { 'class': nodeNameClass }">
				<span data-bind="text: name"></span>
			</div>
			<!-- /ko -->
		</div>
		
		<div style="clear:both;"></div>
	
		<!-- ko ifnot: dynamicContent -->
		<ul data-bind="template: { name: template, foreach: templateData }, attr: { 'class': subListClass }, visible: expanded">			
		</ul>
		<!-- /ko -->
	
		<!-- ko if: dynamicContent -->
		<!-- ko if: dynamicData -->
		<div data-bind="visible: expanded">
			<div data-bind="template: { name: $root.dynamicContentTemplate, data: dynamicData }">			
			</div>
		</div>
		<!-- /ko -->
		<!-- /ko -->
	</li>
</script>	

