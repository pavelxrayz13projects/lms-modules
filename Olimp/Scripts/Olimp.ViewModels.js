if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement /*, fromIndex */) {
        "use strict";

        if (this == null) throw new TypeError();

        var t = Object(this),
            len = t.length >>> 0;

        if (len === 0) return -1;

        var n = 0;
        if (arguments.length > 0) {
            n = Number(arguments[1]);
            if (n != n)
                n = 0;
            else if (n != 0 && n != Infinity && n != -Infinity)
                n = (n > 0 || -1) * Math.floor(Math.abs(n));
        }

        if (n >= len) return -1;

        var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
        for (; k < len; k++)
            if (k in t && t[k] === searchElement) return k;

        return -1;
    }
}

//***********************
//****** Tree view ******
//***********************
(function (__self) {
    function isInitialExpanded(options, collection, lambda) {
        if (options.initialExpanded)
            return ko.observable(true);

        if (options.checkboxes) {
            for (var i = 0; i < collection.length; i++) {
                if (lambda(collection[i]))
                    return ko.observable(true);
            }

            return ko.observable(false);
        }

        return ko.observable(false);
    }

    var treeMapping = { nodes: {}, materials: {} };

    function createMapping(collectionName, data, options, type) {
        var mapping = {},
            self = this;

        var last = null,
            collection = data[collectionName];

        if (collection.length)
            last = collection[collection.length - 1];

        mapping[collectionName] = {
            create: function (o) {
                o.data.listItemClass = o.data == last ? 'last' : '';

                return new type(o.data, options, self);
            }
        };

        return mapping;
    }

    function getExpandeeClass() {
        return this.expanded() ? 'expandee icon icon-minus' : 'expandee icon icon-plus';
    }

    function extendListContainer(self, data, options) {
        self.dynamicContent = !!data.dynamicContent;

        self.hasMaterials = ko.observable(!!data.materials);

        if (!self.dynamicContent) {
            self.template = function () { return (self.hasMaterials() ? 'materialTemplate' : 'nodeTemplate') + (options.templateEngine ? ('_' + options.templateEngine) : ''); };
            self.templateData = ko.computed(function () { return this.hasMaterials() ? this.materials() : this.nodes() }, self);
            self.subListClass = ko.computed(function () { return this.hasMaterials() ? 'materials' : 'nodes' }, self);
        }
        self.expandeeClass = ko.computed(getExpandeeClass, self);

        return self;
    }

    function all(collection, lambda) {
        for (var i = 0; i < collection.length; i++) { if (!lambda(collection[i])) return false }
        return true;
    }

    function loadDynamicContent(rootModel, data) {
        var self = this,
            params = $.extend(rootModel.dynamicContentUrlParams(), { id: data.id });

        $.post(rootModel.dynamicContentUrl, params, function(json) {
            var templateModel = { templateData: json };

            if (rootModel.showCheckBoxes)
                templateModel.nodeChecked = self.checked;

            self.dynamicData(templateModel);
        }, 'json');
    }

    $.extend(__self, {
        Material: function (data, options, rootModel) {
            $.extend(this, data, {
                fullName: data.code + ' ' + data.name,
                selected: ko.observable(false)
            });

            if (options.activatable) {
                this.activated = ko.computed(function() {
                    return rootModel.activeMaterial() == this;
                }, this);
            }

            if (rootModel.selected.indexOf(data[options.checkboxesValue]) >= 0)
                this.selected(true);

            this.locked = ko.computed(function () { return this.passed.indexOf(data[options.passedComparisonField || 'id']) >= 0 || data.locked; }, rootModel);
            this.listItemClass = ko.computed(function() {
                var cls = this.locked() ? data.listItemClass + ' locked' : data.listItemClass;
                if (options.activatable && this.activated())
                    cls += ' activated';
                return cls;
            }, this);

            if (data.iconClass) {
                this.iconClass = data.iconClass;
            } else {
                this.iconClass = ko.computed(function () {
                    if (this.locked()) return 'icon icon-lock-big';
                    if (this.source == 'Commercial') return 'icon icon-circle';
                    if (this.source == 'CreatedByUser') return 'icon icon-people';
                    if (this.source == 'GeneratedByUser' || this.source == 'GeneratedFromNormatives') return 'icon icon-generated';
                }, this);
            }

            this.caption = this.fullName;

            if (!options.materialTemplate && !this.locked() && options.materialsLink) {
                if ($.isFunction(options.materialsLink)) {
                    this.caption = options.materialsLink(data);
                } else if (options.materialsLink.replace) {
                    this.caption = $('<a />')
                        .attr('href', options.materialsLink.replace(/{\s*id\s*}/gi, data.id))
                        .text(this.fullName)
                        .wrap('<div />')
                        .parent()[0]
                        .innerHTML;
                }
            }
        },
        Dynamic: function (data, options, rootModel) {
            var self = $.extend(this, data);

            $.extend(extendListContainer(this, data, options), {
                dynamicData: ko.observable(null),
                checked: ko.observable(false),
                expanded: ko.observable(false),
                listItemClass: data.listItemClass
            })

            this.available = !!data.hasDynamicContent;
            if (!this.available)
                this.listItemClass += ' unavailable';

            rootModel.dynamicContentUrlParams.subscribe(function () {
                if (this.dynamicData() && this.expanded())
                    loadDynamicContent.apply(self, [rootModel, data]);
                else if (!this.expanded())
                    this.dynamicData(null);
            }, this)

            ko.computed(function () {
                if (this.expanded() && !this.dynamicData()) {
                    loadDynamicContent.apply(self, [rootModel, data]);
                }
            }, this);
        },
        Node: function (data, options, rootModel) {

            this.available = true;

            if (data.materials) {
                var materialsMapping = createMapping.apply(rootModel,
                    ['materials', data, options, __self.Material]);

                ko.mapping.fromJS(data, materialsMapping, extendListContainer(this, data, options));

                this.checked = ko.computed({
                    read: function () {
                        return all(this.materials(), function (m) {
                            return m.selected();
                        });
                    },
                    write: function (value) {
                        $.each(this.materials(), function (i, o) {
                            o.selected(value);
                            $('.checker[value="' + o.guid + '"]').trigger('material-checkbox-changed', o);
                        });
                    },
                    owner: this
                });

                this.expanded = isInitialExpanded(
                    options, this.materials(), function (m) { return m.selected() });
            } else if (data.dynamicContent) {
                __self.Dynamic.apply(this, [data, options, rootModel]);
            } else {
                __self.Root.apply(this, [data, options, rootModel]);
            }

            if (options.highlighting) {
                this.highlighted = ko.computed(function () {
                    var highlighted = rootModel.highlight(),
                        nodes = this.nodes ? this.nodes() : [];

                    if (highlighted.indexOf(data.id) >= 0)
                        return true;

                    for (var i = 0; i < nodes.length; i++) {
                        if (nodes[i].highlighted())
                            return true;
                    }

                    return false;
                }, this);

                this.nodeNameClass = ko.computed(function () {
                    return this.highlighted() ? 'node-name highlighted' : 'node-name';
                }, this);
            } else {
                this.nodeNameClass = 'node-name';
            }
        },
        Root: function (data, options, rootModel) {
            var nodesMapping = createMapping.apply(rootModel, ['nodes', data, options, __self.Node]);

            ko.mapping.fromJS(data, nodesMapping, extendListContainer(this, data, options));

            this.checked = ko.computed({
                read: function () {
                    return all(this.nodes(), function(a) {
                        return a.checked();
                    });
                },
                write: function (value) {
                    $.each(this.nodes(), function(i, o) {
                        o.checked(value);
                    });
                },
                owner: this
            });

            this.expanded = isInitialExpanded(
                options, this.nodes(), function (a) { return a.expanded() });
        },
        MaterialsTree: function (element, data, options) {
            var self = this;
            options = $.extend({ id: '', initialExpanded: true, checkboxes: true, checkboxesName: null, checkboxesValue: 'id', materialsLink: null, extendViewModel: self }, options || {})

            self = options.extendViewModel;

            self.passed = ko.observableArray(options.passed || []);
            self.selected = ko.observableArray(options.selected || []);

            data = data || { nodes: [] }

            if (options.dynamicContentUrl) {
                this.dynamicContentUrlParams = ko.observable(options.dynamicContentUrlParams || {});
            }

            var rootMapping = createMapping.apply(self, ['nodes', data, options, __self.Node]);

            if (options.activatable) {
                var _activeMaterial = ko.observable(null);

                this.activeMaterial = ko.computed({
                    read: function () { return _activeMaterial(); },
                    write: function (value) {
                        if (_activeMaterial() !== value) {
                            _activeMaterial(value);
                            element.trigger('materialActivated', { material: value });
                        }
                    }
                });
            }

            if (options.highlighting)
                this.highlight = ko.observableArray([]);

            ko.mapping.fromJS(data, rootMapping, self);

            if ((!data.nodes || !data.nodes.length) && options.emptyMessage)
                $('#' + options.id).after($('<span></span>').text(options.emptyMessage))

            $.extend(self, {
                templateEngine: (ko[options.templateEngine] || ko.nativeTemplateEngine).instance,
                materialTemplate: options.materialTemplate,
                activatable: options.activatable,
                highlighting: options.highlighting,
                showCheckBoxes: options.checkboxes,
                checkBoxesName: options.checkboxesName,
                checkBoxesValue: options.checkboxesValue,
                dynamicContentUrl: options.dynamicContentUrl,
                dynamicContentTemplate: options.dynamicContentTemplate,
                toggleExpand: function (node) {
                    node.expanded(!node.expanded());
                },
                getMinimalCheckedSubtree: function (ignoreRoot, nodes, result) {
                    result = result || [];
                    ko.utils.arrayForEach(nodes || self.nodes(), function(n) {
                        if (n.checked() && (nodes || !ignoreRoot))
                            result.push(n);
                        else if (n.nodes)
                            self.getMinimalCheckedSubtree(ignoreRoot, n.nodes(), result);
                    });

                    return result;
                },
                getSelectedMaterials: function () {
                    var checkboxes = $('#' + options.id + ' .materials :checked');
                    return $.map(checkboxes, function (c) { return ko.dataFor(c) })
                },
                expandAll: function (expanded, root) {
                    root = root || self;

                    if (root.nodes) {
                        ko.utils.arrayForEach(root.nodes(), function(n) {
                            if (n.hasDynamicContent !== false) {
                                n.expanded(expanded);
                                self.expandAll(expanded, n);
                            }
                        });
                    }
                },
                checkAll: function (checked, root) {
                    var elements = null;

                    root = root || self;

                    if (root.nodes)
                        elements = root.nodes();
                    else if (root.materials)
                        elements = root.materials();

                    if (elements)
                        ko.utils.arrayForEach(elements, function (n) { self.checkAll(checked, n) })
                    else if (root.checked)
                        root.checked(checked);
                },
                updateTree: function (newData) { ko.mapping.fromJS(newData, rootMapping, self) }
            });
        }
    });
})(window);

//***********************
//******** Table ********
//***********************
/* Sample json:
 *
 * columns = [{ 
 *		name: 'columnName', 
 * 		caption:'text'		
 * 	}]
 * rows = [{ 'columnName1': value1, 'columnName2': value2 }, 
 *			{ 'columnName1': value1, 'columnName2': value2 }]
 * 
 * options = { 
 *		actions: [
 * 			{ icon: 'name-without-prefix', action: function, attrs: { 'attr': 'value' }, confirm: 'text' }
 *		],
 *		paramsMapping: { pageSize: '', currentPage: '', sortColumn: '', sortAsc: '' }
 *	}
 */

(function(__self) {

    function subscribeObservables(source, target, dontSetInitially) {
        var callback = function(value) { target() !== value && target(value) };

        if (!dontSetInitially)
            callback(source());

        source._subscribtionToDispose = source.subscribe(callback);
    }

    function syncObservables(primary, secondary) {
        subscribeObservables(primary, secondary);
        subscribeObservables(secondary, primary, true);
    }

    function createDisposeMethod(disposables) {
        return function() {
            if (!disposables || !disposables.length)
                return;

            for (var i = 0; i < disposables.length; i++) {
                var item = disposables[i];

                if (item && $.isFunction(item.dispose))
                    item.dispose();
            }
        };
    }

    function disposeAll(array, disposeMethod) {
        if (!array || !array.length)
            return;

        disposeMethod = disposeMethod || '$dispose';

        for (var i = 0; i < array.length; i++) {
            var item = array[i];
            if (item && $.isFunction(item[disposeMethod]))
                item[disposeMethod]();
        }

    }

    function PagesGenerator(leftNumbers, middleNumbers, rightNumbers) {
        $.extend(this, {
            leftNumbers: leftNumbers,
            middleNumbers: middleNumbers,
            rightNumbers: rightNumbers,
            totalNumbers: leftNumbers + middleNumbers + rightNumbers
        });

        this.getNumbers = function(pagesCount, currentPage, createPage, createSeparator) {
            var numbers = [];

            if (pagesCount <= this.totalNumbers) {
                for (var i = 0; i < Math.max(pagesCount, 1); i++)
                    numbers.push(createPage(i + 1))
                return numbers;
            }

            for (var i = 0; i < this.leftNumbers; i++)
                numbers.push(createPage(i + 1));

            var start, end;
            if (currentPage <= this.middleNumbers) {
                start = 1, end = this.middleNumbers;
            } else if (currentPage >= pagesCount - this.rightNumbers) {
                start = pagesCount - this.middleNumbers - 1, end = pagesCount - this.rightNumbers - 1;
            } else {
                var center = (this.middleNumbers - 1) / 2,
                    left = Math.floor(center),
                    right = Math.ceil(center)
                start = currentPage - left - 1, end = currentPage + right - 1;
            }

            if (start + 1 - numbers[numbers.length - 1].number > 1)
                numbers.push(createSeparator());

            for (var i = start; i <= end; i++)
                numbers.push(createPage(i + 1));

            if (pagesCount - this.rightNumbers + 1 - numbers[numbers.length - 1].number > 1)
                numbers.push(createSeparator());

            for (var i = pagesCount - this.rightNumbers; i < pagesCount; i++)
                numbers.push(createPage(i + 1));

            return numbers;
        };
    }

    function onTableReload(self, rowsData, callbacks) {
        self.setRows(rowsData);

        for (var i = 0; i < callbacks.length; i++) {
            var callback = callbacks[i];

            if ($.isFunction(callback))
                callback.apply(self);
        }
    }

    function applyPagingAndSortingToRows(options, data) {
        if (options.sorting) {
            var sortColumn = data[options.paramsMapping.sortColumn],
                sortAsc = data[options.paramsMapping.sortAsc],
                rows = options.tableData.rows.slice(0);

            rows.sort(function(x, y) {
                var first = x[sortColumn],
                    second = y[sortColumn],
                    result = 0;

                if (typeof first === "string" && typeof second === "string")
                    result = first.localeCompare(second)
                else if (typeof first === "number" && typeof second === "number")
                    result = first - second;
                else if (typeof first === "boolean" && typeof second === "boolean")
                    result = first === second ? 0 : (first ? -1 : 1);

                if (!sortAsc)
                    result *= -1;

                return result;
            });
        }

        if (options.paging) {
            var pageSize = data[options.paramsMapping.pageSize],
                currentPage = data[options.paramsMapping.currentPage],
                skip = (currentPage - 1) * pageSize;

            rows = rows.slice(skip, Math.min(skip + pageSize, rows.length));
        }

        return { rows: rows, rowsCount: options.tableData.rowsCount };
    }

    var pagesGenerator = new PagesGenerator(1, 3, 1);

    $.extend(__self, {
        Adding: function(model, options) {
            var action = null, self = this, loadUrl = options.adding.loadModel;

            this.disabled = options.adding.disabled;
            this.text = options.adding.text;

            if (!options.disabled) {
                if (!options.adding.action) {
                    for (var i = 0; i < options.actions.length; i++) {
                        if (options.actions[i].constructor == __self.EditAction) {
                            action = options.actions[i];
                            break;
                        }
                    }
                } else {
                    action = options.adding.action;
                }

                if (!loadUrl && options.adding.newRow) {
                    this.newRow = options.adding.newRow;
                    this.newRow.notifyRowSaved = function(e) { model.reload() }
                }

                this.action = function() {
                    if (loadUrl) {
                        $.post(loadUrl, function(json) {
                            json.notifyRowSaved = function(e) { model.reload() }

                            action.action.call(self, {
                                model: model,
                                data: ko.mapping.fromJS(json),
                                action: action,
                                _dontLoad: true,
                                isNew: true
                            })
                        }, 'json');
                    } else {
                        action.action.call(self, {
                            model: model,
                            data: ko.mapping.fromJS(self.newRow || {}),
                            action: action,
                            _dontLoad: !loadUrl && self.newRow,
                            isNew: true
                        });
                    }
                }
            } else {
                this.action = function() {}
            }
        },
        RowAction: function(icon, action, options) {
            var self = this;

            options = options || {};

            action = action || function() { return self.attrs.href != '#' };
            if (options.confirm) {
                var realAction = action;
                action = function(o) { return confirm(options.confirm) && realAction(o); }
            }

            $.extend(this, options, { icon: icon, action: action })
        },
        DeleteAction: function(deleteUrl, confirm, getData, options, icon) {
            options = options || {};

            if (!options.disabled)
                options.confirm = confirm;

            var action = !options.disabled
                ? function(o) {
                    getData = getData || function(row) { return { id: ko.utils.unwrapObservable(row[o.model.rowId]) }; };
                    var requestData = getData(o.data);
                    $.post(deleteUrl, requestData, function() {
                        if (o.model.selectedIds)
                            o.model.selectedIds.remove(requestData.id);
                        o.model.reload();
                        options.onDeleted && options.onDeleted({ isSingle: true });
                    });
                }
                : function() {};

            __self.RowAction.apply(this, [icon || 'delete-active', action, options]);
        },
        EditAction: function(dialogId, options, icon) {
            if (!options.disabled) {
                var dialog = $('#' + dialogId).wrapInner('<div />'),
                    afterApplyAttr = dialog.attr("data-afterapply"),
                    template = dialog.html(),
                    form = dialog.find('form:first'),
                    url = form.attr('action'),
                    dialogOptions = $.extend(
                    { autoOpen: false, draggable: false, resizable: false, modal: true }, options.dialogOptions || {}),
                    dialogButtons = options.dialogButtons || { saveText: 'Save', cancelText: 'Cancel' },
                    action = function(o) {
                        var rawObject = ko.mapping.toJS(o.data),
                            itemModel = ko.mapping.fromJS(rawObject);

                        dialog.data('isNew', !!o.isNew)
                            .data('invocationParams', o.invocationParams);

                        if (options.loadModel && !o._dontLoad) {
                            var data = {},
                                id = o.model.rowId;

                            data[id] = o.data[id];

                            $.post(options.loadModel, data, function(json) {
                                itemModel = ko.mapping.fromJS(json);

                                ko.cleanNode(dialog.children(0)[0]);
                                dialog.html(template).find('form:first').submit(function() {
                                    dialogOptions.buttons[dialogButtons.saveText]();
                                    return false;
                                });

                                $.validator.unobtrusive.parse(dialog[0])
                                ko.applyBindings(itemModel, dialog.children(0)[0]);

                                dialog.data('sourceModel', o.data).data('itemModel', itemModel)
                                    .trigger('afterApply')
                                    .olimpdialog('open');
                            }, 'json');
                        } else {
                            ko.cleanNode(dialog.children(0)[0]);
                            dialog.html(template).find('form:first').submit(function() {
                                dialogOptions.buttons[dialogButtons.saveText]();
                                return false;
                            });

                            $.validator.unobtrusive.parse(dialog[0])
                            ko.applyBindings(itemModel, dialog.children(0)[0]);

                            dialog.data('sourceModel', o.data).data('itemModel', itemModel)
                                .trigger('afterApply')
                                .olimpdialog('open');
                        }
                    },
                    loadModel = options.loadModel;

                if (afterApplyAttr)
                    eval('dialog.bind("afterApply", ' + afterApplyAttr + ')');

                dialogOptions.beforeClose = function(ev) {
                    return dialog.data('in-progress') !== true;
                }
                dialogOptions.close = function() {
                    dialog.data('in-progress', false)
                        .parent().find('.olimp-dialog-buttonpane button')
                        .olimpbutton('option', 'disabled', false);
                };

                //options.saveTransport({ url: url, data: form.serialize(), dialog: dialog }, function(json)

                dialogOptions.buttons = {};
                dialogOptions.buttons[dialogButtons.saveText] = function() {
                    var form = dialog.find('form:first'),
                        isValid = form.valid(),
                        allowSubmit = dialog.triggerHandler('beforeSubmit') !== false
                    if (!isValid || !allowSubmit)
                        return;

                    dialog.data('in-progress', true)
                        .parent().find('.olimp-dialog-buttonpane button')
                        .olimpbutton('option', 'disabled', true);

                    var invocationParams = dialog.data('invocationParams');
                    var invokeSave = invocationParams && invocationParams.saveTransport && $.isFunction(invocationParams.saveTransport)
                        ? invocationParams.saveTransport
                        : function(ctx, onSuccess, onError) {
                            $.post(ctx.url, ctx.params, onSuccess, 'json').error(onError);
                        }

                    // http://stackoverflow.com/questions/1184624/convert-form-data-to-js-object-with-jquery
                    var o = {}, formArray = form.serializeArray();
                    $.each(formArray, function() {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });

                    invokeSave({ url: url, data: o, params: $.param(formArray) }, function(json) {
                        var itemModel = json || ko.mapping.toJS(dialog.data('itemModel')),
                            sourceModel = dialog.data('sourceModel');

                        if (sourceModel.notifyRowSaved)
                            sourceModel.notifyRowSaved(itemModel);

                        var oldValue = ko.mapping.toJS(sourceModel),
                            newValue = $.extend({}, itemModel);

                        ko.mapping.fromJS(itemModel, {}, sourceModel);

                        dialog.data('in-progress', false).olimpdialog('close');
                        dialog.trigger('afterSubmit', [{ isNew: dialog.data('isNew'), oldValue: oldValue, newValue: newValue }])
                    }, dialogOptions.close);
                };
                dialogOptions.buttons[dialogButtons.cancelText] = function() { dialog.olimpdialog('close') };

                form.submit(function() {
                    dialogOptions.buttons[dialogButtons.saveText]();
                    return false;
                });

                dialog.olimpdialog(dialogOptions);
            }

            __self.RowAction.apply(this, [icon || 'pencil-active', action, options]);
        },
        FileUploadAction: function(dialogId, iframeId, options) {
            options = options || {};

            this.style = options.style || {};
            this.disabled = options.disabled;

            this.text = options.text || '';

            if (!options.disabled) {
                var dialog = $('#' + dialogId),
                    iframe = $('#' + iframeId),
                    form = dialog.find('form:first'),
                    dialogOptions = $.extend(
                    {
                        autoOpen: false,
                        draggable: false,
                        resizable: false,
                        modal: true,
                        beforeClose: function() { return dialog.data('in-progress') !== true },
                        close: function() { dialog.parent().find('.olimp-dialog-buttonpane button').olimpbutton('option', 'disabled', false); }
                    }, options.dialogOptions || {}),
                    dialogButtons = options.dialogButtons || { saveText: 'Save', cancelText: 'Cancel' };

                iframe.load(function() {
                    var doc = $.browser.msie
                            ? document.frames[iframe[0].name].document
                            : iframe[0].contentDocument,
                        content = doc.body.innerText || doc.body.textContent,
                        text;

                    if (!doc.location.host.length)
                        return false;

                    try {
                        text = $.parseJSON(content);
                    } catch (e) {
                        content.length && $.Olimp.showError(content);
                    }

                    if (text) {
                        if (text.status == "success") {
                            dialog.data('model').reload();
                        } else if (text.status && text.status.errorMessage) {
                            $.Olimp.showError(text.status.errorMessage);
                        }
                    }

                    dialog.data('in-progress', false);
                    dialog.olimpdialog('close');
                });


                this.action = function(o) {
                    form[0].reset();
                    dialog.data('model', o.model).olimpdialog('open');

                    options.onUploaded && options.onUploaded();
                };

                dialogOptions.buttons = {};
                dialogOptions.buttons[dialogButtons.saveText] = function() {
                    if (!form.valid())
                        return false;

                    dialog.data('in-progress', true)
                        .parent().find('.olimp-dialog-buttonpane button')
                        .olimpbutton('option', 'disabled', true);

                    form.submit();
                };
                dialogOptions.buttons[dialogButtons.cancelText] = function() {
                    dialog.olimpdialog('close');
                };

                dialog.olimpdialog(dialogOptions);
            } else {
                this.action = function() {};
            }

        },
        MassDeleteAction: function(deleteUrl, confirmText, text, getData, options) {
            getData = function() { /* deprecated */ };
            var inProgress = false;
            this.style = (options || {}).style || {};
            this.disabled = options.disabled;
            this.text = text || '';
            this.action = function(o) {
                if (options.disabled || inProgress)
                    return;

                inProgress = true;

                if (o.selectedIds.length == 0) {
                    if (options.msgifzeroselected)
                        $.Olimp.showError(options.msgifzeroselected);
                    inProgress = false;
                    return;
                }

                if (confirmText && !confirm(confirmText.replace('{0}', o.selectedIds.length))) {
                    inProgress = false;
                    return;
                }

                var ids = $.map(o.selectedIds, function(id) { return 'objects=' + id; });
                $.post(deleteUrl, ids.join('&'), function(json) {
                    inProgress = false;

                    var removed = json && json.removed && $.isArray(json.removed)
                        ? json.removed
                        : o.selectedIds.slice(0);
                    for (var i = 0; i < removed.length; i++)
                        o.model.selectedIds.remove(removed[i]);

                    if (options.successMessage)
                        $.Olimp.showSuccess(options.successMessage.replace('{0}', removed.length));

                    o.model.reload();
                }).error(function() { inProgress = false; });
            };
        },
        TableAction: function(handler, confirmText, text, options) {
            this.style = (options || {}).style || {};

            this.text = text || '';
            this.disabled = options.disabled;

            if (!options.disabled) {
                this.action = function(o) {
                    if (confirmText && !confirm(confirmText))
                        return;
                    handler.call(this, o);
                };
            } else {
                this.action = function() {
                    return false;
                };
            }
        },
        Paging: function(model, options) {
            var self = this,
                _pagingInfo = {};

            $.extend(this, {
                rowAmounts: [10, 20, 50],
                size: ko.observable(options.pagingDefaultSize || 10),
                select: function(o) { self.current(o.number) },
                leftPage: function() {
                    var current = self.current();
                    if (current > 1)
                        self.current(current - 1);
                },
                rightPage: function() {
                    var current = self.current();
                    if (current < self.pagesCount())
                        self.current(current + 1);
                }
            });

            if (options.pagingAllowAll)
                this.rowAmounts.push(99999);

            this.current = ko.observable(1);
            this.pagingInfo = ko.observable(_pagingInfo);
            ko.computed(function() {
                var number = this.current();

                var size = this.size(),
                    total = model.rowsCount(),
                    gap = number * size - total

                if (gap > size)
                    number = (number - Math.floor(gap / size)) || 1;

                this.current(number);

                if (size != _pagingInfo.size || number != _pagingInfo.current)
                    this.pagingInfo($.extend(_pagingInfo, { size: size, current: number }))
            }, self)

            self.reset = function(elements) {
                if ($.browser.msie) {
                    $(".paging-size-select").val(self.size());
                }
            };

            self.pagesCount = ko.computed(function() {
                return Math.ceil(this.rowsCount() / self.size());
            }, model);

            this.pages = ko.computed(function() {
                var count = self.pagesCount(), current = self.current()

                return pagesGenerator.getNumbers(count, current,
                    function(n) { return new __self.Page(self, n) },
                    function() { return new __self.PagesSeparator() });
            }, model);

            this.rowsSpan = ko.computed(function() {
                var size = self.size(),
                    rows = model.rowsCount(),
                    end = self.current() * size,
                    start = end - size + 1;

                return Math.min(start, rows) + '-' + Math.min(end, rows);
            }, model);
        },
        PagesSeparator: function() { this.isSeparator = true; },
        Page: function(paging, number) {
            this.number = number;
            this.isSeparator = false;
            this.selectedClass = ko.computed(function() {
                return this.current() == number ? 'selected' : '';
            }, paging);
        },
        Table: function(columns, options) {
            var self = this,
                rowsMapping = { rows: {} },
                actionsMapping = { actions: {} },
                columnsMapping = { columns: {} },
                groupsMapping = { groups: {} }
            rowsCounter = 0;

            actionsMapping.actions.create = function(o) {
                var action = $.extend(
                    o.data,
                    { icon: ko.observable(o.data.icon) })

                action.attrs = action.attrs || {};
                action.attrs.href = action.attrs.href || '#';

                action.attrs['class'] = ko.computed(function() { return 'action icon icon-' + this.icon() }, action)
                action.__rowId = options.rowId;

                return action;
            }

            columnsMapping.columns.create = function(o) {
                o.data.style = { width: o.data.width };

                if (!options.sorting) {
                    o.data._attrs = { 'class': 'inactive' };
                    return o.data;
                }

                o.data.sortClass = ko.computed(function() {
                    var sorting = self.sorting();
                    if (sorting.name !== this.name)
                        return 'no-sort';

                    return sorting.asc ? 'icon icon-bottom-arrow sort ' : 'icon icon-top-arrow sort';
                }, o.data);

                o.data._attrs = { href: '#' };

                return o.data;
            }

            options = $.extend({
                extendViewModel: self,
                sorting: true,
                paging: true,
                checkboxes: false,
                rememberSelected: true,
                rowHighlighting: false,
                checkboxesName: '',
                rowId: 'Id',
                actions: [],
                paramsMapping: {
                    pageSize: 'PageSize',
                    currentPage: 'CurrentPage',
                    sortColumn: 'SortColumn',
                    sortAsc: 'SortAsc',
                    groupBy: 'GroupBy'
                }
            }, options || {})

            self = options.extendViewModel;

            self.tbodyTemplate = options.groupBy ? 'tableGroupsTemplate' : 'tableBodyTemplate';
            if (options.groupBy)
                self.grouping = { groupBy: ko.observable(options.groupBy) }

            //rowsMapping.rows.key = function(o) { return ko.utils.unwrapObservable(o[options.rowId]) }
            rowsMapping.rows.create = function(o) {
                var row = ko.mapping.fromJS(o.data, {}, { _index: ko.observable(rowsCounter++) }),
                    id = ko.utils.unwrapObservable(row[self.rowId]),
                    disposables = [];

                row._isLocked = ko.computed(function() { return self.rowLocked && row[self.rowLocked](); }, row);
                disposables.push(row._isLocked);

                if (options.rowHighlighting) {
                    row._isHighlighted = ko.computed(function() { return this.highlightedId() == id; }, self);
                    disposables.push(row._isHighlighted);
                }

                row._className = ko.computed(function() {
                    var className = (self.grouping || this._index() % 2 == 0) ? 'even' : 'odd';

                    if (options.rowHighlighting && row._isHighlighted())
                        className += ' highlighted';

                    return className;
                }, row);
                disposables.push(row._className);

                if (options.checkboxes) {
                    if (options.checkboxesInitialChecked) {
                        row._checked = ko.observable(true);
                        self.selectedIds.push(id);
                    } else {
                        var wasChecked = self.selectedIds.indexOf(id) != -1;
                        row._checked = ko.observable(wasChecked);
                    }

                    var subscribtion = row._checked.subscribe(function(value) {
                        if (value)
                            self.selectedIds.push(id);
                        else
                            self.selectedIds.remove(id);
                    });

                    disposables.push(row._checked);
                    disposables.push(subscribtion);
                }

                row.$dispose = createDisposeMethod(disposables);

                return row;
            };

            groupsMapping.groups.create = function(o) {
                var group = { name: o.data.name };
                ko.mapping.fromJS(o.data, rowsMapping, group);
                group.$dispose = function() { disposeAll(group.rows()); };

                return group;
            };

            $.extend(self, {
                tbodyEngine: (ko[options.bodyTemplateEngine] || ko.nativeTemplateEngine).instance,
                rowId: options.rowId,
                rowsCount: ko.observable(0),
                rowLockedTemplate: options.rowLockedTemplate || "defaultRowLockedTemplate",
                rows: ko.observableArray([]),
                groups: ko.observableArray([]),
                rowHighlighting: options.rowHighlighting,
                rowLocked: options.rowLocked || false,
                adding: !!options.adding,
                headerActions: !!options.headerActions,
                tableActions: options.tableActions || [],
                buttons: !!options.adding || (!!options.tableActions && options.tableActions.length),
                checkboxes: options.checkboxes,
                checkboxesName: options.checkboxesName,
                sorting: options.sorting && ko.observable({ name: options.defaultSortColumn || '', asc: options.defaultSortAsc !== false }),
                _rowAction: function(row, link, ip) {
                    return link.action && link.action({
                        model: self,
                        data: row,
                        action: link,
                        invocationParams: ip || {}
                    });
                },
                _rowHighlight: function(row) {
                    if (options.rowHighlighting) {
                        var id = ko.utils.unwrapObservable(row[self.rowId]);
                        self.highlightedId(id);
                    }

                    return true;
                },
                _tableAction: function(button) {
                    var model = { model: self, action: button, items: [], selectedIds: ko.utils.unwrapObservable(self.selectedIds || []) };
                    if (options.checkboxes) {
                        if (!self.grouping) {
                            ko.utils.arrayForEach(self.rows(), function(r) {
                                if (r._checked()) model.items.push(r);
                            })
                        } else {
                            ko.utils.arrayForEach(self.groups(), function(g) {
                                ko.utils.arrayForEach(g.rows(), function(r) {
                                    if (r._checked()) model.items.push(r);
                                })
                            })
                        }
                    }

                    return button.action(model);
                },
                _sort: function(column) {
                    var sorting = self.sorting();
                    var newSorting = sorting.name === column.name
                        ? { name: column.name, asc: !sorting.asc }
                        : { name: column.name, asc: true }

                    self.sorting(newSorting);
                }
            });

            if (self.tbodyEngine !== ko.nativeTemplateEngine.instance)
                self.tbodyTemplate += '_' + options.bodyTemplateEngine;

            self.paging = options.paging && new __self.Paging(self, options);

            if (options.rowHighlighting)
                self.highlightedId = ko.observable(null);

            if (options.adding)
                self.adding = new __self.Adding(self, options);

            self.requestData = ko.observable({});

            if (options.checkboxes) {

                self.getCheckedIds = function() { return self.selectedIds(); };
                self.clearSelected = function() {
                    self.allChecked(false);
                    self.selectedIds.removeAll();
                };
                self.requestData.subscribe(function () {
                    self.clearSelected();
                });

                self.selectedIds = ko.observableArray([]);

                self.selectedItemsCount = ko.computed(function() {
                    return self.selectedIds().length;
                }, self);

                if (!self.grouping) {
                    self.allChecked = ko.computed({
                        read: function() {
                            var rows = this.rows();
                            if (!rows.length)
                                return false;

                            for (var i = 0; i < rows.length; i++) {
                                if (!rows[i]._checked()) return false;
                            }

                            return true;
                        },
                        write: function(value) {
                            ko.utils.arrayForEach(this.rows(), function(o) { o._checked(value) })
                        },
                        owner: self
                    });
                } else {
                    self.allChecked = ko.computed({
                        read: function() {
                            var groups = this.groups();
                            if (!groups.length)
                                return !!options.checkboxesInitialChecked;

                            for (var g = 0; g < groups.length; g++) {
                                var rows = groups[g].rows();
                                for (var i = 0; i < rows.length; i++) {
                                    if (!rows[i]._checked()) return false;
                                }
                            }

                            return true;
                        },
                        write: function(value) {
                            ko.utils.arrayForEach(this.groups(), function(g) {
                                ko.utils.arrayForEach(g.rows(), function(o) { o._checked(value) })
                            });
                        },
                        owner: self
                    });
                }

                if (options.checkboxesCheckedObservable) {
                    if (options.checkboxesCheckedObservable._subscribtionToDispose)
                        options.checkboxesCheckedObservable._subscribtionToDispose.dispose();
                    syncObservables(self.allChecked, options.checkboxesCheckedObservable)
                }
            }
            self.columnsCount = columns.length +
            (options.checkboxes ? 1 : 0) + (options.actions.length ? 1 : 0) + (options.rowHighlighting ? 1 : 0);

            ko.mapping.fromJS({ actions: options.actions }, actionsMapping, self);
            ko.mapping.fromJS({ columns: columns }, columnsMapping, self);

            self.invokeAction = function(index, row, ip) {
                var link = self.actions()[index];
                if (!link)
                    return false;

                return self._rowAction(row, link, ip);
            };

            self.setRows = function(rows) {
                rowsCounter = 0;

                var mapping;

                if (self.grouping) {
                    mapping = groupsMapping;
                    disposeAll(self.groups());
                    self.groups([]);
                } else {
                    mapping = rowsMapping;
                    disposeAll(self.rows());
                    self.rows([]);
                }

                if (options.checkboxes && !options.rememberSelected)
                    self.selectedIds.removeAll();

                ko.mapping.fromJS(rows, mapping, self);

                self._initReloading();
            };

            self._initReloading = function() {
                if (options.reloadInterval && !self._intervalId && !self._reloadSuspended)
                    self._intervalId = setInterval(function() { self.reload() }, options.reloadInterval);
            };
            self._clearReloadInterval = function() {
                if (self._intervalId)
                    clearInterval(self._intervalId);
                self._intervalId = null;
            };

            self.suspendReload = function(unsuspend) {
                (self._reloadSuspended = (unsuspend !== false))
                    ? self._clearReloadInterval()
                    : self._initReloading();
            };

            self.reload = function(cb) {
                var data = self.requestData(),
                    callback = data.__callback;

                delete data.__callback;

                self._clearReloadInterval();

                if (self.grouping)
                    data[options.paramsMapping.groupBy] = self.grouping.groupBy()

                if (options.paging) {
                    var pagingInfo = self.paging.pagingInfo();
                    data[options.paramsMapping.pageSize] = pagingInfo.size;
                    data[options.paramsMapping.currentPage] = pagingInfo.current;
                }

                if (options.sorting) {
                    var sorting = self.sorting();
                    data[options.paramsMapping.sortColumn] = sorting.name;
                    data[options.paramsMapping.sortAsc] = sorting.asc;
                }

                var cbs = [cb, options.onReload, callback];
                if (options.selectUrl && !options.manualLoad) {
                    $.post(options.selectUrl, data, function(json) { onTableReload(self, json, cbs) }, 'json')
                } else if (options.tableData) {
                    var rows = options.tableData;

                    if (options.paging) {
                        rows = applyPagingAndSortingToRows(options, data);
                    }

                    setTimeout(function() { onTableReload(self, rows, cbs) }, 0);
                }
            };

            ko.computed(self.reload, self);

            if (options.manualLoad) {
                self.manualLoaded = false;
                self.load = function(cb) {
                    options.manualLoad = false;
                    self.manualLoaded = true;

                    self.reload(cb);
                }
            }
        }
    });
})(window);

ko.bindingHandlers.tableData = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var bindings = allBindingsAccessor(),
            tableColumns = ko.utils.unwrapObservable(bindings['tableColumns']),
            tableOptions = ko.utils.unwrapObservable(bindings['table']),
            rowsChecked = bindings['tableRowsChecked'],
            value = ko.utils.unwrapObservable(valueAccessor()),
            tableData = ko.mapping.toJS(value),
            e = $(element),
            model = e.data('viewModel');

        if (!model) {
            if (rowsChecked && ko.isObservable(rowsChecked))
                $.extend(tableOptions, { checkboxesCheckedObservable: rowsChecked })

            model = new Table(tableColumns, tableOptions);

            ko.applyBindings(model, $('<div />')
                .addClass("table")
                .attr('data-bind', "template: 'tableTemplate'")
                .appendTo(e.data('viewModel', model))
                .get(0));

            e.find('.buttons button').olimpbutton();
        }

        model.setRows(tableData)

        return { 'controlsDescendantBindings': true };
    }
}

ko.doTEngine = function () { };

(function (ko) {
    function hasClass(el, cls) {
        return el.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
    }

    function removeClass(el, cls) {
        if (hasClass(el, cls)) {
            var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
            el.className = el.className.replace(reg, ' ');
        }
    }

    function parseAndApplyObservables(html, $$contexts) {
        var elems = jQuery['clean']([html]);

        if (elems && elems[0]) {
            var elem = elems[0];
            while (elem.parentNode && elem.parentNode.nodeType !== 11)
                elem = elem.parentNode;

            if (elem.parentNode)
                elem.parentNode.removeChild(elem);
        }

        for (var i = 0; i < elems.length; i++) {
            var e = $(elems[i]);

            var subElements = e.find('.-has-dot-binding');
            if (e.hasClass('-has-dot-binding'))
                subElements = subElements.andSelf();

            subElements.each(function (index, node) {
                var ctxAttr = node.getAttribute('data-dot-context'),
                    bindAttr = node.getAttribute('data-dot-bind');

                if (!ctxAttr || !bindAttr)
                    return;

                removeClass(node, '-has-dot-binding');

                var ctx = $$contexts[ctxAttr]['.bindingContext'],
                    bindings = ko.bindingProvider['instance']['parseBindingsString'](bindAttr, ctx);

                ko.applyBindingsToNode(node, bindings, ctx);
            });
        }

        return elems;
    };

    var compiledTemplateCache = {};

    function getTemplateThroughCache(id) {
        var template = compiledTemplateCache[id];
        if (!template) {
            template = doT.template($('#' + id).html())
            compiledTemplateCache[id] = template;
        }

        return template;
    }

    ko.doTEngine = function () {
        this['allowTemplateRewriting'] = false;
    };

    ko.doTEngine.doTContext = function (bindingContext, dotContexts) {
        this['.bindingContext'] = bindingContext;
        dotContexts = (this['.doTContexts'] = dotContexts || []);

        this['$$contextId'] = dotContexts.length;
        dotContexts.push(this);

        ko.utils.extend(this, bindingContext);
    }
    ko.doTEngine.doTContext.prototype["createChildContext"] = function (dataItemOrAccessor, dataItemAlias, extendCallback) {
        var koContext = this['.bindingContext'].createChildContext(dataItemOrAccessor, dataItemAlias, extendCallback);
        return new ko.doTEngine.doTContext(koContext, this['.doTContexts']);
    }
    ko.doTEngine.doTContext.prototype['renderTemplate'] = function (templateId, data) {
        var template = getTemplateThroughCache(templateId),
            ctx = this.createChildContext(data);

        return template(ctx);
    }

    ko.doTEngine.prototype = ko.utils.extend(new ko.templateEngine(), {
        renderTemplateSource: function (templateSource, bindingContext, options) {
            var precompiled = templateSource.data('precompiled');
            if (!precompiled) {
                precompiled = doT.template(templateSource.text());
                templateSource.data('precompiled', precompiled);
            }

            var doTContext = new ko.doTEngine.doTContext(bindingContext),
                renderedMarkup = precompiled(doTContext);

            return parseAndApplyObservables(renderedMarkup, doTContext['.doTContexts']);
        },
        createJavaScriptEvaluatorBlock: function (script) { return "{{=" + script + "}}" }
    });



    ko.doTEngine.instance = new ko.doTEngine();
})(ko);







