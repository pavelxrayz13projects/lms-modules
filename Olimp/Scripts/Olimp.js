$.Olimp = {
    defaults: {
        loaderUrl: '/Content/Images/loader.gif'
    },
	showMessage: function(type, html, onDisappeared) {
		var win = $(window),
			eventId = '.message' + new Date().getTime(),			
			div =  $('<div />')
				.addClass(type + "-message")			
				.html(html)		
				.appendTo('body');
		
		
		if ($.browser.msie && $.browser.version < 7) {
		    div.css({ 'position': 'absolute', 'top': 6 + win.scrollTop() })
			win.bind('scroll' + eventId, function() {
				setTimeout(function() { div.css('top', 6 + win.scrollTop()) }, 0);
			});
		}
		
		var finalize = function() { 
			div.remove(); 
			win.unbind(eventId);

		    if ($.isFunction(onDisappeared))
		        onDisappeared.apply(window)
		}
		
		div.data('finalizer', finalize);
	    div.css('margin-left', (-1) * (div.outerWidth() / 2))
		div.delay(5000).fadeOut(3000).delay(100, finalize);
		
		return div.mouseenter(function() { div.stop(true, true).fadeIn(500) })
			.mouseleave(function() { div.delay(5000).fadeOut(3000).delay(100, finalize) });
	},
	showError: function (html, onDisappeared) { return $.Olimp.showMessage('error', html, onDisappeared) },
	showWarning: function (html, onDisappeared) { return $.Olimp.showMessage('warning', html, onDisappeared) },
	closeWarnings: function () { 
		$('.warning-message').each(function() {
			$(this).clearQueue().data('finalizer').apply(window);
		})
	},
	showSuccess: function (html, onDisappeared) { return $.Olimp.showMessage('success', html, onDisappeared) },
	showLoader: function () {
	    var position = $('#head .logo').position();
	    if (!position)
	        return;

	    var loader = $('<img class="olimp-loader-logo" src="' + $.Olimp.defaults.loaderUrl + '"/>')
            .css({ position: 'absolute', left: position.left, top: position.top })
			.appendTo('body');

	    ($.Olimp.loaderStack = $.Olimp.loaderStack || []).push(loader);
	},
	hideLoader: function () {
	    if (!$.Olimp.loaderStack)
	        return;

	    var loader = $.Olimp.loaderStack.pop();
	    if (loader)
	        loader.remove();
	},
	newGuid: function () {
	    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
	        var r = Math.random() * 16 | 0,
                v = c == 'x' ? r : (r & 0x3 | 0x8);

	        return v.toString(16);
	    });
	},
	hasWorkplaceToken: function () { return document.cookie.indexOf('WorkplaceToken') != -1 },
	ensureWorkplaceToken: function (guid) {
	    if ($.Olimp.hasWorkplaceToken())
	        return;

	    $.cookie('WorkplaceToken', guid || $.Olimp.newGuid(), { path: '/', expires: 182500 })
	},
	cookiesEnabled: function () {
	    var cookiesEnabled = !!navigator.cookieEnabled;

	    if (!cookiesEnabled && typeof navigator.cookieEnabled == 'undefined') {
	        $.cookie('TestCookie', true, { path: '/' });
	        cookiesEnabled = !!$.cookie('TestCookie');
	        $.cookie('TestCookie', null, { path: '/' });
	    }

	    return cookiesEnabled;
	}
};
$.Olimp.Tooltip = {
    Init: function (target_items, name) {
        $(target_items).each(function (i) {
            if ($("#" + name + i).length == 0) {
                $("body").append("<div class='" + name + "' id='" + name + i + "'><p>" + $(this).attr('title') + "</p></div>");
            }
            var my_tooltip = $("#" + name + i);

            $(this).removeAttr("title").mouseover(function () {
                my_tooltip.css({ display: "none" }).fadeIn(400);
            }).mousemove(function (kmouse) {
                my_tooltip.css({ left: kmouse.pageX + 15, top: kmouse.pageY + 15 });
            }).mouseout(function () {
                my_tooltip.fadeOut(400);
            });
        });
    }
};
$(function() {
    $('body')
        .ajaxStart(function() { $.Olimp.showLoader(); })
        .ajaxStop(function(event, xmlHttpRequest, ajaxOptions) { $.Olimp.hideLoader(); })
        .ajaxError(function(event, jqXHR, ajaxSettings, thrownError) {
            var text = jqXHR.responseText;
            $.Olimp.showError(text && text.length ? text : thrownError);
        })
        .ajaxSuccess(function(event, xmlHttpRequest, ajaxOptions) {
            if (ajaxOptions.dataType != 'json')
                return true;

            setTimeout(function() {
                var json;

                try {
                    json = $.parseJSON(xmlHttpRequest.responseText)
                    if (!json || !json.status)
                        return;
                } catch (e) {
                    return;
                }

                if (json.status.errorMessage)
                    $.Olimp.showError(json.status.errorMessage);
                else if (json.status.successMessage)
                    $.Olimp.showSuccess(json.status.successMessage);
            }, 0);
        })
});