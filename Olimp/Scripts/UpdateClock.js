function UpdateClock(seconds, up) {
	if(clockID)
		clearTimeout(clockID);

	if (!(seconds >= 0)) {
		$('#question-table :checkbox, #question-table :radio').prop('checked', false)
		$('#Next').click();
		return;
	}	

	var SecondsToEnd = seconds;

	var strTime = "";
	var hours = Math.floor(seconds/3600);

	if (hours > 0) {
		strTime += (hours < 10 ? "0" : "") + hours + ":";
		seconds = seconds - hours*3600;
	} else {
		strTime += "00:";
	}

	var minutes = Math.floor(seconds/60);

	if (minutes > 0) {
		strTime += (minutes < 10 ? "0" : "") + minutes + ":";
		seconds = seconds - minutes*60;
	} else {
		strTime += "00:";
	}
	
	var sec = (seconds%60);
	strTime += (sec < 10 ? "0" : "") + sec;
	
	$("#TimeLimit").val(SecondsToEnd);
	$("#timer").html(strTime);
	if(up){
		clockID = setTimeout("UpdateClock("+(SecondsToEnd+1)+",true)", 950);
	}else{
		clockID = setTimeout("UpdateClock("+(SecondsToEnd-1)+",false)", 950);
	}
}
