﻿(function($, undefined) {

var baseClasses = "olimp-button", hoverClass = "olimp-button-hover";

$.widget("ui.olimpbutton", {
    options: { disabled: null },

    _create: function() {
        if (typeof this.options.disabled !== "boolean") {
            this.options.disabled = !!this.element.propAttr("disabled");
        } else {
            this.element.propAttr("disabled", this.options.disabled);
        }

        this._determineButtonType();
        this.hasTitle = !!this.buttonElement.attr("title");

        var self = this, options = this.options;
        
        options.label = this.buttonElement.html();
        
        this.buttonElement
            .addClass(baseClasses)
            .attr("role", "button")
            .bind("mouseenter.olimpbutton", function() {
                if (options.disabled) return;
                
                $(this).addClass(hoverClass);				
            })
            .bind("mouseleave.olimpbutton", function() {
                if (options.disabled) return;
                
                $(this).removeClass(hoverClass);
            })
            .bind("click.olimpbutton", function(event) {
                if (options.disabled) {
                    event.preventDefault();
                    event.stopImmediatePropagation();
                }
            });

        
        if (this.buttonElement.is("a")) {
            this.buttonElement.keyup(function(event) {
                if (event.keyCode === $.ui.keyCode.SPACE) $(this).click()					
            });
        }
        
        this._setOption("disabled", options.disabled);
        this._resetButton();
    },
    
    _determineButtonType: function() {
        this.type = this.element.is("input")
            ?  "input"
            :  "button";

        this.buttonElement = this.element;
    },
    
    widget: function() { return this.buttonElement },

    destroy: function() {
        this.buttonElement
            .removeClass(baseClasses + " " + hoverClass)
            .removeAttr("role")
            .removeAttr("aria-pressed")
            .html(this.buttonElement.find(".olimp-button-text").html());

        if (!this.hasTitle)
            this.buttonElement.removeAttr("title");

        $.Widget.prototype.destroy.call(this);
    },
    
    _setOption: function(key, value) {
        $.Widget.prototype._setOption.apply(this, arguments);
        if (key === "disabled") {
            if (value) {
                this.element.propAttr("disabled", true);
            } else {
                this.element.propAttr("disabled", false);
            }
            
            return;
        }
        
        this._resetButton();
    },
    
    refresh: function() {
        var isDisabled = this.element.is(":disabled");
        if (isDisabled !== this.options.disabled)
            this._setOption("disabled", isDisabled);		
    },
    
    _resetButton: function() {
        if (this.type === "input") {
            if (this.options.label)
                this.element.val(this.options.label);
            
            return;
        }
        
        buttonText = $("<span></span>", this.element[0].ownerDocument)
            .addClass("olimp-button-text")
            .html(this.options.label)
            .appendTo(this.buttonElement.empty())
            .text();
    }
});


})(jQuery);

(function($, undefined) {

    var uiDialogClasses = 'olimp-dialog ',
        sizeRelatedOptions = {
            buttons: true,
            height: true,
            maxHeight: true,
            maxWidth: true,
            minHeight: true,
            minWidth: true,
            width: true
        },
        resizableRelatedOptions = {
            maxHeight: true,
            maxWidth: true,
            minHeight: true,
            minWidth: true
        },
    
        attrFn = $.attrFn || {
            val: true,
            css: true,
            html: true,
            text: true,
            data: true,
            width: true,
            height: true,
            offset: true,
            click: true
        };

    $.widget("ui.olimpdialog", {
        options: {
            autoOpen: true,
            buttons: {},
            closeOnEscape: true,
            closeText: 'Close',
            dialogClass: '',
            draggable: true,
            hide: null,
            height: 'auto',
            maxHeight: false,
            maxWidth: false,
            minHeight: 80,
            minWidth: 150,
            fixedHeight: false,
            modal: false,
            position: {
                my: 'center',
                at: 'center',
                collision: 'fit',			
                using: function(pos) {
                    var topOffset = $(this).css(pos).offset().top;
                    if (topOffset < 0) {
                        $(this).css('top', pos.top - topOffset);
                    }
                }
            },
            show: null,
            title: '',
            width: 530,
            zIndex: 1000		
        },

        _create: function() {
            this.originalTitle = this.element.attr('title');
            if (typeof this.originalTitle !== "string")
                this.originalTitle = "";

            this.options.title = this.options.title || this.originalTitle;

            var self = this, 
                options = self.options,
                title = options.title || '&#160;',
                titleId = $.ui.olimpdialog.getTitleId(self.element),

                uiDialog = (self.uiDialog = $('<div></div>'))
                    .appendTo(document.body)
                    .hide()
                    .addClass(uiDialogClasses + options.dialogClass)
                    .css({ 'zIndex': options.zIndex, 'position': 'absolute' })				
                    .attr('tabIndex', -1)
                    .css('outline', 0)
                    .keydown(function(event) {
                        if (options.closeOnEscape && !event.isDefaultPrevented() && event.keyCode &&
                            event.keyCode === $.ui.keyCode.ESCAPE) {
                        
                            self.close(event);
                            event.preventDefault();
                        }
                    })
                    .attr({ role: 'dialog', 'aria-labelledby': titleId })
                    .mousedown(function(event) { self.moveToTop(false, event); }),
                uiDialogContent = self.element
                    .show()
                    .removeAttr('title')
                    .addClass('olimp-dialog-content')
                    .appendTo(uiDialog),
                uiDialogTitlebar = (self.uiDialogTitlebar = $('<div></div>'))
                    .addClass('olimp-dialog-titlebar')
                    .prependTo(uiDialog),
                uiDialogTitlebarClose = $('<a href="#"></a>')
                    .addClass('olimp-dialog-titlebar-close')
                    .attr({'role': 'button', 'title': options.closeText })	
                    .click(function(event) { self.close(event); return false; })
                    .appendTo(uiDialogTitlebar),			
                uiDialogTitle = $('<span></span>')
                    .addClass('olimp-dialog-title')
                    .attr('id', titleId)
                    .html(title)
                    .prependTo(uiDialogTitlebar);
                
            if ($.browser.msie && $.browser.version <= 6) {
                var frame = $('<iframe />')
                    .css({ width: '100%', top: 0, left: 0, height: 2000, position: 'absolute', zIndex: -9999, opacity: 0, border: 'solid 1px #c5cbd0' })
                    .attr({ frameborder: 0, scrolling: 'no', align: 'bottom', marginheight: 0, marginwidth: 0, src: 'javascript:"";' });
        
                uiDialog.append(frame)
                    .wrapInner($('<div />')
                        .css({ width: '100%', height: '100%', position: 'relative' }));
            }

            if ($.isFunction(options.beforeclose) && !$.isFunction(options.beforeClose))
                options.beforeClose = options.beforeclose;		

            uiDialogTitlebar.find("*").add(uiDialogTitlebar).disableSelection();

            if (options.draggable && $.fn.draggable) self._makeDraggable();

            self._createButtons(options.buttons);
            self._isOpen = false;
        
            if (options.fixedHeight) {
                var win = $(window), dialogHeight = Math.floor(win.height() * options.fixedHeight);
                self._setOptions({ 
                    'height': Math.floor(win.height() * options.fixedHeight),
                    'position': options.position
                });
            
                win.bind('resize.olimp-dialog', function() {
                    setTimeout(function() { 
                        self._setOptions({ 
                            'height': Math.floor(win.height() * options.fixedHeight),
                            'position': options.position
                        })
                    }, 0) 
                })			
            }
        },

        _init: function() { this.options.autoOpen && this.open() },

        destroy: function() {
            var self = this;
        
            if (self.overlay)
                self.overlay.destroy();
        
            self.uiDialog.hide();
            self.element
                .unbind('.olimpdialog')
                .removeData('olimpdialog')
                .removeClass('olimp-dialog-content')
                .hide().appendTo('body');
            self.uiDialog.remove();

            if (self.originalTitle)
                self.element.attr('title', self.originalTitle);
        
            return self;
        },

        widget: function() { return this.uiDialog; },

        close: function(event) {
            var self = this, maxZ, thisZ;
        
            if (false === self._trigger('beforeClose', event))
                return;

            if (self.overlay) self.overlay.destroy();
        
            self.uiDialog.unbind('keypress.olimp-dialog');

            self._isOpen = false;

            if (self.options.hide) {
                self.uiDialog.hide(self.options.hide, function() { self._trigger('close', event) });
            } else {
                self.uiDialog.hide();
                self._trigger('close', event);
            }

            $.ui.olimpdialog.overlay.resize();

            if (self.options.modal) {
                maxZ = 0;
                $('.olimp-dialog').each(function() {
                    if (this !== self.uiDialog[0]) {
                        thisZ = $(this).css('z-index');
                        if(!isNaN(thisZ))
                            maxZ = Math.max(maxZ, thisZ);					
                    }
                });
                $.ui.olimpdialog.maxZ = maxZ;
            }

            return self;
        },

        isOpen: function() { return this._isOpen },

        moveToTop: function(force, event) {
            var self = this, options = self.options, saveScroll;

            if (!options.modal) {			
                self.uiDialog.css('z-index', ++$.ui.olimpdialog.maxZ);
            }

            if ((options.modal && !force) || !options.modal)
                return self._trigger('focus', event);
        
            if (options.zIndex > $.ui.maxZ)
                $.ui.olimpdialog.maxZ = options.zIndex;
        
            if (self.overlay) {
                $.ui.olimpdialog.maxZ += 1;
                self.overlay.$el.css('z-index', $.ui.olimpdialog.overlay.maxZ = $.ui.olimpdialog.maxZ);
            }

            saveScroll = { scrollTop: self.element.scrollTop(), scrollLeft: self.element.scrollLeft() };
            $.ui.olimpdialog.maxZ += 1;
            self.uiDialog.css('z-index', $.ui.olimpdialog.maxZ);
            self.element.attr(saveScroll);
            self._trigger('focus', event);

            return self;
        },

        open: function() {
            if (this._isOpen) { return; }

            var self = this, options = self.options, uiDialog = self.uiDialog;

            self.overlay = options.modal ? new $.ui.olimpdialog.overlay(self) : null;
            self._size();
            self._position(options.position);
            uiDialog.show(options.show);
            self.moveToTop(true);

            if (options.modal) {
                uiDialog.bind("keydown.olimp-dialog", function(event) {
                    if (event.keyCode !== $.ui.keyCode.TAB) return;
                
                    var tabbables = $(':tabbable', this),
                        first = tabbables.filter(':first'),
                        last  = tabbables.filter(':last');

                    if (event.target === last[0] && !event.shiftKey) {
                        first.focus(1);
                        return false;
                    } else if (event.target === first[0] && event.shiftKey) {
                        last.focus(1);
                        return false;
                    }
                });
            }

            $(self.element.find(':tabbable')
                .get()
                .concat(uiDialog.find('.olimp-dialog-buttonpane :tabbable')
                    .get()
                    .concat(uiDialog.get())
                )
            ).eq(0).focus();
        
            if ($.browser.msie && $.browser.version <= 7) { 
                if (this.element[0].scrollHeight > this.element.innerHeight()) {
                    this.element.addClass('olimp-dialog-content-overflowed')
                } else {
                    this.element.removeClass('olimp-dialog-content-overflowed')
                }
            }

            self._isOpen = true;
            self._trigger('open');

            return self;
        },

        _createButtons: function(buttons) {
            var self = this,
                hasButtons = false,
                uiDialogButtonPane = $('<div></div>').addClass('olimp-dialog-buttonpane');

            self.uiDialog.find('.olimp-dialog-buttonpane').remove();

            if (typeof buttons === 'object' && buttons !== null) {
                $.each(buttons, function() {
                    return !(hasButtons = true);
                });
            }
            if (hasButtons) {
                $.each(buttons, function(name, props) {
                    props = $.isFunction( props ) ? { click: props, text: name } : props;
                    var button = $('<button type="button"></button>')
                        .click(function() { props.click.apply(self.element[0], arguments) })
                        .appendTo(uiDialogButtonPane);
                
                    $.each(props, function(key, value) {
                        if (key === "click") return;
                    
                        if (key in attrFn) button[key](value);
                        else button.attr(key, value);					
                    });
                
                    if ($.fn.olimpbutton) button.olimpbutton();				
                });
                uiDialogButtonPane.appendTo(self.uiDialog);
            }
        },

        _makeDraggable: function() {
            var self = this, options = self.options, doc = $(document), heightBeforeDrag;

            function filteredUi(ui) {
                return { position: ui.position, offset: ui.offset };
            }

            self.uiDialog.draggable({
                cancel: '.olimp-dialog-content, .olimp-dialog-titlebar-close',
                handle: '.olimp-dialog-titlebar',
                containment: 'document',
                start: function(event, ui) {
                    heightBeforeDrag = options.height === "auto" ? "auto" : $(this).height();
                    $(this).height($(this).height()).addClass("olimp-dialog-dragging");
                    self._trigger('dragStart', event, filteredUi(ui));
                },
                drag: function(event, ui) { self._trigger('drag', event, filteredUi(ui)) },
                stop: function(event, ui) {
                    options.position = [ui.position.left - doc.scrollLeft(),
                        ui.position.top - doc.scrollTop()];
                    $(this).removeClass("olimp-dialog-dragging").height(heightBeforeDrag);
                    self._trigger('dragStop', event, filteredUi(ui));
                    $.ui.olimpdialog.overlay.resize();
                }
            });
        },

        _minHeight: function() {
            var options = this.options;

            if (options.height === 'auto') { return options.minHeight } 
            else { return Math.min(options.minHeight, options.height) }
        },

        _position: function(position) {
            var myAt = [], offset = [0, 0], isVisible;

            if (position) {
                if (typeof position === 'string' || (typeof position === 'object' && '0' in position)) {
                    myAt = position.split ? position.split(' ') : [position[0], position[1]];
                    if (myAt.length === 1) { myAt[1] = myAt[0]; }

                    $.each(['left', 'top'], function(i, offsetPosition) {
                        if (+myAt[i] === myAt[i]) {
                            offset[i] = myAt[i];
                            myAt[i] = offsetPosition;
                        }
                    });

                    position = { my: myAt.join(" "), at: myAt.join(" "), offset: offset.join(" ") };
                }

                position = $.extend({}, $.ui.olimpdialog.prototype.options.position, position);
            } else {
                position = $.ui.olimpdialog.prototype.options.position;
            }

            isVisible = this.uiDialog.is(':visible');
            if (!isVisible) { this.uiDialog.show() }
        
            this.uiDialog			
                .css({ top: 0, left: 0 })
                .position($.extend({ of: window }, position));
            
            if (!isVisible) { this.uiDialog.hide() }
        },

        _setOptions: function(options) {
            var self = this, resizableOptions = {}, resize = false;

            $.each(options, function(key, value) {
                self._setOption(key, value);
            
                if (key in sizeRelatedOptions) { resize = true; }
                if (key in resizableRelatedOptions) { resizableOptions[key] = value; }
            });

            if (resize) { this._size() }		
        },

        _setOption: function(key, value){
            var self = this, uiDialog = self.uiDialog;

            switch (key) {
                case "beforeclose": key = "beforeClose";  break;
                case "buttons": self._createButtons(value); break;
                case "closeText": self.uiDialogTitlebarClose.attr('title','' + value); break;
                case "dialogClass":
                    uiDialog.removeClass(self.options.olimpdialogClass).addClass(uiDialogClasses + value);
                    break;
                case "disabled":
                    if (value) { uiDialog.addClass('olimp-dialog-disabled') } 
                    else { uiDialog.removeClass('olimp-dialog-disabled') }
                    break;
                case "draggable":
                    var isDraggable = uiDialog.is(":data(draggable)");
                    if (isDraggable && !value) uiDialog.draggable( "destroy" );
                
                    if (!isDraggable && value) { self._makeDraggable() }
                
                    break;
                case "position": self._position(value); break;			
                case "title":
                    $(".olimp-dialog-title", self.uiDialogTitlebar).html("" + (value || '&#160;'));
                    break;
            }

            $.Widget.prototype._setOption.apply(self, arguments);
        },

        _size: function() {
            var options = this.options,
                nonContentHeight,
                minContentHeight,
                isVisible = this.uiDialog.is(":visible");

            this.element.show().css({ width: 'auto', minHeight: 0, height: 0 });

            if (options.minWidth > options.width) { options.width = options.minWidth }

            nonContentHeight = this.uiDialog.css({ height: 'auto', width: options.width })
                .height();
            
            minContentHeight = Math.max(0, options.minHeight - nonContentHeight);
        
            if (options.height === "auto") {
                if ($.support.minHeight) {
                    this.element.css({ minHeight: minContentHeight, height: "auto" });
                } else {
                    this.uiDialog.show();
                    var autoHeight = this.element.css("height", "auto").height();
                    if (!isVisible) { this.uiDialog.hide() }
                    this.element.height( Math.max( autoHeight, minContentHeight ) );
                }
            } else {
                this.element.height( Math.max( options.height - nonContentHeight, 0 ) );
            }	
        }
    });

    $.extend($.ui.olimpdialog, {
        uuid: 0,
        maxZ: 0,

        getTitleId: function($el) {
            var id = $el.attr('id');
            if (!id) {
                this.uuid += 1;
                id = this.uuid;
            }
            return 'olimp-dialog-title-' + id;
        },

        overlay: function(dialog) { this.$el = $.ui.olimpdialog.overlay.create(dialog) }
    });

    $.extend($.ui.olimpdialog.overlay, {
        instances: [],
        oldInstances: [],
        maxZ: 0,
        events: $.map('focus,mousedown,mouseup,keydown,keypress,click'.split(','),
            function(event) { return event + '.olimpdialog-overlay'; }).join(' '),
        create: function(dialog) {
            if (this.instances.length === 0) {
                setTimeout(function() {
                    if ($.ui.olimpdialog.overlay.instances.length) {
                        $(document).bind($.ui.olimpdialog.overlay.events, function(event) {
                            if ($(event.target).zIndex() < $.ui.olimpdialog.overlay.maxZ) {
                                return false;
                            }
                        });
                    }
                }, 1);

                $(document).bind('keydown.olimpdialog-overlay', function(event) {
                    if (dialog.options.closeOnEscape && !event.isDefaultPrevented() && event.keyCode &&
                        event.keyCode === $.ui.keyCode.ESCAPE) {
                    
                        dialog.close(event);
                        event.preventDefault();
                    }
                });

                $(window).bind('resize.olimpdialog-overlay', $.ui.olimpdialog.overlay.resize);
            }

            var $el = (this.oldInstances.pop() || $('<div></div>').addClass('olimp-overlay'))
                .appendTo(document.body)
                .css({ width: this.width(), height: this.height() });

            if ($.browser.msie && $.browser.version <= 6) {
                $el.append($('<iframe />')
                    .css({ width: '100%', height: '100%', opacity: 0 })
                    .attr({ frameborder: 0, scrolling: 'no', align: 'bottom', marginheight: 0, marginwidth: 0, src: 'javascript:"";' }))
            }

            if ($.fn.bgiframe) $el.bgiframe();
        
            this.instances.push($el);
        
            setTimeout (function () { 
                $el.css('font-size', 10)
                    .css('font-size', 11) 
            }, 20);
        
            return $el;
        },

        destroy: function($el) {
            var indexOf = $.inArray($el, this.instances);
            if (indexOf != -1){
                this.oldInstances.push(this.instances.splice(indexOf, 1)[0]);
            }

            if (this.instances.length === 0) { $([document, window]).unbind('.olimpdialog-overlay') }

            $el.remove();
        
            var maxZ = 0;
            $.each(this.instances, function() { maxZ = Math.max(maxZ, this.css('z-index')) });
        
            this.maxZ = maxZ;
        },

        height: function() {
            var scrollHeight, offsetHeight;
        
            if ($.browser.msie && $.browser.version < 7) {
                scrollHeight = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight );
                offsetHeight = Math.max(document.documentElement.offsetHeight, document.body.offsetHeight);

                if (scrollHeight < offsetHeight) { return $(window).height() + 'px'; } 
                else { return scrollHeight + 'px'; }		
            } else {
                return $(document).height() + 'px';
            }
        },

        width: function() {
            var scrollWidth, offsetWidth;
        
            if ($.browser.msie) {
                scrollWidth = Math.max(document.documentElement.scrollWidth, document.body.scrollWidth );
                offsetWidth = Math.max( document.documentElement.offsetWidth, document.body.offsetWidth );

                if (scrollWidth < offsetWidth) { return $(window).width() + 'px'; } 
                else { return scrollWidth + 'px'; }		
            } else {
                return $(document).width() + 'px';
            }
        },

        resize: function() {		
            var $overlays = $([]);
            $.each($.ui.olimpdialog.overlay.instances, function() { $overlays = $overlays.add(this); });

            $overlays.css({ width: 0, height: 0 })
                    .css({
                        width: $.ui.olimpdialog.overlay.width(), height: $.ui.olimpdialog.overlay.height()
                    });
        }
    });

    $.extend($.ui.olimpdialog.overlay.prototype, {
        destroy: function() { $.ui.olimpdialog.overlay.destroy(this.$el); }
    });

    $.widget("ui.olimpsyncformdialog", $.ui.olimpdialog, {        
        _create: function () {
            var self = this;

            if (!this.options.buttons) {
                this.options.buttons = {}
            }

            if (!this.options.buttons[this.options.saveText]) {
                this.options.buttons[this.options.saveText] = function() {
                    var form = self.element.find('form');

                    if ($.isFunction(form.valid) && form.valid() !== true)
                        return false;

                    self.element.data('in-progress', true)
                        .parent().find('.olimp-dialog-buttonpane button')
                        .olimpbutton('option', 'disabled', true);

                    form.submit();
                };
            } else {
                var func = this.options.buttons[this.options.saveText];

                this.options.buttons[this.options.saveText] = function() {
                    func();
                    self.close();
                }
            }

            this.options.buttons[this.options.cancelText] = function () { self.close(); }
            
            return $.ui.olimpdialog.prototype._create.apply(this, arguments);
        }
    });
    $.extend($.ui.olimpsyncformdialog.prototype.options, {
        modal: true,
        autoOpen: false,
        draggable: false,
        beforeClose: function () { return $(this).data('in-progress') !== true },
        saveText: 'Save',
        cancelText: 'Cancel'
    });

}(jQuery));

(function($, undefined) {

$.widget("ui.fixFloatingContainer", {
    _create: function() {
        var self = this,
            buttons = this.element.find('button'),
            form = buttons.parents('form');
            
        if (buttons.length && form.length) {
            form = form.eq(0);
            buttons.filter(':submit').click(function() { form.submit() })
        }
        
        $(window).resize(function() { 
            /*both needed by ie6*/
            self.element.css("bottom", -2);			
            self.element.css("bottom", -1);			
        })
    
        this.element.appendTo('body')						
            .append($('<iframe />')
                .attr({ frameborder: 0, scrolling: 'no', align: 'bottom', marginheight: 0, marginwidth: 0, src: 'javascript:"";' }))				
            .wrapInner($('<div/>'));
    }
});

})(jQuery);


(function($, undefined) {

var baseClasses = "olimp-button", hoverClass = "olimp-button-hover";

$.widget("ui.olimpmenu", {
    options: { moveStep: 10, moveDelay: 20 },

    _create: function() {
        var self = this,
            leftLink = $('<a href="#">&laquo;</a>')
                .css({ 'margin-left': '0', 'margin-right': '3px', 'padding-left': '10px', 'padding-right': '10px' })
                .click(function() { return false; })
                .mousedown(function() { self._moveBegin('left') })				
                .mouseup(function() { self._moveEnd() }),
            rightLink = $('<a href="#">&raquo;</a>')
                .css({ 'padding-left': '10px', 'padding-right': '10px' })
                .click(function() { return false; })
                .mousedown(function() { self._moveBegin('right') })
                .mouseup(function() { self._moveEnd() });
    
        $.extend(this, {
            _leftContainer: $('<td />')
                .css({ 'margin': '0', 'padding': '0', 'display': 'none' })
                .append(leftLink)
                .width(leftLink.outerWidth(true)),
            _bodyContainer: $('<div />').css({ 'width': '100%', 'height': '100%', 'position': 'relative' }),
            _rightContainer: $('<td />')
                .css({ 'margin': '0', 'padding': '0', 'display': 'none' })
                .append(rightLink)
                .width(rightLink.outerWidth(true))
        });
    
        var tabs = this.element.find('.tabs'),
            wrap = tabs.find('.wrap'),			
            tabsTable = $('<table />').css({ 'border-collapse': 'collapse', 'width': '100%'}),
            tabsTableRow = $('<tr />')
                .append(this._leftContainer)
                .append($('<td />')
                    .css({ 'margin': '0', 'padding': '0', 'vertical-align': 'top', 'width': '100%' })
                    .append(this._bodyContainer))
                .append(this._rightContainer)
                .appendTo(tabsTable),			
            links = wrap.children('a').detach().css({ 'float': 'none', 'white-space': 'nowrap' }),	
            linksTable = $('<table />')
                .css({ 'border-collapse': 'collapse', 'float': 'right'})
                .appendTo(this._bodyContainer)
                .wrap('<div style="overflow: hidden; position: absolute; width: 100%; top: 0; left: 0;"/>'),
            linksTableRow = $('<tr />').appendTo(linksTable);
                                            
        links.each(function() { 
            $('<td/>').css({ 'margin': '0', 'padding': '0', 'vertical-align': 'top' })
                .append(this)
                .prependTo(linksTableRow);
        })	
        wrap.replaceWith(tabsTable);

        $.extend(this, {
            _bodyContainer: linksTable.parent(),
            _minWidth: linksTable.outerWidth(),
            _bodyContent: linksTable
        })
        
        this._firstLink = this._bodyContainer.find('a:first');
        this._lastCell = this._bodyContainer.find('a:last').addClass('last-link').parent();
        
        $(window).bind('resize.olimpmenu', function() { setTimeout(function() { self._resize() }, 0) });
        
        this._resize();
    },
    
    _moveBegin: function(direction) {
        if (this._moveInterval)
            return;
                
        if (direction === 'left' && this._bodyContent.css('float') != 'none') {
            var active = this._bodyContainer.find('.active:first');				
            if (active.hasClass('last-link') ) {
                this._bodyContent.css('float', 'none');
            
                var activeOffset = active.offset(),
                    firstOffset = this._firstLink.offset();
            
                this._bodyContainer.scrollLeft(activeOffset.left - firstOffset.left);
            }
        }
        
        var self = this,
            step = this.options.moveStep;
        
        if (direction === 'left')
            step *= (-1);
    
        this._moveInterval = setInterval(function() { 
            self._bodyContainer.scrollLeft(self._bodyContainer.scrollLeft() + step);			
        }, this.options.moveDelay)	
    },
        
    _moveEnd: function() { 
        clearInterval(this._moveInterval);
        this._moveInterval = null;
    },
    
    _resize: function() {			
        if (this._bodyContainer.outerWidth() < this._minWidth) {
            var active = this._bodyContainer.find('.active:first');
            
            if ($.browser.msie && $.browser.msie <= 6)
                this._leftContainer.add(this._rightContainer).css('display', 'block');
            else
                this._leftContainer.add(this._rightContainer).css('display', 'table-cell');
            
            if ($.browser.mozilla)
                this._lastCell.css('padding-right', '1px');
            
            if (active.hasClass('last-link')) {
                this._bodyContent.css('float', 'right');
            } else {
                this._bodyContent.css('float', 'none');
                
                var activeOffset = active.offset(),
                    firstOffset = this._firstLink.offset();
            
                this._bodyContainer.scrollLeft(activeOffset.left - firstOffset.left);
            }		
        } else {
            this._bodyContent.css('float', 'right');
            this._leftContainer.add(this._rightContainer).css('display', 'none');
            if ($.browser.mozilla)
                this._lastCell.css('padding-right', '0');
        }		
    },
    
    destroy: function() {		
        $.Widget.prototype.destroy.call(this);
    }
});

})(jQuery);

(function ($) {
    function _getObservable(ctx, dataBind, observableName) {
        if (!dataBind || !dataBind.length || !ctx)
            return null;
                    
        var reResult = (new RegExp(observableName + '\s*:\s*([^,]+)', 'm')).exec(dataBind) || [],					
            optionsBinding = reResult[1];
                        
        if (!optionsBinding)
            return null;
                        
        return eval('ctx.' + optionsBinding + ' || ' + 'window.' + optionsBinding);			
    };

    $.widget("ui.combobox", {
        options: { minLength: 0, delay: 400, dropDown: true },
        
        _comboBoxVisible: false,
        
        _create: function() {		
            var self = this,
                win = $(window),
                table = this.table = $('<table class="olimp-combobox" />')
                    .appendTo(this.element.parent()),
                tr = $('<tr />').appendTo(table),
                left = $('<td class="first" />').appendTo(tr),
                right = $('<td class="last" />').appendTo(tr);

            if (self.options.dropDown) {
                right.addClass('olimp-combobox-button-wrap')
                    .click(function() { self._buttonClick() })
                
                $('<div class="icon icon-down olimp-combobox-button"></div>').appendTo(right)
            }

            var dataBind = this.element.attr('data-bind'),
                ctx,
                valueObservable;
                    
            if (dataBind && dataBind.length && (ctx = ko.dataFor(this.element[0])))
                valueObservable = _getObservable(ctx, dataBind, 'value');
                    
            this._isSelect = this.element.is('select');				
            if (this._isSelect) {				
                this.options.delay = 0;				
                this.element.hide();
                
                this._populateOptionsFromSelect();						
                                                
                if (ctx) {
                    var optionsObservable = _getObservable(ctx, dataBind, 'options'),
                        valueObservable = _getObservable(ctx, dataBind, 'value');
                    
                    if (ko.isObservable(valueObservable)) {
                        this._koValueBindingObservable = valueObservable;						
                        this._koValueBindingSubscription = valueObservable.subscribe(function() {
                            self.textBox.val(self.element.find('option:selected').text());
                        })
                    }
                        
                    if (optionsObservable && ko.isObservable(optionsObservable)) {						
                        this._koOptionsBindingSubscription = optionsObservable.subscribe(function() {
                            self._populateOptionsFromSelect();							
                        })
                    }
                }
            } else if (ko.isObservable(valueObservable)) {
                this._koValueBindingObservable = valueObservable;			    
            }
                                        
            var textBox = this.textBox = this.element.is(':text')
                ? this.element
                : $('<input type="text" readonly="readonly" />')
                    .css('cursor', 'pointer')
                    .val(this._selectedOption || '')
                    .click(function() { self._buttonClick() }),
                dropDown = this.dropDown = textBox.appendTo(left)
                    .autocomplete(this.options)
                    .data('autocomplete')
                    .menu.element.css('border', 'solid 1px #c9ccd6');
                    
            self._moveToTextBoxFirstLetter();

            textBox.bind('autocompleteclose', function () { self._comboBoxVisible = false; })
                .bind('autocompleteopen', function (ev) { 
                    self._comboBoxVisible = true; 
                    
                    var offset = table.offset(),												
                        scrollTop = win.scrollTop(),
                        dropDownHeight = dropDown.outerWidth(table.outerWidth())
                            .outerHeight(),
                        downTop = offset.top + table.outerHeight(),
                        upTop = offset.top - dropDownHeight,
                        top;
                    
                    if ((downTop + dropDownHeight) >= (win.height() + scrollTop) && upTop >= scrollTop) {
                        top = upTop;
                        dropDown.css({ 'border-top': 'solid 1px #c9ccd6', 'border-bottom': 0 })
                    } else {
                        top = downTop;
                        dropDown.css({ 'border-bottom': 'solid 1px #c9ccd6', 'border-top': 0 })
                    }
                    
                    dropDown.offset({ left: offset.left, top: top });
                    
                    if (self._isSelect) {
                        var selectedIndex = 0;
                        for (var i = 0; i < self._source.length; i++) {
                            if (self._source[i].option.selected) {
                                selectedIndex = i;
                                break;
                            }
                        }
                        
                        dropDown.menu('activate', ev, dropDown.find('.ui-menu-item:eq(' + selectedIndex + ')'))
                        
                        //textBox.focus();						
                    }
                });	
            
            var ac = self.textBox.data('autocomplete'),
                dd = self.dropDown.data('menu');

            if (this._isSelect) {
                textBox.autocomplete('option', 'source', function(request, response) { response(self._source) })
                    .bind('autocompleteselect', function(ev, ui) { 
                        if (!ui.item.observableValue) {
                            var oldVal = self.element.val(ui.item.option.value);
                            ui.item.option.selected = true; 
                        
                            if (oldVal !== self.element.val())
                                self.element.trigger('change', ev)
                        } else {
                            self._koValueBindingObservable(ui.item.observableValue)
                        }

                        self._isDirty = true;
                    })		
                    .bind('keydown', function(event) {
                        if (self.options.disabled)
                            return;
                
                        var keyCode = $.ui.keyCode;
                        switch(event.keyCode) {
                            case keyCode.PAGE_UP: ac['_move']('previousPage', event); break;
                            case keyCode.PAGE_DOWN: ac['_move']('nextPage', event); break;
                            case keyCode.UP: ac['_keyEvent']('previous', event); break;
                            case keyCode.DOWN: ac['_keyEvent']( "next", event ); break;
                            case keyCode.ENTER:
                            case keyCode.NUMPAD_ENTER:							
                                if (dd.active)
                                    event.preventDefault();
                            case keyCode.TAB:
                                if (dd.active)
                                    dd.select(event);
                                break;
                            case keyCode.ESCAPE:
                                self.textBox.val(ac['term']);
                                self._moveToTextBoxFirstLetter();
                                ac['close'](event);
                                break;					
                        }
                    })
                    .focus(function () { self._moveToTextBoxFirstLetter(); })
                    .blur(function () { self.textBox.val(ac['term']); })
                    
                ac['term'] = textBox.val()
            } else {
                textBox.bind('autocompleteselect', function(ev, ui) {
                    ui.item.value && self._koValueBindingObservable != null && self._koValueBindingObservable(ui.item.value);
                });
            }
        },

        _moveToTextBoxFirstLetter: function () {
            if (!this.textBox)
                return;

            var tb = this.textBox[0];

            if (tb.createTextRange) {
                var part = tb.createTextRange();
                part.move("character", 0);
                part.select();
            } else if (tb.setSelectionRange) {
                tb.setSelectionRange(0, 0);
            }
        },
                
        _createOption: function (option, push) {
            var o = $(option),
                text = o.text(),
                item = { label: text, value: text, option: option };

            if (this._koValueBindingObservable)
                item.observableValue = ko.utils.domData.get(option, '__ko.optionValueDomData__');

            push = push || function (a, i) { a.push(i) };
            push(this._source, item);

            return item;
        },

        _addOption: function (item, add, push) {
            if (!this._isSelect)
                return false;

            add = add || function(e, o) { o.appendTo(e) } 

            var option = $('<option />').attr('value', item.value)
                .text(item.label);
                
            add(this.element, option);

            this._createOption(option[0], push);

            return true;
        },

        select: function (id) {
            if (!this._isSelect)
                return false;

            if (id !== null)
                this.element.val(id);

            var text = this.element.find('option:selected').text();

            this.textBox.val(text);
            this.textBox.data('autocomplete')['term'] = text;

            this._selectedOption = text;

            this.element.trigger('change');
        },

        appendOption: function (item) { this._addOption(item) },

        prependOption: function (item) {
            this._addOption(item, function (e, o) { o.prependTo(e) }, function (a, i) { a.splice(0, 0, i) })
        },

        removeOption: function (option) {
            if (!this._isSelect)
                return false;

            if (typeof option === 'number' || typeof option === 'string')
                option = this.element.find('option[value="' + option + '"]')[0]

            for (var i = 0; i < this._source.length; i++) {
                if (this._source[i].option == option) {
                    $(option).remove();
                    this._source.splice(i, 1);
                    break;
                }
            }

            this.select(null);
        },

        _populateOptionsFromSelect: function(setSelected) {
            var self = this;
        
            this._source = [];
            this.element.find('option').each(function() {
                var item = self._createOption(this);
                
                if (this.selected)
                    self._selectedOption = item.label;
            });
            
            if (!this._selectedOption) {
                var first = this._source[0];
                if (first)
                    this._selectedOption = first.label;
            }
            
            this.textBox && (this.textBox.data('autocomplete')['term'] = this._selectedOption);
        },
        
        _buttonClick: function () {
            if (!this._comboBoxVisible) {
                if (this.element.prop('disabled'))
                    return;

                this.textBox.focus();
                this.textBox.autocomplete('search', '');				
            } else {
                this.textBox.autocomplete('close');
            }
        },

        _setOption: function (key, value) {
            $.Widget.prototype._setOption.apply(this, arguments);
            if (key === "disabled") {
                if (value) {
                    this.element.propAttr("disabled", true);
                    this.textBox.autocomplete('close');
                } else {
                    this.element.propAttr("disabled", false);
                }

                return;
            }		   
        },
        
        destroy: function() {
            if (this.element.is(':text')) {			
                this.table.parent().append(this.element);		
                this.textBox.autocomplete('destroy');
            } else if (this.element.is('select')) {
                this.element.show();
            }
            this.table.remove();    
            
            if (this._koOptionsBindingSubscription)
                this._koOptionsBindingSubscription.dispose();		
                
            if (this._koValueBindingSubscription)
                this._koValueBindingSubscription.dispose();
            
            this.element.unbind('.combobox');
            
            $.Widget.prototype.destroy.call(this);
        }
    });

    $.widget("ui.multiselect", {
        options: {
            autoFocus: true,
            minLength: 0,
            delay: 400,
            createOnEnter: true,
            url: "",
            separator: ",",
            menuSourceComparer: null,
            data: null,
            validateAndSendToServerWhenIsDisabled: false
        },

        _comboBoxVisible: false,
        _removeItem: null,
        _source: [],
        _selectedOptions: [],
        _isSelect: false,
        _fieldname: "",
        _textOfSelect: "Выберите элемент",
        _textNoItems: "Нет элементов",
        _addingTextOfDefaultItem: " - создать элемент",
        _addingTip: "Введите текст и нажмите Enter.",

        _create: function() {
            var self = this,
                win = $(window),
                table = this.table = $('<table class="olimp-combobox" />')
                    .appendTo(this.element.parent()),
                tr = $('<tr />').appendTo(table),
                left = $('<td class="first" />').appendTo(tr),
                right = $('<td class="last olimp-combobox-button-wrap" />')
                    .appendTo(tr)
                    .click(function() { self._buttonClick(); }),
                ulContainer = $('<div class="olimp-multiselect-list-container"></div>');

            if (self.options.createOnEnter) {
                var addButton = $('<td class="out olimp-combobox-button-wrap" />')
                    .appendTo(tr)
                    .click(function () { self._addItemPromt(); });
                $('<div class="icon icon-add olimp-combobox-button"></div>').appendTo(addButton);
            }
            
            this._selectedList = $('<ul class="olimp-multiselect-list" />')
                .appendTo(ulContainer);
            ulContainer.appendTo(this.element.parent());
            $('<div class="icon icon-down olimp-combobox-button"></div>').appendTo(right);

            this._isSelect = this.element.is('select');
            this.element.hide();
            this._fieldName = this.element.prop("name");
            if (this._isSelect) {
                if (self.options.data) 
                    this._createDOMForSelectFromArray(self.options.data);

                this.options.delay = 0;
                this._setSelectedOptionsFromSelect();
            } else {
                this._setSelectedOptionsFromInput();
            }
            this._createListOfSelected();

            this.inputTip = $('<div class="olimp-multiselect-input-tip" />')
                    .text(this._addingTip).appendTo(left);
            var textBox = this.textBox = this.element.is(':text')
                    ? $('<input type="text"/>')
                    .css('cursor', 'pointer')
                    : $('<input type="text" readonly="readonly" />')
                    .css('cursor', 'pointer')
                    .val(this._getInputTextWhenSelect())
                    .click(function () { self._buttonClick(); })
                    ,
                dropDown = this.dropDown = textBox.appendTo(left)
                    .autocomplete(this.options)
                    .data('autocomplete')
                    .menu.element.css('border', 'solid 1px #c9ccd6');

            textBox.bind('autocompleteclose', function() {
                self._comboBoxVisible = false;
                if (self._isSelect) 
                    self.textBox.val(self._getInputTextWhenSelect());
            }).bind('autocompleteopen', function() {
                self._comboBoxVisible = true;
                var offset = table.offset(),
                    scrollTop = win.scrollTop(),
                    dropDownHeight = dropDown
                        .outerWidth(table.outerWidth())
                        .outerHeight(),
                    downTop = offset.top + table.outerHeight(),
                    upTop = offset.top - dropDownHeight,
                    top;

                if ((downTop + dropDownHeight) >= (win.height() + scrollTop) && upTop >= scrollTop) {
                    top = upTop;
                    dropDown.css({ 'border-top': 'solid 1px #c9ccd6', 'border-bottom': 0 });
                } else {
                    top = downTop;
                    dropDown.css({ 'border-bottom': 'solid 1px #c9ccd6', 'border-top': 0 });
                }

                dropDown.offset({ left: offset.left, top: top });
            });

            if (this._isSelect) {
                textBox.autocomplete('option', 'source', self._source)
                    .bind('autocompleteselect', function(ev, ui) {
                        self.createListElementAndRemoveFromSource(ui.item);
                        return false;
                    }).bind('focus', function () { return false; });

                self.textBox.data('autocomplete')['term'] = textBox.val();
            } else {
                textBox.autocomplete('option', 'source', function (request, response) {
                    if (self.options.url) {
                        $.post(self.options.url, { filter: request.term, selectedItems: self.getInputValueAsText() }, function(resposeData) {
                            var modifiedResposeData = self._addDefaultItemToResponse(resposeData);
                            response(modifiedResposeData);
                        });
                    } else if (self.options.data) {
                        var re = $.ui.autocomplete.escapeRegex(request.term);
                        var matcher = new RegExp("^" + re + ".*", "i");
                        var data = $.grep(self.options.data, function (item) {
                            return matcher.test(item.label) && self._checkNewItem(item.optionValue);
                        });
                        var modifiedData = self._addDefaultItemToResponse(data);
                        response(modifiedData);
                    }
                }).bind('autocompleteselect', function(ev, ui) {
                    self.createSelectedListElementForInput(ui.item);
                    ev.preventDefault();
                    return false;
                }).bind('keydown', function(event) {
                    if (event.keyCode == $.ui.keyCode.ENTER) {
                        event.preventDefault();
                        return false;
                    }
                    self.inputTip.hide();
                    return true;
                }).blur(function () {
                    self.inputTip.hide();
                });
                self.textBox.data('autocomplete')['term'] = textBox.val();
            }
            
            // Override _renderItem()
            var autocompleteInstance = self.textBox.data('autocomplete');
            autocompleteInstance._renderItem = self._renderMenuItem;
            
            this._multiselectElementsChanged();

            if (this.element.propAttr('disabled'))
                this._setOption('disabled', true);
        },
        _createDOMForSelectFromArray: function (array) {
            for (var i = 0; i < array.length; ++i) {
                var e = array[i];
                var dom = $('<option value="' + e.Value + '">' + e.Text + '</option>');
                dom.attr('selected', e.IsSelected);
                this.element.append(dom);
            }
        },
        _addItemPromt: function () {
            var leftPadding = 5,
                topPadding = 2;
            this.inputTip.css({ left: this.textBox.position().left + leftPadding, top: this.textBox.position().top + topPadding })
                .width(this.textBox.width())
                .height(this.textBox.height());
            this.textBox.val("").focus();
            this.inputTip.show();
        },
        // input
        getInputValueAsText: function() { return this.element.val(); },
        _getInputValueAsArray: function() {
            if (this.element.val() == "") {
                return [];
            }
            return this.element.val().split(this.options.separator);
        },
        _checkNewItem: function(text) {
            if (text == "")
                return false;
            var lowCaseElements = this.getInputValueAsText().toLowerCase().split(this.options.separator);
            return ($.inArray(text.toLowerCase(), lowCaseElements) == -1);
        },
        _setInputValue: function(text) {
            if (typeof text === 'string') {
                this.element.val(text);
            } else {
                var result = "";
                if (text.length == 1) {
                    result = text[0];
                } else if (text.length > 1) {
                    result = text.join(this.options.separator);
                }
                this.element.val(result);
            }
        },
        _setSelectedOptionsFromInput: function() {
            var self = this;
            this._selectedOptions = [];
            var selectedOptions = this._getInputValueAsArray();
            $.each(selectedOptions, function(index, value) {
                var item = { label: value, optionValue: value };
                self._selectedOptions.push(item);
            });
        },
        _createSelectedListElementForInput: function(item) {
            var self = this;
            var html = $('<li />').attr("data-value", item.optionValue),
                leftDiv = $('<div />').appendTo(html);

            $('<div />').text(item.label).appendTo(html);
            $('<a />').addClass('icon icon-small-delete action')
                .attr({ href: '#', title: "Удалить" })
                .click(function () {
                    if (self.textBox.prop("disabled"))
                        return false;
                    self.deleteListElementForInput(item);
                    return false;
                })
                .appendTo(leftDiv);

            html.prependTo(this._selectedList);
            this._multiselectElementsChanged();
        },
        _removeFromSelected: function(removeItem) {
            var item;
            var key;
            for (key = 0; key < this._selectedOptions.length; key += 1) {
                item = this._selectedOptions[key];
                if (item.optionValue == removeItem.optionValue) {
                    this._selectedOptions = this._selectedOptions.slice(0, parseInt(key)).concat(this._selectedOptions.slice(parseInt(key) + 1));
                    break;
                }
            }
            var selectedOptions = this._getInputValueAsArray();
            for (key = 0; key < selectedOptions.length; key += 1) {
                item = selectedOptions[key];
                if (item == removeItem.optionValue) {
                    selectedOptions = selectedOptions.slice(0, parseInt(key)).concat(selectedOptions.slice(parseInt(key) + 1));
                    break;
                }
            }
            this._setInputValue(selectedOptions);
            this._multiselectElementsChanged();
        },
        _addToSelectedOptions: function(item) {
            this._selectedOptions.push(item);
            var selectedOptions = this._getInputValueAsArray();
            selectedOptions.push(item.optionValue);
            this._setInputValue(selectedOptions);
        },
        //select
        _getInputTextWhenSelect: function() {
            var text;
            if (this._source.length > 0) {
                text = this._textOfSelect;
            } else {
                text = this._textNoItems;
            }
            return text;
        },
        _removeFromSource: function (removeItem) {
            for (var key = 0; key < this._source.length; key += 1){
                var item = this._source[key];
                if (item.optionValue == removeItem.optionValue) {
                    this._source = this._source.slice(0, parseInt(key)).concat(this._source.slice(parseInt(key) + 1));
                    break;
                }
            }
            return this._source;
        },
        _sortSourceByLabel: function () {
            var self = this,
                comparer = self.options.menuSourceComparer ? self.options.menuSourceComparer : function (a, b) {
                    return a.label.localeCompare(b.label);
                }

            self._source.sort(comparer);
        },
        _addToSource: function(item) {
            this._source.push(item);
            this._sortSourceByLabel();
            this.textBox.val(this._getInputTextWhenSelect());
            return null;
        },
        _createSelectedListElementForSelect: function (item) {
            var self = this,
                html = $('<li />').attr("data-value", item.optionValue),
                leftDiv = $('<div />').appendTo(html),
                option = self._getElementOptionByValue(item.optionValue);
            if (option.length == 0 || self._getListElementByDataValue(item.optionValue).length > 0) {
                return false;
            }
            option.prop("selected", true);
            self.element.triggerHandler('change');
            $('<div />').text(item.label).appendTo(html);

            $('<a />').addClass('icon icon-small-delete action')
                .attr({ href: '#', title: 'Удалить' })
                .click(function () {
                    if (self.textBox.prop("disabled")) {
                        return false;
                    }
                    self.deleteListElementForSelect(item);
                    return false;
                })
                .appendTo(leftDiv);

            html.prependTo(this._selectedList);
            this._multiselectElementsChanged();

            return false;
        },
        _getElementOptionByValue: function(value) {
            var self = this;
            return self.element.find('option[value="' + value + '"]');
        },
        _setSelectedOptionsFromSelect: function() {
            var self = this;
            this._selectedOptions = [];
            this._source = [];
            this.element.find('option').each(function() {
                var o = $(this),
                    item = { label: o.text(), optionValue: o.val() };

                if (this.selected) {
                    self._selectedOptions.push(item);
                } else {
                    self._source.push(item);
                }
            });
        },
        //both
        _renderMenuItem: function (ul, item) {
            var label = item.label;
            if ('autocompleteMenuLabel' in item)
                label = item.autocompleteMenuLabel;
            return $( "<li>" )
              .data("item.autocomplete", item)
              .append($("<a>").html(label))
              .appendTo( ul );
        },
        _addDefaultItemToResponse: function (response) {
            response = [].concat( response );
            var text = this.textBox.val();
            if (this.options.createOnEnter && this._checkNewItem(text)){ 
                var add = $("<span>")
                        .addClass("olimp-multiselect-menu-add-text")
                        .text(this._addingTextOfDefaultItem)[0].outerHTML;
                var autocompleteMenuLabel = text + " " + add;

                if (response.length >= 1 && response[0].label == text) {
                    return response;
                } else {
                    response.unshift({ label: text, optionValue: text, autocompleteMenuLabel: autocompleteMenuLabel });
                }           
            }
            return response;
        },
        _createListOfSelected: function () {
            this._selectedList.empty();
            for (var key = 0; key < this._selectedOptions.length; key += 1){
                var item = this._selectedOptions[key];
                if (this._isSelect) {
                    this._createSelectedListElementForSelect(item);
                } else {
                    this._createSelectedListElementForInput(item);
                }
            }
        },
        _buttonClick: function () {
            if (!this._comboBoxVisible) {
                if (this.textBox.prop('disabled'))
                    return;

                this.textBox.focus();
                this.textBox.autocomplete('search', '');
            } else {
                this.textBox.autocomplete('close');
            }
        },
        _getListElementByDataValue: function (optionValue) {
            var self = this;
            return self.element.parent().find(".olimp-multiselect-list>li[data-value='" + optionValue + "']");
        },
        _multiselectElementsChanged: function () {
            var listElementsCount = this._selectedList.children().length;
            this._selectedList.trigger('multiselect-elements-changed', listElementsCount);
        },
        clear: function () {
            this.element.val("");
            this.element.triggerHandler('change');
            this._selectedOptions = [];
            this._selectedList.empty();
            this._multiselectElementsChanged();
        },
        removeAllSelectedOptions: function () {
            var self = this,
                options = self.element.children(":selected");
            $.each(options, function (i, element) {
                var option = $(element);
                self.deleteListElementForSelect({
                    label: option.text(),
                    optionValue: option.val()
                });
            });
        },
        getElementOptionTextByValue: function (value) {
            return this._getElementOptionByValue(value).text();
        },
        deleteListElementForSelect: function (item) {
            var self = this,
                option = self._getElementOptionByValue(item.optionValue);
            if (option.length == 0 || !option.prop("selected")) {
                return false;
            }
            self._getListElementByDataValue(item.optionValue).remove();
            option.prop("selected",false);
            self.element.triggerHandler('change');
            self._addToSource(item);
            self._multiselectElementsChanged();
            return false;
        },
        deleteListElementForInput: function (item) {
            var self = this;
            self._getListElementByDataValue(item.optionValue).remove();
            self._removeFromSelected(item);
            self.element.triggerHandler('change');
            self._multiselectElementsChanged();
            return false;
        },
        getAllSelectedElementsAsPlainTextWithSeporators: function () {
            var self = this,
                plainText = '',
                selectedElements = self.element.parent().find('.olimp-multiselect-list>li');

            for (var i = 0; i < selectedElements.length; i++) {
                plainText = plainText + $(selectedElements[i]).text();

                if (i + 1 != selectedElements.length) {
                    plainText = plainText + self.options.separator;
                }
            }

            return plainText;
        },
        createListElementAndRemoveFromSource: function (item) {
            var self = this;

            self._createSelectedListElementForSelect(item);
            self.textBox.autocomplete('option', 'source', self._removeFromSource(item));

            return false;
        },
        createSelectedListElementForInput: function (item) {
            var self = this;

            self._addToSelectedOptions(item);
            self._createSelectedListElementForInput(item);

            return false;
        },
        destroy: function () {
            if (this.element.is(':text')) {
                this.table.parent().append(this.element);
            } else if (this.element.is('select')) {
                this.element.children().remove();
                $('.olimp-multiselect-list-container').remove();
                this.element.removeProp("disabled");
            }
            this.element.show();
            this.textBox.autocomplete('destroy');
            this._selectedOptions = [];
            this._source = [];
            this.table.remove();
            this._selectedList.remove();

            this.element.unbind('.combobox');

            $.Widget.prototype.destroy.call(this);
        },
        _setOption: function (key, value) {
            $.Widget.prototype._setOption.apply(this, arguments);
            if (key === "disabled") {
                if (value) {
                    this.textBox.propAttr("disabled", true);
                    this.textBox.val("");
                    this._selectedList.addClass("disabled-list");
                    if (!this.options.validateAndSendToServerWhenIsDisabled) {
                        this.element.propAttr("disabled", true);
                    }

                    this.textBox.autocomplete('close');
                } else {
                    this.textBox.propAttr("disabled", false);
                    this._selectedList.removeClass("disabled-list");

                    if (!this.options.validateAndSendToServerWhenIsDisabled) {
                        this.element.propAttr("disabled", false);
                    }
                }

                return;
            }
        }
    });
    $.fn.multiselect.initValidation = function () {
        if ($.validator != undefined)
            $.validator.setDefaults({ ignore: ":disabled" });
    };
})(jQuery);

//Keyboard
(function ($, undefined) {

    $.widget("ui.olimpkeyboard", {
        options: {
            locales: ['ru', 'en']
        },

        _buttons: {},

        _input: null,
        _lastCaretPos: 0,

        _localeIndex: 0,

        _localizedValAttrName: function () { return "data-kb-val-" + this.options.locales[this._localeIndex]; },

        _create: function () {
            var self = this,
                localizedValAttrName = self._localizedValAttrName();

            this._input = $('body')
                .find(':text').click(function () {
                    self._input = $(this);
                    self._parseRestriction();
                })
                .filter(':focus')

            $(document).click(function (e) {
                var target = $(e.target);
                if (!target.is(':text') && !target.closest(self.element).length) {
                    self._input = null;
                    self._lastCaretPos = 0;
                    self._parseRestriction();
                } else if (self._input && self._input.length) {
                    self._input[0].focus();
                    if (!target.is(':text'))
                        self._moveCaret(self._lastCaretPos);
                    else
                        self._lastCaretPos = self._caretPosition()
                }
            })

            this.element.find("button").each(function () {
                var btn = $(this),
                    klass = btn.attr("data-kb-class");

                if (!klass || !klass.length) {
                    btn.remove();
                    return;
                }

                if (klass == "chars" || klass == "numbers") {
                    var val = btn.attr("data-kb-val");
                    if (!val || !val.length) {
                        btn.remove();
                        return;
                    }

                    btn.text(val);
                } else if (klass == "letters") {
                    var val = btn.attr(localizedValAttrName);
                    if (!val || !val.length)
                        btn.html('&nbsp;').olimpbutton('option', 'disabled', true)
                    else
                        btn.text(val);
                }

                if (!self._buttons[klass]) {
                    if (klass != "system")
                        self._buttons[klass] = [];
                    else
                        self._buttons[klass] = {};
                }

                if (klass != "system") {
                    self._buttons[klass].push(btn);
                } else {
                    var subClass = btn.attr("data-kb-subclass");
                    if (!subClass || !subClass.length) {
                        btn.remove();
                        return;
                    }

                    if (!self._buttons[klass][subClass])
                        self._buttons[klass][subClass] = [];

                    self._buttons[klass][subClass].push(btn);
                }

                btn.click(function (ev) { return self._click(btn); })
            });
        },

        _buttonAllowed: function (btn, parts) {
            if (parts.length == 0)
                return true;

            var klass = btn.attr("data-kb-class"),
                subClass = btn.attr("data-kb-subclass");

            for (var i = 0; i < parts.length; i++) {
                var part = parts[i],
                    vals = part.indexOf("'") == 0 ? [part] : part.split('.');

                if (vals.length == 1) {
                    if (klass == "chars" && part.indexOf("'") == 0 && part[part.length - 1] == "'") {
                        part = part.substring(1, part.length - 1);

                        if (btn.attr('data-kb-val') == part)
                            return true;
                    } else if (klass == part) {
                        return true;
                    }
                } else if (vals.length == 2) {
                    var val = vals[1];

                    if (val[val.length - 1] == '*') {
                        val = val.substring(0, val.length - 1);

                        if (!subClass)
                            continue;

                        if (klass == vals[0] && subClass.indexOf(val) == 0)
                            return true;
                    } else {
                        if (klass == vals[0] && subClass == vals[1])
                            return true;
                    }
                }
            }

            return false;
        },

        _parseRestriction: function () {
            var self = this,
                allow = self._input ? self._input.attr('data-kb-allow') : null;
            parts = allow && allow.length ? allow.split(',') : [];

            if (this._buttons.system) {
                for (var k in this._buttons.system) {
                    $.each(this._buttons.system[k], function (i, v) {
                        v.olimpbutton('option', 'disabled', !self._buttonAllowed(v, parts))
                    })
                }
            }

            for (var k in this._buttons) {
                if (k == "system")
                    continue;

                $.each(this._buttons[k], function (i, v) {
                    v.olimpbutton('option', 'disabled', !self._buttonAllowed(v, parts))
                })
            }
        },

        _caretPosition: function () {
            var pos = 0;

            if (document.selection) {
                this._input[0].focus();
                var sel = document.selection.createRange();
                sel.moveStart('character', -this._input.val().length);

                pos = sel.text.length;
            }
            else if (this._input[0].selectionStart || this._input[0].selectionStart == '0') {
                pos = this._input[0].selectionStart;
            }

            return pos;
        },

        _moveCaret: function (pos) {
            if (this._input[0].createTextRange) {
                var range = this._input[0].createTextRange();
                range.move('character', pos);
                range.select();
            }
            else {
                if (this._input[0].setSelectionRange) {
                    this._input[0].focus();
                    this._input[0].setSelectionRange(pos, pos);
                }
                else {
                    this._input[0].focus();
                }
            }
        },

        _isShifted: false,
        _shift: function () {
            var self = this;

            $.each(self._buttons.letters, function (index, value) {
                var text = value.text();
                value.text(self._isShifted ? text.toLowerCase() : text.toUpperCase())
            })

            self._isShifted = !self._isShifted;
        },

        _lang: function () {
            var self = this,
                allow = self._input ? self._input.attr('data-kb-allow') : null;
            parts = allow && allow.length ? allow.split(',') : [];

            if (self._localeIndex + 1 < self.options.locales.length)
                self._localeIndex++;
            else
                self._localeIndex = 0;

            var attrName = self._localizedValAttrName();

            $.each(self._buttons.letters, function (index, value) {
                var text = value.attr(attrName);

                if (text && text.length)
                    value.text(text).olimpbutton('option', 'disabled', false) //!self._buttonAllowed(value, parts)
                else
                    value.html('&nbsp;').olimpbutton('option', 'disabled', true)
            })
        },

        _click: function (btn) {
            var klass = btn.attr("data-kb-class"),
                subClass = btn.attr("data-kb-subclass");

            if (!this._input) {
                if (klass != 'system' || (subClass != 'shift' && subClass != 'lang'))
                    return;
            }

            var localizedValAttrName = this._localizedValAttrName(),
                caret = !this._input ? 0 : this._caretPosition(),
                val = !this._input ? '' : this._input.val(),
                add = null;

            this._lastCaretPos = caret;

            if (klass == "letters")
                add = btn.attr(localizedValAttrName);
            else if (klass == "chars" || klass == "numbers")
                add = btn.attr("data-kb-val");

            if (add && add.length) {
                if (this._isShifted) {
                    add = add.toUpperCase();
                    this._shift();
                }

                this._input.val(val.substring(0, caret) + add + val.substring(caret));
                caret++;
            } else if (klass == "system") {
                if (subClass == "erase-backspace") {
                    this._input.val(val.substring(0, caret - 1) + val.substring(caret));
                    this._moveCaret(--caret);
                }
                else if (subClass == "erase-delete") {
                    this._input.val(val.substring(0, caret) + val.substring(caret + 1));
                    this._moveCaret(caret);
                }
                else if (subClass == "navigate-left")
                    this._moveCaret(--caret);
                else if (subClass == "navigate-right")
                    this._moveCaret(++caret);
                else if (subClass == "navigate-home") {
                    this._moveCaret(0);
                    caret = 0;
                } else if (subClass == "navigate-end") {
                    this._moveCaret(val.length);
                    caret = val.length;
                } else if (subClass == "shift")
                    this._shift();
                else if (subClass == "lang")
                    this._lang();
            }

            this._lastCaretPos = caret;
        },

        destroy: function () {
            $.Widget.prototype.destroy.call(this);
        }
    });

})(jQuery);

(function ($) {
    $.widget("ui.olimpcheckradio", {
        __cbUncheckedClass: 'olimp-checkbox-unchecked',
        __cbCheckedClass: 'olimp-checkbox-checked',
        __radioUncheckedClass: 'olimp-radio-unchecked',
        __radioCheckedClass: 'olimp-radio-checked',

        options: { clickable: true },

        _create: function () {
            var self = this;

            this._isRadio = this.element.is(':radio');
            this._name = this.element.attr('name');

            this._checker = $('<div />').addClass("olimpcheckradio").addClass(this._getClass())
                .click(function () { self._onClick() });

            this.element.hide().after(this._checker);
        },

        _uncheckRadio: function () {
            this._checker.removeClass(this.__radioCheckedClass).addClass(this.__radioUncheckedClass)
            this.element.trigger('checkedchanged', [false]);
        },

        _checkRadio: function () {
            this._checker.removeClass(this.__radioUncheckedClass).addClass(this.__radioCheckedClass)
        },

        setChecked: function (value) {
            if (value === this.element.prop('checked'))
                return true;

            this.element.prop('checked', value);
            if (this._isRadio) {
                if (!value) {
                    this._uncheckRadio();
                } else {
                    this._checkRadio();
                    $(':radio[name=' + this._name + ']').not(this.element).each(function () {
                        var widget = $(this).data('olimpcheckradio');
                        widget && widget._uncheckRadio();
                    })
                }
            } else {
                this._checker.removeClass(this.__cbCheckedClass).removeClass(this.__cbUncheckedClass);
                if (value)
                    this._checker.addClass(this.__cbCheckedClass);
                else
                    this._checker.addClass(this.__cbUncheckedClass);
            }

            this.element.trigger('checkedchanged', [value])
        },

        emulateClick: function () {
            if (this._isRadio)
                this.setChecked(true);
            else
                this.setChecked(!this.element.prop('checked'));
        },

        _onClick: function () {
            if (!this.options.clickable)
                return false;

            this.emulateClick();
        },

        _getClass: function () {
            return !this._isRadio
                ? (this.element.prop('checked') ? this.__cbCheckedClass : this.__cbUncheckedClass)
                : (this.element.prop('checked') ? this.__radioCheckedClass : this.__radioUncheckedClass)
        },

        destroy: function () {
            $.Widget.prototype.destroy.call(this);
        }
    });
})(jQuery);

(function ($) {
    $.widget("ui.collapsibleBlock", {
        options: {
            initialyExpanded: false,
            headerText: ""
        },
        _contentExpanded: false,
        _create: function () {
            var self = this,
                wraper = this.wraper = $('<div class="collapsible-block" />')
                .prependTo(this.element.parent()),
                header = this.header = $('<div class="collapsible-block-header" />')
                    .appendTo(wraper)
                    .click(function () { self._headerClick(); }),
                content = this.content = this.element.appendTo(wraper).addClass("collapsible-block-content"),
                icon = this.icon = $('<div />').addClass("icon").addClass(this.options.initialyExpanded ? "icon-filter-hide" : "icon-filter-show"),
                headerText = $('<div />').addClass("collapsible-block-header-text").text(this.options.headerText);
            
            $('<table />')
                .append($('<tr />')
                    .append($('<td />')
                        .append(icon))
                    .append($('<td />')
                        .append(headerText)))
                .appendTo(header);
            if (!this.options.initialyExpanded)
                content.hide();
            this._contentExpanded = this.options.initialyExpanded;
        },
        _headerClick: function () {
            if (!this._contentExpanded) {
                this.content.show();
                this.icon.removeClass("icon-filter-show");
                this.icon.addClass("icon-filter-hide");
            } else {
                this.content.hide();
                this.icon.removeClass("icon-filter-hide");
                this.icon.addClass("icon-filter-show");
            }
            this._contentExpanded = !this._contentExpanded;
        },
        destroy: function () {
            this.element.appendTo(this.wraper.parent());
            this.element.removeClass("collapsible-block-content");
            wraper.remove();
            $.Widget.prototype.destroy.call(this);
        }
    });
})(jQuery);