using log4net;
using Olimp.Controllers;
using Olimp.Core;
using Olimp.Core.Activation;
using Olimp.Core.Mvc;
using Olimp.Core.Mvc.Security;
using Olimp.Core.Security;
using Olimp.Core.Web;
using Olimp.Courses.Facades.Course;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.Ldap;
using Olimp.Domain.Catalogue.Normatives;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Common;
using Olimp.Domain.Common.Nodes;
using Olimp.Ldap;
using Olimp.Reporting;
using Olimp.Service.Contract.EmployeeService.Exists;
using Olimp.Service.Contract.EmployeeService.Position;
using Olimp.Service.Contract.KnowledgeControl.Journal;
using Olimp.Service.Contract.KnowledgeControl.Schedule.FilterSchedule;
using Olimp.Service.Contract.KnowledgeControl.Schedule.ForEmployee;
using Olimp.Service.Contract.KnowledgeControl.Shift;
using Olimp.Service.Contract.PingService;
using Olimp.Service.Contract.ReportingService;
using Olimp.Service.Contract.SyncService.Attempts;
using Olimp.Service.Contract.SyncService.Employee;
using Olimp.Service.Host;
using Olimp.Service.Implementation;
using Olimp.Web.Controllers.Security;
using Olimp.Web.Controllers.Users;
using PAPI.Core;
using PAPI.Core.Logging;
using PAPI.Core.ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Olimp
{
    public class MvcApplication : OlimpMvcApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("!/{*path}");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{fileName}.ico");
            routes.IgnoreRoute("{fileName}.js");
            routes.IgnoreRoute("{fileName}.css");
            routes.IgnoreRoute("{fileName}.gif");
            routes.IgnoreRoute("{fileName}.jpg");
            routes.IgnoreRoute("{fileName}.png");
            routes.IgnoreRoute("{fileName}.htc");
            routes.IgnoreRoute("api/{*pathInfo}");
        }

        private void Application_Error(object sender, EventArgs e)
        {
            if (Log.IsEnabled)
            {
                OlimpApplication.Logger.ErrorFormat("Application error. {0} {1};\n{2}",
                  this.Request.HttpMethod, this.Request.Url, this.Server.GetLastError());
            }
        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleSecurityExceptionAttribute { View = "Error", Order = 200 });
            filters.Add(new HandleVistaDbConcurrencyErrorAttribute { View = "Error", Order = 200 });
            filters.Add(new HandleCannotDeleteUserException { View = "Error", Order = 200 });
            filters.Add(new OlimpDefaultHandleErrorAttribute { View = "Error", Order = 300 });
            filters.Add(new SetWorkplaceTokenAttribute());
        }

        protected override void OnApplicationStart()
        {
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
            //HtmlHelper.ClientValidationEnabled = true;

            RegisterGlobalFilters(GlobalFilters.Filters);
            ConfigureDependencies(Container.Instance);
            RegisterRoutes(RouteTable.Routes);

            AreasTable.Areas["*"].RegisterControllers(typeof(ErrorController));

            var olimpModule = PContext.ExecutingModule as OlimpModule;
            if (olimpModule != null)
            {
                olimpModule.Plugins.RegisterRoutes(AreasTable.Areas, RouteTable.Routes);

                var keyInfo = olimpModule.Activation.GetActivationKeyInfo();
                if (keyInfo != null && keyInfo.Flags.HasFlag(OlimpActivationKeyFlags.IsTrial))
                    SystemTimer.Start(new TimeSpan(0, 1, 0));

                var logger = Log.CreateLogger("api");
                try
                {
                    StartApiService(olimpModule, logger);
                }
                catch (Exception ee)
                {
                    logger.ErrorFormat("Unable to start api service:\n{0}", ee);
                }
            }
        }

        private static void MergeDependenciesFromFunqContainer(Funq.Container container)
        {
            PContext.ExecutingModule.Kernel.Binder
                .Bind<ICurrentFormsAuthProvider>(container)

                .Bind<IModelBinderProvider, OlimpModelBinderProvider>()
                .Bind<IReportEngine>(() => container.Resolve<IReportEngine>())

                //Olimp.Normatives Plugin
                .Bind<IWholeCatalogueProviderFactory>(container)
                .Bind<INormativeRepository>(container)
                .Bind<INormativeContentsProvider>(container)
                .Bind<INormativeQuestionsProvider>(container)
                .Bind<INormativeExcerptRepository>(container)
                .Bind<INormativeExcerptTopicRepository>(container)
                .Bind<INormativeExcerptTreeProvider>(container)
                .Bind<INormativeExcerptRepository>(container)
                .Bind<INormativeExcerptTopicRepository>(container)
                .Bind<INormativeExcerptTopicQuestionRepositoryFactory>(container)
                .Bind<IGeneratedFromNormativesBranchRepository>(container)
                .Bind<IProfessionRepository>(container)
                .Bind<INormativeExcerptSerializer>(container)
                .Bind<INormativeExcerptImporter>(container)
                .Bind<IExcerptCatalogueExporter>(container);
        }

        public static void ConfigureDependencies(Funq.Container container)
        {
            MergeDependenciesFromFunqContainer(container);

            AppWebServiceHost.AppContainer = container;

            Olimp.Reporting.Dependencies.CourseFacadeRepository = container
                .Resolve<Olimp.Core.Model.Repositories.IMaterialRepository<ICourseFacade>>();
            Olimp.Reporting.Dependencies.CourseRepository = container.Resolve<ICourseRepository>();
        }

        private void StartApiService(OlimpModule module, ILog log)
        {
            ServiceStack.Logging.LogManager.LogFactory = new SingletonLogFactory(log);
            var apiHost = new AppWebServiceHost("OlimpWebService", typeof(PingService).Assembly);
#if DEBUG
            apiHost.Plugins.Add(new ServiceStack.ServiceInterface.Admin.RequestLogsFeature
              {
                  AtRestPath = "/debug/log",
                  EnableResponseTracking =
                    String.Equals("true", WebConfigurationManager.AppSettings["ApiMonitoringEnableResponseTracking"], StringComparison.InvariantCultureIgnoreCase),
                  EnableRequestBodyTracking = true,
                  RequiredRoles = new string[0]
              });
#endif

            apiHost.RegisterRoutes(new Dictionary<Type, RestBinding>
      {
        { typeof(SyncFinishedAttempts), new RestBinding("/sync/attempts") },
        { typeof(SyncEmployee), new RestBinding("/sync/employee") },
        { typeof(PingRequest), new RestBinding("/ping") },
        { typeof(AddEmployeePosition), new RestBinding("/employees/position/add", "POST") },
        { typeof(RemoveEmployeePositions), new RestBinding("/employees/position/remove", "POST") },
        { typeof(EmployeeExists), new RestBinding("/employees/exists/{EmployeeId}", "GET") },
        { typeof(ShiftProfileById), new RestBinding("/knowledge-control/shift/profile") },
        { typeof(ShiftProfilesByPosition), new RestBinding("/knowledge-control/shift/position") },
        { typeof(ShiftProfileResultById), new RestBinding("/knowledge-control/shift/profile-result") },
        { typeof(GetKnowledgeControlForEmployees), new RestBinding("/knowledge-control/employees/schedule-for", "POST") },
        { typeof(FilterSchedule), new RestBinding("/knowledge-control/schedule/filter") },
        { typeof(CountKnowledgeControlForEmployees), new RestBinding("/knowledge-control/employees/count-schedule-for") },
        { typeof(JournalData), new RestBinding("/knowledge-control/journal/filter") },
        { typeof(DeleteFromJournal), new RestBinding("/knowledge-control/journal/delete") },
        { typeof(GenerateReport), new RestBinding("/reports/generate") },
        { typeof(CountFilterSchedule), new RestBinding("/knowledge-control/schedule/count-filter") }
      });
            apiHost.Init();
        }

        private static readonly string[] _ignoredByCookieProcessingUrlPrefixes = new[]
    {
        "!/watchdog"
    };

        private static readonly object _yesObject = new Object();
        private static readonly object _noObject = new Object();

        protected virtual bool ShouldProcessAuthCookies()
        {
            const string contextKeyName = "Olimp::MvcApplication::ShouldProcessAuthCookies";

            var shouldProcess = Context.Items[contextKeyName];
            if (shouldProcess == null)
            {
                var prefix = HostingEnvironment.ApplicationVirtualPath;
                if (!prefix.EndsWith("/"))
                    prefix += "/";

                var pathAndQuety = Context.Request.Url.PathAndQuery;

                var shouldIgnore = _ignoredByCookieProcessingUrlPrefixes
                    .Select(p => prefix + p)
                    .Any(p => pathAndQuety.StartsWith(p));

                Context.Items[contextKeyName] = shouldProcess =
                    (!shouldIgnore ? _yesObject : _noObject);
            }

            return shouldProcess == _yesObject;
        }

        protected override void OnApplicationBeginRequest()
        {
            if (ShouldProcessAuthCookies())
                ProcessInputNodeAuthCookie(Context, GetCurrentNodeNamePrefix());
        }

        protected override void OnApplicationPreSendRequestHeaders()
        {
            if (ShouldProcessAuthCookies())
                ProcessOutputNodeAuthCookie(Context, GetCurrentNodeNamePrefix());
        }

        private static void ProcessInputNodeAuthCookie(HttpContext context, string nodeName)
        {
            if (context == null)
                throw new ArgumentNullException("context");
            if (nodeName == null)
                throw new ArgumentNullException("nodeName");

            string nodeAuthCookieName = GetNodeAuthCookieName(nodeName);
            string nodeRolesCookieName = GetNodeRolesCookieName(nodeName);

            var cookies = context.Request.Cookies;

            LoggingHelper.LogDebugWithCurrentMethodFullName(OlimpApplication.Logger, "Checking input cookies");

            // ��������������� ����
            RenameCookies(nodeAuthCookieName, FormsAuthentication.FormsCookieName, cookies, TransformInputCookiePath);

            // ��������������� ����
            RenameCookies(nodeRolesCookieName, Roles.CookieName, cookies, TransformInputCookiePath);
        }

        private static void TransformInputCookiePath(HttpCookie cookie)
        {
            if (HostingEnvironment.ApplicationVirtualPath == "/")
                return;

            if (cookie.Path.StartsWith(HostingEnvironment.ApplicationVirtualPath))
                cookie.Path = cookie.Path.Substring(HostingEnvironment.ApplicationVirtualPath.Length);
        }

        private static void TransformOutputCookiePath(HttpCookie cookie)
        {
            if (HostingEnvironment.ApplicationVirtualPath == "/")
                return;

            if (cookie.Path == "/")
                cookie.Path = HostingEnvironment.ApplicationVirtualPath;
            else
                cookie.Path = String.Join("/", HostingEnvironment.ApplicationVirtualPath.TrimEnd('/'), cookie.Path.TrimStart('/'));
        }

        private static void ProcessOutputNodeAuthCookie(HttpContext context, string nodeName)
        {
            if (context == null)
                throw new ArgumentNullException("context");
            if (nodeName == null)
                throw new ArgumentNullException("nodeName");

            string nodeAuthCookieName = GetNodeAuthCookieName(nodeName);
            string nodeRolesCookieName = GetNodeRolesCookieName(nodeName);

            var cookies = context.Response.Cookies;

            LoggingHelper.LogDebugWithCurrentMethodFullName(OlimpApplication.Logger, "Checking output cookies");

            // ��������������� ����
            RenameCookies(FormsAuthentication.FormsCookieName, nodeAuthCookieName, cookies, TransformOutputCookiePath);

            // ��������������� ����
            RenameCookies(Roles.CookieName, nodeRolesCookieName, cookies, TransformOutputCookiePath);

            // ������� �������� ����(����� �� ��������� ������, ��������� � ������������� ������ XSP ��-�� �������� �����
            // ��� "���������������" � ������ ����� login'�� �� ������ ����, ��� ���������������� logout'� �� �������)
            foreach (var cookie in context.Request.Cookies.AllKeys)
                TryDeleteAdminAuthCookie(context.Response, cookie);
        }

        #region Helper Methods

        private static string GetCurrentNodeNamePrefix()
        {
            if (PContext.ExecutingModule == null || PContext.ExecutingModule.Context.NodeData == null)
                return String.Empty;

            return Node.IsDefaultNodeId(PContext.ExecutingModule.Context.NodeData.Id)
                ? String.Empty
                : PContext.ExecutingModule.Context.NodeData.Name;
        }

        private static string GetNodeAuthCookieName(string nodeName)
        {
            if (nodeName == null)
                throw new ArgumentNullException("nodeName");

            string authCookieName = String.Format("{0}{1}", nodeName, FormsAuthentication.FormsCookieName);

            return authCookieName;
        }

        private static string GetNodeRolesCookieName(string nodeName)
        {
            if (nodeName == null)
                throw new ArgumentNullException("nodeName");

            string rolesCookieName = String.Format("{0}{1}", nodeName, Roles.CookieName);

            return rolesCookieName;
        }

        private static void RemoveCookies(string name, HttpCookieCollection cookies)
        {
            while (cookies.AllKeys.Contains(name))
                cookies.Remove(name);
        }

        private static void RenameCookies(string oldName, string newName, HttpCookieCollection cookies, Action<HttpCookie> transformCookie)
        {
            LoggingHelper.LogDebugFormatWithCurrentMethodFullName(OlimpApplication.Logger, "Checking cookie {0} among {1}", oldName, String.Join(", ", cookies.AllKeys));

            List<HttpCookie> cookiesToUpdate = new List<HttpCookie>();
            int count = cookies.Count;
            for (int i = 0; i < count; i++)
            {
                var cookie = cookies[i];
                if (cookie.Name == oldName)
                    cookiesToUpdate.Add(cookie);
            }

            for (int i = 0; i < cookiesToUpdate.Count; i++)
                cookies.Remove(cookiesToUpdate[i].Name);

            // ������� ���� � ������, ������� �� ����� ���� (����� ����� ��������� � ����� ������, ��� ��������� ������).
            RemoveCookies(newName, cookies);

            for (int i = 0; i < cookiesToUpdate.Count; i++)
            {
                var cookie = cookiesToUpdate[i];
                LoggingHelper.LogDebugFormatWithCurrentMethodFullName(OlimpApplication.Logger, "Rename cookie: {0} -> {1}", cookie.Name, newName);
                cookie.Name = newName;

                if (transformCookie != null)
                    transformCookie.Invoke(cookie);

                cookies.Add(cookie);
            }
        }

        private static void TryDeleteAdminAuthCookie(HttpResponse responce, string cookieName)
        {
            const string adminCookiesPath = "/Admin";
            const string workplaceTokenCookieName = "WorkplaceToken";

            if (cookieName == workplaceTokenCookieName)
                return;

            var currentNodeData = PContext.ExecutingModule.Context.NodeData;
            if (NodeData.IsDefaultNode(currentNodeData))
            {
                if (cookieName == FormsAuthentication.FormsCookieName || cookieName == Roles.CookieName)
                    return;
            }
            else
            {
                if (cookieName.Contains(currentNodeData.Name))
                    return;
            }

            responce.SetCookie(new HttpCookie(cookieName)
            {
                Expires = DateTime.Now.AddDays(-1),
                Path = adminCookiesPath
            });
        }

        #endregion Helper Methods
    }
}