using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Olimp.ICSStaffSyncInfo.ViewModels
{
    public class InfoViewModel
    {
        public IList<SelectListItem> AllLogs { get; set; }

        public string CurrentLog { get; set; }

        public string CurrentErrorLog { get; set; }

        public bool HasErrors { get; set; }

        public DateTime CurrentDate { get; set; }

        public string SyncCommands { get; set; }
    }
}