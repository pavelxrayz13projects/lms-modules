using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.ICSStaffSyncInfo.Controllers;
using PAPI.Core;
using PAPI.Core.Initialization;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.ICSStaffSyncInfo.Plugin))]

namespace Olimp.ICSStaffSyncInfo
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(typeof(ICSStaffSyncController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
        }
    }
}