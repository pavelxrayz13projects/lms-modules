﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<InfoViewModel>"  MasterPageFile="~/Views/Shared/ServerManagement.master" %>

<asp:Content ContentPlaceHolderID="Olimp_ServerManagement_Content" runat="server">
    <h1>Команда синхронизации персонала для данного узла (<a href="#" class="link-button" id="sync-link">скрыть</a>)</h1>    
    <textarea id="sync" style="width:100%; height: 20px;"
        readonly="readonly"><% = Model.SyncCommands %>
    </textarea>

    <h1>Протоколы синхронизации</h1>
	<% using (Html.BeginForm("Info", "ICSStaffSync", FormMethod.Post))
	   { %>
		<div class="block">
			<table class="form-table" id="sysinfo">
				<tr class="first">	 
					<th>Выберите протокол:</th>
					<td><%= Html.DropDownListFor(m => m.CurrentLog, Model.AllLogs, new {id = "current-log"}) %></td>
				</tr>
				<tr class="last">
					<td colspan="2">
						<button type="button" id="delete">Удалить</button>
						<button type="button" id="delete-all">Удалить все</button>
					</td>
				</tr>
			</table>
		</div>		
		
		<button id="get-form-submitter" type="submit" style="display: none;"></button>
	<% } %>
	
	<% using (Html.BeginForm("Delete", "ICSStaffSync", FormMethod.Post))
	   { %>		
		<%= Html.HiddenFor(m => m.CurrentLog, new {id = "delete-name"}) %>
		<button id="delete-form-submitter" type="submit" style="display: none;"></button>
	<% } %>
	
	<% using (Html.BeginForm("DeleteAll", "ICSStaffSync", FormMethod.Post))
	   { %>		
		<button id="delete-all-form-submitter" type="submit" style="display: none;"></button>
	<% } %>

	<% if (!String.IsNullOrWhiteSpace(Model.CurrentLog))
	   { %>
		<h1>Протокол за <%= Model.CurrentDate.ToShortDateString() %> (<a href="#" class="link-button" id="current-link">скрыть</a>)</h1>
		<div class="block" id="current">
			<% Html.RenderAction("Log", new {name = Model.CurrentLog}); %>
		</div>

		<% if (Model.HasErrors)
		   { %>	
			<div id="err-wrapper">
				<h1>Ошибки синхронизации (<a href="#" class="link-button" id="err-link">скрыть</a>)</h1>
				<div class="block" id="err">
					<% Html.RenderAction("Log", new {name = Model.CurrentErrorLog}); %>
				</div>
			</div>
		<% } %>
	<% } %>
	
	<script type="text/javascript">
		$(function() {
		    $('#current-link, #err-link, #sync-link').click(function () {
		        var self = $(this),
		            block = $('#' + self.attr('id').split('-')[0]);

		        if (block.is(':visible')) {
		            $(this).text('показать');
		            block.hide();
		        } else {
		            $(this).text('скрыть');
		            block.show();
		        }

		        return false;
		    });
		
			<% if (Model.HasErrors)
			   { %>
			if ($('#current textarea').length > 0)
				$('#current-link').click();
			else
				$('#err-wrapper').hide();
			<% } %>
		
			$('#current-log').change(function() { $('#get-form-submitter').click(); });
		    $('#delete').olimpbutton().click(function() {
		        $('#delete-name').val($('#current-log').val());
		        $('#delete-form-submitter').click();
		    });
			$('#delete-all').olimpbutton().click(function() { $('#delete-all-form-submitter').click(); });
		})
	</script>
</asp:Content>