using System.Collections.Generic;
using Olimp.Core;
using Olimp.Domain.Catalogue.Security;
using Olimp.ICSStaffSyncInfo.Properties;
using Olimp.ICSStaffSyncInfo.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.ICSStaffSyncInfo.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Admin.Backup.CreateLocal + "," + Permissions.Admin.Backup.RestoreLocal, Order = 2)]
    public class ICSStaffSyncController : PController
    {
        private static readonly string NodeLogsPath =
            new PlatformPaths(PlatformPaths.Default.Root, Settings.Default.DbfImportModuleName,
                PContext.ExecutingModule.Context.NodeData.Name).Logs;

        private static IList<SelectListItem> GetLogFileItems(string path)
        {
            return Directory.GetFiles(path, "icsstaff.dbfimport.log*", SearchOption.TopDirectoryOnly)
                   .Select(f => new
                   {
                       Name = Path.GetFileName(f),
                       Date = System.IO.File.GetLastWriteTime(f).Date,
                       ErrName =
                           Path.Combine(NodeLogsPath,
                               Path.GetFileNameWithoutExtension(f) + ".err" + Path.GetExtension(f))
                   })
                   .OrderByDescending(o => o.Date)
                   .Select(o => System.IO.File.Exists(o.ErrName) && new FileInfo(o.ErrName).Length > 0
                       ? new SelectListItem
                       {
                           Text = o.Date.ToShortDateString() + I18N.Names.ErrorsExist,
                           Value = String.Join("|", o.Name, Path.GetFileName(o.ErrName))
                       }
                       : new SelectListItem { Text = o.Date.ToShortDateString(), Value = o.Name })
                   .ToList();
        }

        public ActionResult Info(string currentLog)
        {
            IList<SelectListItem> files;

            try
            {
                files = GetLogFileItems(NodeLogsPath);
            }
            catch (Exception e)
            {
                OlimpApplication.Logger.Error(e);
                files = Enumerable.Empty<SelectListItem>().ToList();
            }

            if (String.IsNullOrWhiteSpace(currentLog) ||
                !System.IO.File.Exists(Path.Combine(NodeLogsPath,
                    currentLog.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries)[0])))
                currentLog = files.Any() ? files.First().Value : null;

            var syncCommand = string.Format(
                Settings.Default.SyncCommandFormat, 
                PlatformPaths.Default.Root,
                PContext.ExecutingModule.Context.NodeData.Name, 
                PContext.ExecutingModule.Context.NodeData.Id);

            var model = new InfoViewModel
            {
                SyncCommands = syncCommand,
                AllLogs = files,
                CurrentLog = currentLog == null
                    ? null
                    : currentLog.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries)[0],
                HasErrors = currentLog != null && currentLog.IndexOf('|') != -1
            };

            if (model.HasErrors)
                model.CurrentErrorLog = currentLog.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries)[1];

            if (model.CurrentLog != null)
                model.CurrentDate = System.IO.File.GetLastWriteTime(
                    Path.Combine(NodeLogsPath, model.CurrentLog)).Date;

            return View(model);
        }

        public ActionResult Log(string name)
        {
            var fileName = Path.Combine(NodeLogsPath, name);
            if (!System.IO.File.Exists(fileName))
                return null;

            return new LogFileResult(fileName);
        }

        public ActionResult Delete(string currentLog)
        {
            var fileName = Path.Combine(NodeLogsPath, currentLog);
            if (System.IO.File.Exists(fileName))
                System.IO.File.Delete(fileName);

            var errFileName = Path.Combine(NodeLogsPath,
                Path.GetFileNameWithoutExtension(currentLog) + ".err" + Path.GetExtension(currentLog));
            if (System.IO.File.Exists(errFileName))
                System.IO.File.Delete(errFileName);

            return RedirectToAction("Info");
        }

        public ActionResult DeleteAll()
        {
            if (!Directory.Exists(NodeLogsPath))
                return null;

            var files = Directory.GetFiles(NodeLogsPath, "icsstaff.dbfimport.*",
                SearchOption.TopDirectoryOnly);
            foreach (var f in files)
                System.IO.File.Delete(f);

            return RedirectToAction("Info");
        }
    }
}