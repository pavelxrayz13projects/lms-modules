using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Olimp.ICSStaffSyncInfo
{
    public class LogFileResult : FileResult
    {
        private string _fileName;

        public LogFileResult(string fileName)
            : base("text/html")
        {
            _fileName = fileName;
        }

        #region implemented abstract members of System.Web.Mvc.FileResult

        protected override void WriteFile(HttpResponseBase response)
        {
            StreamReader reader;
            try
            {
                reader = new StreamReader(_fileName, Encoding.GetEncoding(1251));
            }
            catch (IOException)
            {
                response.Write(String.Format("<h1 style=\"color:#0068BD\">{0}</h1>", I18N.Names.AccessError));
                return;
            }

            response.Write("<textarea style=\"width:100%; height: 400px;\" readonly=\"readonly\">");
            using (reader)
            {
                while (reader.Peek() >= 0)
                {
                    response.Write(reader.ReadLine());
                    response.Write("\n");
                }
            }
            response.Write("</textarea>");
        }

        #endregion implemented abstract members of System.Web.Mvc.FileResult
    }
}