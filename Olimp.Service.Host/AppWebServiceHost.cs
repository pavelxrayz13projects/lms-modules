﻿using log4net;
using ServiceStack.ServiceHost;
using ServiceStack.Text;
using ServiceStack.WebHost.Endpoints;
using ServiceStack.WebHost.Endpoints.Extensions;
using System;
using System.Collections.Generic;
using System.Reflection;
using LogManager = PAPI.Core.Logging.Log;

namespace Olimp.Service.Host
{
    public class AppWebServiceHost : AppHostBase
    {
        private static readonly ILog _servicelog = LogManager.CreateLogger("api");

        private static ILog ServiceLog { get { return _servicelog; } }

        private static Funq.Container _container;

        public static Funq.Container AppContainer
        {
            get
            {
                if (_container == null)
                    _container = new Funq.Container();

                return _container;
            }
            set
            {
                if (value == null)
                    throw new InvalidOperationException("AppContainer can't be set to null");

                _container = value;
            }
        }

        private readonly IDictionary<Type, RestBinding> _routes = new Dictionary<Type, RestBinding>();

        //Tell Service Stack the name of your application and where to find your web services
        public AppWebServiceHost(string name, Assembly assembly)
            : base(name, assembly)
        { }

        public void RegisterRoutes(IDictionary<Type, RestBinding> routes)
        {
            if (routes == null)
                throw new ArgumentNullException("routes");

            foreach (var item in routes)
            {
                _routes.Add(item);
            }
        }

        protected override ServiceManager CreateServiceManager(params Assembly[] assembliesWithServices)
        {
            return new ServiceManager(AppContainer.CreateChildContainer(), assembliesWithServices);
        }

        public override void Configure(Funq.Container container)
        {
            foreach (var item in _routes)
            {
                Routes.Add(item.Key, item.Value.Uri, item.Value.Verbs);
            }

            SetExceptionHandlers();
        }

        private void SetExceptionHandlers()
        {
            // https://github.com/ServiceStack/ServiceStack/wiki/Error-Handling
            //Handle Exceptions occurring in Services:
            ServiceExceptionHandler = (request, exception) =>
            {
                if (ServiceLog.IsDebugEnabled && request != null)
                {
                    ServiceLog.Debug("Request that causes an error is: " + JsonSerializer.SerializeToString(request, request.GetType()));
                }
                //log your exceptions here
                ServiceLog.Error(exception.ToString());
                //call default exception handler or prepare your own custom response
                return DtoUtils.HandleException(this, request, exception);
            };

            //Handle Unhandled Exceptions occurring outside of Services
            //E.g. in Request binding or filters:
            ExceptionHandler = (req, res, operationName, ex) =>
            {
                // From original code:
                string errorMessage = string.Format("Error occured while Processing Request: {0}", ex.Message);
                int statusCode = ex.ToStatusCode();
                if (!res.IsClosed)
                {
                    res.WriteErrorToResponse(req, req.ResponseContentType, operationName, errorMessage, ex, statusCode);
                }

                ServiceLog.ErrorFormat("Error occured while processing operation {0}: {1}", operationName, ex.ToString());
            };
        }
    }
}