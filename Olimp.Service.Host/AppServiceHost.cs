﻿using ServiceStack.WebHost.Endpoints;
using System;
using System.Collections.Generic;

namespace Olimp.Service.Host
{
    public class AppServiceHost : AppHostHttpListenerLongRunningBase
    {
        private readonly IDictionary<Type, string> _routes = new Dictionary<Type, string>();

        public AppServiceHost(string serviceName, string handlerPath, params System.Reflection.Assembly[] serviceAssemblies)
            : base("SyncService", handlerPath, serviceAssemblies)
        {
        }

        public void RegisterRoutes(IDictionary<Type, string> routes)
        {
            if (routes == null)
                throw new ArgumentNullException("routes");

            foreach (var item in routes)
            {
                _routes.Add(item);
            }
        }

        public override void Configure(Funq.Container container)
        {
            foreach (var item in _routes)
            {
                Routes.Add(item.Key, item.Value, "POST");
            }
        }
    }
}