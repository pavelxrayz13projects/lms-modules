﻿namespace Olimp.Service.Host
{
    public class RestBinding
    {
        public RestBinding()
        { }

        public RestBinding(string uri)
            : this(uri, "POST")
        { }

        public RestBinding(string uri, string verbs)
        {
            this.Uri = uri;
            this.Verbs = verbs;
        }

        public string Uri { get; set; }

        public string Verbs { get; set; }
    }
}