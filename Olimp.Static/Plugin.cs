using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Courses.Facades.Course;
using Olimp.Courses.Facades.Normative;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Normatives;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Static.Controllers;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.Web.Routing;
using ModelRep = Olimp.Core.Model.Repositories;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Static.Plugin))]

namespace Olimp.Static
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["*"].RegisterControllers(typeof(StaticController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var materialsModule = PContext.ExecutingModule as OlimpMaterialsModule;
            if (materialsModule == null)
                throw new InvalidOperationException("Executing module must be OlimpMaterialsModule");

            var connFactoryFactory = new SqliteConnectionFactoryFactory();
            var connFactory = connFactoryFactory.Create();

            var nodesRepository = new OrmLiteNodeRepository(connFactory);

            var courseFacadeRepository = materialsModule.Materials.CourseRepository;
            var normativeFacadeRepository = materialsModule.Materials.NormativeRepository;

            var courseRepository = new OrmLiteCourseRepository(Context.NodeData, connFactory, courseFacadeRepository, nodesRepository);
            var normativeRepository = new OrmLiteNormativeRepository(Context.NodeData, connFactory, normativeFacadeRepository);

            binder.Controller<StaticController>(
                b => b.Bind<ICourseRepository>(() => courseRepository)
                      .Bind<INormativeRepository>(() => normativeRepository)
                      .Bind<ModelRep.IMaterialRepository<ICourseFacade>>(() => courseFacadeRepository)
                      .Bind<ModelRep.IMaterialRepository<INormativeFacade>>(() => normativeFacadeRepository));
        }
    }
}