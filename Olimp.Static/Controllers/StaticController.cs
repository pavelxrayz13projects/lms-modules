using Olimp.Configuration;
using Olimp.Courses.Caching;
using Olimp.Courses.Extensions;
using Olimp.Courses.Facades.Course;
using Olimp.Courses.Facades.Normative;
using Olimp.Courses.Model;
using Olimp.Courses.Unique;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Normatives;
using Olimp.Domain.Exam.NHibernate.Schema;
using Olimp.Web.Controllers.Base;
using PAPI.Core;
using PAPI.Core.Data;
using PAPI.Core.Web;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using ModelRep = Olimp.Core.Model.Repositories;

namespace Olimp.Static.Controllers
{
    public class StaticController : PController
    {
        private readonly ICourseRepository _courseRepository;
        private readonly INormativeRepository _normativeRepository;
        private readonly ModelRep.IMaterialRepository<ICourseFacade> _courseFacadeRepository;
        private readonly ModelRep.IMaterialRepository<INormativeFacade> _normativeFacadeRepository;

        private DateTime _lastWriteTime;

        static StaticController()
        {
        }

        public StaticController(
            ICourseRepository courseRepository,
            INormativeRepository normativeRepository,
            ModelRep.IMaterialRepository<ICourseFacade> courseFacadeRepository,
            ModelRep.IMaterialRepository<INormativeFacade> normativeFacadeRepository)
        {
            if (courseRepository == null)
                throw new ArgumentNullException("courseRepository");

            if (normativeRepository == null)
                throw new ArgumentNullException("normativeRepository");

            if (normativeRepository == null)
                throw new ArgumentNullException("normativeRepository");

            if (courseFacadeRepository == null)
                throw new ArgumentNullException("courseFacadeRepository");

            if (normativeFacadeRepository == null)
                throw new ArgumentNullException("normativeFacadeRepository");

            _courseRepository = courseRepository;
            _normativeRepository = normativeRepository;
            _courseFacadeRepository = courseFacadeRepository;
            _normativeFacadeRepository = normativeFacadeRepository;

            using (UnitOfWork.CreateRead())
            {
                var config = Config.Get<BaseConfiguration>();

                var lwt = config.LastUpdate;
                if (lwt == default(DateTime))
                    lwt = config.LastActivation;

                if (lwt == default(DateTime))
                {
                    var info = new FileInfo(Path.Combine(PlatformPaths.Default.Modules, "Olimp", "Olimp.edi"));
                    if (info.Exists)
                        lwt = info.LastWriteTime;
                }

                _lastWriteTime = new DateTime(lwt.Year, lwt.Month, lwt.Day, lwt.Hour, lwt.Minute, lwt.Second, 0);
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (filterContext.ActionDescriptor.ActionName.Equals("QuestionImage", StringComparison.OrdinalIgnoreCase))
                OnQuestionImageActionExecuting(filterContext);
            else if (filterContext.ActionDescriptor.ActionName.Equals("CachedImage", StringComparison.OrdinalIgnoreCase))
                OnCachedImageActionExecuting(filterContext);
        }

        [NonAction]
        private void OnQuestionImageActionExecuting(ActionExecutingContext filterContext)
        {
            var materialIdResult = ValueProvider.GetValue("materialId");
            var questionIdResult = ValueProvider.GetValue("questionId");
            var versionResult = ValueProvider.GetValue("version");
            var pathResult = ValueProvider.GetValue("path");

            Guid questionId;
            int version;

            if (questionIdResult == null ||
                versionResult == null ||
                !Guid.TryParse(questionIdResult.AttemptedValue, out questionId) ||
                !int.TryParse(versionResult.AttemptedValue, out version))
            {
                filterContext.Result = new HttpStatusCodeResult(400, "Bad request");
                return;
            }

            var materialId = materialIdResult != null
                ? materialIdResult.AttemptedValue
                : null;

            if (ObtainQuestion(materialId, questionId, version) == null)
            {
                filterContext.Result = new HttpStatusCodeResult(500, "Question not found");
                return;
            }

            var ifModifiedSince = Request.Headers["If-Modified-Since"];
            if (ifModifiedSince != null)
            {
                try
                {
                    var ifModifiedSinceTime = DateTime.ParseExact(ifModifiedSince, "r", null);
                    if (_lastWriteTime.ToUniversalTime() <= ifModifiedSinceTime)
                    {
                        Response.ContentType = pathResult != null
                            ? MIME.ByPath(pathResult.AttemptedValue)
                            : "image/jpeg";
                        filterContext.Result = new HttpStatusCodeResult(304);
                    }
                }
                catch { }
            }
        }

        [NonAction]
        private void OnCachedImageActionExecuting(ActionExecutingContext filterContext)
        {
            int imageId;
            var imageIdValue = ValueProvider.GetValue("imageId");
            if (imageIdValue == null || !int.TryParse(imageIdValue.AttemptedValue, out imageId))
            {
                filterContext.Result = new HttpStatusCodeResult(400, "Bad request");
                return;
            }

            //var image = ObtainCachedImage(imageId);

            var ifModifiedSince = Request.Headers["If-Modified-Since"];
            if (ifModifiedSince != null)
            {
                //Response.ContentType = image.Mime;
                filterContext.Result = new HttpStatusCodeResult(304);
            }
        }

        public ActionResult QuestionImage(string materialId, Guid questionId, int version, string path)
        {
            var question = ObtainQuestion(materialId, questionId, version);
            if (question == null) return new HttpStatusCodeResult(500, "Question not found");

            string base64Image;
            if (!question.Images.TryGetValue(path, out base64Image))
                return new HttpStatusCodeResult(500, "Image not found");

            var bytes = Convert.FromBase64String(base64Image);

            Response.Cache.SetLastModified(_lastWriteTime);
            Response.Cache.SetCacheability(HttpCacheability.Public);
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));

            return File(bytes, MIME.ByPath(path));
        }

        public ActionResult CachedImage(int imageId)
        {
            var image = ObtainCachedImage(imageId);

            Response.Cache.SetLastModified(_lastWriteTime);
            Response.Cache.SetCacheability(HttpCacheability.Public);
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));

            return File(image.Data, image.Mime);
        }

        [NonAction]
        private CachedImageSchema ObtainCachedImage(int id)
        {
            using (UnitOfWork.CreateRead())
            {
                var image = Repository.Of<CachedImageSchema, int>().GetById(id);
                if (image == null)
                    throw new HttpException(404, "Cached image was not found");

                return image;
            }
        }

        [NonAction]
        private IQuestion ObtainQuestion(string materialId, Guid questionId, int version)
        {
            var question = CoursesCache.ObtainObject(new UniqueObject(questionId, version)) as IQuestion;
            if (question != null)
                return question;

            if (string.IsNullOrEmpty(materialId))
                return null;

            var materialIdentityInfo = MaterialPrefix.TryParseMaterialIdentity(materialId);

            if (materialIdentityInfo == null ||
                !materialIdentityInfo.Id.HasValue ||
                materialIdentityInfo.Id.Value == 0 ||
                string.IsNullOrEmpty(materialIdentityInfo.Prefix))
            {
                throw new HttpException(400, "Incorrect question image URL");
            }

            if (materialIdentityInfo.Prefix == MaterialPrefix.Course)
                return GetCourseQuestion(materialIdentityInfo.Id.Value, questionId);

            if (materialIdentityInfo.Prefix == MaterialPrefix.Normative)
                return GetNormativeQuestion(materialIdentityInfo.Id.Value, questionId);

            throw new HttpException(400, "Incorrect question image URL");
        }

        [NonAction]
        private IQuestion GetCourseQuestion(int materialId, Guid questionId)
        {
            var cource = _courseRepository.GetByBranchId(materialId);
            if (cource == null) return null;
            var courceFacade = _courseFacadeRepository.Load(cource.Name);
            return courceFacade != null ? courceFacade.Questions.GetById(questionId) : null;
        }

        [NonAction]
        private IQuestion GetNormativeQuestion(int materialId, Guid questionId)
        {
            var normative = _normativeRepository.GetByBranchId(materialId);
            if (normative == null) return null;
            var normativeFacade = _normativeFacadeRepository.Load(normative.Name);
            return normativeFacade != null ? normativeFacade.Questions.GetById(questionId) : null;
        }
    }
}