using System.Web.Routing;
using Olimp.Core.Extensibility;
using Olimp.Core.Web;
using Olimp.UI;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using System;
using System.Web.Mvc;

namespace Olimp.Main.Controllers
{
    [AuthorizeForAdmin]
    public class SettingsController : PController
    {
        private IExtender _extender;

        public SettingsController(IExtender extender)
        {
            if (extender == null)
                throw new ArgumentNullException("extender");

            _extender = extender;
        }

        public ActionResult Index(string returnUrl)
        {
            if (!String.IsNullOrEmpty(returnUrl) && !returnUrl.Equals("/Admin"))
                return Redirect(returnUrl);

            var roles = RolesHelper.GetCurrentUserRoles(ControllerContext);
            var navigation = (Navigation)_extender.GetContainer("/admin/layout/navigation");

            var firstLink = navigation.GetFirstLink(roles);

            if (firstLink == null)
            {
                var routeDictionary = new RouteValueDictionary()
                {
                    { "ErrorMessage", I18N.Setttings.NoAccessToAdminSection }
                };
                return RedirectToRoute("AuthAdmin", routeDictionary);
            }
                

            var url = firstLink.GetUrl(this);

            return Redirect(url);
        }
    }
}