using Olimp.Web.Controllers.Base;
using System.Web.Mvc;

namespace Olimp.Main.Controllers
{
    public class ServiceController : PController
    {
        [HttpPost]
        public ActionResult GetState()
        {
            return Json(new
            {
                IsAlive = true,
            });
        }
    }
}