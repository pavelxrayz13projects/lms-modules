using Olimp.Core;
using Olimp.Core.Extensibility;
using Olimp.Core.Routing;
using Olimp.Main.Controllers;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.Web.Mvc;
using System.Web.Routing;

[assembly: PAPI.Core.Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Main.Plugin))]

namespace Olimp.Main
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        { }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["*"].RegisterControllers(typeof(ServiceController));
            areas["Admin"].RegisterControllers(typeof(SettingsController));

            areas["Prepare"].Context.MapRoute("Prepare_SelectCourse",
                "Prepare/Content/ShowThemes/{employeeId}/{materialId}",
                new { controller = "Content", action = "ShowThemes", employeeId = 0, materialId = "" });

            areas["Prepare"].Context.MapRoute("Prepare_ExamingRenderView",
                "Prepare/Examing/RenderView/{attemptId}/{materialId}",
                new { controller = "Examing", action = "RenderView", attemptId = "", materialId = "" });

            areas["Prepare"].Context.MapRoute("Prepare_ExamingResult",
                "Prepare/Examing/Result/{attemptId}/{materialId}",
                new { controller = "Examing", action = "Result", attemptId = "", materialId = "" });

            areas["Prepare"].Context.MapRoute("Prepare_Docs",
                "Prepare/Doc/{courseId}/{version}/{materialId}",
                new { controller = "Documents", action = "Index" });

            areas["Prepare"].Context.MapRoute("Prepare_Docs_Images",
                "Prepare/Doc/{courseId}/{version}/{materialId}/i/{*path}",
                new { controller = "Documents", action = "Image" });

            areas["Prepare"].Context.MapDefault(new { controller = "StHome", action = "Index", id = "" });
            areas["Exam"].Context.MapDefault(new { controller = "Home", action = "Index", id = "" });
            areas["Competition"].Context.MapDefault(new { controller = "Competition", action = "Splash", id = "" });

            routes.MapRoute("CachedImages",
                "CachedImages/{imageId}",
                new { area = "", controller = "Static", action = "CachedImage" });

            routes.MapRoute("QuestionImagesWithMaterialId",
                "QuestionImages/{materialId}/{questionId}/{version}/{*path}",
                new { area = "", controller = "Static", action = "QuestionImage" });

            routes.MapRoute("QuestionImages",
                "QuestionImages/{questionId}/{version}/{*path}",
                new { area = "", controller = "Static", action = "QuestionImage" });

            routes.MapRoute("NormativeImagesWithMaterialId",
                "NormativeImages/{materialId}/{questionId}/{version}/{*path}",
                new { area = "", controller = "Static", action = "QuestionImage" });

            routes.MapRoute("NormativeImages",
                "NormativeImages/{questionId}/{version}/{*path}",
                new { area = "", controller = "Static", action = "QuestionImage" });

            routes.MapRoute("Default_Admin_Page", "Admin/SystemUser/{action}/{id}", new { area = "Admin", controller = "SystemUser", action = "List", id = "" });
            routes.MapRoute("Default_Teacher_Page", "Admin/ManageProfile/{action}/{id}", new { area = "Admin", controller = "ManageProfile", action = "Index", id = "" });

            areas["Admin"].Context.MapDefault(new { controller = "Settings", action = "Index", id = "" });

            routes.MapRoute("AuthAdmin", "Auth/Admin", new { area = "", controller = "AuthAdmin", action = "Admin" });
            routes.MapRoute("AuthLogout", "Auth/Logout", new { area = "", controller = "AuthAdmin", action = "Logout" });
            routes.MapRoute("AuthUser", "Auth/{action}", new { area = "", controller = "AuthUser", action = "User" });

            routes.MapRoute("Default_FinishPreparation", "", new { area = "", controller = "Splash", action = "Index", id = "" });
            routes.MapDefault(new { controller = "Splash", action = "Index", id = "" });
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (this.Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var olimpModule = PContext.ExecutingModule as OlimpModule;
            if (olimpModule == null)
                throw new InvalidOperationException("Olimp.Main only compatible with OlimpModule");

            var extender = olimpModule.Plugins.GetExtender();

            binder.Controller<SettingsController>(b => b.Bind<IExtender>(() => extender));
        }
    }
}