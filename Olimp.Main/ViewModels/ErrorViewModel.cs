using System;

namespace Olimp.Main.ViewModels
{
    public class ErrorViewModel
    {
        public int Code { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public string ImageUrl { get; set; }

        public Exception Exception { get; set; }
    }
}