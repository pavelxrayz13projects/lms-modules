﻿using AutoMapper;
using Olimp.Domain.Common;
using Olimp.Domain.Exam.ProfileResults;
using System;
using AttemptContracts = Olimp.Service.Contract.SyncService.Attempts;
using AttemptEntities = Olimp.Domain.Exam.NHibernate.Schema;
using FilterScheduleContracts = Olimp.Service.Contract.KnowledgeControl.Schedule;
using FilterScheduleEntities = Olimp.Domain.Exam.Entities.Pseudo;
using Mapper = global::AutoMapper.Mapper;

namespace Olimp.Service.Implementation.EntityMapping
{
    public static class MapCreator
    {
        public static void CreateAttemptsMap()
        {
            Mapper.CreateMap<AttemptEntities.ExamAttemptSchema, AttemptContracts.Attempt>()
                .ForMember(a => a.AllowNavigateQuestions, opt => opt.MapFrom(a => a.Settings.AllowNavigateQuestions))
                .ForMember(a => a.ShuffleAnswers, opt => opt.MapFrom(a => a.Settings.ShuffleAnswers))
                .ForMember(a => a.TimeLimit, opt => opt.MapFrom(a => a.Settings.TimeLimit))
                .ForMember(a => a.RegistrationType, opt => opt.MapFrom(a => a.Settings.RegistrationType))
                .ForMember(a => a.LoginType, opt => opt.MapFrom(a => a.Settings.LoginType))
                .ForMember(a => a.TimeMode, opt => opt.MapFrom(a => a.Settings.TimeMode))
                .ForMember(a => a.Course, opt => opt.MapFrom(a => a.TestMapped))
                .ForMember(a => a.Ticket, opt => opt.MapFrom(a => a.TicketMapped))
                .ForMember(a => a.Tasks, opt => opt.MapFrom(a => a.TasksMapped))
                .ForMember(a => a.Group, opt => opt.MapFrom(a => a.GroupMapped))
                .ForMember(a => a.Employee, opt => opt.MapFrom(a => a.EmployeeMapped))
                .ForMember(a => a.ExamResponsible, opt => opt.ResolveUsing(a => Convert.ToString(a.ResponsiblePersoneMapped)));

            Mapper.CreateMap<AttemptEntities.CachedCourseSchema, AttemptContracts.Course>();

            Mapper.CreateMap<AttemptEntities.CachedGroupSchema, AttemptContracts.Group>();

            Mapper.CreateMap<AttemptEntities.CachedTicketSchema, AttemptContracts.Ticket>();

            Mapper.CreateMap<AttemptEntities.AttemptQuestionTaskSchema, AttemptContracts.QuestionTask>()
                .ForMember(t => t.Question, opt => opt.MapFrom(t => t.QuestionMapped))
                .ForMember(t => t.AsnwerTimestamp, opt => opt.MapFrom(t => t.AnswerTimestamp));

            Mapper.CreateMap<AttemptEntities.CachedQuestionSchema, AttemptContracts.Question>()
                .ForMember(t => t.Topic, opt => opt.MapFrom(t => t.TopicMapped))
                .ForMember(t => t.Answers, opt => opt.MapFrom(t => t.AnswersMapped));

            Mapper.CreateMap<AttemptEntities.CachedAnswerSchema, AttemptContracts.Answer>();

            Mapper.CreateMap<AttemptEntities.CachedTopicSchema, AttemptContracts.Topic>()
                .ForMember(t => t.Course, opt => opt.MapFrom(t => t.CourseMapped)); ;

            Mapper.CreateMap<AttemptEntities.CachedEmployeeSchema, AttemptContracts.Employee>()
                .ForMember(e => e.Positions, opt => opt.MapFrom(e => e.PositionsListMapped));

            Mapper.CreateMap<AttemptEntities.CachedEmployeePositionSchema, AttemptContracts.Position>();
        }

        public static void CreateFilterScheduleMap()
        {
            Mapper.CreateMap<FilterScheduleContracts.FilterSchedule.Filter, ProfileResultsFilter>()
                .ForMember(f => f.EmployeeFullName, opt => opt.MapFrom(e => e.EmployeeFullNameFilter))
                .ForMember(f => f.DateBegin, opt => opt.MapFrom(f => f.FromDate))
                .ForMember(f => f.DateEnd, opt => opt.MapFrom(f => f.ToDate))
                .ForMember(f => f.EmployeeIds, opt => opt.MapFrom(f => f.Employees))
                .ForMember(f => f.ProfileName, opt => opt.Ignore());

            Mapper.CreateMap<FilterScheduleContracts.FilterSchedule.FilterSchedule, PagingValues>()
                .ConstructUsing(ConstructFilterSchedulePaging)
                .ForAllMembers(opt => opt.Ignore());

            Mapper.CreateMap<FilterScheduleEntities.MonitoringProfileResult, Contract.KnowledgeControl.Employee>()
                .ForMember(e => e.FullName, opt => opt.MapFrom(r => r.EmployeeFullName))
                .ForMember(e => e.Id, opt => opt.MapFrom(r => r.EmployeeId));

            Mapper.CreateMap<FilterScheduleEntities.MonitoringProfileResult, FilterScheduleContracts.FilterSchedule.ScheduleRecord>()
                .ForMember(r => r.Employee, opt => opt.ResolveUsing(CreateEmployee))
                .ForMember(r => r.Profile, opt => opt.ResolveUsing(CreateProfile))
                .ForMember(r => r.PreviousDate, opt => opt.MapFrom(r => r.DatePassed))
                .ForMember(r => r.NextDate, opt => opt.MapFrom(r => r.NextDate));

            Mapper.CreateMap<FilterScheduleContracts.ForEmployee.GetKnowledgeControlForEmployees, ProfileResultsFilter>()
                .ForMember(e => e.EmployeeIds, opt => opt.MapFrom(e => e.Employees))
                .ForMember(f => f.DateBegin, opt => opt.MapFrom(f => f.FromDate))
                .ForMember(f => f.DateEnd, opt => opt.MapFrom(f => f.ToDate))
                .ForMember(f => f.EmployeeFullName, opt => opt.Ignore())
                .ForMember(f => f.ProfileName, opt => opt.Ignore())
                .ForMember(f => f.Positions, opt => opt.Ignore());
        }

        public static void CreateJournalMap()
        {
            Mapper.CreateMap<Contract.KnowledgeControl.Journal.Filter, ProfileResultsFilter>()
              .ForMember(f => f.DateBegin, opt => opt.MapFrom(f => f.FromDate))
              .ForMember(f => f.DateEnd, opt => opt.MapFrom(f => f.ToDate))
              .ForMember(f => f.EmployeeFullName, opt => opt.MapFrom(f => f.EmployeeFullNameFilter))
              .ForMember(f => f.EmployeeIds, opt => opt.MapFrom(f => f.Employees))
              .ForMember(f => f.ProfileName, opt => opt.Ignore());

            Mapper.CreateMap<Contract.KnowledgeControl.Journal.JournalData, PagingValues>()
              .ConstructUsing(ConstructJournalPaging)
              .ForAllMembers(opt => opt.Ignore()); ;

            Mapper.CreateMap<Olimp.Domain.Exam.Entities.Pseudo.ArchiveProfileResult, Contract.KnowledgeControl.Journal.JournalRecord>()
                .ForMember(r => r.Employee, opt => opt.ResolveUsing(CreateEmployee))
                .ForMember(r => r.KnowledgeControlTitle, opt => opt.MapFrom(r => r.ProfileName));
        }

        private static PagingValues ConstructFilterSchedulePaging(ResolutionContext ctx)
        {
            var src = (FilterScheduleContracts.FilterSchedule.FilterSchedule)ctx.SourceValue;
            var dst = new PagingValues();

            if (src.Page == null)
                return dst;

            dst.PageNumber = src.Page.PageNumber;
            dst.PageSize = src.Page.PageSize;
            dst.SortAsc = src.Page.SortAsc;

            switch (src.Page.SortByColumn)
            {
                case "Employee.Id":
                    dst.SortByColumn = "EmployeeId";
                    break;

                case "Profile.Id":
                    dst.SortByColumn = "ProfileId";
                    break;

                case "Employee.FullName":
                    dst.SortByColumn = "EmployeeFullName";
                    break;

                case "Profile.Title":
                    dst.SortByColumn = "ProfileName";
                    break;

                case "PreviousDate":
                    dst.SortByColumn = "DatePassed";
                    break;

                case "NextDate":
                    dst.SortByColumn = "NextDate";
                    break;

                default:
                    dst.SortByColumn = src.Page.SortByColumn;
                    break;
            }

            return dst;
        }

        private static PagingValues ConstructJournalPaging(ResolutionContext ctx)
        {
            var src = (Contract.KnowledgeControl.Journal.JournalData)ctx.SourceValue;
            var dst = new PagingValues();

            if (src.Page == null)
                return dst;

            dst.PageNumber = src.Page.PageNumber;
            dst.PageSize = src.Page.PageSize;
            dst.SortAsc = src.Page.SortAsc;

            switch (src.Page.SortByColumn)
            {
                case "ProfileResultId":
                    dst.SortByColumn = "ProfileResultId";
                    break;

                case "Employee.FullName":
                    dst.SortByColumn = "EmployeeFullName";
                    break;

                case "KnowledgeControlTitle":
                    dst.SortByColumn = "ProfileName";
                    break;

                case "DatePassed":
                    dst.SortByColumn = "DatePassed";
                    break;

                default:
                    dst.SortByColumn = src.Page.SortByColumn;
                    break;
            }

            return dst;
        }

        private static Contract.KnowledgeControl.Employee CreateEmployee(FilterScheduleEntities.MonitoringProfileResult result)
        {
            return new Contract.KnowledgeControl.Employee { Id = result.EmployeeId, FullName = result.EmployeeFullName };
        }

        private static Contract.KnowledgeControl.Employee CreateEmployee(Olimp.Domain.Exam.Entities.Pseudo.ArchiveProfileResult result)
        {
            return new Contract.KnowledgeControl.Employee { Id = result.ConcreteEmployeeId, FullName = result.EmployeeFullName };
        }

        private static FilterScheduleContracts.FilterSchedule.Profile CreateProfile(FilterScheduleEntities.MonitoringProfileResult result)
        {
            return new FilterScheduleContracts.FilterSchedule.Profile { Id = result.ProfileId, Title = result.ProfileName };
        }
    }
}