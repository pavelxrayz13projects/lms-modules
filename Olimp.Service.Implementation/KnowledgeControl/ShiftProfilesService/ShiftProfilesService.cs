﻿using Olimp.Domain.Exam.NHibernate.Schema;
using Olimp.Service.Contract.KnowledgeControl.Shift;
using PAPI.Core.Data;
using PAPI.Core.ServiceStack.Service;
using System;
using System.Linq;
using DomainEntities = Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Service.Implementation.KnowledgeControl.ShiftProfilesService
{
    public class ShiftProfilesService : DataServiceBase, IShiftProfileById, IShiftProfilesByPosition, IShiftProfileResultById
    {
        public ShiftProfilesService(IUnitOfWorkFactory unitOfWorkFactory, IRepositoryFactory repositoryFactory)
            : base(unitOfWorkFactory, repositoryFactory)
        { }

        private void EnsureProfileExists(int profileId)
        {
            var profileExists = this.RepositoryFactory.CreateFor<DomainEntities.Profile, int>()
                    .Any(p => p.Id == profileId);

            if (!profileExists)
                throw new InvalidOperationException("Profile doesn't exist");
        }

        private void EnsureEmployeeExists(Guid employeeId)
        {
            var employeeExists = this.RepositoryFactory.CreateFor<DomainEntities.Employee, Guid>()
                    .Any(e => e.Id == employeeId);

            if (!employeeExists)
                throw new InvalidOperationException("Employee doesn't exist");
        }

        protected virtual ProfileResultSchema GetLastProfileResult(IRepository<ProfileResultSchema, int> profileResultRepo, int profileId, Guid employeeId)
        {
            return profileResultRepo.Where(r => r.ConcreteProfileId == profileId && r.ConcreteEmployeeId == employeeId && !r.IsDeleted)
                .OrderByDescending(r => r.DatePassed)
                .FirstOrDefault();
        }

        protected virtual ProfileResult ShiftProfile(
            IRepository<ProfileResultSchema, int> profileResultRepo, ShiftProfilesBase shiftRequest, int profileId)
        {
            var lastResult = this.GetLastProfileResult(profileResultRepo, profileId, shiftRequest.EmployeeId);
            if (lastResult == null)
            {
                lastResult = new ProfileResultSchema
                {
                    ConcreteEmployeeId = shiftRequest.EmployeeId,
                    ConcreteProfileId = profileId,
                    NextDate = shiftRequest.NextDate,
                    IsTemporary = true,
                    Timestamp = DateTime.Now
                };

                profileResultRepo.Save(lastResult);
            }
            else
            {
                lastResult.NextDate = shiftRequest.NextDate;
            }

            return new ProfileResult
            {
                IsPrimary = !lastResult.IsTemporary,
                ProfileResultId = lastResult.Id
            };
        }

        #region IShiftProfileById Members

        public ShiftProfileByIdResponse Post(ShiftProfileById request)
        {
            var profileResultRepo = this.RepositoryFactory.CreateFor<ProfileResultSchema, int>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                this.EnsureEmployeeExists(request.EmployeeId);
                this.EnsureProfileExists(request.ProfileId);

                var result = this.ShiftProfile(profileResultRepo, request, request.ProfileId);

                uow.Commit();

                return new ShiftProfileByIdResponse { ProfileResult = result };
            }
        }

        #endregion IShiftProfileById Members

        #region IShiftProfilesByPosition Members

        public ShiftProfilesByPositionResponse Post(ShiftProfilesByPosition request)
        {
            var profileResultRepo = this.RepositoryFactory.CreateFor<ProfileResultSchema, int>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                var profiles = this.RepositoryFactory.CreateFor<DomainEntities.Employee, Guid>()
                    .Where(e => e.Id == request.EmployeeId)
                    .SelectMany(e => e.Appointments)
                    .Where(a => a.Name == request.Position)
                    .SelectMany(a => a.Profiles)
                    .Select(p => p.Id)
                    .Distinct();

                var results = profiles.AsEnumerable()
                    .Select(profileId => this.ShiftProfile(profileResultRepo, request, profileId))
                    .ToArray();

                uow.Commit();

                return new ShiftProfilesByPositionResponse { ProfileResults = results };
            }
        }

        #endregion IShiftProfilesByPosition Members

        #region IShiftProfileResultById Members

        public ShiftProfileResultByIdResponse Post(ShiftProfileResultById request)
        {
            var profileResultRepo = this.RepositoryFactory.CreateFor<ProfileResultSchema, int>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                this.EnsureEmployeeExists(request.EmployeeId);

                var profileResult = profileResultRepo.FirstOrDefault(r =>
                    r.ConcreteEmployeeId == request.EmployeeId && r.Id == request.ProfileResultId && !r.IsDeleted);

                if (profileResult == null)
                    throw new InvalidOperationException("ProfileResult doesn't exist");

                var lastResult = this.GetLastProfileResult(profileResultRepo, profileResult.ConcreteProfileId, request.EmployeeId);
                if (lastResult.Id > profileResult.Id)
                {
                    if (request.AutoUpdateToNewerProfileResult)
                        profileResult = lastResult;
                    else
                        throw new InvalidOperationException("Profile result is inactual");
                }

                profileResult.NextDate = request.NextDate;

                uow.Commit();

                return new ShiftProfileResultByIdResponse
                {
                    ProfileResult = new ProfileResult
                    {
                        IsPrimary = !profileResult.IsTemporary,
                        ProfileResultId = request.ProfileResultId
                    }
                };
            }
        }

        #endregion IShiftProfileResultById Members
    }
}