﻿using Olimp.Service.Contract.KnowledgeControl.Schedule.ForEmployee;
using System;
using System.Collections.Generic;

namespace Olimp.Service.Implementation.KnowledgeControl.ScheduleService
{
    public class ScheduledExamEqualityComparer : IEqualityComparer<ScheduledExam>
    {
        #region IEqualityComparer<ScheduledExam> Members

        public bool Equals(ScheduledExam x, ScheduledExam y)
        {
            if (x == null && y == null)
                return true;

            if (x == null ^ y == null)
                return false;

            return String.Equals(x.FullName, y.FullName);
        }

        public int GetHashCode(ScheduledExam obj)
        {
            if (obj == null)
                return 0;

            return obj.FullName.GetHashCode();
        }

        #endregion IEqualityComparer<ScheduledExam> Members
    }
}