﻿using AutoMapper;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Common;
using Olimp.Domain.Exam.Entities.Pseudo;
using Olimp.Domain.Exam.ProfileResults;
using Olimp.Service.Contract.KnowledgeControl.Schedule.FilterSchedule;
using Olimp.Service.Contract.KnowledgeControl.Schedule.ForEmployee;
using Olimp.Service.Implementation.EntityMapping;
using PAPI.Core.Data;
using PAPI.Core.ServiceStack.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Olimp.Service.Implementation.KnowledgeControl.ScheduleService
{
    public class ScheduleService : DataServiceBase, IKnowledgeControlForEmployees, IFilterSchedule, ICountKnowledgeControlForEmployees, ICountFilterSchedule
    {
        static ScheduleService()
        {
            MapCreator.CreateFilterScheduleMap();
        }

        private IProfileResultsProvider _profileResultsProvider;
        private IPersonalCatalogueProvider _personalCatalogueProvider;

        public ScheduleService(
            IUnitOfWorkFactory unitOfWorkFactory,
            IRepositoryFactory repositoryFactory,
            IProfileResultsProvider profileResultsProvider,
            IPersonalCatalogueProvider personalCatalogueProvider)
            : base(unitOfWorkFactory, repositoryFactory)
        {
            if (profileResultsProvider == null)
                throw new ArgumentNullException("profileResultsProvider");

            if (personalCatalogueProvider == null)
                throw new ArgumentNullException("personalCatalogueProvider");

            _profileResultsProvider = profileResultsProvider;
            _personalCatalogueProvider = personalCatalogueProvider;
        }

        #region IKnowledgeControlForEmployees Members

        protected virtual string TestUrlRequestMethod { get { return "POST"; } }

        protected virtual string GetCourseTaskFullName(CourseTaskItem item)
        {
            var fullName = new StringBuilder();

            if (!String.IsNullOrWhiteSpace(item.Code))
                fullName.AppendFormat("{0}. ", item.Code);

            fullName.Append(item.Name);

            if (!String.IsNullOrWhiteSpace(item.Area))
                fullName.AppendFormat(" ({0})", item.Area);

            return fullName.ToString();
        }

        protected virtual string GetTestUrl(string referer, Guid employeeId, int branchId)
        {
            return String.Format("/Exam/Test/BeginIntegration?referer={0}&employeeId={1}&materialId={2}",
                HttpUtility.UrlEncode(referer), employeeId, branchId);
        }

        private static IDictionary<Guid, Tuple<bool, bool>> GetFinishedAttempts(
            Guid[] branchIds,
            Guid employeeId,
            PAPI.Core.Data.IRepository<Olimp.Domain.Exam.NHibernate.Schema.ExamAttemptSchema, Guid> attemptsRepo)
        {
            var today = DateTime.Today;
            var tomorrow = today.AddDays(1);

            return attemptsRepo.Where(a => branchIds.Contains(a.TestMapped.UniqueId))
                .Where(a => !a.IsDeleted)
                .Where(a => a.ConcreteEmployeeId == employeeId)
                .Where(a => a.FinishTime >= today && a.FinishTime < tomorrow)
                .Select(a => new { a.IsFinished, a.IsPassed, a.TestMapped.UniqueId, a.FinishTime })
                .AsEnumerable()
                .GroupBy(a => a.UniqueId)
                .ToDictionary(
                    k => k.Key,
                    v => v.OrderByDescending(o => o.FinishTime)
                        .Select(o => new Tuple<bool, bool>(o.IsFinished, o.IsPassed))
                        .First());
        }

        private static Tuple<bool, bool> GetExamStateOrNull(IDictionary<Guid, Tuple<bool, bool>> results, Guid id)
        {
            Tuple<bool, bool> value;
            if (!results.TryGetValue(id, out value))
                return null;

            return value;
        }

        protected virtual ScheduledExam[] MapScheduledExams(
            string referer,
            Guid employeeId,
            bool withTicketsOnly,
            IEnumerable<MonitoringProfileResult> results,
            PAPI.Core.Data.IRepository<Olimp.Domain.LearningCenter.Entities.Profile, int> profilesRepo,
            PAPI.Core.Data.IRepository<Olimp.Domain.Exam.NHibernate.Schema.ExamAttemptSchema, Guid> attemptsRepo)
        {
            var profileIds = results.Select(r => r.ProfileId).ToArray();
            var branchIds = profilesRepo.Where(p => profileIds.Contains(p.Id))
                .SelectMany(p => p.Courses)
                .Select(c => c.MaterialId)
                .Distinct()
                .ToArray();

            var finishedAttempts = GetFinishedAttempts(branchIds, employeeId, attemptsRepo);

            //TODO: Remove duplicate courses (branches with equal uniqueId but different area)
            return _personalCatalogueProvider.GetPersonalCoursesList(branchIds)
                .Select(t => new { Task = t, Result = GetExamStateOrNull(finishedAttempts, t.UniqueId) })
                .Select(t => new ScheduledExam
                {
                    FullName = this.GetCourseTaskFullName(t.Task),
                    TestUrl = this.GetTestUrl(referer, employeeId, t.Task.BranchId),
                    TestUrlRequestMethod = this.TestUrlRequestMethod,
                    HasTickets = t.Task.TicketsCount > 0,
                    IsFinished = t.Result != null && t.Result.Item1,
                    IsPassed = t.Result != null && t.Result.Item2
                })
                .Where(t => !withTicketsOnly || t.HasTickets)
                .Distinct(new ScheduledExamEqualityComparer())
                .OrderBy(t => t.FullName)
                .ToArray();
        }

        public KnowledgeControlForEmployeesResponse Post(GetKnowledgeControlForEmployees request)
        {
            var profilesRepo = this.RepositoryFactory.CreateFor<Olimp.Domain.LearningCenter.Entities.Profile, int>();
            var attemptsRepo = this.RepositoryFactory.CreateFor<Olimp.Domain.Exam.NHibernate.Schema.ExamAttemptSchema, Guid>();

            var filter = Mapper.Map<ProfileResultsFilter>(request);
            var paging = new PagingValues();

            var results = _profileResultsProvider.Filter(filter, paging);
            var employeesHavingExams = results.Entities.GroupBy(
                    e => new Contract.KnowledgeControl.Employee { Id = e.EmployeeId, FullName = e.EmployeeFullName },
                    new EmployeeEqualityComparer())
                .Where(g => g.Any())
                .ToDictionary(k => k.Key, v => this.MapScheduledExams(request.Referer, v.Key.Id, request.WithTicketsOnly, v, profilesRepo, attemptsRepo));

            return new KnowledgeControlForEmployeesResponse { EmployeesHavingExams = employeesHavingExams };
        }

        #endregion IKnowledgeControlForEmployees Members

        #region IFilterSchedule Members

        public FilterScheduleResponse Post(FilterSchedule request)
        {
            var filter = request.Filter != null
                ? Mapper.Map<ProfileResultsFilter>(request.Filter)
                : new ProfileResultsFilter();

            var paging = Mapper.Map<PagingValues>(request);

            var results = _profileResultsProvider.Filter(filter, paging);

            return new FilterScheduleResponse
            {
                FilteredSchedule = Mapper.Map<ScheduleRecord[]>(results.Entities)
            };
        }

        #endregion IFilterSchedule Members

        #region ICountKnowledgeControlForEmployees Members

        public CountKnowledgeControlForEmployeesResponse Post(CountKnowledgeControlForEmployees request)
        {
            //TODO: Изменить логику метода после рефакторинга хранения профилей и курсов, чтобы не делать полную выборку.
            var results = Post(request.Params);
            return new CountKnowledgeControlForEmployeesResponse
            {
                Count = results.EmployeesHavingExams.Sum(e => e.Value.Length)
            };
        }

        #endregion ICountKnowledgeControlForEmployees Members

        #region ICountFilterSchedule Members

        public CountFilterScheduleResponse Post(CountFilterSchedule request)
        {
            var requestFilter = request.Params.Filter;

            var filter = requestFilter != null
               ? Mapper.Map<ProfileResultsFilter>(requestFilter)
               : new ProfileResultsFilter();

            var paging = Mapper.Map<PagingValues>(request.Params);

            var results = _profileResultsProvider.Filter(filter, paging);

            return new CountFilterScheduleResponse
            {
                Count = results.TotalCount
            };
        }

        #endregion ICountFilterSchedule Members
    }
}