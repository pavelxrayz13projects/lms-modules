﻿using System;
using System.Collections.Generic;

namespace Olimp.Service.Implementation.KnowledgeControl.ScheduleService
{
    public class EmployeeEqualityComparer : IEqualityComparer<Contract.KnowledgeControl.Employee>
    {
        #region IEqualityComparer<Employee> Members

        public bool Equals(Contract.KnowledgeControl.Employee x, Contract.KnowledgeControl.Employee y)
        {
            if (x == null && y == null)
                return true;

            if (x == null ^ y == null)
                return false;

            return Guid.Equals(x.Id, y.Id);
        }

        public int GetHashCode(Contract.KnowledgeControl.Employee obj)
        {
            if (obj == null)
                return 0;

            return obj.Id.GetHashCode();
        }

        #endregion IEqualityComparer<Employee> Members
    }
}