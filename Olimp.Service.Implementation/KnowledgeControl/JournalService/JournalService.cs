﻿using AutoMapper;
using Olimp.Domain.Common;
using Olimp.Domain.Exam.NHibernate.Schema;
using Olimp.Domain.Exam.ProfileResults;
using Olimp.Service.Contract.KnowledgeControl.Journal;
using Olimp.Service.Implementation.EntityMapping;
using PAPI.Core.Data;
using PAPI.Core.ServiceStack.Service;
using System;
using System.Linq;

namespace Olimp.Service.Implementation.KnowledgeControl.JournalService
{
    public class JournalService : DataServiceBase, IJournalService, IJournalDeleteService
    {
        static JournalService()
        {
            MapCreator.CreateJournalMap();
        }

        private IProfileResultArchiveProvider _archiveProvider;
        private IProfileResultService _profileResultsService;

        public JournalService(
            IUnitOfWorkFactory unitOfWorkFactory,
            IRepositoryFactory repositoryFactory,
            IProfileResultArchiveProvider archiveProvider,
            IProfileResultService profileResultsService)
            : base(unitOfWorkFactory, repositoryFactory)
        {
            if (archiveProvider == null)
                throw new ArgumentNullException("archiveProvider");

            if (profileResultsService == null)
                throw new ArgumentNullException("profileResultsService");

            _archiveProvider = archiveProvider;
            _profileResultsService = profileResultsService;
        }

        #region IJournalService Members

        public JournalDataResponse Post(JournalData request)
        {
            var filter = request.Filter != null
                ? Mapper.Map<ProfileResultsFilter>(request.Filter)
                : new ProfileResultsFilter();

            var paging = Mapper.Map<PagingValues>(request);

            var results = _archiveProvider.Filter(filter, paging);

            return new JournalDataResponse
            {
                Records = Mapper.Map<JournalRecord[]>(results.Entities)
            };
        }

        #endregion IJournalService Members

        private DeletionResult DeleteProfileResult(ProfileResultSchema schema)
        {
            if (schema == null)
                return DeletionResult.Skipped;

            try
            {
                _profileResultsService.DeleteProfileResult(schema.Id);
                return DeletionResult.Deleted;
            }
            catch
            {
                return DeletionResult.Failed;
            }
        }

        #region IJournalDeleteService Members

        public DeleteFromJournalResponse Post(DeleteFromJournal dto)
        {
            dto.ProfileResultIds = dto.ProfileResultIds ?? new int[0];

            var repo = this.RepositoryFactory.CreateFor<ProfileResultSchema, int>();

            ProfileResultSchema[] results;
            using (this.UnitOfWork.CreateRead())
            {
                results = dto.ProfileResultIds.Select(id => repo.GetById(id))
                    .ToArray();
            }

            return new DeleteFromJournalResponse
            {
                DeletionResults = results.Select(pr => this.DeleteProfileResult(pr))
                    .ToArray()
            };
        }

        #endregion IJournalDeleteService Members
    }
}