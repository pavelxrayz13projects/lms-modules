﻿using log4net;
using Olimp.Service.Contract.PingService;
using System;
using LogManager = PAPI.Core.Logging.Log;

namespace Olimp.Service.Implementation
{
    public class PingService : ServiceStack.ServiceInterface.Service, IPingService
    {
        private static readonly ILog _log = LogManager.CreateLogger("api");

        private static ILog Log { get { return _log; } }

        public PingResponse Post(PingRequest request)
        {
            Log.InfoFormat("Processing PingRequest from {0} with the following message: {1}", RequestContext.IpAddress, request.PingMessage);

            PingResponse result = null;
            if ("Ping" == request.PingMessage)
            {
                result = new PingResponse() { PingReply = "Pong", ReplyTime = DateTime.Now };
                Log.InfoFormat("Successfully processed PingRequest from {0}", RequestContext.IpAddress);
            }
            else
            {
                Log.InfoFormat("Error during processing request - invalid message");
                throw new ArgumentException("Wrong request message");
            }
            return result;
        }
    }
}