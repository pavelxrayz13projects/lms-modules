﻿using Olimp.Reporting;
using Olimp.Reporting.Entities;
using Olimp.Reporting.Orl;
using Olimp.Service.Contract.ReportingService;
using PAPI.Core.Data;
using PAPI.Core.ServiceStack.Service;
using ServiceStack.ServiceInterface;
using System;
using System.IO;
using System.Linq;

namespace Olimp.Service.Implementation.ReportingService
{
    public class ReportingService : DataServiceBase, IReportingService
    {
        private IReportEngine _reportEngine;

        public ReportingService(IUnitOfWorkFactory unitOfWorkFactory, IRepositoryFactory repositoryFactory, IReportEngine reportEngine)
            : base(unitOfWorkFactory, repositoryFactory)
        {
            if (reportEngine == null)
                throw new ArgumentNullException("reportEngine");

            _reportEngine = reportEngine;
        }

        #region IReportingService Members

        [AddHeader(ContentType = "application/octet-stream")]
        public Stream Post(GenerateReport request)
        {
            Report report;
            using (UnitOfWork.CreateRead())
            {
                report = this.RepositoryFactory.CreateFor<Report, int>()
                    .FirstOrDefault(r => r.TemplatePath == request.ReportFileName);
            }

            if (report == null)
                throw new InvalidOperationException("Report " + request.ReportFileName + " not found");

            var provider = _reportEngine.GetReportProvider(report);
            if (provider == null)
                throw new InvalidOperationException("Invalid report " + request.ReportFileName);

            var model = new OrlModel(request.InputValues.ToDictionary(k => k.Key, v => (object)v.Value));
            var reportStream = new MemoryStream();

            provider.GenerateToStream(model, reportStream);
            reportStream.Seek(0, SeekOrigin.Begin);

            return reportStream;
        }

        #endregion IReportingService Members
    }
}