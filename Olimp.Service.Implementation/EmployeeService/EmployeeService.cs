﻿using Olimp.Domain.LearningCenter.Entities;
using Olimp.Service.Contract.EmployeeService.Exists;
using Olimp.Service.Contract.EmployeeService.Position;
using PAPI.Core.Data;
using PAPI.Core.ServiceStack.Service;
using System;
using System.Linq;

namespace Olimp.Service.Implementation.EmployeeService
{
    public class EmployeeService : DataServiceBase, IEmployeeExists, IEmployeeAddPosition, IEmployeeRemovePositions
    {
        public EmployeeService(IUnitOfWorkFactory unitOfWorkFactory, IRepositoryFactory repositoryFactory)
            : base(unitOfWorkFactory, repositoryFactory)
        {
        }

        #region IEmployeeExists Members

        public EmployeeExistsResponse Get(EmployeeExists request)
        {
            using (this.UnitOfWork.CreateRead())
            {
                return new EmployeeExistsResponse
                {
                    Exists = this.RepositoryFactory.CreateFor<Employee, Guid>()
                        .Any(e => e.Id == request.EmployeeId)
                };
            }
        }

        #endregion IEmployeeExists Members

        private Employee GetEmployeeById(IRepository<Employee, Guid> employeeRepo, Guid employeeId)
        {
            var employee = employeeRepo.GetById(employeeId);
            if (employee == null)
                throw new InvalidOperationException("Employee doesn't exist");

            return employee;
        }

        #region IEmployeeAddPosition Members

        public AddEmployeePositionResponse Post(AddEmployeePosition request)
        {
            using (var uow = this.UnitOfWork.CreateWrite())
            {
                var employeeRepo = this.RepositoryFactory.CreateFor<Employee, Guid>();
                var employee = this.GetEmployeeById(employeeRepo, request.EmployeeId);

                if (employee.Appointments.Any(a => a.Name.Equals(request.Position, StringComparison.OrdinalIgnoreCase)))
                    return new AddEmployeePositionResponse { Result = EmployeePositionState.Ignored };

                var appointmentRepo = this.RepositoryFactory.CreateFor<Appointment, int>();
                var appointment = appointmentRepo.FirstOrDefault(a => a.Name == request.Position);

                if (appointment == null)
                {
                    appointment = new Appointment { IsActive = true, IsPrivate = false, Name = request.Position };
                    appointmentRepo.Save(appointment);
                }

                employee.AddAppointment(appointment);

                uow.Commit();
            }

            return new AddEmployeePositionResponse { Result = EmployeePositionState.Added };
        }

        #endregion IEmployeeAddPosition Members

        #region IEmployeeRemovePositions Members

        public RemoveEmployeePositionsResponse Post(RemoveEmployeePositions request)
        {
            if (request.Positions == null || request.Positions.Length == 0)
                return new RemoveEmployeePositionsResponse { Results = new EmployeePositionState[0] };

            using (var uow = this.UnitOfWork.CreateWrite())
            {
                var employeeRepo = this.RepositoryFactory.CreateFor<Employee, Guid>();
                var employee = this.GetEmployeeById(employeeRepo, request.EmployeeId);

                var result = new RemoveEmployeePositionsResponse
                {
                    Results = new EmployeePositionState[request.Positions.Length]
                };

                for (int i = 0; i < request.Positions.Length; i++)
                {
                    var appointment = employee.Appointments
                        .FirstOrDefault(a => a.Name.Equals(request.Positions[i], StringComparison.OrdinalIgnoreCase));

                    if (appointment != null)
                    {
                        employee.Appointments.Remove(appointment);
                        appointment.Employees.Remove(employee);
                        result.Results[i] = EmployeePositionState.Removed;
                    }
                    else
                    {
                        result.Results[i] = EmployeePositionState.Ignored;
                    }
                }

                uow.Commit();

                return result;
            }
        }

        #endregion IEmployeeRemovePositions Members
    }
}