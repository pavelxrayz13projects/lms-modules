﻿using AutoMapper;
using Olimp.Logic.Converters.Employee;
using Olimp.Service.Contract.SyncService.Attempts;
using Olimp.Service.Contract.SyncService.Employee;
using Olimp.Service.Implementation.EntityMapping;
using PAPI.Core.Data;
using PAPI.Core.ServiceStack.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using DomainEntities = Olimp.Domain.LearningCenter.Entities;
using ExamEntities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Service.Implementation.SyncService
{
    public class SyncService : DataServiceBase, ISyncAttempts, ISyncEmployee
    {
        static SyncService()
        {
            MapCreator.CreateAttemptsMap();
        }

        public SyncService(IUnitOfWorkFactory unitOfWorkFactory, IRepositoryFactory repositoryFactory)
            : base(unitOfWorkFactory, repositoryFactory)
        {
        }

        #region ISyncAttempts Members

        public SyncFinishedAttemptsResponse Post(SyncFinishedAttempts request)
        {
            Log.InfoFormat("Received request for syncing maximum {0} attempts starting from {1}",
              request.MaxCountToReturn, request.LastSyncAttemptFinishedTime);

            IList<ExamEntities.ExamAttemptSchema> attemptEntities;
            bool moreAvailable = GetFinishedAttempts(request.LastSyncAttemptFinishedTime, request.MaxCountToReturn, out attemptEntities);

            var attempts = Mapper.Map<IList<ExamEntities.ExamAttemptSchema>, IList<Attempt>>(attemptEntities);

            return new SyncFinishedAttemptsResponse() { Attempts = attempts, MoreItemsAvailable = moreAvailable };
        }

        private bool GetFinishedAttempts(DateTime? lastSyncFinishedTime, int maxCount, out IList<ExamEntities.ExamAttemptSchema> attempts)
        {
            using (this.UnitOfWork.CreateRead())
            {
                var data = this.RepositoryFactory.CreateFor<ExamEntities.ExamAttemptSchema, Guid>()
                    .Where(a => a.IsFinished && a.TestMapped.IsCommercial);

                if (lastSyncFinishedTime.HasValue)
                    data = data.Where(a => a.FinishTime > lastSyncFinishedTime.Value);

                attempts = data.OrderBy(a => a.FinishTime)
                    .ThenBy(a => a.Id)
                    .Take(maxCount + 1)
                    .ToList();

                var result = attempts.Count > maxCount;

                attempts = attempts.Take(maxCount).ToList();

                return result;
            }
        }

        #endregion ISyncAttempts Members

        #region ISyncEmployee Members

        public SyncEmployeeResponse Post(SyncEmployee request)
        {
            EmployeeSyncState result;

            using (var uow = this.UnitOfWork.CreateWrite())
            using (var converter = new BatchEmployeeConverter(this.RepositoryFactory))
            {
                result = this.RepositoryFactory.CreateFor<DomainEntities.Employee, Guid>().Any(e => e.Id == request.Employee.Id)
                    ? EmployeeSyncState.Updated
                    : EmployeeSyncState.Created;

                converter.ConvertAll(new[] { request.Employee });

                uow.Commit();
            }

            return new SyncEmployeeResponse { Result = result };
        }

        #endregion ISyncEmployee Members
    }
}