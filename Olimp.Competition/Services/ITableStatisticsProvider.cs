﻿using Olimp.Competition.ViewModels;

namespace Olimp.Competition.Services
{
    public interface ITableStatisticsProvider
    {
        CompetitionTestTableStatisticsViewModel GetStatistics(int tableId);
    }
}
