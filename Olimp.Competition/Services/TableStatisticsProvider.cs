﻿using Olimp.Domain.LearningCenter.Entities;
using Olimp.Competition.ViewModels;
using Olimp.Configuration;
using PAPI.Core.Data;
using System;
using System.Linq;

namespace Olimp.Competition.Services
{
    public class TableStatisticsProvider : ITableStatisticsProvider
    {
        #region ITableStatisticsProvider Members

        public CompetitionTestTableStatisticsViewModel GetStatistics(int tableId)
        {
            var tableRepo = Repository.Of<CompetitionTable>();
            var resultRepo = Repository.Of<CompetitionTestResult>();

            var total = resultRepo.Count(r => r.CompetitionTable.Id == tableId);
            var answered = resultRepo.Count(r => r.CompetitionTable.Id == tableId && r.Answered);
            var errors = resultRepo.Count(r => r.CompetitionTable.Id == tableId && r.Answered && !r.Correct);

            var placesDistribution = tableRepo.Where(t => t.IsRegistered)
                .Select(t => t.TestResults.Count(r => r.Answered && r.Correct))
                .OrderBy(c => c)
                .Distinct()
                .ToList();

            placesDistribution.Reverse();

            var place = placesDistribution.IndexOf(answered - errors);

            var cfg = Config.Get<CompetitionConfiguration>();

            var finishTime = cfg.ActivationTime.AddMinutes(cfg.Time); 
            var now = DateTime.Now;
            var timeDelta = now <= finishTime
                ? finishTime - now
                : TimeSpan.FromSeconds(0);

            return new CompetitionTestTableStatisticsViewModel
            {
                TimeLeft = timeDelta,
                TotalQuestions = total,
                CurrentQuestionNumber = answered + 1,
                IncorrectAnswers = errors,
                TablePlace = place + 1
            };
        }

        #endregion
    }
}