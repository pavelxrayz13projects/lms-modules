﻿using System;
using System.Collections.Generic;
using System.Linq;
using Olimp.Domain.Common.Content;

namespace Olimp.Competition.Services
{
    public class TransparentTicketShuffler : ITransparentShuffler
    {
        private static Random _rand;

        static TransparentTicketShuffler()
        {
            _rand = new Random();
        }

        private bool _shuffleQuestions;
        private bool _shuffleAnswers;

        public TransparentTicketShuffler(bool shuffleQuestions, bool shuffleAnswers)
        {
            _shuffleQuestions = shuffleQuestions;
            _shuffleAnswers = shuffleAnswers;
        }

        #region IShuffler Members

        public IEnumerable<IQuestion> ShuffleQuestions(IEnumerable<IQuestion> questions)
        {
            if (!_shuffleQuestions)
                return questions;

            return questions.OrderBy(q => _rand.Next());
        }

        public IEnumerable<IAnswer> ShuffleAnswers(IEnumerable<IAnswer> answers)
        {
            if (!_shuffleAnswers)
                return answers;

            return answers.OrderBy(a => _rand.Next());
        }

        #endregion
    }
}