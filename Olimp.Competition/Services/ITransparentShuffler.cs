﻿using System.Collections.Generic;
using Olimp.Domain.Common.Content;

namespace Olimp.Competition.Services
{
    public interface ITransparentShuffler
    {
        IEnumerable<IQuestion> ShuffleQuestions(IEnumerable<IQuestion> questions);

        IEnumerable<IAnswer> ShuffleAnswers(IEnumerable<IAnswer> answers);
    }
}