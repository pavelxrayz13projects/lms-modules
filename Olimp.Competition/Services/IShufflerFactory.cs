﻿using Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Competition.Services
{
    public interface IShufflerFactory
    {
        ITransparentShuffler Create(CompetitionConfiguration config);
    }
}
