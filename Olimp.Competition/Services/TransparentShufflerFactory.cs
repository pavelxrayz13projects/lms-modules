﻿using Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Competition.Services
{
    public class TransparentShufflerFactory : IShufflerFactory
    {
        #region IShufflerFactory Members

        public ITransparentShuffler Create(CompetitionConfiguration config)
        {
            return new TransparentTicketShuffler(config.ShuffleQuestions, config.ShuffleAnswers);
        }

        #endregion
    }
}