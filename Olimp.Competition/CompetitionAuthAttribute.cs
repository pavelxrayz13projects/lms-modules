﻿using Olimp.Domain.LearningCenter.Entities;
using Olimp.Web.Controllers.Base;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace Olimp.Competition
{
    public class CompetitionAuthAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            IUnitOfWork uow = null;
            var request = filterContext.HttpContext.Request;
            var cookie = Plugin.AuthCookie.Get(request);
            int tableId;
            Guid cookieGuid;
            Guid authGuid = Guid.Empty;
            var controller = filterContext.Controller as PController;

            if (controller != null)
                uow = controller.UnitOfWork.CreateRead();

            if (cookie != null && int.TryParse(cookie["tableId"], out tableId))
            {
                try
                {
                    var table = Repository.Of<CompetitionTable>()
                        .FirstOrDefault(t => t.IsRegistered && t.Id == tableId);
                    authGuid = table.AuthGuid;
                }
                catch
                {
                    Plugin.AuthCookie.Unset(filterContext.HttpContext.Response);
                    cookie = null;
                }
                finally
                {
                    if (uow != null)
                        uow.Dispose();
                }
            }
            var authorizedRedirect = filterContext.RouteData.Values["controller"].ToString() == "Competition" && filterContext.RouteData.Values["action"].ToString() == "Splash";

            if (    cookie == null
                    || !int.TryParse(cookie["tableId"], out tableId)
                    || !Guid.TryParse(cookie["authGuid"], out cookieGuid)
                    || (authGuid != cookieGuid)
                    || authorizedRedirect)
            {
                var url = controller.Url.Action(filterContext.RouteData.Values["action"].ToString());
                if (authorizedRedirect)
                {
                    url = controller.Url.Action("Index");
                }
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(
                    new { 
                        action = "Table",
                        controller = "AuthCompetitionTable",
                        returnUrl = url
                    }
                ));
            }
        }
    }
}