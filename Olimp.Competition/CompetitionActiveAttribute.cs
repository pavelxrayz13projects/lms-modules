﻿using Olimp.Domain.LearningCenter.Entities;
using Olimp.Configuration;
using Olimp.Web.Controllers.Base;
using PAPI.Core.Data;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace Olimp.Competition
{
    public class CompetitionActiveAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = filterContext.Controller as PController;
            IUnitOfWork uow = null;
            if (controller != null)
                uow = controller.UnitOfWork.CreateRead();

            try
            {
                var config = Config.Get<CompetitionConfiguration>();
                var isActive = config.Active;
                var finishTime = config.ActivationTime.AddMinutes(config.Time);

                if (!isActive)
                {
                    var route = new RouteValueDictionary(new
                    {
                        controller = "Competition",
                        action = "Index",
                        error = I18N.Competition.CompetitionNotStarted
                    });
                    filterContext.Result = new RedirectToRouteResult(route);
                }
                else if (finishTime < DateTime.Now)
                {
                    var route = new RouteValueDictionary(new
                    {
                        controller = "CompetitionTest",
                        action = "FinalPage"
                    });
                    filterContext.Result = new RedirectToRouteResult(route);
                }
            }
            finally
            {
                if (uow != null)
                    uow.Dispose();
            }
        }
    }
}