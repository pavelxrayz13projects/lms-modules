﻿$.Olimp = $.Olimp || {};
$.Olimp.Timer = {
    init: function (startValue, targetElement, elapsedCallback) {
        var self = this;

        this.__totalSeconds = startValue;
        this.__targetElement = targetElement;
        this.__elapsedCallback = elapsedCallback;

        function timer() {
            self.__updateTime() &&
                setTimeout(timer, 995);
        }

        self.__updateTime();
        timer();
    },

    __updateTime: function () {
        var self = this,
            seconds = this.__totalSeconds,
            timeString = '';

        var hours = Math.floor(seconds / 3600);
        seconds -= hours * 3600;

        var minutes = Math.floor(seconds / 60);
        seconds -= minutes * 60;

        if (hours > 0)
            timeString += hours + ':';

        if (minutes < 10)
            timeString += '0';
        timeString += minutes + ':';

        if (seconds < 10)
            timeString += '0';
        timeString += seconds;

        if (hours == 0 && minutes <= 1)
            timeString = '<span style="color: #d14949;">' + timeString + '</span>';

        this.__targetElement.html(timeString);

        if (this.__totalSeconds > 0) {
            this.__totalSeconds--;
            return true;
        } else {
            setTimeout(function () { self.__elapsedCallback() }, 2000);
            return false;
        }
    }
};
$.Olimp.Chart = {
    Init: function (url, urlToPlaces, urlToStop, charElementId, timerElement, timerTable, charBorderElementId, widthMargin, heightMargin, showAxisOnTop) {
        var self = this;

        this.__url = url;
        this.__showAxisOnTop = showAxisOnTop;
        this.__urlToPlaces = urlToPlaces;
        this.__urlToStop = urlToStop;
        this.__charElementId = charElementId;
        this.__timerElement = timerElement;
        this.__timerTable = timerTable;
        this.__plot = null;
        this.__inited = false;
        this.__result = false;
        this.__data = null
        
        // sizes
        var width = $(window).width() - widthMargin;
        var height = $(window).height() - heightMargin;
        $("#" + charElementId).width(width).height(height);
        $("#" + charBorderElementId).width(width);

        $.jqplot.config.enablePlugins = true;
        $.jqplot.config.defaultTickFormatString = "%d";

        this.ChartSequence();
        this.__intervalId = setInterval(function () { self.ChartSequence() }, 3000);

    },
    __stopCompetition: function () {
        $.ajaxSetup({ async: false });
        $.post(this.__urlToStop);
        $.ajaxSetup({ async: true });
    },
    __getData: function () {
        var newdata;
        $.ajaxSetup({ async: false });
        $.post(this.__url, function (data) {
            newdata = data;
        });
        $.ajaxSetup({ async: true });
        return newdata;
    },
    __getYticks: function (max) {
        var total = Math.floor(max + max * 0.1);
        var step = (Math.ceil(total / 5) == 0) ? 1 : Math.ceil(total / 5);
        var yticks = [];
        var sum = 0;
        while (sum < total) {
            yticks.push(String(sum));
            sum += step;
        }
        yticks.push(String(total));
        return yticks;
    },
    __getNewData: function (data) {
        var newData = [];
        for (var i = 0; i < data.length; i++) {
            newData.push([String(i + 1), data[i]]);
        }
        return newData;
    },
    __arrayToHtml: function (data) {
        var newData = [];
        for (var i = 0; i < data.length; i++) {
            newData.push("<span class='point-label-grey'>" + String(data[i]) + "</span>");
        }
        return newData;
    },
    __getZeroSeria: function (length) {
        var newData = [];
        for (var i = 0; i < length; i++) {
            newData.push(0);
        }
        return newData;
    },
    __getNumberPlaces: function () {
        var newdata;
        $.ajaxSetup({ async: false });
        $.post(this.__urlToPlaces, function (data) {
            newdata = data;
        });
        $.ajaxSetup({ async: true });
        return newdata.places;
    },
    __getStringPlaces: function () {
        var data = this.__getNumberPlaces();
        var newData = [];
        for (var i = 0; i < data.length; i++) {
                switch (data[i]) {
                        case 1:
                            newData.push("<span class='first-place'></span>");
                            break;
                        case 2:
                            newData.push("<span class='second-place'></span>");
                            break;
                        case 3:
                            newData.push("<span class='third-place'></span>");
                            break;
                        default:
                            newData.push("<span class='other-place'>" + data[i] + "</span>");
                    }      
        }
        return newData;
    },

    ShowActivity: function (data, timer) {
        var self = this;
        this.__timerTable.show();
        $.Olimp.Timer.init(Math.floor(data.time), timer, function () {
        });
        var tables = data.tablesQuantity;
        var maxQuestions = data.maxQuestions;
        var yticks = this.__getYticks(data.maxQuestions);
        var ticks = data.tables;
        var answerSeria = data.answeredQuestions;
        if (this.__plot != null)
            this.__plot.destroy();
        this.__plot = $.jqplot(this.__charElementId, [answerSeria], {
            animate: !$.jqplot.use_excanvas,
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                pointLabels: {
                    show: true,
                    escapeHTML: false
                },
                color: '#137ec4'
            },
            axesDefaults: {
                tickOptions: {
                    fontSize: '24px'
                }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks,
                    showTicks: !(self.__showAxisOnTop)
                },
                x2axis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks,
                    show: self.__showAxisOnTop
                },
                yaxis: {
                    ticks: yticks
                }
            },
            grid: {
                backgroundColor: '#fafbfd'
            }
        });
        $(".jqplot-point-label").addClass("point-label-grey");
    },
    Update: function (data) {
        var newData = this.__getNewData(data.answeredQuestions);
        this.__plot.series[0].data = newData;
        this.__plot.replot();
        $(".jqplot-point-label").addClass("point-label-grey");
        $.Olimp.Timer.__totalSeconds = Math.ceil(data.time);
    },
    ShowResult: function (data) {
        var self = this;
        this.__timerTable.hide();
        var line1 = data.correctQuestions;
        var line2 = data.errorQuestions;
        var ticks = data.tables;
        var yticks = this.__getYticks(data.maxQuestions);
        var zeroSeria = this.__getZeroSeria(data.correctQuestions.length);
        var labelsForZero = this.__getStringPlaces();
        if (this.__plot != null)
            this.__plot.destroy();
        this.__plot = $.jqplot(this.__charElementId, [line1, line2, zeroSeria], {
            animate: !$.jqplot.use_excanvas,
            stackSeries: true,
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                pointLabels: { show: true, escapeHTML: false, hideZeros: true}
            },
            series: [
                { label: 'правильные', color: '#63a534' },
                { label: 'неправильные', color: '#d81831' },
				{
				    label: 'нулевая', pointLabels: {
				                labels: labelsForZero
					        }
				}
            ],
            axesDefaults: {
                tickOptions: {
                    fontSize: '20pt'
                }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks,
                    showTicks: !(self.__showAxisOnTop)
                },
                x2axis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks,
                    show: self.__showAxisOnTop
                },
                yaxis: {
                    ticks: yticks
                }
            },
            legend: {
                show: true,
                placement: 'inside'
            },
            grid: {
                backgroundColor: '#fafbfd'
            }
        });
        $(".jqplot-table-legend tr").first().hide();
        $(".third-place, .first-place, .second-place, .other-place").css("display", "inline-block");
        $(".third-place, .first-place, .second-place, .other-place").parent().each(function () {
            var parent = $(this);
            var element = parent.children(0);
            var top = parent.position().top;
            var minTopHeight = top + element.height();
            var diference = minTopHeight - ($(".jqplot-event-canvas").height());
            if (diference >= 0) {
                parent.css("top", top - diference);
            }
        });

    },
    ChartSequence: function ()
    {
        var data = this.__getData();
        if (data.tables.length > 0) {
            if (data.active && data.stopOnTime && data.time <=0) {
                this.__stopCompetition();
            }
            if (data.active) {
                if (!this.__inited) {
                    this.ShowActivity(data, this.__timerElement);
                    this.__inited = true;
                    this.__result = false;
                } else {
                    this.Update(data);
                }
            }
            else if (!data.active) {
                var temp = data;
                temp.time = "";
                if (JSON.stringify(temp) != this.__data)
                    this.__result = false;
                if (!this.__result) {
                    this.ShowResult(data);
                    this.__result = true;
                    this.__inited = false;
                }
                this.__data = JSON.stringify(temp);
            }
        }
    }
};

function clickQuestionInput(input, rowClicked) {
    if (input.is(':radio')) {
        $('.table-question-form td').removeClass('selected');
        input.prop('checked', true).parent().addClass('selected');
    } else if (input.is(':checkbox')) {
        if ((rowClicked ? !input.parent().hasClass('selected') : input.prop('checked')))
            input.prop('checked', true).parent().addClass('selected');
        else
            input.prop('checked', false).parent().removeClass('selected');
    }
}


(function ($) {
    $.widget("ui.olimpcheckradio", {
        __cbUncheckedClass: 'olimp-checkbox-unchecked',
        __cbCheckedClass: 'olimp-checkbox-checked',
        __radioUncheckedClass: 'olimp-radio-unchecked',
        __radioCheckedClass: 'olimp-radio-checked',

        options: { clickable: true },

        _create: function () {
            var self = this;

            this._isRadio = this.element.is(':radio');
            this._name = this.element.attr('name');

            this._checker = $('<div />').addClass("olimpcheckradio").addClass(this._getClass())
                .click(function() { self._onClick() });

            this.element.hide().after(this._checker);
        },

        _uncheckRadio: function () {
            this._checker.removeClass(this.__radioCheckedClass).addClass(this.__radioUncheckedClass)            
            this.element.trigger('checkedchanged', [false]);
        },

        _checkRadio: function () {
            this._checker.removeClass(this.__radioUncheckedClass).addClass(this.__radioCheckedClass)
        },

        setChecked: function (value) {
            if (value === this.element.prop('checked'))
                return true;

            this.element.prop('checked', value);
            if (this._isRadio) {
                if (!value) {
                    this._uncheckRadio();
                } else {
                    this._checkRadio();
                    $(':radio[name=' + this._name + ']').not(this.element).each(function () {
                        var widget = $(this).data('olimpcheckradio');
                        widget && widget._uncheckRadio();
                    })
                }
            } else {                
                this._checker.removeClass(this.__cbCheckedClass).removeClass(this.__cbUncheckedClass);
                if (value)
                    this._checker.addClass(this.__cbCheckedClass);
                else
                    this._checker.addClass(this.__cbUncheckedClass);
            }

            this.element.trigger('checkedchanged', [value])
        },

        emulateClick: function () {
            if (this._isRadio)
                this.setChecked(true);
            else
                this.setChecked(this.element.prop('checked'));
        },

        _onClick: function () {
            if (!this.options.clickable)
                return false;

            this.emulateClick();
        },

        _getClass: function () {
            return !this._isRadio
                ? (this.element.prop('checked') ? this.__cbCheckedClass : this.__cbUncheckedClass)
                : (this.element.prop('checked') ? this.__radioCheckedClass : this.__radioUncheckedClass)
        },

        destroy: function () {
            $.Widget.prototype.destroy.call(this);
        }
    });
})(jQuery);