using System;
using Olimp.Core;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Content;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Content;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Exam.Auth;
using PAPI.Core;
using PAPI.Core.Initialization;
using Olimp.Competition.Controllers;
using Olimp.Core.Routing;
using System.Web.Routing;
using Olimp.Competition.Services;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Competition.Plugin))]
namespace Olimp.Competition
{
    public class Plugin : PluginModule
    {
        public const string AuthorizationCookieName = "competitionCookie";

        public static string AuthorizationCookiePath
        {
            get { return AuthCookie.Path; }
            set { AuthCookie.Path = value; }
        }

        internal static AuthCookieRegistration AuthCookie { get; private set; }

        static Plugin()
        {
            AuthCookie = new AuthCookieRegistration(AuthorizationCookieName, "/Competition", 10);
            AuthCookieManager.RegisterCookie(typeof(Plugin), AuthCookie);
        }

        public Plugin (IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {}
        
        public override void RegisterRoutes (OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Competition"].RegisterControllers(
                typeof(CompetitionController),
                typeof(AuthCompetitionTableController),
                typeof(CompetitionTestController),
                typeof(CompetitionChartController));

            areas["Admin"].RegisterControllers(
                typeof(CompetitionSettingsController),
                typeof(CompetitionTableController),
                typeof(CompetitionAppearanceController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (this.Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var olimpModule = PContext.ExecutingModule as OlimpMaterialsModule;
            if (olimpModule == null)
                throw new InvalidOperationException("Compatible only with OlimpMaterialsModule");

            var facadeRepository = olimpModule.Materials.CourseRepository;

            var connFactoryFactory = new SqliteConnectionFactoryFactory();
            var connFactory = connFactoryFactory.Create();

            var nodesRepository = new OrmLiteNodeRepository(connFactory);
            var courseRepository = new OrmLiteCourseRepository(this.Context.NodeData, connFactory, facadeRepository, nodesRepository);

            var questionsProviderFactory = new OrmLiteCourseQuestionProviderFactory(facadeRepository);

            binder.Controller<CompetitionTestController>(b => b
                .Bind<IShufflerFactory, TransparentShufflerFactory>()
                .Bind<ITableStatisticsProvider, TableStatisticsProvider>()
                .Bind<ICourseRepository>(() => courseRepository)
                .Bind<ICourseQuestionProviderFactory>(() => questionsProviderFactory));
            binder.Controller<CompetitionSettingsController>(b => b
                .Bind<ICourseRepository>(() => courseRepository));
            binder.Controller<CompetitionChartController>(b => b
                .Bind<ICourseRepository>(() => courseRepository));
            
        }
        
    }
}