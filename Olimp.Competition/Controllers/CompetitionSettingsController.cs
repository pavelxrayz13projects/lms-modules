﻿using Olimp.Domain.Catalogue.Entities;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Competition.ViewModels;
using Olimp.Configuration;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Security;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Competition.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Competition, Order = 2)]
    public class CompetitionSettingsController : PController
    {
        private ICourseRepository _courseRepository;

        private IEnumerable<SelectListItem> AddDefaultOption(IEnumerable<SelectListItem> list, string dataTextField, string selectedValue)
        {
            var items = new List<SelectListItem>
            {
                new SelectListItem {Text = dataTextField, Value = selectedValue}
            };
            items.AddRange(list);
            return items;
        }

        public CompetitionSettingsController(ICourseRepository courseRepository)
        {
            if (courseRepository == null)
                throw new ArgumentNullException("courseRepository");

            _courseRepository = courseRepository;
        }


        public ActionResult GetSettings(string message)
        {
            return View(new CompetitionGetSettingsViewModel { Message = message });
        }

        public ActionResult CompetitionSettingsForm()
        {
            using (UnitOfWork.CreateRead())
            {
                var config = Config.Get<CompetitionConfiguration>();
                var courseName = "";

                if (config != null && config.MaterialGuid != Guid.Empty)
                {
                    var course = _courseRepository.GetByUniqueId(config.MaterialGuid);
                    courseName = course != null ? course.ToString() : "";
                }

                var viewModel = new CompetitionSettingsViewModel
                {
                    Config = config,
                    SelectedCourseName = courseName,
                    Courses = AddDefaultOption(
                    _courseRepository
                    .GetByOrigin(CourseSource.NonCommercial)
                    .OrderBy(c => c.Name)
                    .Select(c => new SelectListItem { Text = c.Name, Value = c.MaterialId.ToString(), Selected = (courseName == c.Name) }),
                    I18N.CompetitionSettings.SelectCourse, Guid.Empty.ToString())
                };

                return PartialView(viewModel);
            }
        }

        [HttpPost]
        public ActionResult Save(CompetitionSettingsViewModel model) 
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var config = Config.Get<CompetitionConfiguration>();
                if (config.Active)
                {
                    config.Time = model.Config.Time;
                    model.Config = config;
                }
                else
                {
                    model.Config = config;
                    this.UpdateModel(model);
                }
                Config.Set(model.Config);
                uow.Commit();
            }
            return RedirectToAction("GetSettings", new { message = I18N.CompetitionSettings.SaveMessage});
        }

        [HttpPost]
        public ActionResult ClearData()
        {
            var resultRepo = Repository.Of<CompetitionTestResult>();
            var answersRepo = Repository.Of<CompetitionTestAnswer>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                var config = Config.Get<CompetitionConfiguration>();
                config.Active = false;
                config.ActivationTime = DateTime.MinValue;

                Config.Set(config);

                foreach (var testResult in Repository.Of<CompetitionTestResult>())
                {
                    testResult.CompetitionTable = null;

                    foreach (var a in testResult.Answers.ToList())
                    {
                        a.TestResult = null;
                        testResult.Answers.Remove(a);

                        answersRepo.Delete(a);
                    }
                    resultRepo.Delete(testResult);
                }
                uow.Commit();
            }

            return null;
        }

        [HttpPost]
        public ActionResult StartStopCompetition() 
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var config = Config.Get<CompetitionConfiguration>();
                if (!config.Active)
                    config.ActivationTime = DateTime.Now;

                config.Active = !config.Active;
                Config.Set(config);
                uow.Commit();
            }

            return null;
            
        }

    }
}
