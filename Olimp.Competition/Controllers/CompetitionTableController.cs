﻿using Olimp.Domain.LearningCenter.Entities;
using Olimp.Competition.ViewModels;
using Olimp.Domain.Catalogue.Security;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Competition.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Competition, Order = 2)]
    public class CompetitionTableController : PController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetTables(TableViewModel model)
        {
            using (UnitOfWork.CreateRead())
            {
                return Json(Repository.Of<CompetitionTable>().GetTableData(model, c => new { c.Id, c.Name, c.IsRegistered, c.Password, c.AuthGuid}));
            }
        }

        [HttpPost]
        public ActionResult Edit(CompetitionTableEditViewModel model)
        {
            if (!ModelState.IsValid)
                return null;

            using (var uow = UnitOfWork.CreateWrite())
            {
                if (model.Table.AuthGuid == Guid.Empty)
                    model.Table.AuthGuid = Guid.NewGuid();
                Repository.Of<CompetitionTable>().Save(model.Table);
                uow.Commit();
            }
            return null;
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var resultRepo = Repository.Of<CompetitionTestResult>();
                var answersRepo = Repository.Of<CompetitionTestAnswer>();

                foreach (var testResult in resultRepo.Where(tr => tr.CompetitionTable.Id == id))
                {
                    testResult.CompetitionTable = null;

                    foreach (var a in testResult.Answers.ToList())
                    {
                        a.TestResult = null;
                        testResult.Answers.Remove(a);

                        answersRepo.Delete(a);
                    }
                    resultRepo.Delete(testResult);
                }
                Repository.Of<CompetitionTable>().DeleteById(id);

                uow.Commit();
            }

            return null;
        }
        
        [HttpPost]
        public ActionResult RegisterTable(int id)
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var table = Repository.Of<CompetitionTable>().GetById(id);
                table.IsRegistered = !table.IsRegistered;
                Repository.Of<CompetitionTable>().Save(table);

                uow.Commit();
            }

            return null;
        }

        [HttpPost]
        public ActionResult ChangeCookie(int id)
        {
            var guid = Guid.NewGuid();
            using (var uow = UnitOfWork.CreateWrite())
            {
                var table = Repository.Of<CompetitionTable>().GetById(id);
                table.AuthGuid = guid;
                Repository.Of<CompetitionTable>().Save(table);

                uow.Commit();
            }

            return Json(new {guid});
        }

        [HttpPost]
        public ActionResult UnregisterTables()
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var tableRepo = Repository.Of<CompetitionTable>();
                foreach (var table in tableRepo)
                {
                    table.IsRegistered = false;
                    Repository.Of<CompetitionTable>().Save(table);
                }

                uow.Commit();
            }

            return null;
        }
    }
}
