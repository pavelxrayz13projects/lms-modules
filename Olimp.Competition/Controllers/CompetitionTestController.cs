﻿using Olimp.Domain.LearningCenter.Entities;
using Olimp.Competition.Services;
using Olimp.Competition.ViewModels;
using Olimp.Configuration;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Content;
using Olimp.Web.Controllers.Base;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Competition.Controllers
{
    [CompetitionAuth(Order = 1)]    
    public class CompetitionTestController : PController
    {
        private IShufflerFactory _shufflerFactory;
        private ITableStatisticsProvider _tableStatsProvider;
        private ICourseRepository _courseRepository;
        private ICourseQuestionProviderFactory _questionProviderFactory;

        private Lazy<int> _tableId;

        public CompetitionTestController(
            ICourseRepository courseRepository, 
            IShufflerFactory shufflerFactory, 
            ITableStatisticsProvider tableStatsProvider,
            ICourseQuestionProviderFactory questionProviderFactory)
        {
            if (courseRepository == null)
                throw new ArgumentNullException("courseRepository");

            if (shufflerFactory == null)
                throw new ArgumentNullException("shufflerFactory");

            if (tableStatsProvider == null)
                throw new ArgumentNullException("tableStatsProvider");

            if (questionProviderFactory == null)
                throw new ArgumentNullException("questionProviderFactory");

            _courseRepository = courseRepository;
            _shufflerFactory = shufflerFactory;
            _tableStatsProvider = tableStatsProvider;
            _questionProviderFactory = questionProviderFactory;

            _tableId = new Lazy<int>(this.GetTableId);
        }

        private int TableId { get { return _tableId.Value; } }

        [NonAction]
        private int GetTableId()
        {
            var cookie = Plugin.AuthCookie.Get(this.Request);
            return int.Parse(cookie.Values["tableId"]);
        }

        [NonAction]
        private CompetitionTestResult GetCurrentResult()
        {
            return Repository.Of<CompetitionTestResult>()
                .Where(r => r.CompetitionTable.Id == this.TableId)
                .OrderBy(r => r.Id)
                .FirstOrDefault(r => !r.Answered);
        }

        [HttpGet]
        [CompetitionActive(Order = 2)]
        public ActionResult Index()
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var table = Repository.Of<CompetitionTable>().GetById(this.TableId);
                if (table.TestResults.Any())
                    return RedirectToAction("Question");
                
                var config = Config.Get<CompetitionConfiguration>();

                var course = _courseRepository.GetByUniqueId(config.MaterialGuid);

                if (course == null)
                    return RedirectToAction("Index", "Competition", new { error = I18N.Competition.CourseNotFound });

                var questionProvider = _questionProviderFactory.Create(course);
                var shuffler = _shufflerFactory.Create(config);
                foreach (var q in shuffler.ShuffleQuestions(questionProvider.GetAll()))
                {
                    var result = new CompetitionTestResult
                    {
                        MaterialId = config.MaterialGuid,
                        CompetitionTable = table,
                        QuestionId = q.UniqueId,
                    };

                    table.TestResults.Add(result);
                }

                uow.Commit();
            }

            return RedirectToAction("Question");
        }

        [HttpGet]
        [CompetitionActive(Order = 2)]
        public ActionResult Question()
        {
            Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Expires", "0");

            using (UnitOfWork.CreateRead())
            {
                var current = this.GetCurrentResult();
                if (current == null)
                    return RedirectToAction("FinalPage");

                var config = Config.Get<CompetitionConfiguration>();
                var shuffler = _shufflerFactory.Create(config);

                var course = _courseRepository.GetByUniqueId(config.MaterialGuid);
                var questionProvider = _questionProviderFactory.Create(course);

                var question = questionProvider.GetById(current.QuestionId);
                var model = new CompetitionTestQuestionViewModel(question, current.Id)
                {
                    Statistics = _tableStatsProvider.GetStatistics(this.TableId),
                    Answers = shuffler.ShuffleAnswers(question.Answers)
                        .Select(a => new CompetitionTestAnswerViewModel(a))
                        .ToArray(),
                    ShowErrors = config.ShowErrors,
                    ShowPlace = config.ShowPlace
                };

                return View(model);
            }
        }

        [HttpPost]
        [CompetitionActive(Order = 2)]
        public ActionResult Answer(int questionId, int[] answers)
        {
            answers = answers ?? new int[0];
            if (answers.Length == 0)
                return RedirectToAction("Question");

            using (var uow = UnitOfWork.CreateWrite())
            {
                var current = Repository.Of<CompetitionTestResult>().GetById(questionId);
                if (current.Answered)
                    return RedirectToAction("Question");

                var config = Config.Get<CompetitionConfiguration>();
                var course = _courseRepository.GetByUniqueId(config.MaterialGuid);
                var questionProvider = _questionProviderFactory.Create(course);

                var question = questionProvider.GetById(current.QuestionId);
                
                current.Answered = true;
                current.DateAnswered = DateTime.Now;
                current.Correct = question.Answers.Count(a => a.IsCorrect) == answers.Length;                
                foreach (var a in answers)
                {
                    if (a < 0 || a >= question.Answers.Length)
                        return RedirectToAction("Question");

                    var testAnswer = new CompetitionTestAnswer { TestResult = current, AnswerId = a };
                    current.Answers.Add(testAnswer);

                    if (!question.Answers[a].IsCorrect)
                        current.Correct = false;
                }

                uow.Commit();

                return RedirectToAction("Question");
            }
        }

        [HttpGet]
        public ActionResult FinalPage()
        {
            var result = new CompetitionTestFinalPageViewModel();
            using (UnitOfWork.CreateRead())
            {
                var stats = _tableStatsProvider.GetStatistics(this.TableId);

                if (stats.TotalQuestions < stats.CurrentQuestionNumber)
                {
                    result.Message = I18N.Competition.AllQuestionsAnswered;
                    result.Image = Url.Content("~/Content/Images/competition-all-answered.gif");
                }
                else if (stats.TimeLeft == TimeSpan.Zero)
                {
                    result.Image = Url.Content("~/Content/Images/competition-hourglass.gif");
                    result.Message = I18N.Competition.TimeIsOver;
                }
                else
                {
                    result.Message = I18N.Competition.CompetitionIsOver;
                    result.Image = Url.Content("~/Content/Images/competition-ended.gif");
                }
            }

            return View(result);
        }

        [HttpPost]
        public ActionResult Logout()
        {
            return RedirectToRoute("Default");
        }

        [HttpPost]
        public ActionResult ToChart()
        {
            return RedirectToAction("Index", "CompetitionChart");
        }
    }
}
