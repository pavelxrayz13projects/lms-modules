﻿using System.Linq.Expressions;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Competition.ViewModels;
using Olimp.Web.Controllers.Base;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Competition.Controllers
{
    public class AuthCompetitionTableController : PController
    {

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            ViewData["returnUrl"] = Request["returnUrl"];
        }

        [HttpGet]	
        public ActionResult Table(string message)
        {
            var competitionCookie = Plugin.AuthCookie.Get(this.Request);
            int tableId;
            Guid authGuid;
            if (competitionCookie != null && int.TryParse(competitionCookie["tableId"], out tableId) && Guid.TryParse(competitionCookie["authGuid"], out authGuid))
                return RedirectToAction("AuthorizedTable", new {tableId, authGuid, returnUrl = Request["returnUrl"] });
            
            using (UnitOfWork.CreateRead())
                return View("SelectTable", new SelectTableViewModel { Tables = Repository.Of<CompetitionTable>()
                    .Where(t => !t.IsRegistered).OrderBy(t => t.Name)
                    .Select(a => new SelectListItem { Text = a.Name, Value = a.Id.ToString() }),
                     ErrorMessage = message
                });
        }

        [HttpGet]
        public ActionResult AuthorizedTable(int tableId, Guid authGuid)
        {
            using (UnitOfWork.CreateRead())
            {
                var competitionTableRepo = Repository.Of<CompetitionTable>();
                var table = competitionTableRepo.GetById(tableId);
                if (table == null || table.AuthGuid != authGuid)
                    return RedirectToAction("LogOut", new {tableId, returnUrl = Request["returnUrl"] });

                return View(new AuthorizedTableViewModel
                {
                    Table = competitionTableRepo.GetById(tableId)
                });
            }
        }

        [NonAction]
        private ActionResult RedirectBack(int tableId, Guid authGuid)
        {
            var url = Request["returnUrl"];
            Plugin.AuthCookie.Set(this.Response, new {tableId, authGuid });

            return Redirect(url);
        }

        [HttpGet]
        public ActionResult RedirectAuthorizedBack(string returnUrl)
        {
            return Redirect(returnUrl);
        }

        [NonAction]
        private CompetitionTable GetTableAndSetSatatus(Expression<Func<CompetitionTable, bool>> expression, bool isRegistered)
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var competitionTableRepo = Repository.Of<CompetitionTable>();
                var table = competitionTableRepo.FirstOrDefault(expression);

                if (table != null)
                {
                    table.IsRegistered = isRegistered;
                    competitionTableRepo.Save(table);
                    uow.Commit();
                }

                return table;
            }
        }

        [HttpGet]
        public ActionResult LogOut(int tableId, string returnUrl)
        {
            Plugin.AuthCookie.Unset(this.Response);
            GetTableAndSetSatatus(t => t.IsRegistered && t.Id == tableId, false);
            return Redirect(returnUrl);
        }

        [HttpPost]
        public ActionResult SelectTable(SelectTableViewModel model)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("Table", new { message = I18N.CompetitionTable.PasswordError, returnUrl = Request["returnUrl"] });

            var table = GetTableAndSetSatatus(t => !t.IsRegistered && t.Id == model.TableId && t.Password == model.Password, true);
            if(table != null)
                RedirectBack(table.Id, table.AuthGuid);

            return RedirectToAction("Table", new { message = I18N.CompetitionTable.PasswordError, returnUrl = Request["returnUrl"]});
        }
    }
}
