﻿using Olimp.Domain.LearningCenter.Entities;
using Olimp.Competition.ViewModels;
using Olimp.Configuration;
using Olimp.Domain.Catalogue;
using Olimp.Web.Controllers.Base;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Competition.Controllers
{
    //TODO: delete AllowGet in json requests
    public class CompetitionChartController : PController
    {
        private ICourseRepository _courseRepository;

        public CompetitionChartController(ICourseRepository courseRepository)
        {
            if (courseRepository == null)
                throw new ArgumentNullException("courseRepository");

            _courseRepository = courseRepository;
        }

        public ActionResult Index()
        {
            var model = new CompetitionChartViewModel();
            using (UnitOfWork.CreateRead())
            {
                var config = Config.Get<CompetitionConfiguration>();
                model.ChartCss = config.ChartCss;
                model.ShowAxisOnTop = config.ShowAxisOnTop;
            }
            return View(model);
        }

        public ActionResult GetCompetitionData()
        {
            using (UnitOfWork.CreateRead())
            {
                var config = Config.Get<CompetitionConfiguration>();
                var tables = Repository.Of<CompetitionTable>().Where(t => t.IsRegistered).OrderBy(t => t.Name).ToList();
                var course = _courseRepository.GetByUniqueId(config.MaterialGuid);
                var finishTime = config.ActivationTime.AddMinutes(config.Time);
                var now = DateTime.Now;
                var time = now <= finishTime
                    ? (finishTime - now).TotalSeconds
                    : TimeSpan.FromSeconds(0).TotalSeconds;
                
                return Json(new
                {
                    active = config.Active,
                    stopOnTime = config.ShowChartOnTimeEnd,
                    time,
                    maxQuestions = (course == null) ? 0 : course.QuestionsCount,
                    tables = tables.Select(t => t.Name),
                    answeredQuestions = tables.Select(t => t.TestResults.Count(tr => tr.Answered)),
                    correctQuestions = tables.Select(t => t.TestResults.Count(tr => tr.Answered && tr.Correct)),
                    errorQuestions = tables.Select(t => t.TestResults.Count(tr => tr.Answered && !tr.Correct))
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetCompetitionPlaces()
        {
            using (UnitOfWork.CreateRead())
            {
                var tables = Repository.Of<CompetitionTable>().Where(t => t.IsRegistered).OrderBy(t => t.Name);
                var scores = tables.Select(t => t.TestResults.Count(tr => tr.Answered && tr.Correct)).ToArray();
                var dates = tables.Select(t => t.TestResults.Max(r => r.DateAnswered)).ToArray();

                var places = scores.Select((s, i) => new { Score = s, LastDate = dates[i], Index = i })
                    .OrderByDescending(o => o.Score)
                    .ThenBy(o => o.LastDate)
                    .Select((o, i) => new {o.Index, Place = i + 1 })
                    .OrderBy(o => o.Index)
                    .Select(o => o.Place);
                return Json(new
                {
                    places
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StopCompetition()
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var config = Config.Get<CompetitionConfiguration>();
                config.Active = false;
                Config.Set(config);
                uow.Commit();
            }

            return null;

        }

    }
}
