﻿using Olimp.Competition.ViewModels;
using Olimp.Web.Controllers.Base;
using System.Web.Mvc;

namespace Olimp.Competition.Controllers
{
    [CompetitionAuth]
    public class CompetitionController : PController
    {
        public ActionResult Index(string error)
        {
            return View(new CompetitionIndexViewModel { Error = error });
        }

        public ActionResult Splash()
        {
            return RedirectToAction("Index");
        }

    }
}
