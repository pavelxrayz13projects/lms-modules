﻿using Olimp.Domain.LearningCenter.Entities;
using Olimp.Competition.ViewModels;
using Olimp.Configuration;
using Olimp.Domain.Catalogue.Security;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using System.Web.Mvc;

namespace Olimp.Competition.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Competition, Order = 2)]
    public class CompetitionAppearanceController : PController
    {

        public ActionResult Index()
        {
            var config = Config.Get<CompetitionConfiguration>();
            return View(new CompetitionAppearanceViewModel { ChartCss = System.Web.HttpUtility.JavaScriptStringEncode(config.ChartCss) });
        }

        [HttpPost]
        public ActionResult Save(string chartCss)
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var config = Config.Get<CompetitionConfiguration>();
                config.ChartCss = chartCss;
                Config.Set(config);
                uow.Commit();
            }
            return null;
        }

    }
}
