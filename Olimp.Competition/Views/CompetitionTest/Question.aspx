﻿<%@ Page Title="Олимп" Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<CompetitionTestQuestionViewModel>" MasterPageFile="~/Views/Shared/Competition.master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="Olimp_Competition_Js" runat="server">
    <% Platform.RenderExtensions("/olimp/competition/js"); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Competition_Css" runat="server">
    <% Platform.RenderExtensions("/olimp/competition/test/css"); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Competition_Top" runat="server">
    <center>
        <div class="competition-info-block">
            <div id="remaining-time" class="value"></div>
            <div class="name">Время до окончания</div>
        </div>
        <div class="competition-info-block">
            <div class="value"><%= String.Format("{0} / {1}", Model.Statistics.CurrentQuestionNumber, Model.Statistics.TotalQuestions) %></div>
            <div class="name">Текущий вопрос</div>
        </div>
    </center>
    <div style="position: absolute; right: 0; top: 20px;">
        <% if (Model.ShowPlace) { %>
        <div class="competition-info-block">
            <div class="value"><%= Model.Statistics.TablePlace %></div>
            <div class="name">Место в зачете</div>
        </div>
         <% } %>
        <% if (Model.ShowErrors) { %> 
        <div class="competition-info-block">
            <div class="value"><%= Model.Statistics.IncorrectAnswers %></div>
            <div class="name">Допущено ошибок</div>
        </div>
        <% } %>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Competition_Content" runat="server">
    <% using (Html.BeginForm("Answer", "CompetitionTest", FormMethod.Post, new { @class="table-question-form" })) { %>
        <%= Html.HiddenFor(m => m.QuestionId) %>

	    <h1 class="competition-question"><%= Model.Text%></h1>
        <ul class="competition-answers">            
        <% for (int i = 0; i < Model.Answers.Length; i++) { %>
            <% var a = Model.Answers[i]; %>                    
            <% var type = Model.IsMultiple ? "checkbox" : "radio"; %>                
            <li>
                <table>
                    <tr>
                        <td class="checker">
                            <input type="<%= type %>" name="answers" value="<%= a.Id%>" />
                        </td>
                        <td class="text">
                            <% if (a.LineThrough) { %>
                                <label style="text-decoration:line-through;"><%= a.Text%></label>
                            <% } else { %>
                                <label><%= a.Text%></label>
                            <% } %>
                        </td>
                    </tr>               
                </table>
            </li>
        <% } %>
        </ul>

        <center>
            <button type="submit" class="big-button">Ответить</button>
        </center>
    <% } %>

    <script>
        $(function () {
            var timeContainer = $('#remaining-time'),
                totalSeconds = <%= (int) Model.Statistics.TimeLeft.TotalSeconds %>;

            $.Olimp.Timer.init(totalSeconds, timeContainer, function () {
                window.location = '<%= Url.Action("FinalPage") %>';
            });

            $('input[type!=hidden]').olimpcheckradio({ clickable: false }).bind('checkedchanged', function (ev, checked) {
                var parent = $(this).parents(3);
                if (checked)
                    parent.addClass("selected");
                else
                    parent.removeClass("selected");
            });
            $('button').olimpbutton();
            $('.competition-answers li').click(function (ev) { 
                $(this).find('input').olimpcheckradio('emulateClick'); 
            })            
        })
    </script>
</asp:Content>