﻿<%@ Page  Title="Олимп" Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<CompetitionTestFinalPageViewModel>" MasterPageFile="~/Views/Shared/Competition.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Olimp_Competition_Head" runat="server">
    <style>
        form { display: inline; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Olimp_Competition_Content" runat="server">
    <div>
        <%-- 1/2 of block height + double padding (2*45px) + #competition-head height (118px) + double margin (2*20px)--%>
        <div style="text-align: center;">
            <% if (!String.IsNullOrEmpty(Model.Image)) { %>
                <div class="final-page-image">
                    <img src="<%= Model.Image %>" />
                </div>
            <% } %>
            <h1 style="font-size: 36px;"><%= Model.Message %></h1>
            <div style="margin-top:60px;">
                <% using (Html.BeginForm("ToChart", "CompetitionTest")) { %>
                    <button type="submit" class="big-button">К графику</button>
                <% } %>
                <% using (Html.BeginForm("Logout", "CompetitionTest")) { %>
                    <button type="submit" class="big-button">Выход</button>
                <% } %>
            </div>        
        </div>
    </div>

	<script type="text/javascript">
	    $(function () { $('button').olimpbutton(); });
	</script>
</asp:Content>