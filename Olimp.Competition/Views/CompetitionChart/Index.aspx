﻿<%@ Page Title="Олимп" Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<CompetitionChartViewModel>" MasterPageFile="~/Views/Shared/CompetitionBase.master" %>
<asp:Content ID="Content4" ContentPlaceHolderID="Olimp_CompetitionBase_Css" runat="server">
    <% Platform.RenderExtensions("/olimp/competition/chart/css"); %>
    <%="<!--[if lte IE 6]><link href=\"/Content/olimp.competition.ie.css\" rel=\"stylesheet\" /><![endif]-->" %>
    <style type="text/css">
        <%= Model.ChartCss%>
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Olimp_CompetitionBase_Js" runat="server">
	<% Platform.RenderExtensions("/olimp/competition/js"); %>
    <script type="text/javascript">
        $(document).ready(function () {
            $.Olimp.Chart.Init("<%= Url.Action("GetCompetitionData") %>", "<%= Url.Action("GetCompetitionPlaces") %>", "<%= Url.Action("StopCompetition") %>", 'chart', $(".chart-timer-time"), $(".chart-timer-table"), 'chart-border', 80, 200, <%= Model.ShowAxisOnTop.ToString().ToLower()%> );
        });
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Olimp_CompetitionBase_Top" runat="server">
    <div id="chart-top-wrapper">
        <table id="chart-top" cellpadding="0" cellspacing="0">
            <tr>
                <td class="chart-timer">
                    <table class="chart-timer-table" style="display:none" cellpadding="0" cellspacing="0">
                        <tr><td class="chart-timer-time"></td></tr>
                    </table>
                    &nbsp;
                </td>
                <td class="chart-text">
                    <span>Диаграмма активности</span>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Olimp_CompetitionBase_Content" runat="server">
	<center>
		<div id="chart-border">
			<div id="chart"></div>
		</div>
	</center>
</asp:Content>