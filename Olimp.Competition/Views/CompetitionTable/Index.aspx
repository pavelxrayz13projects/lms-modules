﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<dynamic>"  MasterPageFile="~/Views/Shared/CompetitionAdmin.master" %>
<%@ Import Namespace="I18N=Olimp.Competition.I18N" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Olimp_Competition_Content" runat="server">
	<script type="text/javascript">
	    function registerTable() {
	        var self = this;
	        var message = self.IsRegistered() ? "Стол будет разрегистрирован. Продолжить?" : "Стол будет зарегистрирован. Продолжить?"
	        if (!confirm(message))
                return;

           
	        $.post('<%= Url.Action("RegisterTable") %>', { id: this.Id() }, function () {
	            var message = self.IsRegistered() ? "Стол разрегистрирован." : "Стол зарегистрирован."
	            self.IsRegistered(!self.IsRegistered());
	            $.Olimp.showSuccess(message);
            })
	    }
	    function changeCookieTable() {
	        var self = this;
	        var message = "Cookie авторизации будет сгенерирована заново. Продолжить?";
	        if (!confirm(message))
	            return;

	        $.post('<%= Url.Action("ChangeCookie") %>', { id: this.Id() }, function (data) {
	            self.AuthGuid(data.guid);
	            var message = "Cookie установлена";
	            $.Olimp.showSuccess(message);
	        })
        }
	    function unregisterAllTables(o) {
	        $.post('<%= Url.Action("UnregisterTables") %>', function () {
	            o.model.reload();
	            $.Olimp.showSuccess("Столы разрегистрированы.");
	        })
        }
    </script>
	<script type="text/html" id="registered-template">
        <!-- ko if: IsRegistered -->
		    <span>Зарегистрирован</span>
        <!-- /ko -->
        <!-- ko ifnot: IsRegistered -->
		    <span>Не зарегистрирован</span>
        <!-- /ko -->
	</script>
	<script id="register-action" type="text/html">
		<a href="#" class="icon icon-generate-3 action" title="Регистрация/разрегистрация" data-bind="click: registerTable"></a>
	</script>
	<script id="change-cookie-action" type="text/html">
		<a href="#" class="icon icon-generate-2 action" title="Перегенерация cookie авторизации" data-bind="click: changeCookieTable"></a>
	</script>

    
    <h1><%= I18N.CompetitionTable.Tables %></h1>		
	
	<div id="edit-dialog">	
		<% Html.RenderPartial("Edit"); %>
	</div>


	<div class="block">
		<% Html.Table(
            new TableOptions
            {
                SelectUrl = Url.Action("GetTables"),
                Adding = new AddingOptions
                {
                    Text = I18N.CompetitionTable.Add,
                    New = new { Id = 0, Name = "", Password = "", IsRegistered = false, AuthGuid = Guid.Empty }
                },
                DefaultSortColumn = "Name",
                PagingAllowAll = true,
                Columns = new[] { 
					new ColumnOptions("Name", I18N.CompetitionTable.Name),
                    new ColumnOptions("Password", I18N.CompetitionTable.Password),
                    new ColumnOptions("IsRegistered", I18N.CompetitionTable.IsRegistered){ TemplateId = "registered-template" }, 
                    new ColumnOptions("AuthGuid", I18N.CompetitionTable.AuthGuid),
                },

                Actions = new RowAction[] { 
					new EditAction("edit-dialog", new DialogOptions { Title = I18N.CompetitionTable.Edit, Width = "600px" }),
                    new TemplateAction("register-action"),
                    new TemplateAction("change-cookie-action"),
					new DeleteAction(Url.Action("Delete"), I18N.CompetitionTable.ConfirmDelete)	},
                TableActions = new TableAction[] { new CustomTableAction("unregisterAllTables", "Столы будут разрегистрированы. Продолжить?", "Разрегистрация столов", "")  
				}
            }); %>
	</div>
</asp:Content>
