﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<CompetitionTableEditViewModel>" %>

<% Html.EnableClientValidation(); %>
	
<% using (Html.BeginForm("Edit", "CompetitionTable", FormMethod.Post))
   { %>
	
	<%= Html.HiddenFor(m => m.Table.Id, new Dictionary<string, object> { 
		{ "data-bind", "value: Id" } }) %>
	<%= Html.HiddenFor(m => m.Table.AuthGuid, new Dictionary<string, object> { 
		{ "data-bind", "value: AuthGuid" } }) %>
	<%= Html.HiddenFor(m => m.Table.IsRegistered, new Dictionary<string, object> { 
		{ "data-bind", "value: IsRegistered() ? 'true' : 'false'"} }) %>
	
	<table class="form-table">
		<tr class="first">
			<th><%= Html.LabelFor(m => m.Table.Name) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Table.Name) %></div>
				<%= Html.TextBoxFor(m => m.Table.Name, new Dictionary<string, object> { 
					{ "data-bind", "value: Name" } }) %>
			</td>
		</tr>
		<tr class="last">
			<th><%= Html.LabelFor(m => m.Table.Password) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Table.Password) %></div>
				<%= Html.TextBoxFor(m => m.Table.Password, new Dictionary<string, object> { 
					{ "data-bind", "value: Password" } }) %>
			</td>
		</tr>
	</table>
<% } %>	