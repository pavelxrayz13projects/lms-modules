﻿<%@ Page Title="Олимп" Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<CompetitionIndexViewModel>" MasterPageFile="~/Views/Shared/Popup.master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="Olimp_Css" runat="server">
    <% Platform.RenderExtensions("/olimp/competition/layout/css"); %>
    <style>
        body .wrapper { background-image: none; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Olimp_Body" runat="server">
        <div id="start-testing-center">
            <div id="start-testing-center-bottom">
                <%= Html.ActionLink("Начать соревнование", "Index", new { controller = "CompetitionTest" }, new { @class="big-button"})%>
            </div>
        </div>

	<script type="text/javascript">
	    $(function () {
	        $('.big-button').olimpbutton();
	        <% if (!String.IsNullOrWhiteSpace(Model.Error)) { %>
	        if (window.location.hash === "#badAttempt") {
                window.location = '<%= Url.Action("Index", "CompetitionTest") %>';
	        } else {
	            $.Olimp.showError('<%: Model.Error %>');
	            window.location.hash = "badAttempt";
	        }	        
            <% } %>
	    });
	</script>
</asp:Content>