﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<CompetitionAppearanceViewModel>"  MasterPageFile="~/Views/Shared/CompetitionAdmin.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Olimp_Competition_Content" runat="server">   
    <% Html.EnableClientValidation(); %>

    <h1>Внешний вид</h1>
    <% using(Html.BeginForm("Save", "CompetitionAppearance", FormMethod.Post)) { %>
        <div id="appearanceFormTable">
            <div class='block'>		
	            <table class="form-table">	
		            <tr class="first last">
			            <th>Стили для диаграммы активности</th>
			            <td>
                            <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ChartCss) %></div>
                            <%= Html.TextAreaFor(m => m.ChartCss,new Dictionary<string, object> { 
					            { "data-bind", "value: chartCss" }, {"style", "height: 100px;"} }) %>	
			            </td>
		            </tr>	
	            </table>
            </div>

            <button id="save" type="button" data-bind="click : saveData">Сохранить</button>
        </div>

        <script type="text/javascript">
            $(function () {
                $("button").olimpbutton();

                var viewModel = {
                    chartCss: ko.observable("<%= Model.ChartCss%>"),

                    saveData: function () {
                        $.post("<%= Url.Action("Save") %>", {chartCss: this.chartCss()}, function (data) {
                                $.Olimp.showSuccess("Данные сохранены");
                       });
                    }
                };

                ko.applyBindings(viewModel, $('#appearanceFormTable')[0]);
            });
        </script>
	<% } %>			
</asp:Content>