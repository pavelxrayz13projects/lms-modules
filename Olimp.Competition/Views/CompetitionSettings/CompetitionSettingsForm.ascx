﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<CompetitionSettingsViewModel>" %>
<h1>Настройки соревнования</h1>
<div id="competitionFormTable">
    <div class='block'>
        <table class="form-table">

            <tr class="first">
                <th><%= Html.LabelFor(m => m.Config.MaterialGuid) %></th>
                <td>
                    <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Config.MaterialGuid) %></div>
                    <div>
                        <%= Html.DropDownListFor(m => m.Config.MaterialGuid, Model.Courses,new Dictionary<string, object> { 
                            { "data-bind", "value: guid, disable: active" }, { "class", "materials-list"} }) %>
                    </div>
                    
                </td>
            </tr>
            <tr>
                <th><%= Html.LabelFor(m => m.Config.Time) %></th>
                <td>
                    <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Config.Time) %></div>
                    <%= Html.TextBoxFor(m => m.Config.Time,new Dictionary<string, object> { 
                        { "data-bind", "value: time" } }) %>	
                </td>
            </tr>
            <tr>
                <th><%= Html.LabelFor(m => m.Config.ShuffleQuestions) %></th>
                <td class="checkbox">
                    <%= Html.CheckBoxFor(m => m.Config.ShuffleQuestions,new Dictionary<string, object> { 
                        { "data-bind", "checked: shuffleQuestions, disable: active" } }) %>	
                </td>
            </tr>
            <tr>
                <th><%= Html.LabelFor(m => m.Config.ShuffleAnswers) %></th>
                <td class="checkbox">
                    <%= Html.CheckBoxFor(m => m.Config.ShuffleAnswers,new Dictionary<string, object> { 
                        { "data-bind", "checked: shuffleAnswers, disable: active" } }) %>	
                </td>
            </tr>
            <tr>
                <th><%= Html.LabelFor(m => m.Config.ShowErrors) %></th>
                <td class="checkbox">
                    <%= Html.CheckBoxFor(m => m.Config.ShowErrors,new Dictionary<string, object> { 
                        { "data-bind", "checked: showErrors, disable: active" } }) %>	
                </td>
            </tr>
            <tr>
                <th><%= Html.LabelFor(m => m.Config.ShowPlace) %></th>
                <td class="checkbox">
                    <%= Html.CheckBoxFor(m => m.Config.ShowPlace,new Dictionary<string, object> { 
                        { "data-bind", "checked: showPlace, disable: active" } }) %>	
                </td>
            </tr>
            <tr>
                <th><%= Html.LabelFor(m => m.Config.ShowChartOnTimeEnd) %></th>
                <td class="checkbox">
                    <%= Html.CheckBoxFor(m => m.Config.ShowChartOnTimeEnd,new Dictionary<string, object> { 
                        { "data-bind", "checked: showChartOnTimeEnd, disable: active" } }) %>	
                </td>
            </tr>
            <tr class="last">
                <th><%= Html.LabelFor(m => m.Config.ShowAxisOnTop) %></th>
                <td class="checkbox">
                    <%= Html.CheckBoxFor(m => m.Config.ShowAxisOnTop,new Dictionary<string, object> { 
                        { "data-bind", "checked: showAxisOnTop, disable: active" } }) %>	
                </td>
            </tr>
        </table>
    </div>

    <button id="saveSettings" type="submit">Сохранить</button>
    <button id="startTest" type="button" data-bind="text : testStatusText, click : manageCompition"></button>
    <button id="clearData" data-bind="click: clearData" type="button">Сбросить результаты</button>
</div>




<script type="text/javascript">
    $(function () {
        $("button").olimpbutton();
        $('select').combobox();

        var toBoolean = function (string) {
            switch (string.toLowerCase()) {
                case "true": case "1": return true;
                case "false": case "0": case "": case null: return false;
                default: return Boolean(string);
            }
        }
        var checkEmptyCourse = function(string){
            if (string == "" || string == null){
                return "Курс не выбран";
            }else{
                return string;
            }
        }
        var viewModel = {
            shuffleAnswers: ko.observable(<%= Model.Config.ShuffleAnswers.ToString().ToLower() %>),
            shuffleQuestions: ko.observable(<%= Model.Config.ShuffleQuestions.ToString().ToLower() %>),
            showErrors: ko.observable(<%= Model.Config.ShowErrors.ToString().ToLower() %>),
            showPlace: ko.observable(<%= Model.Config.ShowPlace.ToString().ToLower() %>),
            showChartOnTimeEnd: ko.observable(<%= Model.Config.ShowChartOnTimeEnd.ToString().ToLower() %>),
            showAxisOnTop: ko.observable(<%= Model.Config.ShowAxisOnTop.ToString().ToLower() %>),
            time: ko.observable('<%= Model.Config.Time %>'),
            active: ko.observable(toBoolean('<%= Model.Config.Active %>')),
            guid: ko.observable('<%= Model.Config.MaterialGuid %>'),
            courseName: ko.observable(checkEmptyCourse('<%= Model.SelectedCourseName %>')),

            clearData: function () {
                if (confirm("Удалить данные о соревновании?")) {
                    $.post("<%= Url.Action("ClearData") %>", function (data) {
                        $.Olimp.showSuccess("Данные удалены");
                    });
                }
            },
            manageCompition: function () {
                var self = this;
                var message = (!self.active()) ? "Начать соревнование?" : "Закончить соревнование?";
                var successMessage = (!self.active()) ? "Соревнование началось" : "Соревнование закончилось";
                if (confirm(message)) {
                    $.post("<%= Url.Action("StartStopCompetition") %>", function (data) {
                        self.active(!self.active());
                        $.Olimp.showSuccess(successMessage);
                    });
                }
            }
        };

        ko.computed(function () {
            $(".materials-list").combobox('option', 'disabled', this.active());
        }, viewModel);

        viewModel.testStatusText = ko.computed(function () {
            var message = (!this.active()) ? "Начать соревнование" : "Закончить соревнование";
            return message;
        }, viewModel);

        ko.applyBindings(viewModel, $('#competitionFormTable')[0]);
    });
</script>