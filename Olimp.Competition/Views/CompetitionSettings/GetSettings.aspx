﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<CompetitionGetSettingsViewModel>"  MasterPageFile="~/Views/Shared/CompetitionAdmin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Olimp_Competition_Js" runat="server">
    <script type="text/javascript">
        $(function () {
            var message = "<%= Model.Message %>";
            if (message != "") {
                $.Olimp.showSuccess(message);
            }
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Olimp_Competition_Content" runat="server">   
    <% Html.EnableClientValidation(); %>
    <% using(Html.BeginForm("Save", "CompetitionSettings", FormMethod.Post, new {enctype = "multipart/form-data"  })) { %>
		<div id="competitionForm">	
			<% Html.RenderAction("CompetitionSettingsForm"); %>
		</div>	
	<% } %>			
</asp:Content>