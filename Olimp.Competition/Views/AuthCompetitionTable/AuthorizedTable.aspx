﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<AuthorizedTableViewModel>" MasterPageFile="~/Views/Shared/AuthUser.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Olimp_Head" runat="server">
    <title>Вход в систему</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Olimp_Content" runat="server">
    <h1>Авторизация столов для соревнования</h1>
    <div class="block" id="authorized-table">
        <table class="form-table">
            <tr class="first">
                <th>Авторизированный стол: </th>
                <td><strong><%= Model.Table.Name %></strong></td>
            </tr>
            <tr class="last">
                <td colspan="2">
                    <%= Html.ActionLink("Продолжить", "RedirectAuthorizedBack",new {returnUrl = ViewData["returnUrl"]})%>
                    <%= Html.ActionLink("Выход", "LogOut",new {tableId = Model.Table.Id, returnUrl = ViewData["returnUrl"]})%>
                </td>
            </tr>
        </table>
    </div>

    <h1>Диаграмма</h1>
    <div class="block" id="chart">
        <%= Html.ActionLink("Диаграмма", "Index",new {controller = "CompetitionChart"})%>
    </div>
    <script>
        $(function () {$('#chart a, #authorized-table a').olimpbutton(); })
    </script>
</asp:Content>


