﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<SelectTableViewModel>" MasterPageFile="~/Views/Shared/AuthUser.master" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="Olimp_Head" runat="server">		
<title>Вход в систему</title>	
</asp:Content>	

<asp:Content ID="Content2" ContentPlaceHolderID="Olimp_Content" runat="server">		
	<% if (!String.IsNullOrEmpty(Model.ErrorMessage)) { %>
			<script type="text/javascript">
			    $.Olimp.showError('<%= Model.ErrorMessage %>');
			</script>
    <% } %>
	
    <% Html.EnableClientValidation(); %>
	
	<% using (Html.BeginForm("SelectTable", "AuthCompetitionTable",  FormMethod.Post)) { %>
	
		<%= Html.Hidden("returnUrl") %>
	
		<h1>Авторизация столов для соревнования</h1>
		<div class="block">
			<table class="form-table">
				<tr class="first">
					<th><%= Html.LabelFor(m => m.TableId) %></th>
					<td>						
						<div><%= Html.DropDownListFor(m => m.TableId, Model.Tables, "") %></div>
						<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.TableId) %></div>
					</td>
				</tr>
				<tr >
					<th><%= Html.LabelFor(m => m.Password) %></th>
					<td>						
						<div><%= Html.PasswordFor(m => m.Password) %></div>
						<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Password) %></div>
					</td>
				</tr>
				<tr class="last" >
					<td colspan="2">
						<button type="submit">Продолжить</button>
					</td>
				</tr>
			</table>			
		</div>	
	<% } %>

    <h1>Диаграмма</h1>
    <div class="block" id="chart">
            <%= Html.ActionLink("Диаграмма", "Index",new {controller = "CompetitionChart"})%>
    </div>
    <script>
        $(function () { $('button').olimpbutton(); $('#chart a').olimpbutton(); })
    </script>
</asp:Content>


