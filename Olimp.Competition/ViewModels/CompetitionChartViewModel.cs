﻿namespace Olimp.Competition.ViewModels
{
    public class CompetitionChartViewModel
    {
        public string ChartCss { get; set; }

        public bool ShowAxisOnTop { get; set; }
    }
}