﻿using Olimp.Core;
using Olimp.Domain.Common.Content;

namespace Olimp.Competition.ViewModels
{
    public class CompetitionTestAnswerViewModel
    {
        public CompetitionTestAnswerViewModel(IAnswer answer)
        {
            this.Id = answer.Id;
            this.Text = answer.Text;
            this.LineThrough = !answer.IsCorrect && OlimpApplication.GodMode;
        }

        public bool LineThrough { get; set; }

        public int Id { get; set; }

        public string Text { get; set; }
    }
}