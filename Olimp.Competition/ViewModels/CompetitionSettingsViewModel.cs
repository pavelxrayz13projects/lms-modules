﻿using System.Collections.Generic;
using System.Web.Mvc;
using Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Competition.ViewModels
{
    public class CompetitionSettingsViewModel
    {
       public CompetitionConfiguration Config { get; set; }

       public string SelectedCourseName { get; set; }

       public IEnumerable<SelectListItem> Courses { get; set; }

    }
}