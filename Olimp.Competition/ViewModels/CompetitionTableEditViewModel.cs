﻿using Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Competition.ViewModels
{
    public class CompetitionTableEditViewModel
    {
        public CompetitionTable Table { get; set; }
    }
}