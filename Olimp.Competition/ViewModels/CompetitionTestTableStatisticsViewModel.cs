﻿using System;

namespace Olimp.Competition.ViewModels
{
    public class CompetitionTestTableStatisticsViewModel
    {
        public TimeSpan TimeLeft { get; set; }

        public int TablePlace { get; set; }

        public int TotalQuestions { get; set; }

        public int CurrentQuestionNumber { get; set; }

        public int IncorrectAnswers { get; set; }
    }
}