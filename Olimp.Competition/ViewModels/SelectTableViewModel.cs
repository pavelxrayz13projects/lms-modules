﻿using Olimp.Core.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.Competition.ViewModels
{
    public class SelectTableViewModel
    {
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.CompetitionTable), DisplayNameResourceName = "Name")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int TableId { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.CompetitionTable), DisplayNameResourceName = "Password")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public string Password { get; set; }

        public IEnumerable<SelectListItem> Tables { get; set; }

        public string ErrorMessage { get; set; }
    }
}