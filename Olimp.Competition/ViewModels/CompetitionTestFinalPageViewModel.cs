﻿namespace Olimp.Competition.ViewModels
{
    public class CompetitionTestFinalPageViewModel
    {
        public string Image { get; set; }

        public string Message { get; set; }
    }
}