﻿using Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Competition.ViewModels
{
    public class AuthorizedTableViewModel
    {
        public CompetitionTable Table { get; set; }
    }
}