﻿using Olimp.Domain.Common.Content;

namespace Olimp.Competition.ViewModels
{
    public class CompetitionTestQuestionViewModel
    {
        public CompetitionTestQuestionViewModel(IQuestion courseQuestion, int id)
        {
            this.QuestionId = id;
            this.Text = courseQuestion.Text;
            this.IsMultiple = courseQuestion.AllowMultiple;
        }

        public CompetitionTestTableStatisticsViewModel Statistics { get; set; }

        public CompetitionTestAnswerViewModel[] Answers{ get; set; }

        public int QuestionId { get; set; }

        public bool ShowErrors { get; set; }

        public bool ShowPlace { get; set; }

        public string Text { get; set; }

        public bool IsMultiple { get; set; }
    }
}