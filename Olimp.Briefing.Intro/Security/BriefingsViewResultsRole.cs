﻿using Olimp.Domain.Catalogue.Security.IntegratedRoles;

namespace Olimp.Briefing.Intro.Security
{
    public class BriefingViewResultsRole : EveryoneRole
    {
        public BriefingViewResultsRole()
            : base(Domain.Catalogue.Security.Permissions.Briefings.Intro.ViewResults)
        {
            this.Id = "BreifingsIntroViewResults";
        }

        public override string Title { get { return I18N.Common.BreifingsIntroViewResults; } }
    }
}