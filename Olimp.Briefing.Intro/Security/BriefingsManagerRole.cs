﻿using Olimp.Domain.Catalogue.Security.IntegratedRoles;

namespace Olimp.Briefing.Intro.Security
{
    public class BriefingsManagerRole : EveryoneRole
    {
        public BriefingsManagerRole()
            : base(Domain.Catalogue.Security.Permissions.Briefings.Intro.Manage)
        {
            this.Id = "BreifingsIntroManager";
        }

        public override string Title { get { return I18N.Common.BreifingsIntroManagerRole; } }
    }
}