﻿using Olimp.Courses.Model;
using Olimp.Domain.Catalogue.Entities;
using PAPI.Core.Data;
using System;
using System.Linq;

namespace Olimp.Briefing.Intro
{
    public class DefaultScormLinkProvider : IScormLinkProvider
    {
        //private IRepository<Course, int> _courseRepo;

        public DefaultScormLinkProvider(IRepositoryFactory repoFactory)
        {
            throw new NotImplementedException();
            /*if (repoFactory == null)
                throw new ArgumentNullException("repoFactory");

            _courseRepo = repoFactory.CreateFor<Course, int>();*/
        }

        private int? GetScormCourseId(Course owner, bool seekAll, string scormName)
        {
            throw new NotImplementedException();
            /*
            if (String.IsNullOrWhiteSpace(scormName))
                return null;

            IQueryable<Course> courses;

            if (seekAll)
            {
                courses = _courseRepo.Where(c => c.IsActive && c.Code == scormName);
            }
            else if (owner.Source != CourseSource.Commercial)
            {
                if (File.Exists(Path.Combine(OlimpPaths.Default.Courses, owner.Path)))
                    return owner.Id;
                else
                    return null;
            }
            else
            {
                if (owner.Code.Contains("_") && owner.Code.Equals(scormName) && System.IO.File.Exists(Path.Combine(OlimpPaths.Default.Courses, owner.Path)))
                    return owner.Id;

                courses = _courseRepo.Where(c => c.IsActive && c.Code == scormName && c.Source == CourseSource.Commercial);
            }

            return courses.AsEnumerable()
                .Where(c => File.Exists(Path.Combine(OlimpPaths.Default.Courses, c.Path)))
                .OrderByDescending(c => c.Id)
                .Select(c => (int?)c.Id)
                .FirstOrDefault();*/
        }

        #region IScormLinkProvider Members

        public string GetLink(Course material, CourseScormReference scorm)
        {
            var link = scorm.Link;

            var seekAll = link.StartsWith("../");
            if (seekAll)
                link = link.Substring(3);

            var scormName = scorm.Link.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();

            var courseId = this.GetScormCourseId(material, seekAll, scormName);
            if (!courseId.HasValue)
                return null;

            if (!link.StartsWith("/"))
                link = "/" + link;

            return String.Format("/!/scorms.ecp/{0}{1}", courseId, link);
        }

        #endregion IScormLinkProvider Members
    }
}