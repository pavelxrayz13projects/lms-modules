﻿using PAPI.Core.Data;
using System;
using System.Collections.Generic;

namespace Olimp.Briefing.Intro.Models
{
    public class BriefingIntroEmployee : Entity<int>
    {
        public BriefingIntroEmployee()
        {
            this.Results = new List<BriefingIntroResult>();
        }

        public virtual string FullName { get; set; }

        public virtual DateTime BirthDate { get; set; }

        public virtual DateTime LoginTimeStamp { get; set; }

        public virtual IList<BriefingIntroResult> Results { get; protected set; }
    }
}