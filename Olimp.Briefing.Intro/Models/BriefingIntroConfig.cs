﻿using Olimp.Configuration;

namespace Olimp.Briefing.Intro.Models
{
    [SettingsContainer]
    public class BriefingIntroConfig
    {
        [ConfigKey("olimp/briefing-intro/allow-briefing")]
        public virtual bool AllowBriefing { get; set; }

        [ConfigKey("olimp/briefing-intro/always-show-kb")]
        public virtual bool AlwaysShowKeyboard { get; set; }

        [ConfigKey("olimp/briefing-intro/display-briefing-passed-btn")]
        public virtual bool DisplayBriefingPassedButton { get; set; }
    }
}