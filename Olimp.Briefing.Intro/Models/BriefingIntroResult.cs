﻿using Olimp.Domain.Exam.NHibernate.Schema;
using PAPI.Core.Data;
using System;

namespace Olimp.Briefing.Intro.Models
{
    public class BriefingIntroResult : Entity<int>
    {
        public virtual bool Finished { get; set; }

        public virtual DateTime Timestamp { get; set; }

        public virtual BriefingIntroEmployee Employee { get; set; }

        public virtual CachedCourseSchema Briefing { get; set; }
    }
}