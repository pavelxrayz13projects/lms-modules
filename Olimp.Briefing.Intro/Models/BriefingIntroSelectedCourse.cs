﻿using PAPI.Core.Data;
using System;

namespace Olimp.Briefing.Intro.Models
{
    public class BriefingIntroSelectedCourse : Entity<int>
    {
        public virtual Guid CourseId { get; set; }
    }
}