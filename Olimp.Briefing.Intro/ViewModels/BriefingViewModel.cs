﻿using System;

namespace Olimp.Briefing.Intro.ViewModels
{
    public class BriefingViewModel
    {
        public Guid Id { get; set; }

        public string Link { get; set; }

        public string Name { get; set; }
    }
}