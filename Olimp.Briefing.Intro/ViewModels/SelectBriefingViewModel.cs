﻿using System.Collections.Generic;

namespace Olimp.Briefing.Intro.ViewModels
{
    public class SelectBriefingViewModel
    {
        public bool RedirectIfNo { get; set; }

        public bool RedirectIfSingle { get; set; }

        public bool AutoSelectIfSingle { get; set; }

        public int EmployeeId { get; set; }

        public IList<BriefingViewModel> Courses { get; set; }
    }
}