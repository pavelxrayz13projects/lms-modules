﻿using System;

namespace Olimp.Briefing.Intro.ViewModels
{
    public class SelectedBriefingViewModel
    {
        public int EmployeeId { get; set; }

        public Guid? BriefingId { get; set; }

        public string BriefingLink { get; set; }

        public bool DisplayBriefingPassedButton { get; set; }
    }
}