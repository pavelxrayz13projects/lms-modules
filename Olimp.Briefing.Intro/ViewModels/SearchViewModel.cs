﻿using Olimp.UI.ViewModels;
using System;

namespace Olimp.Briefing.Intro.ViewModels
{
    public class SearchViewModel : TableViewModel
    {
        public string FullName { get; set; }

        public DateTime? DateStart { get; set; }

        public DateTime? DateEnd { get; set; }
    }
}