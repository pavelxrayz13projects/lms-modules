﻿using System;

namespace Olimp.Briefing.Intro.ViewModels
{
    using Olimp.Core.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using I18N = Olimp.Briefing.Intro.I18N;

    public class CreateUserViewModel
    {
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Auth), DisplayNameResourceName = "FullName")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [StringLength(50, ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "StringLengthInvalid")]
        public string FullName { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Auth), DisplayNameResourceName = "BirthDate")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [Remote("DateFormatValid", "Auth", "BriefingIntro", ErrorMessageResourceType = typeof(I18N.Common), ErrorMessageResourceName = "DateFormatInvalid")]
        public DateTime? BirthDate { get; set; }

        public bool AlwaysShowKeyboard { get; set; }
    }
}