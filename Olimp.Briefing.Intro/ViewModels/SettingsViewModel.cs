﻿using Olimp.Briefing.Intro.Models;
using Olimp.Core.ComponentModel;
using System;
using System.Collections.Generic;

namespace Olimp.Briefing.Intro.ViewModels
{
    public class SettingsViewModel : BriefingIntroConfig
    {
        public SettingsViewModel()
        {
            this.Materials = new List<Guid>();
        }

        public IList<Guid> Materials { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Common), DisplayNameResourceName = "AllowBriefing")]
        public override bool AllowBriefing { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Common), DisplayNameResourceName = "AlwaysShowKeyboard")]
        public override bool AlwaysShowKeyboard { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Common), DisplayNameResourceName = "DisplayBriefingPassedButton")]
        public override bool DisplayBriefingPassedButton { get; set; }
    }
}