﻿using Olimp.Briefing.Intro.Models;
using Olimp.UI.ViewModels;
using System;
using System.Linq.Expressions;

namespace Olimp.Briefing.Intro.TableHandlers
{
    public class OrderByBriefingNameSortingHandler : IExpressionBuilder
    {
        #region IExpressionBuilder Members

        public LambdaExpression Build()
        {
            Expression<Func<BriefingIntroResult, string>> expression =
                r => r.Briefing.Name;

            return expression;
        }

        #endregion IExpressionBuilder Members
    }
}