﻿using Olimp.Briefing.Intro.Models;
using Olimp.UI.ViewModels;
using System;
using System.Linq.Expressions;

namespace Olimp.Briefing.Intro.TableHandlers
{
    public class GroupByFullNameGroupingHandler : IExpressionBuilder
    {
        #region IExpressionBuilder Members

        public LambdaExpression Build()
        {
            Expression<Func<BriefingIntroResult, string>> expression =
                r => r.Employee.FullName + " (" +
                    (r.Employee.BirthDate.Day < 10 ? ("0" + r.Employee.BirthDate.Day.ToString()) : r.Employee.BirthDate.Day.ToString()) + "." +
                    (r.Employee.BirthDate.Month < 10 ? ("0" + r.Employee.BirthDate.Month.ToString()) : r.Employee.BirthDate.Month.ToString()) + "." +
                    r.Employee.BirthDate.Year.ToString() + ")";

            return expression;
        }

        #endregion IExpressionBuilder Members
    }
}