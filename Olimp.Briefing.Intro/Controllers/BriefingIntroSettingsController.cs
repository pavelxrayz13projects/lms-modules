﻿using Olimp.Briefing.Intro.Models;
using Olimp.Briefing.Intro.ViewModels;
using Olimp.Configuration;
using Olimp.Core.Mvc.Security;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Briefing.Intro.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Domain.Catalogue.Security.Permissions.Briefings.Intro.Manage, Order = 2)]
    [CheckActivation]
    public class BriefingIntroSettingsController : PController
    {
        private IBriefingsCatalogueProvider _catalogueProvider;

        public BriefingIntroSettingsController(IBriefingsCatalogueProvider catalogueProvider)
        {
            if (catalogueProvider == null)
                throw new ArgumentNullException("catalogueProvider");

            _catalogueProvider = catalogueProvider;
        }

        [HttpGet]
        public ActionResult Index()
        {
            using (UnitOfWork.CreateRead())
            {
                var ids = Repository.Of<BriefingIntroSelectedCourse>()
                    .Select(c => c.CourseId)
                    .ToList();

                var cfg = Config.Get<BriefingIntroConfig>();

                return View(new SettingsViewModel
                {
                    Materials = ids,
                    AllowBriefing = cfg.AllowBriefing,
                    AlwaysShowKeyboard = cfg.AlwaysShowKeyboard,
                    DisplayBriefingPassedButton = cfg.DisplayBriefingPassedButton
                });
            }
        }

        [HttpPost]
        public ActionResult GetCourses()
        {
            return Json(_catalogueProvider.GetBriefingsCatalogue());
        }

        [HttpPost]
        public ActionResult Save(SettingsViewModel model)
        {
            var selectedCoursesRepo = Repository.Of<BriefingIntroSelectedCourse>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                foreach (var c in selectedCoursesRepo.ToList())
                    selectedCoursesRepo.Delete(c);

                foreach (var id in model.Materials)
                    selectedCoursesRepo.Save(new BriefingIntroSelectedCourse { CourseId = id });

                Config.Set(new BriefingIntroConfig
                {
                    AllowBriefing = model.AllowBriefing,
                    AlwaysShowKeyboard = model.AlwaysShowKeyboard,
                    DisplayBriefingPassedButton = model.DisplayBriefingPassedButton
                });

                uow.Commit();
            }

            return RedirectToAction("Index");
        }
    }
}