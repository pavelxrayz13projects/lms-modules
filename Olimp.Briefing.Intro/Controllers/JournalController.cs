﻿using Olimp.Briefing.Intro.Models;
using Olimp.Briefing.Intro.TableHandlers;
using Olimp.Briefing.Intro.ViewModels;
using Olimp.Core.Mvc.Security;
using Olimp.Domain.Catalogue.Security;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Briefing.Intro.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Briefings.Intro.ViewResults, Order = 2)]
    [CheckActivation]
    public class JournalController : PController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetEmployees(SearchViewModel model)
        {
            model.GroupingHandlers["FullName"] = new GroupByFullNameGroupingHandler();
            model.SortingHandlers["BriefingName"] = new OrderByBriefingNameSortingHandler();

            using (UnitOfWork.CreateRead())
            {
                var employees = Repository.Of<BriefingIntroResult>()
                    .Where(r => r.Finished);

                if (!String.IsNullOrWhiteSpace(model.FullName))
                {
                    var fullName = model.FullName;
                    employees = employees.Where(e => e.Employee.FullName.Contains(fullName));
                }

                var dateStart = model.DateStart.GetValueOrDefault();
                if (dateStart != default(DateTime))
                {
                    dateStart = dateStart.Date;
                    employees = employees.Where(e => e.Timestamp >= dateStart);
                }

                var dateEnd = model.DateEnd.GetValueOrDefault();
                if (dateEnd != default(DateTime))
                {
                    dateEnd = dateEnd.Date.AddDays(1);
                    employees = employees.Where(e => e.Timestamp < dateEnd);
                }

                return Json(employees.GetTableData(model, e => new
                {
                    FullName = String.Format("{0} ({1})", e.Employee.FullName, e.Employee.BirthDate.ToShortDateString()),
                    BriefingName = e.Briefing.Name,
                    Timestamp = e.Timestamp.ToString()
                }));
            }
        }
    }
}