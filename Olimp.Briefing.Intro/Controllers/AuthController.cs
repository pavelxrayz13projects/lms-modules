﻿using Olimp.Briefing.Intro.Models;
using Olimp.Briefing.Intro.ViewModels;
using Olimp.Configuration;
using Olimp.Core.Mvc.Security;
using Olimp.Web.Controllers.Base;
using PAPI.Core.Data;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Briefing.Intro.Controllers
{
    [CheckActivation]
    public class AuthController : PController
    {
        [HttpGet]
        public ActionResult User()
        {
            using (UnitOfWork.CreateRead())
            {
                var alwaysShow = Config.Get<BriefingIntroConfig>().AlwaysShowKeyboard;

                return View(new CreateUserViewModel { AlwaysShowKeyboard = alwaysShow });
            }
        }

        [HttpPost]
        public ActionResult User(CreateUserViewModel model)
        {
            var repo = Repository.Of<BriefingIntroEmployee>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                if (!Config.Get<BriefingIntroConfig>().AllowBriefing)
                    return RedirectToAction("Disabled", "View");

                var employee = repo.FirstOrDefault(e => e.FullName == model.FullName && e.BirthDate == model.BirthDate);
                if (employee == null)
                {
                    employee = new BriefingIntroEmployee
                    {
                        FullName = model.FullName,
                        BirthDate = model.BirthDate.Value,
                    };

                    repo.Save(employee);
                }

                employee.LoginTimeStamp = DateTime.Now;

                uow.Commit();

                return RedirectToAction("Select", "View", new { employeeId = employee.Id });
            }
        }

        [HttpGet]
        public ActionResult DateFormatValid(string birthDate)
        {
            DateTime date;
            var result = DateTime.TryParseExact(birthDate, "dd.MM.yyyy", CultureInfo.CurrentCulture.DateTimeFormat, DateTimeStyles.None, out date);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}