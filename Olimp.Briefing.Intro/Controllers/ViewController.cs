﻿using Olimp.Briefing.Intro.Models;
using Olimp.Briefing.Intro.ViewModels;
using Olimp.Configuration;
using Olimp.Core.Mvc.Security;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Content;
using Olimp.Domain.Exam.NHibernate.Attempts;
using Olimp.Web.Controllers.Base;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Briefing.Intro.Controllers
{
    [CheckActivation(Order = 1)]
    [RequireWorkplaceToken(Order = 2)]
    [LimitUsers(Order = 3)]
    public class ViewController : PController
    {
        private ICourseRepository _courseRepository;
        private ITopicsProviderFactory _topicsProviderFactory;
        private ITopicContentsProvider _topicContentsProvider;
        private CachedCourseFactory _cachedCourseFactory;

        public ViewController(
            ICourseRepository courseRepository,
            ITopicsProviderFactory topicsProviderFactory,
            ITopicContentsProvider topicContentsProvider,
            CachedCourseFactory cachedCourseFactory)
        {
            if (courseRepository == null)
                throw new ArgumentNullException("courseRepository");

            if (topicsProviderFactory == null)
                throw new ArgumentNullException("topicsProviderFactory");

            if (topicContentsProvider == null)
                throw new ArgumentNullException("topicContentsProvider");

            if (cachedCourseFactory == null)
                throw new ArgumentNullException("cachedCourseFactory");

            _courseRepository = courseRepository;
            _topicsProviderFactory = topicsProviderFactory;
            _topicContentsProvider = topicContentsProvider;
            _cachedCourseFactory = cachedCourseFactory;
        }

        public ActionResult Disabled()
        {
            return View();
        }

        public ActionResult Select(SelectBriefingViewModel model)
        {
            if (model.EmployeeId == 0)
                return RedirectToAction("User", "Auth");
            
            Guid[] remainingBriefings;
            using (UnitOfWork.CreateRead())
            {
                var passed = Repository.Of<BriefingIntroEmployee>()
                    .Where(e => e.Id == model.EmployeeId)
                    .SelectMany(e => e.Results)
                    .Where(r => r.Timestamp >= DateTime.Now.AddDays(-1))
                    .Select(r => r.Briefing.UniqueId)
                    .Distinct()
                    .ToList();

                remainingBriefings = Repository.Of<BriefingIntroSelectedCourse>()
                    .Where(c => !passed.Contains(c.CourseId))
                    .Select(c => c.CourseId)
                    .ToArray();
            }

            var briefings = _courseRepository.GetMany(remainingBriefings)
                .Select(c => new
                {
                    Course = c,
                    FirstTopic = _topicsProviderFactory.Create(c).GetFirst()
                })
                .Where(o => o.FirstTopic != null)
                .Select(o => new BriefingViewModel
                {
                    Id = o.Course.MaterialId,
                    Name = o.Course.Name,
                    Link = _topicContentsProvider.GetContents(o.Course, o.FirstTopic)
                        .Scorms
                        .Where(s => s.IsAvailable)
                        .Select(s => s.GetLink("/!/scorms.ecp"))
                        .FirstOrDefault()
                })
                .Where(b => b.Link != null)
                .ToList();

            if ((briefings.Count == 0 && model.RedirectIfNo) || (briefings.Count == 1 && model.RedirectIfSingle))
                return RedirectToAction("User", "Auth");

            if (briefings.Count == 1 && model.AutoSelectIfSingle)
            {
                return RedirectToAction("Index", new
                {
                    employeeId = model.EmployeeId,
                    briefingId = briefings[0].Id,
                    briefingLink = briefings[0].Link
                });
            }
            else
            {
                return View(new SelectBriefingViewModel
                {
                    EmployeeId = model.EmployeeId,
                    Courses = briefings
                });
            }
        }

        public ActionResult Index(SelectedBriefingViewModel model)
        {
            ModelState.Clear();

            using (UnitOfWork.CreateRead())
            {
                var cfg = Config.Get<BriefingIntroConfig>();

                model.DisplayBriefingPassedButton = cfg.DisplayBriefingPassedButton;
            }

            var briefingId = model.BriefingId.GetValueOrDefault();
            if (briefingId == Guid.Empty)
            {
                var parts = model.BriefingLink.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length > 1)
                {
                    model.BriefingId = Guid.Parse(parts[0]);
                    model.BriefingLink = String.Join("|", parts.Skip(1));
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Finish(ConfirmViewModel model)
        {
            if (!model.IsLearnt)
                return this.Confirm(model);

            var course = _courseRepository.GetByUniqueId(model.BriefingId.GetValueOrDefault());

            using (var uow = UnitOfWork.CreateWrite())
            {
                var employee = Repository.Of<BriefingIntroEmployee>()
                    .GetById(model.EmployeeId);

                var result = new BriefingIntroResult
                {
                    Briefing = _cachedCourseFactory.GetOrCreateCachedCourse(course, null),
                    Employee = employee,
                    Finished = true,
                    Timestamp = DateTime.Now
                };

                employee.Results.Add(result);

                uow.Commit();
            }

            return View("PutASign", new SelectBriefingViewModel { EmployeeId = model.EmployeeId, RedirectIfNo = true, AutoSelectIfSingle = false });
        }

        [HttpPost]
        public ActionResult Confirm(SelectedBriefingViewModel model)
        {
            //Must be explicit because called from Finish
            return View("Confirm");
        }
    }
}