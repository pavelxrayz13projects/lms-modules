﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="keyboard" class="keyboard">
    <div class="keyboard-shortcut-wrapper">
        <div class="keyboard-shortcut">
            <div class="keyboard-shortcut-ico"></div>
        </div>
    </div>
    <div class="keyboard-wrapper">
	    <div class="keyboard-row row-1">
	        <button type="button" data-kb-class="numbers" data-kb-val="1"></button>	
	        <button type="button" data-kb-class="numbers" data-kb-val="2"></button>	
	        <button type="button" data-kb-class="numbers" data-kb-val="3"></button>	
	        <button type="button" data-kb-class="numbers" data-kb-val="4"></button>	
	        <button type="button" data-kb-class="numbers" data-kb-val="5"></button>	
	        <button type="button" data-kb-class="numbers" data-kb-val="6"></button>	
	        <button type="button" data-kb-class="numbers" data-kb-val="7"></button>	
	        <button type="button" data-kb-class="numbers" data-kb-val="8"></button>	
	        <button type="button" data-kb-class="numbers" data-kb-val="9"></button>	
	        <button type="button" data-kb-class="numbers" data-kb-val="0"></button>	
	        <button type="button" data-kb-class="chars" data-kb-val="-"></button>
	        <button type="button" data-kb-class="system" data-kb-subclass="erase-backspace" class="keyboard-functional keyboard-backspace">&#x21E6;</button>
	        <button type="button" data-kb-class="system" data-kb-subclass="erase-delete" class="keyboard-functional keyboard-delete" style="">Del</button>	
        </div>
        <div class="keyboard-row row-2">
	        <button type="button" data-kb-class="letters" data-kb-val-ru="й" data-kb-val-en="q"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="ц" data-kb-val-en="w"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="у" data-kb-val-en="e"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="к" data-kb-val-en="r"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="е" data-kb-val-en="t"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="н" data-kb-val-en="y"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="г" data-kb-val-en="u"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="ш" data-kb-val-en="i"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="щ" data-kb-val-en="o"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="з" data-kb-val-en="p"></button>
	        <button type="button" data-kb-class="letters" data-kb-val-ru="х"></button>
	        <button type="button" data-kb-class="letters" data-kb-val-ru="ъ"></button>
	        <button type="button" data-kb-class="system" data-kb-subclass="navigate-home" class="keyboard-functional keyboard-home">Home</button>	        
        </div>
        <div class="keyboard-row row-3">
	        <button type="button" data-kb-class="letters" data-kb-val-ru="ф" data-kb-val-en="a"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="ы" data-kb-val-en="s"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="в" data-kb-val-en="d"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="а" data-kb-val-en="f"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="п" data-kb-val-en="g"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="р" data-kb-val-en="h"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="о" data-kb-val-en="j"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="л" data-kb-val-en="k"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="д" data-kb-val-en="l"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="ж"></button>
	        <button type="button" data-kb-class="letters" data-kb-val-ru="э"></button>
            <button type="button" data-kb-class="system" data-kb-subclass="navigate-end" class="keyboard-functional keyboard-end">End</button>		
        </div>
        <div class="keyboard-row row-4">
	        <button type="button" data-kb-class="system" data-kb-subclass="shift" class="keyboard-functional keyboard-shift keyboard-shift-left">&#x21e7; Shift</button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="я" data-kb-val-en="z"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="ч" data-kb-val-en="x"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="с" data-kb-val-en="c"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="м" data-kb-val-en="v"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="и" data-kb-val-en="b"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="т" data-kb-val-en="n"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="ь" data-kb-val-en="m"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="б"></button>	
	        <button type="button" data-kb-class="letters" data-kb-val-ru="ю"></button>	
	        <button type="button" data-kb-class="chars" data-kb-val="."></button>
	        <button type="button" data-kb-class="system" data-kb-subclass="shift" class="keyboard-functional keyboard-shift keyboard-shift-right">&#x21e7; Shift</button>	
        </div>
        <div class="keyboard-row row-5">
	        <button type="button" data-kb-class="system" data-kb-subclass="lang" class="keyboard-functional keyboard-lang">Рус/Eng</button>		
	        <button type="button" data-kb-class="chars" data-kb-val="&nbsp;" class="keyboard-space"></button>	
	        <button type="button" data-kb-class="system" data-kb-subclass="lang" class="keyboard-functional keyboard-lang">Рус/Eng</button>		
	        <button type="button" data-kb-class="system" data-kb-subclass="navigate-left" class="keyboard-functional keyboard-left">&#x2190;</button>			
	        <button type="button" data-kb-class="system" data-kb-subclass="navigate-right" class="keyboard-functional keyboard-right">&#x2192;</button>		
        </div>
    </div>
</div>