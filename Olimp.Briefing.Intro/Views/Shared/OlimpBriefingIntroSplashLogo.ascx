﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div class="logo" style="margin-top: 25px;">
    <img src="<%= Url.Content("~/Content/Images/briefing_logo_inner.gif") %>" alt="" />
</div>