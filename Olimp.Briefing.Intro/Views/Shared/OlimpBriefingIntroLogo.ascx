﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<a href="<%= Url.RouteUrl("Default") %>" class="logo" >
	<img src="<%= Url.Content("~/Content/Images/briefing_logo_inner.gif") %>" alt="Олимп:ИНСТРУКТАЖ" />
</a>