﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage" MasterPageFile="~/Views/Shared/BriefingIntro.master" %>
<%@ Import Namespace="I18N=Olimp.Briefing.Intro.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_BriefingIntro_Content" runat="server">
    <h1><%= I18N.Common.Journal %></h1>
    <div class="block">
        <div id="search-block">
            <table class="form-table">
                <tr class="first">
                    <th>ФИО</th>
                    <td>
                        <input type="text" data-bind="value: searchData.FullName" />
                    </td>
                </tr>
                <tr>
                    <th>Дата инструктажа</th>
                    <td class="date-span">
                        <span>с:&nbsp;</span>
                        <input type="text" data-bind="value: searchData.DateStart" />
                        <span>&nbsp;по:&nbsp;</span>
                        <input type="text" data-bind="value: searchData.DateEnd" />
                    </td>
                </tr>
                <tr class="last">
                    <td colspan="2">
                        <button type="button" data-bind="click: applyFilter"><%= I18N.Common.Search %></button>
                        <button type="button" data-bind="click: clearFilter"><%= I18N.Common.Clear %></button>
                    </td>
                </tr>
            </table>
        </div>		
        <% Html.Table(new TableOptions {				
            SelectUrl = Url.Action("GetEmployees"),
            BodyTemplateEngine = TemplateEngines.DoT,      
            GroupBy = "FullName",
	        Columns = new[] { 
                new ColumnOptions("BriefingName", "Инструктаж"),	        
	            new ColumnOptions("Timestamp", "Дата инструктажа")
	        }}, new { id = "archive-results" }); %>
    </div>
	
    <script type="text/javascript">
        $(function () {            
            var searchResultsModel = $('#archive-results').data('viewModel');
            var viewModel = {
                searchData: {
                    FullName: ko.observable(),
                    DateStart: ko.observable(),
                    DateEnd: ko.observable()
                },
                applyFilter: function () {
                    var data = ko.mapping.toJS(this.searchData);
                    searchResultsModel.requestData(data);
                },
                clearFilter: function () {
                    this.searchData.FullName("");
                    this.searchData.DateStart("");
                    this.searchData.DateEnd("");
                    searchResultsModel.requestData({});
                }
            };

            var searchBlock = $('#search-block');
                        
            ko.applyBindings(viewModel, searchBlock[0]);
            searchBlock.collapsibleBlock({ headerText: "<%= I18N.Common.JournalSearch %>" });
            searchBlock.find('.date-span input').datepicker({ changeMonth: true, changeYear: true });
            searchBlock.find('.last button').olimpbutton();
        });
    </script>
</asp:Content>