﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<SettingsViewModel>" MasterPageFile="~/Views/Shared/BriefingIntro.master" %>

<asp:Content ContentPlaceHolderID="Olimp_BriefingIntro_Content" runat="server">  

    <% using(Html.BeginForm("Save", "BriefingIntroSettings")) { %>

    <h1>Настройки инструктажа</h1>
    <div class="block">
        <table class="form-table">
            <tr class="first">
                <th>
                    <%= Html.LabelFor(m => m.AllowBriefing) %>
                </th>
                <td class="checkbox">
                    <%= Html.CheckBoxFor(m => m.AllowBriefing) %>
                </td>
            </tr>
            <tr>
                <th>
                    <%= Html.LabelFor(m => m.AlwaysShowKeyboard) %>
                </th>
                <td class="checkbox">
                     <%= Html.CheckBoxFor(m => m.AlwaysShowKeyboard) %>
                </td>
            </tr>
            <tr class="last">
                <th>
                    <%= Html.LabelFor(m => m.DisplayBriefingPassedButton) %>
                </th>
                <td class="checkbox">
                     <%= Html.CheckBoxFor(m => m.DisplayBriefingPassedButton) %>
                </td>
            </tr>
        </table>
    </div>

    <h1>Выберите материалы для вводного инструктажа</h1>
    <div class="block">
        <% Html.Catalogue(
		    new CatalogueOptions {	
			    SelectUrl = Url.Action("GetCourses"),
                CheckBoxesValue = "guid",
			    CheckBoxesName = "Materials",
			    InitialExpanded = false,
			    TemplateEngine = TemplateEngines.DoT,
			    SelectedMaterials = Model.Materials }); 
	    %>
    </div>

    <button type="submit">Сохранить</button>

    <% } %>

    <script type="text/ecmascript">
        $(function () {
            $('button').olimpbutton();
        })
    </script>
</asp:Content>