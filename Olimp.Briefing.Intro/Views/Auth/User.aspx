﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<CreateUserViewModel>" MasterPageFile="~/Views/Shared/BriefingIntroUser.master" %> 

<asp:Content ContentPlaceHolderID="Olimp_BriefingIntroUser_Content" runat="server">
    
    <% Html.EnableClientValidation(); %>

    <div id="briefing-intro-auth-container" class="briefing-intro-container">
        <div id="briefing-intro-logo">
            <img src="<%= Url.Content("~/Content/Images/briefing_logo.png") %>" />
        </div>

        <% using (Html.BeginForm("User", "Auth", FormMethod.Post)) { %>
		
            <h1><%= I18N.Auth.RegistrationTitle %></h1>
            <div class="tip"><%= I18N.Auth.CreateUserShortExplain %></div>		
  
            <table class="form-table">
                <tr class="first">
                    <th><%= Html.LabelFor(m => m.FullName) %></th>
                    <td><%= Html.TextBoxFor(m => m.FullName, new { id = "full-name" })%></td>
                </tr>
                <tr class="last">
                    <th><%= Html.LabelFor(m => m.BirthDate) %></th>
                    <td>						
                        <%= Html.TextBoxFor(m => m.BirthDate, new Dictionary<string, object> { 
                            { "id", "birth-date" },
                            { "data-kb-allow", "numbers,system.erase-*,system.navigate-*,'.'" }
                        }) %>
                    </td>
                </tr>			      
            </table>

            <button type="submit"><%= I18N.Auth.Continue %></button>            
        <% } %>
    </div>

</asp:Content>	

<asp:Content ContentPlaceHolderID="Olimp_BriefingIntroUser_AfterBody" runat="server">
   
    <% Html.RenderPartial("Keyboard"); %>

    <script type="text/javascript">
        $.validator.methods.required = (function (required) {
            return function (value, element, o) {
                return !$(element).hasClass("watermark") && required.apply(this, [value, element, o]);
            };
        })($.validator.methods.required);

        $.validator.setDefaults({ onkeyup: false, onfocusin: false, onfocusout: false, focusInvalid: false  })

        var hideAllWatermarks = $.watermark.hideAll;
        $.watermark.hideAll = function () {
            if (!$('form').validate().errorList.length)
                hideAllWatermarks()
        }
        
        $(function () {
            $('#briefing-intro-auth-container button').olimpbutton().addClass('gradient');
            $('#keyboard').olimpkeyboard().find('button').olimpbutton();

            $('#keyboard .keyboard-shortcut').click(function () { $('body').toggleClass('keyboard-visible'); })

            $('#full-name').watermark('<%= I18N.Auth.FullNameWatermark %>');
            $('#birth-date').watermark('<%= I18N.Auth.BirthDateWatermark %>');                

            $("form").bind("invalid-form.validate", function () {
                var errors = $(this).validate().errorList,
                    message = '';
                if (!errors.length)
                    return;

                for (var i = 0; i < errors.length; i++)
                    message += "<div>" + errors[i].message + "</div>"

                $.Olimp.showError(message);
            });

            <% if (Model.AlwaysShowKeyboard) { %>
                $('body').addClass('keyboard-visible');
            <% } %>
        })
    </script>
</asp:Content>


