﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<SelectedBriefingViewModel>" MasterPageFile="~/Views/Shared/Popup.master" %>

<asp:Content ContentPlaceHolderID="Olimp_InitMetas" runat="server">
    <% ViewContext.ViewData["X-UA-Compatible"] = "IE=EmulateIE7"; %>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="Olimp_Js" runat="server">
    <%= "<!--[if lte IE 8]>" %>
    <% Platform.RenderExtensions("/olimp/briefing-intro/user-layout/js-ie-lte8"); %>
    <%= "<![endif]-->" %>    
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Css" runat="server">
    <% Platform.RenderExtensions("/olimp/briefing-intro/user-layout/css"); %>	    
    
    <style type="text/css">
        body { overflow: hidden; }
    </style>

    <%= "<!--[if lte IE 8]>" %>
    <style type="text/css">
        .touch-button-bar .touch-button { behavior: url(<%= Url.VersionSuffixedContent("~/Content/PIE-2.0beta1.htc)") %>); }
    </style>
    <%= "<![endif]-->" %>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Body" runat="server">    
    <script type="text/javascript">
        function doFinish() { $('#finish-form').submit(); }

        function goBack() { $('#back-form').submit(); }

        function resizeFrame(frame, innerWrapper) {
            var frameWindow = $(frame[0].contentDocument || frame[0].contentWindow.document),
                frameWindowHeight = frameWindow.outerHeight(),
                buttonBar = innerWrapper.find('.touch-button-bar'),
                loader = $('#scorm-frame-loader').hide();

            innerWrapper.css({
                width: frameWindow.outerWidth() || frame[0].contentWindow.outerWidth,
                height: frameWindowHeight || frame[0].contentWindow.outerHeight,
                marginTop: -(frameWindowHeight / 2) | 0
            });

            frame.css({ width: '100%', height: '100%' });

            var distance = (buttonBar.offset().left - innerWrapper.offset().left) | 0,
                minWidth = distance + (buttonBar.outerWidth() * 2);

            $('body').css({
                'min-height': frameWindowHeight + 4 /*borders*/,
                'min-width': minWidth + 8 /*borders x2*/ + 32 /*empty space x2*/
            });
        }

        $(document).ready(function () {
            var frameWrapper = $('#scorm-frame-wrapper'),
                innerWrapper = frameWrapper.find('.scorm-frame-inner-wrapper'),
                frame = frameWrapper.find('iframe'),
                frameSrc = frame[0].src;
            
                frame.load(function () {
                    setTimeout(function () { resizeFrame(frame, innerWrapper.show()) }, 0);
                })

            if ($.browser.safari || $.browser.opera) {
                frame[0].src = '';
                frame[0].src = frameSrc;
            }
        })       
    </script>
        
    <% using (Html.BeginForm("Confirm", "View", FormMethod.Post, new { id = "finish-form" })) {  %>
        <%= Html.HiddenFor(m => m.EmployeeId) %>
        <%= Html.HiddenFor(m => m.BriefingId) %>
        <%= Html.HiddenFor(m => m.BriefingLink) %>
    <% } %>

    <% using (Html.BeginForm("Select", "View", FormMethod.Post, new { id = "back-form" })) {  %>
        <%= Html.HiddenFor(m => m.EmployeeId) %>
        <input type="hidden" name="RedirectIfNo" value="true" />
        <input type="hidden" name="RedirectIfSingle" value="true" />
    <% } %>

    <div id="scorm-frame-loader">
        <img src="<%= Url.Content("~/Content/Images/briefing-intro-loader.gif") %>" />
    </div>

    <div id="scorm-frame-wrapper">
        <div class="scorm-frame-inner-wrapper" style="display: none;">            
            <iframe src="<%= Model.BriefingLink %>" scrolling="no" frameBorder="0"></iframe>            
            <div class="touch-button-bar">
                <div class="touch-button touch-button-home" onclick="goBack();"></div>
                <% if (Model.DisplayBriefingPassedButton) { %>    
                    <div class="touch-button touch-button-finish" onclick="doFinish();"></div>
                <% } %>
            </div>
        </div>        
    </div>
            
</asp:Content>