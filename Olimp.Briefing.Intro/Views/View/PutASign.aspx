﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<SelectBriefingViewModel>" MasterPageFile="~/Views/Shared/BriefingIntroUser.master" %> 

<asp:Content ContentPlaceHolderID="Olimp_BriefingIntroUser_Content" runat="server">
    <script type="text/javascript">
        function redirect() { $('#select-form').submit(); }

        $(function () {
            $(document).click(function () { redirect(); })

            setTimeout(redirect, 20000);
        })
    </script>

    <div id="briefing-intro-dont-forget-container">
        <div id="briefing-intro-logo">
            <img src="<%= Url.Content("~/Content/Images/briefing_logo.png") %>" />
        </div>

        <h1><%= I18N.Common.DontForget %></h1>
        <div class="tip"><%= I18N.Common.DontForgetTip %></div>
        <img src="<%= Url.Content("~/Content/Images/briefing-intro-put-a-sign.gif") %>" />
    </div>

    <% using(Html.BeginForm("Select", "View", FormMethod.Post, new { id = "select-form" })) { %>
        <%= Html.HiddenFor(m => m.EmployeeId) %>
        <%= Html.HiddenFor(m => m.RedirectIfNo) %>
        <%= Html.HiddenFor(m => m.AutoSelectIfSingle) %>
    <% } %>   
</asp:Content>	


