﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage" MasterPageFile="~/Views/Shared/BriefingIntroUser.master" %> 

<asp:Content ContentPlaceHolderID="Olimp_BriefingIntroUser_Content" runat="server">
    <script type="text/javascript">
        function redirect() { window.location = '<%= Url.Action("User", "Auth") %>'; }

        $(function () {
            $(document).click(function () { redirect() })

            setTimeout(redirect, 20000);
        })
    </script>

    <div id="briefing-intro-disable-container">
        <div id="briefing-intro-logo">
            <img src="<%= Url.Content("~/Content/Images/briefing_logo.png") %>" />
        </div>

        <h1><%= I18N.Common.BriefingDisabled %></h1>
    </div>
</asp:Content>	


