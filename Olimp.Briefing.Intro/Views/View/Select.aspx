﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<SelectBriefingViewModel>" MasterPageFile="~/Views/Shared/BriefingIntroUser.master" %> 

<asp:Content ContentPlaceHolderID="Olimp_BriefingIntroUser_Content" runat="server">
    
    <% Html.EnableClientValidation(); %>

    <div id="briefing-intro-select-container" class="briefing-intro-container">
        <div id="briefing-intro-logo">
            <img src="<%= Url.Content("~/Content/Images/briefing_logo.png") %>" />
        </div>

        <% if (Model.Courses.Count > 0) { %>
           <% using (Html.BeginForm("Index", "View", FormMethod.Post)) { %>
                <%= Html.HiddenFor(m => m.EmployeeId) %>

                <h1><%= I18N.Common.SelectTitle %></h1>
  
                <ul id="briefing-intro-briefings" class="briefing-intro-check-list">            
                    <% foreach (var b in Model.Courses) { %>
                        <li>
                            <table>
                                <tr>
                                    <td class="checker">
                                        <input type="radio" name="briefingLink" value="<%= String.Join("|", b.Id, b.Link) %>" />
                                    </td>
                                    <td class="text">
                                        <label><%= b.Name %></label>
                                    </td>
                                </tr>               
                            </table>
                        </li>
                    <% } %>
                </ul>
                
                <button id="do-exit" type="button"><%= I18N.Common.DoExit %></button>    
                <button type="submit"><%= I18N.Common.DoPass %></button>            
            <% } %>   
        <% } else { %>
            <h1><%= I18N.Common.NoBriefings %></h1>

            <script type="text/javascript">
                function redirect() { window.location = '<%= Url.Action("User", "Auth") %>'; }

                $(function () {
                    $(document).click(function () { redirect(); })

                    setTimeout(redirect, 20000);
                })
            </script>
        <% } %>
    </div>

    <script type="text/javascript">
        $(function () {
            $('button').olimpbutton().addClass('gradient')
                .filter(':submit').olimpbutton('option', 'disabled', true);

            $('#briefing-intro-briefings input').olimpcheckradio({ clickable: false }).bind('checkedchanged', function (ev, checked) {
                var parent = $(this).parents(3);
                if (checked) {
                    parent.addClass("selected");
                    $(':submit').olimpbutton('option', 'disabled', false);
                } else {
                    parent.removeClass("selected");
                }
            });

            $('#briefing-intro-briefings li').click(function (ev) {
                $(this).find('input').olimpcheckradio('emulateClick');
            })

            $('#do-exit').click(function () { window.location = '<%= Url.Action("User", "Auth") %>' })
        })
    </script>

</asp:Content>	


