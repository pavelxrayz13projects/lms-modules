﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<ConfirmViewModel>" MasterPageFile="~/Views/Shared/BriefingIntroUser.master" %> 

<asp:Content ContentPlaceHolderID="Olimp_BriefingIntroUser_Content" runat="server">
    
    <% Html.EnableClientValidation(); %>

    <div id="briefing-intro-confirm-container" class="briefing-intro-container">
        <div id="briefing-intro-logo">
            <img src="<%= Url.Content("~/Content/Images/briefing_logo.png") %>" />
        </div>

        <% using (Html.BeginForm("Index", "View", FormMethod.Post, new { id = "again-form" })) { %>
            <%= Html.HiddenFor(m => m.EmployeeId) %>
            <%= Html.HiddenFor(m => m.BriefingId) %>
            <%= Html.HiddenFor(m => m.BriefingLink) %>
        <% } %>

        <% using (Html.BeginForm("Finish", "View", FormMethod.Post)) { %>
            <%= Html.HiddenFor(m => m.EmployeeId) %>
            <%= Html.HiddenFor(m => m.BriefingId) %>
        		   
            <h1><%= I18N.Common.ConfirmTitle %></h1>
            <div class="tip"><%= I18N.Common.ConfirmTip %></div>		
  
            <ul id="briefing-intro-check" class="briefing-intro-check-list">            
                <li>
                    <table>
                        <tr>
                            <td class="checker">                                
                                <input type="checkbox" name="IsLearnt" value="true" />
                            </td>
                            <td class="text">
                                <label><%= I18N.Common.IsLearnt %></label>
                            </td>
                        </tr>               
                    </table>
                </li>
            </ul>

             <input type="hidden" name="IsLearnt" value="false" />
            
            <button id="confirm-submit" type="submit" disabled="disabled"><%= I18N.Common.DoConfirm %></button>            
            <button id="again-button" type="button"><%= I18N.Common.DoPassAgain %></button>            
        <% } %>
    </div>

    <script type="text/javascript">
        $(function () {
            $('button').olimpbutton().addClass('gradient');

            $('#briefing-intro-check input').olimpcheckradio({ clickable: false }).bind('checkedchanged', function (ev, checked) {
                var parent = $(this).parents(3);

                if (checked)
                    parent.addClass("selected");
                else
                    parent.removeClass("selected");

                $('#confirm-submit').olimpbutton('option', 'disabled', !checked);
            });

            $('#briefing-intro-check li').click(function (ev) {
                $(this).find('input').olimpcheckradio('emulateClick');
            })

            $('#again-button').click(function () { $('#again-form').submit() });
        })
    </script>
</asp:Content>	


