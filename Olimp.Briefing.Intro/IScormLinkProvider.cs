﻿using Olimp.Courses.Model;
using Olimp.Domain.Catalogue.Entities;

namespace Olimp.Briefing.Intro
{
    public interface IScormLinkProvider
    {
        string GetLink(Course material, CourseScormReference scorm);
    }
}