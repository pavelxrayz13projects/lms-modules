using Olimp.Briefing.Intro.Controllers;
using Olimp.Briefing.Intro.Models;
using Olimp.Briefing.Intro.Security;
using Olimp.Configuration;
using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.Content;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Catalogue;
using Olimp.Domain.Catalogue.OrmLite.Content;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Catalogue.Security.IntegratedRoles;
using Olimp.Domain.Common.Security;
using Olimp.Domain.Exam.NHibernate.Attempts;
using PAPI.Core;
using PAPI.Core.Data;
using PAPI.Core.Data.NHibernate;
using PAPI.Core.Initialization;
using System;
using System.Collections.Generic;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Briefing.Intro.Plugin))]

namespace Olimp.Briefing.Intro
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        { }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["BriefingIntro"].RegisterControllers(
                typeof(AuthController),
                typeof(ViewController));

            areas["Admin"].RegisterControllers(
                typeof(BriefingIntroSettingsController),
                typeof(JournalController));

            areas["BriefingIntro"].Context
                .MapDefault(new { controller = "Auth", action = "User", id = "" });
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (this.Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            this.RegisterPermissions(RolesCatalogue.Roles);

            var olimpModule = PContext.ExecutingModule as OlimpMaterialsModule;
            if (olimpModule == null)
                throw new InvalidOperationException("Compatible only with OlimpMaterialsModule");

            var facadeRepository = olimpModule.Materials.CourseRepository;

            var connFactoryFactory = new SqliteConnectionFactoryFactory();
            var connFactory = connFactoryFactory.Create();

            var topicsProviderFactory = new OrmLiteTopicProviderFactory(facadeRepository);

            var nodesRepository = new OrmLiteNodeRepository(connFactory);
            var courseRepository = new OrmLiteCourseRepository(this.Context.NodeData, connFactory, facadeRepository, nodesRepository);

            var topicContentsProvider = new OrmLiteTopicContentsProvider(courseRepository, facadeRepository);

            var cachedCourseFactory = new CachedCourseFactory(olimpModule.Database.RepositoryFactory, facadeRepository);

            binder.Controller<ViewController>(b => b
                .Bind<ITopicsProviderFactory>(() => topicsProviderFactory)
                .Bind<ICourseRepository>(() => courseRepository)
                .Bind<ITopicContentsProvider>(() => topicContentsProvider)
                .Bind<CachedCourseFactory>(() => cachedCourseFactory));

            var catalogueTreeBuilder = new DefaultCatalogueTreeBuilder();

            var briefingsCatalogueProvider = new OrmLiteBriefingsCatalogueProvider(connFactory, catalogueTreeBuilder, this.Context.NodeData.Id);

            binder.Controller<BriefingIntroSettingsController>(b => b
                .Bind<IBriefingsCatalogueProvider>(() => briefingsCatalogueProvider));
        }

        protected virtual void RegisterPermissions(IDictionary<string, Role> roles)
        {
            var briefingsManagerRole = new BriefingsManagerRole();
            var briefingsViewResults = new BriefingViewResultsRole();

            Role teacherMetaRole;
            if (roles.TryGetValue(TeacherMetaRole.RoleId, out teacherMetaRole))
            {
                teacherMetaRole.AddPermissionsOfRole(briefingsManagerRole);
                teacherMetaRole.AddPermissionsOfRole(briefingsViewResults);
            }

            RolesCatalogue.Add(briefingsManagerRole);
            RolesCatalogue.Add(briefingsViewResults);
        }

        public override void InitializeModule(ISessionManager sessionManager, IRepositoryFactory repositoryFactory)
        {
            Config.Set(new BriefingIntroConfig { AllowBriefing = true }, sessionManager);
        }
    }
}