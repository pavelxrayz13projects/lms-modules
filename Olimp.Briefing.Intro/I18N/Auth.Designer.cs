﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Olimp.Briefing.Intro.I18N {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Auth {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Auth() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Olimp.Briefing.Intro.I18N.Auth", typeof(Auth).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Неверный логин и/или пароль.
        /// </summary>
        public static string AuthError {
            get {
                return ResourceManager.GetString("AuthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дата рождения.
        /// </summary>
        public static string BirthDate {
            get {
                return ResourceManager.GetString("BirthDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ДД.ММ.ГГГГ.
        /// </summary>
        public static string BirthDateWatermark {
            get {
                return ResourceManager.GetString("BirthDateWatermark", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ПРОДОЛЖИТЬ.
        /// </summary>
        public static string Continue {
            get {
                return ResourceManager.GetString("Continue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Для начала экзамена требуется указать Ваши фамилию, отчество, наименование организации и должность.
        /// </summary>
        public static string CreateUserLongExplain {
            get {
                return ResourceManager.GetString("CreateUserLongExplain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Для начала инструктажа требуется указать Ваши фамилию, имя, отчество и дату рождения.
        /// </summary>
        public static string CreateUserShortExplain {
            get {
                return ResourceManager.GetString("CreateUserShortExplain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Используйте клавишу TAB или мышь для перехода между полями формы.
        /// </summary>
        public static string CreateUserTip {
            get {
                return ResourceManager.GetString("CreateUserTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ФИО.
        /// </summary>
        public static string FullName {
            get {
                return ResourceManager.GetString("FullName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Иванов Иван Иванович.
        /// </summary>
        public static string FullNameWatermark {
            get {
                return ResourceManager.GetString("FullNameWatermark", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Группа.
        /// </summary>
        public static string Group {
            get {
                return ResourceManager.GetString("Group", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Логин.
        /// </summary>
        public static string Login {
            get {
                return ResourceManager.GetString("Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вход в систему.
        /// </summary>
        public static string LoginSystemTitle {
            get {
                return ResourceManager.GetString("LoginSystemTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Пароль.
        /// </summary>
        public static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Регистрация.
        /// </summary>
        public static string RegistrationTitle {
            get {
                return ResourceManager.GetString("RegistrationTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите организацию....
        /// </summary>
        public static string SelectCompany {
            get {
                return ResourceManager.GetString("SelectCompany", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите группу....
        /// </summary>
        public static string SelectGroup {
            get {
                return ResourceManager.GetString("SelectGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите пользователя....
        /// </summary>
        public static string SelectUser {
            get {
                return ResourceManager.GetString("SelectUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Пользователь.
        /// </summary>
        public static string User {
            get {
                return ResourceManager.GetString("User", resourceCulture);
            }
        }
    }
}
