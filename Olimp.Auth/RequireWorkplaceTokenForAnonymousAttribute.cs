﻿using Olimp.Core.Mvc.Security;
using Olimp.Domain.LearningCenter.Caching;
using Olimp.Domain.LearningCenter.NotPersisted;
using System;

namespace Olimp.Auth
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class RequireWorkplaceTokenForAnonymousAttribute : RequireWorkplaceTokenAttribute
    {
        public override void OnActionExecuting(System.Web.Mvc.ActionExecutingContext filterContext)
        {
            if (ProfileCache.RegistrationType == RegistrationType.Anonymous)
                base.OnActionExecuting(filterContext);
        }
    }
}