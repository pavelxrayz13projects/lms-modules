﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<AuthSystemUserViewModel>" MasterPageFile="~/Views/Shared/SimpleLayout.master" %> 
<%@ Import Namespace="I18N=Olimp.Auth.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Head" runat="server">		
<title>Вход в систему</title>	
</asp:Content>	

<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">
	
	<% Html.EnableClientValidation(); %>
	
	<% using (Html.BeginForm("Admin", "Auth", FormMethod.Post)) { %>
	
		<%= Html.HiddenFor(m => m.ReturnUrl) %>
	
		<h1><%= I18N.Auth.LoginSystemTitle %></h1>
		<% if (!String.IsNullOrEmpty(Model.ErrorMessage)) { %>
			<script type="text/javascript">
			$.Olimp.showError('<%= Model.ErrorMessage %>');
			</script>
		<% } %>
	
		<% Html.RenderPartial("AuthForm", Model); %>
	<% } %>
	
</asp:Content>	


