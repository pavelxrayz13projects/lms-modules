﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<IAuthViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Auth.I18N" %>

<div class="block">
	<table class="form-table">
		<tr class="first">
			<th><%= Html.LabelFor(m => m.Login) %></th>
			<td>						
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Login) %></div>		
				<%= Html.TextBoxFor(m => m.Login) %>
			</td>
		</tr>
		<tr>
			<th><%= Html.LabelFor(m => m.Password) %></th>
			<td>						
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Password) %></div>
				<%= Html.TextBoxFor(m => m.Password, new { type = "password" }) %>
			</td>
		</tr>
		<tr class="last" >
			<td colspan="2">
				<button type="submit"><%= I18N.Auth.Continue %></button>
			</td>
		</tr>
	</table>			
</div>	

<script>
$(function() { $('button').olimpbutton() })	
</script>