﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GivenNameRequiredViewModel>" %>

<tr>
	<th><%= Html.LabelFor(m => m.GivenName) %> (*)</th>
	<td>						
		<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.GivenName) %></div>
		<div><%= Html.TextBoxFor(m => m.GivenName) %></div>
	</td>
</tr>
