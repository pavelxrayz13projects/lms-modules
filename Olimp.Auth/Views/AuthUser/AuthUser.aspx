﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<AuthUserViewModel>" MasterPageFile="~/Views/Shared/AuthUser.master" %> 
<%@ Import Namespace="I18N=Olimp.Auth.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Head" runat="server">		
<title>Вход в систему</title>	
</asp:Content>	

<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">
	
	<% Html.EnableClientValidation(); %>
	
	<% using (Html.BeginForm("User", "Auth", FormMethod.Post)) { %>
		<%= Html.Hidden("returnUrl") %>
		<%= Html.HiddenFor(m => m.GroupId) %>
	
		<h1><%= I18N.Auth.RegistrationTitle %></h1>
		
		<% if (!String.IsNullOrEmpty(Model.ErrorMessage)) { %>
			<script type="text/javascript">
			$.Olimp.showError('<%= HttpUtility.JavaScriptStringEncode(Model.ErrorMessage) %>');
			</script>
		<% } %>
	
		<% Html.RenderPartial("AuthForm", Model); %>
	<% } %>
	
</asp:Content>	


