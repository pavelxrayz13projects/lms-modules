﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<SelectGroupViewModel>" MasterPageFile="~/Views/Shared/AuthUser.master" %> 
<%@ Import Namespace="I18N=Olimp.Auth.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Head" runat="server">		
<title>Вход в систему</title>	
</asp:Content>	

<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">		
	
	<% Html.EnableClientValidation(); %>
	
	<% using (Html.BeginForm("SelectGroup", "Auth", FormMethod.Post)) { %>
	
		<%= Html.Hidden("returnUrl") %>
	
		<h1><%= I18N.Auth.RegistrationTitle %></h1>
		<div class="block">
			<table class="form-table">
				<tr class="first">
					<th><%= Html.LabelFor(m => m.GroupId) %></th>
					<td>						
						<div><%= Html.DropDownListFor(m => m.GroupId, Model.Groups, I18N.Auth.SelectGroup) %></div>
						<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.GroupId) %></div>
					</td>
				</tr>
				<tr class="last" >
					<td colspan="2">
						<button type="submit"><%= I18N.Auth.Continue %></button>
					</td>
				</tr>
			</table>			
		</div>
	
		<script>
		$(function() { $('button').olimpbutton() })	
		</script>
	
	<% } %>
</asp:Content>


