﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<CreateUserViewModel>" MasterPageFile="~/Views/Shared/AuthUser.master" %> 
<%@ Import Namespace="I18N=Olimp.Auth.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Head" runat="server">		
<title>Вход в систему</title>	
</asp:Content>	

<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">

	<% Html.EnableClientValidation(); %>
	
	<% using (Html.BeginForm("User", "Auth", FormMethod.Post)) { %>
		
		<%= Html.Hidden("returnUrl") %>
	
		<h1><%= I18N.Auth.RegistrationTitle %></h1>
		<% if (Model is CreateUserLongViewModel) {%>
			<div class="explain"><%= I18N.Auth.CreateUserLongExplain %></div>
		<% } else { %>
			<div class="explain"><%= I18N.Auth.CreateUserShortExplain %></div>
		<% } %>
		<div class="tip"><%= I18N.Auth.CreateUserTip %></div>
		<div class="block">
			<table class="form-table">
				<tr class="first">
					<th><%= Html.LabelFor(m => m.Surname) %></th>
					<td>						
						<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Surname) %></div>
						<div><%= Html.TextBoxFor(m => m.Surname) %></div>
					</td>
				</tr>
				<tr>
					<th><%= Html.LabelFor(m => m.Name) %></th>
					<td>						
						<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Name) %></div>
						<div><%= Html.TextBoxFor(m => m.Name) %></div>
					</td>
				</tr>
				<% if (Model.GivenNameRequired) { %>
					<% Html.RenderPartial("GivenNameRequired", new GivenNameRequiredViewModel { GivenName = Model.GivenName }); %>
				<% } else { %>
					<% Html.RenderPartial("GivenName", new GivenNameViewModel { GivenName = Model.GivenName }); %>
				<% } %>
		
				<% if (Model is CreateUserLongViewModel) {%>	
					<% Html.RenderPartial("CreateUserAdditionalFields", Model); %>
				<% } %>
		
				<tr class="last" >
					<td colspan="2">
						<button type="submit"><%= I18N.Auth.Continue %></button>
					</td>
				</tr>
			</table>
		</div>
	
		<script>
		$(function() { $('button').olimpbutton() })	
		</script>
	
	<% } %>
	
</asp:Content>	


