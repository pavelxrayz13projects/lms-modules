﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<CreateUserLongViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Auth.I18N" %>

<tr>
	<th><%= Html.LabelFor(m => m.CompanyId) %></th>
	<td>						
		<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.CompanyId) %></div>
		<div><%= Html.DropDownListFor(m => m.CompanyId, Model.Companies, I18N.Auth.SelectCompany) %></div>
	</td>
</tr>
<tr>
	<th><%= Html.LabelFor(m => m.AppointmentName) %></th>
	<td>						
		<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.AppointmentName) %></div>
		<div><%= Html.TextBoxFor(m => m.AppointmentName) %></div>
	</td>
</tr>
