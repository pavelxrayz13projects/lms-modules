﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Olimp.Auth.I18N {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Auth {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Auth() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Olimp.Auth.I18N.Auth", typeof(Auth).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Неверный логин и/или пароль.
        /// </summary>
        public static string AuthError {
            get {
                return ResourceManager.GetString("AuthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Указан неверный логин и/или пароль, либо пользователь &apos;{0}&apos; не является членом выбранной группы..
        /// </summary>
        public static string AuthErrorOrUserIsNotAMemberOfTheGroup {
            get {
                return ResourceManager.GetString("AuthErrorOrUserIsNotAMemberOfTheGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ^[A-Za-zА-Яа-я-]{2,50}$.
        /// </summary>
        public static string CheckFullNameRegex {
            get {
                return ResourceManager.GetString("CheckFullNameRegex", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Продолжить.
        /// </summary>
        public static string Continue {
            get {
                return ResourceManager.GetString("Continue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Для начала экзамена требуется указать Ваши фамилию, имя, отчество, наименование организации и должность.
        /// </summary>
        public static string CreateUserLongExplain {
            get {
                return ResourceManager.GetString("CreateUserLongExplain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Для начала экзамена требуется указать Ваши фамилию, имя и отчество.
        /// </summary>
        public static string CreateUserShortExplain {
            get {
                return ResourceManager.GetString("CreateUserShortExplain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Используйте клавишу TAB или мышь для перехода между полями формы.
        /// </summary>
        public static string CreateUserTip {
            get {
                return ResourceManager.GetString("CreateUserTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поле &quot;{0}&quot; может содержать только буквы одного алфавита и иметь длину от 2 до 50 символов.
        /// </summary>
        public static string GivenNameInvalid {
            get {
                return ResourceManager.GetString("GivenNameInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Группа.
        /// </summary>
        public static string Group {
            get {
                return ResourceManager.GetString("Group", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Логин.
        /// </summary>
        public static string Login {
            get {
                return ResourceManager.GetString("Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вход в систему.
        /// </summary>
        public static string LoginSystemTitle {
            get {
                return ResourceManager.GetString("LoginSystemTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поле &quot;{0}&quot; может содержать только буквы одного алфавита или пробел и иметь длину от 2 до 50 символов.
        /// </summary>
        public static string NameInvalid {
            get {
                return ResourceManager.GetString("NameInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Пароль.
        /// </summary>
        public static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Регистрация.
        /// </summary>
        public static string RegistrationTitle {
            get {
                return ResourceManager.GetString("RegistrationTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите организацию....
        /// </summary>
        public static string SelectCompany {
            get {
                return ResourceManager.GetString("SelectCompany", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите группу....
        /// </summary>
        public static string SelectGroup {
            get {
                return ResourceManager.GetString("SelectGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите пользователя....
        /// </summary>
        public static string SelectUser {
            get {
                return ResourceManager.GetString("SelectUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поле &quot;{0}&quot; может содержать только буквы одного алфавита, пробел или дефис и иметь длину от 2 до 50 символов.
        /// </summary>
        public static string SurnameInvalid {
            get {
                return ResourceManager.GetString("SurnameInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to №{0}.
        /// </summary>
        public static string UnregisteredNamePattern {
            get {
                return ResourceManager.GetString("UnregisteredNamePattern", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Незарегистрированный пользователь.
        /// </summary>
        public static string UnregisteredSurname {
            get {
                return ResourceManager.GetString("UnregisteredSurname", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Пользователь.
        /// </summary>
        public static string User {
            get {
                return ResourceManager.GetString("User", resourceCulture);
            }
        }
    }
}
