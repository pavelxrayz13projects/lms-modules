﻿using Olimp.Domain.Common.Security;
using Olimp.Domain.LearningCenter.Auth;
using Olimp.Domain.LearningCenter.Entities;
using System;

namespace Olimp.Auth
{
    public class DynamicEmployeeAuthenticator : IEmployeeAuthenticator
    {
        private readonly Func<string, string, AuthenticationResult<Employee>> _authFunc;

        public DynamicEmployeeAuthenticator(Func<string, string, AuthenticationResult<Employee>> authFunc)
        {
            if (authFunc == null)
                throw new ArgumentNullException("authFunc");

            _authFunc = authFunc;
        }

        public AuthenticationResult<Employee> Authenticate(string login, string password)
        {
            return _authFunc(login, password);
        }
    }
}