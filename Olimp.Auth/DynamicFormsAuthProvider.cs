﻿using Olimp.Core.Web;
using System;
using System.Web.Security;

namespace Olimp.Auth
{
    public class DynamicFormsAuthProvider : ICurrentFormsAuthProvider
    {
        private readonly Func<ICurrentFormsAuthProvider> _authFormProviderSwitcher;

        public DynamicFormsAuthProvider(
            Func<ICurrentFormsAuthProvider> authFormProviderSwitcher)
        {
            if (authFormProviderSwitcher == null)
                throw new ArgumentNullException("authFormProviderSwitcher");

            _authFormProviderSwitcher = authFormProviderSwitcher;
        }

        private ICurrentFormsAuthProvider FormsProvider
        {
            get
            {
                var provider = _authFormProviderSwitcher();
                if (provider == null)
                    throw new InvalidOperationException("authFormProviderSwitcher has returned null");
                
                return provider;
            }
        }

        #region ICurrentFormsAuthProvider Members

        public MembershipProvider CurrentMembershipProvider
        {
            get { return FormsProvider.CurrentMembershipProvider; }
        }

        public RoleProvider CurrentRoleProvider
        {
            get { return FormsProvider.CurrentRoleProvider; }
        }

        #endregion ICurrentFormsAuthProvider Members
    }
}