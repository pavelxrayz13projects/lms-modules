using Olimp.Core.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Olimp.Auth.ViewModels
{
    using I18N = Olimp.I18N.Domain;

    public class CreateUserViewModel
    {
        private const string NameRegex = @"^([\u0401\u0451\u0410-\u044F\ ]{2,50})|([\u0041-\u005a\u0061-\u007a\ ]{2,50})$";
        private const string SurnameRegex = @"^([\u0401\u0451\u0410-\u044F\ -]{2,50})|([\u0041-\u005a\u0061-\u007a\ -]{2,50})$";

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Employee), DisplayNameResourceName = "LastName")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [RegularExpression(SurnameRegex, ErrorMessageResourceType = typeof(Olimp.Auth.I18N.Auth), ErrorMessageResourceName = "SurnameInvalid")]
        public string Surname { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Employee), DisplayNameResourceName = "FirstName")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [RegularExpression(NameRegex, ErrorMessageResourceType = typeof(Olimp.Auth.I18N.Auth), ErrorMessageResourceName = "NameInvalid")]
        public string Name { get; set; }

        public string GivenName { get; set; }

        public bool GivenNameRequired { get; set; }
    }
}