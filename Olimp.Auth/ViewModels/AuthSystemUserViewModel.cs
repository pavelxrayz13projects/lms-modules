using Olimp.Core.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Olimp.Auth.ViewModels
{
    using I18N = Olimp.I18N.Domain;

    public class AuthSystemUserViewModel : SystemUserViewModel, IAuthViewModel
    {
        #region IAuthViewModel implementation

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Employee), DisplayNameResourceName = "Login")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public string Login { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Employee), DisplayNameResourceName = "Password")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public string Password { get; set; }

        #endregion IAuthViewModel implementation
    }
}