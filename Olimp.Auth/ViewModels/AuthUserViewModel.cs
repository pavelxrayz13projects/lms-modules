using Olimp.Core.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Olimp.Auth.ViewModels
{
    using I18N = Olimp.I18N.Domain;

    public class AuthUserViewModel : IAuthViewModel
    {
        public int GroupId { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Employee), DisplayNameResourceName = "Login")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public string Login { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Employee), DisplayNameResourceName = "Password")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public string Password { get; set; }

        public string ErrorMessage { get; set; }
    }
}