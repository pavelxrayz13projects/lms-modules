using Olimp.Core.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.Auth.ViewModels
{
    using I18N = Olimp.I18N.Domain;

    public class CreateUserLongViewModel : CreateUserViewModel
    {
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Employee), DisplayNameResourceName = "Appointment")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [StringLength(100, ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "StringLengthInvalid")]
        public string AppointmentName { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Employee), DisplayNameResourceName = "Company")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int CompanyId { get; set; }

        public IEnumerable<SelectListItem> Companies { get; set; }
    }
}