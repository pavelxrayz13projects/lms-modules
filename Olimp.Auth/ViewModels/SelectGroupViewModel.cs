using Olimp.Core.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.Auth.ViewModels
{
    public class SelectGroupViewModel
    {
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Auth), DisplayNameResourceName = "Group")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int GroupId { get; set; }

        public IEnumerable<SelectListItem> Groups { get; set; }
    }
}