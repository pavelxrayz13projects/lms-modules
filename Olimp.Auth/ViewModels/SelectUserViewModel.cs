using Olimp.Core.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.Auth.ViewModels
{
    public class SelectUserViewModel
    {
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Auth), DisplayNameResourceName = "User")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public Guid UserId { get; set; }

        public IEnumerable<SelectListItem> Users { get; set; }
    }
}