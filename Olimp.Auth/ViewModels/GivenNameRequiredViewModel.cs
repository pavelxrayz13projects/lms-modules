using Olimp.Core.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Olimp.Auth.ViewModels
{
    using I18N = Olimp.I18N.Domain;

    public class GivenNameRequiredViewModel
    {
        public const string GivenNameRequiredRegex = @"^([\u0401\u0451\u0410-\u044F]{2,50})|([\u0041-\u005a\u0061-\u007a]{2,50})$";

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Employee), DisplayNameResourceName = "GivenName")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [RegularExpression(GivenNameRequiredRegex, ErrorMessageResourceType = typeof(Olimp.Auth.I18N.Auth), ErrorMessageResourceName = "GivenNameInvalid")]
        public string GivenName { get; set; }
    }
}