using Olimp.Core.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Olimp.Auth.ViewModels
{
    using I18N = Olimp.I18N.Domain;

    public class GivenNameViewModel
    {
        private const string GivenNameRegex = @"^([\u0401\u0451\u0410-\u044F]{2,50})|([\u0041-\u005a\u0061-\u007a]{2,50})|(.{0})$";

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Employee), DisplayNameResourceName = "GivenName")]
        [RegularExpression(GivenNameRegex, ErrorMessageResourceType = typeof(Olimp.Auth.I18N.Auth), ErrorMessageResourceName = "GivenNameInvalid")]
        public virtual string GivenName { get; set; }
    }
}