namespace Olimp.Auth.ViewModels
{
    public class SystemUserViewModel
    {
        public string ReturnUrl { get; set; }

        public string ErrorMessage { get; set; }
    }
}