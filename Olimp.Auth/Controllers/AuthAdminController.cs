using Olimp.Auth.ViewModels;
using Olimp.Core;
using Olimp.Web.Controllers.Base;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Olimp.Auth.Controllers
{
    public class AuthAdminController : PController
    {
        public AuthAdminController()
        {
        }

        [HttpGet]
        public ActionResult Admin(SystemUserViewModel model)
        {
            return View("AuthSystemUser", new AuthSystemUserViewModel
            {
                ReturnUrl = model.ReturnUrl,
                ErrorMessage = model.ErrorMessage
            });
        }

        [HttpPost]
        public ActionResult Admin(AuthSystemUserViewModel model)
        {
            if (!ModelState.IsValid)
                return View("AuthSystemUser", model);

            OlimpApplication.Logger.DebugFormat("Authenticating user {0}", model.Login);
            if (Membership.ValidateUser(model.Login, model.Password))
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.SetAuthCookie(model.Login, false);

                return RedirectToAction("Index", "Settings", new { area = "Admin", returnUrl = HttpUtility.UrlDecode(model.ReturnUrl) });
            }

            return RedirectToAction("Admin", new { ErrorMessage = I18N.Auth.AuthError });
        }

        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToRoute("Default");
        }
    }
}