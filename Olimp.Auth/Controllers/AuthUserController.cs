using Olimp.Auth.ViewModels;
using Olimp.Configuration;
using Olimp.Core.Mvc.Security;
using Olimp.Domain.Exam.Auth;
using Olimp.Domain.LearningCenter;
using Olimp.Domain.LearningCenter.Auth;
using Olimp.Domain.LearningCenter.DataServices;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Domain.LearningCenter.NotPersisted;
using Olimp.Domain.LearningCenter.Services;
using Olimp.Ldap;
using Olimp.Web.Controllers.Registration;
using Olimp.Web.Controllers.Users;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Auth.Controllers
{
    public class AuthUserController : AuthCookieManagerUserController
    {
        private readonly IGroupService _groupService;
        private readonly IEmployeeService _employeeService;
        private readonly ICompanyService _companyService;
        private readonly EmployeeNameUnificator _nameUnificator;
        private readonly IEmployeeAuthenticator _employeeAuthenticator;

        public AuthUserController(
            IGroupService groupService,
            IEmployeeService employeeService,
            ICompanyService companyService,
            IEmployeeAuthenticator employeeAuthenticator,
            EmployeeNameUnificator nameUnificator)
        {
            if (groupService == null)
                throw new ArgumentNullException("groupService");

            if (employeeService == null)
                throw new ArgumentNullException("employeeService");

            if (companyService == null)
                throw new ArgumentNullException("companyService");

            if (employeeAuthenticator == null)
                throw new ArgumentNullException("employeeAuthenticator");

            if (nameUnificator == null)
                throw new ArgumentNullException("nameUnificator");

            _groupService = groupService;
            _employeeService = employeeService;
            _companyService = companyService;
            _employeeAuthenticator = employeeAuthenticator;
            _nameUnificator = nameUnificator;
        }

        public override bool IsAuthorizing { get { return true; } }

        protected override string UserCookieName
        {
            get { return AuthCookieManager.GetNameByUrl(Request["returnUrl"] ?? (string)ViewData["returnUrl"]); }
        }

        protected override string UserCookiePath
        {
            get { return AuthCookieManager.GetPathByUrl(Request["returnUrl"] ?? (string)ViewData["returnUrl"]); }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            ViewData["returnUrl"] = Request["returnUrl"];
        }

        [NonAction]
        private ActionResult RedirectUserBack(Guid employeeId)
        {
            var url = Request["returnUrl"];
            AuthCookieManager.SetCookie(url, new { employeeId = employeeId });

            var join = "";
            if (url.EndsWith("?")) join = "";
            else if (url.IndexOf("?") == -1) join = "?";
            else join = "&";
            return Redirect(String.Format("{0}{1}employeeId={2}", url, join, employeeId));
        }

        [HttpGet]
        [RequireWorkplaceTokenForAnonymous]
        public ActionResult User()
        {
            using (UnitOfWork.CreateRead())
            {
                var config = Config.Get<OlimpConfiguration>();

                switch (config.RegistrationType)
                {
                    case RegistrationType.ByFullName:
                        return View("CreateUser", new CreateUserViewModel { GivenNameRequired = config.RequireGivenName });

                    case RegistrationType.ByFullNameExpanded:
                        return View("CreateUser", new CreateUserLongViewModel
                    {
                        GivenNameRequired = config.RequireGivenName,
                        Companies = _companyService.GetActiveCompaniesList()
                    });

                    case RegistrationType.Anonymous:
                        return this.Anonymous();

                    default:
                        return View("SelectGroup", new SelectGroupViewModel { Groups = _groupService.GetGroupsListForAuth() });
                }
            }
        }

        [RegistrationType(RegistrationType.ByFullName, RegistrationType.ByFullNameExpanded, RegistrationType.Anonymous)]
        [RequireWorkplaceToken]
        public ActionResult Anonymous()
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var employeeRepo = Repository.Of<Employee, Guid>();

                var name = String.Format(I18N.Auth.UnregisteredNamePattern, employeeRepo.Count());
                var login = "Anonymous_" + Guid.NewGuid().ToString();
                var surname = I18N.Auth.UnregisteredSurname;
                var employee = _employeeService.CreateUnregisteredAnonymous(name, surname, login);

                uow.Commit();

                return RedirectUserBack(employee.Id);
            }
        }

        [HttpPost]
        [RegistrationType(RegistrationType.PreRegistration)]
        [RequireWorkplaceToken]
        public ActionResult SelectGroup(SelectGroupViewModel model)
        {
            if (!ModelState.IsValid)
                return View("SelectGroup", model);

            using (UnitOfWork.CreateRead())
            {
                var olimpConfig = Config.Get<OlimpConfiguration>();

                if (olimpConfig.LoginType.Equals(LoginType.SelectFromList))
                {
                    return View("SelectUser", new SelectUserViewModel
                    {
                        Users = _employeeService
                            .GetActiveEmployeesList(model.GroupId)
                    });
                }
                else
                {
                    return View("AuthUser", new AuthUserViewModel { GroupId = model.GroupId });
                }
            }
        }

        [HttpPost]
        [RegistrationType(RegistrationType.PreRegistration)]
        [RequireWorkplaceToken]
        public ActionResult SelectUser(SelectUserViewModel model)
        {
            if (!ModelState.IsValid)
                return View("SelectUser", model);
            return RedirectUserBack(model.UserId);
        }

        [HttpPost]
        [RegistrationType(RegistrationType.PreRegistration)]
        [RequireWorkplaceToken]
        public ActionResult User(AuthUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                // _employeeAuthenticator.Authenticate can update (sync) employee.
                using (var uow = UnitOfWork.CreateWrite())
                {
                    var result = _employeeAuthenticator.Authenticate(model.Login, model.Password);
                    uow.Commit();

                    if (result.IsAuthenticated && result.Entity.Group.Id == model.GroupId)
                    {
                        return RedirectUserBack(result.Entity.Id);
                    }
                }
            }

            model.ErrorMessage = String.Format(I18N.Auth.AuthErrorOrUserIsNotAMemberOfTheGroup, model.Login);
            return View("AuthUser", model);
        }

        [NonAction]
        private void NormalizeFullName(CreateUserViewModel model)
        {
            var name = model.Name;
            var surname = model.Surname;
            var givenName = model.GivenName;

            _nameUnificator.NormalizeFullName(ref name, ref surname, ref givenName);

            model.Name = name;
            model.Surname = surname;
            model.GivenName = givenName;
        }

        [HttpPost]
        [RegistrationType(RegistrationType.ByFullName)]
        [RequireWorkplaceToken]
        public ActionResult User(CreateUserViewModel model)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("User");

            this.NormalizeFullName(model);

            using (var uow = UnitOfWork.CreateWrite())
            {
                var employee = _employeeService.CreateAnonymous(
                    model.Name, model.Surname, model.GivenName, null, null);

                uow.Commit();
                return RedirectUserBack(employee.Id);
            }
        }

        [HttpPost]
        [RegistrationType(RegistrationType.ByFullNameExpanded)]
        [RequireWorkplaceToken]
        public ActionResult User(CreateUserLongViewModel model)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("User");

            this.NormalizeFullName(model);

            using (var uow = UnitOfWork.CreateWrite())
            {
                var employee = _employeeService.CreateAnonymous(
                    model.Name, model.Surname, model.GivenName, model.AppointmentName, model.CompanyId);

                var olimpConfig = Config.Get<OlimpConfiguration>();
                if (!olimpConfig.InheritIndirectProfiles)
                    employee.DoNotInheritIndirectProfiles = true;

                uow.Commit();
                return RedirectUserBack(employee.Id);
            }
        }
    }
}