using Olimp.Auth.Controllers;
using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Core.Web;
using Olimp.Domain.Catalogue.Ldap;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter.Auth;
using Olimp.Domain.LearningCenter.DataServices;
using Olimp.Domain.LearningCenter.Services;
using Olimp.Ldap;
using Olimp.Ldap.Auth;
using Olimp.Ldap.Web;
using PAPI.Core;
using PAPI.Core.Initialization;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Auth.Plugin))]

namespace Olimp.Auth
{
    public class Plugin : PluginModule
    {
        private readonly IModuleContext _context;

        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
            _context = context;
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["*"].RegisterControllers(typeof(AuthAdminController));
            areas["*"].RegisterControllers(typeof(AuthUserController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (_context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            SetAuthSchemes(Container.Instance.Resolve<ILdapAuthSchemeRepository>());

            ConfigureFormsAuthProviders();

            var defultAuthenticator = (IEmployeeAuthenticator)new DefaultEmployeeAuthenticator(PContext.ExecutingModule.Database.RepositoryFactory);

            var employeeAuthenticator = new DynamicEmployeeAuthenticator(
                (login, password) =>
                (LdapScheme.CurrentSchemeForEmployers != null
                        ? new LdapEmployeeAuthenticator(PContext.ExecutingModule.Database.RepositoryFactory, LdapScheme.CurrentSchemeForEmployers)
                        : defultAuthenticator)
                    .Authenticate(login, password)
            );

            binder.Controller<AuthUserController>(b => b
                .Bind<IGroupService, GroupService>()
                .Bind<IEmployeeService>(() => new EmployeeService(PContext.ExecutingModule.Database.RepositoryFactory))
                .Bind<ICompanyService, CompanyService>()
                .Bind<IEmployeeAuthenticator>(() => employeeAuthenticator)
                .BindToSelf<EmployeeNameUnificator>());
        }

        private static void SetAuthSchemes(ILdapAuthSchemeRepository repo)
        {
            LdapScheme.CurrentSchemeForSystemUsers = repo.GetCurrentForSystemUsers();
            LdapScheme.CurrentSchemeForEmployers = repo.GetCurrentForEmployers();
        }

        private static void ConfigureFormsAuthProviders()
        {
            var defaultFormAuthProvider = new SimpleFormsAuthProvider(
                Container.Instance.Resolve<UsersRepositoryMembershipProvider>(),
                Container.Instance.Resolve<UsersRepositoryRoleProvider>());

            Container.Instance.Register<ICurrentFormsAuthProvider>(c =>
                new DynamicFormsAuthProvider(
                    () => LdapScheme.CurrentSchemeForSystemUsers != null
                        ? new SimpleFormsAuthProvider(
                            Container.Instance.Resolve<LdapMembershipProvider>(),
                            Container.Instance.Resolve<LdapRoleProvider>())
                        : defaultFormAuthProvider));
        }
    }
}