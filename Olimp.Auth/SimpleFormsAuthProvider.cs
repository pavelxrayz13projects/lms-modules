﻿using Olimp.Core.Web;
using System;
using System.Web.Security;

namespace Olimp.Auth
{
    public class SimpleFormsAuthProvider : ICurrentFormsAuthProvider
    {
        private readonly MembershipProvider _membershipProvider;
        private readonly RoleProvider _roleProvider;

        public SimpleFormsAuthProvider(MembershipProvider membershipProvider, RoleProvider roleProvider)
        {
            if (membershipProvider == null)
                throw new ArgumentNullException("membershipProvider");
            if (roleProvider == null)
                throw new ArgumentNullException("roleProvider");

            _membershipProvider = membershipProvider;
            _roleProvider = roleProvider;
        }
        
        public MembershipProvider CurrentMembershipProvider
        {
            get { return _membershipProvider; }
        }

        public RoleProvider CurrentRoleProvider
        {
            get { return _roleProvider; }
        }
    }
}