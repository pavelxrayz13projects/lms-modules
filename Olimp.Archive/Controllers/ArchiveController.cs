using System.Collections.Generic;
using System.Linq;
using System.Net;
using Olimp.Archive.ViewModels;
using Olimp.Core;
using Olimp.Core.Mvc.Security;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.Content;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Exam.Attempts;
using Olimp.Domain.Exam.Content;
using Olimp.Domain.Exam.Entities;
using Olimp.Domain.Exam.NHibernate.Schema;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using System;
using System.Web.Mvc;
using PAPI.Core.Data;
using ServiceStack.Text;

namespace Olimp.Archive.Controllers
{
    // TODO : move QuestionInfo, ResultQuestionInfo and all connected views to Activity
    public class ArchiveController : PController
    {
        private readonly IAttemptRepository _attemptRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly ICourseQuestionProviderFactory _questionProviderFactory;
        private readonly ICachedQuestionProvider _cachedQuestionProvider;
        private readonly IExamAreasListProvider _examAreasListProvider;

        public ArchiveController(
            IAttemptRepository attemptRepository,
            ICourseRepository courseRepository,
            ICourseQuestionProviderFactory questionProviderFactory,
            ICachedQuestionProvider cachedQuestionProvider,
            IExamAreasListProvider examAreasListProvider)
        {
            if (attemptRepository == null)
                throw new ArgumentNullException("attemptRepository");

            if (courseRepository == null)
                throw new ArgumentNullException("courseRepository");

            if (questionProviderFactory == null)
                throw new ArgumentNullException("questionProviderFactory");

            if (cachedQuestionProvider == null)
                throw new ArgumentNullException("cachedQuestionProvider");

            if (examAreasListProvider == null)
                throw new ArgumentNullException("examAreasListProvider");

            _attemptRepository = attemptRepository;
            _courseRepository = courseRepository;
            _questionProviderFactory = questionProviderFactory;
            _cachedQuestionProvider = cachedQuestionProvider;
            _examAreasListProvider = examAreasListProvider;
        }

        [AuthorizeForAdmin(Roles = Permissions.Reports.Generate + "," + Permissions.Reports.ManageReportTemplates, Order = 2)]
        public ActionResult Reports()
        {
            return View();
        }

        [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
        [AuthorizeForAdmin(Roles = Permissions.KnowledgeControl.Courses.Read + "," + Permissions.KnowledgeControl.Courses.Write, Order = 2)]
        public ActionResult ResultQuestionInfo(int id)
        {
            var question = _cachedQuestionProvider.GetByTaskId(id);
            var model = new QuestionInfoViewModel
            {
                Question = question.Text,
                TableData = question.Answers.GetTableData(
                  new TableViewModel(), a => new { a.Text, Correct = a.IsCorrect })
            };

            return PartialView("QuestionInfo", model);
        }

        [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
        [AuthorizeForAdmin(
            Roles = Permissions.KnowledgeControl.Courses.Read + "," +
                Permissions.KnowledgeControl.Courses.Write + "," +
                Permissions.Courses.TreeInfo,
            Order = 2)]
        public ActionResult QuestionInfo(int materialId, Guid questionId)
        {
            var course = _courseRepository.GetByBranchId(materialId);
            var questionsProvider = _questionProviderFactory.Create(course);
            var question = questionsProvider.GetById(questionId);

            var model = new QuestionInfoViewModel
            {
                Question = question.Text,
                TableData = question.Answers.GetTableData(new TableViewModel(), a => new
                {
                    a.Text, Correct = a.IsCorrect
                })
            };

            return PartialView(model);
        }

        public ActionResult Print(Guid attemptId)
        {
            return View(new ArchiveResultsViewModel { AttemptId = attemptId });
        }

        public ActionResult GetProtocol(ArchiveResultsViewModel model)
        {
            var protocol = _attemptRepository.GetProtocol(model.AttemptId);
            var attempt = (ExamAttempt) _attemptRepository.GetById(model.AttemptId);

            return Json(new
            {
                Id = protocol.AttemptId,
                MaterialFullName = protocol.CourseName,
                Passed = protocol.IsPassed,
                Finished = protocol.IsFinished,
                protocol.EmployeeFullName,
                StartTime = protocol.AttemptStartTime.ToString(),
                Company = protocol.EmployeeCompany ?? String.Empty,
                Number = protocol.EmployeeNumber ?? String.Empty,
                Job = protocol.EmployeePosition ?? String.Empty,
                ResponsibleFullName = protocol.AttemptResponsibleFullName ?? String.Empty,
                Ticket = protocol.CourseTicketNumber,
                MaxErrorsCount = protocol.AllowedMistakesCount,
                ErrorsCount = protocol.MistakesCount,
                TestResults = protocol.TicketAnswers.GetTableData(model, r => new TestResultViewModel(r)),
                ExamAreas = _examAreasListProvider.GetAreas().Select(e => new
                {
                    Text = Convert.ToString(e),
                    Value = e.Code,
                    IsSelected = attempt.ExamAreas != null && attempt.ExamAreas.Contains(e.Code)
                })
            });
        }

        [HttpPost]
        public ActionResult SaveExamAreas(Guid attemptId, string examAreas)
        {
            var repository = Repository.Of<ExamAttemptSchema, Guid>();
            var e = repository.GetById(attemptId);

            if (e == null)
                throw new InvalidOperationException(string.Format(I18N.Archive.AttemptIsNullMessageFormat, attemptId));

            e.ExamAreas = examAreas;

            using (var uow = UnitOfWork.Create())
            {
                repository.Save(e);
                uow.Commit();
            }

            return new HttpStatusCodeResult((int) HttpStatusCode.OK);
        }
    }
}