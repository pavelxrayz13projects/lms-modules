﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<ArchiveResultsViewModel>"  MasterPageFile="~/Views/Shared/Popup.master" %>

<asp:Content ContentPlaceHolderID="Olimp_Css" runat="server">
	<style type="text/css">	
		* { font-family: Arial; font-size: x-small; }	
		table * { font-weight: normal; }
		body { padding: 20px 10px; }
		table { border-collapse: collapse; border: solid 1px black; border-bottom: 0; border-right: 0; margin: 10px 0; }
		table tr th,
		table tr td { border-right: solid 1px black; border-bottom: solid 1px black; padding: 3px; }
		.separated-line { margin: 20px 0; }
		
		@media print {
			button { display: none; }
		}
	</style>
</asp:Content>	

<asp:Content ContentPlaceHolderID="Olimp_Body" runat="server">
	<h1 data-bind="text: EmployeeFullName"></h1>
	<div>
		<span>Организация:</span>
		<span data-bind="text: Company"></span>
	</div>
	<div>
		<span>Должность:</span>
		<span data-bind="text: Job"></span>
	</div>
	<div>
		<span>Предмет тестирования:</span>
		<span data-bind="text: MaterialFullName"></span>
	</div>
	<div>
		<span>Дата и время проведения тестирования:</span>
		<span data-bind="text: StartTime"></span>
	</div>
	<div>
		<span>Номер билета:</span>
		<span data-bind="text: Ticket"></span>
	</div>
	
	<table>
		<thead>
			<tr>
				<th>№</th>
				<th><%= Olimp.I18N.Domain.Attempt.Question %></th>
				<th><%= Olimp.I18N.Domain.Attempt.Answer %></th>
				<th><%= Olimp.I18N.Domain.Attempt.AnswerCorrect %></th>
			</tr>
		</thead>
		<tbody data-bind="foreach: TestResults.rows">
			<tr>
				<td data-bind="html: Number"></td>
				<td data-bind="html: Question"></td>
				<td data-bind="html: Answer"></td>
				<td>
					<!-- ko if: Correct -->
					<%= Olimp.I18N.Domain.Attempt.CorrectAnswer %>
					<!-- /ko -->

					<!-- ko ifnot: Correct -->
					<%= Olimp.I18N.Domain.Attempt.IncorrectAnswer %>
					<!-- /ko -->
				</td>
			</tr>
		</tbody>
	</table>
	
	<div style="font-weight: bold;">
		<span>Допустимое количество ошибок:</span>
		<span data-bind="text: MaxErrorsCount"></span>
	</div>
	<div style="font-weight: bold;">
		<span>Допущено ошибок:</span>
		<span data-bind="text: ErrorsCount"></span>
	</div>
	<div>
		<span>Результат тестирования:</span>
		<!-- ko if: Passed -->		
		<span>СДАНО</span>
		<!-- /ko -->
		
		<!-- ko ifnot: Passed -->		
		<span>НЕ СДАНО</span>
		<!-- /ko -->
	</div>
	<div class="separated-line">При проведении тестирования нарушений его порядка не зафиксировано</div>
	<div class="separated-line">
		<span>Ответственный за проведение тестирования,</span>
		<span>__________________</span>
		<span>/</span><span data-bind="text: ResponsibleFullName"></span><span>/</span>
	</div>
	<div class="separated-line">
		<span>Тестируемый</span>
		<span>__________________</span>
		<span>/</span><span data-bind="text: EmployeeFullName"></span><span>/</span>
	</div>
	
	<div class="separated-line">
		<button onclick="window.print()">Печать</button>
	</div>
	
	<script type="text/javascript">
		$.post('<%= Url.Action("GetProtocol", new { AttemptId = Model.AttemptId }) %>', function(json) {
			json.ResponsibleFullName = json.ResponsibleFullName || '__________________________________';		
			ko.applyBindings(json);
		}, 'json')
	</script>
</asp:Content>