﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl" %>
<%@ Import Namespace="NHibernate.Mapping" %>

<% Platform.RenderExtensions("/olimp/archive/js"); %>
<script type="text/javascript">
    window.Archive.protocolUrl = '<%= Url.Action("GetProtocol", "Archive") %>';
    window.Archive.printUrl = '<%= Url.Action("Print", "Archive") %>';
</script>

<% Html.RenderPartial("QuestionInfoActionTemplate"); %>
		
<div id="protocol-dialog" title="Результаты экзамена">
    <% Html.RenderPartial("ProtocolInfo"); %>
    <% Html.RenderPartial("AnswerResultTemplate"); %>
    
    <table class="form-table">
        <tr>
            <th>
                <span><%= Html.Label(Olimp.I18N.Domain.Profile.ExamArea) %>
                    (<span id="exam-areas-count"></span>)
                </span>
            </th>
            <td>
                <div id="exam-areas-block">
                    <select id="exam-areas-list" multiple="multiple"></select>            
                </div>
            </td>
        </tr>
    </table>
    
    <input type="hidden" id="attempt-id" value="">

    <button id="exam-area-save" style="margin: 5px 0 10px 0;">
        <%= Olimp.Archive.I18N.Archive.ExamAreasSaveButton %>
    </button>

    <h1>Ответы</h1>
    <div class="block">
        <%
            Html.Table(new TableOptions
            {
                OmitTemplates = true,
                DeferredInitializationScriptRendering = true,
                Sorting = false,
                Paging = false,
                Columns = new[] {
					new ColumnOptions("Number", Olimp.I18N.Domain.Attempt.Number),
					new ColumnOptions("Question", Olimp.I18N.Domain.Attempt.Question),
					new ColumnOptions("Answer", Olimp.I18N.Domain.Attempt.Answer),
					new ColumnOptions("Correct", Olimp.I18N.Domain.Attempt.AnswerCorrect) { TemplateId = "answer-result" }
                },
                Actions = new RowAction[] { new TemplateAction("question-info-action") }
            }, new { id = "test-results" });
        %>
    </div>
</div>

<% Html.TableInitializationScript("test-results"); %>
	
<script type="text/html" id="attempt-results-action-doT">
	<a href="javascript:Archive.showResults('{{= it.$data.Id() }}')" class="icon icon-stats action" title="Результаты экзамена"></a>
</script>

<script type="text/html" id="attempt-print-action-doT">
    <a data-bind="attr: { href: window.Archive.printUrl + '?attemptId=' + '{{= it.$data.Id() }}' }" class="icon icon-print-small action" title="Распечатать результаты экзамена" target="_blank"></a>
</script>

<script>
    var getData = function () {
        var attemptId = $('#attempt-id').val(),
            examAreas = $('#exam-areas-block .olimp-multiselect-list li'),
            examAreasCodes = $.map(examAreas, function(e) {
                return $(e).data('value');
            });

        return {
            attemptId: attemptId,
            examAreas: JSON.stringify(examAreasCodes)
        }
    };

    $(function () {
        var button = $("#exam-area-save");

        button.olimpbutton();

        button.click(function (e) {
            var target = $(e.target);

            if (target.data('post') === true)
                return false;

            target.data('post', true);

            $.post('<%= Url.Action("SaveExamAreas", "Archive") %>', getData()).done(function() {
                $.Olimp.showSuccess('<%= Olimp.Archive.I18N.Archive.ExamAreasSaveSuccess %>');
            });

            setTimeout(function() {
                target.data('post', false);
            }, 2000);

            return true;
        });
    });
</script>