﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script id="answer-result" type="text/html">
	<!-- ko if: Correct -->
	<%= Olimp.I18N.Domain.Attempt.CorrectAnswer %>
	<!-- /ko -->

	<!-- ko ifnot: Correct -->
	<%= Olimp.I18N.Domain.Attempt.IncorrectAnswer %>
	<!-- /ko -->
</script>