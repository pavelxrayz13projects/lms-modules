﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script id="attempt-passed-doT" type="text/html">		
	{{? it.$data.Finished() }}	
		{{? it.$data.Passed() }}		
		<%= Olimp.I18N.Domain.Attempt.Passed %>
		{{??}}
		<%= Olimp.I18N.Domain.Attempt.Failed %>
		{{?}}
	{{?}}
</script>