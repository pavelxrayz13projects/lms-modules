﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script id="answer-result" type="text/html">
	{{? ko.utils.unwrapObservable(it.$data.Correct) }}		
	<%= Olimp.I18N.Domain.Attempt.CorrectAnswer %>
	{{??}}	
	<%= Olimp.I18N.Domain.Attempt.IncorrectAnswer %>
	{{?}}		
</script>