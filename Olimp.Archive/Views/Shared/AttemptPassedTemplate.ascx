﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script id="attempt-passed" type="text/html">
	<!-- ko if: Finished -->
		<!-- ko if: Passed -->
		<%= Olimp.I18N.Domain.Attempt.Passed %>
		<!-- /ko -->
		
		<!-- ko ifnot: Passed -->
		<%= Olimp.I18N.Domain.Attempt.Failed %>
		<!-- /ko -->
	<!-- /ko -->		
</script>