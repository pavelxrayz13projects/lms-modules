﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl" %>

<% Platform.RenderExtensions("/olimp/archive/js"); %>
<script type="text/javascript">window.Archive.protocolUrl = '<%= Url.Action("GetProtocol", "Archive") %>';</script>

<% Html.RenderPartial("QuestionInfoActionTemplate"); %>
		
<div id="protocol-dialog" title="Результаты экзамена">
    <% Html.RenderPartial("ProtocolInfo"); %>
    <% Html.RenderPartial("AnswerResultTemplate"); %>
    
    <h1>Ответы</h1>
    <div class="block">
        <%
            Html.Table(new TableOptions
            {
                OmitTemplates = true,
                DeferredInitializationScriptRendering = false,
                Sorting = false,
                Paging = false,
                Columns = new[] {
					new ColumnOptions("Number", Olimp.I18N.Domain.Attempt.Number),
					new ColumnOptions("Question", Olimp.I18N.Domain.Attempt.Question),
					new ColumnOptions("Answer", Olimp.I18N.Domain.Attempt.Answer),
					new ColumnOptions("Correct", Olimp.I18N.Domain.Attempt.AnswerCorrect) { TemplateId = "answer-result" }
                },
                Actions = new RowAction[] { new TemplateAction("question-info-action") }
            }, new { id = "test-results" });
        %>
    </div>
</div>
	
<script type="text/html" id="attempt-results-action">
	<a href="#" class="icon icon-stats action" title="Результаты экзамена" data-bind="click: function() { Archive.showResults($data.Id()) }"></a>
</script>