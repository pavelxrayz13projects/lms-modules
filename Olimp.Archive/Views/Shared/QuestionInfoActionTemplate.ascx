﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div title="Информация о вопросе" id="question-dialog"></div>
<script type="text/javascript">window.Archive.questionInfoUrl = '<%= Url.Action("ResultQuestionInfo", "Archive") %>';</script>

<script id="question-info-action" type="text/html">
	<a href="#" class="icon icon-info action" title="Информация о вопросе" data-bind="click: Archive.showQuestionInfo"></a>
</script>	