﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="I18N=Olimp.Archive.I18N" %>

<div id="protocol-info">
    <div class="print-block" style="height: 19px;">
        <a data-bind="attr: { href: '<%= Url.Action("Print", "Archive") %>?attemptId=' + $data.Id }" target="_blank" class="right">
            <span><%= Olimp.I18N.PlatformShared.PrintLink %></span>
            <div class="icon icon-print"></div>
        </a>
    </div>
    
    <h1 data-bind="text: EmployeeFullName"></h1>
    <div class="block">
        <table class="form-table">
            <tr class="first">
                <th><%= I18N.Archive.Company %></th>
                <td data-bind="text: Company"></td>
            </tr>
            <tr>
                <th><%= Olimp.I18N.Domain.Attempt.Job %></th>
                <td data-bind="text: Job"></td>
            </tr>
            <tr>
                <th><%= Olimp.I18N.Domain.Attempt.Ticket %></th>
                <td data-bind="text: Ticket"></td>
            </tr>
            <tr>
                <th><%= I18N.Archive.Course %></th>
                <td data-bind="text: MaterialFullName"></td>
            </tr>
            <tr>
                <th><%= I18N.Archive.StartTime %></th>
                <td data-bind="text: StartTime"></td>
            </tr>
            <tr>
                <th><%= Olimp.I18N.Domain.Attempt.MaxErrors %></th>
                <td data-bind="text: MaxErrorsCount"></td>
            </tr>
            <tr>
                <th>Допущено ошибок</th>
                <td data-bind="text: ErrorsCount"></td>
            </tr>
            <tr class="last">
                <th><%= I18N.Archive.Result %></th>
                <td data-bind="template: { name: 'attempt-passed', data: $data }"></td>
            </tr>
        </table>
    </div>
</div>