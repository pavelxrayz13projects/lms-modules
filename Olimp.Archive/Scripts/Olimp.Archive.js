window.Archive = window.Archive || { protocolUrl: '', questionInfoUrl: '' };

$.extend(window.Archive, {
	loadResults: function(id, onsuccess) {
	    $.post(window.Archive.protocolUrl, 'AttemptId=' + id, function(json) {
	        $('#test-results').data('viewModel').setRows(json.TestResults);

	        var examAreasList = $('#exam-areas-list');
	        if (examAreasList) {
	            examAreasList.multiselect("destroy");

	            examAreasList.multiselect({
	                createOnEnter: false,
	                data: json.ExamAreas,
	                menuSourceComparer: function(a, b) {
	                    var aa = String(a.optionValue).split("."),
	                        bb = String(b.optionValue).split("."),
	                        minimal = Math.min(aa.length, bb.length);

	                    for (var i = 0; i < minimal; ++i) {
	                        var valA = parseInt(aa[i]) || aa[i].toLowerCase(),
	                            valB = parseInt(bb[i]) || bb[i].toLowerCase();

	                        if (valA < valB) {
	                            return -1;
	                        } else if (valA > valB) {
	                            return 1;
	                        }
	                    }
	                    return 0;
	                }
	            });

	            var setExamAreasCount = function() {
	                var ulContainerChildCount = $("#exam-areas-block .olimp-multiselect-list").children().length;
	                $("#exam-areas-count").text(ulContainerChildCount);
	            };

	            setExamAreasCount();

	            $("#exam-areas-block").on("multiselect-elements-changed", ".olimp-multiselect-list", setExamAreasCount);

	            $('#attempt-id').val(json.Id);
	        }

	        json.TestResults = null;

	        var infoNode = $('#protocol-info')[0];
	        ko.cleanNode(infoNode);
	        ko.applyBindings(json, infoNode);

	        $('#protocol-dialog').olimpdialog('open');

	        if (onsuccess)
	            onsuccess();
	    }, 'json');
	},
	showResults: function (id) {
		if (window.Archive.reloadDialog && window.Archive.reloadDialog > 0)
			$('#search-results').data('viewModel').suspendReload();
			
		window.Archive.loadResults(id, function() {
			if (!window.Archive.reloadDialog || window.Archive.reloadDialog <= 0)
				return;
				
			window.Archive._intervalId = setInterval(function() { window.Archive.loadResults(id) }, window.Archive.reloadDialog);
		})
	},
	showQuestionInfo: function() {
		var dialog = $('#question-dialog')
		dialog.load(window.Archive.questionInfoUrl, { id: this.Id() }, function() { dialog.olimpdialog('open').olimpdialog('moveToTop') });
	}
})

$(function() {
	$('#protocol-dialog').olimpdialog({ 
		close: function () { 
			if (window.Archive.reloadDialog && window.Archive.reloadDialog > 0) {
				clearInterval(window.Archive._intervalId);
				$('#search-results').data('viewModel').suspendReload(false); 				
			}
		},
		autoOpen: false, draggable: false, resizable: false, modal: true, width: '90%', fixedHeight: 0.9 }),
	
	$('#question-dialog').olimpdialog({ autoOpen: false, modal: true,resizable: false, draggable: false, width: '60%' });
	
})
