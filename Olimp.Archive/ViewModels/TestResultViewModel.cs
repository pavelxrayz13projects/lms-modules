using Olimp.Domain.Exam.Entities;
using System.Linq;

namespace Olimp.Archive.ViewModels
{
    public class TestResultViewModel
    {
        public TestResultViewModel(AttemptQuestionTask task)
        {
            this.Id = task.Id;
            this.Question = task.Question.Text;
            this.Answer = task.IsAnswered && task.GivenAnswerNumbers.Any()
                ? task.AnswerText
                : Olimp.Archive.I18N.Archive.TimeEndMessage;
            this.Correct = task.IsCorrect;
            this.Number = task.Order;
        }

        public int Id { get; set; }

        public int Number { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public bool Correct { get; set; }
    }
}