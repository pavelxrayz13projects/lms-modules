namespace Olimp.Archive.ViewModels
{
    public class QuestionInfoViewModel
    {
        public string Question { get; set; }

        public object TableData { get; set; }
    }
}