using Olimp.Core.ComponentModel;
using Olimp.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Olimp.Archive.ViewModels
{
    public class ArchiveSearchViewModel : TableViewModel
    {
        public bool AllowDelete { get; set; }

        public IEnumerable<SelectListItem> Profiles { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Archive), DisplayNameResourceName = "FIO")]
        public string FullName { get; set; }

        public string Number { get; set; }

        public string Company { get; set; }

        public string Material { get; set; }

        public int? ProfileId { get; set; }

        public DateTime? DateBegin { get; set; }

        public DateTime? DateEnd { get; set; }

        public bool? Passed { get; set; }
    }
}