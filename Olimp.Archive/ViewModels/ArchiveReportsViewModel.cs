using System.Collections.Generic;

namespace Olimp.Archive.ViewModels
{
    public class ArchiveReportsViewModel
    {
        public IEnumerable<ReportData> Reports { get; set; }
    }
}