using System;

namespace Olimp.Archive.ViewModels
{
    public class ReportData
    {
        public ReportData(Guid guid, string name)
        {
            this.Guid = guid;
            this.Name = name;
        }

        public Guid Guid { get; set; }

        public string Name { get; set; }
    }
}