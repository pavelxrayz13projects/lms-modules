using Olimp.UI.ViewModels;
using System;

namespace Olimp.Archive.ViewModels
{
    public class ArchiveResultsViewModel : TableViewModel
    {
        public Guid AttemptId { get; set; }
    }
}