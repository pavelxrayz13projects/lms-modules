using Olimp.Archive.Controllers;
using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.Content;
using Olimp.Domain.Catalogue.Entities;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Catalogue;
using Olimp.Domain.Catalogue.OrmLite.Content;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Common;
using Olimp.Domain.Exam.Attempts;
using Olimp.Domain.Exam.Content;
using Olimp.Domain.Exam.NHibernate.Attempts;
using Olimp.Domain.Exam.NHibernate.Content;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Archive.Plugin))]

namespace Olimp.Archive
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var materialsModule = PContext.ExecutingModule as OlimpMaterialsModule;
            if (materialsModule == null)
                throw new InvalidOperationException("Executing module must be OlimpMaterialsModule");

            var facadeRepository = materialsModule.Materials.CourseRepository;

            var connFactoryFactory = new PerHttpRequestConnectionFactoryFactory(
                new SqliteConnectionFactoryFactory());

            var connFactory = connFactoryFactory.Create();

            var nodeRepository = new OrmLiteNodeRepository(connFactory);

            var attemptRepository = new NHibernateAttemptRepository(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                nodeRepository);

            var courseRepository = new OrmLiteCourseRepository(
                Context.NodeData, 
                connFactory,
                materialsModule.Materials.CourseRepository,
                nodeRepository);

            var questionProviderFactory = new OrmLiteCourseQuestionProviderFactory(facadeRepository);

            var cachedQuestionProvider = new NHibernateCachedQuestionProvider(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory);

            var examAreasListProvider = new CachedExamAreasListProvider(
                new XmlExamAreasListProvider(OlimpPaths.Default.ExamAreasListFile));

            binder.Controller<ArchiveController>(b => b
                .Bind<IAttemptRepository>(() => attemptRepository)
                .Bind<ICourseRepository>(() => courseRepository)
                .Bind<ICourseQuestionProviderFactory>(() => questionProviderFactory)
                .Bind<ICachedQuestionProvider>(() => cachedQuestionProvider)
                .Bind<IExamAreasListProvider>(() => examAreasListProvider));
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(ArchiveController));
        }
    }
}