﻿namespace Olimp.Exam.ViewModels
{
    public class ScormHomeIndexViewModel : HomeIndexViewModel
    {
        public object TreeData { get; set; }

        public string OlimpPath { get; set; }
    }
}