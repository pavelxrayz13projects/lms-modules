using Olimp.Domain.Exam.Entities;
using System;
using System.Collections.Generic;

namespace Olimp.Exam.ViewModels
{
    public class HomeIndexViewModel
    {
        public Guid ConcreteEmployeeId { get; set; }

        public CachedEmployee Employee { get; set; }

        public IEnumerable<Guid> PassedMaterials { get; set; }

        public bool IsAnonymousMode { get; set; }
    }
}