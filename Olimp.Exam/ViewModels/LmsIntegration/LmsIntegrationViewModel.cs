﻿namespace Olimp.Exam.ViewModels.LmsIntegration
{
    public class LmsIntegrationViewModel<TModel>
    {
        public string OlimpPath { get; set; }

        public TModel RealModel { get; set; }
    }

    public class LmsIntegrationViewModel : LmsIntegrationViewModel<object>
    {
    }
}