namespace Olimp.Exam.ViewModels.LmsIntegration
{
    public class ScormTestResultsViewModel : TestResultsViewModel
    {
        public string DoneProfileIds { get; set; }

        public string FailedProfileIds { get; set; }

        public bool IsFinishedProfile { get; set; }
    }
}