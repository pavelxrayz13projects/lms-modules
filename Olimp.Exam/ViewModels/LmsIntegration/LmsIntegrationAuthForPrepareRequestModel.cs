﻿namespace Olimp.Exam.ViewModels.LmsIntegration
{
    public class LmsIntegrationAuthForPrepareRequestModel : LmsIntegrationRequestModel
    {
        public string FullName { get; set; }

        public int ProfileId { get; set; }

        public string Login { get; set; }

        public string PersonnelNumber { get; set; }

        public string Email { get; set; }

        public string Organization { get; set; }

        public string Position { get; set; }
    }
}