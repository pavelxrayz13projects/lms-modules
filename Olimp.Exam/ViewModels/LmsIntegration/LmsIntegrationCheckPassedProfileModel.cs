﻿using System;

namespace Olimp.Exam.ViewModels.LmsIntegration
{
    public class LmsIntegrationCheckPassedProfileModel : LmsIntegrationRequestModel
    {
        public Guid EmployeeId { get; set; }

        public int ProfileId { get; set; }
    }
}