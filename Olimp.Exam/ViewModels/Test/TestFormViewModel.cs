using System;

namespace Olimp.Exam.ViewModels
{
    public class TestFormViewModel
    {
        public Guid AttemptId { get; set; }

        public Guid QuestionId { get; set; }

        public int TimeLimit { get; set; }

        public int TimeLimitAll { get; set; }

        public int[] Answers { get; set; }
    }
}