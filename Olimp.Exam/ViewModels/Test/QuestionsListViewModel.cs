﻿using Olimp.Domain.Exam.Entities.Pseudo;
using System;
using System.Collections.Generic;

namespace Olimp.Exam.ViewModels
{
    public class QuestionsListViewModel
    {
        public Guid AttemptId { get; set; }

        public bool NavigationAllowed { get; set; }

        public Guid CurrentQuestionId { get; set; }

        public IEnumerable<TicketInfoTask> Tasks { get; set; }
    }
}