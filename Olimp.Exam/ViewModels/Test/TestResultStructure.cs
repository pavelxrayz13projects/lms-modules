namespace Olimp.Exam.ViewModels
{
    public class TestResultStructure
    {
        public int Number { get; set; }

        public string Question { get; set; }

        public string Answers { get; set; }

        public bool Correct { get; set; }
    }
}