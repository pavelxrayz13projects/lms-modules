using Olimp.Domain.Exam.Entities;
using System;

namespace Olimp.Exam.ViewModels
{
    public class TestViewModel
    {
        public Guid AttemptId { get; set; }

        public int TicketNumber { get; set; }

        public bool NavigationAllowed { get; set; }

        public bool ShowStopOffer { get; set; }

        public bool ShowStopButton { get; set; }

        public CachedQuestion Question { get; set; }

        public int[] GivenAnswers { get; set; }

        public int TimeLimit { get; set; }

        public string TimeLimitString { get; set; }

        public int TimeLimitType { get; set; }

        public int TimeLimitAll { get; set; }
    }
}