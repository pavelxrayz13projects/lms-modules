using System;

namespace Olimp.Exam.ViewModels
{
    public class TestResultsViewModel
    {
        public bool IsAnonymousMode { get; set; }

        public int TicketNumber { get; set; }

        public Guid EmployeeId { get; set; }

        public Guid AttemptId { get; set; }

        public string EmployeeFullName { get; set; }

        public string Company { get; set; }

        public string Job { get; set; }

        public string MaterialFullName { get; set; }

        public string StartTime { get; set; }

        public string MaxErrorsCount { get; set; }

        public int ErrorsCount { get; set; }

        public bool Finished { get; set; }

        public bool Passed { get; set; }

        public object TableData { get; set; }
    }
}