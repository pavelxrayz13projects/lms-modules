using Olimp.Domain.Exam.Entities;

namespace Olimp.Archive.ViewModels
{
    public class QuestionTaskViewModel
    {
        public QuestionTaskViewModel(AttemptQuestionTask result)
        {
            this.Id = result.Id;
            this.Question = result.Question.Text;
            this.Answer = result.IsAnswered
                ? result.AnswerText
                : Olimp.Archive.I18N.Archive.TimeEndMessage;
            this.Correct = result.IsCorrect;
            this.Number = result.Order;
        }

        public int Id { get; set; }

        public int Number { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public bool Correct { get; set; }
    }
}