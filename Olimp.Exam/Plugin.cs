using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Catalogue;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Common.Questions;
using Olimp.Domain.Exam.Attempts;
using Olimp.Domain.Exam.Auth;
using Olimp.Domain.Exam.NHibernate;
using Olimp.Domain.Exam.NHibernate.Attempts;
using Olimp.Domain.Exam.NHibernate.Auth;
using Olimp.Domain.Exam.NHibernate.Catalogue;
using Olimp.Domain.Exam.NHibernate.ProfileResults;
using Olimp.Domain.Exam.NHibernate.Tickets;
using Olimp.Domain.Exam.Tickets;
using Olimp.Domain.LearningCenter.DataServices;
using Olimp.Exam.Controllers;
using Olimp.Exam.Test;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.Web.Routing;

[assembly: PAPI.Core.Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Exam.Plugin))]

namespace Olimp.Exam
{
    public class Plugin : PluginModule
    {
        public const string AuthorizationCookieName = "examCookie";

        public static string AuthorizationCookiePath
        {
            get { return AuthCookie.Path; }
            set { AuthCookie.Path = value; }
        }

        internal static AuthCookieRegistration AuthCookie { get; private set; }

        static Plugin()
        {
            AuthCookie = new AuthCookieRegistration(AuthorizationCookieName, "/Exam", 1);
            AuthCookieManager.RegisterCookie(typeof(Plugin), AuthCookie);
        }

        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        { }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Exam"].RegisterControllers(
                typeof(HomeController),
                typeof(TestController));
            areas["*"].RegisterControllers(typeof(LmsIntegrationController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (this.Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var materialsModule = PContext.ExecutingModule as OlimpMaterialsModule;
            if (materialsModule == null)
                throw new InvalidOperationException("Available only with OlimpMaterialsModule");
            var facadeRepository = materialsModule.Materials.CourseRepository;

            var connFactoryFactory = new SqliteConnectionFactoryFactory();
            var connFactory = connFactoryFactory.Create();

            var treeBuilder = new DefaultCatalogueTreeBuilder();

            var branchesProvider = new NHibernateProfileBranchesProvider(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory);

            var catalogueProvider = new OrmLitePersonalCatalogueProvider(
                this.Context.NodeData, connFactory, treeBuilder, branchesProvider);

            var webAuthProvider = new NHibernateSimpleWebAuthProvider(
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.GetSessionManager());

            binder.Controller<HomeController>(b => b
                .Bind<ISimpleWebAuthProvider>(() => webAuthProvider)
                .Bind<IPersonalCatalogueProvider>(() => catalogueProvider));

            var shuffler = new DefaultShuffler();
            var taskNavigator = new NHibernateAttemptTasksNavigator(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                shuffler);

            var nodeRepository = new OrmLiteNodeRepository(connFactory);
            var courseRepository = new OrmLiteCourseRepository(this.Context.NodeData,
                connFactory, 
                facadeRepository, 
                nodeRepository);

            var materialCodesWithExamAreasProvider = new MaterialCodesWithExamAreasProvider(connFactory);

            var profileExamAreaProvider = new NHibernateProfileExamAreaProvider(
                PContext.ExecutingModule.Database.RepositoryFactory);

            var attemptFactory = new NHibernateAttemptFactory(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                PContext.ExecutingModule.Database.GetSessionManager(),
                courseRepository,
                facadeRepository,
                shuffler,
                materialCodesWithExamAreasProvider,
                profileExamAreaProvider);

            var ticketInfoProvider = new NHibernateTicketInfoProvider(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory);

            var assessor = new DefaultExamAssessor();
            var answerValidator = new SimpleAnswerValidator();

            var attemptWriter = new NHibernateAttemptAnswerWriter(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                answerValidator);

            var attemptFinalizer = new NHibernateExamAttemptFinalizer(
               PContext.ExecutingModule.Database.RepositoryFactory,
               PContext.ExecutingModule.Database.UnitOfWorkFactory,
               nodeRepository,
               assessor);

            var profileResultsService = new NHibernateProfileResultService(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                facadeRepository,
                courseRepository);

            binder.Controller<TestController>(b => b
                .Bind<ISimpleWebAuthProvider>(() => webAuthProvider)
                .Bind<ITicketInfoProvider>(() => ticketInfoProvider)
                .Bind<ExamInteractionDispatcherBase>(() => new ExamInteractionDispatcher(attemptFactory, attemptWriter, taskNavigator, attemptFinalizer, profileResultsService)));

            binder.Controller<LmsIntegrationController>(b => b
                .Bind<ILmsIntegrationEmployeeService>(() => new LmsIntegrationEmployeeService(PContext.ExecutingModule.Database.RepositoryFactory))
                .Bind<ExamInteractionDispatcherBase>(() => new ScormExamInteractionDispatcher(attemptFactory, attemptWriter, taskNavigator, attemptFinalizer, profileResultsService)));
        }
    }
}