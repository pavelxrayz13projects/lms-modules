﻿using Olimp.Archive.ViewModels;
using Olimp.Domain.Exam.Attempts;
using Olimp.Domain.Exam.Entities;
using Olimp.Domain.Exam.Entities.Pseudo;
using Olimp.Domain.Exam.ProfileResults;
using Olimp.Exam.ViewModels;
using Olimp.UI.ViewModels;
using System;
using System.Web.Mvc;

namespace Olimp.Exam.Test
{
    public abstract class ExamInteractionDispatcherBase
    {
        private readonly IAttemptFactory _attemptFactory;
        private readonly IAttemptAnswerWriter _answerWriter;
        private readonly IAttemptTasksNavigator _taskNavigator;
        private readonly IAttemptFinalizer<ExamAttempt> _attemptFinalizer;

        protected IProfileResultService ProfileResultService { get; private set; }

        public ExamInteractionDispatcherBase(
            IAttemptFactory attemptFactory,
            IAttemptAnswerWriter answerWriter,
            IAttemptTasksNavigator taskNavigator,
            IAttemptFinalizer<ExamAttempt> attemptFinalizer,
            IProfileResultService profileResultService)
        {
            if (attemptFactory == null)
                throw new ArgumentNullException("attemptFactory");

            if (answerWriter == null)
                throw new ArgumentNullException("answerWriter");

            if (taskNavigator == null)
                throw new ArgumentNullException("taskNavigator");

            if (attemptFinalizer == null)
                throw new ArgumentNullException("attemptFinalizer");

            if (profileResultService == null)
                throw new ArgumentNullException("profileResultService");

            _attemptFactory = attemptFactory;
            _answerWriter = answerWriter;
            _taskNavigator = taskNavigator;
            _attemptFinalizer = attemptFinalizer;

            this.ProfileResultService = profileResultService;
        }

        #region Redirect to result

        private Func<Guid, ActionResult> _redirectToResultFactory;

        public ExamInteractionDispatcherBase SetRedirectToResult(Func<Guid, ActionResult> actionResultFactory)
        {
            if (actionResultFactory == null)
                throw new ArgumentNullException("actionResultFactory");

            _redirectToResultFactory = actionResultFactory;

            return this;
        }

        protected virtual ActionResult DoRedirectToResult(Guid attemptId)
        {
            if (_redirectToResultFactory == null)
                throw new InvalidOperationException("RedirectToResult action is not set. Use SetRedirectToResult.");

            return _redirectToResultFactory.Invoke(attemptId);
        }

        #endregion Redirect to result

        #region Redirect to next question

        private Func<Guid, ActionResult> _redirectToNextQuestionFactory;

        public ExamInteractionDispatcherBase SetRedirectToNextQuestion(Func<Guid, ActionResult> actionResultFactory)
        {
            if (actionResultFactory == null)
                throw new ArgumentNullException("actionResultFactory");

            _redirectToNextQuestionFactory = actionResultFactory;

            return this;
        }

        protected virtual ActionResult DoRedirectToNextQuestion(Guid attemptId)
        {
            if (_redirectToNextQuestionFactory == null)
                throw new InvalidOperationException("RedirectToNextQuestion action is not set. Use SetRedirectToNextQuestion.");

            return _redirectToNextQuestionFactory.Invoke(attemptId);
        }

        #endregion Redirect to next question

        #region Render question view

        private Func<TestViewModel, ActionResult> _renderQuestionViewFactory;

        public ExamInteractionDispatcherBase SetRenderQuestionView(Func<TestViewModel, ActionResult> actionResultFactory)
        {
            if (actionResultFactory == null)
                throw new ArgumentNullException("actionResultFactory");

            _renderQuestionViewFactory = actionResultFactory;

            return this;
        }

        protected virtual ActionResult DoRenderQuestionView(TestViewModel model)
        {
            if (_renderQuestionViewFactory == null)
                throw new InvalidOperationException("RenderQuestionView action is not set. Use SetRenderQuestionView.");

            return _renderQuestionViewFactory.Invoke(model);
        }

        #endregion Render question view

        #region Render results view

        private Func<TestResultsViewModel, ActionResult> _renderResultsViewFactory;

        public ExamInteractionDispatcherBase SetRenderResultsView(Func<TestResultsViewModel, ActionResult> actionResultFactory)
        {
            if (actionResultFactory == null)
                throw new ArgumentNullException("actionResultFactory");

            _renderResultsViewFactory = actionResultFactory;

            return this;
        }

        protected virtual ActionResult DoRenderResultsView(TestResultsViewModel model)
        {
            if (_renderResultsViewFactory == null)
                throw new InvalidOperationException("RenderResultsView action is not set. Use SetRenderResultsView.");

            return _renderResultsViewFactory.Invoke(model);
        }

        #endregion Render results view

        public ActionResult StartNewTestOrContinue(Guid employeeId, int materialId, Action<Attempt> processAttempt = null)
        {
            var attempt = _attemptFactory.CreateExam(employeeId, materialId);

            if (processAttempt != null)
                processAttempt.Invoke(attempt);

            if (attempt.IsFinished)
                return this.DoRedirectToResult(attempt.Id);

            return this.DoRedirectToNextQuestion(attempt.Id);
        }

        public ActionResult AcceptAnswer(TestFormViewModel result)
        {
            try
            {
                _answerWriter.SetAnswers(result.AttemptId, result.QuestionId, result.Answers);
            }
            catch (AttemptFinishedException)
            {
                return this.DoRedirectToResult(result.AttemptId);
            }

            return this.DoRedirectToNextQuestion(result.AttemptId);
        }

        protected virtual bool ShouldRedirectToResult(TaskNavigationResult navigationResult)
        {
            if (navigationResult.NavigationState.HasFlag(TaskNavigationState.AttemptFinished) || navigationResult.NavigationState.HasFlag(TaskNavigationState.OutOfTime))
                return true;

            return navigationResult.NavigationState.HasFlag(TaskNavigationState.OutOfQuestions) && !navigationResult.NavigationAllowed;
        }

        public ActionResult ProvideNextQuestion(Guid attemptId, int? testResultId)
        {
            var navigationResult = testResultId.HasValue
                ? _taskNavigator.NavigateTo(testResultId.Value)
                : _taskNavigator.NavigateToNextNotAnswered(attemptId);

            if (this.ShouldRedirectToResult(navigationResult))
                return this.DoRedirectToResult(attemptId);

            var remainingSeconds = navigationResult.RemainingSeconds.GetValueOrDefault();
            var model = new TestViewModel
            {
                AttemptId = attemptId,
                TimeLimitType = (int)navigationResult.ExamTimeMode,
                TimeLimit = remainingSeconds,
                TimeLimitString = new TimeSpan(0, 0, remainingSeconds).ToString(),
                Question = navigationResult.Question,
                GivenAnswers = navigationResult.GivenAnswers,
                NavigationAllowed = navigationResult.NavigationAllowed,
                TicketNumber = navigationResult.TicketNumber
            };

            if (navigationResult.NavigationState.HasFlag(TaskNavigationState.OutOfQuestions))
            {
                model.ShowStopButton = testResultId.HasValue;
                model.ShowStopOffer = !testResultId.HasValue;
            }

            return this.DoRenderQuestionView(model);
        }

        public ActionResult FinalizeIfNeededAndShowResult(
            Guid attemptId, 
            int profileId, 
            Func<QuestionTaskViewModel, QuestionTaskViewModel> transformQuestion = null)
        {
            if (transformQuestion == null) 
                transformQuestion = t => t;

            var viewModel = CreateViewModel();

            var result = _attemptFinalizer.FinalizeAttempt(attemptId);
            if (!result.AlreadyFinalized)
                this.FinalizeProfiles(result, profileId, viewModel);

            InitializeViewModel(viewModel, attemptId, result.Attempt, result, transformQuestion);

            return DoRenderResultsView(viewModel);
        }

        protected abstract void FinalizeProfiles(AttemptFinalizationResult<ExamAttempt> result, int profileId, TestResultsViewModel viewModel);

        protected virtual TestResultsViewModel CreateViewModel()
        {
            return new TestResultsViewModel();
        }

        private static void InitializeViewModel(
            TestResultsViewModel viewModel,
            Guid attemptId,
            ExamAttempt examAttempt,
            AttemptFinalizationResult<ExamAttempt> attemptFinalizationResult,
            Func<QuestionTaskViewModel, QuestionTaskViewModel> transformQuestion)
        {
            viewModel.IsAnonymousMode = examAttempt.Settings.RegistrationType == Domain.Exam.RegistrationType.Anonymous;
            viewModel.AttemptId = attemptId;
            viewModel.Finished = examAttempt.IsFinished;
            viewModel.Passed = examAttempt.IsPassed;
            viewModel.EmployeeId = attemptFinalizationResult.ConcreteEmployeeId;
            viewModel.EmployeeFullName = Convert.ToString(examAttempt.Employee);
            viewModel.Company = Convert.ToString(examAttempt.Employee.Company);
            viewModel.TicketNumber = examAttempt.Ticket.Number;
            viewModel.Job = Convert.ToString(examAttempt.Employee.Positions);
            viewModel.MaterialFullName = Convert.ToString(examAttempt.Test);
            viewModel.StartTime = examAttempt.StartTime.ToString();
            viewModel.MaxErrorsCount = examAttempt.Test.MaxErrors.ToString();
            viewModel.ErrorsCount = examAttempt.MistakesCount.GetValueOrDefault();
            viewModel.TableData = examAttempt.Tasks.GetTableData(
                new TableViewModel(), t => transformQuestion.Invoke((new QuestionTaskViewModel(t))));
        }
    }
}