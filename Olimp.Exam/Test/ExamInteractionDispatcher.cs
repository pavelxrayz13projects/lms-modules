﻿using Olimp.Domain.Exam.Attempts;
using Olimp.Domain.Exam.Entities;
using Olimp.Domain.Exam.Entities.Pseudo;
using Olimp.Domain.Exam.ProfileResults;
using Olimp.Exam.ViewModels;

namespace Olimp.Exam.Test
{
    public class ExamInteractionDispatcher : ExamInteractionDispatcherBase
    {
        public ExamInteractionDispatcher(
            IAttemptFactory attemptFactory,
            IAttemptAnswerWriter answerWriter,
            IAttemptTasksNavigator taskNavigator,
            IAttemptFinalizer<ExamAttempt> attemptFinalizer,
            IProfileResultService profileResultService)
            : base(attemptFactory, answerWriter, taskNavigator, attemptFinalizer, profileResultService)
        {
        }

        protected override void FinalizeProfiles(AttemptFinalizationResult<ExamAttempt> result, int profileId, TestResultsViewModel viewModel)
        {
            ProfileResultService.FinishProfilesOnAttemptFinish(result.ConcreteEmployeeId, result.Attempt.Test.UniqueId);
        }
    }
}