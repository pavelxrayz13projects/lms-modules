﻿using Olimp.Domain.Exam.Attempts;
using Olimp.Domain.Exam.Entities;
using Olimp.Domain.Exam.Entities.Pseudo;
using Olimp.Domain.Exam.ProfileResults;
using Olimp.Exam.ViewModels;
using Olimp.Exam.ViewModels.LmsIntegration;
using ServiceStack.Text;

namespace Olimp.Exam.Test
{
    public class ScormExamInteractionDispatcher : ExamInteractionDispatcherBase
    {
        public ScormExamInteractionDispatcher(
            IAttemptFactory attemptFactory,
            IAttemptAnswerWriter answerWriter,
            IAttemptTasksNavigator taskNavigator,
            IAttemptFinalizer<ExamAttempt> attemptFinalizer,
            IProfileResultService profileResultService)
            : base(attemptFactory, answerWriter, taskNavigator, attemptFinalizer, profileResultService)
        {
        }

        protected override TestResultsViewModel CreateViewModel()
        {
            return new ScormTestResultsViewModel();
        }

        protected override void FinalizeProfiles(AttemptFinalizationResult<ExamAttempt> result, int profileId, TestResultsViewModel viewModel)
        {
            var profileResultInfo = ProfileResultService.FinishProfilesOnAttemptFinish(result.ConcreteEmployeeId, result.Attempt.Test.UniqueId);
            var vm = (ScormTestResultsViewModel) viewModel;

            vm.DoneProfileIds = profileResultInfo.DoneProfileIds.ToJson();
            vm.FailedProfileIds = profileResultInfo.FailedProfileIds.ToJson();
            vm.IsFinishedProfile = ProfileResultService.IsFinished(profileId, result.ConcreteEmployeeId);
        }
    }
}