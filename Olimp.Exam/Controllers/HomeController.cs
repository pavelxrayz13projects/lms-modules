using System.Net;
using Olimp.Core;
using Olimp.Core.Mvc.Security;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Exam.Auth;
using Olimp.Domain.Exam.Entities.Pseudo;
using Olimp.Exam.ViewModels;
using Olimp.Web.Controllers;
using Olimp.Web.Controllers.Learning.Exam;
using Olimp.Web.Controllers.Users;
using System;
using System.Web.Mvc;

namespace Olimp.Exam.Controllers
{
    [AllowByConfiguration(FieldName = "AllowExam")]
    [CheckActivation]
    public class HomeController : ExamUserController
    {
        private readonly IPersonalCatalogueProvider _catalogueProvider;

        public HomeController(IPersonalCatalogueProvider catalogueProvider, ISimpleWebAuthProvider authProvider)
            : base(authProvider)
        {
            if (catalogueProvider == null) throw new ArgumentNullException("catalogueProvider");

            _catalogueProvider = catalogueProvider;
        }

        public ActionResult GetCourse(Guid employeeId)
        {
            return Json(_catalogueProvider.GetPersonalExamCourses(employeeId));
        }

        public override bool IsAuthorizing
        {
            get { return base.EmployeeId == Guid.Empty && ValueProvider.GetValue("employeeId") == null; }
        }

        [HttpGet]
        public ActionResult Index(Guid? employeeId)
        {
            return EnterExam(employeeId, result =>
            {
                ExitIntegrationMode();

                if (!result.IsAuthorized)
                    return RedirectToAction("User", "AuthUser", new { area = "", returnUrl = Url.Action("Index") });

                return null;
            });
        }

        [HttpPost]
        public ActionResult BeginIntegration(string referer, Guid employeeId)
        {
            return EnterExam(employeeId, result =>
            {
                EnterIntegrationMode(referer, new { IntegrationRoot = "Exam/Home/Index" });

                return null;
            });
        }

        public ActionResult BeginScormIntegration(string callback, Guid employeeId, int profileId)
        {
            if (profileId == default (int))
                profileId = ExamAndPrepareScormIntegrationHelper.GetProfileIdFromCookisOrUrl(Request);

            if (profileId == default(int))
            {
                const string errorMessage = "profileId is not specified.";
                OlimpApplication.Logger.Error(errorMessage);
                return new HttpStatusCodeResult((int)HttpStatusCode.BadRequest, errorMessage);
            }
            
            return EnterScormExam(callback, employeeId, profileId, result =>
            {
                //We don't need referer, so, using fake uri here
                EnterIntegrationMode("http://example.com", new
                {
                    IntegrationRoot = "Exam/Home/ExamScorm",
                    profileId
                });

                return null;
            });
        }

        [HttpPost]
        public ActionResult Logout()
        {
            AuthProvider.Logout(Plugin.AuthCookie, ControllerContext.RequestContext);

            return RedirectToRoute("Default");
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
            Response.AppendHeader("Expires", "0");
        }

        [NonAction]
        private ActionResult EnterExam(Guid? employeeId, Func<SimpleAuthResult, ActionResult> processAuthResult = null)
        {
            return EnterExam(employeeId, result => View("Index", new HomeIndexViewModel
            {
                ConcreteEmployeeId = result.ConcreteEmployeeId.Value,
                Employee = result.Employee,
                IsAnonymousMode = result.IsAnonymous.Value,
                PassedMaterials = result.PassedMaterials
            }),
            processAuthResult);
        }

        [NonAction]
        private ActionResult EnterScormExam(
            string callback,
            Guid? employeeId,
            int profileId,
            Func<SimpleAuthResult, ActionResult> processAuthResult = null)
        {
            return EnterExam(
                employeeId,
                result => JsonpPartialView(callback, "ExamScormCoursesTree", new ScormHomeIndexViewModel
                {
                    ConcreteEmployeeId = result.ConcreteEmployeeId.Value,
                    Employee = result.Employee,
                    IsAnonymousMode = result.IsAnonymous.Value,
                    PassedMaterials = result.PassedMaterials,
                    TreeData = _catalogueProvider.GetPersonalExamCourses(profileId),
                    OlimpPath = Request.Url.GetRequestUrlBase()
                }),
                processAuthResult);
        }

        [NonAction]
        private ActionResult EnterExam(
            Guid? employeeId,
            Func<SimpleAuthExamResult, ActionResult> getActionResult,
            Func<SimpleAuthResult, ActionResult> processAuthResult = null)
        {
            var result = AuthProvider.AuthorizeForExam(Plugin.AuthCookie, ControllerContext.RequestContext, employeeId);

            if (processAuthResult != null)
            {
                var actionResult = processAuthResult.Invoke(result);
                if (actionResult != null) return actionResult;
            }

            return getActionResult(result);
        }
    }
}