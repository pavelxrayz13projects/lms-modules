using Olimp.Core.Mvc.Security;
using Olimp.Domain.Exam.Auth;
using Olimp.Domain.Exam.Tickets;
using Olimp.Exam.Test;
using Olimp.Exam.ViewModels;
using Olimp.Web.Controllers.Learning;
using Olimp.Web.Controllers.Users;
using System;
using System.Web.Mvc;

namespace Olimp.Exam.Controllers
{
    [CheckActivation(Order = 1)]
    [AllowByConfiguration(FieldName = "AllowExam", Order = 2)]
    [RequireWorkplaceToken(Order = 3)]
    [LimitUsers(Order = 4)]
    public class TestController : ExamUserController
    {
        private ExamInteractionDispatcherBase _interactionDispatcher;
        private ITicketInfoProvider _ticketInfoProvider;

        public TestController(
            ISimpleWebAuthProvider authProvider,
            ITicketInfoProvider ticketInfoProvider,
            ExamInteractionDispatcherBase interactionDispatcher)
            : base(authProvider)
        {
            if (interactionDispatcher == null)
                throw new ArgumentNullException("interactionDispatcher");

            if (ticketInfoProvider == null)
                throw new ArgumentNullException("ticketInfoProvider");

            _interactionDispatcher = interactionDispatcher
                .SetRedirectToResult(attemptId => RedirectToAction("Result", new { attemptId }))
                .SetRedirectToNextQuestion(attemptId => RedirectToAction("RenderView", new { attemptId }))
                .SetRenderQuestionView(model => View("Test", model))
                .SetRenderResultsView(model => View("Result", model));

            _ticketInfoProvider = ticketInfoProvider;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            this.Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            this.Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
            this.Response.AppendHeader("Expires", "0"); // Proxies.
        }

        protected override bool KeepIntegrationContext(IntegrationContext ctx)
        {
            if (ctx["IntegrationRoot"] == "Exam/Home/Index")
                return true;

            var result = this.ValueProvider.GetValue("attemptId");
            if (result == null)
                return false;

            return String.Equals(result.AttemptedValue, ctx["attemptId"], StringComparison.OrdinalIgnoreCase);
        }

        [HttpPost]
        public ActionResult BeginIntegration(string referer, Guid employeeId, int materialId)
        {
            this.AuthProvider.AuthorizeForExam(Plugin.AuthCookie, ControllerContext.RequestContext, employeeId);

            return _interactionDispatcher.StartNewTestOrContinue(employeeId, materialId, attempt =>
                EnterIntegrationMode(referer, new { IntegrationRoot = "Exam/Test/Test", attemptId = attempt.Id }));
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Test(Guid employeeId, int materialId)
        {
            return _interactionDispatcher.StartNewTestOrContinue(employeeId, materialId, attempt =>
            {
                if (IntegrationContext["IntegrationRoot"] != "Exam/Home/Index")
                    ExitIntegrationMode();
            });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Test(TestFormViewModel result)
        {
            return _interactionDispatcher.AcceptAnswer(result);
        }

        [ChildActionOnly]
        public ActionResult QuestionsList(Guid attemptId, bool navigationAllowed, Guid currentQuestionId)
        {
            var ticketInfo = _ticketInfoProvider.GetByAttemptId(attemptId);
            var model = new QuestionsListViewModel
            {
                AttemptId = attemptId,
                NavigationAllowed = navigationAllowed,
                CurrentQuestionId = currentQuestionId,
                Tasks = ticketInfo.Tasks
            };

            return PartialView(model);
        }

        public ActionResult RenderView(Guid attemptId, int? testResultId)
        {
            return _interactionDispatcher.ProvideNextQuestion(attemptId, testResultId);
        }

        //TODO: Optimize Lazy loading
        //TODO: Should be POST
        public ActionResult Result(Guid attemptId)
        {
            return _interactionDispatcher.FinalizeIfNeededAndShowResult(attemptId, default(int));
        }
    }
}