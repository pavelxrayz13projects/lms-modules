﻿using Olimp.Domain.Exam.Auth;
using Olimp.Web.Controllers.Learning;
using System;
using System.Web.Mvc;

namespace Olimp.Exam.Controllers
{
    public abstract class ExamUserController : IntegrationUserController
    {
        protected ISimpleWebAuthProvider AuthProvider { get; private set; }

        public ExamUserController(ISimpleWebAuthProvider authProvider)
        {
            if (authProvider == null)
                throw new ArgumentNullException("authProvider");

            this.AuthProvider = authProvider;
        }

        protected override string UserCookieName { get { return Plugin.AuthorizationCookieName; } }

        protected override string UserCookiePath { get { return Plugin.AuthorizationCookiePath; } }

        protected override bool ExitIntegrationModeOnReturn { get { return true; } }

        protected override bool KeepIntegrationContext(IntegrationContext ctx)
        {
            return true;
        }

        public override ActionResult ReturnToRefereingSystem()
        {
            this.AuthProvider.Logout(Plugin.AuthCookie, this.ControllerContext.RequestContext);

            return base.ReturnToRefereingSystem();
        }

        public override void DeleteUserCookie()
        {
            Plugin.AuthCookie.Unset(this.Response);
        }
    }
}