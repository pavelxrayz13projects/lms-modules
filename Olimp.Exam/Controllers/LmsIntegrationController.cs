﻿using Olimp.Archive.ViewModels;
using Olimp.Configuration;
using Olimp.Domain.LearningCenter;
using Olimp.Domain.LearningCenter.DataServices;
using Olimp.Domain.LearningCenter.Dto;
using Olimp.Exam.Helpers;
using Olimp.Exam.Test;
using Olimp.Exam.ViewModels;
using Olimp.Exam.ViewModels.LmsIntegration;
using Olimp.Exceptions;
using Olimp.Web.Controllers;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Learning.Exam;
using Olimp.Web.Controllers.Security;
using System;
using System.Web;
using System.Web.Mvc;

namespace Olimp.Exam.Controllers
{
    [JsonpHandleError(Order = 10)]
    public class LmsIntegrationController : PController
    {
        private readonly ILmsIntegrationEmployeeService _employeeService;
        private readonly ExamInteractionDispatcherBase _interactionDispatcher;

        public LmsIntegrationController(ILmsIntegrationEmployeeService employeeService, ExamInteractionDispatcherBase interactionDispatcher)
        {
            if (employeeService == null)
                throw new ArgumentNullException("employeeService");

            if (interactionDispatcher == null)
                throw new ArgumentNullException("interactionDispatcher");

            _employeeService = employeeService;

            _interactionDispatcher = interactionDispatcher
                .SetRedirectToResult(attemptId => RedirectToAction("ExamResult", new
                {
                    attemptId,
                    Callback = this.GetCallback(),
                    profileId = ExamAndPrepareScormIntegrationHelper.GetProfileIdFromCookisOrUrl(Request)
                }))
                .SetRedirectToNextQuestion(attemptId => RedirectToAction("ExamQuestion", new
                {
                    attemptId, Callback = this.GetCallback(),
                }))
                .SetRenderQuestionView(model => JsonpPartialView(this.GetCallback(), "ExamQuestion", this.CreateLmsModel(model)))
                .SetRenderResultsView(model => JsonpPartialView(this.GetCallback(), "ExamResult", this.CreateLmsModel((ScormTestResultsViewModel)model)));
        }

        [NonAction]
        private QuestionTaskViewModel TranformQuestionLinks(QuestionTaskViewModel questionModel)
        {
            var olimpUrl = Request.Url.GetRequestUrlBase();

            questionModel.Answer = TestHelper.ConvertImagesRelativeHrefs(olimpUrl, questionModel.Answer);
            questionModel.Question = TestHelper.ConvertImagesRelativeHrefs(olimpUrl, questionModel.Question);

            return questionModel;
        }

        [NonAction]
        private LmsIntegrationViewModel<TModel> CreateLmsModel<TModel>(TModel model)
        {
            return new LmsIntegrationViewModel<TModel>
            {
                RealModel = model,
                OlimpPath = Request.Url.GetRequestUrlBase()
            };
        }

        [NonAction]
        private LmsIntegrationViewModel CreateLmsModel()
        {
            return new LmsIntegrationViewModel { OlimpPath = Request.Url.GetRequestUrlBase() };
        }

        [NonAction]
        private string GetCallback()
        {
            var result = this.ValueProvider.GetValue("Callback");
            if (result != null && !String.IsNullOrWhiteSpace(result.AttemptedValue))
                return result.AttemptedValue;

            throw new InvalidOperationException("No Callback value provided by the request");
        }

        public ActionResult AuthorizeEmployee(LmsIntegrationAuthForPrepareRequestModel requestModel)
        {
            Guid workPlaceToken;
            var workPlaceTokenCookie = Request.Cookies["WorkplaceToken"];

            if (workPlaceTokenCookie == null || !Guid.TryParse(workPlaceTokenCookie.Value, out workPlaceToken))
            {
                throw new CookiesDisabledException();
            }
            string employeeId;
            using (var uow = UnitOfWork.CreateWrite())
            {
                var lmsEmployee = new LmsEmployeeInfo
                {
                    ProfileId = requestModel.ProfileId,
                    Number = requestModel.PersonnelNumber,
                    FullName = requestModel.FullName,
                    Login = requestModel.Login,
                    Email = requestModel.Email,
                    Company = requestModel.Organization,
                    Position = requestModel.Position
                };

                var employee = _employeeService.CreateEmployeeForLmsIntegration(lmsEmployee, workPlaceToken);
                employeeId = Convert.ToString(employee.Id);
                uow.Commit();
            }

            var prepareCookie = new HttpCookie("prepareCookie")
            {
                Expires = DateTime.Today.AddDays(1),
                Path = "/Prepare"
            };
            prepareCookie.Values.Set("employeeId", employeeId);
            Response.SetCookie(prepareCookie);

            return Jsonp(requestModel.Callback, new { employeeId });
        }

        public ActionResult CloseWindow()
        {
            return View("CloseWindow");
        }

        public ActionResult GetQuitExam(LmsIntegrationRequestModel requestMode)
        {
            return JsonpPartialView(requestMode.Callback, "QuitExam", CreateLmsModel());
        }

        public ActionResult GetCssLinks(LmsIntegrationRequestModel requestModel)
        {
            string lmsCss;
            var olimpPath = Request.Url.GetRequestUrlBase();
            using (UnitOfWork.CreateRead())
            {
                lmsCss = Config.Get<OlimpConfiguration>().LmsCss;
                if(!String.IsNullOrWhiteSpace(lmsCss))
                    lmsCss = lmsCss.Replace("{olimp-path}", olimpPath);
            }

            return JsonpPartialView(requestModel.Callback, "CssLinks", new LmsIntegrationViewModel<LmsIntegrationAppearanceViewModel>
            {
                OlimpPath = olimpPath,
                RealModel = new LmsIntegrationAppearanceViewModel { LmsCss = lmsCss }
            });
        }

        public ActionResult GetJsLinks(LmsIntegrationRequestModel requestModel)
        {
            return JsonpPartialView(requestModel.Callback, "JsLinks", CreateLmsModel());
        }

        public ActionResult GetSplashScreen(LmsIntegrationRequestModel requestModel)
        {
            return JsonpPartialView(requestModel.Callback, "Splash", CreateLmsModel());
        }

        public ActionResult BeginExam(Guid employeeId, int materialId)
        {
            return _interactionDispatcher.StartNewTestOrContinue(employeeId, materialId);
        }

        public ActionResult ExamAnswer(TestFormViewModel result)
        {
            return _interactionDispatcher.AcceptAnswer(result);
        }

        public ActionResult ExamQuestion(Guid attemptId, int? testResultId)
        {
            return _interactionDispatcher.ProvideNextQuestion(attemptId, testResultId);
        }

        public ActionResult ExamResult(Guid attemptId, int profileId)
        {
            return _interactionDispatcher.FinalizeIfNeededAndShowResult(attemptId, profileId, TranformQuestionLinks);
        }
    }
}