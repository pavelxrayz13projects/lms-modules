using Olimp.Core;
using Olimp.Exam.ViewModels;
using Olimp.Exam.ViewModels.LmsIntegration;
using Olimp.Web.Controllers;
using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Olimp.Exam.Helpers
{
    public static class TestHelper
    {
        private const string QuestionImagesPartOfHref = "/QuestionImages/";
        private const string CachedImagesPartOfHref = "/CachedImages/";
        private const string NormativeImagesPartOfHref = "/NormativeImages/";

        public static string ConvertImagesRelativeHrefs(string olimpUrl, string content)
        {
            if (String.IsNullOrWhiteSpace(content) || String.IsNullOrWhiteSpace(olimpUrl))
                return content;

            var questionImagesReplacement = olimpUrl + QuestionImagesPartOfHref.Substring(1);
            var cachedImagesReplacement = olimpUrl + CachedImagesPartOfHref.Substring(1);
            var normativeImagesReplacement = olimpUrl + NormativeImagesPartOfHref.Substring(1);

            return content.Replace(QuestionImagesPartOfHref, questionImagesReplacement)
                .Replace(CachedImagesPartOfHref, cachedImagesReplacement)
                .Replace(NormativeImagesPartOfHref, normativeImagesReplacement);
        }

        public static string QuestionForm(this HtmlHelper<LmsIntegrationViewModel<TestViewModel>> html)
        {
            var olimpUrl = html.ViewContext.HttpContext.Request.Url.GetRequestUrlBase();

            return RenderQuestionForm(
                html, 
                html.ViewData.Model.RealModel,
                t => ConvertImagesRelativeHrefs(olimpUrl, t));
        }

        public static string QuestionForm(this HtmlHelper<TestViewModel> html)
        {
            return RenderQuestionForm(html, html.ViewData.Model);
        }

        private static string RenderQuestionForm(HtmlHelper html, TestViewModel model, Func<string, string> transformText = null)
        {
            if (transformText == null)
                transformText = t => t;

            var result = new StringBuilder();

            if (model == null)
                return result.ToString();

            if (model.ShowStopOffer)
            {
                result.AppendFormat("<div class=\"block\">{0}<br />{1}</div>",
                    I18N.Exam.StopMessage1,
                    I18N.Exam.StopMessage2);
                result.AppendFormat("<div class=\"olimp-buttons-container\"><button id=\"Stop\" type=\"button\" >{0}</button></div>{1}",
                    I18N.Exam.StopButton,
                    html.Hidden("TimeLimit", model.TimeLimit));
            }
            else
            {
                result.AppendFormat("<div id='question-text'>{0}</div>{1}{2}{3}",
                    transformText.Invoke(model.Question.Text),
                    html.Hidden("AttemptId", model.AttemptId),
                    html.Hidden("TimeLimit", model.TimeLimit),
                    html.Hidden("TimeLimitType", model.TimeLimitType));

                if (model.TimeLimitType == 3)
                    result.Append(html.Hidden("TimeLimitAll", model.TimeLimitAll));

                result.Append(html.Hidden("QuestionId", model.Question.UniqueId));
                result.Append("<div class=\"block\"><table id=\"question-table\" class=\"form-table\" cellspacing=\"0\" cellpadding=\"0\" >");

                var i = 0;
                foreach (var an in model.Question.Answers)
                {
                    i++;

                    var check = model.GivenAnswers.Contains(an.Number);

                    var cls = "";
                    if (i == 0)
                        cls = "class=\"first\"";
                    else if (i == model.Question.Answers.Length)
                        cls = "class=\"last\"";

                    result.AppendFormat("<tr {0}><td class=\"answer-input checkbox\"><input type=\"{1}\" name=\"Answers\" {2} value=\"{3}\"/></td><td class=\"{4}\">{5}</td></tr>",
                        cls,
                        !model.Question.AllowMultiple ? "radio" : "checkbox",
                        check ? "checked=\"true\"" : "",
                        an.Number,
                        OlimpApplication.GodMode && !an.IsCorrect ? "answer-text godmode-incorrect" : "answer-text",
                        transformText.Invoke(an.Text));
                }

                result.Append("</table></div>");

                result.AppendFormat("<div class=\"olimp-buttons-container margin-buttons \">{0}<button id=\"Next\" type=\"submit\" >{1}</button></div>",
                    model.ShowStopButton
                        ? String.Format("<button id=\"Stop\" type=\"button\" >{0}</button>", I18N.Exam.StopButton)
                        : "",
                    I18N.Exam.AnswerButton);
            }

            return result.ToString();
        }
    }
}