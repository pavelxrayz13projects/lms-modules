﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<TestViewModel>" MasterPageFile="~/Views/Shared/Exam.master"%>
<%@ Import Namespace="I18N=Olimp.Exam.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Exam_Js" runat="server">
	<% Platform.RenderExtensions("/olimp/exam/js"); %>
	<script type="text/javascript">
		$(function () {	
			if(typeof clockID !=="undefined"){
				clearTimeout(clockID);
			}
			<% if (Model.TimeLimit > 0 && Model.TimeLimitType > 1) {%>	
				clockID = setTimeout("UpdateClock(<%=Model.TimeLimit%> - 1, false)", 950);
				$('#Next').olimpbutton().click(function(){
					if ($("input:checkbox:checked").length == 0 && $("input:radio:checked").length == 0 && $("#TimeLimit").val() > 0){
						alert("<%= I18N.Exam.AnswerShouldBeChoosen %>");
						return false;
					}
					clearTimeout(clockID);
				});
			<%}else{%>
				clockID = setTimeout("UpdateClock(<%=Model.TimeLimit%> + 1, true)", 950);
				$('#Next').olimpbutton().click(function(){
					if ($("input:checkbox:checked").length == 0 && $("input:radio:checked").length == 0){
						alert("<%= I18N.Exam.AnswerShouldBeChoosen %>");
						return false;
					}
					clearTimeout(clockID);
				});
			<%}%>
				$('#Stop').olimpbutton().click(function(){
					clearTimeout(clockID);
					window.location.href = '<%=Url.Action("Result")%>' + '?attemptId=<%=Model.AttemptId%>';
					return false;
				});

		});		
	</script>	
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Exam_Status" runat="server">
		<div id="timer"><%= Model.TimeLimitString%></div>
		<div id="timer-caption"><%= I18N.Exam.TimeOfAnswer %></div>  	
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Exam_Sidebar" runat="server">
	<div id="question-nav">	
		<%Html.RenderAction("QuestionsList", new {
            attemptId = Model.AttemptId, 
            navigationAllowed = Model.NavigationAllowed, 
            currentQuestionId = Model.Question != null ? Model.Question.UniqueId : Guid.Empty
        });%>
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Exam_Navigation" runat="server">
	<a href="#" class="active"><%= I18N.Exam.Ticket %> <%= Model.TicketNumber %></a>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Exam_Content" runat="server">		
	
<% using (Html.BeginForm(new { controller="Test", action="Test" })) { %>	
	<%= Html.QuestionForm() %>
<% } %>
	
</asp:Content>