﻿<%@ Page Language="C#" Inherits="IntegratableViewPage<TestResultsViewModel>" MasterPageFile="~/Views/Shared/SimpleLayout.master"%>
<%@ Import Namespace="I18N=Olimp.Exam.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Js" runat="server">
	<script type="text/javascript">
	$(function () { $('#finish, #next').olimpbutton(); });
	</script>
		
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_TopRight" runat="server">
	<div class="top-right-text">	
		<table cellpadding="0" cellspacing="0">	
			<tr>
				<td><%= Model.EmployeeFullName %></td>
			</tr>
		</table>
	</div>	
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">
	
<%Html.RenderPartial("AttemptPassedTemplate_doT");%>	
<%Html.RenderPartial("AnswerResultTemplate_doT");%>		
	
<div id="protocol-info">
	<div class="print-block" style="height: 19px; margin-bottom: 6px;">
		<a href="<%= Url.Action("Print", "Archive", new { area = "Admin", attemptId = Model.AttemptId} ) %>" target="_blank" class="right">
			<span><%= Olimp.I18N.PlatformShared.PrintLink %></span>
			<div class="icon icon-print"></div>
		</a>	
	</div>
	<div class="block" style="clear: both;" >
		<table class="form-table">
			<tr class="first">			
				<th><%= Olimp.Archive.I18N.Archive.Course %></th>
				<td><%= Model.MaterialFullName %></td>
			</tr>	
			<tr>	
				<th><%= Olimp.I18N.Domain.Attempt.Ticket %></th>
				<td><%= Model.TicketNumber %></td>
			</tr>		
			<tr>			
				<th><%= Olimp.Archive.I18N.Archive.StartTime %></th>
				<td><%= Model.StartTime %></td>
			</tr>
			<%if (Model.MaxErrorsCount.Length > 0) {%>
				<tr>			
					<th><%= Olimp.I18N.Domain.Attempt.MaxErrors %></th>
					<td><%= Model.MaxErrorsCount %></td>
				</tr>	
			<%}%>	
		
			<tr>
				<th><%= I18N.Exam.TotalErrors %></th>
				<td><%= Model.ErrorsCount %></td>
			</tr>
		
			<%if (!String.IsNullOrWhiteSpace(Model.Company)) {%>
				<tr>						
					<th><%= Olimp.Archive.I18N.Archive.Company %></th>
					<td><%= Model.Company %></td>
				</tr>
			<%}%>
			<%if (!String.IsNullOrWhiteSpace(Model.Job)) {%>
				<tr>	
					<th><%= Olimp.I18N.Domain.Attempt.Job %></th>
					<td><%= Model.Job %></td>
				</tr>
			<%}%>
					
			<tr class="last">			
				<th><%= Olimp.Archive.I18N.Archive.Result %></th>
			
				<td>
					<%if (Model.Finished) {%>
						<%if (Model.Passed) {%>
							<span class="passed">	<%= Olimp.I18N.Domain.Attempt.Passed %> </span>
						<%}else{%>	
							<span class="failed">	<%= Olimp.I18N.Domain.Attempt.Failed %> </span>
						<%}%>
					<%}%>	
				</td>
			</tr>			
		</table>		
	</div>
</div>
	
	<h1><%= I18N.Exam.Answers %></h1>
	<div class="block">
		<% Html.Table(new TableOptions {                
			TableData = Model.TableData,
			BodyTemplateEngine = TemplateEngines.DoT,
			Sorting = false, 
			Paging = false, 
			Columns = new[] {  
				new ColumnOptions("Number", Olimp.I18N.Domain.Attempt.Number), 
               	new ColumnOptions("Question", Olimp.I18N.Domain.Attempt.Question), 
               	new ColumnOptions("Answer", Olimp.I18N.Domain.Attempt.Answer), 
               	new ColumnOptions("Correct", Olimp.I18N.Domain.Attempt.AnswerCorrect) { TemplateId = "answer-result" } },                     
           	}, new { id = "test-results" }); %>	
	</div>
        
    <% if (IntegrationContext.IntegrationState != IntegrationContextState.InIntegrationContext) { %>
        <%= Html.ActionLink("Завершить", "Index", "Home", new { }, new { id="finish" }) %>
    	<%= Html.ActionLink("Следующий экзамен", "Index", "Home", new { employeeId = Model.EmployeeId }, new { id="next" }) %>
    <% } else { %>     
        <% Html.ReturnToReferringSystemButton("Завершить", new { attemptId = Model.AttemptId }, new { id = "finish" }); %>
        <% if (IntegrationContext["IntegrationRoot"] == "Exam/Home/Index") { %>
            <% using (Html.BeginForm("BeginIntegration", "Home", new { employeeId = Model.EmployeeId }, FormMethod.Post, new { style="display: inline" })) { %>
                <input type="hidden" name="referer" value="<%: IntegrationContext.IntegrationReferer %>" />
                <button id="next" type="submit">Следующий экзамен</button>
            <% } %>            
        <% } %>
    <% } %>
</asp:Content>
