﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<QuestionsListViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Exam.I18N" %>

<div class="questions">
<span style="float: left; margin-right: 6px;"><%= I18N.Exam.Questions %></span>

<% 
    foreach(var task in Model.Tasks) 
    {
        string classPostfix = String.Empty;        
        if (task.QuestionId == Model.CurrentQuestionId)
            classPostfix = "active";
        else if (task.IsComplete)
            classPostfix = "done";
        
        if (Model.NavigationAllowed) { %>
            <%= Html.ActionLink(task.Number.ToString(), "RenderView", new { testResultId = task.TaskId, attemptId = Model.AttemptId }, 
                new { @class = String.Join(" ", "navigation-link", classPostfix) }) %>
        <% } else { %>
            <a class="<%= classPostfix %>" href="#"><%= task.Number %></a>
        <%}
    }
%>	
</div>	
	
