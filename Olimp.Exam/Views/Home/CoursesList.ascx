﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<HomeIndexViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Exam.I18N" %>
<% Html.Catalogue(	
	new CatalogueOptions { 
		SelectUrl = Url.Action("GetCourse", new { employeeId = Model.ConcreteEmployeeId }),
		TemplateEngine = TemplateEngines.DoT,
		PassedMaterials = Model.PassedMaterials,
		PassedComparisonField = "guid",
		InitialExpanded = true, 
		ShowCheckBoxes = false,
		EmptyMessage = I18N.Exam.NoCoursesInProfile,
        MaterialLink = (Url.Action("Test", new { controller = "Test", employeeId = Model.ConcreteEmployeeId }) + "&materialId={id}")   
	}); 
%>