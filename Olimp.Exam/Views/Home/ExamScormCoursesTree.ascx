﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<ScormHomeIndexViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Exam.I18N" %>

<script type="text/javascript">
    function beginExam(id) {
        callJsonPService('LmsIntegration/BeginExam', { employeeId: olimpContext.employeeId, materialId: id }, setDynamicContent);
    }

    function quitExam() {
        callJsonPService('LmsIntegration/GetQuitExam', {}, setDynamicContent);
    }
</script>

<div id="status">
    <a class="status-link" onclick="javascript: quitExam(); return false;"  href="#">
        <table>
            <tr>
                <td><div class="icon icon-finish-flag"></div></td>
                <td class="text">Закончить экзамен</td>
            </tr>
        </table>
    </a>
</div>

<div id="head">
    <a class="logo">
        <img src="<%= Url.VersionSuffixedContent("~/Content/Images/logo.gif", Model.OlimpPath) %>" alt="">
    </a>
                                
    <div class="top-right-text">	
        <table cellpadding="0" cellspacing="0">	
            <tr>
                <td><%= Model.Employee.ToString()%></td>
            </tr>
        </table>
    </div>
</div>

<div id="main">
    <h1><%= I18N.Exam.SelectCourse %></h1>
    
    <% Html.Catalogue(
        new CatalogueOptions { 
            TreeData = Model.TreeData,
            TemplateEngine = TemplateEngines.DoT,
            PassedMaterials = Model.PassedMaterials,
            PassedComparisonField = "guid",
            InitialExpanded = true, 
            ShowCheckBoxes = false,
            EmptyMessage = I18N.Exam.NoCoursesInProfile,
            MaterialLink = "javascript:beginExam({id})"
        }); 
    %>
</div>

<div id="foot"></div>