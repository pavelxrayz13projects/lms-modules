﻿<%@ Page Language="C#" Inherits="IntegratableViewPage<HomeIndexViewModel>" MasterPageFile="~/Views/Shared/SimpleLayout.master" %>
<%@ Import Namespace="I18N=Olimp.Exam.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Status" runat="server">
	<% if (Model.IsAnonymousMode || IntegrationContext.IntegrationState == IntegrationContextState.InIntegrationContext) { %>	
		<% Html.RenderPartial("Logout"); %>	
	<% } %>
</asp:Content>
<asp:Content ContentPlaceHolderID="Olimp_TopRight" runat="server">
	<div class="top-right-text">	
		<table cellpadding="0" cellspacing="0">	
			<tr>
				<td><%= Model.Employee.ToString()%></td>
			</tr>
		</table>
	</div>
</asp:Content>
	
<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">
	<h1><%= I18N.Exam.SelectCourse %></h1>
	<%Html.RenderPartial("CoursesList");%>
</asp:Content>
