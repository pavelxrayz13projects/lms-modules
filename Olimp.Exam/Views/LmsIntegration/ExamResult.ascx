﻿<%@ Control Language="C#" Inherits="IntegratableUserControl<LmsIntegrationViewModel<ScormTestResultsViewModel>>" %>
<%@ Import Namespace="I18N=Olimp.Exam.I18N" %>

<script type="text/javascript">
    $(function () {
        var doneProfileIds = <%=Model.RealModel.DoneProfileIds%>;
        var failedProfileIds = <%=Model.RealModel.FailedProfileIds%>;
        var profileId = olimpContext.profileId;

        if (doneProfileIds.indexOf(profileId) >= 0) {
            scorm.set('cmi.core.lesson_status', 'completed');
        } else if (failedProfileIds.indexOf(profileId) >= 0 && scorm.get('cmi.core.lesson_status') !== 'failed') {
            scorm.set('cmi.core.lesson_status', 'failed');
        }
        
        $('#next').olimpbutton().click(function () {
            <% if (Model.RealModel.IsFinishedProfile) {  %>
                    callJsonPService('LmsIntegration/GetQuitExam', {}, setDynamicContent);
            <% } else {%>
                    callJsonPService('/Exam/Home/BeginScormIntegration', getOlimpContext(), setDynamicContent);
            <% }%>

            return false;
        });

        var olimpLinkLastElementIndex = olimpContext.olimpLink.length-1,
            olimpLink = olimpContext.olimpLink.lastIndexOf("/") == olimpLinkLastElementIndex
                ? olimpContext.olimpLink.substring(0, olimpLinkLastElementIndex)
                : olimpContext.olimpLink,
            printLink = olimpLink + "<%= Url.Action("Print", "Archive", new { area = "Admin", attemptId = Model.RealModel.AttemptId} ) %>";
        $('#print-link').attr('href', printLink);
    });
</script>

<div id="status"></div>
    
<div id="head">
    <a class="logo">
        <img src="<%= Url.VersionSuffixedContent("~/Content/Images/logo.gif", Model.OlimpPath) %>" alt="">
    </a>
    <div class="top-right-text">
        <table cellpadding="0" cellspacing="0">	
            <tr>
                <td><%= Model.RealModel.EmployeeFullName %></td>
            </tr>
        </table>
    </div>	
</div>

<div id="main">
    <%Html.RenderPartial("AttemptPassedTemplate_doT");%>
    <%Html.RenderPartial("AnswerResultTemplate_doT");%>
    
    <div id="protocol-info">
        <div class="print-block" style="height: 19px; margin-bottom: 6px;">
            <a id="print-link" href="#" target="_blank" class="right">
                <span><%= Olimp.I18N.PlatformShared.PrintLink %></span>
                <div class="icon icon-print"></div>
            </a>
        </div>
        <div class="block" style="clear: both;" >
            <table class="form-table">
                <tr class="first">
                    <th><%= Olimp.Archive.I18N.Archive.Course %></th>
                    <td><%= Model.RealModel.MaterialFullName %></td>
                </tr>
                <tr>
                    <th><%= Olimp.I18N.Domain.Attempt.Ticket %></th>
                    <td><%= Model.RealModel.TicketNumber %></td>
                </tr>
                <tr>
                    <th><%= Olimp.Archive.I18N.Archive.StartTime %></th>
                    <td><%= Model.RealModel.StartTime %></td>
                </tr>
                <%if (Model.RealModel.MaxErrorsCount.Length > 0) {%>
                    <tr>
                        <th><%= Olimp.I18N.Domain.Attempt.MaxErrors %></th>
                        <td><%= Model.RealModel.MaxErrorsCount %></td>
                    </tr>
                <%}%>
        
                <tr>
                    <th><%= I18N.Exam.TotalErrors %></th>
                    <td><%= Model.RealModel.ErrorsCount %></td>
                </tr>
        
                <%if (!String.IsNullOrWhiteSpace(Model.RealModel.Company)) {%>
                    <tr>
                        <th><%= Olimp.Archive.I18N.Archive.Company %></th>
                        <td><%= Model.RealModel.Company %></td>
                    </tr>
                <%}%>
                <%if (!String.IsNullOrWhiteSpace(Model.RealModel.Job)) {%>
                    <tr>	
                        <th><%= Olimp.I18N.Domain.Attempt.Job %></th>
                        <td><%= Model.RealModel.Job %></td>
                    </tr>
                <%}%>
                        
                <tr class="last">
                    <th><%= Olimp.Archive.I18N.Archive.Result %></th>
                
                    <td>
                        <%if (Model.RealModel.Finished) {%>
                            <%if (Model.RealModel.Passed) {%>
                                <span class="passed">	<%= Olimp.I18N.Domain.Attempt.Passed %> </span>
                            <%}else{%>	
                                <span class="failed">	<%= Olimp.I18N.Domain.Attempt.Failed %> </span>
                            <%}%>
                        <%}%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
    <h1><%= I18N.Exam.Answers %></h1>
    <div class="block">
        <% Html.Table(new TableOptions {
            TableData = Model.RealModel.TableData,
            BodyTemplateEngine = TemplateEngines.DoT,
            Sorting = false, 
            Paging = false, 
            Columns = new[] {
                new ColumnOptions("Number", Olimp.I18N.Domain.Attempt.Number), 
                new ColumnOptions("Question", Olimp.I18N.Domain.Attempt.Question), 
                new ColumnOptions("Answer", Olimp.I18N.Domain.Attempt.Answer), 
                new ColumnOptions("Correct", Olimp.I18N.Domain.Attempt.AnswerCorrect) { TemplateId = "answer-result" } },
            }, new { id = "test-results" }); %>
    </div>
    <button id="next">Продолжить</button>
</div>

<div id="foot"></div>