﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LmsIntegrationViewModel>" %>

<script>
    $(function () {
        var lessonStatus = scorm.get('cmi.core.lesson_status');

        if (lessonStatus === 'completed')
            $('#exam-result-content').text('Вы сдали профиль.');
        else if (lessonStatus === 'failed')
            $('#exam-result-content').text('Вы не сдали профиль.');
        else {
            scorm.set('cmi.core.lesson_status', 'incomplete');
            $('#exam-result-content').text('Вы прошли не все курсы данного профиля. Пожалуйста, пройдите остальные курсы.');
        }
        scorm.quit();
    });
</script>

<style>
    #exam-result-container{
        position:absolute;
        top:0;
        bottom:0;
        left:0;
        right:0;
        overflow:hidden;
        z-index:-1
    }
    #exam-result-table {
        display: table; 
        width: 100%; 
        height: 100%;
    }
    #exam-result-table-cell {
        display: table-cell; 
        text-align: center; 
        vertical-align: middle;
    }
    #exam-result-content {
        font-weight: bold;
        margin: 50px 30px 0 30px;
        max-width: 500px;
        display: inline-block;
    }
</style>

<div id="exam-result-container">
    <div id="exam-result-table">
        <div id="exam-result-table-cell">
            <div class="logo">
                 <img src="<%= Url.VersionSuffixedContent("~/Content/Images/logo.gif", Model.OlimpPath) %>" alt="" />
            </div>
            <div>
                <p id="exam-result-content"></p>
            </div>
        </div>
    </div>
</div>