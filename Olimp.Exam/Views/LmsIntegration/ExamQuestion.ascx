﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<LmsIntegrationViewModel<TestViewModel>>" %>
<%@ Import Namespace="I18N=Olimp.Exam.I18N" %>

<script type="text/javascript">
    $(function () {	
	    if(typeof clockID !== "undefined")
		    clearTimeout(clockID);
        
        <% if (Model.RealModel.TimeLimit > 0 && Model.RealModel.TimeLimitType > 1) { %>	

            clockID = setTimeout("UpdateClock(<%=Model.RealModel.TimeLimit%> - 1, false)", 950);
		    $('#Next').olimpbutton().click(function() {
		        if ($("input:checkbox:checked").length == 0 && $("input:radio:checked").length == 0 && $("#TimeLimit").val() > 0){
			        alert("<%= I18N.Exam.AnswerShouldBeChoosen %>");
                    return false;
		        }

		        clearTimeout(clockID);
		    });
		
        <% } else { %>

            clockID = setTimeout("UpdateClock(<%=Model.RealModel.TimeLimit%> + 1, true)", 950);

            $('#Next').olimpbutton().click(function () {
			    if ($("input:checkbox:checked").length == 0 && $("input:radio:checked").length == 0){
				    alert("<%= I18N.Exam.AnswerShouldBeChoosen %>");
					return false;
                }

                clearTimeout(clockID);
            });
        <% } %>

        $('#Stop').olimpbutton().click(function () {
            clearTimeout(clockID);

            callJsonPService('LmsIntegration/ExamResult', {
                attemptId: '<%=Model.RealModel.AttemptId%>',
                profileId: getOlimpContext().ProfileId
            }, setDynamicContent);

			return false;
        });

        var answerForm = $('#answer-form');
        answerForm.submit(function () {
            callJsonPService(answerForm.attr('action'), answerForm.serialize(), setDynamicContent);
            return false;
        });
    });		
</script>	

<div id="status">
    <div id="timer"><%= Model.RealModel.TimeLimitString%></div>
	<div id="timer-caption"><%= I18N.Exam.TimeOfAnswer %></div>  	
</div>

<div id="head">
    <a class="logo">
        <img src="<%= Url.VersionSuffixedContent("~/Content/Images/logo.gif", Model.OlimpPath) %>" alt="">
    </a>
									
	<div class="navigation">
		<div class="tabs">
			<div class="wrap">
				<a href="#" class="active"><%= I18N.Exam.Ticket %> <%= Model.RealModel.TicketNumber %></a>
			</div>
		</div>
		<div class="body">
			<div id="question-nav">	
		        <%Html.RenderAction("QuestionsList", "Test", new {
                    area = "Exam",
                    attemptId = Model.RealModel.AttemptId, 
                    navigationAllowed = Model.RealModel.NavigationAllowed, 
                    currentQuestionId = Model.RealModel.Question != null ? Model.RealModel.Question.UniqueId : Guid.Empty
                });%>
	        </div>
		</div>
	</div>	
</div>

<div id="main">	
    <% using (Html.BeginForm("ExamAnswer", "LmsIntegration", FormMethod.Get, new { id = "answer-form" })) { %>	
	    <%= Html.QuestionForm()%>
    <% } %>
</div>

<div id="foot"></div>