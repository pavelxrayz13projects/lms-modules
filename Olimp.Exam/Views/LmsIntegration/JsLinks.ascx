﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<LmsIntegrationViewModel>" %>

<% Platform.RenderExtensions("/olimp/exam/lms/js", new Dictionary<string, object> { { "Inline", true } }); %>

<script type="text/javascript">
    if (olimpContext && olimpContext.loadState)
        olimpContext.loadState.js = true;
</script>