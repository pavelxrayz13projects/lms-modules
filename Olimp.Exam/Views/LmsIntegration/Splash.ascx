﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<LmsIntegrationViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Exam.I18N" %>
<style type="text/css">
    #nodes-menu-container { width: 314px; margin-top: 25px;}   
</style>

<div id="cluster-splash-factoy">
    <div class="picture"></div>
</div>

<center id="cluster-splash-wrapper">
    <div class="system-logo">
        <img src="<%= Url.VersionSuffixedContent("~/Content/Images/logo.gif", Model.OlimpPath) %>" alt="" />
    </div>
    <div class="lms-nodes-menu-wrapper">
         <div id="nodes-menu-wrapper">
             <div id="nodes-menu-container">
                 <a id="exam-link" href="#" class="node-section-link">
                     <div class="node-section-icon section-icon-exam"></div>
                     <div class="node-section-text"><%= I18N.Exam.ExamTitle %></div>
                 </a>
                 <a id="prepare-link" href="#" target="_blank" class="node-section-link">
                     <div class="node-section-icon section-icon-prep"></div>
                     <div class="node-section-text"><%= I18N.Exam.PrepareTitle %></div>
                 </a>
                 <div style="clear: both;"></div>
             </div>
         </div>
    </div>
</center>

<script type="text/javascript">
    $(function () {
        var prepareLink = olimpContext.olimpLink + 'Prepare/StHome/BeginScormIntegration' +
            '?referer=' + olimpContext.olimpLink + 'LmsIntegration/CloseWindow' +
            '&EmployeeId=' + olimpContext.employeeId +
            '&ProfileId=' + olimpContext.profileId;

        $('#prepare-link').attr('href', prepareLink);

        $('#exam-link').click(function () {
            callJsonPService('Exam/Home/BeginScormIntegration', getOlimpContext(), setDynamicContent);

            return false;
        });
    });
</script>
