﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<LmsIntegrationViewModel<LmsIntegrationAppearanceViewModel>>" %>

<style type="text/css">
    <%= Model.RealModel.LmsCss%>
</style>

<link rel="shortcut icon" href="<%= Url.VersionSuffixedContent("~/Content/Images/favicon.ico", Model.OlimpPath) %>" type="image/x-icon" />

<% Platform.RenderExtensions("/olimp/exam/lms/css", new Dictionary<string, object> { { "Prefix", Model.OlimpPath } }); %>