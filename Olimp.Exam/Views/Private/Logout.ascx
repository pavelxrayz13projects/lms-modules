﻿<%@ Control Language="C#" Inherits="IntegratableUserControl<object>" %>
<%@ Import Namespace="I18N=Olimp.Exam.I18N" %>

<% if (IntegrationContext.IntegrationState == IntegrationContextState.InIntegrationContext) { %>
<% Html.BeginForm("ReturnToRefereingSystem", "Home", new { area = "Exam" }, FormMethod.Post, new { id = "logout-form" }).Dispose(); %>
<% } else { %>
<% Html.BeginForm("Logout", "Home", new { area="Exam" }, FormMethod.Post, new { id="logout-form"}).Dispose(); %>
<% } %>

<script type="text/javascript">
    function logout() {
        $('#logout-form')[0].submit();
    }
</script>
	
<%-- onclick needed for IE to handle event --%>
<a class="status-link" href="javascript:void(0)" onclick="logout();">
    <table>
        <tr>
            <td><div class="icon icon-finish-flag"></div></td>
            <td class="text"><%= I18N.Exam.FinishExam %></td>
        </tr>
    </table>
</a>