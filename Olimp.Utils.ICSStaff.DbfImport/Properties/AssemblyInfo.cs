using System.Reflection;
using System.Runtime.CompilerServices;
using PAPI.Core;

[assembly: AssemblyTitle("Olimp.Utils.ICSStaff.DbfImport")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("1.0.0.0")]

[assembly:EncryptedAssemblyReference("Olimp.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=bd83c7d5d855788a")]
[assembly:EncryptedAssemblyReference("Olimp.Domain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=bd83c7d5d855788a")]
[assembly:EncryptedAssemblyReference("Olimp.Courses, Version=1.0.0.0, Culture=neutral, PublicKeyToken=bd83c7d5d855788a")]

[assembly:SingleInstance]