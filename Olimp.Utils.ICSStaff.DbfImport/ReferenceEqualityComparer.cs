using System;
using System.Collections.Generic;
using Olimp.Utils.ICSStaff.DbfImport.Domain;
using Olimp.Utils.ICSStaff.DbfImport.Dbf;

namespace Olimp.Utils.ICSStaff.DbfImport
{
	public class ReferenceEqualityComparer<T> : IEqualityComparer<T>
		where T: IReferenceEntity
	{
		public bool Equals (T d1, T d2)
		{
			if (d1 == null ^ d2 == null)
				return false;
			
			if (d1 == null && d2 == null)
				return true;
			
			return d1.Id == d2.Id || d1.ToString() == d2.ToString();
		}
		
		public int GetHashCode(T d)
		{
			return d.Id.GetHashCode() ^ d.ToString().GetHashCode();
		}
	}
}

