using System;
using Olimp.Configuration;
using PAPI.Core.Data.NHibernate;

namespace Olimp.Utils.ICSStaff.DbfImport
{
	public class SettingsRepo : NHibernateRepository<Setting, string>
	{
		public SettingsRepo (ISessionManager sessionManager)
			: base (sessionManager)
		{ }
		
		public virtual void Save (Setting entity, bool forceCreate)
		{
        	if (!string.IsNullOrEmpty(entity.Id) && !forceCreate)
            	Session.Update(entity);
			else 
            	Session.Save(entity);
		}
	}
}

