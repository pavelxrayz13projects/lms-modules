using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using NHibernate.Cfg;
using Olimp.Configuration;
using Olimp.Core;
using Olimp.Core.Security;
using Olimp.Domain.LearningCenter;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Utils.ICSStaff.DbfImport.Dbf;
using Olimp.Utils.ICSStaff.DbfImport.Domain;
using PAPI.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using Settings = Olimp.Utils.ICSStaff.DbfImport.Properties.Settings;

namespace Olimp.Utils.ICSStaff.DbfImport
{
    public class Runner : IModuleRunner
    {
        private const string FirstLoadSettingKey = "ics-staff/dbf-import/is-first";

        private static string NodeLogsPath
        {
            get { return OlimpPaths.OfExecutingModule().PlatformPaths.Logs; }
        }

        private static void DeleteOldLogFiles(string path, string logFilePrefix, int skip)
        {
            var logsFilesToDelete =
                Directory.GetFiles(path, logFilePrefix + ".log*", SearchOption.TopDirectoryOnly)
                         .OrderByDescending(f => File.GetLastWriteTime(f))
                         .Skip(skip);

            foreach (var lf in logsFilesToDelete)
            {
                var errLf = Path.Combine(path, logFilePrefix + ".err" + Path.GetExtension(lf));
                if (File.Exists(errLf))
                    File.Delete(errLf);

                var sysLf = Path.Combine(path, logFilePrefix + ".sys" + Path.GetExtension(lf));
                if (File.Exists(sysLf))
                    File.Delete(sysLf);

                if (File.Exists(lf))
                    File.Delete(lf);
            }
        }

        public void Run(IModule module, IDictionary<string, string> moduleParams)
        {
            string deptsPath, jobsPath, staffPath, nodeLogsPath = NodeLogsPath;

            var errLogFileName = Path.Combine(nodeLogsPath, Settings.Default.LogFilePrefix + ".err");
            var sysLogFileName = Path.Combine(nodeLogsPath, Settings.Default.LogFilePrefix + ".sys");

            var log = PAPI.Core.Logging.Log.CreateLogger(Settings.Default.LogFilePrefix, Level.Info.ToString());
            var errLog = PAPI.Core.Logging.Log.CreateLogger(errLogFileName, Level.Info.ToString());
            var sysLog = PAPI.Core.Logging.Log.CreateLogger(errLogFileName, Level.Info.ToString());

            try
            {
                DeleteOldLogFiles(nodeLogsPath, Settings.Default.LogFilePrefix, Settings.Default.DeleteOldLogFilesSkipCount);
            }
            catch (Exception e)
            {
                errLog.ErrorFormat("Delete old log files failed: ", e);
            }

            string stopString;
            var stopService = moduleParams.TryGetValue("stop", out stopString) &&
                stopString.Equals("stop", StringComparison.InvariantCultureIgnoreCase);

            if (!moduleParams.TryGetValue("depts", out deptsPath) || !File.Exists(deptsPath))
            {
                errLog.ErrorFormat(I18N.Messages.CA01NotFound, deptsPath);
                return;
            }

            if (!moduleParams.TryGetValue("jobs", out jobsPath) || !File.Exists(jobsPath))
            {
                errLog.ErrorFormat(I18N.Messages.CA06NotFound, jobsPath);
                return;
            }

            if (!moduleParams.TryGetValue("staff", out staffPath) || !File.Exists(staffPath))
            {
                errLog.ErrorFormat(I18N.Messages.CA07NotFound, staffPath);
                return;
            }

            Thread.CurrentThread.CurrentCulture =
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("ru-RU");

            log.InfoFormat(I18N.Messages.StartMessage, deptsPath, jobsPath, staffPath, DateTime.Now);

            int addedWorkers = 0, updatedWorkers = 0, deletedWorkers = 0, restoredWorkers = 0, errorsCount = 0;
            try
            {
                if (stopService)
                {
                    Process.Start(new ProcessStartInfo("net.exe", "stop OlimpService") { UseShellExecute = false })
                        .WaitForExit();
                }

                var dbContext = DbContext.OfNode(ModuleFactory.GetNodeDataOrDefault(moduleParams));
                var today = DateTime.Today;

                using (var db = new DbfDatabase())
                {
                    db.AddReference<Department>(deptsPath, d => d.EndDate.Year == 9999);
                    db.AddReference<Profession>(jobsPath, p => p.EndDate.Year == 9999);
                    db.AddTable<Worker>(staffPath);

                    var appointments = new Dictionary<Profession, Appointment>(new ReferenceEqualityComparer<Profession>());
                    var companies = new Dictionary<Department, Company>(new ReferenceEqualityComparer<Department>());

                    var employeeRepo = dbContext.Repository.CreateFor<Employee, Guid>();
                    var appointmentRepo = dbContext.Repository.CreateFor<Appointment, int>();
                    var companyRepo = dbContext.Repository.CreateFor<Company, int>();
                    var groupRepo = dbContext.Repository.CreateFor<Group, int>();

                    var settingsRepo = new SettingsRepo(dbContext.SessionManager);

                    var passwordGenerator = new CommonPasswordGenerator();

                    using (var uow = dbContext.UnitOfWork.CreateWrite(IsolationLevel.Serializable))
                    {
                        var setting = settingsRepo.GetById(FirstLoadSettingKey);

                        var groupName = I18N.Names.GroupName;
                        var group = groupRepo.FirstOrDefault(g => g.Name == groupName);
                        if (group == null)
                        {
                            var config = Config.Get<OlimpConfiguration>(dbContext.SessionManager);
                            var defaultGroup = groupRepo.GetById(config.DefaultGroup);

                            log.InfoFormat(I18N.Messages.SelectedProfile, String.Join(", ", defaultGroup.Profiles));

                            group = new Group
                            {
                                Name = groupName,
                                Prefix = I18N.Names.GroupPrefix,
                                Description = I18N.Names.GroupDescription,
                                ExamBeginDate = today,
                                ExamSettings = defaultGroup.ExamSettings,
                                ExamEndDate = today.AddYears(2)
                            };

                            foreach (var profile in defaultGroup.Profiles)
                                group.AddProfile(profile);

                            groupRepo.Save(group);

                            log.InfoFormat(I18N.Messages.GroupAdded, group.Name, group.Prefix, group.Description, String.Join(", ", group.Profiles));
                        }
                        else if (!group.IsActive)
                        {
                            group.IsActive = true;
                            groupRepo.Save(group);

                            log.InfoFormat(I18N.Messages.GroupRestored, group.Name, group.Prefix, group.Description, String.Join(", ", group.Profiles));
                        }

                        var workers = db.GetTable<Worker>()
                            .Where(w => w.ProfessionId != 0 && w.ProfessionId != 99999999)
                            .GroupBy(w => w.Number)
                            .Select(g => g.OrderByDescending(w => w.DateFired).First());

                        foreach (var w in workers)
                        {
                            var addedWorkersOld = addedWorkers;
                            string message = null;
                            try
                            {
                                var number = w.Number.ToString();
                                var employee = employeeRepo.FirstOrDefault(e => e.Number == number);
                                if (employee == null && w.IsFired)
                                    continue;

                                Appointment appointment = null;
                                Company company = null;
                                if (!w.IsFired)
                                {
                                    if (!appointments.TryGetValue(w.Profession, out appointment))
                                    {
                                        var name = Convert.ToString(w.Profession);
                                        appointment = appointmentRepo.FirstOrDefault(a => a.Name == name);

                                        if (appointment == null)
                                        {
                                            appointment = new Appointment { Name = name };
                                            appointmentRepo.Save(appointment);
                                        }

                                        appointment.IsActive = true;
                                        appointments.Add(w.Profession, appointment);
                                    }
                                    if (!companies.TryGetValue(w.Department, out company))
                                    {
                                        var name = Convert.ToString(w.Department);
                                        company = companyRepo.FirstOrDefault(c => c.Name == name);

                                        if (company == null)
                                        {
                                            company = new Company { Name = name };
                                            companyRepo.Save(company);
                                        }

                                        company.IsActive = true;
                                        companies.Add(w.Department, company);
                                    }
                                }

                                var isNew = employee == null;
                                if (isNew)
                                {
                                    employee = new Employee {Id = Guid.NewGuid(), Number = number};
                                    employee.SetGroup(group);
                                    employee.Password = passwordGenerator.Generate(8);

                                    if (setting != null && setting.Value == "true")
                                    {
                                        message = String.Format(I18N.Messages.WorkerAdded,
                                            number,
                                            String.Join(" ", w.Surname, w.Name, w.Midname),
                                            company,
                                            appointment,
                                            groupName);
                                    }

                                    addedWorkers++;
                                }
                                else if (!w.IsFired &&
                                         (!String.Equals(employee.Name, w.Name) || !String.Equals(employee.Surname, w.Surname) || !String.Equals(employee.GivenName, w.Midname) ||
                                            !Object.Equals(employee.Company, company) || !employee.Appointments.Contains(appointment, Appointment.EqualityComparer)))
                                {
                                    message = String.Format(I18N.Messages.WorkerUpdated,
                                        employee.Number, number,
                                        employee, String.Join(" ", w.Surname, w.Name, w.Midname),
                                        employee.Company, company,
                                        employee.AppointmentsToString(), appointment);

                                    updatedWorkers++;
                                }
                                else if (employee.IsActive && w.IsFired)
                                {
                                    message = String.Format(I18N.Messages.WorkerDeleted, employee.Number);

                                    deletedWorkers++;
                                }
                                else if (!employee.IsActive && !w.IsFired)
                                {
                                    message = String.Join("\n", I18N.Messages.WorkerRestored, message);

                                    restoredWorkers++;
                                }

                                employee.Name = w.Name;
                                employee.Surname = w.Surname;
                                employee.GivenName = w.Midname;
                                employee.Company = company;

                                if (!employee.Appointments.Contains(appointment, Appointment.EqualityComparer))
                                    employee.AddAppointment(appointment);

                                employee.IsActive = !w.IsFired;

                                if (isNew)
                                    employee.SetSurnamePrefixLogin(employeeRepo);

                                employeeRepo.Save(employee, isNew);
                            }
                            catch (Exception ee)
                            {
                                if (ee is DbfException)
                                    errLog.ErrorFormat("{0} (#{1} {2})", ee.Message, w.Number, w);
                                else
                                    errLog.ErrorFormat(I18N.Messages.WorkerException, ee, w.Number, w);

                                errorsCount++;

                                addedWorkers = addedWorkersOld;

                                continue;
                            }

                            try
                            {
                                if (message != null)
                                    log.Info(message);
                            }
                            catch { }
                        }

                        if (setting == null)
                        {
                            setting = new Setting { Id = FirstLoadSettingKey, Type = typeof(bool).FullName, Value = "true" };
                            settingsRepo.Save(setting, true);
                        }

                        uow.Commit();
                    }
                }
            }
            catch (DbfException ee)
            {
                errorsCount++;
                errLog.Error(ee.Message);
                sysLog.Error(ee);
            }
            catch (Exception ee)
            {
                errorsCount++;
                errLog.ErrorFormat(I18N.Messages.Exception, ee);
                sysLog.Error(ee);
            }
            finally
            {
                log.InfoFormat(I18N.Messages.Success, addedWorkers, restoredWorkers, updatedWorkers, deletedWorkers, errorsCount);

                if (stopService)
                {
                    Process.Start(new ProcessStartInfo("net.exe", "start OlimpService") { UseShellExecute = false })
                        .WaitForExit();
                }

                foreach (var a in errLog.Logger.Repository.GetAppenders())
                    a.Close();

                if (File.Exists(errLogFileName) && errorsCount == 0 && new FileInfo(errLogFileName).Length == 0)
                    File.Delete(errLogFileName);

                if (File.Exists(sysLogFileName) && new FileInfo(sysLogFileName).Length == 0)
                    File.Delete(sysLogFileName);
            }
        }
    }
}