using Olimp.Configuration;
using PAPI.Core;
using PAPI.Core.Data;
using PAPI.Core.Data.NHibernate;

namespace Olimp.Utils.ICSStaff.DbfImport
{
    public class DbContext
    {
        public ISessionManager SessionManager { get; private set; }

        private DbContext(NodeData node)
        {
            Module = PModule.Load("Olimp", node, ModuleLoadFlags.LoadForDatabaseInitialization);

            SessionManager = Module.Database.GetSessionManager();

            UnitOfWork = Module.Database.UnitOfWorkFactory;
            Repository = Module.Database.RepositoryFactory;
        }

        public IModule Module { get; private set; }

        public IUnitOfWorkFactory UnitOfWork { get; private set; }

        public IRepositoryFactory Repository { get; private set; }

        public T GetConfig<T>()
            where T : class
        {
            return Config.Get<T>(SessionManager);
        }

        public void SetConfig<T>(T settings)
            where T : class
        {
            Config.Set<T>(settings, SessionManager);
        }

        public static DbContext OfNode(NodeData node)
        {
            return new DbContext(node);
        }
    }
}