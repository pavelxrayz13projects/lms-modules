﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Olimp.Utils.ICSStaff.DbfImport.I18N {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Names {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Names() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Olimp.Utils.ICSStaff.DbfImport.I18N.Names", typeof(Names).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Подразделение.
        /// </summary>
        public static string DepartmentEntityName {
            get {
                return ResourceManager.GetString("DepartmentEntityName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Работники, импортированные из ИСУ &quot;Персонал&quot;.
        /// </summary>
        public static string GroupDescription {
            get {
                return ResourceManager.GetString("GroupDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ИСУ &quot;Персонал&quot;.
        /// </summary>
        public static string GroupName {
            get {
                return ResourceManager.GetString("GroupName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ИСУП.
        /// </summary>
        public static string GroupPrefix {
            get {
                return ResourceManager.GetString("GroupPrefix", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Профессия.
        /// </summary>
        public static string ProfessionEntityName {
            get {
                return ResourceManager.GetString("ProfessionEntityName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Работник.
        /// </summary>
        public static string WorkerEntityName {
            get {
                return ResourceManager.GetString("WorkerEntityName", resourceCulture);
            }
        }
    }
}
