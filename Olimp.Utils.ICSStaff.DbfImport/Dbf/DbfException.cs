using System;
using System.Linq;

namespace Olimp.Utils.ICSStaff.DbfImport.Dbf
{
	public class DbfException : Exception
	{
		public DbfException (string str, params string[] args)
			: base (string.Format(str, args))
		{ }
		
		public DbfException (string str, Type type, object id)
			: this (str, ((DbfEntityNameAttribute) type.GetCustomAttributes(typeof(DbfEntityNameAttribute), true).First()).Name, Convert.ToString(id))
		{ }
	}
}

