﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Olimp.Utils.ICSStaff.DbfImport.Dbf
{
    public class DbfField
    {
        private static CultureInfo _defaultFormat = new CultureInfo("en-US", false);

        public DbfField (string fieldName, Type fieldType, int size)
        {
            this.FieldName = fieldName;
            this.FieldType = fieldType;
            this.FieldSize = size;
        }

        public Type FieldType { get; private set; }

        public string FieldName { get; private set; }

        public int FieldSize { get; private set; }

        public object GetValue(string rawValue)
        {
            if (rawValue.Equals(String.Empty))
                return DBNull.Value;
            else if (this.FieldType == typeof(DateTime))
                return DateTime.ParseExact(rawValue, "yyyyMMdd", _defaultFormat.DateTimeFormat);
            else if (this.FieldType == typeof(Double) || this.FieldType == typeof(Decimal) || this.FieldType == typeof(Int32))
                return Convert.ChangeType(rawValue, this.FieldType, _defaultFormat.NumberFormat);
            else if (this.FieldType == typeof(Boolean))
                return rawValue.Equals("T");
            else
                return rawValue;
        }
    }
}
