﻿using System;

namespace Olimp.Utils.ICSStaff.DbfImport.Dbf
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class DbfMappedFieldAttribute : Attribute
    {
        public DbfMappedFieldAttribute(string name)
        {
            this.Name = name;
        }

        public string Name { get; private set; }

        public bool IsPrimaryKey { get; set; }
    }
}

