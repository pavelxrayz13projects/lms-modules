﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

namespace Olimp.Utils.ICSStaff.DbfImport.Dbf
{
    public class DbfRow : IEnumerable<object>
    {
        private IDictionary<string, object> _dictionary;

        public DbfRow(IDictionary<string, object> dictionary)
        {
            _dictionary = dictionary;
        }

        public object this[string key] { get { return _dictionary[key]; } }

        public IEnumerator<object> GetEnumerator()
        {
            return _dictionary.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
