using System;
using System.Reflection;

namespace  Olimp.Utils.ICSStaff.DbfImport.Dbf
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public class DbfEntityNameAttribute : Attribute
	{
		public DbfEntityNameAttribute (string name)
		{
			this.Name = name;
		}
		
		public DbfEntityNameAttribute (Type type, string propertyName)
		{
			this.Name = (string) type.GetProperty(propertyName, BindingFlags.Public | BindingFlags.Static)
				.GetValue(null, null);
		}
		
		public string Name { get; private set; }
	}
}

