﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Olimp.Utils.ICSStaff.DbfImport.Dbf
{
    public interface IReferenceEntity
    {
        int Id { get; }

        DateTime StartDate { get; }

        DateTime EndDate { get; }
    }
}
