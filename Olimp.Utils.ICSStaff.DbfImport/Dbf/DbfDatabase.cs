﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Linq.Expressions;

namespace Olimp.Utils.ICSStaff.DbfImport.Dbf
{
    public class DbfDatabase : IDisposable
    {
        private static BindingFlags _defaultBindingFlags = 
            BindingFlags.GetProperty | 
            BindingFlags.GetField | 
            BindingFlags.Instance | 
            BindingFlags.FlattenHierarchy | 
            BindingFlags.Public | 
            BindingFlags.NonPublic;

        private static MethodInfo _getLazyValue;

        static DbfDatabase()
        {
            _getLazyValue = typeof(DbfDatabase).GetMethod("GetLazyArgumentValue", BindingFlags.Instance | BindingFlags.NonPublic);
        }

        private IDictionary<Type, IDictionary<int, object>> _references;
        private IDictionary<Type, DbfReader> _tables;

        public DbfDatabase()
        {
            _references = new Dictionary<Type, IDictionary<int, object>>();
            _tables = new Dictionary<Type, DbfReader>();
        }

        private TReference GetLazyArgumentValue<TReference>(int id)
        {
            IDictionary<int, object> reference;
            if (!_references.TryGetValue(typeof(TReference), out reference))
                throw new InvalidOperationException(String.Format("Missing reference [{0}]", typeof(TReference).Name));

            object value;
            if (!reference.TryGetValue(id, out value))
				throw new DbfException(I18N.Messages.MissingEntityException, typeof(TReference), id);

            if (value == null)
				throw new DbfException(I18N.Messages.InvalidEntityValueException, typeof(TReference), id);

            return (TReference)value;
        }

         private object MakeLazyValue(Type type, int id)         
         {             
            if (!typeof(Lazy<>).IsAssignableFrom(type.GetGenericTypeDefinition()))
                throw new ArgumentException("type must be Lazy<T>");

            var genericArg = type.GetGenericArguments().First();
            
            var expression = Expression.Lambda(
                 typeof(Func<>).MakeGenericType(genericArg),
                 Expression.Call(Expression.Constant(this), _getLazyValue.MakeGenericMethod(genericArg), Expression.Constant(id))); 
             
             return Activator.CreateInstance(type, expression.Compile());
         }

         private TEntity CreateEntity<TEntity>(DbfRow row, IDictionary<MemberInfo, string> members)
        {
            var entity = Activator.CreateInstance<TEntity>();

            foreach (var m in members)
            {
                var value = row[m.Value];

                var propertyInfo = m.Key as PropertyInfo;               
                if (propertyInfo != null)
                {
					try
					{
                    	propertyInfo.SetValue(
                        	entity, 
                        	propertyInfo.PropertyType.IsGenericType
                            	? value = this.MakeLazyValue(propertyInfo.PropertyType, (int) value)
                            	: Convert.ChangeType(value, propertyInfo.PropertyType), 
                        	null);
					}
					catch (Exception ee)
					{
						throw new DbfException(I18N.Messages.SystemError,
						        String.Format("{0}.{1}[{2}] => '{3}'[{4}]\n{5}", 
								typeof(TEntity).Name, 
						        propertyInfo.Name, 
						        propertyInfo.PropertyType,
						        value,
						        value == null ? "unknown" : value.GetType().FullName,
						        ee));
					}
                }

                var fieldInfo = m.Key as FieldInfo;
                if (fieldInfo != null)
                {
					try
					{
						fieldInfo.SetValue(entity, fieldInfo.FieldType.IsGenericType
                        	? this.MakeLazyValue(fieldInfo.FieldType, (int)value)
                        	: value);
					}
					catch (Exception ee)
					{
						throw new DbfException(I18N.Messages.SystemError,
						        String.Format("{0}.{1}[{2}] => '{3}'[{4}]\n{5}", 
								typeof(TEntity).Name, 
						        fieldInfo.Name, 
						        fieldInfo.FieldType,
						        value,
						        value == null ? "unknown" : value.GetType().FullName, 
						        ee));
					}
                }
            }

            return entity;
        }

        private IDictionary<MemberInfo, string> GetMapping<TEntity>()
        {
            return typeof(TEntity).GetMembers(_defaultBindingFlags)
                .Select(m => new
                {
                    Member = m,
                    Attr = m.GetCustomAttributes(typeof(DbfMappedFieldAttribute), true)
                        .Cast<DbfMappedFieldAttribute>()
                        .FirstOrDefault()
                })
                .Where(o => o.Attr != null)
                .ToDictionary(k => k.Member, v => v.Attr.Name);
        }

        private IEnumerable<TEntity> EnumerateEntities<TEntity>(DbfReader reader)
        {
            var members = this.GetMapping<TEntity>();
            var filter = members.Values.Distinct().ToArray();

            return reader.EnumerateRows(filter)
                .Select(r => this.CreateEntity<TEntity>(r, members));
        }

        public void AddReference<TEntity>(string fileName)
            where TEntity : IReferenceEntity
        {
            this.AddReference<TEntity>(fileName, null);
        }

        public object CheckIfSingle<TEntity>(int key, IEnumerable<TEntity> elements)
        {
            try
            {
                return elements.SingleOrDefault();
            }
            catch
            {
				throw new DbfException(I18N.Messages.MultipleEntitiesException, typeof(TEntity), key);
            }
        }

        public void AddReference<TEntity>(string fileName, Func<TEntity, bool> condition)
            where TEntity : IReferenceEntity
		{
            if (condition == null)
                condition = e => true;

			using (var reader = new DbfReader(fileName))
			{
                var reference = this.EnumerateEntities<TEntity>(reader)
                    .Where(condition)
                    .GroupBy(k => k.Id)
                    .ToDictionary(k => k.Key, v => this.CheckIfSingle(v.Key, v));

                _references.Add(typeof(TEntity), reference);
			}
		}

        public void AddTable<TEntity>(string fileName)
        {
            _tables.Add(typeof(TEntity), new DbfReader(fileName));
        }

        public IEnumerable<TEntity> GetTable<TEntity>()
        {
            DbfReader reader;
            if (!_tables.TryGetValue(typeof(TEntity), out reader))
				throw new InvalidOperationException(String.Format("Missing table {0}", typeof(TEntity)));

            return this.EnumerateEntities<TEntity>(reader);
        }

        public void Dispose()
        {
            if (_tables == null)
                return;

            foreach (var r in _tables.Values)
                r.Dispose();
        }
    }
}

