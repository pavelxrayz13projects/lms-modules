﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;

namespace Olimp.Utils.ICSStaff.DbfImport.Dbf
{
    public class DbfReader : IDisposable
    {
        private static Encoding _cp866 = Encoding.GetEncoding(866);
        static DbfReader() { }

        private Stream _fileStream;

        private Lazy<int> _recordsCount, _fieldsCount;
        private Lazy<DbfField[]> _fields;

        public DbfReader(string fileName)
        {
            _fileStream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);

            _recordsCount = new Lazy<int>(this.GetRecordsCount);
            _fieldsCount = new Lazy<int>(this.GetFieldsCount);
            _fields = new Lazy<DbfField[]>(this.GetFields);
        }

        private int GetRecordsCount()
        {
            var buffer = new byte[4];

            _fileStream.Seek(4, SeekOrigin.Begin);
            _fileStream.Read(buffer, 0, buffer.Length);

            return BitConverter.ToInt32(buffer, 0);
        }

        private int GetFieldsCount()
        {
            var buffer = new byte[2];

            _fileStream.Seek(8, SeekOrigin.Begin);
            _fileStream.Read(buffer, 0, buffer.Length);

            return (BitConverter.ToInt16(buffer, 0) / 32) - 1;
        }

        private static Type GetTypeBySign(char typeSign, byte digitsCount)
        {
            switch (typeSign)
            {
                case 'L': return typeof(Boolean);
                case 'D': return typeof(DateTime);
                case 'N': return digitsCount == 0 ? typeof(Int32) : typeof(Decimal);
                case 'F': return typeof(Double);
                default: return typeof(String);
            }
        }

        private DbfField[] GetFields()
        {
            var fields = new DbfField[_fieldsCount.Value];
            var buffer = new byte[32];

            _fileStream.Seek(32, SeekOrigin.Begin);
            for (int i = 0; i < fields.Length; i++)
            {
                _fileStream.Read(buffer, 0, buffer.Length);

                var name = Encoding.Default.GetString(buffer, 0, 10).TrimEnd((char) 0x00);
                var typeSign = (char) buffer[11];
                var size = buffer[16];
                var digitsCount = buffer[17];

                var type = GetTypeBySign(typeSign, digitsCount);

                fields[i] = new DbfField(name, type, size);
            }

            return fields;
        }

        public IEnumerable<DbfRow> EnumerateRows()
        {
            return this.EnumerateRows(null);
        }

        public IEnumerable<DbfRow> EnumerateRows(string[] fields)
        {
            HashSet<string> fieldNames = null;
            if (fields != null)
                fieldNames = new HashSet<string>(fields);

            for (int i = 0; i < _recordsCount.Value; i++)
            {
                if (i == 0)
                    _fileStream.Seek(32 + (32 * _fields.Value.Length) + 1, SeekOrigin.Begin);

                _fileStream.ReadByte();

                var dictionary = new Dictionary<string, object>(fieldNames != null ? fieldNames.Count : _fieldsCount.Value);

                for (int j = 0; j < _fieldsCount.Value; j++)
                {
                    var field = _fields.Value[j];
                    if (fieldNames != null && !fieldNames.Contains(field.FieldName))
                    {
                        _fileStream.Seek(field.FieldSize, SeekOrigin.Current);
                        continue;
                    }

                    var buffer = new byte[field.FieldSize];

                    _fileStream.Read(buffer, 0, buffer.Length);

                    var stringValue = _cp866.GetString(buffer, 0, buffer.Length).TrimEnd((char)0x00, (char) 0x20);
                    dictionary.Add(field.FieldName, field.GetValue(stringValue));                                                              
                }

                yield return new DbfRow(dictionary);
            }
        }

        public void Dispose()
        {
            if (_fileStream != null)
                _fileStream.Dispose();
        }
    }
}
