﻿using System;
using System.Linq;
using Olimp.Utils.ICSStaff.DbfImport.Dbf;
using System.Text;
using System.Collections.Generic;

namespace Olimp.Utils.ICSStaff.DbfImport.Domain
{
	[DbfEntityName(typeof(I18N.Names), "DepartmentEntityName")]
    public class Department : IReferenceEntity
    {
		private Lazy<string> _stringName;
		
		public Department()
		{
			_stringName = new Lazy<string>(this.ToStringInternal);
		}
		
        [DbfMappedField("SOBID")]
        private Lazy<Department> _parent;
		
		[DbfMappedField("SOBID")]
        private int _parentId;

        [DbfMappedField("OBJID", IsPrimaryKey = true)]
        public int Id { get; private set; }

        [DbfMappedField("TLINE")]
        public string FullName { get; private set; }
        
        [DbfMappedField("BEGDA")]
        public DateTime StartDate { get; private set; }

        [DbfMappedField("ENDDA")]
        public DateTime EndDate { get; private set; }

        public Department Parent { get { return _parent.Value; } }

		public virtual string ToStringInternal()
        {
            var depts = new List<string>(2);

            Department dept = null;
            for (int i = 0; i < depts.Capacity; i++)
            {
                dept = dept == null ? this : dept.Parent;
                if (dept == null)
                    break;

                depts.Add(dept.FullName);
				
				if (dept._parentId == 60010296)
					break;
            }

            return String.Join(" | ", depts.Reverse<string>());
        }
		
        public override string ToString()
        {
			return _stringName.Value;
        }
    }
}

