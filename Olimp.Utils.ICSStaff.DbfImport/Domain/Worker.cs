﻿using System;
using Olimp.Utils.ICSStaff.DbfImport.Dbf;

namespace Olimp.Utils.ICSStaff.DbfImport.Domain
{
	[DbfEntityName(typeof(I18N.Names), "WorkerEntityName")]
    public class Worker
    {       
        [DbfMappedField("SIND")]
        private Lazy<Profession> _profession;

        [DbfMappedField("TABN", IsPrimaryKey = true)]
        public int Number { get; private set; }

        [DbfMappedField("SURNAME")]
        public string Surname { get; private set; }

        [DbfMappedField("NAME")]
        public string Name { get; private set; }

        [DbfMappedField("MIDNAME")]
        public string Midname { get; private set; }
		
		[DbfMappedField("ENDDA")]
		public DateTime DateFired { get; private set; }

        [DbfMappedField("SIND")]
        public int ProfessionId { get; private set; }

        public bool IsFired { get { return this.DateFired.Year != 9999; } }

        public Profession Profession { get { return _profession.Value; } }

        public Department Department 
        { 
            get { return this.Profession != null ? this.Profession.Department : null;  } 
        }

        public override string ToString()
        {
            return String.Join(" ", this.Surname, this.Name, this.Midname);
        }
    }
}

