﻿using System;
using Olimp.Utils.ICSStaff.DbfImport.Dbf;

namespace Olimp.Utils.ICSStaff.DbfImport.Domain
{
	[DbfEntityName(typeof(I18N.Names), "ProfessionEntityName")]
    public class Profession : IReferenceEntity
    {
        [DbfMappedField("SOBID1")]
        private Lazy<Department> _department;

        [DbfMappedField("OBJID", IsPrimaryKey = true)]
        public int Id { get; private set; }

        [DbfMappedField("TLINE")]
        public string FullName { get; private set; }

        [DbfMappedField("BEGDA")]
        public DateTime StartDate { get; private set; }

        [DbfMappedField("ENDDA")]
        public DateTime EndDate { get; private set; }

        public Department Department { get { return _department.Value; } }

        public override string ToString()
        {
            return this.FullName;
        }
    }
}

