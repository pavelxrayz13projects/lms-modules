﻿$.Olimp = $.Olimp || {};
$.Olimp.CoursesCreator = $.Olimp.CoursesCreator || {};
$.Olimp.CoursesCreator.Index = {
    init: function (getTopicsUrl, saveBtnText, cancelBtnText) {
        this.getTopicsUrl = getTopicsUrl;
        this.saveBtnText = saveBtnText;
        this.cancelBtnText = cancelBtnText;
    },

    openGenerator: function () { 
		var self = this;

        $.post($.Olimp.CoursesCreator.Index.getTopicsUrl, { BranchId: this.Id() }, function(json) {
            $('#topics-distribution').data('viewModel').setRows(json.TopicsDistribution);

            json.TopicsDistribution = null;

            var dialog = $('#ticket-options')[0];
            ko.cleanNode(dialog);
            ko.applyBindings(json, dialog);

            $('#generate-dialog .title')
                .text(self.Code() + ' ' + self.Name());

            $('#generate-dialog').data('material', self).olimpdialog('open');
        }, 'json');
    },

    check100Percent: function () {
        var sum = 0,
            textBoxes = $('#generate-dialog .percentage').each(function() { sum += Number($(this).val()); });
		
		$('#percentage-validation').text(sum != 100 
			? 'Сумма процентных соотношений должна быть равна ста процентам'
			: '');
		
		textBoxes.unbind('.generateDialog').bind('change.generateDialog', function () { $.Olimp.CoursesCreator.Index.check100Percent(); });
					
        return sum == 100;
    }
};

$(function () {
    var buttons = {};
    buttons[$.Olimp.CoursesCreator.Index.saveBtnText] = function () {
        var self = $(this),
            form = self.find('form:first'),
            is100Percent = $.Olimp.CoursesCreator.Index.check100Percent(),
            isValid = form.valid();

        if (!is100Percent || !isValid)
            return;
        
        self.data('in-progress', true);
        self.parent().find('.olimp-dialog-buttonpane button').olimpbutton('option', 'disabled', true);

        $.post(form.attr('action'), form.serialize(), function(json) {
            var material = self.data('material');

            ko.mapping.fromJS(json, {}, material);
            
            self.data('in-progress', false);
            self.parent().find('.olimp-dialog-buttonpane button').olimpbutton('option', 'disabled', false);

            self.olimpdialog('close');
        }, 'json')
            .fail(function () {
                self.data('in-progress', false);
                self.parent().find('.olimp-dialog-buttonpane button').olimpbutton('option', 'disabled', false);
        });
    };

    buttons[$.Olimp.CoursesCreator.Index.cancelBtnText] = function() { $(this).olimpdialog('close'); };

    $('#generate-dialog').olimpdialog({
        autoOpen: false,
        draggable: false,
        resizable: false,
        modal: true,
        width: '70%',
        title: 'Генерация билетов',
        beforeClose: function () {
            return $(this).data('in-progress') != true;
        },
        buttons: buttons
    });
})