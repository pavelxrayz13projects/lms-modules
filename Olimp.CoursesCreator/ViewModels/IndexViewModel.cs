﻿namespace Olimp.CoursesCreator.ViewModels
{
    public class IndexViewModel
    {
        public bool AllowGenerationTableAction { get; set; }

        public bool AllowShareTableAction { get; set; }

        public bool AllowCreateCourses { get; set; }
    }
}