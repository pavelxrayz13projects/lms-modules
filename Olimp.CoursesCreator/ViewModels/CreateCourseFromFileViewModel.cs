using Olimp.Core.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Olimp.CoursesCreator.ViewModels
{
    public class CreateCourseFromFileViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.CoursesCreator.I18N.Courses), DisplayNameResourceName = "FromFile")]
        public HttpPostedFileBase CourseXml { get; set; }
    }
}