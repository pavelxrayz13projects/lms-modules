﻿﻿using System;

namespace Olimp.CoursesCreator.ViewModels
{
    public class TopicPercentStructure
    {
        public string TopicName { get; set; }

        public int Percent { get; set; }

        public Guid Id { get; set; }
    }
}