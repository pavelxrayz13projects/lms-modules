using Olimp.Core.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Olimp.CoursesCreator.ViewModels
{
    using Olimp.Domain.Catalogue.Entities;
    using I18N = Olimp.I18N.Domain;

    public class CreateTicketsViewModel : ITicketOptions
    {
        public string BranchName { get; set; }

        public int BranchId { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.TestTemplate), DisplayNameResourceName = "TicketsAmount")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int TicketsAmount { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.TestTemplate), DisplayNameResourceName = "TicketSize")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int QuestionsInTicket { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.TestTemplate), DisplayNameResourceName = "MaxErrors")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int AllowedMistakesCount { get; set; }

        public IList<TopicPercentStructure> Topics { get; set; }

        public CreateTicketsViewModel()
        {
            Topics = new List<TopicPercentStructure>();
        }
    }
}