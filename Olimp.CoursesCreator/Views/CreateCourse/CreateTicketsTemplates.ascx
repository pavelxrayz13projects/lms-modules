﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script id="percent-template" type="text/html">
	<input type="hidden" data-bind="
    value: Id,
    attr: { name: 'Topics[' + $data.Index() + '].Id' }" />	
	
	<input type="text" class="percentage" data-bind="
    value: Percent,
    attr: { name: 'Topics[' + $data.Index() + '].Percent' }" />	
</script>