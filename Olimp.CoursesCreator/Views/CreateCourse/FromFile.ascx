﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<CreateCourseFromFileViewModel>" %>

<% Html.EnableClientValidation(); %>

<% using(Html.BeginForm("FromFile", "CreateCourse", FormMethod.Post, new { 
	id= "create-material-from", target = "create-material-fake-frame", enctype = "multipart/form-data"  })) { %>
		

	<table class="form-table">
		<tr class="first last">
			<th><%= Html.LabelFor(m => m.CourseXml) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.CourseXml) %></div>
				<div><%= Html.TextBoxFor(m => m.CourseXml, new { type="file" }) %></div>
			</td>
		</tr>
	</table>

<% } %>

<iframe id="create-material-fake-frame" name="create-material-fake-frame" style="display: none"></iframe>  