﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<IndexViewModel>"  MasterPageFile="~/Views/Shared/Courses.master" %>

<asp:Content ContentPlaceHolderID="Olimp_Courses_Js" runat="server">
    <% Platform.RenderExtensions("/olimp/courses-creator/index/layout/js"); %>
    <script>
        $.Olimp.CoursesCreator.Index.init(
            '<%= Url.Action("GetTopics") %>', '<%= Olimp.I18N.PlatformShared.Save %>', '<%= Olimp.I18N.PlatformShared.Cancel %>');
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Courses_Content" runat="server">
    <% if (Model.AllowCreateCourses) { %>	
	    <div id="upload-dialog">
		    <% Html.RenderPartial("FromFile", new CreateCourseFromFileViewModel()); %>	
	    </div>
	<% } %>
    		
	<script type="text/html" id="download-action">
		<a class="icon icon-download action -has-dot-binding" title="Выгрузить курс" 
            data-dot-bind="attr: { href: '<%= Url.Action("Download") %>/' + $data.Id() }"
            data-dot-context="{{= it.$$contextId }}">
		</a>
	</script>
		
    <% if (Model.AllowGenerationTableAction) { %>	
	<script type="text/html" id="generate-action">
		<a href="#" class="icon icon-gear action -has-dot-binding" title="Параметры генерации билетов" 
            data-dot-bind="click: $.Olimp.CoursesCreator.Index.openGenerator"
            data-dot-context="{{= it.$$contextId }}">
		</a>
	</script>    
    <% } %>

    <% if (Model.AllowShareTableAction) { %>
        <% Html.RenderPartial("SwitchShareUserMaterialBranchActionTemplate", new ShareUserBranchViewModel("CreateCourse"));  %>
    <% } %>

	<h1>Курсы</h1>	
	<div class="block">	
        <%
            var actions = new List<RowAction>();
            
            if (Model.AllowCreateCourses)
                actions.Add(new TemplateAction("download-action"));
            
            if (Model.AllowCreateCourses && Model.AllowGenerationTableAction)
                actions.Add(new TemplateAction("generate-action"));
                
            if (Model.AllowShareTableAction)
                actions.Add(new TemplateAction("share-action"));
            
            if (Model.AllowCreateCourses)    
                actions.Add(new DeleteAction(Url.Action("Delete"), "Удалить курс?"));
        %>

		<% Html.Table(
            new TableOptions
            {
                BodyTemplateEngine = TemplateEngines.DoT,
                SelectUrl = Url.Action("GetCourses"),
                Columns = new[] { 
					new ColumnOptions("Code", Olimp.I18N.Domain.Material.Code), 
					new ColumnOptions("Name", Olimp.I18N.Domain.Material.Name) },
                Actions = actions.ToArray(),
                TableActions = new TableAction[] { 
					new FileUploadAction("upload-dialog", "create-material-fake-frame", "Создать курс",
						new DialogOptions { Title = "Создать курс" })
					{
					    Disabled = !Model.AllowCreateCourses
					}
				}
            }, new { id = "courses-table" }); %>
	</div>
	<% if (Model.AllowCreateCourses) { %>
	    <h1>Загрузка программы установки шаблона курса в формате InfoPath</h1>
	    <div class="tip">Для установки шаблона курса на персональном компьютере требуются права локального администратора</div>
	    <div class="block">
		    <a href="<%=Url.Action("GetInfoPathMsi")%>">Загрузить программу установки</a>	
	    </div>

        <% Html.RenderPartial("CreateTicketsTemplates"); %>	
	    <div id="generate-dialog">
		    <% Html.RenderPartial("CreateTickets", new CreateTicketsViewModel()); %>	
	    </div>		
        <% Html.TableInitializationScript("topics-distribution"); %>	
    <% } %>
</asp:Content>