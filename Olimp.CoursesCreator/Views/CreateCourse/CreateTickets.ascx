﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<CreateTicketsViewModel>" %>

<% using(Html.BeginForm("CreateTickets", "CreateCourse", FormMethod.Post)){ %>
	    
	<h1 class="title"></h1>
	<div class="block">
		<table id="ticket-options" class="form-table">
			<tr class="first">						
				<th><%= Html.LabelFor(m => m.TicketsAmount, "Количество билетов") %></th>
				<td>
                    <%= Html.HiddenFor(m => m.BranchId, new Dictionary<string, object> {
                        { "data-bind", "value: Id" }
                    })%>

					<div class="validation-message">
					<%= Html.ValidationMessageFor(m => m.TicketsAmount) %>
					</div>
											
					<%= Html.TextBoxFor(m => m.TicketsAmount, new Dictionary<string, object> {
						{ "data-bind", "value: TicketAmount" }
					}) %>
				</td>
			</tr>		
			<tr>	
				<th><%= Html.LabelFor(m => m.QuestionsInTicket, "Количество вопросов в билете") %></th>
				<td>
					<div class="validation-message">
					<%= Html.ValidationMessageFor(m => m.QuestionsInTicket) %>
					</div>
			
					<%= Html.TextBoxFor(m => m.QuestionsInTicket, new Dictionary<string, object> {
						{ "data-bind", "value: TicketSize" }
					}) %>
				</td>
			</tr>
			<tr class="last">
				<th><%= Html.LabelFor(m => m.AllowedMistakesCount, "Максимальное количество ошибок") %></th>
				<td>
					<div class="validation-message">
					<%= Html.ValidationMessageFor(m => m.AllowedMistakesCount) %>
					</div>
			
					<%= Html.TextBoxFor(m => m.AllowedMistakesCount, new Dictionary<string, object> {
						{ "data-bind", "value: MaxErrors" }
					}) %>
				</td>
			</tr>			
		</table>	
	</div>

	<div id="percentage-validation" class="validation-message"></div>

	<% Html.Table(
		new TableOptions {		
            DeferredInitializationScriptRendering = true,	
			OmitTemplates = true,
			Sorting = false,
			Paging = false,
			Columns = new[] { 
				new ColumnOptions("Name", "Наименование темы"),
				new ColumnOptions("Percent", "Процент вопросов") { TemplateId = "percent-template" } }
		}, new { id = "topics-distribution" }); %>
	
<% } %>