using Olimp.Core;
using Olimp.Core.Extensibility;
using Olimp.Core.Routing;
using Olimp.Courses.Generation.Generators.Course;
using Olimp.Courses.Generation.Tickets;
using Olimp.Courses.Security;
using Olimp.CoursesCreator.Controllers;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Catalogue;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Catalogue.OrmLite.Security;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.IO;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.CoursesCreator.Plugin))]

namespace Olimp.CoursesCreator
{
    public class Plugin : PluginModule
    {
        private static Lazy<bool> _disableGenerationAction;

        public static bool DisableGenerationTableAction { get { return _disableGenerationAction.Value; } }

        private static Lazy<bool> _disableShareTableAction;

        public static bool DisableShareTableAction { get { return _disableShareTableAction.Value; } }

        static Plugin()
        {
            _disableGenerationAction = new Lazy<bool>(() => GetDisableActionValue("/olimp/courses-creator/layout/allow-generate-btn"));
            _disableShareTableAction = new Lazy<bool>(() => GetDisableActionValue("/olimp/courses-creator/layout/allow-share-btn"));
        }

        private static bool GetDisableActionValue(string extensionPath)
        {
            var olimpModule = PContext.ExecutingModule as OlimpModule;
            if (olimpModule == null)
                return true;

            var container = olimpModule.Plugins.GetExtender().GetContainer(extensionPath) as BoolExtensionContainer;
            if (container == null)
                return true;

            return !container.GetValue();
        }

        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        { }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(CreateCourseController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (this.Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var materialsModule = PContext.ExecutingModule as OlimpMaterialsModule;
            if (materialsModule == null)
                throw new InvalidOperationException("Executing module is not OlimpMaterialsModule");

            var distributionInfo = materialsModule.Context.DistributionInfoProvider.Get();
            if (distributionInfo == null)
                throw new FileNotFoundException("Distribution info file wasn't found");

            var nodeData = materialsModule.Context.NodeData;

            var cryptoService = new CryptoService(
                distributionInfo.GetDistributionIdBytes());

            var connFactoryFactory = new PerHttpRequestConnectionFactoryFactory(
                new SqliteConnectionFactoryFactory());

            var connFactory = connFactoryFactory.Create();

            var predefinedProvider = new OrmLitePredefinedCatalogueBranchesProvider(connFactory);

            var nodeRepository = new OrmLiteNodeRepository(connFactory);
            var containerBranchRepository = new OrmLiteContainerBranchRepository(this.Context.NodeData, connFactory, predefinedProvider);
            var courseRepository = new OrmLiteCourseRepository(nodeData, connFactory, materialsModule.Materials.CourseRepository, nodeRepository);

            var courseService = new EcpCourseService(cryptoService, materialsModule.Materials.CourseRepository);
            var activationNumberProvider = new OlimpModuleActivationNumberProvider(materialsModule);
            var ticketGeneratorFactory = new DefaultTicketGeneratorFactory();
            var coursePersister = new OrmLiteCoursePersister(
                courseService, nodeData, activationNumberProvider, ticketGeneratorFactory);

            var branchRepository = new PermissionInsuredCreatedByUserBranchRepository(
                new OrmLiteCreatedByUserBranchRepository(
                    nodeData, connFactory, nodeRepository, containerBranchRepository, courseRepository, coursePersister, materialsModule.Materials.CourseRepository),
                new RoleProviderPermissionChecker());

            var topicProviderFactory = new OrmLiteTopicProviderFactory(materialsModule.Materials.CourseRepository);

            binder.Controller<CreateCourseController>(b => b
                .Bind<ICreatedByUserBranchRepository>(() => branchRepository)
                .Bind<ITopicsProviderFactory>(() => topicProviderFactory));
        }
    }
}