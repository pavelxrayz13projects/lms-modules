using Olimp.Core.Mvc.Security;
using Olimp.Courses.Generation.Tickets;
using Olimp.CoursesCreator.ViewModels;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Entities;
using Olimp.Domain.Catalogue.Security;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Materials;
using Olimp.Web.Controllers.Security;
using PAPI.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Olimp.CoursesCreator.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.UserCourses.Create + "," + Permissions.LocalCluster.UserCourses.Share, Order = 2)]
    [CheckActivation]
    public class CreateCourseController : ShareableUserMaterialBranchController<CreatedByUserMaterialBranch>
    {
        private ICreatedByUserBranchRepository _branchRepository;
        private ITopicsProviderFactory _topicProviderFactory;

        public CreateCourseController(ICreatedByUserBranchRepository branchRepository, ITopicsProviderFactory topicProviderFactory)
            : base(branchRepository)
        {
            if (branchRepository == null)
                throw new ArgumentNullException("branchRepository");

            if (topicProviderFactory == null)
                throw new ArgumentNullException("topicProviderFactory");

            _branchRepository = branchRepository;
            _topicProviderFactory = topicProviderFactory;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new IndexViewModel
            {
                AllowGenerationTableAction = !Plugin.DisableGenerationTableAction,
                AllowShareTableAction = !Plugin.DisableShareTableAction,
                AllowCreateCourses = Roles.IsUserInRole(Permissions.UserCourses.Create)
            });
        }

        public ActionResult GetCourses(TableViewModel model)
        {
            //TODO: Paging
            var branches = _branchRepository.GetAll()
                .Select(m => new { m.Id, m.Code, m.Name, IsShared = m.IsSharedGlobally })
                .GetTableData(model, m => m);

            return Json(branches);
        }

        public ActionResult GetTopics(int branchId)
        {
            var branch = _branchRepository.GetById(branchId);
            var course = (UserCourse)branch.Material;

            var topicProvider = _topicProviderFactory.Create(course);

            var distributionRows = new LinkedList<object>();
            foreach (var t in topicProvider.GetAll())
            {
                double percent;
                if (!branch.TopicsDistribution.TryGetValue(t.UniqueId, out percent))
                    percent = 0;

                distributionRows.AddLast(
                    new { Id = t.UniqueId, Index = distributionRows.Count, Percent = percent, t.Name });
            }

            return Json(new
            {
                Id = branchId,
                MaterialName = course.Name,
                TicketAmount = branch.TicketsAmount,
                TicketSize = branch.QuestionsInTicket,
                MaxErrors = branch.AllowedMistakesCount,
                TopicsDistribution = new { rows = distributionRows }
            });
        }

        public ActionResult GetInfoPathMsi()
        {
            var fileName = "olimpcourse.msi";
            var file = System.IO.File.OpenRead(Path.Combine(PlatformPaths.Default.Root, fileName));

            return File(file, "application/x-msdownload", fileName);
        }

        public ActionResult Delete(int id)
        {
            _branchRepository.DeleteById(id);

            //TODO: Reset cache
            //CourseTreeCache.BlameAll();

            return null;
        }

        public ActionResult Download(int id)
        {
            CoursePackageFormat format;
            var stream = _branchRepository.OpenSourcesStream(id, out format);

            return format == CoursePackageFormat.InfoPathZip
                ? File(stream, "application/octet-stream", id.ToString() + ".zip")
                : File(stream, "text/xml", id.ToString() + ".xml");
        }

        [HttpPost]
        public ActionResult CreateTickets(CreateTicketsViewModel model)
        {
            if (!ModelState.IsValid)
                return null;

            var distribution = model.Topics.Where(o => o.Percent > 0)
                .ToDictionary(k => k.Id, v => (double)v.Percent);

            try
            {
                _branchRepository.GenerateTickets(model.BranchId, model, distribution);
            }
            catch (NotEnoughQuestionsException)
            {
                return new StatusCodeResultWithText(500, I18N.Courses.NotEnoughQuestions);
            }

            return null;

            //CourseTreeCache.BlameAll();
            //return Json (new { Id = id, Code = code, Name = name });
        }

        [HttpPost]
        public ActionResult FromFile(CreateCourseFromFileViewModel model)
        {
            if (!ModelState.IsValid)
                return Json(new { status = new { errorMessage = I18N.Courses.InvalidRequest } }, "text/plain");

            CoursePackageFormat packageFormat;

            if (model.CourseXml.FileName.EndsWith(".zip"))
                packageFormat = CoursePackageFormat.InfoPathZip;
            else if (model.CourseXml.FileName.EndsWith(".xml"))
                packageFormat = CoursePackageFormat.InfoPathXml;
            else
                return Json(new { status = new { errorMessage = I18N.Courses.InvalidFormat } }, "text/plain");

            try
            {
                using (var stream = model.CourseXml.InputStream)
                    _branchRepository.Include(stream, packageFormat);
            }
            catch
            {
                return Json(new { status = new { errorMessage = I18N.Courses.InvalidFormat } }, "text/plain");
            }

            return Json(new { status = "success" }, "text/plain");
        }
    }
}