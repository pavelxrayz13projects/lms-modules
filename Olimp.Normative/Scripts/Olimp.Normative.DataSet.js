$.Olimp = $.Olimp || {};
$.Olimp.Normative = $.Olimp.Normative || {};
$.Olimp.Normative.DataSet = $.Olimp.Normative.DataSet || {};

$.extend($.Olimp.Normative.DataSet, {
	options: { getTopicQuestionsTableUrl: null, addTopicUrl: null, deleteTopicUrl: null, saveSelectionUrl: null, saveTopicUrl: null, addSelectionUrl: null, deleteSelectionUrl: null },
	setup: function(options) { $.extend($.Olimp.Normative.DataSet.options, options) }
});	
		
var createMappings = function(collectionName, data, options, type) {
	var mapping = {},
		self = this,
		last = null,
		collection = data[collectionName];
			
	if (collection.length)
		last = collection[collection.length - 1];		
			
	mapping[collectionName] = { 
		key: function(o) { return ko.utils.unwrapObservable(o.id) },
		create: function (o) { 
			o.data.listItemClass = (o.data == last || o.data.last) ? ko.observable('last') : ko.observable('');
			o.data.parentId = ( !data.id ) ? 0 : data.id;
			return new type(o.data, options, self);				
		}
	};	
		
	return mapping;
}
	
function isInitialExpanded(options, collection, lambda) {
	if (options.initialExpanded)	
		return ko.observable(true);
	return ko.observable(false);
}
		
function getExpandeeClass() {
	return this.expanded() ? 'expandee icon icon-minus' : 'expandee icon icon-plus';
}
	
function extendListContainer(self, data) {	
	self.template = 'nodeTemplate';		
	self.templateData = ko.computed(function() { return this.topics() }, self);			
	self.subListClass = 'materials';		
	self.expandeeClass = ko.computed(getExpandeeClass, self);

	return self;
}
		
var Selection =  function  (data, options, rootModel) {				
	var nodesMapping = createMappings.apply(rootModel, ['topics', data, options, Topic]);
	data = data || { selections: [] };
	var self = this;
	self.expanded = isInitialExpanded(
		options, data.nodes, function (a) { return a.expanded });
	ko.mapping.fromJS(data, nodesMapping, extendListContainer(self, data));
	self.name = ko.observable(data.name);
	if(self.editing)
		self.editing = 	ko.observable(true);
	else
		self.editing = 	ko.observable(false);
		
	self.highlight = ko.computed(function(){
						var result = false;
						for(var key = 0; key < self.topics().length; key++){
							if (self.topics()[key].highlight() != ""){
								var result = true;
							}
						}
						if(result){
							return "highlight";
						}else{
							return "";
						}
					}) ;
			
	self.addTopic = function(){
		$.post($.Olimp.Normative.DataSet.options.addTopicUrl, { selectionId : self.id }, function(data) {
			if((self.topics().length - 1) >= 0){
				self.topics.mappedCreate({"id":data.id,"name":data.name, "editing" : true, "isnew" : true});
			}else{
				self.topics.mappedCreate({"id":data.id,"name":data.name, "editing" : true,"last" : true, "isnew" : true});
			}
			self.topics.sort(function(left, right) {
					if(left.isnew && right.isnew){
						return (left.id > right.id ? -1 : 1);
					}else if( left.isnew && !right.isnew){
						return -1;
					}else if ( !left.isnew && right.isnew){
						return 1;
					}else if ( !left.isnew && !right.isnew) 
						return left.name() == right.name() ? 0 : (left.name() < right.name() ? -1 : 1);	
			});
		}, 'json');
	}
	self.removeTopic = function(topic){
		if (confirm("Удалить тему?")) {
			$.post($.Olimp.Normative.DataSet.options.deleteTopicUrl, { topicId : topic.id }, function(data) {
				if(data.error) {
					$.Olimp.showError(data.message);
				} else {
					if (topic.listItemClass() == "last"){
						if((self.topics().length - 2) >= 0)
							self.topics()[self.topics().length - 2].listItemClass("last");
					}
					self.topics.mappedRemove(topic);					
					var questions = $('#table-with-questions');
					ko.cleanNode(questions[0]);
					questions.html(data);
				}
			},'json');
		}
								
	}
	
	self.showQuestions = function(topic){
		$.post($.Olimp.Normative.DataSet.options.getTopicQuestionsTableUrl, { topicId: topic.id }, function(data){
		    $('#table-with-questions').html(data);

			if (self.highlight() != ""){
				for(var key = 0; key < self.topics().length; key++){
					if (self.topics()[key].highlight() != "")
						self.topics()[key].highlight("");
				}	
			} else {
				for(var key = 0; key < TreeViewModel.selections().length; key++){
					if (TreeViewModel.selections()[key].highlight() != ""){
						for(var topicKey=0; topicKey <  TreeViewModel.selections()[key].topics().length; topicKey++ ){
							if (TreeViewModel.selections()[key].topics()[topicKey].highlight() != "")
								TreeViewModel.selections()[key].topics()[topicKey].highlight("");
						}
					}
				}
			}
			topic.highlight("highlight");
		},
		'html');
	}
	
	this.saveSelection = function(selection){
		if (selection.name() != ""){
			$.post($.Olimp.Normative.DataSet.options.saveSelectionUrl, {selectionId : selection.id, name : selection.name()}, function(data){
				if(data.error)
					$.Olimp.showError(data.message);
				else selection.editing(!selection.editing()); 					
					
			},'json');
						
		}else{
			$.Olimp.showError('Имя выборки не может быть пустым.');
		}
	}
	
	this.saveSelection = function(selection){
		if (selection.name() != ""){
			$.post($.Olimp.Normative.DataSet.options.saveSelectionUrl, {selectionId : selection.id, name : selection.name()}, function(data){
				if(data.error)
					$.Olimp.showError(data.message);
				else selection.editing(!selection.editing()); 					
					
			},'json');
						
		}else{
			$.Olimp.showError('Имя выборки не может быть пустым.');
		}
	}

};
		
var Topic =  function  (data, options, rootModel) {	
	$.extend(this, data);
	this.name = ko.observable(data.name);	
	this.highlight = ko.observable("");
	if(this.editing)
		this.editing = 	ko.observable(true);
	else
		this.editing = 	ko.observable(false);
	
	this.saveTopic = function(topic) {
		if (topic.name() != ""){
			$.post($.Olimp.Normative.DataSet.options.saveTopicUrl, { topicId : topic.id, name : topic.name() }, function(data) {
				if(data.error)
					$.Olimp.showError(data.message);
				else 
					topic.editing(!topic.editing()); 					
			}, 'json');
						
		}else{
			$.Olimp.showError('Имя темы выборки не может быть пустым.');
		}

	}
	

};
	
var Tree = function (data, options) {
	options = $.extend({ id: '', initialExpanded: true, extendViewModel: this }, options || {});
	var self = options.extendViewModel;
	data = data || { selections: [] };
					
	var nodeMapping = createMappings.apply(self, ['selections', data, options, Selection]);
			
	ko.mapping.fromJS(data, nodeMapping, self);
		
	$.extend(self, {
		toggleExpand: function (node) { node.expanded(!node.expanded()) }, 
		toggleEditing: function (node) { node.editing(!node.editing()); },
		addSelection: function()	{
			$.post($.Olimp.Normative.DataSet.options.addSelectionUrl, function(data) {
				if((self.selections().length - 1) >= 0){
					self.selections.mappedCreate({"id":data.id,"name":data.name,"topics":[], "editing" : true, "isnew" : true}) ;
				}else{
					self.selections.mappedCreate({"id":data.id,"name":data.name,"topics":[], "editing" : true, "last" : true, "isnew" : true}) ;
				}
				self.selections.sort(function(left, right) {
						if(left.isnew && right.isnew){
							return (left.id() > right.id() ? -1 : 1);
						}else if( left.isnew && !right.isnew){
							return -1;
						}else if ( !left.isnew && right.isnew){
							return 1;
						}else if ( !left.isnew && !right.isnew) 
							return left.name() == right.name() ? 0 : (left.name() < right.name() ? -1 : 1);	
				});
			}, 'json');
		},
		deleteSelection: function (selection) { 
			if (confirm("Удалить выборку?")) {
				$.post($.Olimp.Normative.DataSet.options.deleteSelectionUrl, {selectionId : selection.id}, function(data){
					if(data.error){
						$.Olimp.showError(data.message);
					}else{
						if (selection.listItemClass() == "last"){
							if((self.selections().length - 2) >= 0)
								self.selections()[self.selections().length - 2].listItemClass("last");
						}
						self.selections.mappedRemove(selection);					
					}
				},'json');
			}
		},
        updateTree: function(newData) { ko.mapping.fromJS(newData, rootMapping, self) }
	});
}
