$.Olimp = $.Olimp || {};
$.Olimp.Normative = $.Olimp.Normative || {};
$.Olimp.Normative.Info = $.Olimp.Normative.Info || {};

$.extend($.Olimp.Normative.Info, {
	options: { questionInfoUrl: null, addUrl: null, removeUrl: null, getSelectionDataUrl: null, selections: null, getHighlightedUrl: null },
	setup: function(options) { $.extend($.Olimp.Normative.Info.options, options) }
});

function showQuestionInfo (id) {
	var dialog = $('#question-dialog'),
		data = { 
			normativeId: $('#normatives').data('viewModel').activeMaterial().id,
			questionId: id
		};
	
	dialog.load($.Olimp.Normative.Info.options.questionInfoUrl, data, function() { dialog.olimpdialog('open') })
}
		
$(function () {
    function toggleDialogButtons(dialog, disable) {
        dialog.data('in-progress', disable).parent()
            .find('.olimp-dialog-buttonpane button').olimpbutton('option', 'disabled', disable);
    }

    var menu = $('#selection-menu'),
		menuMapping = {
			topics: { key: function(o) { return ko.utils.unwrapObservable(o.Id) } },
			selections: { key: function(o) { return ko.utils.unwrapObservable(o.Id) } }
		},
		dialog = $('#create-topic-dialog').olimpdialog({ 
			autoOpen: false, 
			draggable: false, 
			resizable: false, 
			modal: true, 
			width: 580, 						
			title: 'Создание темы',
			beforeClose: function () {
			    return dialog.data('in-progress') !== true;
			},
			buttons: {
				'Создать': function() {		
				    if (dialogForm.valid()) {
				        toggleDialogButtons(dialog, true);

				        $.post(dialogForm.attr('action'), dialogForm.serialize(), function (json) {
				            if (json.selection) {
				                viewModel.selections.push(json.selection);

				                viewModel.skipLoadingSelectionQuestions = true;
				                viewModel.selection(json.selection);
				            }

				            viewModel.topics.push(json.topic);
				            viewModel.topic(json.topic);

				            viewModel.save(
                                function () { toggleDialogButtons(dialog, false); dialog.olimpdialog('close'); },
                                function () { toggleDialogButtons(dialog, false) })
				        }, 'json').error(function () { toggleDialogButtons(dialog, false) });
					}				
				},
				'Отмена': function() { $(this).olimpdialog('close') }
			}
		}),
		dialogForm = dialog.find('form'),
		dialogModel = {};
		
	menu.find('button').olimpbutton(); 
		
	var viewModel = {
		normative: ko.observable(null),
		selection: ko.observable(null),
		skipLoadingSelectionQuestions: false,
		selections: ko.observableArray($.Olimp.Normative.Info.options.selections),
		highlighted: ko.observableArray([]),
		topic: ko.observable(null),
		topics: ko.observableArray([]),
		questionsCount: ko.observable(0),
		getAddRemoveData: function(catalogue) {
			var checkedNodes = catalogue.data('viewModel').getMinimalCheckedSubtree(true),
				items = $.map(checkedNodes, function(e) { return 'Items=' + ko.utils.unwrapObservable(e.id) }),
				questions = [];

			ko.utils.arrayForEach(catalogue.find('.tree-table-wrapper'), function(t) {
				var tableModel = $(t).data('viewModel');
		
				if (tableModel && !tableModel.allChecked()) {
					ko.utils.arrayForEach(tableModel.groups(), function(g) {
						ko.utils.arrayForEach(g.rows(), function(r) { r._checked() && questions.push('Questions=' + r.Id()) });
					});					
				}
			})
		
			if (!items.length && !questions.length)
				return null;			
		
			var dataParts = ['NormativeId=' + this.normative().id];
			if (this.topic())
				dataParts.push('TopicId=' + ko.utils.unwrapObservable(this.topic().Id))
			if (this.selection())
				dataParts.push('SelectionId=' + ko.utils.unwrapObservable(this.selection().Id))
		
			if (items.length)
				dataParts.push(items.join('&'));
		
			if (questions.length)
				dataParts.push(questions.join('&'));
		
			return dataParts.join('&');
		},
		save: function(cb, errorCb) {
			var self = this;
			if (!this.selection() || !this.topic()) {
				dialogForm.find(':text').val('')
				dialog.olimpdialog('open');
		
				return;
			}
		
			var catalogue = $('#normative-content .catalogue'),
				catalogueData = catalogue.data('viewModel'),
				requestData = this.getAddRemoveData(catalogue);
				
			if (!requestData) {
				$.Olimp.showSuccess('Выборка успешно сохранена.');
				$.isFunction(cb) && cb();	
				return;
			}
					
			$.post($.Olimp.Normative.Info.options.addUrl, requestData, function (json) {
			    ko.mapping.fromJS({ highlighted: json.highlighted }, {}, self);

			    $.isFunction(cb) && cb();

			    catalogueData.dynamicContentUrlParams({ selectionId: self.selection().Id })
			    catalogueData.checkAll(false);

			    catalogueData.highlight(json.highlighted);

			    var msg = 'Выборка успешно сохранена.';
			    if (json.addedCount)
			        msg += '<br/>Добавлено вопросов: ' + json.addedCount + '.';
			    if (json.movedCount)
			        msg += '<br/>Перемещено в другую тему: ' + json.movedCount + '.';

			    $.Olimp.showSuccess(msg);
			}, 'json').error($.isFunction(errorCb) ? errorCb : function () { })
		},
		remove: function() {
			var self = this;
			if (!this.selection())
				return;
					
			var catalogue = $('#normative-content .catalogue'),
				catalogueData = catalogue.data('viewModel'),
				requestData = this.getAddRemoveData(catalogue);
				
			if (!requestData) {
				$.Olimp.showSuccess('Выборка успешно сохранена.');
				return;
			}
					
			$.post($.Olimp.Normative.Info.options.removeUrl, requestData, function(json) {
				catalogueData.dynamicContentUrlParams({ selectionId: self.selection().Id })
				catalogueData.checkAll(false);
				
				catalogueData.highlight(json.highlighted);
		
				var msg = 'Выборка успешно сохранена.';
				if (json.removedCount)
					msg += '<br/>Удалено вопросов: ' + json.removedCount + '.';
		
				$.Olimp.showSuccess(msg);
			}, 'json')
		},
		updateHighlighted: function(catalogueModel) {
			$.post($.Olimp.Normative.Info.options.getHighlightedUrl, { id: this.selection().Id, normativeId: this.normative().id }, function(json) {
				catalogueModel.highlight(json.highlighted);
			})
		}
	};
		
	viewModel.selection.subscribe(function(selection) {
		var catalogueModel = $('#normative-content .catalogue').data('viewModel');
	
		if (selection) {
			if (viewModel.skipLoadingSelectionQuestions) {
				viewModel.skipLoadingSelectionQuestions = false;
				$.extend(catalogueModel.dynamicContentUrlParams(), { selectionId: selection.Id })
				
				return;
			}
		
			$.post($.Olimp.Normative.Info.options.getSelectionDataUrl, { id: selection.Id, normativeId: viewModel.normative().id }, function(json) { 
				viewModel.topics([]);
				//catalogueModel.checkAll(false);
				ko.mapping.fromJS({ topics: json.topics }, menuMapping, viewModel)
				catalogueModel.highlight(json.highlighted);
			}, 'json');
		
			catalogueModel.dynamicContentUrlParams({ selectionId: selection.Id })
		} else {						
			
			if (catalogueModel) {
				catalogueModel.dynamicContentUrlParams({});
				catalogueModel.highlight([]);
			}								
			
			viewModel.topics([])		
		}
	})
	
	$('#normatives').bind('materialActivated', function(ev, data) { 
		data !== null && viewModel.normative(data.material); 
		
		var selection = viewModel.selection();			
		
		if (selection) {			
			var catalogueModel = $('#normative-content .catalogue').data('viewModel');
			
			catalogueModel.dynamicContentUrlParams({ selectionId: selection.Id })
			viewModel.updateHighlighted(catalogueModel);		
		}
	})		
	viewModel.menuVisible = ko.computed(function() { return !!this.normative() }, viewModel);
		
	ko.applyBindings(viewModel, menu[0]);
		
	$.extend(dialogModel,{
		selectionId: ko.computed(function() {
			var selection = this.selection();
			return selection ? selection.Id : null;
		}, viewModel),
		createSelectionVisible: ko.computed(function() { return !this.selection() }, viewModel),
		lastRowClass: ko.computed(function() { return this.selection() ? 'last' : 'first last' }, viewModel)
	});
		
	ko.applyBindings(dialogModel, dialog[0]);
		
	$('#question-dialog').olimpdialog({ title: 'Информация о вопросе', autoOpen: false, resizable: false, width: '60%' });
})	
