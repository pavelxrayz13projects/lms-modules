using Olimp.Courses.Extensions;
using Olimp.Normative.ViewModels;
using Olimp.UI.ViewModels;
using PAPI.Core;
using ServiceStack.Text;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Normative.Controllers
{
    using Olimp.Core.Mvc.Security;
    using Olimp.Domain.Catalogue;
    using Olimp.Domain.Catalogue.Catalogue;
    using Olimp.Domain.Catalogue.Entities;
    using Olimp.Domain.Catalogue.Entities.Pseudo;
    using Olimp.Domain.Catalogue.Normatives;
    using Olimp.Domain.Catalogue.Security;
    using Olimp.Web.Controllers.Base;
    using Olimp.Web.Controllers.Security;

    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Normative, Order = 2)]
    [CheckActivation]
    public class NormativeInfoController : PController
    {
        private IWholeCatalogueProviderFactory _catalogueProviderFactory;
        private INormativeRepository _normativeRepository;
        private INormativeContentsProvider _normativeContentsProvider;
        private INormativeQuestionsProvider _normativeQuestionsProvider;
        private INormativeExcerptRepository _normativeExcerptRepository;
        private INormativeExcerptTopicRepository _normativeExcerptTopicRepository;

        public NormativeInfoController(
            IWholeCatalogueProviderFactory catalogueProviderFactory,
            INormativeRepository normativeRepository,
            INormativeContentsProvider normativeContentsProvider,
            INormativeQuestionsProvider normativeQuestionsProvider,
            INormativeExcerptRepository normativeExcerptRepository,
            INormativeExcerptTopicRepository normativeExcerptTopicRepository)
        {
            if (catalogueProviderFactory == null)
                throw new ArgumentNullException("catalogueProviderFactory");

            if (normativeRepository == null)
                throw new ArgumentNullException("normativeRepository");

            if (normativeContentsProvider == null)
                throw new ArgumentNullException("normativeContentsProvider");

            if (normativeQuestionsProvider == null)
                throw new ArgumentNullException("normativeQuestionsProvider");

            if (normativeExcerptRepository == null)
                throw new ArgumentNullException("normativeExcerptRepository");

            if (normativeExcerptTopicRepository == null)
                throw new ArgumentNullException("normativeExcerptTopicRepository");

            _catalogueProviderFactory = catalogueProviderFactory;
            _normativeRepository = normativeRepository;
            _normativeContentsProvider = normativeContentsProvider;
            _normativeQuestionsProvider = normativeQuestionsProvider;
            _normativeExcerptRepository = normativeExcerptRepository;
            _normativeExcerptTopicRepository = normativeExcerptTopicRepository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var statistics = _normativeRepository.GetStatistics();
            if (statistics.TotalNormativesCount == 0)
                return View("NoNormativesSplash");

            var excerpts = _normativeExcerptRepository.GetAll()
                .OrderBy(e => e.Name)
                .Select(o => new { o.Id, o.Name });

            return View(new NormativeInfoIndexViewModel
            {
                Selections = JsonSerializer.SerializeToString(excerpts)
            });
        }

        [HttpPost]
        public ActionResult GetHighlighted(int id, int normativeId)
        {
            return Json(new
            {
                highlighted = _normativeExcerptRepository.GetNormativeContainerItemIds(id, normativeId)
            });
        }

        [HttpPost]
        public ActionResult GetSelectionData(int id, int normativeId)
        {
            var topics = _normativeExcerptTopicRepository.GetByExcerptId(id);

            return Json(new
            {
                topics = topics.OrderBy(t => t.Name).Select(t => new { t.Id, t.Name }),
                highlighted = _normativeExcerptRepository.GetNormativeContainerItemIds(id, normativeId)
            });
        }

        [HttpPost]
        public ActionResult CreateSelectionAndTopic(NormativeInfoCreateSelectionAndTopic model)
        {
            ModelState.Remove("SelectionName");
            if (!ModelState.IsValid)
                return null;

            NormativeExcerpt excerpt = null;

            var excerptId = model.SelectionId.GetValueOrDefault();
            if (excerptId == 0)
            {
                if (string.IsNullOrEmpty(model.SelectionName))
                    return null;

                excerpt = _normativeExcerptRepository.Create(model.SelectionName);
                excerptId = excerpt.Id;
            }

            var topic = _normativeExcerptTopicRepository.CreateTopic(excerptId, model.TopicName);

            return Json(new
            {
                selection = excerpt != null
                    ? new { excerpt.Id, excerpt.Name }
                    : null,
                topic = new { topic.Id, topic.Name }
            });
        }

        [HttpPost]
        public ActionResult Add(NormativeInfoSaveTopicViewModel model)
        {
            var normative = _normativeRepository.GetByBranchId(model.NormativeId);
            var addData = new ModifyNormativeQuestionsData(normative, model.Questions, model.Items);

            var result = _normativeExcerptTopicRepository.AddQuestions(model.TopicId, addData);

            return Json(new
            {
                highlighted = result.ContainerItemIds,
                addedCount = result.TotalQuestionsAdded,
                movedCount = result.TotalQuestionsMoved
            });
        }

        [HttpPost]
        public ActionResult Remove(NormativeInfoSaveTopicViewModel model)
        {
            var normative = _normativeRepository.GetByBranchId(model.NormativeId);
            var removeData = new ModifyNormativeQuestionsData(normative, model.Questions, model.Items);

            var result = _normativeExcerptRepository.RemoveQuestions(model.SelectionId, removeData);

            return Json(new
            {
                highlighted = result.ContainerItemIds,
                removedCount = result.TotalQuestionsRemoved
            });
        }

        public ActionResult NormativeTree()
        {
            return View();
        }

        public ActionResult GetNormativeStat()
        {
            var statistics = _normativeRepository.GetStatistics();

            var model = new NormativeStatViewModel
            {
                NumberNormative = statistics.TotalNormativesCount,
                TotalNumberQuestion = statistics.TotalQuestionsCount,
                NumberSelectionGenerated = statistics.TotalExcerptsCount,
                TotalNumberQuestionInSelection = statistics.TotalExcerptsQuestionsCount
            };

            return PartialView("NormativeStat", model);
        }

        public ActionResult GetNormatives()
        {
            var catalogueProvider = _catalogueProviderFactory.Create(
                CatalogueLockingPolicy.NoLocking, PContext.ExecutingModule.Context.NodeData.Id);

            return Json(catalogueProvider.GetWholeCatalogue(MaterialType.Normative));

            //TODO: Caching
            //return NormativeCache.GetNormativesTree();
        }

        [HttpPost]
        public ActionResult GetQuestions(int normativeId, int? selectionId, Guid id)
        {
            var normative = _normativeRepository.GetByBranchId(normativeId);

            NormativeExcerpt excerpt = null;
            var excerptId = selectionId.GetValueOrDefault();
            if (excerptId > 0)
                excerpt = _normativeExcerptRepository.GetById(excerptId);

            var questions = _normativeQuestionsProvider.GetQuestions(normative, excerpt, id);

            var model = new TableViewModel { GroupBy = "Topic" };
            var data = questions.GetTableData(model,
                q => new
                         {
                             Id = q.UniqueId,
                             Text = q.GetTextWithFixedNormativeImageLinks(normativeId)
                         });

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNormative(int IdNormative)
        {
            //TODO: Cache
            //var normative = NormativeCache.GetNormative(IdNormative);

            var normative = _normativeRepository.GetByBranchId(IdNormative);
            var tree = _normativeContentsProvider.GetContents(normative);

            return PartialView("NormativeWizard", new NormativeEditViewModel
            {
                NormativeId = IdNormative,
                QuestionsCount = normative.QuestionsCount,
                ItemsCount = normative.ItemsCount,
                Items = tree
            });
        }

        [HttpPost]
        public ActionResult QuestionInfo(int normativeId, Guid questionId)
        {
            var normative = _normativeRepository.GetByBranchId(normativeId);
            var question = _normativeQuestionsProvider.GetQuestion(normative, questionId);

            return PartialView(new QuestionInfoViewModel
            {
                Question = question.GetTextWithFixedNormativeImageLinks(normativeId),
                TableData = question.Answers.GetTableData(
                    new TableViewModel(), a => new { a.Text, Correct = a.IsCorrect })
            });
        }
    }
}