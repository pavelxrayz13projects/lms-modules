using Olimp.Core;
using Olimp.Normative.ViewModels;
using Olimp.UI.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Normative.Controllers
{
    using Olimp.Core.Mvc.Security;
    using Olimp.Courses.Generation;
    using Olimp.Courses.Generation.Tickets;
    using Olimp.Domain.Catalogue;
    using Olimp.Domain.Catalogue.Entities;
    using Olimp.Domain.Catalogue.Normatives;
    using Olimp.Domain.Catalogue.Security;
    using Olimp.Normative.I18N;
    using Olimp.Normative.SortingHandlers;
    using Olimp.Web.Controllers;
    using Olimp.Web.Controllers.Materials;
    using Olimp.Web.Controllers.Security;
    using System.Web.Security;

    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Normative + "," + Permissions.LocalCluster.UserCourses.Share, Order = 2)]
    [CheckActivation]
    public class NormativeTestController : ShareableUserMaterialBranchController<GeneratedFromNormativesUserMaterialBranch>
    {
        private INormativeRepository _normativeRepository;
        private IGeneratedFromNormativesBranchRepository _branchRepository;
        private INormativeExcerptRepository _excerptRepository;
        private INormativeExcerptTopicRepository _topicRepository;
        private IProfessionRepository _professionRepository;
        private INormativeExcerptSerializer _excerptSerializer;
        private INormativeExcerptImporter _excerptImporter;
        private IExcerptCatalogueExporter _catalogueExporter;

        public NormativeTestController(
            INormativeRepository normativeRepository,
            IGeneratedFromNormativesBranchRepository branchRepository,
            INormativeExcerptRepository excerptRepository,
            INormativeExcerptTopicRepository topicRepository,
            IProfessionRepository professionRepository,
            INormativeExcerptSerializer excerptSerializer,
            INormativeExcerptImporter excerptImporter,
            IExcerptCatalogueExporter catalogueExporter)
            : base(branchRepository)
        {
            if (normativeRepository == null)
                throw new ArgumentNullException("normativeRepository");

            if (branchRepository == null)
                throw new ArgumentNullException("branchRepository");

            if (excerptRepository == null)
                throw new ArgumentNullException("excerptRepository");

            if (topicRepository == null)
                throw new ArgumentNullException("topicRepository");

            if (professionRepository == null)
                throw new ArgumentNullException("professionRepository");

            if (excerptSerializer == null)
                throw new ArgumentNullException("excerptSerializer");

            if (excerptImporter == null)
                throw new ArgumentNullException("excerptImporter");

            if (catalogueExporter == null)
                throw new ArgumentNullException("catalogueExporter");

            _normativeRepository = normativeRepository;
            _branchRepository = branchRepository;
            _excerptRepository = excerptRepository;
            _topicRepository = topicRepository;
            _professionRepository = professionRepository;
            _excerptSerializer = excerptSerializer;
            _excerptImporter = excerptImporter;
            _catalogueExporter = catalogueExporter;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var statistics = _normativeRepository.GetStatistics();
            if (statistics.TotalNormativesCount > 0)
            {
                return View(new NormativeTestIndexViewModel
                {
                    AllowExportCatalogueButton = OlimpApplication.InternalFeaturesMode,
                    AllowGenerateNormativeTests = Roles.IsUserInRole(Permissions.Normative)
                });
            }

            return View("NoNormativesSplash");
        }

        [NonAction]
        private object CreateDefaultTemplateValues()
        {
            return new
            {
                Id = 0,
                Code = String.Empty,
                Name = String.Empty,
                IsShared = false,
                TicketSize = 5,
                TicketsAmount = 10,
                MaxErrors = 1,
                SelectionId = String.Empty,
                ProfessionName = String.Empty,
                CurrentTicketsCount = 0,
                CurrentVersionsCount = 0,
                Distribution = String.Empty
            };
        }

        [NonAction]
        private object CreateExistingTemplateValues(GeneratedFromNormativesUserMaterialBranch branch)
        {
            var userMaterial = branch.Material as UserCourse;
            var currentTicketsCount = userMaterial != null
                ? userMaterial.TicketsCount
                : 0;

            return new
            {
                Id = branch.Id,
                Code = branch.Code ?? String.Empty,
                Name = branch.Name,
                IsShared = branch.IsSharedGlobally,
                TicketSize = branch.QuestionsInTicket,
                TicketsAmount = branch.TicketsAmount,
                MaxErrors = branch.AllowedMistakesCount,
                SelectionId = branch.Excerpt.Id.ToString(),
                Selection = branch.Excerpt.Name,
                ProfessionName = branch.Profession != null
                    ? branch.Profession.Name
                    : String.Empty,
                CurrentTicketsCount = currentTicketsCount,
                CurrentVersionsCount = branch.GeneratedVersionsCount,
                Distribution = branch.TopicsDistribution.GetTableData(
                    new TableViewModel(), p => new { Percent = p.Value, Id = p.Key.Id, Name = p.Key.Name })
            };
        }

        public ActionResult GetTemplate(int? id)
        {
            var templateId = id.GetValueOrDefault();
            if (templateId > 0)
            {
                var branch = _branchRepository.GetById(templateId);
                return Json(this.CreateExistingTemplateValues(branch));
            }
            else
            {
                return Json(this.CreateDefaultTemplateValues());
            }
        }

        [HttpGet]
        public ActionResult Edit()
        {
            var excerpts = _excerptRepository.GetAll()
                .OrderBy(e => e.Name)
                .Select(e => new SelectListItem { Text = e.Name, Value = e.Id.ToString() });

            return PartialView(new NormativeTestEditViewModel { Selections = excerpts });
        }

        [HttpPost]
        public ActionResult LookupProfession(string filter)
        {
            var professions = _professionRepository.Lookup(filter)
                .OrderBy(p => p.Name)
                .Select(p => p.Name);

            return Json(professions);
        }

        public ActionResult GetTemplates(TableViewModel model)
        {
            model.SortingHandlers["Name"] = new SortTestsByCodeAndNameSortingHandler();

            //TODO: Paging
            var tableData = _branchRepository.GetAll().Select(t => new TemplateViewModel
            {
                TicketSize = t.QuestionsInTicket,
                MaxErrors = t.AllowedMistakesCount,
                Id = t.Id,
                Code = t.Code ?? String.Empty,
                Name = t.Name,
                IsShared = t.IsSharedGlobally,
                TicketsAmount = t.TicketsAmount,
                Selection = Convert.ToString(t.Excerpt),
                CurrentTicketsCount = t.Material != null
                    ? ((UserCourse)t.Material).TicketsCount
                    : 0,
                CurrentVersionsCount = t.GeneratedVersionsCount
            }).GetTableData(model, t => t);

            return Json(tableData);
        }

        [HttpPost]
        public ActionResult GetTopicsDistribution(int? selectionId)
        {
            var topics = _topicRepository.GetByExcerptId(selectionId.GetValueOrDefault());

            return Json(new
            {
                topicsDistribution = topics.Select(st => new { st.Id, st.Name, Percent = 0 })
            });
        }

        [HttpPost]
        public ActionResult Save(int? id)
        {
            var model = new NormativeTestEditViewModel();
            this.UpdateModel(model);

            var profession = !String.IsNullOrWhiteSpace(model.ProfessionName)
                ? _professionRepository.GetOrCreate(model.ProfessionName)
                : null;
            var excerpt = _excerptRepository.GetById(model.SelectionId.Value);

            var templateId = id.GetValueOrDefault();

            var branch = templateId > 0
                ? _branchRepository.Update(templateId, model.TemplateCode, model.TemplateName, profession, excerpt, model, model.TopicsDistribution)
                : _branchRepository.Include(model.TemplateCode, model.TemplateName, profession, excerpt, model, model.TopicsDistribution);

            return Json(this.CreateExistingTemplateValues(branch));
        }

        [HttpPost]
        public ActionResult DeleteTemplate(int id)
        {
            _branchRepository.DeleteById(id);

            return null;

            //TODO: caching
            //CourseTreeCache.BlameAll();
        }

        public ActionResult DeleteTickets(int id)
        {
            _branchRepository.DeleteTickets(id);

            return null;

            //TODO: Caching
            //	CourseTreeCache.BlameAll();
        }

        public ActionResult GenerateTickets(int id)
        {
            try
            {
                _branchRepository.GenerateTickets(id);
            }
            catch (NotEnoughQuestionsException)
            {
                return new StatusCodeResultWithText(500, I18N.Selection.NotEnoughQuestions);
            }

            return null;

            //TODO: Caching
            //CourseTreeCache.BlameAll();
        }

        public ActionResult ExportTest(int id)
        {
            var test = _branchRepository.GetById(id);

            var fileName = !String.IsNullOrWhiteSpace(test.Code)
                ? CourseNaming.ConvertName(test.Code, false)
                : "normatives-test";

            fileName += ".ene";

            return File(
                stream => _excerptSerializer.SerializeToStream(test, stream),
                "application/octet-stream",
                fileName);
        }

        public ActionResult ImportTest(ImportTestViewModel model)
        {
            ITestFromNormatives test;
            try
            {
                using (var stream = model.Package.InputStream)
                {
                    test = _excerptSerializer.DeserializeFromStream(stream);
                }
                _excerptImporter.Import(test, NormativeTest.DuplicatePostfixTemplate);
            }
            catch (InvalidOperationException)
            {
                return Json(new { status = new { errorMessage = I18N.NormativeTest.NormativeNotFound } }, "text/plain");
            }
            catch (Exception)
            {
                return Json(new { status = new { errorMessage = I18N.NormativeTest.NormativeImportTestError } }, "text/plain");
            }

            return Json(new { status = "success" }, "text/plain");
        }

        [InternalFeature]
        [HttpPost]
        public ActionResult ExportCatalogue()
        {
            var branches = _branchRepository.GetAll()
                .Where(b => !String.IsNullOrWhiteSpace(b.Code));

            return File(
                stream => _catalogueExporter.Export(branches, stream),
                "text/xml",
                "ExcerptCatalogue.xml");
        }
    }
}