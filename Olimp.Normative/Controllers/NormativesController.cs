using Olimp.Domain.Catalogue.Normatives;
using Olimp.Domain.Catalogue.Security;
using Olimp.Web.Controllers.Materials;
using Olimp.Web.Controllers.Security;
using System.Web.Mvc;

namespace Olimp.Normative.Controllers
{
    [HandleError]
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Admin.Materials.Switch, Order = 2)]
    public class NormativesController : MaterialsController<Olimp.Domain.Catalogue.Entities.Normative>
    {
        public NormativesController(INormativeRepository normativesRepository)
            : base(normativesRepository)
        { }

        /*protected override void CleanCache ()
        {
            NormativeCache.BlameTree();
        }*/
    }
}