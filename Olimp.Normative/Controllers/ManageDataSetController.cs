using Olimp.Courses.Extensions;
using Olimp.Normative.ViewModels;
using Olimp.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Normative.Controllers
{
    using Olimp.Core.Mvc.Security;
    using Olimp.Domain.Catalogue.Normatives;
    using Olimp.Domain.Catalogue.Security;
    using Olimp.Web.Controllers.Base;
    using Olimp.Web.Controllers.Security;

    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Normative, Order = 2)]
    [CheckActivation]
    public class ManageDataSetController : PController
    {
        private INormativeRepository _normativeRepository;
        private INormativeExcerptTreeProvider _normativeExcerptTreeProvider;
        private INormativeExcerptRepository _normativeExcerptRepository;
        private INormativeExcerptTopicRepository _normativeExcerptTopicRepository;
        private INormativeExcerptTopicQuestionRepositoryFactory _normativeExcerptTopicQuestionRepositoryFactory;

        public ManageDataSetController(
            INormativeRepository normativeRepository,
            INormativeExcerptTreeProvider normativeExcerptTreeProvider,
            INormativeExcerptRepository normativeExcerptRepository,
            INormativeExcerptTopicRepository normativeExcerptTopicRepository,
            INormativeExcerptTopicQuestionRepositoryFactory normativeExcerptTopicQuestionRepositoryFactory)
        {
            if (normativeRepository == null)
                throw new ArgumentNullException("normativeRepository");

            if (normativeExcerptTreeProvider == null)
                throw new ArgumentNullException("normativeExcerptTreeProvider");

            if (normativeExcerptRepository == null)
                throw new ArgumentNullException("normativeExcerptRepository");

            if (normativeExcerptTopicRepository == null)
                throw new ArgumentNullException("normativeExcerptTopicRepository");

            if (normativeExcerptTopicQuestionRepositoryFactory == null)
                throw new ArgumentNullException("normativeExcerptTopicQuestionRepositoryFactory");

            _normativeRepository = normativeRepository;
            _normativeExcerptTreeProvider = normativeExcerptTreeProvider;
            _normativeExcerptRepository = normativeExcerptRepository;
            _normativeExcerptTopicRepository = normativeExcerptTopicRepository;
            _normativeExcerptTopicQuestionRepositoryFactory = normativeExcerptTopicQuestionRepositoryFactory;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var statistics = _normativeRepository.GetStatistics();
            if (statistics.TotalNormativesCount > 0)
                return View();

            return View("NoNormativesSplash");
        }

        public ActionResult GetSelectionTopics()
        {
            var tree = new
            {
                selections = _normativeExcerptTreeProvider.GetWholeTree()
            };

            return Json(tree, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNormativeSelectionsStatistics()
        {
            var statistics = _normativeRepository.GetStatistics();

            return PartialView("NormativeSelectionsStatistics", new SelectionsStatisticsViewModel
            {
                SelectionsCount = statistics.TotalExcerptsCount,
                QuestionInSelectionsCount = statistics.TotalExcerptsQuestionsCount
            });
        }

        public ActionResult GetTopicQuestionsTable(int topicId)
        {
            var topic = _normativeExcerptTopicRepository.GetById(topicId);
            var excerpt = _normativeExcerptRepository.GetByTopicId(topicId);

            var model = new GetTopicQuestionsViewModel
            {
                TopicId = topicId,
                TopicName = topic.Name,
                SelectionName = excerpt.Name
            };

            return PartialView("TopicQuestionsTable", model);
        }

        public ActionResult GetTopicQuestions(GetTopicQuestionsViewModel model)
        {
            var repo = _normativeExcerptTopicQuestionRepositoryFactory.Create(model.TopicId);
            var page = repo.GetPage(model.PageSize.GetValueOrDefault(), model.CurrentPage.GetValueOrDefault());

            return Json(new
                            {
                                rows = page.Entities.Select(q =>
                                    new
                                        {
                                            Id = q.Id,
                                            QuestionId = q.UniqueId,
                                            Question = q.GetTextWithFixedNormativeImageLinks(q.MaterialId),
                                            Number = q.Number
                                        }),
                                rowsCount = page.TotalCount
                            });
        }

        [HttpPost]
        public ActionResult AddSelection()
        {
            var excerpt = _normativeExcerptRepository.Create();

            return Json(new
            {
                error = false,
                id = excerpt.Id,
                name = excerpt.Name
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddTopic(int selectionId)
        {
            var topic = _normativeExcerptTopicRepository.CreateTopic(selectionId);

            return Json(new
            {
                error = false,
                id = topic.Id,
                name = topic.Name
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveSelection(int selectionId, string name)
        {
            _normativeExcerptRepository.Rename(selectionId, name);

            return Json(new
            {
                error = false,
                id = selectionId,
                name = name
            }, JsonRequestBehavior.AllowGet);

            //TODO: error handling
            //return Json(new { error = true, message = Olimp.Normative.I18N.Selection.SavingSelectionError });
        }

        public ActionResult SaveTopic(int topicId, string name)
        {
            _normativeExcerptTopicRepository.Rename(topicId, name);

            return Json(new
            {
                error = false,
                id = topicId,
                name = name
            }, JsonRequestBehavior.AllowGet);

            //TODO: error handling
            //return Json(new {error = true, message = Olimp.Normative.I18N.Selection.SavingSelectionTopicError});
        }

        public ActionResult ShowQuestion(int topicId, int questionInSelectionId)
        {
            var repo = _normativeExcerptTopicQuestionRepositoryFactory.Create(topicId);
            var question = repo.GetById(questionInSelectionId);

            return PartialView("QuestionInfo", new QuestionInfoViewModel
            {
                Question = question.Text,
                TableData = question.Answers.GetTableData(
                    new TableViewModel(), a => new { a.Text, Correct = a.IsCorrect })
            });
        }

        [HttpPost]
        public ActionResult DeleteSelection(int selectionId)
        {
            _normativeExcerptRepository.DeleteById(selectionId);

            return Json(new { error = false });
        }

        [HttpPost]
        public ActionResult DeleteTopic(int topicId)
        {
            _normativeExcerptTopicRepository.DeleteById(topicId);

            return Json(new { error = false });
        }

        public ActionResult Delete(int topicId, ICollection<int> objects)
        {
            var repo = _normativeExcerptTopicQuestionRepositoryFactory.Create(topicId);
            repo.DeleteManyById(objects.ToArray());

            return null;
        }
    }
}