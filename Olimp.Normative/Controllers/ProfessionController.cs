using Olimp.Domain.Catalogue.Normatives;
using Olimp.Domain.Catalogue.Security;
using Olimp.Normative.ViewModels;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using System;
using System.Web.Mvc;

namespace Olimp.Normative.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Normative, Order = 2)]
    public class ProfessionController : PController
    {
        private IProfessionRepository _professionRepository;

        public ProfessionController(IProfessionRepository professionRepository)
        {
            if (professionRepository == null)
                throw new ArgumentNullException("professionRepository");

            _professionRepository = professionRepository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        //TODO: Add datamember annotations for automatic json serialization
        [HttpPost]
        public ActionResult GetProfessions(TableViewModel model)
        {
            var professions = _professionRepository.GetSortedPage(
                model.PageSize.GetValueOrDefault(),
                model.CurrentPage.GetValueOrDefault(),
                model.SortColumn,
                model.SortAsc.HasValue && !model.SortAsc.Value);

            return Json(new
            {
                rows = professions.Entities,
                rowsCount = professions.TotalCount
            });
        }

        [HttpPost]
        public ActionResult Edit(ProfessionEditViewModel model)
        {
            if (!ModelState.IsValid)
                return null;

            if (model.Id > 0)
                _professionRepository.Rename(model.Id, model.Name);
            else
                _professionRepository.GetOrCreate(model.Name);

            return null;
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            _professionRepository.DeleteById(id);

            return null;
        }
    }
}