﻿using Olimp.Normative.ViewModels;
using Olimp.UI.ViewModels;
using System;
using System.Linq.Expressions;

namespace Olimp.Normative.SortingHandlers
{
    public class SortTestsByCodeAndNameSortingHandler : IExpressionBuilder
    {
        #region IExpressionBuilder Members

        public LambdaExpression Build()
        {
            Expression<Func<TemplateViewModel, string>> expression = t =>
                !String.IsNullOrWhiteSpace(t.Code)
                    ? String.Format("[{0}] {1}", t.Code, t.Name)
                    : t.Name;

            return expression;
        }

        #endregion IExpressionBuilder Members
    }
}