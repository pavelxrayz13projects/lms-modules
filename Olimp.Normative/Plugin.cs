using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.Normatives;
using Olimp.Normative.Controllers;
using PAPI.Core;
using PAPI.Core.Initialization;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Normative.Plugin))]

namespace Olimp.Normative
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(ManageDataSetController),
                typeof(NormativesController),
                typeof(NormativeInfoController),
                typeof(NormativeTestController),
                typeof(ProfessionController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            var resolver = PContext.ExecutingModule.Kernel.Resolver;

            binder.Controller<NormativeInfoController>(b => b
                .Bind<IWholeCatalogueProviderFactory>(resolver)
                .Bind<INormativeRepository>(resolver)
                .Bind<INormativeContentsProvider>(resolver)
                .Bind<INormativeQuestionsProvider>(resolver)
                .Bind<INormativeExcerptRepository>(resolver)
                .Bind<INormativeExcerptTopicRepository>(resolver));

            binder.Controller<ManageDataSetController>(b => b
                .Bind<INormativeRepository>(resolver)
                .Bind<INormativeExcerptTreeProvider>(resolver)
                .Bind<INormativeExcerptRepository>(resolver)
                .Bind<INormativeExcerptTopicRepository>(resolver)
                .Bind<INormativeExcerptTopicQuestionRepositoryFactory>(resolver));

            binder.Controller<NormativeTestController>(b => b
                .Bind<INormativeRepository>(resolver)
                .Bind<IGeneratedFromNormativesBranchRepository>(resolver)
                .Bind<INormativeExcerptRepository>(resolver)
                .Bind<INormativeExcerptTopicRepository>(resolver)
                .Bind<IProfessionRepository>(resolver)
                .Bind<INormativeExcerptSerializer>(resolver)
                .Bind<INormativeExcerptImporter>(resolver)
                .Bind<IExcerptCatalogueExporter>(resolver));

            binder.Controller<ProfessionController>(b => b.Bind<IProfessionRepository>(resolver));

            binder.Controller<NormativesController>(b => b.Bind<INormativeRepository>(resolver));
        }
    }
}