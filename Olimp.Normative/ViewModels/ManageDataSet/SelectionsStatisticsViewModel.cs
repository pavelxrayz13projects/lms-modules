namespace Olimp.Normative.ViewModels
{
    public class SelectionsStatisticsViewModel
    {
        public int SelectionsCount { get; set; }

        public int QuestionInSelectionsCount { get; set; }
    }
}