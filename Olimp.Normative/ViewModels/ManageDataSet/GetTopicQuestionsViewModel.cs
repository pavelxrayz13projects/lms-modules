using Olimp.UI.ViewModels;

namespace Olimp.Normative.ViewModels
{
    public class GetTopicQuestionsViewModel : TableViewModel
    {
        public int TopicId { get; set; }

        public string TopicName { get; set; }

        public string SelectionName { get; set; }
    }
}