﻿namespace Olimp.Normative.ViewModels
{
    public class NormativeTestIndexViewModel
    {
        public bool AllowExportCatalogueButton { get; set; }

        public bool AllowGenerateNormativeTests { get; set; }
    }
}