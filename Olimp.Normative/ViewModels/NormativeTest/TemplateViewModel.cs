﻿namespace Olimp.Normative.ViewModels
{
    public class TemplateViewModel
    {
        public int TicketSize { get; set; }

        public int MaxErrors { get; set; }

        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public bool IsShared { get; set; }

        public int TicketsAmount { get; set; }

        public string Selection { get; set; }

        public int CurrentTicketsCount { get; set; }

        public int CurrentVersionsCount { get; set; }
    }
}