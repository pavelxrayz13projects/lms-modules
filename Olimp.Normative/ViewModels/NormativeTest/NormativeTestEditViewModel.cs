using Olimp.Core.ComponentModel;
using Olimp.Domain.Catalogue.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.Normative.ViewModels
{
    public class NormativeTestEditViewModel : ITicketOptions
    {
        public IEnumerable<SelectListItem> Selections { get; set; }

        public NormativeTestEditViewModel()
        {
            this.TopicsDistribution = new Dictionary<int, double>();
        }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.Normative.I18N.Profession), DisplayNameResourceName = "Professions")]
        public string ProfessionName { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.TestTemplate), DisplayNameResourceName = "Code")]
        public string TemplateCode { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.TestTemplate), DisplayNameResourceName = "Name")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public string TemplateName { get; set; }

        public IDictionary<int, double> TopicsDistribution { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.Normative.I18N.Selection), DisplayNameResourceName = "Name")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int? SelectionId { get; set; }

        #region ITicketOptions Members

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.TestTemplate), DisplayNameResourceName = "MaxErrors")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int AllowedMistakesCount { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.TestTemplate), DisplayNameResourceName = "TicketSize")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int QuestionsInTicket { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.TestTemplate), DisplayNameResourceName = "TicketsAmount")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int TicketsAmount { get; set; }

        #endregion ITicketOptions Members
    }
}