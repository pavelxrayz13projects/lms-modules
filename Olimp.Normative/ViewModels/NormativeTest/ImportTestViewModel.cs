﻿using Olimp.Core.ComponentModel;
using Olimp.Normative.I18N;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Olimp.Normative.ViewModels
{
    public class ImportTestViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(NormativeTest), DisplayNameResourceName = "Package")]
        public HttpPostedFileBase Package { get; set; }
    }
}