using System;
using System.Collections.Generic;

namespace Olimp.Normative.ViewModels
{
    public class NormativeInfoSaveTopicViewModel
    {
        public NormativeInfoSaveTopicViewModel()
        {
            this.Items = new List<Guid>();
            this.Questions = new List<Guid>();
        }

        public int SelectionId { get; set; }

        public int TopicId { get; set; }

        public int NormativeId { get; set; }

        public IList<Guid> Items { get; private set; }

        public IList<Guid> Questions { get; private set; }
    }
}