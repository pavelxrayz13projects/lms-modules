namespace Olimp.Normative.ViewModels
{
    public class QuestionInfoViewModel
    {
        public string Question { get; set; }

        public object TableData { get; set; }
    }
}