namespace Olimp.Normative.ViewModels
{
    public class NormativeStatViewModel
    {
        public int NumberNormative { get; set; }

        public int TotalNumberQuestion { get; set; }

        public int NumberSelectionGenerated { get; set; }

        public int TotalNumberQuestionInSelection { get; set; }
    }
}