using Olimp.Core.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Olimp.Normative.ViewModels
{
    using I18NNorm = Olimp.Normative.I18N;

    public class NormativeInfoCreateSelectionAndTopic : IValidatableObject
    {
        public int? SelectionId { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18NNorm.Selection), DisplayNameResourceName = "SelectionName")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public string SelectionName { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18NNorm.Selection), DisplayNameResourceName = "TopicName")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public string TopicName { get; set; }

        #region IValidatableObject implementation

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if (!this.SelectionId.HasValue)
            {
                Validator.TryValidateProperty(
                    this.SelectionName,
                    new ValidationContext(this, null, null) { MemberName = "SelectionName" },
                    results);
            }

            Validator.TryValidateProperty(
                this.TopicName,
                new ValidationContext(this, null, null) { MemberName = "TopicName" },
                results);

            return results;
        }

        #endregion IValidatableObject implementation
    }
}