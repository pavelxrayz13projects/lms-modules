namespace Olimp.Normative.ViewModels
{
    public class NormativeEditViewModel
    {
        public int NormativeId { get; set; }

        public object Items { get; set; }

        public int QuestionsCount { get; set; }

        public int ItemsCount { get; set; }
    }
}