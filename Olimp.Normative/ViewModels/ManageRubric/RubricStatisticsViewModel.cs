namespace Olimp.Normative.ViewModels
{
    public class RubricStatisticsViewModel
    {
        public int RubricsCount { get; set; }

        public int QuestionInRubricsCount { get; set; }
    }
}