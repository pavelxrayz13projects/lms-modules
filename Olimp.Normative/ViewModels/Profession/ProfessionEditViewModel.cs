using Olimp.Core.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Olimp.Normative.ViewModels
{
    public class ProfessionEditViewModel
    {
        public int Id { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Profession), DisplayNameResourceName = "Name")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public string Name { get; set; }
    }
}