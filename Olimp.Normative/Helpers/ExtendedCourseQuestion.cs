using Olimp.Courses.Model;
using System;

namespace Olimp.Normative.Helpers
{
    public class ExtendedCourseQuestion
    {
        private NormativeQuestion _question;

        public ExtendedCourseQuestion(NormativeQuestion question, TopicName topicName)
        {
            _question = question;

            this.TopicName = topicName;
        }

        public string Text { get { return _question.Text; } }

        public Guid Id { get { return _question.Id; } }

        public TopicName TopicName { get; private set; }
    }
}