using System;

namespace Olimp.Normative.Helpers
{
    using I18N = Olimp.Normative.I18N.Selection;

    public class TopicName : IComparable<TopicName>
    {
        public TopicName(string name)
        {
            this.Value = name;
        }

        public TopicName()
        {
            this.IsDefault = true;
            this.Value = I18N.NotInSelection;
        }

        public bool IsDefault { get; private set; }

        public string Value { get; private set; }

        public override string ToString()
        {
            return this.Value;
        }

        public override bool Equals(object obj)
        {
            var other = obj as TopicName;
            if (other == null)
                return false;

            return this.Value.Equals(other.Value);
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        #region IComparable implementation

        public int CompareTo(TopicName other)
        {
            if (other == null)
                return 1;

            if (this.IsDefault && other.IsDefault)
                return 0;

            if (this.IsDefault)
                return 1;

            if (other.IsDefault)
                return -1;

            return this.Value.CompareTo(other.Value);
        }

        #endregion IComparable implementation
    }
}