﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NormativeStatViewModel>" %>


<table class="form-table">
	<tr class="first">		
		<th>Количество нормативов:</th>
		<td><%= Model.NumberNormative%></td>
	</tr>
	<tr>		
		<th>Общее количество вопросов:</th>
		<td><%= Model.TotalNumberQuestion%></td>
	</tr>
	<tr>		
		<th>Количество сформированных выборок:</th>
		<td><%= Model.NumberSelectionGenerated%></td>
	</tr>
	<tr class="last">		
		<th>Общее количество вопросов в выборках:</th>
		<td><%= Model.TotalNumberQuestionInSelection%></td>
	</tr>	
</table>

  


