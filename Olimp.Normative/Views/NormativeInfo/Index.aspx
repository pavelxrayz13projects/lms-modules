﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<NormativeInfoIndexViewModel>"  MasterPageFile="~/Views/Shared/Normative.master" %>

<asp:Content ContentPlaceHolderID="Olimp_Normative_Css" runat="server">
<%= "<!--[if lte IE 6]>" %>
	<link href="/Content/float-container.fix.ie6.css" rel="stylesheet" />
	<style type="text/css">
	@media screen {	
		* html div.button-float-container { height: 70px; margin-left: 0; }
		* html div.float-container-menu { position: absolute !important; height: auto !important; bottom: 0; left: 0; padding: 20px 6px 6px 20px; }		
		* html div.float-container-menu table tr td .olimp-button { position: static; }
	}
	</style>
<%= "<![endif]-->" %>
<%= "<!--[if lte IE 7]>" %>	
	<style type="text/css">
	div.float-container-menu table tr td .olimp-button { margin-right: 6px; }
	</style>
<%= "<![endif]-->" %>	
</asp:Content>	

<asp:Content ContentPlaceHolderID="Olimp_Normative_Js" runat="server">
<%= "<!--[if lte IE 6]>" %>
<script type="text/javascript">
	$(function() { $('#selection-menu').fixFloatingContainer() })
</script>
<%= "<![endif]-->" %>
	
<% Platform.RenderExtensions("/olimp/normative/info/js"); %>
	
<script type="text/javascript">	
$.Olimp.Normative.Info.setup({
	questionInfoUrl: '<%= Url.Action("QuestionInfo") %>',
	addUrl: '<%= Url.Action("Add") %>',
	removeUrl: '<%= Url.Action("Remove") %>',
	getSelectionDataUrl: '<%= Url.Action("GetSelectionData") %>',
	getHighlightedUrl: '<%= Url.Action("GetHighlighted") %>',
	selections: <%= Model.Selections %>
});
</script>
</asp:Content>
			
<asp:Content ContentPlaceHolderID="Olimp_Normative_Content" runat="server">

<% Html.EnableClientValidation(); %>
<% Html.RenderPartial("TableTemplates"); %>	
<% Html.RenderPartial("TableTemplates_doT"); %>		
	
<h1>Работа с каталогом ОКС:Норматив</h1>

<script type="text/html" id="question-info">
	<a href="javascript:showQuestionInfo('{{= ko.utils.unwrapObservable(it.$data.Id) }}')" class="icon icon-info action" title="Информация о вопросе"></a>
</script>

<script type="text/html" id="questionsTableTemplate">		
	<div style="clear: both;"></div>	
	<div class="tree-table-wrapper" data-bind="
		table: { 
		    paging: false, 
		    sorting: false, 
		    checkboxes: true, 
		    groupBy: 'TopicName', 
		    checkboxesInitialChecked: $data.nodeChecked(),
		    rowHighlighting: true,
			bodyTemplateEngine: 'doTEngine',
			actions:[new RowAction('', null, { templateId:'question-info' })]
		},
		tableColumns: [{ name: 'Text', caption: 'Вопрос' }],
		tableData: templateData,
		tableRowsChecked: nodeChecked">
	</div>	
</script>	
	
<% Html.RenderPartial("AnswerResultTemplate"); %>			
	
<div class="block" style="margin-bottom: 80px;">
	<table style="width: 100%;">		
		<tr>
			<td style="width: 350px; vertical-align: top; padding-right: 20px;">
				<div id='normative-tree'> 
					<% Html.RenderPartial("NormativeTree"); %>		
				</div>		
			</td>
			<td style="vertical-align: top; padding-left: 20px; border-left: solid 1px #C7CBD4;">
				<div id="normative-current">
					<h1>Статистика ОКС:Норматив</h1>
					<div class="block">					
					<% Html.RenderAction("GetNormativeStat"); %>	
					</div>
				</div>
			</td>
		</tr>		
	</table>		
</div>

<div id="selection-menu" data-bind="visible: menuVisible" class="button-float-container">	
	<div class="float-container-menu">
		<table>
			<tr>
				<td style="padding-right: 10px; border-right: solid 1px #C7CBD4;">
					<div style="font-size: 11px; font-weight: bold;">Текущая выборка:</div>					
					<select style="width: 250px;" data-bind="
						options: selections,
						optionsText: 'Name',
						optionsCaption: 'Новая выборка...',
						value: selection">
					</select>
				</td>
				<td style="padding: 0 10px; border-right: solid 1px #C7CBD4;">
					<div style="font-size: 11px; font-weight: bold;">Текущая тема:</div>
					<select style="width: 250px;" data-bind="
						options: topics,
						optionsText: 'Name',
						optionsCaption: 'Новая тема...',
						value: topic">
					</select>
				</td>
				<td style="padding-left: 10px;">
					<button data-bind="click: save" type="button">Добавить</button>	
					<button data-bind="click: remove" type="button">Удалить</button>
				</td>				
			</tr>
		</table>		
	</div>
</div>	

<div id="create-topic-dialog">	
<% Html.RenderPartial("CreateTopicDialog", new NormativeInfoCreateSelectionAndTopic()); %>		
</div>		

<div id="question-dialog"></div>

</asp:Content>