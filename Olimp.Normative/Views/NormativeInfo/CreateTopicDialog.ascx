﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NormativeInfoCreateSelectionAndTopic>" %>

<% using (Html.BeginForm("CreateSelectionAndTopic", "NormativeInfo")) { %>
	<%= Html.HiddenFor(m => m.SelectionId, new Dictionary<string, object> { 
		{ "data-bind", "value: selectionId" } }) %>
	
	<table class="form-table">
		<tr class="first" data-bind="visible: createSelectionVisible">
			<th><%= Html.LabelFor(m => m.SelectionName) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.SelectionName) %></div>
				<%= Html.TextBoxFor(m => m.SelectionName) %>
			</td>
		</tr>
		<tr data-bind="attr: { 'class': lastRowClass }">
			<th><%= Html.LabelFor(m => m.TopicName) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.TopicName) %></div>
				<%= Html.TextBoxFor(m => m.TopicName) %>
			</td>
		</tr>
	</table>
<% } %>