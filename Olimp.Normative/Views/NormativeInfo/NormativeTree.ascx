﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>


<script type="text/html" id="normativeLinkTemplate">
	<a href="#" class="-has-dot-binding" data-dot-context="{{= it.$$contextId }}" data-dot-bind="click: function() { return activateNormative($data, $root); }">{{= it.$data.fullName }}</a>	
</script>	

<%--
<script type="text/html" id="normativeLinkTemplate">
	<a href="#" data-bind="text: fullName, click: function() { return activateNormative($data, $root); }"></a>	
</script>	
--%>
<% Html.Catalogue(      
	new CatalogueOptions {  
		Id = "normatives",
    	SelectUrl = Url.Action("GetNormatives"), 
    	TemplateEngine = TemplateEngines.DoT,
		InitialExpanded = false, 
        ShowCheckBoxes = false,
        Activatable = true,
		MaterialTemplate = "normativeLinkTemplate"
	}); 
%>
     
<script type="text/javascript">
	function activateNormative(el, root) { 
		$.post('<%= Url.Action("GetNormative", new { controller = "NormativeInfo" })%>', { IdNormative: el.id },
			function(html){ 
				var current = $('#normative-current');
	
				ko.cleanNode(current[0]);
				
				current.html(html);
				root.activeMaterial(el); 
			},
			'html');
			
		return false; 
	}		
</script>