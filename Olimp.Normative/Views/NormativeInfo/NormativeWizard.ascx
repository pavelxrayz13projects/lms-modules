﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NormativeEditViewModel>" %>

<h1>Содержание</h1>
<div class="block" id='normative-content'> 
	<table class="form-table" style="margin-bottom: 6px;">
		<tr class="first">
			<th>Количество вопросов в нормативе:</th>
			<td><%=Model.QuestionsCount %></td>
		</tr>
		<tr>
			<th>Количество разделов в нормативе:</th>
			<td><%=Model.ItemsCount%></td>
		</tr>
		<tr>
			<td colspan="2">
				<a href="#" id="expand-headings">развернуть</a><span>&nbsp;/&nbsp;</span><a href="#" id="collapse-headings">свернуть</a>
			</td>
		</tr>
	</table>	
		
     <% Html.Catalogue(      
          new CatalogueOptions {  
               	TreeData = Model.Items,
               	TemplateEngine = TemplateEngines.DoT,
               	DynamicContentUrl = Url.Action("GetQuestions", new { normativeId = Model.NormativeId }),
               	DynamicContentTemplate = "questionsTableTemplate",
               	InitialExpanded = true, 
               	ShowCheckBoxes = true,
               	Highlighting = true
          }); 
     %>
</div>

<script type="text/javascript">
$(function() {
	$('#expand-headings, #collapse-headings').click(function() { 
		var model = $('#normative-current .catalogue').data('viewModel');
		if (model) 
			model.expandAll($(this).attr('id') === 'expand-headings');
	
		return false;
	})
})	
</script>	
		