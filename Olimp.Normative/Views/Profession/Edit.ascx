﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<ProfessionEditViewModel>" %>

<% Html.EnableClientValidation(); %>
	
<% using(Html.BeginForm("Edit", "Profession")) { %>
	
	<%= Html.HiddenFor(m => m.Id, new Dictionary<string, object> { 
		{ "data-bind", "value: Id" } }) %>
	
	<table class="form-table">
		<tr class="first last">
			<th><%= Html.LabelFor(m => m.Name) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Name) %></div>
				<%= Html.TextBoxFor(m => m.Name, new Dictionary<string, object> { 
					{ "data-bind", "value: Name" } }) %>
			</td>
		</tr>
	</table>
<% } %>			


	