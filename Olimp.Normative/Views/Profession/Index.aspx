﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage"  MasterPageFile="~/Views/Shared/Register.master" %>
<%@ Import Namespace="I18N=Olimp.Normative.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Register_Content" runat="server">
	<h1><%= I18N.Profession.Professions %></h1>		
	
	<div id="edit-dialog">	
		<% Html.RenderPartial("Edit"); %>
	</div>
	
	<div class="block">	
		<% Html.Table(
			new TableOptions {
				SelectUrl = Url.Action("GetProfessions"),
				Adding = new AddingOptions { 
					Text = I18N.Profession.Add,
					New = new { Id = 0, Name = "" }
				},
				DefaultSortColumn = "Name",
				PagingAllowAll = true,
				Columns = new[] { 
					new ColumnOptions("Name", I18N.Profession.Name) },
				Actions = new RowAction[] { 
					new EditAction("edit-dialog", new DialogOptions { Title = I18N.Profession.Edit, Width = "600px" }),
					new DeleteAction(Url.Action("Delete"), I18N.Profession.ConfirmDelete)	}
			}); %>
	</div>
</asp:Content>
