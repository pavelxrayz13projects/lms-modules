﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<ImportTestViewModel>" %>

<% Html.EnableClientValidation(); %>

<% using(Html.BeginForm("ImportTest", "NormativeTest", FormMethod.Post, new { 
	id= "import-test-from", target = "import-test-fake-frame", enctype = "multipart/form-data"  })) { %>

	<table class="form-table">
		<tr class="first last">
			<th><%= Html.LabelFor(m => m.Package) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Package) %></div>
				<div><%= Html.TextBoxFor(m => m.Package, new { type="file" }) %></div>
			</td>
		</tr>
	</table>

<% } %>

<iframe id="import-test-fake-frame" name="import-test-fake-frame" style="display: none"></iframe>  