﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<NormativeTestIndexViewModel>"  MasterPageFile="~/Views/Shared/Normative.master" %>

<asp:Content ContentPlaceHolderID="Olimp_Normative_Js" runat="server">
	<% Html.EnableClientValidation(); %>
	
	<script type="text/javascript">	
	function deleteTickets () {
	    if (!confirm('Тест будет деактивирован. Продолжить?'))
			return;
		
		var self = this;
	    $.post('<%= Url.Action("DeleteTickets") %>', { id: this.Id() }, function() {
	        self.CurrentTicketsCount(0);
	        $.Olimp.showSuccess('Тест деактивирован');
	    });
	}	
	function generateTickets () {
	    var self = this;
	    if (typeof self.InProgress === 'undefined')
	        self.InProgress = false;

	    if (self.InProgress === true || (self.CurrentTicketsCount() > 0) && !confirm('Текущая версия теста будет заменена новой. Продолжить?'))
	        return;
	    
	    self.InProgress = true;
	    $.post('<%= Url.Action("GenerateTickets") %>', { id: this.Id() }, function() {
	        self.CurrentTicketsCount(self.TicketsAmount());
	        self.CurrentVersionsCount(self.CurrentVersionsCount() + 1);
	        self.InProgress = false;
	        $.Olimp.showSuccess('Генерация билетов завершена');
	    }).fail(function () {
	        self.InProgress = false;
	    });
	}

	$(function() {
	    $('#edit-dialog').bind('beforeSubmit', function() {
	        var self = this,
	            sum = 0,
	            textBoxes = $(this).find('.percentage').each(function() { sum += Number($(this).val()) })

	        $('#percentage-validation').text(sum != 100
	            ? 'Сумма процентных соотношений должна быть равна ста процентам'
	            : '');

	        textBoxes.unbind('.editDialog')
	            .bind('change.editDialog', function() { $(self).triggerHandler('beforeSubmit'); });

	        return sum == 100;
	    });
	});	
	</script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Normative_Content" runat="server">

    <% if (Model.AllowExportCatalogueButton) { %>
        <script type="text/javascript">
            function exportCatalogue() { $('#export-catalogue-form').submit(); };
        </script>
        <% using (Html.BeginForm("ExportCatalogue", "NormativeTest", FormMethod.Post, new { id = "export-catalogue-form" })); %>
    <% } %>

	<script id="percent-template" type="text/html">
		<input type="hidden" data-bind="attr: { 
			name: 'TopicsDistribution[' + $data._index() + '].Key',
			value: Id }" />
		<input class="percentage" type="text" data-bind="attr: { 
			name: 'TopicsDistribution[' + $data._index() + '].Value' },
			value: Percent" />
	</script>

	<script id="topic-name" type="text/html">
		<span data-bind="text: Name"></span>
	</script>
    <% if (Model.AllowGenerateNormativeTests) { %>
	    <div id="edit-dialog">
		    <% Html.RenderAction("Edit"); %>
	    </div>

        <div id="upload-dialog">
		    <% Html.RenderPartial("Import", new ImportTestViewModel()); %>	
	    </div>
    <% } %>

    <script type="text/html" id="test-name-template">
        {{ var code = it.$data.Code(), name = it.$data.Name(); }}
        {{= code ? ('[' + code + '] ' + name) : name }}
	</script>

    <script type="text/html" id="download-action">
		<a class="icon icon-download action -has-dot-binding" title="Экспортировать тест и выборку" 
            data-dot-bind="attr: { href: '<%= Url.Action("ExportTest") %>/' + $data.Id() }"
            data-dot-context="{{= it.$$contextId }}">
		</a>
	</script>
	
	<script id="delete-tickets-action" type="text/html">
		<a href="#" class="icon icon-trash action -has-dot-binding" title="Деактивировать тест" 
            data-dot-bind="click: deleteTickets"
            data-dot-context="{{= it.$$contextId }}">
		</a>
	</script>
	
	<script id="generate-tickets-action" type="text/html">
		<a href="#" class="icon icon-generate-3 action -has-dot-binding" title="Сгенерировать билеты" 
            data-dot-bind="click: generateTickets"
            data-dot-context="{{= it.$$contextId }}">
		</a>
	</script>	
    
    <% Html.RenderPartial("SwitchShareUserMaterialBranchActionTemplate", new ShareUserBranchViewModel("NormativeTest"));  %>	
	
	<h1>Генерация тестов ОКС:Норматив</h1>	
	<div class="block">	
        <%
            var tableActions = new List<TableAction>();
            if (Model.AllowGenerateNormativeTests)
                tableActions.Add(new FileUploadAction("upload-dialog", "import-test-fake-frame", "Импорт", new DialogOptions { Title = "Импорт теста и выборки", Width = "550px" }));

            if (Model.AllowGenerateNormativeTests && Model.AllowExportCatalogueButton)
                tableActions.Add(new CustomTableAction("exportCatalogue", null, "Экспортировать каталог", null));
                
            var actions = new List<RowAction>();
            if (Model.AllowGenerateNormativeTests)
            {
                actions.Add(new EditAction("edit-dialog", new DialogOptions { Title = "Наcтройки генерации билетов", Width = "80%", FixedHeight = 0.8 }, new { title = "Редактировать тест" })
                {
                    LoadModelUrl = Url.Action("GetTemplate")
                });
                actions.Add(new TemplateAction("download-action"));
                actions.Add(new TemplateAction("generate-tickets-action"));
            }
            actions.Add(new TemplateAction("share-action"));
            if (Model.AllowGenerateNormativeTests)
            {
                actions.Add(new TemplateAction("delete-tickets-action"));
                actions.Add(new DeleteAction(Url.Action("DeleteTemplate"), "Тест будет удален. Продолжить?", null, new { title = "Удалить тест" }));
            }
        %>

		<% Html.Table(
			new TableOptions {
                BodyTemplateEngine = TemplateEngines.DoT,
				SelectUrl = Url.Action("GetTemplates"),
				Adding = new AddingOptions {
                    Disabled = !Model.AllowGenerateNormativeTests,
					Text = "Создать тест",
					LoadModelUrl = Url.Action("GetTemplate")
				},
				Columns = new[] { 
					new ColumnOptions("Name", "Наименование теста") { TemplateId = "test-name-template" },
					new ColumnOptions("Selection", "Выборка"),
                    new ColumnOptions("CurrentVersionsCount", Olimp.I18N.Domain.TestTemplate.CurrentVersions),
					new ColumnOptions("CurrentTicketsCount", Olimp.I18N.Domain.TestTemplate.CurrentTickets),
					new ColumnOptions("TicketsAmount", Olimp.I18N.Domain.TestTemplate.TicketsAmount),
					new ColumnOptions("TicketSize", Olimp.I18N.Domain.TestTemplate.TicketSize),					
					new ColumnOptions("MaxErrors", Olimp.I18N.Domain.TestTemplate.MaxErrors)
				},
				Actions = actions.ToArray(),
                TableActions = tableActions.ToArray()
			}); %>
	</div>	
	
	<script type="text/javascript">
	$(function() {
		$('#edit-dialog').bind('afterApply', function() {
			$('#profession-name').combobox({ 
				source: function (request, response) {
				    $.post('<%= Url.Action("LookupProfession") %>', { filter: request.term }, response);
				} });
		
			$('#selection-from-list').combobox().change(function(){
				var selectionId = $(this).find('option:selected').val();
			    $.post('<%= Url.Action("GetTopicsDistribution") %>', { selectionId: selectionId }, function(data) {
			        var topicsDistribution = data.topicsDistribution;
			        if (topicsDistribution.length == 0) {
			            $('#topics-distribution').data('viewModel').setRows({ rows: [] });
			        } else {
			            var rows = $('#topics-distribution').data('viewModel').rows(),
			                defaultPercent = Math.floor(100 / topicsDistribution.length),
			                lastPercent = 100 - (defaultPercent * (topicsDistribution.length - 1)),
			                lastId = topicsDistribution[topicsDistribution.length - 1].Id;

			            $('#topics-distribution').data('viewModel').setRows({
			                rowsCount: topicsDistribution.length,
			                rows: $.map(topicsDistribution, function(td) {
			                    return { Id: td.Id, Name: td.Name, Percent: td.Id == lastId ? lastPercent : defaultPercent };
			                })
			            });
			        }
			    });
			});	
		});
	})
	</script>
</asp:Content>