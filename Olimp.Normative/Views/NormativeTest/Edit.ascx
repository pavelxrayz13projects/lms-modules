﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NormativeTestEditViewModel>" %>
	

<% using(Html.BeginForm("Save", "NormativeTest")) { %>
	<%= Html.Hidden("id", "0", new Dictionary<string, object> { { "data-bind", "value: Id" } }) %>

	<table class="form-table">
		<tr class="first">						
			<th><%= Html.LabelFor(m => m.TemplateName) %></th>
			<td>
				<div class="validation-message"><%= Html.ValidationMessageFor(m => m.TemplateName) %></div>
				<%= Html.TextBoxFor(m => m.TemplateName, new Dictionary<string, object> {
					{ "data-bind", "value: Name" }
				}) %>				
			</td>
		</tr>
        <tr>						
			<th><%= Html.LabelFor(m => m.TemplateCode) %></th>
			<td>
				<div class="validation-message"><%= Html.ValidationMessageFor(m => m.TemplateCode) %></div>
				<%= Html.TextBoxFor(m => m.TemplateCode, new Dictionary<string, object> {
					{ "data-bind", "value: Code" }
				}) %>				
			</td>
		</tr>
		<tr>
			<th><%= Html.LabelFor(m => m.SelectionId) %></th>
			<td>
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.SelectionId) %></div>
				<%= Html.DropDownListFor(m => m.SelectionId, Model.Selections, "Выберите выборку...", new Dictionary<string, object> {
					{ "id", "selection-from-list" },
					{ "data-bind", "value: SelectionId" }
				}) %>
			</td>
		</tr>
		<tr>
			<th><%= Html.LabelFor(m => m.ProfessionName) %></th>
			<td>
				<div class="olimp-validation-error">
					<%= Html.ValidationMessageFor(m => m.ProfessionName) %>
				</div>
				<div>
					<%= Html.TextBoxFor(m => m.ProfessionName, new Dictionary<string, object> { 
						{ "id", "profession-name" },
						{ "style", "width: 130px;" },
						{ "data-bind", "value: ProfessionName" } }) %>										
				</div>
			</td>
		</tr>	
		<tr>						
			<th><%= Html.LabelFor(m => m.TicketsAmount) %></th>
			<td>
				<div class="validation-message">
					<%= Html.ValidationMessageFor(m => m.TicketsAmount) %>
				</div>
				<%= Html.TextBoxFor(m => m.TicketsAmount, new Dictionary<string, object> {
					{ "data-bind", "value: TicketsAmount"}
				}) %>				
			</td>
		</tr>	
		<tr>						
			<th><%= Html.LabelFor(m => m.QuestionsInTicket) %></th>
			<td>
				<div class="validation-message">
				<%= Html.ValidationMessageFor(m => m.QuestionsInTicket) %>
				</div>
				<%= Html.TextBoxFor(m => m.QuestionsInTicket, new Dictionary<string, object> {
					{ "data-bind", "value: TicketSize"}
				}) %>				
			</td>
		</tr>
		<tr>						
			<th><%= Html.LabelFor(m => m.AllowedMistakesCount) %></th>
			<td>
				<div class="validation-message">
				<%= Html.ValidationMessageFor(m => m.AllowedMistakesCount) %>
				</div>
				<%= Html.TextBoxFor(m => m.AllowedMistakesCount, new Dictionary<string, object> {
					{ "data-bind", "value: MaxErrors"}
				}) %>				
			</td>
		</tr>	
	</table>	

	<div id="percentage-validation" class="validation-message"></div>
	
	<div style="margin-top: 6px;" id="topics-distribution" data-bind="
		table: { paging: false, sorting: false },
		tableColumns: [
			{ name: 'Name', caption: 'Наименование темы', templateId: 'topic-name' },
			{ name: 'Percent', caption: 'Процент вопросов', templateId: 'percent-template' }],
		tableData: Distribution">
	</div>
<% } %>

