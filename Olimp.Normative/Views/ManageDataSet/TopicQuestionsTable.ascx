﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<GetTopicQuestionsViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Normative.I18N" %>

<script type="text/javascript">	
window.showQuestion = function(questionInselection){
	$.post('<%= Url.Action("ShowQuestion", new { TopicId = Model.TopicId }) %>', { questionInSelectionId : questionInselection.Id()}, function(html) {
		$("#question-dialog").html(html);
	
		$("#question-dialog").olimpdialog({
			title: '<%= I18N.Selection.Question %>' + ' ' + questionInselection.Number(),
			width: 650,
			resizable: false,
			modal: true
		});
	})	
	return false;
}
</script>

<script type="text/html" id="show-question-action">
	<a title="<%= I18N.Selection.ShowQuestion%>" href="#" class="icon icon-info action" data-bind="click: window.showQuestion"></a>
</script>

<script id="question-number" type="text/html">
	<span style="white-space:nowrap;"><%= I18N.Selection.Question%> <span data-bind="text: Number"></span></span>
</script>
<div id="question-dialog"></div>	
		
<h1>Вопросы по выборке &laquo;<%=Model.SelectionName%>&raquo;, тема &laquo;<%=Model.TopicName%>&raquo;</h1>
<%--<div class="block">--%>
<% Html.Table(new TableOptions {                
		SelectUrl = Url.Action("GetTopicQuestions", new { TopicId = Model.TopicId }),
    	Sorting = false, 
		Checkboxes = true,
        RowsHighlighting = true,
    	Paging = true, 
    	Columns = new[] {  
			new ColumnOptions("Number", I18N.Selection.QuestionNumber) { TemplateId = "question-number" },
        	new ColumnOptions("Question", "Текст вопроса") },   
		Actions = new RowAction[] { new TemplateAction("show-question-action")},
		TableActions = new TableAction[] { 
			new MassDeleteAction(Url.Action("Delete", new { TopicId = Model.TopicId }), I18N.Selection.ConfirmDelete, Olimp.I18N.PlatformShared.Delete)	}
	}, new { id = "topic-questions" }); %>	
<%--</div>--%>
