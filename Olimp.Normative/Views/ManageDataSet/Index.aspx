﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage"  MasterPageFile="~/Views/Shared/Normative.master" %>
<asp:Content ContentPlaceHolderID="Olimp_Normative_Css" runat="server">
<style type="text/css">	
	#SelectionCatalogue .catalogue	{padding-top: 15px;}	
	.add-selection-link * { float: left; }
		
	.add-selection-link {font-size: 13px; font-weight: bold; cursor: pointer;}
	
	.catalogue .node-caption .topic-name { float: none; padding-left:10px}
		
	.catalogue .node-caption .text-input { position:relative; margin: -3px 0 3px 0; }
		
	.catalogue-icon { cursor: pointer; margin: 0 5px 0 0; }
		
	.catalogue .node-caption .topic-name .highlight { font-weight: bold; }
	
	.catalogue .node-caption .node-name .highlight { text-decoration: underline; }
			
</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="Olimp_Normative_Js" runat="server">
	
<% Platform.RenderExtensions("/olimp/normative/dataSet/js"); %>	
	
<script type="text/javascript">	
$.Olimp.Normative.DataSet.setup({
	getTopicQuestionsTableUrl: '<%= Url.Action("GetTopicQuestionsTable")%>',
	addTopicUrl: '<%= Url.Action("AddTopic")%>',
	deleteTopicUrl: '<%= Url.Action("DeleteTopic")%>',
	saveSelectionUrl: '<%= Url.Action("SaveSelection")%>',
	saveTopicUrl: '<%= Url.Action("SaveTopic")%>',
	addSelectionUrl: '<%= Url.Action("AddSelection")%>',
	deleteSelectionUrl: '<%= Url.Action("DeleteSelection")%>'
});
</script>
</asp:Content>
		
<asp:Content ContentPlaceHolderID="Olimp_Normative_Content" runat="server">	
	<h1>Работа с выборками ОКС:Норматив</h1>
	<div class="block">
		<%-- <% Html.RenderAction("GetNormativeSelectionsStatistics"); %> --%>
		<table style="width:100%;" cellspacing="0" cellpadding="0">
			<tr>
		    	<td id="SelectionCatalogue" style="padding: 0 10px 10px 10px;width:300px;vertical-align:top;border-right: 1px solid #C7CBD4;">
					<a title="Добавить выборку" href="#" class="add-selection-link" data-bind="click: addSelection"><span style="display:inline;" class="icon catalogue-icon icon-add"></span>Добавить выборку </a>
					<ul class="catalogue without-checkboxes" data-bind="template: { name: 'nodeTemplate', foreach: selections}">
					</ul>
				
					<script id="nodeTemplate" type="text/html">
						<li data-bind="attr: { 'class': listItemClass }">
									
							<div class="node-caption">
								<div data-bind="attr: { 'class': expandeeClass }, click: $root.toggleExpand"></div>
								<div class="gap">&nbsp;</div>
								<div class="node-name">
									<span title="Добавить тему" class="icon catalogue-icon icon-add" data-bind="click: addTopic"></span>
									<span title="Удалить выборку" class="icon catalogue-icon icon-delete-active" data-bind="click: $root.deleteSelection"></span>
									<!-- ko ifnot: editing -->
										<span title="Редактировать выборку" class="icon catalogue-icon icon-pencil-active" data-bind="click: $root.toggleEditing"></span>
									 	<span class="selection-name" style="cursor: default; float:none;" data-bind="text: name, attr:{'class': highlight}"></span>											
									<!-- /ko -->
									<!-- ko if: editing -->
										<span title="Сохранить выборку" class="icon catalogue-icon icon-save" data-bind="click: saveSelection"></span>
										<input class="text-input" data-bind="value: name" />		
									<!-- /ko -->
								</div>
							</div>
							<div style="clear:both"></div>
							<ul data-bind="template: {name: 'topicTemplate', foreach: templateData}, attr: { 'class': subListClass }, visible: expanded">
							</ul>
						</li>
					</script>
					
					<script id="topicTemplate" type="text/html">
						<li data-bind="attr: { 'class': listItemClass }">
									
							<div class="node-caption">
								<div class="gap">&nbsp;</div>
								<div class="topic-name">
									<span  title="Удалить тему" class="icon catalogue-icon icon-delete-active" data-bind="click: $parent.removeTopic"></span>
									<!-- ko ifnot: editing -->
										<span title="Редактировать тему" class="icon catalogue-icon icon-pencil-active" data-bind="click: $root.toggleEditing"></span>
									 	<a href="#" style="margin: -3px 0 3px 0; float:none;" data-bind="text: name, click: $parent.showQuestions, attr : {'class' : highlight}"></a>

									<!-- /ko -->
									<!-- ko if: editing -->
										<span title="Сохранить тему" class="icon catalogue-icon icon-save" data-bind="click: saveTopic"></span>
										<input class="text-input" data-bind="value: name" />
									<!-- /ko -->
											
								</div>
							</div>
							<div style="clear:both"></div>
						</li>
					</script>
					
					<script type="text/javascript">
					$(function(){
						$.post('/Admin/ManageDataSet/GetSelectionTopics', function(json){
							TreeViewModel = new Tree(json, { id:'SelectionCatalogue'});
							ko.applyBindings(TreeViewModel, $('#SelectionCatalogue').get(0));
						}, 'json');
										
						$(".text-input").live("focus", function(){ $(this).select(); });
					});
					</script>	
				</td>
		        <td id="table-with-questions" style="vertical-align:top;padding-left: 20px;"></td>
			</tr>           
		</table>  
	</div>	
</asp:Content>