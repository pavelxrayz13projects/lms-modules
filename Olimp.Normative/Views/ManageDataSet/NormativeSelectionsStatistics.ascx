﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SelectionsStatisticsViewModel>" %>
<table class="form-table" style="margin-bottom: 6px;">
	<tr class="first">		
		<th>Количество сформированных выборок:</th>
		<td><%= Model.SelectionsCount%></td>
	</tr>
	<tr>		
		<th>Общее количество вопросов в выборках:</th>
		<td><%= Model.QuestionInSelectionsCount%></td>
	</tr>	
</table>
