﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<QuestionInfoViewModel>" %>

<script id="answer-result" type="text/html">
	<!-- ko if: Correct -->
	<%= Olimp.I18N.Domain.Attempt.CorrectAnswer %>
	<!-- /ko -->

	<!-- ko ifnot: Correct -->
	<%= Olimp.I18N.Domain.Attempt.IncorrectAnswer %>
	<!-- /ko -->
</script>
		
<h1><%= Model.Question %></h1>
<div class="block">
	<% Html.Table(new TableOptions {			
		TableData = Model.TableData,
		Sorting = false,
		Paging = false,
		Columns = new[] { 
			new ColumnOptions("Text", Olimp.I18N.Domain.Attempt.Answer),
			new ColumnOptions("Correct", Olimp.I18N.Domain.Attempt.AnswerCorrect) { TemplateId = "answer-result" }
		}}); %>
</div>