﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage"  MasterPageFile="~/Views/Shared/Normative.master" %>

<asp:Content ContentPlaceHolderID="Olimp_Normative_Content" runat="server">

<h1>Комплект тестов &laquo;ОКС:Норматив&raquo; не обнаружен в Вашей системе</h1>	

<center>
	<img src="<%= Url.Content("~/Content/Images/normsplash.jpg") %>" 
		alt="Комплект тестов &laquo;ОКС:Норматив&raquo;"
		style="width: 600px;" />
</center>	

</asp:Content>