﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Olimp.LmsIntegration.I18N {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class LmsIntegration {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LmsIntegration() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Olimp.LmsIntegration.I18N.LmsIntegration", typeof(LmsIntegration).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Внешний вид.
        /// </summary>
        public static string Appearance {
            get {
                return ResourceManager.GetString("Appearance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите группу.
        /// </summary>
        public static string ChooseGroup {
            get {
                return ResourceManager.GetString("ChooseGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выгрузить тестовый пакет.
        /// </summary>
        public static string ExportDebugPackage {
            get {
                return ResourceManager.GetString("ExportDebugPackage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выгрузить профиль в формате SCORM 1.2.
        /// </summary>
        public static string ExportProfileInScrorm_1_2 {
            get {
                return ResourceManager.GetString("ExportProfileInScrorm_1_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Группа пользователей LMS.
        /// </summary>
        public static string LmsGroup {
            get {
                return ResourceManager.GetString("LmsGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to E-mail.
        /// </summary>
        public static string LmsScormMappingsEmail {
            get {
                return ResourceManager.GetString("LmsScormMappingsEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ФИО.
        /// </summary>
        public static string LmsScormMappingsFullName {
            get {
                return ResourceManager.GetString("LmsScormMappingsFullName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Логин.
        /// </summary>
        public static string LmsScormMappingsLogin {
            get {
                return ResourceManager.GetString("LmsScormMappingsLogin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Организация.
        /// </summary>
        public static string LmsScormMappingsOrganization {
            get {
                return ResourceManager.GetString("LmsScormMappingsOrganization", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Табельный номер.
        /// </summary>
        public static string LmsScormMappingsPersonnelNumber {
            get {
                return ResourceManager.GetString("LmsScormMappingsPersonnelNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Должность.
        /// </summary>
        public static string LmsScormMappingsPosition {
            get {
                return ResourceManager.GetString("LmsScormMappingsPosition", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выгрузка профилей.
        /// </summary>
        public static string LmsScormProfileDownload {
            get {
                return ResourceManager.GetString("LmsScormProfileDownload", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Настройки LMS-интеграции.
        /// </summary>
        public static string LmsSettings {
            get {
                return ResourceManager.GetString("LmsSettings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Схема соответствия SCORM API.
        /// </summary>
        public static string LmsSettingsScormApiMappings {
            get {
                return ResourceManager.GetString("LmsSettingsScormApiMappings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выгрузка профиля в формате SCORM .
        /// </summary>
        public static string ProfileExtractInScormFormat {
            get {
                return ResourceManager.GetString("ProfileExtractInScormFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SCORM.
        /// </summary>
        public static string Scorm {
            get {
                return ResourceManager.GetString("Scorm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Настройки сохранены.
        /// </summary>
        public static string SettingsSaved {
            get {
                return ResourceManager.GetString("SettingsSaved", resourceCulture);
            }
        }
    }
}
