using System.Collections.Generic;

namespace Olimp.LmsIntegration.ScormCourses
{
    public interface IScormCourseZipEntriesFactory
    {
        IEnumerable<ScormCourseZipEntry> Create(string olimpLink, int profileId, string scormCourseTitle);
    }
}