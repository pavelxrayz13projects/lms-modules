using System.IO;

namespace Olimp.LmsIntegration.ScormCourses
{
    public interface IScormCourseFactory
    {
        MemoryStream Create(string olimpLink, int profileId, string scormCourseTitle);

        MemoryStream CreateDebugPackage();
    }
}