using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace Olimp.LmsIntegration.ScormCourses
{
    internal class DefaultScormCourseFactory : IScormCourseFactory
    {
        private readonly IScormCourseZipEntriesFactory _scormCourseZipEntriesFactory;

        public DefaultScormCourseFactory(IScormCourseZipEntriesFactory scormCourseZipEntriesFactory)
        {
            _scormCourseZipEntriesFactory = scormCourseZipEntriesFactory;
        }

        public MemoryStream Create(string olimpLink, int profileId, string scormCourseTitle)
        {
            var courseZipEntries = _scormCourseZipEntriesFactory.Create(olimpLink, profileId, scormCourseTitle);
            return CreateFromEntries(courseZipEntries);
        }

        public MemoryStream CreateDebugPackage()
        {
            var courseZipEntries = DebugScormCourseZipEntriesFactory.Create();
            return CreateFromEntries(courseZipEntries);
        }

        private static MemoryStream CreateFromEntries(IEnumerable<ScormCourseZipEntry> courseZipEntries)
        {
            const int levelCompression = 9;

            var outputMemoryStream = new MemoryStream();
            using (var zipOutputStream = new ZipOutputStream(outputMemoryStream))
            {
                zipOutputStream.SetLevel(levelCompression);

                foreach (var e in courseZipEntries)
                    CopyEntryInStream(e, zipOutputStream);

                zipOutputStream.IsStreamOwner = false;
                zipOutputStream.Close();
            }

            outputMemoryStream.Position = 0;
            return outputMemoryStream;
        }

        private static void CopyEntryInStream(ScormCourseZipEntry entry, ZipOutputStream zipOutputStream)
        {
            var content = Encoding.UTF8.GetBytes(entry.Content);
            var zEntry = new ZipEntry(entry.Name) { DateTime = DateTime.Now, Size = content.Length };
            zipOutputStream.PutNextEntry(zEntry);

            using (var zipEntryMemoryStream = new MemoryStream(content))
                StreamUtils.Copy(zipEntryMemoryStream, zipOutputStream, content);

            zipOutputStream.CloseEntry();
        }
    }
}