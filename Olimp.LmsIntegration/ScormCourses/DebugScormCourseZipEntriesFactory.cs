﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Olimp.Configuration;
using Olimp.Domain.LearningCenter;
using Olimp.LmsIntegration.Properties;
using Olimp.Domain.LearningCenter.Entities;

namespace Olimp.LmsIntegration.ScormCourses
{
    internal static class DebugScormCourseZipEntriesFactory
    {
        private const string ImsmanifestZipEntryName = "imsmanifest.xml";
        private const string IndexHtmlZipEntryName = "index.html";
        private const string JqueryLibZipEntryName = "jquery-1.7.2.min.js";
        private const string ScormWrapperLibZipEntryName = "SCORM_API_wrapper.js";
        private const string QunitCssLibZipEntryName = "qunit.css";
        private const string QunitJsLibZipEntryName = "qunit-1.14.0.js";

        public static IEnumerable<ScormCourseZipEntry> Create()
        {
            var entries =
                new Collection<ScormCourseZipEntry>
                    {
                        new ScormCourseZipEntry {Content = Resources.debugImsmanifest, Name = ImsmanifestZipEntryName},
                        new ScormCourseZipEntry {Content = Resources.debugIndex, Name = IndexHtmlZipEntryName},
                        new ScormCourseZipEntry {Content = Resources.jquery_1_7_2_min, Name = JqueryLibZipEntryName},
                        new ScormCourseZipEntry {Content = Resources.SCORM_API_wrapper, Name = ScormWrapperLibZipEntryName},
                        new ScormCourseZipEntry {Content = Resources.qunit, Name = QunitCssLibZipEntryName},
                        new ScormCourseZipEntry {Content = Resources.qunit_1_14_0, Name = QunitJsLibZipEntryName},
                    };

            PrepareIndexHtml(entries, Config.Get<LmsMappingsConfiguration>().FullName);

            return entries;
        }

        private static void PrepareIndexHtml(
            IEnumerable<ScormCourseZipEntry> entryes, 
            string scormMappedFullName)
        {
            var entry = entryes.First(e => e.Name == IndexHtmlZipEntryName);
            entry.Content = entry.Content.Replace(ScormMappingDefinitions.FullName, scormMappedFullName);
        }
    }
}
