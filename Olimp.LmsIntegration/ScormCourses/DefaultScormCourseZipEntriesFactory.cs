using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Olimp.LmsIntegration.Properties;
using Olimp.Configuration;
using Olimp.Domain.LearningCenter.Entities;

namespace Olimp.LmsIntegration.ScormCourses
{
    internal class DefaultScormCourseZipEntriesFactory : IScormCourseZipEntriesFactory
    {
        private const string ProfileIdTag = "'{profileId}'";
        private const string OlimpLinkTag = "{olimpLinkTag}";
        private const string ScormCourseTitleTag = "{scormCourseTitleTag}";
        private const string ScormCourseIdentifierTag = "{scormCourseIdentifier}";
        private const string ScormCourseIdentifier = "olimp-scorm-profile";

        private const string ImsmanifestZipEntryName = "imsmanifest.xml";
        private const string IndexHtmlZipEntryName = "index.html";
        private const string JqueryLibZipEntryName = "jquery-1.7.2.min.js";
        private const string ScormWrapperLibZipEntryName = "SCORM_API_wrapper.js";

        public IEnumerable<ScormCourseZipEntry> Create(string olimpLink, int profileId, string scormCourseTitle)
        {
            var entries =
                new Collection<ScormCourseZipEntry>
                    {
                        new ScormCourseZipEntry {Content = Resources.imsmanifest, Name = ImsmanifestZipEntryName},
                        new ScormCourseZipEntry {Content = Resources.index, Name = IndexHtmlZipEntryName},
                        new ScormCourseZipEntry {Content = Resources.jquery_1_7_2_min, Name = JqueryLibZipEntryName},
                        new ScormCourseZipEntry {Content = Resources.SCORM_API_wrapper, Name = ScormWrapperLibZipEntryName}
                    };

            PrepareIndexHtml(entries, olimpLink, profileId, Config.Get<LmsMappingsConfiguration>());
            PrepareManifest(entries, scormCourseTitle, profileId);

            return entries;
        }

        private static void PrepareIndexHtml(
            IEnumerable<ScormCourseZipEntry> entryes, 
            string olimpLink, 
            int profileId,
            LmsMappingsConfiguration scormConfig)
        {
            if (!olimpLink.EndsWith("/")) olimpLink = string.Concat(olimpLink, "/");
            var entry = entryes.First(e => e.Name == IndexHtmlZipEntryName);
            entry.Content = entry.Content.Replace(OlimpLinkTag, olimpLink)
                .Replace(ProfileIdTag, profileId.ToString())
                .Replace(ScormMappingDefinitions.Login, scormConfig.Login)
                .Replace(ScormMappingDefinitions.PersonnelNumber, scormConfig.Number)
                .Replace(ScormMappingDefinitions.FullName, scormConfig.FullName)
                .Replace(ScormMappingDefinitions.Email, scormConfig.Email)
                .Replace(ScormMappingDefinitions.Organization, scormConfig.Organization)
                .Replace(ScormMappingDefinitions.Position, scormConfig.Position);
        }

        private static void PrepareManifest(IEnumerable<ScormCourseZipEntry> entryes, string scormCourseTitle, int profileId)
        {
            var entry = entryes.First(e => e.Name == ImsmanifestZipEntryName);

            entry.Content = entry.Content
                                 .Replace(ScormCourseTitleTag, scormCourseTitle)
                                 .Replace(ScormCourseIdentifierTag, string.Concat(ScormCourseIdentifier, "-", profileId));
        }
    }
}