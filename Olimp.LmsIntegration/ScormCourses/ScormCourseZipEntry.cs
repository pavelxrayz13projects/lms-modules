namespace Olimp.LmsIntegration.ScormCourses
{
    public class ScormCourseZipEntry
    {
        public string Name { get; set; }

        public string Content { get; set; }
    }
}