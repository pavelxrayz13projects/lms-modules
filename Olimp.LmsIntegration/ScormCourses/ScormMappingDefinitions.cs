﻿namespace Olimp.LmsIntegration.ScormCourses
{
    internal static class ScormMappingDefinitions
    {
        public const string Login = "{scormMappedLogin}";
        public const string PersonnelNumber = "{scormMappedPersonnelNumber}";
        public const string FullName = "{scormMappedFullName}";
        public const string Email = "{scormMappedEmail}";
        public const string Organization = "{scormMappedOrganization}";
        public const string Position = "{scormMappedPosition}";
    }
}