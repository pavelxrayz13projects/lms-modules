﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Olimp.Configuration;
using Olimp.Core.Mvc.Security;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.LmsIntegration.ScormCourses;
using Olimp.LmsIntegration.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using PAPI.Core.Data.Entities;

namespace Olimp.LmsIntegration.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Admin.LmsIntegration, Order = 2)]
    public class LmsIntegrationSettingsController : PController
    {
        private readonly IScormCourseFactory _scormCourseFactory;

        public LmsIntegrationSettingsController(IScormCourseFactory scormCourseFactory)
        {

            if (scormCourseFactory == null)
                throw new ArgumentNullException("scormCourseFactory");

            _scormCourseFactory = scormCourseFactory;
        }

        [HttpGet]
        public ActionResult Index()
        {
            using (UnitOfWork.CreateRead())
            {
                var config = Config.Get<OlimpConfiguration>();
                var lmsConfig = Config.Get<LmsMappingsConfiguration>();

                var viewModel = new LmsIntegrationSettingsViewModel
                {
                    GroupId = config.DefaultLmsGroup,
                    Groups = Repository.Of<Group>()
                        .Where(new EntityIsActive<Group>())
                        .Select(g => new SelectListItem { 
                            Text = g.Name, 
                            Value = g.Id.ToString(CultureInfo.InvariantCulture),
                            Selected = g.Id == config.DefaultLmsGroup
                         })
                        .ToList(),
                    Login = lmsConfig.Login,
                    PersonnelNumber = lmsConfig.Number,
                    FullName = lmsConfig.FullName,
                    Email = lmsConfig.Email,
                    Organization = lmsConfig.Organization,
                    Position = lmsConfig.Position
                };

                return View(viewModel);
            }
            
        }

        [HttpPost]
        public ActionResult SaveSettings(LmsIntegrationSaveSettingsViewModel model)
        {
            if (!TryValidateModel(model))
                return new StatusCodeResultWithText((int)HttpStatusCode.BadRequest, "The model is invalid.");

            using (var uow = UnitOfWork.CreateWrite())
            {
                try
                {
                    var config = Config.Get<OlimpConfiguration>();
                    config.DefaultLmsGroup = model.GroupId;

                    var lmsConfig = Config.Get<LmsMappingsConfiguration>();
                    lmsConfig.Login = model.Login;
                    lmsConfig.Number = model.PersonnelNumber;
                    lmsConfig.FullName = model.FullName;
                    lmsConfig.Email = model.Email;
                    lmsConfig.Organization = model.Organization;
                    lmsConfig.Position = model.Position;

                    Config.Set(config);
                    Config.Set(lmsConfig);

                    uow.Commit();
                }
                catch (Exception e)
                {
                     return new StatusCodeResultWithText((int)HttpStatusCode.BadRequest, e.Message);
                }
            }

            return null;
        }

        [HttpGet]
        public ActionResult ExportDebugPackage()
        {
            var memoryStream = _scormCourseFactory.CreateDebugPackage();
            const string contentType = "application/octet-stream";

            return File(memoryStream, contentType, "scorm_debug_package.zip");
        }
    }
}
