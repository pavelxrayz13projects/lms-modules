﻿using System.IO;
using System.Web.Mvc;
using Olimp.Configuration;
using Olimp.Core;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter;
using Olimp.LmsIntegration.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Web;

namespace Olimp.LmsIntegration.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Admin.LmsIntegration, Order = 2)]
    public class LmsIntegrationAppearanceController : PController
    {
        public ActionResult Index()
        {
            using (UnitOfWork.CreateRead())
            {
                var config = Config.Get<OlimpConfiguration>();
                return View(new LmsIntegrationAppearancуViewModel
                {
                    LmsCss = System.Web.HttpUtility.JavaScriptStringEncode(config.LmsCss),
                    AppliesToWholeSystem = config.AppliesLmsCssToWholeSystem
                });
            }
        }

        [HttpPost]
        public ActionResult Save(string lmsCss, bool appliesToWholeSystem)
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var config = Config.Get<OlimpConfiguration>();
                config.LmsCss = lmsCss;
                config.AppliesLmsCssToWholeSystem = appliesToWholeSystem;
                Config.Set(config);
                uow.Commit();
            }

            return null;
        }
        
        [AllowAnonymous]
        public ActionResult ShowPicture(string fileName)
        {
            var path = Path.Combine(OlimpPaths.Default.LmsIntegrationImages, fileName);
            if (!System.IO.File.Exists(path))
                return new HttpStatusCodeResult(404, "Not found");
            var file =  System.IO.File.OpenRead(path);
            Response.AddHeader("Content-Disposition", "inline; filename=" + fileName);
            return File(file, MIME.ByPath(path));
        }
    }
}
