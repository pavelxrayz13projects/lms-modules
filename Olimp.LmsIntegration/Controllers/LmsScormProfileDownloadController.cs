﻿using System;
using System.Linq;
using System.Web.Mvc;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.LmsIntegration.ScormCourses;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Nodes;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;

namespace Olimp.LmsIntegration.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Admin.LmsIntegration, Order = 2)]
    public class LmsScormProfileDownloadController : PController
    {
        private readonly IScormCourseFactory _scormCourseFactory;
        private readonly IOlimpLinkProvider _olimpLinkProvider;

        public LmsScormProfileDownloadController(
            IScormCourseFactory scormCourseFactory,
            IOlimpLinkProvider olimpLinkProvider)
        {

            if (scormCourseFactory == null)
                throw new ArgumentNullException("scormCourseFactory");

            if (olimpLinkProvider == null)
                throw new ArgumentNullException("olimpLinkProvider");

            _scormCourseFactory = scormCourseFactory;
            _olimpLinkProvider = olimpLinkProvider;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetProfiles(TableViewModel model)
        {
            using (UnitOfWork.CreateRead())
            {
                return Json(
                    Repository.Of<Profile>().GetTableData(model, p => new
                    {
                        p.Id, 
                        p.Name, 
                        p.AlternativeName, 
                        ExamArea = p.ExamAreas != null ? String.Join(", ", p.ExamAreas) : "", 
                        p.Periodicity
                    }));
            }
        }

        [HttpGet]
        public ActionResult ExportProfile(int profileId)
        {
            var profile = Repository.Of<Profile>().GetById(profileId);

            var scormCourseTitle = !string.IsNullOrWhiteSpace(profile.AlternativeName)
                ? profile.AlternativeName
                : profile.Name;

            var olimpLink = _olimpLinkProvider.Get(Request.Url.Host);

            var memoryStream = _scormCourseFactory.Create(olimpLink, profileId, scormCourseTitle);

            const string contentType = "application/octet-stream";
            var fileDownloandName = string.Concat("scorm_profile_", profileId, ".zip");

            return File(memoryStream, contentType, fileDownloandName);
        }
    }
}
