using System;
using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Domain.LearningCenter;
using Olimp.LmsIntegration.Controllers;
using Olimp.LmsIntegration.ScormCourses;
using Olimp.Web.Controllers.Nodes;
using PAPI.Core;
using PAPI.Core.Initialization;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.LmsIntegration.Plugin))]

namespace Olimp.LmsIntegration
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(LmsIntegrationSettingsController),
                typeof(LmsIntegrationAppearanceController));

            areas["Admin"].RegisterControllers(
                typeof(LmsScormProfileDownloadController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization)) 
                return;

            var olimpModule = PContext.ExecutingModule as LearningCenterModule;
            if (olimpModule == null)
                throw new InvalidOperationException("Olimp.Main only compatible with LearningCenterModule");

            var olimpLinkProvider =
                new DefaultOlimpLinkProvider(
                    olimpModule.Context.NodeData,
                    olimpModule.LocalCluster.GetStartupRegistry());

            var scormCourseFactory = new DefaultScormCourseFactory(new DefaultScormCourseZipEntriesFactory());

            binder.Controller<LmsIntegrationSettingsController>(b => b
                  .Bind<IScormCourseFactory>(() => scormCourseFactory));

            binder.Controller<LmsScormProfileDownloadController>(b => b
                .Bind<IScormCourseFactory>(() => scormCourseFactory)
                .Bind<IOlimpLinkProvider>(() => olimpLinkProvider));
        }
    }
}