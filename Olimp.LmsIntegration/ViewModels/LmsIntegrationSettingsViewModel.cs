﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Olimp.Core.ComponentModel;

namespace Olimp.LmsIntegration.ViewModels
{
    public class LmsIntegrationSettingsViewModel
    {
        public IEnumerable<SelectListItem> Groups { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LmsIntegration), DisplayNameResourceName = "LmsGroup")]
        public int? GroupId { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LmsIntegration), DisplayNameResourceName = "LmsScormMappingsLogin")]
        public string Login { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LmsIntegration), DisplayNameResourceName = "LmsScormMappingsPersonnelNumber")]
        public string PersonnelNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LmsIntegration), DisplayNameResourceName = "LmsScormMappingsFullName")]
        public string FullName { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LmsIntegration), DisplayNameResourceName = "LmsScormMappingsEmail")]
        public string Email { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LmsIntegration), DisplayNameResourceName = "LmsScormMappingsOrganization")]
        public string Organization { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LmsIntegration), DisplayNameResourceName = "LmsScormMappingsPosition")]
        public string Position { get; set; }
    }
}