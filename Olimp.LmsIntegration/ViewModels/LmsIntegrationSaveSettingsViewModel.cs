﻿namespace Olimp.LmsIntegration.ViewModels
{
    public class LmsIntegrationSaveSettingsViewModel
    {
        public int GroupId { get; set; }
        public string Login { get; set; }
        public string PersonnelNumber { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
    }
}