﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/LmsIntegration.master" Inherits="Olimp.Core.Mvc.OlimpViewPage<LmsIntegrationAppearancуViewModel>" %>
<%@ Import Namespace="I18N = Olimp.LmsIntegration.I18N" %>
<%@ Import Namespace="Olimp.I18N" %>

<asp:Content ID="Content" ContentPlaceHolderID="Olimp_LmsIntegration_Content" runat="server">
    <% Html.EnableClientValidation(); %>

    <h1><%= I18N.LmsIntegration.Appearance %></h1>

    <% using (Html.BeginForm("Save", "CompetitionAppearance", FormMethod.Post, new {Id = "lms-appearance-form"}))
       { %>
        <div class='block'>
            <table class="form-table">
                <tr class="first">
                    <th><%=Olimp.I18N.Domain.Config.AppliesLmsCssToWholeSystem %></th>
                    <td class="checkbox">
                        <%= Html.CheckBoxFor(m => m.AppliesToWholeSystem, new {data_bind = "checked: appliesToWholeSystem"}) %>
                    </td>
                </tr>
                <tr class="last">
                    <th><%=Olimp.I18N.Domain.Config.LmsCss %></th>
                    <td>
                        <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.LmsCss) %></div>
                        <%= Html.TextAreaFor(m => m.LmsCss, new {data_bind = "value: lmsCss", style = "height:200px;"}) %>
                    </td>
                </tr>
            </table>
        </div>

        <button id="save" type="button" data-bind="click: saveData"><%= PlatformShared.Save %></button>
    <% } %>

    <script type="text/javascript">
        $(function() {
            $("button").olimpbutton();

            var viewModel = {
                lmsCss: ko.observable("<%= Model.LmsCss %>"),
                appliesToWholeSystem: ko.observable("<%= Model.AppliesToWholeSystem %>" === 'True' ? true : false),

                saveData: function () {
                    var data = { lmsCss: this.lmsCss(), appliesToWholeSystem: this.appliesToWholeSystem() };
                    $.post("<%= Url.Action("Save") %>", data, function () {
                        $.Olimp.showSuccess("<%=I18N.LmsIntegration.SettingsSaved%>");
                    });
                }
            };

            ko.applyBindings(viewModel, $('#lms-appearance-form')[0]);
        });
    </script>
</asp:Content>