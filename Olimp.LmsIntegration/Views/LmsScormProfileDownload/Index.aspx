﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/LmsIntegration.master" %>
<%@ Import Namespace="Olimp.I18N" %>
<%@ Import Namespace="I18N=Olimp.LmsIntegration.I18N" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Olimp_LmsIntegration_Content" runat="server">

    <h1><%= I18N.LmsIntegration.ProfileExtractInScormFormat %></h1>
    <div class="block">
        <% Html.Table(
               new TableOptions
               {
                   SelectUrl = Url.Action("GetProfiles"),
                   DefaultSortColumn = "Name",
                   Columns = new[]
                   {
                       new ColumnOptions("Name", Olimp.I18N.Domain.Profile.Name),
                       new ColumnOptions("AlternativeName", Olimp.I18N.Domain.Profile.AlternativeName),
                       new ColumnOptions("ExamArea", Olimp.I18N.Domain.Profile.ExamArea),
                       new ColumnOptions("Periodicity", Olimp.I18N.Domain.Profile.Periodicity)
                   },
                   Actions = new RowAction[]
                   {
                       new TemplateAction("export-scorm-package")
                   }
               }); %>
    </div>

    <script type="text/javascript">
        function exportProfileHref(profile) {
            return "<%= Url.Action("ExportProfile") %>" + "?profileid=" + profile.Id();
        }
    </script>
    
    <script id="export-scorm-package" type="text/html">
        <a href="#" class="icon icon-download action" title="<%=I18N.LmsIntegration.ExportProfileInScrorm_1_2 %>" 
           data-bind="attr: { href: exportProfileHref($data) }">
        </a>
    </script>

</asp:Content>
