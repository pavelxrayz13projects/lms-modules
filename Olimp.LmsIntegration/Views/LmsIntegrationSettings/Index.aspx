﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/LmsIntegration.master" Inherits="Olimp.Core.Mvc.OlimpViewPage<LmsIntegrationSettingsViewModel>" %>
<%@ Import Namespace="Olimp.I18N" %>
<%@ Import Namespace="I18N=Olimp.LmsIntegration.I18N" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Olimp_LmsIntegration_Content" runat="server">
    <% Html.EnableClientValidation(); %>
    <% using (Html.BeginForm("SaveSettings", "LmsInteGrationSettings", FormMethod.Post, new { id = "lms-settings-form", data_bind = "submit: submit"}))
       { %>
        <h1><%=I18N.LmsIntegration.LmsSettings %></h1>
        <div class="block">
            <table class="form-table">	
                <tr class="first">
                    <th><%= Html.LabelFor(m => m.GroupId) %></th>
                    <td>
                        <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.GroupId) %></div>
                        <%= Html.DropDownListFor(m => m.GroupId, Model.Groups, I18N.LmsIntegration.ChooseGroup, new Dictionary<string, object>
                            {
                                {"id", "group-combobox"}
                            }) %>
                    </td>
                </tr>
            </table>
        </div>

        <h1><%= I18N.LmsIntegration.LmsSettingsScormApiMappings %></h1>
        <div class="block">
            <table class="form-table">
                <tr>
                    <th><%= Html.LabelFor(m => m.Login) %></th>
                    <td>
                        <div><%= Html.TextBoxFor(m => m.Login) %></div>
                    </td>
                </tr>
                <tr>
                    <th><%= Html.LabelFor(m => m.PersonnelNumber) %></th>
                    <td>
                        <div><%= Html.TextBoxFor(m => m.PersonnelNumber) %></div>
                    </td>
                </tr>
                <tr>
                    <th><%= Html.LabelFor(m => m.FullName) %></th>
                    <td>
                        <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.FullName) %></div>
                        <div><%= Html.TextBoxFor(m => m.FullName) %></div>
                    </td>
                </tr>
                <tr>
                    <th><%= Html.LabelFor(m => m.Email) %></th>
                    <td>
                        <div><%= Html.TextBoxFor(m => m.Email) %></div>
                    </td>
                </tr>
                <tr>
                    <th><%= Html.LabelFor(m => m.Organization) %></th>
                    <td>
                        <div><%= Html.TextBoxFor(m => m.Organization) %></div>
                    </td>
                </tr>
                <tr>
                    <th><%= Html.LabelFor(m => m.Position) %></th>
                    <td>
                        <div><%= Html.TextBoxFor(m => m.Position) %></div>
                    </td>
                </tr>
                <tr class="last">
                    <td colspan="2">
                        <button id="save-settings" type="submit"><%= PlatformShared.Save %></button>
                        <a id="export-debug-package" href="<%= Url.Action("ExportDebugPackage") %>"><%=I18N.LmsIntegration.ExportDebugPackage %></a>
                    </td>
                </tr>
            </table>
        </div>
    <% } %>

    <script type="text/javascript">
        $(function() {
            $("#save-settings, #export-debug-package").olimpbutton();
            $("#group-combobox").combobox();

            var successMessage = "<%= I18N.LmsIntegration.SettingsSaved %>";
            var settingsForm = $('#lms-settings-form');
            settingsForm.validate().settings.ignore = "";
            var viewModel = {
                submit: function() {
                    if (settingsForm.valid()) {
                        $.post(
                            settingsForm.attr("action"),
                            settingsForm.serialize(),
                            function() {
                                $.Olimp.showSuccess(successMessage);
                            }
                        );
                    }
                    return false;
                }
            };
            ko.applyBindings(viewModel, settingsForm[0]);
        });
    </script>
</asp:Content>