using System.Collections.Generic;
using Olimp.Core.Mvc.Security;
using Olimp.Documents.ViewModels;
using Olimp.Domain.Catalogue.OrmLite.Content;
using Olimp.Domain.Exam.Auth;
using Olimp.Prepare.Controllers;
using Olimp.Web.Controllers.Users;
using PAPI.Core.Web;
using System;
using System.Web.Mvc;

namespace Olimp.Documents.Controllers
{
    [AllowByConfiguration(FieldName = "AllowExamPreparation")]
    [CheckActivation]
    [LimitUsers]
    public class DocumentsController : PrepareUserController
    {
        private readonly OrmLiteDocumentStreamerProvider _streamerProvider;
        private readonly ISimpleWebAuthProvider _authProvider;

        public DocumentsController(
            OrmLiteDocumentStreamerProvider streamerProvider, 
            ISimpleWebAuthProvider authProvider)
        {
            if (streamerProvider == null)
                throw new ArgumentNullException("streamerProvider");

            if (authProvider == null)
                throw new ArgumentNullException("authProvider");

            _streamerProvider = streamerProvider;
            _authProvider = authProvider;
        }

        public ActionResult Index(int courseId, int version, Guid materialId)
        {
            if (!Request.RawUrl.EndsWith("/"))
                return Redirect(Request.RawUrl + "/");

            if (!IsAuthorized(null, _authProvider))
            {
                return RedirectToAuthUser("Index", "Documents", new Dictionary<string, object>
                {
                    {"courseId", courseId}, {"version", version}, {"materialId", materialId}
                });
            }

            return View(new MaterialViewModel {CourseId = courseId, MaterialId = materialId, Version = version});
        }

        [ChildActionOnly]
        public ActionResult Document(MaterialViewModel model)
        {
            var doc = _streamerProvider.GetStreamer(model.CourseId, model.MaterialId, model.Version);
            if (doc == null)
                return new HttpNotFoundResult();

            var stream = doc.OpenBody();
            return File(stream, "text/html");
        }

        [ChildActionOnly]
        public ActionResult Heading(MaterialViewModel model)
        {
            var doc = _streamerProvider.GetStreamer(model.CourseId, model.MaterialId, model.Version);
            if (doc == null)
                return new HttpNotFoundResult();

            var stream = doc.OpenHeading();
            return File(stream, "text/html");
        }

        public ActionResult Image(int courseId, int version, Guid materialId, string path)
        {
            if (!IsAuthorized(null, _authProvider))
            {
                return RedirectToAuthUser("Image", "Documents", new Dictionary<string, object>
                {
                    {"courseId", courseId}, {"version", version}, {"materialId", materialId}, {"path", path }
                });
            }

            var doc = _streamerProvider.GetStreamer(courseId, materialId, version);
            if (doc == null)
                return new HttpNotFoundResult();

            var stream = doc.OpenImage(path);
            return File(stream, MIME.ByPath(path));
        }
    }
}