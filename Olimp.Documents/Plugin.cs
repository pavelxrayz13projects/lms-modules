using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Documents.Controllers;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Content;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Exam.Auth;
using Olimp.Domain.Exam.NHibernate.Auth;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Documents.Plugin))]

namespace Olimp.Documents
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
        }

        static Plugin()
        {
            AuthCookie = new AuthCookieRegistration("prepareCookie", "/Prepare", 1);
            AuthCookieManager.RegisterCookie(typeof(Plugin), AuthCookie);
        }

        internal static AuthCookieRegistration AuthCookie { get; private set; }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Prepare"].RegisterControllers(
                typeof(DocumentsController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var module = PContext.ExecutingModule as OlimpMaterialsModule;
            if (module == null)
                throw new InvalidOperationException("Can be used only with OlimpMaterialsModule");

            var connectionFactoryFactory = new SqliteConnectionFactoryFactory();
            var connectionFactory = connectionFactoryFactory.Create();

            var nodesRepository = new OrmLiteNodeRepository(connectionFactory);

            var courseRepository = new OrmLiteCourseRepository(
                module.Context.NodeData, 
                connectionFactory, 
                module.Materials.CourseRepository, 
                nodesRepository);

            var streamerProvider = new OrmLiteDocumentStreamerProvider(
                courseRepository, 
                module.Materials.CourseRepository);

            var webAuthProvider = new NHibernateSimpleWebAuthProvider(
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.GetSessionManager());

            binder.Controller<DocumentsController>(b => b
                .Bind<OrmLiteDocumentStreamerProvider>(() => streamerProvider)
                .Bind<ISimpleWebAuthProvider>(() => webAuthProvider));
        }
    }
}