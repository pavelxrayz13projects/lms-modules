﻿<%@ Page Title="" Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<MaterialViewModel>" MasterPageFile="~/Views/Shared/Layout.master" %>
<%@ Import Namespace="I18N=Olimp.Documents.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Sidebar" runat="server">
	
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Status" runat="server">
		<% Html.RenderPartial("PrepareCloseWindow"); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Navigation" runat="server">
	<a href="#" data-bind="attr: { 'class': documentClass }, click: displayDocument"><%= I18N.Documents.Body %></a>
	<a href="#" data-bind="attr: { 'class': headingClass }, click: displayHeading"><%= I18N.Documents.Contents %></a>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">		
	<div data-bind="visible: showDocument">
		<% Html.RenderAction("Document", new { CourseId = Model.CourseId, MaterialId = Model.MaterialId, Version = Model.Version }); %>
	</div>	
	<div class="document-heading" style="display: none" data-bind="visible: showHeading">
		<% Html.RenderAction("Heading", new { CourseId = Model.CourseId, MaterialId = Model.MaterialId, Version = Model.Version }); %>
	</div>
	
	<script type="text/javascript">
	$(function () {	
		var model = { showDocument: ko.observable(true) };
		
		model.displayDocument = function() { model.showDocument(true) };
		model.displayHeading = function() { model.showDocument(false) };
		
		model.showHeading = ko.computed(function() { return !this.showDocument() }, model);
		model.documentClass = ko.computed(function() { return this.showDocument() ? 'active' : '' }, model)
		model.headingClass = ko.computed(function() { return this.showHeading() ? 'active' : '' }, model)
		
		$('.document-heading a').click(function() { model.showDocument(true); return true; });
		
		ko.applyBindings(model);
	})
	</script>
</asp:Content>	