using System;

namespace Olimp.Documents.ViewModels
{
    public class MaterialViewModel
    {
        public int CourseId { get; set; }

        public Guid MaterialId { get; set; }

        public int Version { get; set; }
    }
}