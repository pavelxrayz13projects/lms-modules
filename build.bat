rem usage:
rem build.bat MSBuildProjectFile MSBuildOptions
rem   MSBuildProjectFile - MSBuild xml project file to build
rem   MSBuildOptions - Additional options that is passed to MSBuild
rem example:
rem build.bat Olimp.AutoBuild.Deploy.xml "/p:Revision=%SVN_REVISION%"
rem BuildAll - default target to build
rem Some options:
rem "/p:PapiSdkPath=<system path>" - Use specified PAPISDK path 
rem "/p:UpdatePlatformLibsFromPAPISDKFolder=True" - Update Platform libs in local 'Libs' folder from PAPISDK
rem "/p:CopyLicense=True" - Copy Olimp.aat and Olimp.edi files to output Olimp module folder - for testing purposes only
rem "/p:PlatformInstallerFile=<system path>" - Use specified PAPISDK Installer path 
rem "/p:RunSchemaGeneratorOnDeploy=True" - Run UtilityModules.SchemaGenerator on Deploy stage

set VSTOOLS="%VS120COMNTOOLS%"
for /F "usebackq" %%i in (`echo "%PATH%" ^| find /I /C "%WINDIR%\Microsoft.NET\Framework\v4.0.30319"`) do set FOUNDMS40=%%i
if %VSTOOLS%=="" (
  if "%FOUNDMS40%"=="0" (set "PATH=%PATH%;%WINDIR%\Microsoft.NET\Framework\v4.0.30319")
  goto skipvsinit
)

call "%VSTOOLS:~1,-1%vsvars32.bat"
if errorlevel 1 goto end

:skipvsinit

msbuild.exe "%~1" %~2 %~3 %~4 %~5 %~6 %~7 %~8 %~9

if errorlevel 1 goto end

:end
exit /b %ERRORLEVEL%
