using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.SystemMonitoring.Controllers;
using PAPI.Core;
using PAPI.Core.Initialization;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.SystemMonitoring.Plugin))]

namespace Olimp.SystemMonitoring
{
    public class Plugin : PluginModule
    {
        public static bool LinkVisible
        {
            get
            {
#if DEBUG
        return true;
#else
                return false;
#endif
            }
        }

        private IModuleContext _context;

        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
            _context = context;
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
              typeof(SystemMonitoringController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (_context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;
        }
    }
}