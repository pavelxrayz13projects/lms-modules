﻿using Olimp.Domain.Catalogue.Security;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using ServiceStack.ServiceClient.Web;
using ServiceStack.ServiceInterface.Admin;
using System;
using System.Web.Mvc;

namespace Olimp.SystemMonitoring.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.LocalCluster.Nodes.Manage + "," + Permissions.LocalCluster.Nodes.StartStop, Order = 2)]
    public class SystemMonitoringController : PController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetApiRequests(TableViewModel model)
        {
            using (JsonServiceClient client = new JsonServiceClient(
              String.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~/api/"))))
            {
                var res = client.Post<RequestLogsResponse>("debug/log", new RequestLogs());
                return Json(res.Results.GetTableData(model, r => new
                {
                    Date = r.DateTime.ToString(),
                    Path = r.PathInfo,
                    Request = r.RequestDto,
                    Reply = r.ResponseDto
                }));
            }
        }
    }
}