﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage"  MasterPageFile="~/Views/Shared/SystemMonitoring.master" %>
<asp:Content ContentPlaceHolderID="Olimp_SystemMonitoring_Js" runat="server">
	<script type="text/javascript">
		function reloadLog() {
			$('#requests-log').data('viewModel').reload();
		}
	</script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Olimp_SystemMonitoring_Content" runat="server">
	<h1>Недавние запросы</h1>
	<div class="block">	
		<% Html.Table(
			new TableOptions {
				SelectUrl = Url.Action("GetApiRequests"),
				PagingDefaultSize = 50,
				Checkboxes = false,
				Columns = new[] { 
					new ColumnOptions("Date"),
					new ColumnOptions("Path"),
					new ColumnOptions("Request"),
					new ColumnOptions("Reply")
				},
				TableActions = new TableAction[]
				{
					new CustomTableAction("reloadLog", "", "Обновить", null)
				}
			}, new { id = "requests-log" }); %>
	</div>
</asp:Content>