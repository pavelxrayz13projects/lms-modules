using log4net;
using Microsoft.Win32;
using Olimp.Core;
using Olimp.LocalNodes.Core.Services.Clients;
using PAPI.Core;
using PAPI.Core.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace DefaultNode
{
    public class Runner : IModuleRunner
    {
        private static ILog _log = PAPI.Core.Logging.Log.CreateLogger("default-node");

        protected ILog Log
        {
            get { return _log; }
        }

        [DllImport("kernel32.dll")]
        private static extern bool CreateProcess(
            string lpApplicationName,
            string lpCommandLine,
            IntPtr lpProcessAttributes,
            IntPtr lpThreadAttributes,
            bool bInheritHandles,
            uint dwCreationFlags,
            IntPtr lpEnvironment,
            string lpCurrentDirectory,
            ref STARTUPINFO lpStartupInfo,
            out PROCESS_INFORMATION lpProcessInformation);

        private struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public uint dwProcessId;
            public uint dwThreadId;
        }

        private struct STARTUPINFO
        {
            public uint cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public uint dwX;
            public uint dwY;
            public uint dwXSize;
            public uint dwYSize;
            public uint dwXCountChars;
            public uint dwYCountChars;
            public uint dwFillAttribute;
            public uint dwFlags;
            public short wShowWindow;
            public short cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        private string GetLaunchCommandFromRegistry(string url)
        {
            if (String.IsNullOrWhiteSpace(url))
                throw new ArgumentException("'url' is null or empty string");

            if (!Platform.IsWindows)
            {
                Log.Warn("This method is not intended to run on non-Windows OS.");
                return null;
            }

            string pattern = null;
            string registryKey = null;
            try
            {
                registryKey = @"Software\Classes\http\shell\open\command";
                var httpShelLCommand = Registry.CurrentUser.OpenSubKey(registryKey);
                pattern = Convert.ToString(httpShelLCommand.GetValue(null));
            }
            catch (Exception e)
            {
                Log.WarnFormat(@"Error occurs while trying to get default value of registry key 'HKCU\{0}': {1}", registryKey, e.ToString());
            }

            if (string.IsNullOrEmpty(pattern))
            {
                try
                {
                    registryKey = @"HTTP\shell\open\command";
                    var httpShelLCommand = Registry.ClassesRoot.OpenSubKey(registryKey);
                    pattern = Convert.ToString(httpShelLCommand.GetValue(null));
                }
                catch (Exception e)
                {
                    Log.ErrorFormat(@"Error trying to get default value of registry key 'HKCR\{0}': {1}", registryKey, e.ToString());
                }
            }

            if (string.IsNullOrEmpty(pattern))
            {
                Log.Warn("Wasn't able to obtain launch command from registry");
                return null;
            }

            return pattern.Contains("%1")
              ? pattern.Replace("%1", url)
              : String.Format("{0} \"{1}\"", pattern, url);
        }

        private string GetNodeUrlFromNodeService(string nodeName)
        {
            var sc = new DefaultLocalNodeControllerServiceClient(OlimpPaths.OlimpClusterServiceUri);

            var nodes = sc.GetAllNodes().Where(n => n.IsAlive);
            var node =
                nodes.FirstOrDefault(n => n.Node.Name.Equals(nodeName, StringComparison.OrdinalIgnoreCase)) ??
                nodes.FirstOrDefault(n => n.Node.Name.Equals(Platform.DefaultNode.Name, StringComparison.OrdinalIgnoreCase)) ??
                nodes.FirstOrDefault();

            if (node == null)
            {
                throw new ApplicationException(DefaultNode.I18N.Messages.NothingRunning);
            }

            return String.Format("http://localhost:{0}", node.Node.Port);
        }

        private void StartSystemProcess(string launchCommand)
        {
            if (String.IsNullOrWhiteSpace(launchCommand))
                throw new ArgumentException("'launchCommand' is null or empty string");

            var startupInfo = new STARTUPINFO();
            var procInfo = new PROCESS_INFORMATION();
            CreateProcess(
                null,
                launchCommand,
                IntPtr.Zero,
                IntPtr.Zero,
                false,
                0,
                IntPtr.Zero,
                null,
                ref startupInfo,
                out procInfo);
        }

        public void Run(IModule module, IDictionary<string, string> moduleParams)
        {
            Thread.CurrentThread.CurrentCulture =
              Thread.CurrentThread.CurrentUICulture =
                new CultureInfo("ru-RU");

            string nodeName;
            if (!moduleParams.TryGetValue("nodeName", out nodeName))
                nodeName = Platform.DefaultNode.Name;

            string url;
            if (!moduleParams.TryGetValue("url", out url))
            {
                try
                {
                    url = GetNodeUrlFromNodeService(nodeName);
                }
                catch (Exception e)
                {
                    Log.Error(e.ToString());
                    MessageBox.Show(DefaultNode.I18N.Messages.Unavailable);
                    throw new ApplicationException(DefaultNode.I18N.Messages.Unavailable, e);
                }
            }

            try
            {
                Process.Start(new ProcessStartInfo
                {
                    FileName = url,
                    UseShellExecute = true
                });
                return;
            }
            catch (Exception e)
            {
                Log.WarnFormat("Failed to open url '{0}' with shell execute: {1}", url, e.ToString());
                // Let's try other options below...
            }

            // Try other options
            var launchCommand = GetLaunchCommandFromRegistry(url);
            if (!string.IsNullOrEmpty(launchCommand))
            {
                Log.InfoFormat("Starting system process using launch command from registry: '{0}'", launchCommand);
                try
                {
                    StartSystemProcess(launchCommand);
                    return;
                }
                catch (Exception e)
                {
                    Log.WarnFormat("Failed to start system process using launch command from registry ({0}):\n{1}", launchCommand, e.ToString());
                }
            }

            // The last option to try
            try
            {
                Log.InfoFormat("Trying to use iexplore.exe to open url '{0}'.", url);
                Process.Start(new ProcessStartInfo
                {
                    FileName = "iexplore.exe",
                    Arguments = url,
                    UseShellExecute = true
                });
                return;
            }
            catch (Exception e)
            {
                Log.WarnFormat("Failed to use iexplore.exe to open url '{0}':\n{1}", url, e.ToString());
            }

            MessageBox.Show(String.Format(DefaultNode.I18N.Messages.CantRunBrowser, url));
            throw new ApplicationException(String.Format(DefaultNode.I18N.Messages.CantRunBrowser, url));
        }
    }
}