using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Debug.Controllers;
using PAPI.Core;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Debug.Plugin))]

namespace Olimp.Debug
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        { }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            if (!this.Context.Flags.HasFlag(ModuleLoadFlags.LoadInDebugMode))
                return;

            areas["Debug"].RegisterControllers(
                typeof(DebugHeadersController));

            areas["Debug"].Context.MapRoute("Debug_Request_Headers",
                "Debug/Headers/{*path}", new { controller = "DebugHeaders", action = "Index", path = "" });
        }
    }
}