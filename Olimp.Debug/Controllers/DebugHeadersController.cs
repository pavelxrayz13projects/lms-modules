﻿using Olimp.Debug.ViewModels;
using Olimp.Web.Controllers.Base;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Olimp.Debug.Controllers
{
    public class DebugHeadersController : PController
    {
        public ActionResult Index(string path)
        {
            var headers = new Dictionary<string, string>();
            foreach (var key in Request.Headers.AllKeys)
                headers[key] = Request.Headers[key];

            return View(new IndexViewModel
            {
                Path = path,
                Headers = headers
            });
        }
    }
}