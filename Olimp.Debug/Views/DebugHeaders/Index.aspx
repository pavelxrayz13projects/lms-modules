﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IndexViewModel>" %>

<html>
<head></head>
<body>
    <h1><%= Model.Path %></h1>

    <table>
    <% foreach(var pair in Model.Headers) { %>
        <tr>
            <th style="text-align:left; padding-right: 20px;"><%= pair.Key %></th>
            <td><%= pair.Value %></td>
        </tr>
    <% } %>
    </table>
</body>
</html>
