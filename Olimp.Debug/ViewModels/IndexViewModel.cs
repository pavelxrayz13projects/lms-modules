﻿using System.Collections.Generic;

namespace Olimp.Debug.ViewModels
{
    public class IndexViewModel
    {
        public string Path { get; set; }

        public IDictionary<string, string> Headers { get; set; }
    }
}