using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.Content;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Catalogue;
using Olimp.Domain.Catalogue.OrmLite.Content;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Common.Questions;
using Olimp.Domain.Exam.Attempts;
using Olimp.Domain.Exam.Auth;
using Olimp.Domain.Exam.Entities;
using Olimp.Domain.Exam.NHibernate;
using Olimp.Domain.Exam.NHibernate.Attempts;
using Olimp.Domain.Exam.NHibernate.Auth;
using Olimp.Domain.Exam.NHibernate.Catalogue;
using Olimp.Domain.Exam.NHibernate.ProfileResults;
using Olimp.Domain.Exam.NHibernate.Tickets;
using Olimp.Domain.Exam.Tickets;
using Olimp.Prepare.Controllers;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.Web.Routing;

[assembly: PAPI.Core.Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Prepare.Plugin))]

namespace Olimp.Prepare
{
    public class Plugin : PluginModule
    {
        public static bool DisplayEmployeeName { get; set; }

        public static string AuthorizationCookiePath
        {
            get { return AuthCookie.Path; }
            set { AuthCookie.Path = value; }
        }

        public static string AuthorizationCookieName
        {
            get { return AuthCookie.Name; }
            set { AuthCookie.Name = value; }
        }

        internal static AuthCookieRegistration AuthCookie { get; private set; }

        static Plugin()
        {
            DisplayEmployeeName = true;

            AuthCookie = new AuthCookieRegistration("prepareCookie", "/Prepare", 1);
            AuthCookieManager.RegisterCookie(typeof(Plugin), AuthCookie);
        }

        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        { }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Prepare"].RegisterControllers(
                typeof(ContentController),
                typeof(ExamingController),
                typeof(StHomeController),
                typeof(ExpressTestOnCourseController),
                typeof(ExpressTestOnTopicController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (this.Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var materialsModule = PContext.ExecutingModule as OlimpMaterialsModule;
            if (materialsModule == null)
                throw new InvalidOperationException("Executing module must be OlimpMaterialsModule");

            var facadeRepository = materialsModule.Materials.CourseRepository;

            var connFactoryFactory = new SqliteConnectionFactoryFactory();
            var connFactory = connFactoryFactory.Create();

            var treeBuilder = new DefaultCatalogueTreeBuilder();

            var branchesProvider = new NHibernateProfileBranchesProvider(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory);

            var catalogueProvider = new OrmLitePersonalCatalogueProvider(
                this.Context.NodeData, connFactory, treeBuilder, branchesProvider);

            var webAuthProvider = new NHibernateSimpleWebAuthProvider(
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.GetSessionManager());

            binder.Controller<StHomeController>(b => b
                .Bind<ISimpleWebAuthProvider>(() => webAuthProvider)
                .Bind<IPersonalCatalogueProvider>(() => catalogueProvider));

            var topicStatesProvider = new NHibernatePrepareTopicStatesProvider(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory);

            var nodesRepository = new OrmLiteNodeRepository(connFactory);
            var courseRepository = new OrmLiteCourseRepository(this.Context.NodeData, connFactory, materialsModule.Materials.CourseRepository, nodesRepository);

            var topicsProviderFactory = new OrmLiteTopicProviderFactory(facadeRepository);

            var prepareCourseStateProvider = new OrmLitePrepareCourseStateProvider(
                topicStatesProvider, topicsProviderFactory);

            var topicContentsProvider = new OrmLiteTopicContentsProvider(courseRepository, materialsModule.Materials.CourseRepository);

            var questionsProviderFactory = new OrmLiteCourseQuestionProviderFactory(materialsModule.Materials.CourseRepository);

            var answerValidator = new SimpleAnswerValidator();

            binder.Controller<ContentController>(b => b
                .Bind<ISimpleWebAuthProvider>(() => webAuthProvider)
                .Bind<IPrepareCourseStateProvider>(() => prepareCourseStateProvider)
                .Bind<ITopicContentsProvider>(() => topicContentsProvider)
                .Bind<ICourseRepository>(() => courseRepository)
                .Bind<ITopicsProviderFactory>(() => topicsProviderFactory)
                .Bind<ICourseQuestionProviderFactory>(() => questionsProviderFactory)
                .Bind<IAnswerValidator>(() => answerValidator));

            var shuffler = new DefaultShuffler();
            var taskNavigator = new NHibernateAttemptTasksNavigator(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                shuffler);

            var materialCodesWithExamAreasProvider = new MaterialCodesWithExamAreasProvider(connFactory);

            var profileExamAreaProvider = new NHibernateProfileExamAreaProvider(
                PContext.ExecutingModule.Database.RepositoryFactory);

            var attemptFactory = new NHibernateAttemptFactory(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                PContext.ExecutingModule.Database.GetSessionManager(),
                courseRepository,
                materialsModule.Materials.CourseRepository,
                shuffler,
                materialCodesWithExamAreasProvider,
                profileExamAreaProvider);

            var ticketInfoProvider = new NHibernateTicketInfoProvider(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory);

            var assessor = new DefaultExamAssessor();
            var attemptWriter = new NHibernateAttemptAnswerWriter(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                answerValidator);

            var attemptFinalizer = new NHibernatePrepareExamAttemptFinalizer(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                nodesRepository,
                assessor);

            binder.Controller<ExamingController>(b => b
                .Bind<IAttemptTasksNavigator>(() => taskNavigator)
                .Bind<IAttemptFactory>(() => attemptFactory)
                .Bind<ITicketInfoProvider>(() => ticketInfoProvider)
                .Bind<IAttemptAnswerWriter>(() => attemptWriter)
                .Bind<IAttemptFinalizer<PrepareExamAttempt>>(() => attemptFinalizer)
                .Bind<ISimpleWebAuthProvider>(() => webAuthProvider));

            var prepareOnTopicTaskNavigator = new NHibernatePrepareOnTopicAttemptTasksNavigator(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                shuffler);

            var expressOnTopicFinalizer = new NHibernatePrepareOnTopicAttemptFinalizer(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                nodesRepository,
                assessor);

            binder.Controller<ExpressTestOnTopicController>(b => b
               .Bind<IAttemptTasksNavigator>(() => prepareOnTopicTaskNavigator)
               .Bind<IAttemptFactory>(() => attemptFactory)
               .Bind<IAttemptAnswerWriter>(() => attemptWriter)
               .Bind<IAttemptFinalizer<PrepareOnTopicAttempt>>(() => expressOnTopicFinalizer));

            var prepareOnCourseTaskNavigator = new NHibernatePrepareOnCourseAttemptTasksNavigator(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                shuffler,
                materialsModule.Materials.CourseRepository);

            var prepareOnCourseAnswerWriter = new NHibernatePrepareOnCourseAttemptAnswerWriter(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                answerValidator,
                materialsModule.Materials.CourseRepository);

            var prepareOnCourseFinalizer = new NHibernatePrepareOnCourseAttemptFinalizer(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                nodesRepository,
                assessor);

            binder.Controller<ExpressTestOnCourseController>(b => b
                .Bind<IAttemptTasksNavigator>(() => prepareOnCourseTaskNavigator)
                .Bind<IAttemptFactory>(() => attemptFactory)
                .Bind<IAttemptAnswerWriter>(() => prepareOnCourseAnswerWriter)
                .Bind<IAttemptFinalizer<PrepareOnCourseAttempt>>(() => prepareOnCourseFinalizer)
                .Bind<ISimpleWebAuthProvider>(() => webAuthProvider));
        }
    }
}