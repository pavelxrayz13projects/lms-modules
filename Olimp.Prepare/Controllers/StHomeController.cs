using System.Net;
using Olimp.Core.Mvc.Security;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Exam.Auth;
using Olimp.Domain.Exam.Entities.Pseudo;
using Olimp.Prepare.ViewModels;
using Olimp.Web.Controllers.Learning.Exam;
using Olimp.Web.Controllers.Users;
using System;
using System.Web.Mvc;

namespace Olimp.Prepare.Controllers
{
    [CheckActivation(Order = 1)]
    [AllowByConfiguration(FieldName = "AllowExamPreparation", Order = 2)]
    public class StHomeController : PrepareUserController
    {
        private readonly ISimpleWebAuthProvider _authProvider;
        private readonly IPersonalCatalogueProvider _catalogueProvider;

        public StHomeController(IPersonalCatalogueProvider catalogueProvider, ISimpleWebAuthProvider authProvider)
        {
            if (catalogueProvider == null) throw new ArgumentNullException("catalogueProvider");
            if (authProvider == null) throw new ArgumentNullException("authProvider");

            _catalogueProvider = catalogueProvider;
            _authProvider = authProvider;
        }

        public ActionResult GetCourse(Guid employeeId, int? profileId)
        {
            return Json(profileId.GetValueOrDefault() == 0
                ? _catalogueProvider.GetPersonalPrepareCourses(employeeId)
                : _catalogueProvider.GetPersonalPrepareCourses(profileId.Value));
        }

        public override bool IsAuthorizing
        {
            get
            {
                var cookie = Plugin.AuthCookie.Get(Request);

                Guid employeeId;
                return cookie == null || !Guid.TryParse(cookie.Values.Get("employeeId") ?? "", out employeeId);
            }
        }

        public ActionResult Index(Guid? employeeId)
        {
            return EnterPreparation(employeeId, r =>
            {
                if (IntegrationContext["employeeId"] != employeeId.GetValueOrDefault().ToString())
                    ExitIntegrationMode();

                if (r.Employee == null)
                {
                    return r.IsAnonymous.GetValueOrDefault()
                        ? RedirectToAction("Anonymous", "AuthUser", new { area = "", returnUrl = Url.Action("Index") })
                        : RedirectToAction("User", "AuthUser", new { area = "", returnUrl = Url.Action("Index") });
                }

                return null;
            });
        }

        //Better to keep it POST
        //[HttpPost]
        public ActionResult BeginIntegration(string referer, Guid? employeeId)
        {
            if (!employeeId.HasValue)
            {
                return RedirectToAction("Anonymous", "AuthUser", new
                {
                    area = "",
                    returnUrl = Url.Action("BeginIntegration", new { referer })
                });
            }

            return EnterPreparation(employeeId, r =>
            {
                EnterIntegrationMode(referer, new
                {
                    IntegrationRoot = "Prepare/StHome/Index",
                    employeeId
                });

                return null;
            });
        }

        public ActionResult BeginScormIntegration(string referer, Guid? employeeId, int? profileId)
        {
            return EnterPreparation(
                employeeId,
                r =>
                {
                    EnterIntegrationMode(referer, new
                    {
                        IntegrationRoot = "Prepare/StHome/Index",
                        employeeId,
                        profileId
                    });

                    return null;
                });
        }

        [HttpPost]
        public override ActionResult ReturnToRefereingSystem()
        {
            _authProvider.Logout(Plugin.AuthCookie, ControllerContext.RequestContext);

            return base.ReturnToRefereingSystem();
        }

        [HttpPost]
        public ActionResult Logout()
        {
            _authProvider.Logout(Plugin.AuthCookie, ControllerContext.RequestContext);

            return RedirectToRoute("Default_FinishPreparation");
        }

        //TODO: ���������� ������������� ������ ����� ������ (profileId �������� ����� �� �����)
        [NonAction]
        private ActionResult EnterPreparation(Guid? employeeId, Func<SimpleAuthResult, ActionResult> processAuthResult = null)
        {
            var result = _authProvider.AuthorizeForPrepare(Plugin.AuthCookie, ControllerContext.RequestContext, employeeId);

            if (processAuthResult != null)
            {
                var actionResult = processAuthResult.Invoke(result);
                if (actionResult != null) return actionResult;
            }

            var profileId = ExamAndPrepareScormIntegrationHelper.GetProfileIdFromCookisOrUrl(Request);

            return View("Index", new StHomeIndexViewModel
            {
                Employee = result.Employee,
                ConcreteEmployeeId = result.ConcreteEmployeeId.Value,
                ProfileId = profileId
            });
        }
    }
}