using System.Collections.Generic;
using Olimp.Core.Mvc.Security;
using Olimp.Courses.Extensions;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Content;
using Olimp.Domain.Common.Prepare;
using Olimp.Domain.Common.Questions;
using Olimp.Domain.Exam.Auth;
using Olimp.Prepare.ViewModels;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Users;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Prepare.Controllers
{
    [CheckActivation(Order = 1)]
    [AllowByConfiguration(FieldName = "AllowExamPreparation", Order = 2)]
    [RequireWorkplaceToken(Order = 3)]
    [LimitUsers(Order = 4)]
    public class ContentController : PrepareUserController
    {
        private readonly ISimpleWebAuthProvider _authProvider;
        private readonly IPrepareCourseStateProvider _prepareCourseStateProvider;
        private readonly ITopicContentsProvider _topicContentsProvider;
        private readonly ICourseRepository _courseRepository;
        private readonly ITopicsProviderFactory _topicsProviderFactory;
        private readonly ICourseQuestionProviderFactory _questionProviderFactory;
        private readonly IAnswerValidator _answerValidator;

        public ContentController(
            ISimpleWebAuthProvider authProvider,
            IPrepareCourseStateProvider prepareCourseStateProvider,
            ITopicContentsProvider topicContentsProvider,
            ICourseRepository courseRepository,
            ITopicsProviderFactory topicsProviderFactory,
            ICourseQuestionProviderFactory questionProviderFactory,
            IAnswerValidator answerValidator)
        {
            if (authProvider == null)
                throw new ArgumentNullException("authProvider");

            if (prepareCourseStateProvider == null)
                throw new ArgumentNullException("prepareCourseStateProvider");

            if (topicContentsProvider == null)
                throw new ArgumentNullException("topicContentsProvider");

            if (courseRepository == null)
                throw new ArgumentNullException("courseRepository");

            if (topicsProviderFactory == null)
                throw new ArgumentNullException("topicsProviderFactory");

            if (questionProviderFactory == null)
                throw new ArgumentNullException("questionProviderFactory");

            if (answerValidator == null)
                throw new ArgumentNullException("answerValidator");

            _authProvider = authProvider;
            _prepareCourseStateProvider = prepareCourseStateProvider;
            _topicContentsProvider = topicContentsProvider;
            _courseRepository = courseRepository;
            _topicsProviderFactory = topicsProviderFactory;
            _questionProviderFactory = questionProviderFactory;
            _answerValidator = answerValidator;
        }

        [HttpPost]
        public ActionResult BeginIntegration(string referer, Guid employeeId, int materialId)
        {
           _authProvider.AuthorizeForPrepare(Plugin.AuthCookie, ControllerContext.RequestContext, employeeId);

            return EnterPreparation(employeeId, materialId, () => EnterIntegrationMode(referer, new
            {
                IntegrationRoot = "Prepare/Content/ShowThemes"
            }));
        }

        public ActionResult ShowThemes(Guid employeeId, int materialId)
        {
            if (!IsAuthorized(employeeId, _authProvider))
            {
                return RedirectToAuthUser("ShowThemes", "Content", new Dictionary<string, object>
                {
                    {"employeeId", employeeId}, {"materialId", materialId}
                });
            }

            return EnterPreparation(employeeId, materialId, () =>
            {
                if (IntegrationContext["IntegrationRoot"] != "Prepare/StHome/Index")
                    ExitIntegrationMode();
            });
        }

        public ActionResult ShowDocs(Guid employeeId, int materialId, Guid themeId)
        {
            if (!IsAuthorized(employeeId, _authProvider))
            {
                return RedirectToAuthUser("ShowDocs", "Content", new Dictionary<string, object>
                {
                    {"employeeId", employeeId}, {"materialId", materialId}, {"themeId", themeId}
                });
            }

            var course = _courseRepository.GetByBranchId(materialId);
            var topicProvider = _topicsProviderFactory.Create(course);
            var topic = topicProvider.GetById(themeId);

            var contents = _topicContentsProvider.GetContents(course, topic);

            var model = new ContentShowDocsViewModel
            {
                ConcreteEmployeeId = employeeId,
                MaterialBranchId = materialId,
                MaterialFullName = Convert.ToString(course),
                TopicName = topic.Name,
                TopicUniqueId = themeId,
                TopicDocs = contents.Documents,
                TopicScorms = contents.Scorms,
                TopicHasQuestions = topic.QuestionsCount > 0
            };

            return View(model);
        }

        public ActionResult ShowThemeQuestions(Guid employeeId, int materialId, Guid themeId)
        {
            if (!IsAuthorized(employeeId, _authProvider))
            {
                return RedirectToAuthUser("ShowThemeQuestions", "Content", new Dictionary<string, object>
                {
                    {"employeeId", employeeId}, {"materialId", materialId}, {"themeId", themeId}
                });
            }

            var course = _courseRepository.GetByBranchId(materialId);
            var topicProvider = _topicsProviderFactory.Create(course);
            var topic = topicProvider.GetById(themeId);

            var state = _prepareCourseStateProvider.GetTopicState(employeeId, course, topic);

            var model = new ContentShowTQuestionsViewModel
            {
                ConcreteEmployeeId = employeeId,
                MaterialBranchId = materialId,
                MaterialFullName = Convert.ToString(course),
                TopicHasMaterials = topic.ScormsCount + topic.DocumentsCount > 0,
                TopicName = topic.Name,
                TopicUniqueId = topic.UniqueId,
                Questions = state.Questions.GetTableData(new TableViewModel(), q => new
                {
                    Id = q.UniqueId,
                    HasHelp = q.Help.Length > 0,
                    q.Number,
                    Answered = q.State != PrepareState.Unkonwn,
                    Correct = q.State == PrepareState.Passed,
                    QuestionText = q.GetTextWithFixedCourseImageLinks(materialId)
                })
            };

            return View(model);
        }

        public ActionResult ShowQHelp(int materialId, Guid questionId, int number)
        {
            var course = _courseRepository.GetByBranchId(materialId);
            var questionsProvider = _questionProviderFactory.Create(course);
            var question = questionsProvider.GetById(questionId);

            return Content(question.Help, "text/html");
        }

        public ActionResult ShowQuestion(int materialId, Guid questionId)
        {
            var course = _courseRepository.GetByBranchId(materialId);
            var questionsProvider = _questionProviderFactory.Create(course);
            var question = questionsProvider.GetById(questionId);

            return PartialView(new ContentShowQuestionViewModel
            {
                MaterialId = materialId,
                QuestionId = question.UniqueId,
                Question = question
            });
        }

        public ActionResult CheckQuestion(ContentCheckQuestionFormViewModel result)
        {
            var course = _courseRepository.GetByBranchId(result.MaterialId);
            var questionsProvider = _questionProviderFactory.Create(course);
            var question = questionsProvider.GetById(result.QuestionId);

            var isCorrect = _answerValidator.Validate(question.Answers, result.Answers);
            var model = new ContentCheckQuestionViewModel
            {
                Correct = isCorrect,
                Help = question.Help
            };

            return PartialView(model);
        }

        //TODO: Refactor with MvcSiteMapProvider
        [ChildActionOnly]
        public ActionResult ShowBreadcrumb(BreadcrumbViewModel bc)
        {
            var model = new ContentShowBreadcrumbViewModel();
            model.Breadcrumbs.Add(new BreadcrumbStructure
            {
                Text = I18N.Prepare.SelectCourseForPrepare,
                Link = Url.Action("Index", "StHome", new { employeeId = bc.EmployeeId }),
                IsAvailable = true
            });

            if (bc.BcMaterialId.HasValue)
            {
                model.Breadcrumbs.Add(new BreadcrumbStructure
                {
                    Text = bc.BcMaterialName,
                    Link = Url.Action("ShowThemes", "Content", new { employeeId = bc.EmployeeId, materialId = bc.BcMaterialId }),
                    IsAvailable = true
                });
            }

            if (!String.IsNullOrWhiteSpace(bc.BcCurrent))
                model.Breadcrumbs.Add(new BreadcrumbStructure { Text = bc.BcCurrent, IsAvailable = false });

            return PartialView(model);
        }

        [NonAction]
        private ActionResult EnterPreparation(Guid employeeId, int materialId, Action process = null)
        {
            var course = _courseRepository.GetByBranchId(materialId);
            var state = _prepareCourseStateProvider.GetCourseState(employeeId, course);

            var model = new ContentShowThemesViewModel
            {
                ConcreteEmployeeId = employeeId,
                MaterialBranchId = materialId,
                CourseFullName = Convert.ToString(course),
                CourseHasQuestions = course.QuestionsCount > 0,
                CourseHasTickets = course.TicketsCount > 0,
                Themes = state.Topics.Select(t => new ThemeWithStatusStructure
                {
                    Topic = t,
                    HasQuestions = t.QuestionsCount > 0,
                    HasMaterials = (t.ScormsCount + t.DocumentsCount) > 0,
                    Answered = t.State != PrepareState.Unkonwn,
                    Correct = t.State == PrepareState.Passed
                })
                    .ToList()
            };

            if (process != null)
                process.Invoke();

            return View("ShowThemes", model);
        }
    }
}