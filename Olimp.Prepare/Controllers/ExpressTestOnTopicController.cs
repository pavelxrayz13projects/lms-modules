using Olimp.Core.Mvc.Security;
using Olimp.Domain.Exam.Attempts;
using Olimp.Domain.Exam.Entities;
using Olimp.Domain.Exam.Entities.Pseudo;
using Olimp.Prepare.ViewModels;
using Olimp.Web.Controllers.Users;
using System;
using System.Web.Mvc;

namespace Olimp.Prepare.Controllers
{
    [CheckActivation(Order = 1)]
    [AllowByConfiguration(FieldName = "AllowExamPreparation", Order = 2)]
    [RequireWorkplaceToken(Order = 3)]
    [LimitUsers(Order = 4)]
    public class ExpressTestOnTopicController : PrepareUserController
    {
        private IAttemptFactory _attemptFactory;
        private IAttemptTasksNavigator _taskNavigator;
        private IAttemptAnswerWriter _answerWriter;
        private IAttemptFinalizer<PrepareOnTopicAttempt> _attemptFinalizer;

        public ExpressTestOnTopicController(
            IAttemptFactory attemptFactory,
            IAttemptTasksNavigator taskNavigator,
            IAttemptAnswerWriter answerWriter,
            IAttemptFinalizer<PrepareOnTopicAttempt> attemptFinalizer)
        {
            if (attemptFactory == null)
                throw new ArgumentNullException("attemptFactory");

            if (taskNavigator == null)
                throw new ArgumentNullException("taskNavigator");

            if (answerWriter == null)
                throw new ArgumentNullException("answerWriter");

            if (attemptFinalizer == null)
                throw new ArgumentNullException("attemptFinalizer");

            _attemptFactory = attemptFactory;
            _taskNavigator = taskNavigator;
            _answerWriter = answerWriter;
            _attemptFinalizer = attemptFinalizer;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Test(Guid employeeId, int materialId, Guid themeId)
        {
            var attempt = _attemptFactory.CreateExpressTestOnTopic(employeeId, materialId, themeId);

            return RedirectToAction("RenderView", new { attemptId = attempt.Id, materialId = materialId, themeId = themeId });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Test(TestOnTopicFormViewModel result)
        {
            try
            {
                _answerWriter.SetAnswers(result.AttemptId, result.QuestionId, result.Answers);
            }
            catch (AttemptFinishedException)
            {
                return RedirectToAction("Stop", new { attemptId = result.AttemptId, materialId = result.MaterialId, themeId = result.ThemeId });
            }

            return RedirectToAction("RenderView", new { attemptId = result.AttemptId, materialId = result.MaterialId, themeId = result.ThemeId });
        }

        public ActionResult RenderView(Guid attemptId, int materialId, Guid themeId)
        {
            var navigationResult = _taskNavigator.NavigateToNextNotAnswered(attemptId);
            if (navigationResult.NavigationState.HasFlag(TaskNavigationState.OutOfQuestions))
                return RedirectToAction("Stop", new { attemptId = attemptId, materialId = materialId, themeId = themeId });

            var model = new TestOnTopicViewModel
            {
                AttemptId = attemptId,
                Question = navigationResult.Question,
                Number = navigationResult.QuestionNumber.GetValueOrDefault(),
                ThemeId = themeId,
                QuestionsAmount = navigationResult.QuestionsCount,
                MaterialBranchId = materialId
            };

            return View("Test", model);
        }

        public ActionResult Stop(Guid attemptId, int materialId, Guid themeId)
        {
            var result = _attemptFinalizer.FinalizeAttempt(attemptId);

            return RedirectToAction("ShowThemeQuestions", "Content", new
            {
                employeeId = result.ConcreteEmployeeId,
                materialId = materialId,
                themeId = themeId
            });
        }
    }
}