using System.Collections.Generic;
using Olimp.Core.Mvc.Security;
using Olimp.Domain.Exam.Attempts;
using Olimp.Domain.Exam.Auth;
using Olimp.Domain.Exam.Entities;
using Olimp.Domain.Exam.Entities.Pseudo;
using Olimp.Domain.Exam.Tickets;
using Olimp.Domain.LearningCenter;
using Olimp.Prepare.ViewModels;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Users;
using System;
using System.Web.Mvc;

namespace Olimp.Prepare.Controllers
{
    [CheckActivation(Order = 1)]
    [AllowByConfig(typeof(OlimpConfiguration), FieldName = "AllowExamPreparation", Order = 2)]
    [RequireWorkplaceToken(Order = 3)]
    [LimitUsers(Order = 4)]
    //TODO: Create single base controller class for Prepare.ExamingController and Exam.TestController
    public class ExamingController : PrepareUserController
    {
        private readonly IAttemptFactory _attemptFactory;
        private readonly IAttemptTasksNavigator _taskNavigator;
        private readonly ITicketInfoProvider _ticketInfoProvider;
        private readonly IAttemptAnswerWriter _answerWriter;
        private readonly IAttemptFinalizer<PrepareExamAttempt> _attemptFinalizer;
        private readonly ISimpleWebAuthProvider _authProvider;

        public ExamingController(
            IAttemptFactory attemptFactory,
            IAttemptTasksNavigator taskNavigator,
            ITicketInfoProvider ticketInfoProvider,
            IAttemptAnswerWriter answerWriter,
            IAttemptFinalizer<PrepareExamAttempt> attemptFinalizer, 
            ISimpleWebAuthProvider authProvider)
        {
            if (attemptFactory == null)
                throw new ArgumentNullException("attemptFactory");

            if (taskNavigator == null)
                throw new ArgumentNullException("taskNavigator");

            if (ticketInfoProvider == null)
                throw new ArgumentNullException("ticketInfoProvider");

            if (answerWriter == null)
                throw new ArgumentNullException("answerWriter");

            if (attemptFinalizer == null)
                throw new ArgumentNullException("attemptFinalizer");

            if (authProvider == null)
                throw new ArgumentNullException("authProvider");

            _attemptFactory = attemptFactory;
            _taskNavigator = taskNavigator;
            _ticketInfoProvider = ticketInfoProvider;
            _answerWriter = answerWriter;
            _attemptFinalizer = attemptFinalizer;
            _authProvider = authProvider;
        }

        //TODO: Copy-pasted from TestController
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
            Response.AppendHeader("Expires", "0"); // Proxies.
        }

        //TODO: Copy-pasted from TestController
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Test(Guid employeeId, int materialId)
        {
            if (!IsAuthorized(employeeId, _authProvider))
            {
                return RedirectToAuthUser("Test", "Examing", new Dictionary<string, object>
                {
                    {"employeeId", employeeId}, {"materialId", materialId}
                });
            }

            var attempt = _attemptFactory.CreateTrainingExam(employeeId, materialId);

            return RedirectToAction("RenderView", new { attemptId = attempt.Id, materialId = materialId });
        }

        //TODO: Copy-pasted from TestController
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Test(ExamTestFormViewModel result)
        {
            if (!IsAuthorized(null, _authProvider))
                return RedirectToAuthUser("Index", "StHome", new Dictionary<string, object>());

            try
            {
                _answerWriter.SetAnswers(result.AttemptId, result.QuestionId, result.Answers);
            }
            catch (AttemptFinishedException)
            {
                return RedirectToAction("Result", new { attemptId = result.AttemptId, materialId = result.MaterialId });
            }

            return RedirectToAction("RenderView", new { result.AttemptId, materialId = result.MaterialId });
        }

        //TODO: Copy-pasted from TestController
        [NonAction]
        private bool ShouldRedirectToResult(TaskNavigationResult navigationResult)
        {
            if (navigationResult.NavigationState.HasFlag(TaskNavigationState.AttemptFinished) || navigationResult.NavigationState.HasFlag(TaskNavigationState.OutOfTime))
                return true;

            return navigationResult.NavigationState.HasFlag(TaskNavigationState.OutOfQuestions) && !navigationResult.NavigationAllowed;
        }

        //TODO: Copy-pasted from TestController
        [ChildActionOnly]
        public ActionResult QuestionsList(Guid attemptId, int materialId, bool navigationAllowed, Guid? currentQuestionId)
        {
            var ticketInfo = _ticketInfoProvider.GetByAttemptId(attemptId);
            var model = new QuestionsListViewModel
            {
                AttemptId = attemptId,
                MaterialId = materialId,
                NavigationAllowed = navigationAllowed,
                CurrentQuestionId = currentQuestionId.GetValueOrDefault(),
                Tasks = ticketInfo.Tasks
            };

            return PartialView(model);
        }

        //TODO: Copy-pasted from TestController
        public ActionResult RenderView(Guid attemptId, int materialId, int? testResultId)
        {
            if (!IsAuthorized(null, _authProvider))
            {
                return RedirectToAuthUser("Test", "Examing", new Dictionary<string, object>
                {
                    {"attemptId", attemptId}, {"materialId", materialId}, {"testResultId", testResultId}
                });
            }

            var navigationResult = testResultId.HasValue
               ? _taskNavigator.NavigateTo(testResultId.Value)
               : _taskNavigator.NavigateToNextNotAnswered(attemptId);

            if (ShouldRedirectToResult(navigationResult))
                return RedirectToAction("Result", new {attemptId, materialId});

            var remainingSeconds = navigationResult.RemainingSeconds.GetValueOrDefault();
            var model = new ExamTestViewModel
            {
                AttemptId = attemptId,
                MaterialId = materialId,
                TimeLimitType = (int)navigationResult.ExamTimeMode,
                TimeLimit = remainingSeconds,
                TimeLimitString = new TimeSpan(0, 0, remainingSeconds).ToString(),
                Question = navigationResult.Question,
                GivenAnswers = navigationResult.GivenAnswers,
                NavigationAllowed = navigationResult.NavigationAllowed,
                TicketNumber = navigationResult.TicketNumber
            };

            return View("Test", model);
        }

        //TODO: Copy-pasted from TestController
        //TODO: Optimize Lazy loading
        public ActionResult Result(Guid attemptId, int materialId)
        {
            if (!IsAuthorized(null, _authProvider))
            {
                return RedirectToAuthUser("Test", "Examing", new Dictionary<string, object>
                {
                    {"attemptId", attemptId}, {"materialId", materialId}
                });
            }

            var result = _attemptFinalizer.FinalizeAttempt(attemptId);
            var examAttempt = result.Attempt;

            var model = new ExamTestResultViewModel
            {
                AttemptId = attemptId,
                MaterialId = materialId,
                Finished = examAttempt.IsFinished,
                Passed = examAttempt.IsPassed,
                EmployeeId = result.ConcreteEmployeeId,
                EmployeeFullName = Convert.ToString(examAttempt.Employee),
                Company = Convert.ToString(examAttempt.Employee.Company),
                TicketNumber = examAttempt.Ticket.Number,
                Job = Convert.ToString(examAttempt.Employee.Positions),
                MaterialFullName = Convert.ToString(examAttempt.Test),
                StartTime = examAttempt.StartTime.ToString(),
                MaxErrorsCount = examAttempt.Test.MaxErrors.ToString(),
                ErrorsCount = examAttempt.MistakesCount.GetValueOrDefault(),
                TableData = examAttempt.Tasks.GetTableData(
                    new TableViewModel(), t => new QuestionTaskViewModel(t))
            };

            return View(model);
        }
    }
}