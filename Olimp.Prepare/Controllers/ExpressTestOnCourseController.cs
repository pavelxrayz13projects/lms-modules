using System.Collections.Generic;
using Olimp.Core.Mvc.Security;
using Olimp.Domain.Exam.Attempts;
using Olimp.Domain.Exam.Auth;
using Olimp.Domain.Exam.Entities;
using Olimp.Domain.Exam.Entities.Pseudo;
using Olimp.Prepare.ViewModels;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Users;
using System;
using System.Web.Mvc;

namespace Olimp.Prepare.Controllers
{
    [CheckActivation(Order = 1)]
    [AllowByConfiguration(FieldName = "AllowExamPreparation", Order = 2)]
    [RequireWorkplaceToken(Order = 3)]
    [LimitUsers(Order = 4)]
    public class ExpressTestOnCourseController : PrepareUserController
    {
        private readonly IAttemptFactory _attemptFactory;
        private readonly IAttemptTasksNavigator _taskNavigator;
        private readonly IAttemptAnswerWriter _answerWriter;
        private readonly IAttemptFinalizer<PrepareOnCourseAttempt> _attemptFinalizer;
        private readonly ISimpleWebAuthProvider _authProvider;

        public ExpressTestOnCourseController(
            IAttemptFactory attemptFactory,
            IAttemptTasksNavigator taskNavigator,
            IAttemptAnswerWriter answerWriter,
            IAttemptFinalizer<PrepareOnCourseAttempt> attemptFinalizer,
            ISimpleWebAuthProvider authProvider)
        {
            if (attemptFactory == null)
                throw new ArgumentNullException("attemptFactory");

            if (taskNavigator == null)
                throw new ArgumentNullException("taskNavigator");

            if (answerWriter == null)
                throw new ArgumentNullException("answerWriter");

            if (attemptFinalizer == null)
                throw new ArgumentNullException("attemptFinalizer");

            if (authProvider == null)
                throw new ArgumentNullException("authProvider");

            _attemptFactory = attemptFactory;
            _taskNavigator = taskNavigator;
            _answerWriter = answerWriter;
            _attemptFinalizer = attemptFinalizer;
            _authProvider = authProvider;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Test(Guid employeeId, int materialId)
        {
            if (!IsAuthorized(employeeId, _authProvider))
            {
                return RedirectToAuthUser("Test", "ExpressTestOnCourse", new Dictionary<string, object>
                {
                    {"employeeId", employeeId}, {"materialId", materialId}
                });
            }

            var attempt = _attemptFactory.CreateExpressTestOnCourse(employeeId, materialId, 1, 0.2);

            return RedirectToAction("RenderView", new { attemptId = attempt.Id, materialId });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Test(TestOnCourseFormViewModel result)
        {
            if (!IsAuthorized(null, _authProvider))
                return RedirectToAuthUser("Index", "StHome", new Dictionary<string, object>());
            
            try
            {
                _answerWriter.SetAnswers(result.AttemptId, result.QuestionId, result.Answers);
            }
            catch (AttemptFinishedException)
            {
                return RedirectToAction("Result", new { attemptId = result.AttemptId, materialId = result.MaterialId });
            }

            return RedirectToAction("RenderView", new { attemptId = result.AttemptId, materialId = result.MaterialId });
        }

        public ActionResult RenderView(Guid attemptId, int materialId)
        {
            if (!IsAuthorized(null, _authProvider))
            {
                return RedirectToAuthUser("RenderView", "ExpressTestOnCourse", new Dictionary<string, object>
                {
                    {"attemptId", attemptId}, {"materialId", materialId}
                });
            }

            var result = _taskNavigator.NavigateToNextNotAnswered(attemptId);

            if (result.NavigationState.HasFlag(TaskNavigationState.OutOfQuestions))
                return RedirectToAction("Result", new {attemptId, materialId});

            return View("Test", new TestOnCourseViewModel
            {
                AttemptId = attemptId, MaterialId = materialId, Question = result.Question
            });
        }

        //TODO: Optimize Lazy loading
        public ActionResult Result(Guid attemptId, int materialId)
        {
            if (!IsAuthorized(null, _authProvider))
            {
                return RedirectToAuthUser("Result", "ExpressTestOnCourse", new Dictionary<string, object>
                {
                    {"attemptId", attemptId}, {"materialId", materialId}
                });
            }

            var result = _attemptFinalizer.FinalizeAttempt(attemptId);

            var model = new TestOnCourseResultViewModel
            {
                MaterialId = materialId,
                EmployeeId = result.ConcreteEmployeeId,
                ErrorsQ = result.Attempt.MistakesCount.Value
            };

            foreach (var testResult in result.Attempt.Tasks)
            {
                var testResultStucture = new TestOnCourseResultStructure
                {
                    TopicName = testResult.Question.Topic.Name,
                    Question = testResult.Question.Text,
                    Correct = testResult.IsCorrect,
                    Answers = testResult.AnswerText
                };

                model.TestResults.Add(testResultStucture);

                var i = 1;
                model.TableData = model.TestResults.GetTableData(
                    new TableViewModel { GroupBy = "TopicName" }, t => new { Number = i++, t.Question, t.Answers, t.Correct });
            }

            return View(model);
        }
    }
}