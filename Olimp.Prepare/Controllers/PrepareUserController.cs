using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Olimp.Domain.Exam.Auth;
using Olimp.Web.Controllers.Learning;

namespace Olimp.Prepare.Controllers
{
    public class PrepareUserController : IntegrationUserController
    {
        public override void DeleteUserCookie()
        {
            Plugin.AuthCookie.Unset(Response);
        }

        protected override string UserCookieName { get { return Plugin.AuthorizationCookieName; } }

        protected override string UserCookiePath { get { return Plugin.AuthorizationCookiePath; } }

        protected override bool ExitIntegrationModeOnReturn { get { return true; } }

        protected override bool KeepIntegrationContext(IntegrationContext ctx)
        {
            return true;
        }

        [NonAction]
        protected ActionResult RedirectToAuthUser(
            string fromAction, 
            string fromController, 
            IDictionary<string, object> urlParams)
        {
            return RedirectToAction("User", "AuthUser", new
            {
                area = "",
                returnUrl = Url.Action(fromAction, fromController, new RouteValueDictionary(urlParams))
            });
        }

        [NonAction]
        protected bool IsAuthorized(Guid? employeeId, ISimpleWebAuthProvider authProvider)
        {
            var authResult = authProvider.AuthorizeForPrepare(
                Plugin.AuthCookie,
                ControllerContext.RequestContext,
                employeeId);

            return authResult.IsAuthorized;
        }
    }
}