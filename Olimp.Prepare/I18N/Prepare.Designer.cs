﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Olimp.Prepare.I18N {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Prepare {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Prepare() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Olimp.Prepare.I18N.Prepare", typeof(Prepare).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Работа с темой курса.
        /// </summary>
        public static string ActionsWithTopic {
            get {
                return ResourceManager.GetString("ActionsWithTopic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Дополнительные материалы для изучения.
        /// </summary>
        public static string AdditionalMaterials {
            get {
                return ResourceManager.GetString("AdditionalMaterials", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ответить.
        /// </summary>
        public static string AnswerButton {
            get {
                return ResourceManager.GetString("AnswerButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Необходимо выбрать минимум один вариант ответа!.
        /// </summary>
        public static string AnswerShouldBeChoosen {
            get {
                return ResourceManager.GetString("AnswerShouldBeChoosen", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вернуться к списку курсов.
        /// </summary>
        public static string BackToCoursesList {
            get {
                return ResourceManager.GetString("BackToCoursesList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вернуться к списку тем курса.
        /// </summary>
        public static string BackToTopicsList {
            get {
                return ResourceManager.GetString("BackToTopicsList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Закрыть.
        /// </summary>
        public static string CloseButton {
            get {
                return ResourceManager.GetString("CloseButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Закрыть.
        /// </summary>
        public static string CloseWindow {
            get {
                return ResourceManager.GetString("CloseWindow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Продолжить.
        /// </summary>
        public static string Continue {
            get {
                return ResourceManager.GetString("Continue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Контрольные вопросы.
        /// </summary>
        public static string ControlQuestions {
            get {
                return ResourceManager.GetString("ControlQuestions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Пробный тест по курсу.
        /// </summary>
        public static string ControlTestOnCourse {
            get {
                return ResourceManager.GetString("ControlTestOnCourse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Правильный ответ.
        /// </summary>
        public static string CorrectAnswer {
            get {
                return ResourceManager.GetString("CorrectAnswer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Экспресс-тест по курсу.
        /// </summary>
        public static string ExpressTestOnCourse {
            get {
                return ResourceManager.GetString("ExpressTestOnCourse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Экспресс-тест по теме курса.
        /// </summary>
        public static string ExpressTestOnTopic {
            get {
                return ResourceManager.GetString("ExpressTestOnTopic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Закончить обучение.
        /// </summary>
        public static string FinishPreparation {
            get {
                return ResourceManager.GetString("FinishPreparation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Завершить.
        /// </summary>
        public static string FinishStopButton {
            get {
                return ResourceManager.GetString("FinishStopButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Основные материалы для изучения.
        /// </summary>
        public static string GeneralMaterials {
            get {
                return ResourceManager.GetString("GeneralMaterials", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Неправильный ответ.
        /// </summary>
        public static string IncorrectAnswer {
            get {
                return ResourceManager.GetString("IncorrectAnswer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Главная страница.
        /// </summary>
        public static string MainPage {
            get {
                return ResourceManager.GetString("MainPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Материалы для изучения.
        /// </summary>
        public static string Materials {
            get {
                return ResourceManager.GetString("Materials", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ни одного курса в профиле не установлено. Обратитесь к преподавателю..
        /// </summary>
        public static string NoCoursesInProfile {
            get {
                return ResourceManager.GetString("NoCoursesInProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to №.
        /// </summary>
        public static string Number {
            get {
                return ResourceManager.GetString("Number", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to из.
        /// </summary>
        public static string Of {
            get {
                return ResourceManager.GetString("Of", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Подготовка к экзамену.
        /// </summary>
        public static string PrepareTitle {
            get {
                return ResourceManager.GetString("PrepareTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вопрос.
        /// </summary>
        public static string Question {
            get {
                return ResourceManager.GetString("Question", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Помощь к вопросу №.
        /// </summary>
        public static string QuestionHelpTitle {
            get {
                return ResourceManager.GetString("QuestionHelpTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to № вопроса.
        /// </summary>
        public static string QuestionNumber {
            get {
                return ResourceManager.GetString("QuestionNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вопросы.
        /// </summary>
        public static string Questions {
            get {
                return ResourceManager.GetString("Questions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to вопрос.
        /// </summary>
        public static string QuestionSmalLetter {
            get {
                return ResourceManager.GetString("QuestionSmalLetter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Текст вопроса.
        /// </summary>
        public static string QuestionText {
            get {
                return ResourceManager.GetString("QuestionText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Повторить.
        /// </summary>
        public static string Repeat {
            get {
                return ResourceManager.GetString("Repeat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выбор курса для самоподготовки.
        /// </summary>
        public static string SelectCourseForPrepare {
            get {
                return ResourceManager.GetString("SelectCourseForPrepare", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выберите тему курса для подготовки к экзамену..
        /// </summary>
        public static string SelectTopicExplain {
            get {
                return ResourceManager.GetString("SelectTopicExplain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Выбор темы курса.
        /// </summary>
        public static string SelectTopicTitle {
            get {
                return ResourceManager.GetString("SelectTopicTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Посмотреть помощь к вопросу.
        /// </summary>
        public static string ShowHelp {
            get {
                return ResourceManager.GetString("ShowHelp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Посмотреть вопрос.
        /// </summary>
        public static string ShowQuestion {
            get {
                return ResourceManager.GetString("ShowQuestion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вопрос №.
        /// </summary>
        public static string ShowQuestionTitle {
            get {
                return ResourceManager.GetString("ShowQuestionTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Статус.
        /// </summary>
        public static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Прервать.
        /// </summary>
        public static string StopButton {
            get {
                return ResourceManager.GetString("StopButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Вы ответили на все вопросы билета. Для завершения экзамена нажмите кнопку &quot;Завершить&quot;.
        /// </summary>
        public static string StopMessage1 {
            get {
                return ResourceManager.GetString("StopMessage1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Для продолжения экзамена выберите нужный вопрос.
        /// </summary>
        public static string StopMessage2 {
            get {
                return ResourceManager.GetString("StopMessage2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Билет.
        /// </summary>
        public static string Ticket {
            get {
                return ResourceManager.GetString("Ticket", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Превышено допустимое время ответа на вопрос.
        /// </summary>
        public static string TimeEndMessage {
            get {
                return ResourceManager.GetString("TimeEndMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to время ответа:.
        /// </summary>
        public static string TimeOfAnswer {
            get {
                return ResourceManager.GetString("TimeOfAnswer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Тема.
        /// </summary>
        public static string Topic {
            get {
                return ResourceManager.GetString("Topic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Допущено ошибок.
        /// </summary>
        public static string TotalErrors {
            get {
                return ResourceManager.GetString("TotalErrors", resourceCulture);
            }
        }
    }
}
