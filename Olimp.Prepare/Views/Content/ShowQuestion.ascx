﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<ContentShowQuestionViewModel>" %>

<% using (Html.BeginForm("CheckQuestion", "Content", FormMethod.Post,new {id = "question-form"})) { %>	
	<%=  Html.ExpressQuestionForm() %>
<% } %>

