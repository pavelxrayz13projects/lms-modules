﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<ContentShowThemesViewModel>" %>

<table id="themes-table" cellspacing="0" cellpadding="0">
<%int i = 0;%>		
<% foreach(var c in Model.Themes){ %>
	<%i++;%>		
	<tr>				
		<td>
			<%if (c.Answered ){%>
				<%if (c.Correct){%>
					<span class="topic-icon topic-icon-passed"></span>
				<% }else{ %>
					<span class="topic-icon topic-icon-failed"></span>
				<% } %>		
			<% }else if(c.HasMaterials || c.HasQuestions){ %>
					<span class="topic-icon topic-icon-unknown"></span>
			<% }else{ %>
					<span class="topic-icon topic-icon-disabled"></span>	
			<% } %>
		</td>
		<td class="theme-text">
			<%if (c.HasMaterials){%>
				<%= Html.ActionLink(c.Topic.Name, "ShowDocs",new {
					area = "Prepare",
					controller = "Content", 
					employeeId = Model.ConcreteEmployeeId,
					materialId = Model.MaterialBranchId, 
					themeId = c.Topic.UniqueId })%>
			<% }else if(c.HasQuestions){ %>
				<%= Html.ActionLink(c.Topic.Name, "ShowThemeQuestions",new {
					area = "Prepare",
					controller = "Content", 
					employeeId = Model.ConcreteEmployeeId,
					materialId = Model.MaterialBranchId, 
					themeId = c.Topic.UniqueId })%>
			<% }else{ %>
				<%=c.Topic.Name%>
			<% } %>
			<div class="theme-description">
				<%= c.Topic.Description%>
			</div>
		</td>
		
	</tr>
	<%if (Model.Themes.Count > i){%>
		<tr>
			<td colspan="2">
				<div class="theme-spacer"></div>
			</td>
		</tr>
	<%}%>
<% } %>
		
</table>