﻿<%@ Page Language="C#" Inherits="OlimpViewPage<ContentShowThemesViewModel>" MasterPageFile="~/Views/Shared/Prepare.master" %>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Navigation" runat="server">
	<%--	<%= Html.ActionLink("Темы курса", "ShowThemes",new {controller = "Content", employeeId = Model.Employee.Id, materialId = Model.Material.Id},new {@class = "active"})%> --%>
	<% Platform.RenderExtensions("/prepare/layout/navigation", new Dictionary<string, object> { { "ActiveLink", "show-themes" } }); %>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Sidebar" runat="server">
	<div class="links">	
		<% if (Model.CourseHasQuestions) { %>	
			<%= Html.ActionLink(I18N.Prepare.ExpressTestOnCourse, "Test", new { controller = "ExpressTestOnCourse", employeeId = Model.ConcreteEmployeeId, materialId = Model.MaterialBranchId }, new { @class = "navigation-link" })%>	
		<%}%>
		<%if (Model.CourseHasTickets){%>	
			<%= Html.ActionLink(I18N.Prepare.ControlTestOnCourse, "Test", new { controller = "Examing", employeeId = Model.ConcreteEmployeeId, materialId = Model.MaterialBranchId }, new { @class = "navigation-link" })%>
		<%}%>
		<%= Html.ActionLink(I18N.Prepare.BackToCoursesList, "Index", new { controller = "StHome", employeeId = Model.ConcreteEmployeeId, materialId = Model.MaterialBranchId }, new { @class = "navigation-link" })%>	
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Breadcrumb" runat="server">
	<%= Html.Action("ShowBreadcrumb", new { employeeId = Model.ConcreteEmployeeId, bcCurrent = Model.CourseFullName })%>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Content" runat="server">
	<h1><%= I18N.Prepare.SelectTopicTitle %></h1>
	<p class="normal-text"><%= I18N.Prepare.SelectTopicExplain %></p>
	<div class="block">
		<%Html.RenderPartial("ThemesList");%>
	</div>
</asp:Content>

