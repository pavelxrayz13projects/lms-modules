﻿<%@ Page Language="C#" Inherits="OlimpViewPage<ContentShowTQuestionsViewModel>" MasterPageFile="~/Views/Shared/Prepare.master" %>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Js" runat="server">
	<script type="text/javascript">
		window.showHelp = function(question){
			$.post('<%= Url.Action("ShowQHelp") %>', 
				{ materialId : <%= Model.MaterialBranchId %>, questionId: question.Id(), number: question.Number() },
				function(html) {
					var dialog = $("#help-dialog");
		
					dialog.html(html);		
					
					var scrollTop = $(document).scrollTop();
					dialog.olimpdialog("option", "title", '<%= I18N.Prepare.QuestionHelpTitle %>' + question.Number())
						.olimpdialog("option", "height", 'auto')
						.olimpdialog('open')		
						.olimpdialog("option", "height", dialog.height() > 500 ? 500 : 'auto');						
					$(document).scrollTop(scrollTop);		
					dialog.olimpdialog("option", "position", "center center");
				}
			)	
			return false;
		};
		
		window.showQuestion = function(question){
			$.post('<%= Url.Action("ShowQuestion") %>',
				{ materialId : <%= Model.MaterialBranchId %>, questionId: question.Id() },
				function(html) {
					var dialog = $("#question-dialog"),
						reload = function() {
							var buttons = dialog
								.html(dialog.data('html'))
								.olimpdialog("option", "title", '<%= I18N.Prepare.ShowQuestionTitle %>' + question.Number())
								.olimpdialog('open')
								.olimpdialog("option", "height", 'auto')
								.olimpdialog("option", "position", "center center")
								.parent().find('.olimp-dialog-buttonpane button').hide();
		
							buttons.eq(0).show();
							buttons.eq(2).show();
						};
					
					dialog.data('reload', reload)
						.data('html', html);
					
					reload();					
				}
			)	
			return false;
		}
		
		$(function() {
			$("#help-dialog").olimpdialog({
				width: 650,
				resizable: false,
				modal: true,
				autoOpen: false,
				buttons: {
					'<%= I18N.Prepare.CloseButton %>': function() { $(this).olimpdialog('close') }
				}
			});
		
			$("#question-dialog").olimpdialog({
				width: 650,
				resizable: false,
				modal: true,
				autoOpen: false,
				buttons: {
					'<%= I18N.Prepare.AnswerButton %>': function() {
						var dialog = $(this);
		
						if (!$('#question-form :checked').length) {
							alert("Необходимо выбрать как минимум один вариант ответа");
							return false;
						}
	
					 	$.post($("#question-form").attr("action"), $("#question-form").serialize(), function(html) { 
							dialog.olimpdialog("option", "height", 'auto')
								.html(html);
							
							var scrollTop = $(document).scrollTop();
							if(dialog.height() > 500)
								dialog.olimpdialog("option", "height", 500 );
							$(document).scrollTop(scrollTop);	
							dialog.olimpdialog("option", "position", "center center");													
						});
						return false;
					},
					'<%= I18N.Prepare.Repeat %>': function() { $(this).data('reload')() },
					'<%= I18N.Prepare.CloseButton %>': function() { $(this).olimpdialog('close') }
				}
			});
		});
	</script>
	
	<script id="question-status" type="text/html">
		<!-- ko if: Answered -->
			<!-- ko if: Correct -->
				<span class="icon icon-success"></span>
			<!-- /ko -->
			<!-- ko ifnot: Correct -->
				<span class="icon icon-fail"></span>
			<!-- /ko -->
		<!-- /ko -->	
		<!-- ko ifnot: Answered -->
			<span class="icon icon-unknown"></span>
		<!-- /ko -->
	</script>	
		
	<script id="question-number" type="text/html">
			<span style="white-space:nowrap;"><%= I18N.Prepare.Question%> <span data-bind="text: Number"></span></span>
	</script>
	
	<script type="text/html" id="show-question-action">
		<a title="<%= I18N.Prepare.ShowQuestion%>" href="#" class="icon icon-info action" data-bind="click : showQuestion"></a>
	</script>
	
	<script type="text/html" id="show-help-action">
		<!-- ko if: HasHelp -->
			<a title="<%= I18N.Prepare.ShowHelp%>" href="#" class="icon icon-question action" data-bind="click : showHelp"></a>
		<!-- /ko -->
	</script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Navigation" runat="server">
	<a href="#" class="active"><%= I18N.Prepare.ActionsWithTopic %></a>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Sidebar" runat="server">
	<div class="links">	
		<% if (Model.TopicHasMaterials){ %>
			<%= Html.ActionLink(I18N.Prepare.Materials, "ShowDocs", new { controller = "Content", employeeId = Model.ConcreteEmployeeId, materialId = Model.MaterialBranchId, themeId = Model.TopicUniqueId }, new { @class = "navigation-link" })%>	
		<% } %>
		<%= Html.ActionLink(I18N.Prepare.ControlQuestions, "ShowThemeQuestions", new { controller = "Content", employeeId = Model.ConcreteEmployeeId, materialId = Model.MaterialBranchId, themeId = Model.TopicUniqueId }, new { @class = "navigation-link active" })%>			
		<%= Html.ActionLink(I18N.Prepare.ExpressTestOnTopic,  "Test", new { controller = "ExpressTestOnTopic", employeeId = Model.ConcreteEmployeeId, materialId = Model.MaterialBranchId, themeId = Model.TopicUniqueId }, new { @class = "navigation-link" })%>
		<%= Html.ActionLink(I18N.Prepare.BackToTopicsList, "ShowThemes", new { controller = "Content", employeeId = Model.ConcreteEmployeeId, materialId = Model.MaterialBranchId }, new { @class = "navigation-link" })%>		
	</div>	
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Breadcrumb" runat="server">
	<%= Html.Action("ShowBreadcrumb", new { 
        employeeId = Model.ConcreteEmployeeId, 
        bcMaterialId = Model.MaterialBranchId, 
        bcMaterialName = Model.MaterialFullName,
        bcCurrent = Model.TopicName
    })%>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Content" runat="server">
	<h1><%=I18N.Prepare.ControlQuestions%></h1>
	<div class="block">	
		<% Html.Table(
			new TableOptions {
                TableData = Model.Questions,
				Columns = new[] { 
					new ColumnOptions("Number", I18N.Prepare.QuestionNumber){ TemplateId = "question-number" },
					new ColumnOptions("Correct", I18N.Prepare.Status){ TemplateId = "question-status" },
					new ColumnOptions("QuestionText", I18N.Prepare.QuestionText)},
				Actions = new RowAction[] { 
					new TemplateAction("show-question-action"),
					new TemplateAction("show-help-action")}
			}, new { id = "theme-questions" }); %>
	</div>
	
	<div id="question-dialog">
	</div>
	
	<div id="help-dialog">
	</div>
</asp:Content>
