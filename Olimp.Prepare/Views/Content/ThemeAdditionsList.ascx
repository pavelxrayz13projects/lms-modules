﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<ContentShowDocsViewModel>" %>

<script type="text/javascript">
$(function() {
	$('.scorm-link-active').click(function() {
		window.open(this.href, "ModuleWindow", 
			"toolbar=no,location=no,status=no,menubar=no,resizable=no,directories=no,scrollbars=no, width=1018,height=682");	
		return false;
	})
})	
function openScorm(el) {
	
}	
</script>

<ul class="materials-list">	
<% foreach(var ad in Model.TopicScorms){ %>
	<li>	
		<span class="icon icon-scorm"></span>
	
		<% if (ad.IsAvailable) { %>			
			<a href="<%= ad.GetLink("/!/scorms.ecp") %>" class="scorm-link scorm-link-active"><%= ad.Title %></a>			
		<% } else { %>
			<span class="scorm-link"><%= ad.Title %></span>
		<% } %>			
	</li>
<% } %>
</ul>		
