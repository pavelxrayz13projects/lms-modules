﻿<%@ Page Language="C#" Inherits="OlimpViewPage<ContentShowDocsViewModel>" MasterPageFile="~/Views/Shared/Prepare.master" %>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Navigation" runat="server">
	<a href="#" class="active">Работа с темой курса</a>		
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Sidebar" runat="server">
	<div class="links">		
		<%= Html.ActionLink(I18N.Prepare.Materials, "ShowDocs", new { controller = "Content", employeeId = Model.ConcreteEmployeeId, materialId = Model.MaterialBranchId, themeId = Model.TopicUniqueId }, new { @class = "navigation-link active" }) %>	
		<% if (Model.TopicHasQuestions){ %>
			<%= Html.ActionLink(I18N.Prepare.ControlQuestions, "ShowThemeQuestions", new { controller = "Content", employeeId = Model.ConcreteEmployeeId, materialId = Model.MaterialBranchId, themeId = Model.TopicUniqueId }, new { @class = "navigation-link" }) %>
			<%= Html.ActionLink(I18N.Prepare.ExpressTestOnTopic, "Test", new { controller = "ExpressTestOnTopic", employeeId = Model.ConcreteEmployeeId, materialId = Model.MaterialBranchId, themeId = Model.TopicUniqueId }, new { @class = "navigation-link" }) %>
		<%}%>	
		<%= Html.ActionLink(I18N.Prepare.BackToTopicsList, "ShowThemes", new { controller = "Content", employeeId = Model.ConcreteEmployeeId, materialId = Model.MaterialBranchId },new { @class = "navigation-link" }) %>	
	</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Breadcrumb" runat="server">
	<%= Html.Action("ShowBreadcrumb", new { 
        employeeId = Model.ConcreteEmployeeId, 
        bcMaterialId = Model.MaterialBranchId, 
        bcMaterialName = Model.MaterialFullName, 
        bcCurrent = Model.TopicName 
    })%>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Prepare_Content" runat="server">
	<% if (Model.TopicDocs.Count > 0){ %>	
		<h1><%= I18N.Prepare.GeneralMaterials %></h1>
		<div class="block">	
			<%Html.RenderPartial("ThemeMaterialsList");%>
		</div>
	<%}%>
	<% if (Model.TopicScorms.Count > 0){ %>
		<h1><%= I18N.Prepare.AdditionalMaterials %></h1>
		<div class="block">
			<%Html.RenderPartial("ThemeAdditionsList");%>
		</div>
	<%}%>
</asp:Content>

