﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<ContentShowDocsViewModel>" %>
<ul class="materials-list">	
<% foreach(var c in Model.TopicDocs){ %>
	<li>	
		<span class="icon icon-document"></span>
		
		<% if (c.IsAvailable) { %>
            <a href="<%= c.GetLink("/Prepare/Doc") %>" target="_blank" class="document-caption"><%= c.Title %></a>
		<% } else { %>
			<span class="document-caption"><%= c.Title %></span>
		<% } %>
	</li>
<% } %>
</ul>	
