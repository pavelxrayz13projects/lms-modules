﻿<%@ Control Language="C#" Inherits="IntegratableUserControl<ContentShowBreadcrumbViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<% var renderSeparator = IntegrationContext.IntegrationState != IntegrationContextState.InIntegrationContext; %>
<% if (renderSeparator) { %>
    <%= Html.RouteLink(I18N.Prepare.MainPage, "Default") %>
<% } %>

<% foreach(var link in Model.Breadcrumbs){ %>
    <% if (renderSeparator) { %>
    <span>&nbsp;/&nbsp;</span>
    <% } else { renderSeparator = true; } %>

	<span>
	<% if (link.IsAvailable){ %>
        <a href="<%= link.Link %>"><%= link.Text %></a>
	<% }else{ %>
		<%=link.Text%>
	<% } %>
	</span>
<% } %>