﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<ContentCheckQuestionViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<%if (!Model.Correct){%>		
	<h1><%= I18N.Prepare.IncorrectAnswer %></h1>
	<%if ( Model.Help.Length > 0) {%>
		<div class="block">	
			<%=Model.Help%>	
		</div>
	<%}%>
	
	<script type="text/javascript">
		$(function() {
		    var buttons = $('#question-dialog').parent()
                .find('.olimp-dialog-buttonpane button').hide();
			buttons.eq(1).show();
			buttons.eq(2).show();
		});
	</script>
<%}else{%>
	<h1><%= I18N.Prepare.CorrectAnswer %></h1>

	<script type="text/javascript">
		$(function() {
		    $('#question-dialog').parent()
                .find('.olimp-dialog-buttonpane button').hide()
                .eq(2).show();
		});
	</script>
<%}%>		
