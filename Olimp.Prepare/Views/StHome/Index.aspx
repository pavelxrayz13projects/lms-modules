﻿<%@ Page Language="C#" Inherits="OlimpViewPage<StHomeIndexViewModel>" MasterPageFile="~/Views/Shared/SimpleLayout.master" %>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Head" runat="server">
<title>Самоподготовка</title>	
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Status" runat="server">
	<% Html.RenderPartial("PrepareLogout"); %>	
</asp:Content>
<asp:Content ContentPlaceHolderID="Olimp_TopRight" runat="server">
	<% if (Olimp.Prepare.Plugin.DisplayEmployeeName) { %>	
	<div class="top-right-text">	
		<table cellpadding="0" cellspacing="0">	
			<tr>
				<td><%= Model.Employee.ToString()%></td>
			</tr>
		</table>
	</div>	
	<% } %>
</asp:Content>
	
<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">
	<h1><%= I18N.Prepare.SelectCourseForPrepare %></h1>
	<%Html.RenderPartial("PrepareCoursesList");%>
</asp:Content>
