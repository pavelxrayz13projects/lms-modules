﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<StHomeIndexViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<% // TODO: get course tree from service
    Html.Catalogue(	
	new CatalogueOptions {
        SelectUrl = Url.Action("GetCourse", new { employeeId = Model.ConcreteEmployeeId, profileId = Model.ProfileId}),
		TemplateEngine = TemplateEngines.DoT,
		InitialExpanded = true, 
		ShowCheckBoxes = false,
		EmptyMessage = I18N.Prepare.NoCoursesInProfile,
        MaterialLink = (Url.RouteUrl("Prepare_SelectCourse", new { employeeId = Model.ConcreteEmployeeId }) + "/{id}")    
	}); 
%>