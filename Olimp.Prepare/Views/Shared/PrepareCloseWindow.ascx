﻿<%@ Control Language="C#" Inherits="IntegratableUserControl<Object>" %>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<a class="status-link" onclick="javascript: window.close(); return false;"  href="#">
    <table>
        <tr>
            <td><div class="icon icon-finish-flag"></div></td>
            <td class="text"><%= I18N.Prepare.CloseWindow %></td>
        </tr>
    </table>
</a>