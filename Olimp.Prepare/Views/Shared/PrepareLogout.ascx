﻿<%@ Control Language="C#" Inherits="IntegratableUserControl<Object>" %>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<% if (IntegrationContext.IntegrationState == IntegrationContextState.InIntegrationContext) { %>
<% Html.BeginForm("ReturnToRefereingSystem", "StHome", new { area = "Prepare" }, FormMethod.Post, new { id = "logout-form" }).Dispose(); %>
<% } else { %>
<% Html.BeginForm("Logout", "StHome", new { area="Prepare" }, FormMethod.Post, new { id="logout-form"}).Dispose(); %>
<% } %>
<script type="text/javascript">
function logout() { $('#logout-form')[0].submit() }
</script>
	

<%-- onclick needed for IE to handle event --%>
<a class="status-link" href="javascript: logout()" onclick="logout()">
    <table>
        <tr>
            <td><div class="icon icon-finish-flag"></div></td>
            <td class="text"><%= I18N.Prepare.FinishPreparation %></td>
        </tr>
    </table>
</a>