﻿<%@ Page Language="C#" Inherits="OlimpViewPage<TestOnCourseResultViewModel>" MasterPageFile="~/Views/Shared/SimpleLayout.master"%>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Js" runat="server">
	<script type="text/javascript">
	$(function () {	$('#Next').olimpbutton(); });	
	</script>	
</asp:Content>
	
<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">	
	<%Html.RenderPartial("AnswerResultTemplate_doT");%>	
	
	<h1>Допущено ошибок: <%=Model.ErrorsQ%> </h1>	
	<div class="block">
		<% Html.Table(new TableOptions {			
			TableData = Model.TableData,
			BodyTemplateEngine = TemplateEngines.DoT,
			GroupBy = "TopicName",
			OmitTemplates = false,
			Sorting = false,
			Paging = false,
			Columns = new[] { 
				new ColumnOptions("Number", I18N.Prepare.Number),
				new ColumnOptions("Question", I18N.Prepare.Question),
				new ColumnOptions("Answers", Olimp.I18N.Domain.Attempt.Answer),
				new ColumnOptions("Correct", Olimp.I18N.Domain.Attempt.AnswerCorrect) { TemplateId = "answer-result" }
			}}); %>
	</div>
	<% using (Html.BeginForm("ShowThemes", "Content", new { employeeId = Model.EmployeeId, materialId = Model.MaterialId }, FormMethod.Get) ){ %>
		<button id="Next" type="submit"><%= I18N.Prepare.Continue %></button>
	<% } %>	
</asp:Content>
