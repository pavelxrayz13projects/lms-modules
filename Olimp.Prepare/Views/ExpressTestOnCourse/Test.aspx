﻿<%@ Page Language="C#" Inherits="OlimpViewPage<TestOnCourseViewModel>" MasterPageFile="~/Views/Shared/SimpleLayout.master" %>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Js" runat="server">
	<script type="text/javascript">
		$(function () {	
				$('#Next').olimpbutton().click(function(){
					if ($("input:checkbox:checked").length == 0 && $("input:radio:checked").length == 0){
						alert("<%= I18N.Prepare.AnswerShouldBeChoosen %>");
						return false;
					}
				});
				$('#Stop').olimpbutton().click(function(){
					window.location.href = '<%=Url.Action("Result", "ExpressTestOnCourse", new { attemptId = Model.AttemptId, materialId = Model.MaterialId })%>'
				});
		});	
	</script>	
</asp:Content>
	
<asp:Content ContentPlaceHolderID="Olimp_TopRight" runat="server">
	<div class="top-right-text">	
		<table cellpadding="0" cellspacing="0">	
			<tr>
				<td><%= I18N.Prepare.ExpressTestOnCourse %></td>
			</tr>
		</table>
	</div>		
</asp:Content>
	
<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">
<% using (Html.BeginForm(new { controller="ExpressTestOnCourse", action="Test" })) { %>	
	<%=  Html.TestOnCourseQuestionForm() %>
<% } %>
</asp:Content>