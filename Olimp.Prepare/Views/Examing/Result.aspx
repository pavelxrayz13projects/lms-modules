﻿<%@ Page Language="C#" Inherits="OlimpViewPage<ExamTestResultViewModel>" MasterPageFile="~/Views/Shared/SimpleLayout.master"%>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Js" runat="server">
	<script type="text/javascript">
		$(function () { $('#Next').olimpbutton(); });
	</script>
		
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_TopRight" runat="server">
	<% if (Olimp.Prepare.Plugin.DisplayEmployeeName) { %>		
	<div class="top-right-text">	
		<table cellpadding="0" cellspacing="0">	
			<tr>
				<td><%= Model.EmployeeFullName%></td>
			</tr>
		</table>
	</div>	
	<% } %>	
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">
<%Html.RenderPartial("AttemptPassedTemplate_doT");%>	
<%Html.RenderPartial("AnswerResultTemplate_doT");%>	
	
<div id="protocol-info">
	<div class="block" >
		<table class="form-table">
			<tr class="first">			
				<th><%= Olimp.Archive.I18N.Archive.Course %></th>
				<td><%= Model.MaterialFullName %></td>
			</tr>			
			<tr>			
				<th><%= Olimp.Archive.I18N.Archive.StartTime %></th>
				<td><%= Model.StartTime %></td>
			</tr>
			<%if (Model.MaxErrorsCount.Length > 0) {%>
				<tr>			
					<th><%= Olimp.I18N.Domain.Attempt.MaxErrors %></th>
					<td><%= Model.MaxErrorsCount %></td>
				</tr>	
			<%}%>	
		
			<tr>
				<th><%= I18N.Prepare.TotalErrors %></th>
				<td><%= Model.ErrorsCount %></td>
			</tr>		
			<%if (!String.IsNullOrWhiteSpace(Model.Company)) {%>
				<tr>						
					<th><%= Olimp.Archive.I18N.Archive.Company %></th>
					<td><%= Model.Company %></td>
				</tr>
			<%}%>
			<%if (!String.IsNullOrWhiteSpace(Model.Job)) {%>
				<tr>	
					<th><%= Olimp.I18N.Domain.Attempt.Job %></th>
					<td><%= Model.Job %></td>
				</tr>
			<%}%>
			<tr>	
				<th><%= Olimp.I18N.Domain.Attempt.Ticket %></th>
				<td><%= Model.TicketNumber %></td>
			</tr>
		
					
			<tr class="last">			
				<th><%= Olimp.Archive.I18N.Archive.Result %></th>
			
				<td>
					<%if (Model.Finished) {%>
						<%if (Model.Passed) {%>
							<span class="passed">	<%= Olimp.I18N.Domain.Attempt.Passed %> </span>
						<%}else{%>	
							<span class="failed">	<%= Olimp.I18N.Domain.Attempt.Failed %> </span>
						<%}%>
					<%}%>	
				</td>
			</tr>			
		</table>		
	</div>
</div>
	
	<h1>Ответы</h1>
	<div class="block">
		<% Html.Table(new TableOptions {                
			TableData = Model.TableData,
			BodyTemplateEngine = TemplateEngines.DoT,
			Sorting = false, 
			Paging = false, 
			Columns = new[] {  
				new ColumnOptions("Number", Olimp.I18N.Domain.Attempt.Number), 
               	new ColumnOptions("Question", Olimp.I18N.Domain.Attempt.Question), 
               	new ColumnOptions("Answer", Olimp.I18N.Domain.Attempt.Answer), 
               	new ColumnOptions("Correct", Olimp.I18N.Domain.Attempt.AnswerCorrect) { TemplateId = "answer-result" } },                     
           	}, new { id = "test-results" }); %>	
	</div>
	
	<% using (Html.BeginForm(new { controller="Content", action="ShowThemes", employeeId = Model.EmployeeId, materialId = Model.MaterialId })) { %>
		<button id="Next" type="submit" ><%= I18N.Prepare.Continue %></button>
	<% } %>		
</asp:Content>
