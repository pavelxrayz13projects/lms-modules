﻿<%@ Page Language="C#" Inherits="OlimpViewPage<ExamTestViewModel>" MasterPageFile="~/Views/Shared/Layout.master" %>
<%@ Import Namespace="I18N=Olimp.Prepare.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Js" runat="server">
	<script src="<%= Url.Content("~/Scripts/UpdateClock.js") %>" type="text/javascript"></script>
	<script type="text/javascript">
		$(function () {	
			$("#logout").hide();
			if(typeof clockID !=="undefined"){
				clearTimeout(clockID);
			}
			<% if (Model.TimeLimit > 0 && Model.TimeLimitType > 1) {%>	
				clockID = setTimeout("UpdateClock(<%=Model.TimeLimit%> - 1, false)", 950);
				$('#Next').olimpbutton().click(function(){
					if ($("input:checkbox:checked").length == 0 && $("input:radio:checked").length == 0 && $("#TimeLimit").val() > 0){
						alert("<%= I18N.Prepare.AnswerShouldBeChoosen %>");
						return false;
					}
					clearTimeout(clockID);
				});
			<%}else{%>
				clockID = setTimeout("UpdateClock(<%=Model.TimeLimit%> + 1, true)", 950);
				$('#Next').olimpbutton().click(function(){
					if ($("input:checkbox:checked").length == 0 && $("input:radio:checked").length == 0){
						alert("<%= I18N.Prepare.AnswerShouldBeChoosen %>");
						return false;
					}
					clearTimeout(clockID);
				});
		    <%}%>

		    $('#Stop').olimpbutton().click(function () {
		        clearTimeout(clockID);
		        window.location.href = '<%=Url.Action("Result", new { attemptId = Model.AttemptId, materialId = Model.MaterialId })%>';
				return false;
            });

		});		
	</script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Status" runat="server">
		<div id="timer"><%= Model.TimeLimitString%></div>
		<div id="timer-caption"><%= I18N.Prepare.TimeOfAnswer %></div>  	
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Sidebar" runat="server">
<div id="quiestion-nav">	
	<%Html.RenderAction("QuestionsList", new { 
       attemptId = Model.AttemptId, 
       materialId = Model.MaterialId,
       navigationAllowed = Model.NavigationAllowed, 
       currentQuestionId = Model.Question != null ? (Guid?) Model.Question.UniqueId : null
   });%>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Navigation" runat="server">
	<a href="#" class="active"><%= I18N.Prepare.Ticket %> <%= Model.TicketNumber %></a>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">
<% using (Html.BeginForm(new { controller="Examing", action="Test" })) { %>	
    <%= Html.HiddenFor(m => m.MaterialId) %>

	<%=  Olimp.Prepare.Helpers.TestHelper.QuestionForm(Html) %>
<% } %>
</asp:Content>