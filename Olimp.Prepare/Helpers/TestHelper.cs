using Olimp.Core;
using Olimp.Prepare.ViewModels;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Olimp.Prepare.Helpers
{
    public static class TestHelper
    {
        //TODO: question-text two id
        public static string QuestionForm(this HtmlHelper<ExamTestViewModel> html)
        {
            var result = new StringBuilder();

            if (html.ViewData.Model == null)
                return result.ToString();

            var model = html.ViewData.Model;

            if (model.Question == null)
            {
                result.AppendFormat("<div class=\"block\">{0}<br />{1}</div>",
                    Olimp.Prepare.I18N.Prepare.StopMessage1,
                    Olimp.Prepare.I18N.Prepare.StopMessage2);
                result.AppendFormat("<div class=\"olimp-buttons-container\"><button id=\"Stop\" type=\"button\" >{0}</button></div>{1}",
                    Olimp.Prepare.I18N.Prepare.FinishStopButton,
                    html.HiddenFor(m => m.TimeLimit));

                return result.ToString();
            }

            result.AppendFormat("<div id='question-text'>{0}</div>{1}{2}{3}",
                    model.Question.Text,
                    html.HiddenFor(m => m.AttemptId),
                    html.HiddenFor(m => m.TimeLimit),
                    html.HiddenFor(m => m.TimeLimitType));

            if (model.TimeLimitType == 3)
                result.Append(html.HiddenFor(m => m.TimeLimitAll));

            result.Append(html.Hidden("QuestionId", model.Question.UniqueId));
            result.Append("<div class=\"block\"><table id=\"question-table\" class=\"form-table\" cellspacing=\"0\" cellpadding=\"0\" >");

            var i = 0;
            foreach (var an in model.Question.Answers)
            {
                i++;
                var check = model.GivenAnswers.Contains(an.Number);

                var cls = "";
                if (i == 0)
                    cls = "class=\"first\"";
                else if (i == model.Question.Answers.Length)
                    cls = "class=\"last\"";

                result.AppendFormat("<tr {0}><td class=\"answer-input checkbox\"><input type=\"{1}\" name=\"Answers\" {2} value=\"{3}\"/></td><td class=\"{4}\">{5}</td></tr>",
                    cls,
                    !model.Question.AllowMultiple ? "radio" : "checkbox",
                    check ? "checked=\"true\"" : "",
                    an.Number,
                    OlimpApplication.GodMode && !an.IsCorrect ? "answer-text godmode-incorrect" : "answer-text",
                    an.Text);
            }

            result.Append("</table></div>");

            result.AppendFormat("<div class=\"olimp-buttons-container margin-buttons \"><button id=\"Next\" type=\"submit\" >{0}</button></div>",
                Olimp.Prepare.I18N.Prepare.AnswerButton);

            return result.ToString();
        }

        public static string TestOnCourseQuestionForm(this HtmlHelper<TestOnCourseViewModel> html)
        {
            var result = new StringBuilder();

            if (html.ViewData.Model == null)
                return result.ToString();

            var model = html.ViewData.Model;

            result.AppendFormat("<div id='question-text'>{0}</div>{1}{2}{3}",
                model.Question.Text,
                html.HiddenFor(m => m.AttemptId),
                html.HiddenFor(m => m.MaterialId),
                html.Hidden("QuestionId", model.Question.UniqueId));

            result.Append("<div class=\"block\"><table id=\"question-table\" class=\"form-table\" cellspacing=\"0\" cellpadding=\"0\">");

            var i = 0;
            foreach (var an in model.Question.Answers)
            {
                i++;

                var cls = "";
                if (i == 0)
                    cls = "class=\"first\"";
                else if (i == model.Question.Answers.Length)
                    cls = "class=\"last\"";

                result.AppendFormat("<tr {0}><td class=\"answer-input checkbox\"><input type=\"{1}\" name=\"Answers\" value=\"{2}\"/></td><td class=\"{3}\">{4}</td></tr>",
                    cls,
                    !model.Question.AllowMultiple ? "radio" : "checkbox",
                    an.Number,
                    OlimpApplication.GodMode && !an.IsCorrect ? "answer-text godmode-incorrect" : "answer-text",
                    an.Text);
            }

            result.Append("</table></div>");
            result.AppendFormat("<div class=\"olimp-buttons-container margin-buttons\"><button id=\"Stop\" type=\"button\">{0}</button><button id=\"Next\" type=\"submit\">{1}</button></div>",
                Olimp.Prepare.I18N.Prepare.StopButton,
                Olimp.Prepare.I18N.Prepare.AnswerButton);

            return result.ToString();
        }

        public static string TestOnTopicQuestionForm(this HtmlHelper<TestOnTopicViewModel> html)
        {
            var result = new StringBuilder();

            if (html.ViewData.Model == null)
                return result.ToString();

            var model = html.ViewData.Model;

            result.AppendFormat("<div id='topic-text'>{0}</div>", model.Question.Topic.Name);
            result.AppendFormat("<div id='question-text'>{0}</div>{1}{2}{3}{4}",
                model.Question.Text,
                html.HiddenFor(m => m.AttemptId),
                html.HiddenFor(m => m.ThemeId),
                html.Hidden("MaterialId", model.MaterialBranchId),
                html.Hidden("QuestionId", model.Question.UniqueId));

            result.Append("<div class=\"block\"><table id=\"question-table\" class=\"form-table\" cellspacing=\"0\" cellpadding=\"0\">");

            var i = 0;
            foreach (var an in model.Question.Answers)
            {
                i++;

                var cls = "";
                if (i == 0)
                    cls = "class=\"first\"";
                else if (i == model.Question.Answers.Length)
                    cls = "class=\"last\"";

                result.AppendFormat("<tr {0}><td class=\"answer-input checkbox\"><input type=\"{1}\" name=\"Answers\" value=\"{2}\"/></td><td class=\"{3}\">{4}</td></tr>",
                    cls,
                    !model.Question.AllowMultiple ? "radio" : "checkbox",
                    an.Number,
                    OlimpApplication.GodMode && !an.IsCorrect ? "answer-text godmode-incorrect" : "answer-text",
                    an.Text);
            }

            result.Append("</table></div><div class=\"olimp-buttons-container margin-buttons\">");
            result.AppendFormat("<div class=\"questions-quantity\"><span> {0} {1} {2} {3} </span></div>",
                Olimp.Prepare.I18N.Prepare.QuestionSmalLetter,
                model.Number,
                Olimp.Prepare.I18N.Prepare.Of,
                model.QuestionsAmount);

            result.AppendFormat("<button id=\"Stop\" type=\"button\">{0}</button><button id=\"Next\" type=\"submit\">{1}</button>",
                Olimp.Prepare.I18N.Prepare.StopButton,
                Olimp.Prepare.I18N.Prepare.AnswerButton);

            result.Append("</div>");

            return result.ToString();
        }

        public static string ExpressQuestionForm(this HtmlHelper<ContentShowQuestionViewModel> html)
        {
            var result = new StringBuilder();
            if (html.ViewData.Model == null)
                return result.ToString();

            var model = html.ViewData.Model;
            result.AppendFormat("<div id='question-text'>{0}</div>{1}{2}",
                model.Question.Text,
                html.HiddenFor(m => m.QuestionId),
                html.HiddenFor(m => m.MaterialId));

            result.Append("<table id=\"question-table\" class=\"form-table\" cellspacing=\"0\" cellpadding=\"0\">");

            var i = 0;
            foreach (var an in model.Question.Answers)
            {
                i++;

                var cls = "";
                if (i == 0)
                    cls = "class=\"first\"";
                else if (i == model.Question.Answers.Length)
                    cls = "class=\"last\"";

                result.AppendFormat("<tr {0}><td class=\"answer-input checkbox\"><input type=\"{1}\" name=\"Answers\" value=\"{2}\"/></td><td class=\"{3}\">{4}</td></tr>",
                    cls,
                    !model.Question.AllowMultiple ? "radio" : "checkbox",
                    an.Id,
                    OlimpApplication.GodMode && !an.IsCorrect ? "answer-text godmode-incorrect" : "answer-text",
                    an.Text);
            }

            result.Append("</table>");

            return result.ToString();
        }
    }
}