namespace Olimp.Prepare.ViewModels
{
    public class ExamTestResultStructure
    {
        public int Number { get; set; }

        public string Question { get; set; }

        public string Answers { get; set; }

        public bool Correct { get; set; }
    }
}