using System;

namespace Olimp.Prepare.ViewModels
{
    public class ExamTestFormViewModel
    {
        public Guid AttemptId { get; set; }

        public int MaterialId { get; set; }

        public Guid QuestionId { get; set; }

        public int TimeLimit { get; set; }

        public int TimeLimitAll { get; set; }

        public int[] Answers { get; set; }
    }
}