using Olimp.Courses.Model;
using Olimp.Domain.Exam.Entities;
using System;
using System.Collections.Generic;

namespace Olimp.Prepare.ViewModels
{
    public class ExamTestViewModel
    {
        public Guid AttemptId { get; set; }

        public int MaterialId { get; set; }

        public int QuestionQuantity { get; set; }

        public int TicketNumber { get; set; }

        public bool NavigationAllowed { get; set; }

        public CachedQuestion Question { get; set; }

        public IEnumerable<CourseQuestionAnswer> Answers { get; set; }

        public int TimeLimit { get; set; }

        public string TimeLimitString { get; set; }

        public int TimeLimitType { get; set; }

        public int TimeLimitAll { get; set; }

        public int[] GivenAnswers { get; set; }

        public ExamTestViewModel()
        {
            this.GivenAnswers = new int[0];
        }
    }
}