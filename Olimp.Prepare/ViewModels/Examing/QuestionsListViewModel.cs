﻿using Olimp.Domain.Exam.Entities.Pseudo;
using System;
using System.Collections.Generic;

namespace Olimp.Prepare.ViewModels
{
    public class QuestionsListViewModel
    {
        public int MaterialId { get; set; }

        public Guid AttemptId { get; set; }

        public bool NavigationAllowed { get; set; }

        public Guid CurrentQuestionId { get; set; }

        public IEnumerable<TicketInfoTask> Tasks { get; set; }
    }
}