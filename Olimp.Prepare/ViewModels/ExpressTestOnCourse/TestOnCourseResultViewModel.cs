using System;
using System.Collections.Generic;

namespace Olimp.Prepare.ViewModels
{
    public class TestOnCourseResultViewModel
    {
        public int MaterialId { get; set; }

        public Guid EmployeeId { get; set; }

        public int ErrorsQ { get; set; }

        public IList<TestOnCourseResultStructure> TestResults { get; set; }

        public TestOnCourseResultViewModel()
        {
            TestResults = new List<TestOnCourseResultStructure>();
        }

        public object TableData { get; set; }
    }
}