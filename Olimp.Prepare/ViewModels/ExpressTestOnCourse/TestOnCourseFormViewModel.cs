using System;

namespace Olimp.Prepare.ViewModels
{
    public class TestOnCourseFormViewModel
    {
        public Guid AttemptId { get; set; }

        public int MaterialId { get; set; }

        public Guid QuestionId { get; set; }

        public int[] Answers { get; set; }

        public string Action { get; set; }
    }
}