namespace Olimp.Prepare.ViewModels
{
    public class TestOnCourseResultStructure
    {
        public int Number { get; set; }

        public string Question { get; set; }

        public string TopicName { get; set; }

        public string Answers { get; set; }

        public bool Correct { get; set; }
    }
}