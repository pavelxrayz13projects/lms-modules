using Olimp.Domain.Exam.Entities;
using System;

namespace Olimp.Prepare.ViewModels
{
    public class TestOnCourseViewModel
    {
        public int MaterialId { get; set; }

        public Guid AttemptId { get; set; }

        public CachedQuestion Question { get; set; }
    }
}