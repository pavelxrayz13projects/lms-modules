using System;

namespace Olimp.Prepare.ViewModels
{
    public class ContentCheckQuestionFormViewModel
    {
        public int[] Answers { get; set; }

        public int MaterialId { get; set; }

        public Guid QuestionId { get; set; }
    }
}