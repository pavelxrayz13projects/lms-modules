using Olimp.Courses.Model;

namespace Olimp.Prepare.ViewModels
{
    public class ContentScormReferenceViewModel
    {
        public CourseScormReference ScormReference { get; set; }

        public string ScormLink { get; set; }

        public bool Available { get; set; }
    }
}