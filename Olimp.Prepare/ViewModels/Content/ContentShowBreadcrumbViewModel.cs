using System.Collections.Generic;

namespace Olimp.Prepare.ViewModels
{
    public class ContentShowBreadcrumbViewModel
    {
        public IList<BreadcrumbStructure> Breadcrumbs { get; private set; }

        public ContentShowBreadcrumbViewModel()
        {
            Breadcrumbs = new List<BreadcrumbStructure>();
        }
    }
}