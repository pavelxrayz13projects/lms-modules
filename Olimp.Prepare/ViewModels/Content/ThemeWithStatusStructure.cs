using Olimp.Domain.Common.Prepare;

namespace Olimp.Prepare
{
    public class ThemeWithStatusStructure
    {
        public IStatefulTopic Topic { get; set; }

        public bool Answered { get; set; }

        public bool Correct { get; set; }

        public bool HasQuestions { get; set; }

        public bool HasMaterials { get; set; }
    }
}