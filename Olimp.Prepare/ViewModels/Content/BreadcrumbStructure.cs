namespace Olimp.Prepare
{
    public class BreadcrumbStructure
    {
        public string Text { get; set; }

        public string Link { get; set; }

        public bool IsAvailable { get; set; }
    }
}