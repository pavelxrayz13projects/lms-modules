using Olimp.Domain.Catalogue.Entities.Pseudo;
using System;

namespace Olimp.Prepare.ViewModels
{
    public class ContentShowQuestionViewModel
    {
        public Question Question { get; set; }

        public Guid QuestionId { get; set; }

        public int MaterialId { get; set; }
    }
}