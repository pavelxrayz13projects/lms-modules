using System;

namespace Olimp.Prepare.ViewModels
{
    public class ContentShowTQuestionsViewModel
    {
        public object Questions { get; set; }

        public int MaterialBranchId { get; set; }

        //For breadcrumb
        public string MaterialFullName { get; set; }

        public Guid ConcreteEmployeeId { get; set; }

        public Guid TopicUniqueId { get; set; }

        public string TopicName { get; set; }

        public bool TopicHasMaterials { get; set; }
    }
}