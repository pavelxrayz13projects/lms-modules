using Olimp.UI.ViewModels;
using System;

namespace Olimp.Prepare.ViewModels
{
    public class ContentShowThemeQuestionsViewModel : TableViewModel
    {
        public int EmployeeId { get; set; }

        public int MaterialId { get; set; }

        public Guid ThemeId { get; set; }
    }
}