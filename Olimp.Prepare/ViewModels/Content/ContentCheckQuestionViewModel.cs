namespace Olimp.Prepare.ViewModels
{
    public class ContentCheckQuestionViewModel
    {
        public bool Correct { get; set; }

        public string Help { get; set; }
    }
}