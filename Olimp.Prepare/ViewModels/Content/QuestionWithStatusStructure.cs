using Olimp.Courses.Model;

namespace Olimp.Prepare
{
    public class QuestionWithStatusStructure
    {
        public CourseQuestion Question { get; set; }

        public int Number { get; set; }

        public bool Answered { get; set; }

        public bool Correct { get; set; }
    }
}