using System;
using System.Collections.Generic;

namespace Olimp.Prepare.ViewModels
{
    public class ContentShowThemesViewModel
    {
        public Guid ConcreteEmployeeId { get; set; }

        public int MaterialBranchId { get; set; }

        public string CourseFullName { get; set; }

        public bool CourseHasQuestions { get; set; }

        public bool CourseHasTickets { get; set; }

        public IList<ThemeWithStatusStructure> Themes { get; set; }

        public ContentShowThemesViewModel()
        {
            Themes = new List<ThemeWithStatusStructure>();
        }
    }
}