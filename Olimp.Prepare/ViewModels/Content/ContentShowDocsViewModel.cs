using Olimp.Domain.Catalogue.Content;
using System;
using System.Collections.Generic;

namespace Olimp.Prepare.ViewModels
{
    public class ContentShowDocsViewModel
    {
        public int MaterialBranchId { get; set; }

        //For breadcrumb
        public string MaterialFullName { get; set; }

        public Guid ConcreteEmployeeId { get; set; }

        public Guid TopicUniqueId { get; set; }

        public string TopicName { get; set; }

        public IList<ContentLink> TopicScorms { get; set; }

        public IList<ContentLink> TopicDocs { get; set; }

        public bool TopicHasQuestions { get; set; }
    }
}