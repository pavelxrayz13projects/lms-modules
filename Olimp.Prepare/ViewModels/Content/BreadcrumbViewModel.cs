﻿using System;

namespace Olimp.Prepare.ViewModels
{
    public class BreadcrumbViewModel
    {
        public Guid EmployeeId { get; set; }

        public int? BcMaterialId { get; set; }

        public string BcMaterialName { get; set; }

        public string BcCurrent { get; set; }
    }
}