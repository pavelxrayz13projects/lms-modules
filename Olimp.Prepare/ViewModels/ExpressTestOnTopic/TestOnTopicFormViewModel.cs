using System;

namespace Olimp.Prepare.ViewModels
{
    public class TestOnTopicFormViewModel
    {
        public Guid AttemptId { get; set; }

        public Guid QuestionId { get; set; }

        public Guid ThemeId { get; set; }

        public int MaterialId { get; set; }

        public int[] Answers { get; set; }

        public string Action { get; set; }
    }
}