using Olimp.Domain.Exam.Entities;
using System;

namespace Olimp.Prepare.ViewModels
{
    public class TestOnTopicViewModel
    {
        public Guid AttemptId { get; set; }

        public Guid ThemeId { get; set; }

        public CachedQuestion Question { get; set; }

        public int Number { get; set; }

        public int QuestionsAmount { get; set; }

        public int MaterialBranchId { get; set; }
    }
}