using Olimp.Domain.Exam.Entities;
using System;
using System.Collections.Generic;

namespace Olimp.Prepare.ViewModels
{
    public class StHomeIndexViewModel
    {
        public CachedEmployee Employee { get; set; }

        public Guid ConcreteEmployeeId { get; set; }

        public int? ProfileId { get; set; }

        public ICollection<int> Materials { get; set; }
    }
}