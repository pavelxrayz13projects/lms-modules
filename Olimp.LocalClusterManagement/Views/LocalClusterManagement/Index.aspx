﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IndexViewModel>" MasterPageFile="~/Views/Shared/ClusterManagement.master" %>

<asp:Content runat="server" ContentPlaceHolderID="Olimp_ClusterManagement_Css">
    <style>
        #add-dialog .form-table th, #update-dialog .form-table th { 
            width: 140px; 
            vertical-align: top;
        }
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="Olimp_ClusterManagement_Content">

    <% if (Model.CanManage) { %>
    <div id="add-dialog"><% Html.RenderPartial("Add", new NodeViewModel()); %></div>
    <div id="update-dialog"><% Html.RenderPartial("Update", new NodeViewModel()); %></div>
    <% } %>

    <script type="text/html" id="quota-column-template">
        {{ var quota = ko.utils.unwrapObservable(it.$data.Quota); }}
        {{= (quota == -1 ? '<%= I18N.LocalClusterManagement.Unlimited %>' : quota) }}
    </script>

    <script type="text/html" id="url-column-template">
        {{ var url = ko.utils.unwrapObservable(it.$data.Url); }}
        <a href="{{= url }}" target="_blank">{{= url }}</a>
    </script>

    <script type="text/javascript">
        function toggleParametersText() {
            this.ParametersTextVisible(!this.ParametersTextVisible());
        }

        function openEditDefaultNodeDialog(args) {
            $('#nodes-table').data('viewModel').invokeAction(1, this, { 
                saveTransport: function (ctx, onSuccess, onError) {
                    $.Olimp.showLoader();
                    $.ajax({
                        url: ctx.url,
                        data: ctx.params,
                        type: 'POST',
                        dataType: 'json',
                        global: false,
                        error: function () {
                            var retries = 30;
                            var interval = setInterval(function () {
                                $.ajax({
                                    url: '<%= Url.Action("GetState", "Service", new { area="" }) %>',
                                    success: function (state) {
                                        if ($.isFunction(onSuccess)) {
                                            onSuccess();
                                            onSuccess = null;
                                            if (shouldUseNewPort) {
                                                location.port = formVals.Port;
                                            }
                                        }
                                        $.Olimp.hideLoader();

                                        clearInterval(interval);
                                    },
                                    error: function () {
                                        if (--retries > 0)
                                            return;

                                        if (ctx && ctx.data && ctx.data.Port && location.port != ctx.data.Port) {
                                            location.port = ctx.data.Port;
                                            return;
                                        }

                                        onError();
                                        clearInterval(interval);
                                    },
                                    type: 'POST',
                                    global: false
                                });
                            }, 500)
                        },
                        success: function (json) {
                            $.Olimp.hideLoader();

                            if (json && json.error) {
                                if ($.isFunction(onError))
                                    onError();
                                $.Olimp.showError(json.error)
                            } else {
                                if ($.isFunction(onSuccess))
                                    onSuccess();
                            }
                        }
                    });                   
                }
            });

            return false;
        }

        function nodeActivationResultHandler(res) {
            <%-- Информация обновляется асинхронно (file system watcher) --%>
            res && setTimeout($('#nodes-table').data('viewModel').reload, 1000);
        }
        function activateNode() {
            $.post('<%= Url.Action("Activate") %>', { name: this.Name() }, nodeActivationResultHandler, 'json')

            return false;
        }
        function deactivateNode() {
            var self = this;
            $.post('<%= Url.Action("Deactivate") %>', { name: this.Name() }, nodeActivationResultHandler, 'json')

            return false;
        }
    </script>

    <script type="text/html" id="switch-node-action">
        {{? !ko.utils.unwrapObservable(it.$data.IsActive) }}	
            <a class="action icon icon-turnon -has-dot-binding" title="Активировать" href="#"
                data-dot-bind="click: activateNode"
                data-dot-context="{{= it.$$contextId }}">
            </a>
        {{?? ko.utils.unwrapObservable(it.$data.IsActive) }}
            <a class="action icon icon-turnoff -has-dot-binding" title="Деактивировать" href="#"
                data-dot-bind="click: deactivateNode"
                data-dot-context="{{= it.$$contextId }}">
            </a>
        {{?}}
    </script>

    <script type="text/html" id="default-node-locked-template">
        <div style="text-align:right">
            <a class="icon icon-pencil-active action -has-dot-binding" href="#" title="<%= PlatformShared.Edit %>"
                data-dot-context="{{= it.$$contextId }}"
                data-dot-bind="click: openEditDefaultNodeDialog.bind($data)">
            </a>
        </div>
    </script>

    <h1>Локальный кластер</h1>
    <div class="block">
        <% 
            var tableOptions = new TableOptions
            {
                SelectUrl = Url.Action("GetNodes"),
                BodyTemplateEngine = TemplateEngines.DoT,
                RowId = "Name",
                RowLocked = "IsLocked",
                RowLockedTemplate = "default-node-locked-template",              
                DefaultSortColumn = "Name",
                PagingAllowAll = true,
                Columns = new[] { 
                    new ColumnOptions("Name", I18N.LocalClusterManagement.Name, "15%"),                    
                    new ColumnOptions("ReadableName", I18N.LocalClusterManagement.ReadableName),                    
                    new ColumnOptions("Quota", I18N.LocalClusterManagement.Quota, "15%") { TemplateId = "quota-column-template" },
                    new ColumnOptions("Port", I18N.LocalClusterManagement.Port, "10%"),
                    new ColumnOptions("Url", I18N.LocalClusterManagement.Url, "15%") { TemplateId = "url-column-template" }
                },
                Actions = new RowAction[]
                {
                    new TemplateAction("switch-node-action"),
                    new EditAction("update-dialog", new DialogOptions { Title = I18N.LocalClusterManagement.UpdateNode, Width = "580px" })
                    {
                        Disabled = !Model.CanManage
                    },
                    new DeleteAction(Url.Action("Delete"), I18N.LocalClusterManagement.ConfirmDelete)
                    {
                        Disabled = !Model.CanManage
                    },
                },
                Adding = new AddingOptions
                {
                    Disabled = !Model.CanManage,
                    Text = "Добавить узел",
                    New = new { Id = "", Name = "", Port = 0, Quota = 0, ReadableName = Model.CompanyName, ParametersText = "", ParametersTextVisible = true },
                    Action = new EditAction("add-dialog", new DialogOptions { Title = I18N.LocalClusterManagement.AddNode, Width = "580px" })
                }
            };

            Html.Table(tableOptions, new { id = "nodes-table" }); %>
    </div>

    <script type="text/javascript">
        $(function () {
            $('#update-dialog').bind('afterSubmit', function () { $('#nodes-table').data('viewModel').reload() });
            $('#update-dialog').bind('afterApply',
                function () {
                    var itemModel = $(this).data('itemModel');
                    if (itemModel && itemModel.IsLocked()) {
                            var textInput = $(this).find('input[name="ReadableName"]');
                            if (textInput.length) {
                                textInput.attr("readonly", "readonly");
                            }
                    }
                });
        })
    </script>

</asp:Content>
