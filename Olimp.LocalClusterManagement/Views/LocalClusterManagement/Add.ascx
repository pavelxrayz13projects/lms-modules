﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NodeViewModel>" %>

<% Html.EnableClientValidation(); %>
	
<% using(Html.BeginForm("Add", "LocalClusterManagement")) { %>
	
    <input type="hidden" name="Id" value="" />

	<table class="form-table">
		<tr class="first">
			<th><%= Html.LabelFor(m => m.Name) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Name) %></div>
				<%= Html.TextBoxFor(m => m.Name, new Dictionary<string, object> { { "data-bind", "value: Name" } }) %>
			</td>
		</tr>
        <tr>
			<th><%= Html.LabelFor(m => m.ReadableName) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ReadableName) %></div>
				<%= Html.TextBoxFor(m => m.ReadableName, new Dictionary<string, object> { { "data-bind", "value: ReadableName" } }) %>
			</td>
		</tr>
        <tr>
			<th><%= Html.LabelFor(m => m.Port) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Port) %></div>
				<%= Html.TextBoxFor(m => m.Port, new Dictionary<string, object> { { "data-bind", "value: Port" } }) %>
			</td>
		</tr>
        <tr>
			<th><%= Html.LabelFor(m => m.Quota) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Quota) %></div>
				<%= Html.TextBoxFor(m => m.Quota, new Dictionary<string, object> { { "data-bind", "value: Quota" } }) %>
			</td>
		</tr>
        <tr class="last">
			<th>
                <%= Html.LabelFor(m => m.ParametersText) %>
                (<a href="#" data-bind="
                    text: $data.ParametersTextVisible() ? '<%= PlatformShared.Hide %>' : '<%= PlatformShared.Show %>', 
                    click: toggleParametersText.bind($data)">
                </a>)
			</th>
			<td>	                
                <div data-bind="visible: ParametersTextVisible">
				    <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ParametersText) %></div>
				    <%= Html.TextAreaFor(m => m.ParametersText, new { data_bind = "value: ParametersText", style="height: 100px;" }) %>
                </div>
			</td>
		</tr>
	</table>
	
<% } %>