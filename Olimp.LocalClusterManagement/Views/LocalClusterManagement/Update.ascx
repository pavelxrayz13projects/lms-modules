﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<NodeViewModel>" %>

<% Html.EnableClientValidation(); %>
	
<% using(Html.BeginForm("Update", "LocalClusterManagement")) { %>
	
    <%= Html.HiddenFor(m => m.Id, new { data_bind = "value: Id" })%>

	<table class="form-table">
		<tr class="first">
			<th><%= Html.LabelFor(m => m.Name) %></th>
			<td>
                <%= Html.TextBoxFor(m => m.Name, new { @readonly = "readonly", data_bind="value: Name" })%>

			</td>
		</tr>
        <tr>
			<th><%= Html.LabelFor(m => m.ReadableName) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ReadableName) %></div>
				<%= Html.TextBoxFor(m => m.ReadableName, new { data_bind = "value: ReadableName" }) %>
			</td>
		</tr>
        <tr>
			<th><%= Html.LabelFor(m => m.Port) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Port) %></div>
				<%= Html.TextBoxFor(m => m.Port, new Dictionary<string, object> { { "data-bind", "value: Port" } }) %>
			</td>
		</tr>
        <tr>
			<th><%= Html.LabelFor(m => m.Quota) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Quota) %></div>                              
				<%= Html.TextBoxFor(m => m.Quota, new { data_bind = "value: Quota, disable: IsLocked, visible: !IsLocked()" }) %>

                <!-- ko if: IsLocked() && Quota() == -1 -->
                <input type="text" disabled="disabled" value="<%= I18N.LocalClusterManagement.Unlimited %>" />
                <!-- /ko -->
			</td>
		</tr>
        <tr class="last">
			<th>
                <%= Html.LabelFor(m => m.ParametersText) %>
                (<a href="#" data-bind="
                    text: $data.ParametersTextVisible() ? '<%= PlatformShared.Hide %>' : '<%= PlatformShared.Show %>', 
                    click: toggleParametersText.bind($data)">
                </a>)
			</th>
			<td>	                
                <div data-bind="visible: ParametersTextVisible">
				    <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ParametersText) %></div>
				    <%= Html.TextAreaFor(m => m.ParametersText, new { data_bind = "value: ParametersText", style="height: 100px;" }) %>
                </div>
			</td>
		</tr>
	</table>
	
<% } %>