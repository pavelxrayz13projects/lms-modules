using Olimp.Core;
using Olimp.Core.Activation;
using Olimp.Core.Activation.HardwareBinding;
using Olimp.Core.Routing;
using Olimp.Domain.Common.Nodes;
using Olimp.Domain.LearningCenter;
using Olimp.LocalClusterManagement.Controllers;
using Olimp.LocalNodes.Core.Cluster;
using Olimp.LocalNodes.Core.Services.Clients;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.LocalClusterManagement.Plugin))]

namespace Olimp.LocalClusterManagement
{
    public class Plugin : PluginModule
    {
        private IModuleContext _context;

        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
            _context = context;
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                      typeof(LocalClusterManagementController));

            areas["Services"].RegisterControllers(typeof(PingServiceController));
            areas["Services"].Context.MapRoute("OlimpServices.Ping",
              "Services/Ping/{nodeId}",
              new { controller = "PingService", action = "Ping", nodeId = "" });

            areas["Services"].RegisterControllers(typeof(SyncServiceController));
            areas["Services"].Context.MapRoute("OlimpServices.Sync.Attempts",
              "Services/Sync/Attempts/{nodeId}",
              new { controller = "SyncService", action = "SyncAttemptResults", nodeId = "" });
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (_context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var module = PContext.ExecutingModule as LearningCenterModule;
            if (module == null)
                throw new InvalidOperationException("Executing module is not LearningCenterModule");

            var activationContext = module.Activation;

            var startupRegistry = module.LocalCluster.GetStartupRegistry();
            var activationKeyInfoProviderFactory = new OlimpActivationKeyInfoProviderFactory(
                new OlimpHardwareIdProvider(),
                module.Context.DistributionInfoProvider,
                module.Context.ExclusiveActivationChecker,
                module.Activation.ActivationKeyProvider);

            binder.Controller<SyncServiceController>(b => b
                .Bind<INodesRepository>(() => startupRegistry));

            binder.Controller<LocalClusterManagementController>(b => b
                .Bind<INodesRepository>(() => startupRegistry)
                .Bind<ILocalClusterManager>(() => new DefaultLocalClusterManager(module.Database, startupRegistry))
                .Bind<ILocalNodeController>(() => new WebLocalNodeControllerServiceClient(OlimpPaths.OlimpClusterServiceUri))
                .Bind<IQuotaValidator>(() =>
                    new DefaultQuotaValidator(startupRegistry, activationContext, activationKeyInfoProviderFactory)
                )
                .Bind<INodesTableProvider>(() =>
                    new NodesTableProvider(
                        new WebLocalNodeControllerServiceClient(OlimpPaths.OlimpClusterServiceUri),
                        activationContext
                    )
                )
            );
        }
    }
}