﻿using Olimp.Core.Activation;
using Olimp.Core.Security;
using Olimp.Domain.Common.Nodes;
using System;
using System.Linq;

namespace Olimp.LocalClusterManagement
{
    public class DefaultQuotaValidator : IQuotaValidator
    {
        private INodesRepository _startupRegistry;
        private IModuleActivationContext _activationContext;
        private IActivationKeyInfoProviderFactory _keyInfoProviderFactory;

        public DefaultQuotaValidator(
            INodesRepository startupRegistry,
            IModuleActivationContext activationContext,
            IActivationKeyInfoProviderFactory keyInfoProviderFactory)
        {
            if (startupRegistry == null)
                throw new ArgumentNullException("startupRegistry");

            if (activationContext == null)
                throw new ArgumentNullException("activationContext");

            if (keyInfoProviderFactory == null)
                throw new ArgumentNullException("keyInfoProviderFactory");

            _startupRegistry = startupRegistry;
            _activationContext = activationContext;
            _keyInfoProviderFactory = keyInfoProviderFactory;
        }

        protected virtual bool LocalNodeFilterCondition(Node nodeInfo)
        {
            var keyContainer = nodeInfo as IActivatableNode;

            return keyContainer != null &&
                !String.IsNullOrWhiteSpace(keyContainer.ActivationKey) &&
                !Node.IsDefaultNode(nodeInfo);
        }

        #region IQuotaValidator Members

        public bool Validate(Guid? id, int quota)
        {
            if (quota < 1)
                return false;

            if (_activationContext.UsersQuotaProvider.MaxUsers == UsersQuota.Infinite)
                return true;

            var rootKeyInfo = _activationContext.GetActivationKeyInfo();
            if (rootKeyInfo == null)
                return false;

            var totalQuota = rootKeyInfo.MaxUsers;
            var nodes = _startupRegistry.GetAll().Where(this.LocalNodeFilterCondition);

            foreach (var n in nodes)
            {
                if (id.HasValue && id.Value == n.Id)
                    continue;

                var keyContainer = (IActivatableNode)n;

                var provider = _keyInfoProviderFactory.Create(n.Name);
                var nodeKeyInfo = provider.GetActivationKeyInfo(keyContainer.ActivationKey);
                if (nodeKeyInfo != null)
                    totalQuota -= nodeKeyInfo.MaxUsers;

                if (totalQuota < quota)
                    break;
            }

            return totalQuota >= quota;
        }

        #endregion IQuotaValidator Members
    }
}