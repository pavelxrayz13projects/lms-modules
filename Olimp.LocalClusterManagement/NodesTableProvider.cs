﻿using Olimp.Core.Activation;
using Olimp.Domain.Common.Nodes;
using Olimp.LocalClusterManagement.ViewModels;
using Olimp.LocalNodes.Core.Cluster;
using Olimp.Web.Controllers.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Olimp.LocalClusterManagement
{
    public class NodesTableProvider : INodesTableProvider
    {
        private ILocalNodeController _controllerService;
        private IModuleActivationContext _activationContext;

        public NodesTableProvider(
            ILocalNodeController controllerService,
            IModuleActivationContext activationContext)
        {
            if (controllerService == null)
                throw new ArgumentNullException("controllerService");

            if (activationContext == null)
                throw new ArgumentNullException("activationContext");

            _controllerService = controllerService;
            _activationContext = activationContext;
        }

        private static string GetRequestUrl(Uri requestUrl, int port)
        {
            var builder = new UriBuilder
            {
                Scheme = requestUrl.Scheme,
                Host = requestUrl.Host,
                Port = port
            };

            return String.Format(@"<a href=""{0}"" target=""_blank"">{0}</a>", builder.Uri.ToString());
        }

        public IEnumerable<NodeViewModel> GetTable()
        {
            var request = HttpContext.Current.Request;
            var nodes = _controllerService.GetAllNodes()
                .Where(s => s.Node is DefaultNode || s.Node is LocalNode);

            NodeViewModel defaultNodeModel = null;
            foreach (var n in nodes)
            {
                var isDefault = Node.IsDefaultNode(n.Node);

                var model = new NodeViewModel(n);
                if (isDefault)
                    defaultNodeModel = model;

                if (model.IsActive)
                    model.Url = request.GetNodeUrl(n.Node);

                if (!isDefault)
                    yield return model;
            }

            if (defaultNodeModel == null)
                throw new InvalidOperationException("No (default) node");

            defaultNodeModel.Quota = _activationContext.UsersQuotaProvider.MaxUsers;

            yield return defaultNodeModel;
        }
    }
}