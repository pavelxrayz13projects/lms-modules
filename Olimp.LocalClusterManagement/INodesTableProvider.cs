﻿using Olimp.LocalClusterManagement.ViewModels;
using System.Collections.Generic;

namespace Olimp.LocalClusterManagement
{
    public interface INodesTableProvider
    {
        IEnumerable<NodeViewModel> GetTable();
    }
}