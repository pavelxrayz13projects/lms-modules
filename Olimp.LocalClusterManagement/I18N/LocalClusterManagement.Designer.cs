﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18063
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Olimp.LocalClusterManagement.I18N {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class LocalClusterManagement {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LocalClusterManagement() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Olimp.LocalClusterManagement.I18N.LocalClusterManagement", typeof(LocalClusterManagement).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Добавить узел.
        /// </summary>
        public static string AddNode {
            get {
                return ResourceManager.GetString("AddNode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Узел и все его данные будут безвозвратно удалены. Продолжить?.
        /// </summary>
        public static string ConfirmDelete {
            get {
                return ResourceManager.GetString("ConfirmDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Неправильный INI-формат параметров узла.
        /// </summary>
        public static string IncorrectIniFileFormat {
            get {
                return ResourceManager.GetString("IncorrectIniFileFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Неправильный формат ключей и/или значений параметров узла.
        /// </summary>
        public static string IncorrectKeyValueParametersFormat {
            get {
                return ResourceManager.GetString("IncorrectKeyValueParametersFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Идентификатор.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Узел с таким идентификатором уже существует.
        /// </summary>
        public static string NodeNameAlreadyExists {
            get {
                return ResourceManager.GetString("NodeNameAlreadyExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Узел не найден.
        /// </summary>
        public static string NodeNotFound {
            get {
                return ResourceManager.GetString("NodeNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Параметры.
        /// </summary>
        public static string ParametersText {
            get {
                return ResourceManager.GetString("ParametersText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Порт.
        /// </summary>
        public static string Port {
            get {
                return ResourceManager.GetString("Port", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &quot;Порт {0} уже зарезервирован за другим узлом&quot;.
        /// </summary>
        public static string PortAlreadyReservedByNode {
            get {
                return ResourceManager.GetString("PortAlreadyReservedByNode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Порт должен быть задан числом от 1 до 65535.
        /// </summary>
        public static string PortInvalid {
            get {
                return ResourceManager.GetString("PortInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &quot;Порт {0} закрыт или занят другим приложением&quot;.
        /// </summary>
        public static string PortIsBusyByTcpCheck {
            get {
                return ResourceManager.GetString("PortIsBusyByTcpCheck", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &quot;Порт {0} является небезопасным и блокируется некоторыми браузерами&quot;.
        /// </summary>
        public static string PortIsRestricted {
            get {
                return ResourceManager.GetString("PortIsRestricted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Квота рабочих мест.
        /// </summary>
        public static string Quota {
            get {
                return ResourceManager.GetString("Quota", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Квота должна лежать в диапозоне от 1 до максимального числа рабочих мест.
        /// </summary>
        public static string QuotaInvalid {
            get {
                return ResourceManager.GetString("QuotaInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Наименование.
        /// </summary>
        public static string ReadableName {
            get {
                return ResourceManager.GetString("ReadableName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Наименование не может быть длиннее {1} символов.
        /// </summary>
        public static string ReadableNameLengthInvalid {
            get {
                return ResourceManager.GetString("ReadableNameLengthInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Неограничено.
        /// </summary>
        public static string Unlimited {
            get {
                return ResourceManager.GetString("Unlimited", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Настройки узла.
        /// </summary>
        public static string UpdateNode {
            get {
                return ResourceManager.GetString("UpdateNode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Адрес.
        /// </summary>
        public static string Url {
            get {
                return ResourceManager.GetString("Url", resourceCulture);
            }
        }
    }
}
