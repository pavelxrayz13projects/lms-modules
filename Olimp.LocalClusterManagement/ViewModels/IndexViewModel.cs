﻿namespace Olimp.LocalClusterManagement.ViewModels
{
    public class IndexViewModel
    {
        public bool CanManage { get; set; }

        public string CompanyName { get; set; }
    }
}