﻿using Olimp.Core.ComponentModel;
using Olimp.Core.I18N;
using Olimp.Domain.Catalogue.Nodes;
using Olimp.Domain.Common.Nodes;
using PAPI.Core;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.LocalClusterManagement.ViewModels
{
    public class NodeViewModel
    {
        public const int MaximumNodeNameLength = 30;
        public const int MaximumReadableNameLength = 500;

        private static string _companyName;

        static NodeViewModel()
        {
            var distributionInfo = PContext.ExecutingModule.Context.DistributionInfoProvider.Get();
            if (distributionInfo != null)
                _companyName = distributionInfo.CompanyName;
        }

        public NodeViewModel()
        {
            this.Url = String.Empty;
        }

        public NodeViewModel(NodeState stateInfo)
            : this()
        {
            var localNodeInfo = stateInfo.Node as LocalNode;
            var companyName = localNodeInfo != null
                ? localNodeInfo.DistributionLabel
                : (string)null;

            var quota = localNodeInfo != null ? localNodeInfo.Quota : 0;

            this.Id = stateInfo.Node.Id;
            this.Name = stateInfo.Node.Name;
            this.ReadableName = companyName ?? _companyName ?? String.Empty;
            this.Port = stateInfo.Node.Port;
            this.Quota = quota;
            this.IsLocked = Node.IsDefaultNode(stateInfo.Node);
            this.IsActive = stateInfo.IsActive;

            this.ParametersText = NodeSettingsUtility.GetNodeSettingsAsText(stateInfo.Node);
        }

        public bool IsLocked { get; set; }

        public bool IsActive { get; set; }

        public Guid? Id { get; set; }

        [StringLength(MaximumNodeNameLength, ErrorMessageResourceType = typeof(OlimpCoreResources), ErrorMessageResourceName = "NodeValidationNodeNameLengthTooLarge")]
        [Remote("ValidateNodeName", "LocalClusterManagement", "Admin", AdditionalFields = "Id", ErrorMessageResourceType = typeof(I18N.LocalClusterManagement), ErrorMessageResourceName = "NodeNameAlreadyExists")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LocalClusterManagement), DisplayNameResourceName = "Name")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public string Name { get; set; }

        [StringLength(MaximumReadableNameLength, ErrorMessageResourceType = typeof(I18N.LocalClusterManagement), ErrorMessageResourceName = "ReadableNameLengthInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LocalClusterManagement), DisplayNameResourceName = "ReadableName")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public string ReadableName { get; set; }

        public string Url { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LocalClusterManagement), DisplayNameResourceName = "Port")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [AnyTypeRegEx("^[0-9]+$", ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "NumberInvalid")]
        [Range(1, 65535, ErrorMessageResourceType = typeof(I18N.LocalClusterManagement), ErrorMessageResourceName = "PortInvalid")]
        [Remote("ValidateNodePort", "LocalClusterManagement", "Admin", AdditionalFields = "Id")]
        public int Port { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LocalClusterManagement), DisplayNameResourceName = "Quota")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [AnyTypeRegEx("^[0-9]+$", ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "NumberInvalid")]
        [Range(1, 2147483647, ErrorMessageResourceType = typeof(I18N.LocalClusterManagement), ErrorMessageResourceName = "QuotaInvalid")]
        [Remote("ValidateQuota", "LocalClusterManagement", AdditionalFields = "Id", ErrorMessageResourceType = typeof(I18N.LocalClusterManagement), ErrorMessageResourceName = "QuotaInvalid")]
        public int Quota { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LocalClusterManagement), DisplayNameResourceName = "ParametersText")]
        public string ParametersText { get; set; }

        public bool ParametersTextVisible { get { return String.IsNullOrWhiteSpace(this.ParametersText); } }
    }
}