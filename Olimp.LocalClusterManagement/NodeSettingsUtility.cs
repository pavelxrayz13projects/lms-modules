﻿using IniParser.Model;
using IniParser.Model.Configuration;
using IniParser.Parser;
using Olimp.Domain.Common.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Olimp.LocalClusterManagement
{
    public static class NodeSettingsUtility
    {
        private static readonly Regex _keyFilter = new Regex(@"^[a-zA-Z0-9]+$");
        private static readonly Regex _paramsFilter = new Regex(@"^[\w.,-_ ]+$");

        public static string GetNodeSettingsAsText(Node node)
        {
            if (node == null)
                throw new ArgumentNullException("node");

            var data = new IniData();

            if (node.Params.Any())
                SerializeToSection(data, "Params", node.Params);

            if (node.Settings.Any())
                SerializeToSection(data, "Settings", node.Settings);

            return data.ToString();
        }

        private static void SerializeToSection(IniData data, string sectionName, IDictionary<string, string> values)
        {
            data.Sections.AddSection(sectionName);

            var section = data.Sections[sectionName];

            foreach (var pair in values)
                section.AddKey(pair.Key, pair.Value);
        }

        private static bool TryGetSectionData(KeyDataCollection section, bool validateValue, out IDictionary<string, string> values)
        {
            values = new Dictionary<string, string>();

            if (section == null)
                return true;

            foreach (var pair in section)
            {
                if (!_keyFilter.IsMatch(pair.KeyName) || (validateValue && !_paramsFilter.IsMatch(pair.Value)))
                {
                    values.Clear();
                    return false;
                }

                values[pair.KeyName] = pair.Value;
            }

            return true;
        }

        public static void UpdateNodeFromParametersText(Node node, string text)
        {
            if (node == null)
                throw new ArgumentNullException("node");

            var parser = new IniDataParser(new DefaultIniParserConfiguration
            {
                ThrowExceptionsOnError = false
            });

            var data = parser.Parse(text);
            if (data == null)
                throw new InvalidOperationException(I18N.LocalClusterManagement.IncorrectIniFileFormat);

            IDictionary<string, string> settings, @params;
            if (!TryGetSectionData(data["Settings"], false, out settings) || !TryGetSectionData(data["Params"], true, out @params))
                throw new InvalidOperationException(I18N.LocalClusterManagement.IncorrectKeyValueParametersFormat);

            node.Settings.Clear();
            foreach (var s in settings)
                node.Settings[s.Key] = s.Value;

            node.Params.Clear();
            foreach (var p in @params)
                node.Params[p.Key] = p.Value;
        }
    }
}