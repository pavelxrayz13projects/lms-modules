﻿using System;

namespace Olimp.LocalClusterManagement
{
    public interface IQuotaValidator
    {
        bool Validate(Guid? id, int quota);
    }
}