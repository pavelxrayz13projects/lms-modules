﻿using log4net;
using Olimp.Service.Client;
using Olimp.Service.Contract.PingService;
using Olimp.Web.Controllers.Base;
using PAPI.Core.Logging;
using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LogManager = PAPI.Core.Logging.Log;

namespace Olimp.LocalClusterManagement.Controllers
{
    public class PingServiceController : PController
    {
        private static readonly ILog _log = LogManager.CreateLogger("servicecontroller");

        private static ILog Log { get { return _log; } }

        public ActionResult Ping(string nodeId, string url)
        {
            url = HttpUtility.UrlDecode(url);

            Log.InfoFormat("Starting processing Ping request to node '{0}' on {1}", nodeId, url);

            if (String.IsNullOrEmpty(nodeId))
                return new HttpStatusCodeResult((int)HttpStatusCode.BadRequest);

            try
            {
                using (var client = new PingServiceClient(String.Format("{0}/api/", url)))
                {
                    PingResponse res = client.Post(new PingRequest() { PingMessage = "Ping" });
                    if (res.PingReply == "Pong")
                    {
                        Log.InfoFormat("Successfully processed ping request to node '{0}' on {1}", nodeId, url);
                        return new HttpStatusCodeResult((int)HttpStatusCode.OK);
                    }
                    else
                    {
                        Log.InfoFormat("Ping request to node '{0}' on {1} returned unrecognized reply: '{2}'",
                          nodeId, url, res.PingReply);
                        return new HttpStatusCodeResult((int)HttpStatusCode.NotImplemented);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return new HttpStatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}