﻿using System.Linq;
using System.Security;
using Olimp.Core.Mvc.Security;
using Olimp.Core.Web;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Common.Nodes;
using Olimp.LocalClusterManagement.ViewModels;
using Olimp.LocalNodes.Core.Cluster;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core;
using System;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;

namespace Olimp.LocalClusterManagement.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Admin.Users.ManageGlobal + "," + 
                               Permissions.LocalCluster.Nodes.Manage + "," + 
                               Permissions.LocalCluster.Nodes.StartStop, Order = 2)]
    // [CheckActivation]
    public class LocalClusterManagementController : PController
    {
        private INodesRepository _nodeRegistry;
        private INodesTableProvider _tableProvider;
        private ILocalClusterManager _clusterManager;
        private ILocalNodeController _nodeControllerService;
        private IQuotaValidator _quotaValidator;

        //TODO: Refactor overinjection
        public LocalClusterManagementController(
            INodesRepository nodeRegistry,
            ILocalClusterManager clusterManager,
            INodesTableProvider tableProvider,
            ILocalNodeController nodeControllerService,
            IQuotaValidator quotaValidator)
        {
            if (nodeRegistry == null)
                throw new ArgumentNullException("nodeRegistry");

            if (clusterManager == null)
                throw new ArgumentNullException("clusterManager");

            if (tableProvider == null)
                throw new ArgumentNullException("tableProvider");

            if (nodeControllerService == null)
                throw new ArgumentNullException("nodeControllerService");

            if (quotaValidator == null)
                throw new ArgumentNullException("quotaValidator");

            _nodeRegistry = nodeRegistry;
            _clusterManager = clusterManager;
            _tableProvider = tableProvider;
            _nodeControllerService = nodeControllerService;
            _quotaValidator = quotaValidator;
        }

        [NonAction]
        private LocalNode CreateLocalNodeInfo(NodeViewModel model)
        {
            var node = new LocalNode(model.Id.HasValue ? model.Id.Value : Guid.NewGuid(), model.Name)
            {
                Quota = model.Quota,
                Port = model.Port,
                DistributionLabel = model.ReadableName
            };

            NodeSettingsUtility.UpdateNodeFromParametersText(node, model.ParametersText);

            return node;
        }

        [NonAction]
        private Guid? GetNodeIdFromQueryString()
        {
            Guid? id = null;

            //Somehow id isn't binded if in parameters
            var idString = this.Request.QueryString["Id"];

            Guid idValue;
            if (!String.IsNullOrWhiteSpace(idString) && Guid.TryParse(idString, out idValue))
                id = idValue;

            return id;
        }

        [NonAction]
        private bool HasPermissionToActiveOrDeactivate()
        {
            return RolesHelper.GetCurrentUserRoles(ControllerContext)
                              .Contains(Permissions.LocalCluster.Nodes.StartStop);
        }

        [HttpGet]
        [ValidateInput(false)]
        public ActionResult ValidateNodeName(string name)
        {
            var id = this.GetNodeIdFromQueryString();

            if (id.HasValue && Node.IsDefaultNodeId(id.Value) && Node.IsDefaultNodeName(name))
                return Json(true, JsonRequestBehavior.AllowGet);

            LocalNodeValidator validator = new LocalNodeValidator();
            string nodeValidationError;
            if (!validator.ValidateNodeName(name, id, _nodeRegistry, out nodeValidationError))
                return Json(nodeValidationError, JsonRequestBehavior.AllowGet);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ValidateNodePort(int port)
        {
            var id = this.GetNodeIdFromQueryString();

            LocalNodeValidator validator = new LocalNodeValidator();
            string nodeValidationError;
            if (!validator.ValidateNodePort(port, id, _nodeRegistry, out nodeValidationError))
                return Json(nodeValidationError, JsonRequestBehavior.AllowGet);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateQuota(int quota)
        {
            var id = this.GetNodeIdFromQueryString();
            var result = _quotaValidator.Validate(id, quota);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            var model = new IndexViewModel
            {
                CanManage = Roles.IsUserInRole(Permissions.LocalCluster.Nodes.Manage)
            };

            var distributionInfo = PContext.ExecutingModule.Context.DistributionInfoProvider.Get();
            if (distributionInfo != null)
                model.CompanyName = distributionInfo.CompanyName;

            return View(model);
        }

        [HttpPost]
        public ActionResult GetNodes(TableViewModel model)
        {
            var tableData = _tableProvider.GetTable()
                .GetTableData(model, n => n);

            return Json(tableData);
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.LocalCluster.Nodes.Manage, Order = 2)]
        public ActionResult Add(NodeViewModel model)
        {
            if (!_quotaValidator.Validate(model.Id, model.Quota))
                return new StatusCodeResultWithText((int)HttpStatusCode.Conflict, I18N.LocalClusterManagement.QuotaInvalid);

            var localNode = this.CreateLocalNodeInfo(model);
            LocalNodeValidator validator = new LocalNodeValidator();
            string nodeValidationError;
            if (!validator.ValidateNode(localNode, _nodeRegistry, out nodeValidationError))
                return new StatusCodeResultWithText((int)HttpStatusCode.Conflict, nodeValidationError);

            _clusterManager.Add(localNode);

            var activationResult = _nodeControllerService.Activate(localNode.Name);
            if (activationResult != NodeControllerResult.Success)
                return new StatusCodeResultWithText((int)HttpStatusCode.InternalServerError, activationResult.GetMessage(model.Name));

            return null;
        }

        [HttpPost]
        public ActionResult Deactivate(string name)
        {
            if (!HasPermissionToActiveOrDeactivate())
                throw new SecurityException(Olimp.Web.Controllers.I18N.Auth.UserNotInRoleAuthError);

            var result = _nodeControllerService.Deactivate(name);
            if (result != NodeControllerResult.Success)
                return new StatusCodeResultWithText((int)HttpStatusCode.InternalServerError, result.GetMessage(name));

            return Json(result == NodeControllerResult.Success);
        }

        [HttpPost]
        public ActionResult Activate(string name)
        {
            if (!HasPermissionToActiveOrDeactivate())
                throw new SecurityException(Olimp.Web.Controllers.I18N.Auth.UserNotInRoleAuthError);

            var result = _nodeControllerService.Activate(name);
            if (result != NodeControllerResult.Success)
                return new StatusCodeResultWithText((int)System.Net.HttpStatusCode.InternalServerError, result.GetMessage(name));

            return Json(result == NodeControllerResult.Success);
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.LocalCluster.Nodes.Manage, Order = 2)]
        public ActionResult Update(NodeViewModel model)
        {
            var node = _nodeRegistry.GetByName(model.Name);

            var keyContainer = node as IActivatableNode;

            if (keyContainer == null)
                return new StatusCodeResultWithText((int)System.Net.HttpStatusCode.InternalServerError, I18N.LocalClusterManagement.NodeNotFound);

            if (node is DefaultNode)
            {
                node.Port = model.Port;
                keyContainer.DistributionLabel = model.ReadableName;

                try
                {
                    NodeSettingsUtility.UpdateNodeFromParametersText(node, model.ParametersText);
                }
                catch (Exception ee)
                {
                    return Json(new { error = ee.Message });
                }
            }
            else
            {
                if (!_quotaValidator.Validate(model.Id, model.Quota))
                    return new StatusCodeResultWithText((int)System.Net.HttpStatusCode.Conflict, I18N.LocalClusterManagement.QuotaInvalid);

                LocalNode localNode;

                try
                {
                    localNode = this.CreateLocalNodeInfo(model);
                }
                catch (Exception ee)
                {
                    return new StatusCodeResultWithText(500, ee.Message);
                }

                localNode.ActivationKey = keyContainer.ActivationKey;

                LocalNodeValidator validator = new LocalNodeValidator();
                string nodeValidationError;
                if (!validator.ValidateNode(localNode, _nodeRegistry, out nodeValidationError))
                    return new StatusCodeResultWithText((int)System.Net.HttpStatusCode.Conflict, nodeValidationError);

                node = localNode;
            }

            var result = _nodeControllerService.UpdateNodeInfo(node);
            if (result != NodeControllerResult.Success)
                return new StatusCodeResultWithText((int)System.Net.HttpStatusCode.InternalServerError, result.GetMessage(model.Name));

            return null;
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.LocalCluster.Nodes.Manage, Order = 2)]
        // Table's DeleteAction submits "id" parameter
        public ActionResult Delete([System.Web.Mvc.Bind(Prefix = "id")] string name)
        {
            var result = _nodeControllerService.Deactivate(name);
            if (result != NodeControllerResult.Success
                // Остановленный узел нельзя деактивировать, активный ранее узел могли деактивировать в другом сеансе
                && result != NodeControllerResult.InvalidNodeState
                // Узел могли удалить в другом сеансе
                && result != NodeControllerResult.NodeNotFound)
            {
                throw new ApplicationException(
                    String.Format("Failed to deactivate node '{0}' - service has returned '{1}'", name, result));
            }
            _clusterManager.Remove(name);

            return null;
        }
    }
}