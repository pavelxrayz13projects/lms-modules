﻿using log4net;
using Olimp.Domain.Common.Nodes;
using Olimp.LocalClusterManagement.Properties;
using Olimp.Logic.Converters.Attempts;
using Olimp.Service.Client;
using Olimp.Service.Contract.SyncService.Attempts;
using Olimp.Web.Controllers.Base;
using PAPI.Core;
using PAPI.Core.Data;
using PAPI.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;
using LogManager = PAPI.Core.Logging.Log;

namespace Olimp.LocalClusterManagement.Controllers
{
    public class SyncServiceController : PController
    {
        private static readonly ILog _log = LogManager.CreateLogger("servicecontroller");

        private static ILog Log { get { return _log; } }

        private readonly INodesRepository _nodesRepository;

        public SyncServiceController(INodesRepository nodesRepository)
        {
            if (nodesRepository == null)
                throw new ArgumentNullException("nodesRepository");

            _nodesRepository = nodesRepository;
        }

        public ActionResult SyncAttemptResults(Guid nodeId, string url)
        {
            url = HttpUtility.UrlDecode(url);

            Log.InfoFormat("Starting processing SyncAttemptResults request to node '{0}' on {1}", nodeId, url);

            try
            {
                DateTime? lastSyncAttemptFinishedTime;
                GetNodeAttemptsLastSyncParams(nodeId, out lastSyncAttemptFinishedTime);

                SyncFinishedAttemptsResponse res = null;
                using (var client = new SyncAttemptsServiceClient(String.Format("{0}/api/", url)))
                {
                    res = client.Post(new SyncFinishedAttempts()
                      {
                          LastSyncAttemptFinishedTime = lastSyncAttemptFinishedTime,
                          MaxCountToReturn = Settings.Default.SyncServiceControllerFinishedAttemptsMaxResultCount
                      });
                }

                if (res.Attempts != null)
                {
                    ProcessAttempts(nodeId, res.Attempts);
                    Log.InfoFormat(
                      "Successfully processed sync/attempts request to node '{0}' on {1} ({2} attempt(s) synced)",
                      nodeId, url, res.Attempts.Count);
                    return new HttpStatusCodeResult((int)HttpStatusCode.OK);
                }
                else
                {
                    Log.InfoFormat("Sync/attempts request to node '{0}' on {1} returned unexpected reply: 'Attempts = null'",
                      nodeId, url);
                    return new HttpStatusCodeResult((int)HttpStatusCode.NotImplemented);
                }
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return new HttpStatusCodeResult((int)HttpStatusCode.InternalServerError);
            }
        }

        [NonAction]
        private void GetNodeAttemptsLastSyncParams(Guid nodeId, out DateTime? lastSyncFinishedTime)
        {
            var attempts = Repository.Of<Entities.ExamAttemptSchema, Guid>();
            var sites = Repository.Of<Entities.ForeignNodeSiteSchema, int>();

            using (UnitOfWork.CreateRead())
            {
                lastSyncFinishedTime = sites.Join(attempts, s => s.Id, a => a.SiteMapped.Id, (s, a) => new { NodeId = s.NodeId, NodeType = s.NodeType, FinishTime = a.FinishTime })
                    .Where(o => o.NodeId == nodeId && o.NodeType == NodeType.Local)
                    .Max(o => o.FinishTime);
            }
        }

        [NonAction]
        private void ProcessAttempts(Guid nodeId, IList<Attempt> list)
        {
            if (list == null)
                throw new ArgumentNullException("list");

            var node = _nodesRepository.GetById(nodeId);
            if (node == null)
                throw new InvalidOperationException("Node doesn't exist");

            if (!(node is LocalNode))
                throw new NotSupportedException("Synchronization is allowed only from local node to default node");

            if (!list.Any())
                return;

            IList<Entities.ExamAttemptSchema> result;

            try
            {
                var sites = Repository.Of<Entities.ForeignNodeSiteSchema>();
                var attempts = Repository.Of<Entities.ExamAttemptSchema, Guid>();

                using (var uow = UnitOfWork.CreateWrite())
                {
                    var site = sites.FirstOrDefault(s => s.NodeId == nodeId && s.NodeType == NodeType.Local);
                    if (site == null)
                    {
                        site = new Entities.ForeignNodeSiteSchema
                        {
                            NodeId = nodeId,
                            NodeType = NodeType.Local
                        };
                    }

                    using (var converter = new BatchAttemptsConverter(PContext.ExecutingModule.Database.RepositoryFactory, site))
                    {
                        result = converter.ConvertAll(list);
                    }

                    if (result != null && result.Any())
                        uow.Commit();
                }
            }
            catch (Exception e)
            {
                Log.Error("Error occured while processing synchronization: " + e.ToString());
                throw;
            }
        }
    }
}