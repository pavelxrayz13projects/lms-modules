﻿using PAPI.Core;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace ResetClusterAdminAccount
{
    internal class ElevationHelper
    {
        public static bool SupportsElevation
        {
            get { return Platform.IsWindows && Environment.OSVersion.Version.Major >= 6; }
        }

        public static bool TryEnsureElevation()
        {
            if (!ElevationHelper.SupportsElevation)
                return false;

            if (ElevationHelper.IsProcessElevated())
                return false;

            return ElevationHelper.TryElevate();
        }

        public static bool TryElevate()
        {
            var args = Platform.CommandLineArgs
                .Select(a => a.Contains(" ") ? String.Format(@"""{0}""", a) : a);

            var startInfo = new ProcessStartInfo
            {
                UseShellExecute = true,
                WorkingDirectory = PlatformPaths.Default.Root,
                FileName = PlatformPaths.Default.ExecutablePath,
                Arguments = String.Join(" ", args),
                Verb = "runas"
            };

            try
            {
                Process.Start(startInfo);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsProcessElevated()
        {
            var isElevated = false;
            SafeTokenHandle hToken = null;
            var tokenElevationHandle = IntPtr.Zero;

            try
            {
                // Open the access token of the current process with TOKEN_QUERY.
                if (!NativeMethods.OpenProcessToken(Process.GetCurrentProcess().Handle, NativeMethods.TOKEN_QUERY, out hToken))
                    throw new Win32Exception(Marshal.GetLastWin32Error());

                // Allocate a buffer for the elevation information.
                var tokenElevation = Marshal.SizeOf(typeof(NativeMethods.TOKEN_ELEVATION));
                tokenElevationHandle = Marshal.AllocHGlobal(tokenElevation);
                if (tokenElevationHandle == IntPtr.Zero)
                    throw new Win32Exception(Marshal.GetLastWin32Error());

                // Retrieve token elevation information.
                if (!NativeMethods.GetTokenInformation(hToken, NativeMethods.TOKEN_INFORMATION_CLASS.TokenElevation, tokenElevationHandle, tokenElevation, out tokenElevation))
                {
                    // When the process is run on operating systems prior to Windows
                    // Vista, GetTokenInformation returns false with the error code
                    // ERROR_INVALID_PARAMETER because TokenElevation is not supported
                    // on those operating systems.
                    throw new Win32Exception(Marshal.GetLastWin32Error());
                }

                // Marshal the TOKEN_ELEVATION struct from native to .NET object.
                var elevation = (NativeMethods.TOKEN_ELEVATION)Marshal.PtrToStructure(tokenElevationHandle, typeof(NativeMethods.TOKEN_ELEVATION));

                // TOKEN_ELEVATION.TokenIsElevated is a non-zero value if the token
                // has elevated privileges; otherwise, a zero value.
                isElevated = (elevation.TokenIsElevated != 0);
            }
            finally
            {
                // Centralized cleanup for all allocated resources.
                if (hToken != null)
                {
                    hToken.Close();
                    hToken = null;
                }

                if (tokenElevationHandle != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(tokenElevationHandle);
                    tokenElevationHandle = IntPtr.Zero;
                }
            }

            return isElevated;
        }
    }
}