﻿using System.Globalization;
using System.Threading;

namespace ResetClusterAdminAccount
{
    internal class ThreadCultureHelper
    {
        public static void SetCulture(string name)
        {
            Thread.CurrentThread.CurrentCulture =
                Thread.CurrentThread.CurrentUICulture =
                    CultureInfo.GetCultureInfo(name);
        }
    }
}