using log4net;
using Olimp.Domain.Catalogue.Ldap;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.GlobalStorage;
using Olimp.Domain.Catalogue.OrmLite.Ldap;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Common;
using Olimp.Domain.Common.Security;
using PAPI.Core;
using PAPI.Core.Logging;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace ResetClusterAdminAccount
{
    public class Runner : IModuleRunner
    {
        //Form is to put MessageBoxes on top after elevation dialog
        private class FakeForm : Form
        {
            private Runner _currentRunner;
            private ILog _log;

            public FakeForm(Runner currentRunner, ILog log)
            {
                _currentRunner = currentRunner;
                _log = log;

                this.TopMost = true;
            }

            private DialogResult ShowMessage(
                string message,
                string title,
                MessageBoxIcon icon,
                MessageBoxButtons buttons = MessageBoxButtons.OK,
                MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
            {
                return MessageBox.Show(this, message, title, buttons, icon, defaultButton, (MessageBoxOptions)0x40000);
            }

            protected override void OnLoad(EventArgs e)
            {
                base.OnLoad(e);

                var questionResult = this.ShowMessage(
                    Messages.ShouldResetSys, Messages.ShouldResetTitle, MessageBoxIcon.Question, MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button2);

                if (questionResult != DialogResult.Yes)
                {
                    _log.Info("User refused to reset password");
                    this.Close();

                    return;
                }

                var stopped = _currentRunner.StartStopService(true);

                try
                {
                    var authSchemeRepo = _currentRunner.GetAuthSchemeRepository();
                    authSchemeRepo.SetCurrentFromEmployers(null);
                    authSchemeRepo.SetCurrentFromSystemUsers(null);

                    var usersRepo = _currentRunner.GetUserRepository();

                    var user = usersRepo.GetByLogin(TargetLogin, false);
                    if (user != null)
                        usersRepo.DeleteGivenVersion(user);

                    var role = _currentRunner.RolesProvider.GetById(TargetRole);
                    if (role == null)
                        throw new InvalidOperationException("Invalid role: " + TargetRole);

                    var newUser = _currentRunner.UserFactory.Create(TargetLogin, TargetPassword, true);
                    newUser.Roles.Add(role);

                    usersRepo.Add(newUser);

                    this.ShowMessage(
                        String.Format(Messages.SysHasBeenResetFormat, TargetLogin, TargetPassword), Messages.SysHasBeenResetTitle, MessageBoxIcon.Information);
                }
                catch (Exception ee)
                {
                    _log.Error(ee);

                    this.ShowMessage(
                        String.Format(Messages.Error, TargetLogin, TargetPassword), Messages.ErrorTitle, MessageBoxIcon.Error);
                }
                finally
                {
                    if (!_currentRunner.StartStopService(false))
                    {
                        this.ShowMessage(
                            Messages.FailedToStart, Messages.ErrorTitle, MessageBoxIcon.Error);
                    }
                    else if (!stopped)
                    {
                        this.ShowMessage(
                            Messages.FailedToStop, Messages.WarningTitle, MessageBoxIcon.Warning);
                    }
                }

                this.Close();
            }
        }

        private const string TargetLogin = "sys";
        private const string TargetPassword = "123";
        private const string TargetRole = "ClusterAdmin";

        private IDbConnectionFactory _connectionFactory;

        internal IRolesProvider RolesProvider;
        internal IUserFactory UserFactory;

        public Runner()
        {
            RolesProvider = new IntegratedRolesProvider();
            UserFactory = new DefaultUserFactory();

            var connectionFactoryFactory = new SqliteConnectionFactoryFactory();
            _connectionFactory = connectionFactoryFactory.Create();

            GlobalConfiguration.SetStorage(new OrmLiteJsonStorage(_connectionFactory));
        }

        private IUserRepository GetUserRepository()
        {
            var nodeRepository = new OrmLiteNodeRepository(_connectionFactory);

            return new OrmLiteUserRepository(Platform.DefaultNode, _connectionFactory, nodeRepository, RolesProvider);
        }

        private ILdapAuthSchemeRepository GetAuthSchemeRepository()
        {
            return new OrmLiteLdapAuthSchemeRepository(_connectionFactory);
        }

        private bool StartStopService(bool stop)
        {
            var args = stop ? "stop" : "start";

            string executable;
            if (Platform.IsWindows)
            {
                executable = "net.exe";
                args += " OlimpService";
            }
            else
            {
                executable = "/etc/init.d/olimpd";
            }

            var proc = Process.Start(new ProcessStartInfo
            {
                UseShellExecute = true,
                FileName = executable,
                Arguments = args
            });

            proc.WaitForExit();

            return proc.ExitCode == 0;
        }

        public void Run(IModule module, IDictionary<string, string> moduleParams)
        {
            if (ElevationHelper.TryEnsureElevation())
                return;

            var thread = new Thread(() =>
            {
                var log = Log.CreateLogger("reset-sys-acc");

                ThreadCultureHelper.SetCulture("ru-RU");

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FakeForm(this, log));
            });

            thread.SetApartmentState(ApartmentState.STA);

            thread.Start();
        }
    }
}