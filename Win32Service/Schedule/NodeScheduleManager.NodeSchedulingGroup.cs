﻿using Olimp.Domain.Catalogue.Nodes;
using Quartz;
using System;

namespace Win32Service.Schedule
{
    public partial class NodeScheduleManager
    {
        #region Nested class NodeSchedulingGroup

        public class NodeSchedulingGroup
        {
            #region Static Members

            private static readonly NodeSchedulingGroup _empty = new NodeSchedulingGroup("");

            public static NodeSchedulingGroup Empty
            {
                get { return _empty; }
            }

            static NodeSchedulingGroup()
            {
                NewSchedulingGroup = (name, cronSchedule, nodeDataProvider, jobType, shouldRunOnNode, shouldRescheduleOnNodeStateChange)
                  => new NodeSchedulingGroup(
                      name,
                      cronSchedule,
                      s => nodeDataProvider.Invoke(s.Node),
                      jobType,
                      shouldRunOnNode,
                      shouldRescheduleOnNodeStateChange);
            }

            #endregion Static Members

            #region Fields

            private readonly string _name;
            private readonly Func<NodeState, JobDataMap> _nodeDataProvider;
            private readonly string _cronSchedule;
            private readonly Type _jobType;
            private readonly Func<NodeState, bool> _shouldRunOnNode;
            private readonly Func<LocalNodeStateInfoChanged, bool> _shouldRescheduleOnNodeStateChange;

            #endregion Fields

            #region Constructors

            private NodeSchedulingGroup(string name)
            {
                _name = name;
            }

            private NodeSchedulingGroup(string name, string cronSchedule, Func<NodeState, JobDataMap> nodeDataProvider, Type jobType,
              Func<NodeState, bool> shouldRunOnNode, Func<LocalNodeStateInfoChanged, bool> shouldRescheduleOnNodeStateChange)
                : this(name)
            {
                if (String.IsNullOrWhiteSpace(name))
                    throw new ArgumentException("name is null or empty or whitespace string");
                if (nodeDataProvider == null)
                    throw new ArgumentNullException("nodeDataProvider");
                if (String.IsNullOrWhiteSpace(cronSchedule))
                    throw new ArgumentException("cronSchedule is null or empty or whitespace string");
                if (jobType == null)
                    throw new ArgumentNullException("jobType");
                if (shouldRunOnNode == null)
                    throw new ArgumentNullException("shouldRunOnNode");
                if (shouldRescheduleOnNodeStateChange == null)
                    throw new ArgumentNullException("shouldRescheduleOnChange");

                _nodeDataProvider = nodeDataProvider;
                _cronSchedule = cronSchedule;
                _jobType = jobType;
                _shouldRunOnNode = shouldRunOnNode;
                _shouldRescheduleOnNodeStateChange = shouldRescheduleOnNodeStateChange;
            }

            #endregion Constructors

            #region Properties

            public string Name
            {
                get { return _name; }
            }

            public Func<NodeState, JobDataMap> NodeDataProvider
            {
                get { return _nodeDataProvider; }
            }

            public string CronSchedule
            {
                get { return _cronSchedule; }
            }

            public Type JobType
            {
                get { return _jobType; }
            }

            public Func<NodeState, bool> ShouldRunOnNode
            {
                get { return _shouldRunOnNode; }
            }

            public Func<LocalNodeStateInfoChanged, bool> ShouldRescheduleOnNodeStateChange
            {
                get { return _shouldRescheduleOnNodeStateChange; }
            }

            #endregion Properties

            #region Overridden Members

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(this, obj))
                    return true;

                if (obj == null || !(obj is NodeSchedulingGroup))
                    return false;

                NodeSchedulingGroup other = (NodeSchedulingGroup)obj;
                return Equals(this.Name, other.Name);
            }

            public override int GetHashCode()
            {
                return Name.GetHashCode();
            }

            #endregion Overridden Members
        }

        #endregion Nested class NodeSchedulingGroup
    }
}