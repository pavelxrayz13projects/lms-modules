﻿using log4net;
using Olimp.Domain.Catalogue.Nodes;
using Olimp.Domain.Common.Nodes;
using Olimp.LocalNodes.Core.Cluster;
using Quartz;
using Quartz.Impl.Matchers;
using System;
using System.Collections.Generic;
using System.Linq;
using Win32Service.Properties;
using LogManager = PAPI.Core.Logging.Log;

namespace Win32Service.Schedule
{
    public partial class NodeScheduleManager
    {
        #region Static members

        private static readonly ILog _log = LogManager.CreateLogger("scheduler");

        private static ILog Log { get { return _log; } }

        private static readonly string allNodeJobsGroupName = "olimp.scheduler.job.node";

        private static readonly NodeSchedulingGroup _dummy = NodeSchedulingGroup.Empty;

        private static Func<string, string, Func<Node, JobDataMap>, Type, Func<NodeState, bool>, Func<LocalNodeStateInfoChanged, bool>, NodeSchedulingGroup> NewSchedulingGroup;

        // TODO: Переделать на GUID.
        private static Func<Node, string> NodeInfoIdSelector = node => node.Name;

        public static NodeSchedulingGroup CreateNodeJobGroup<TJob>(string name, string cronSchedule, Func<Node, JobDataMap> nodeDataProvider,
          Func<NodeState, bool> shouldRunOnNode, Func<LocalNodeStateInfoChanged, bool> shouldRescheduleOnNodeStateChange)
          where TJob : IJob
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentException("Parameter 'name' is null or empty.");
            if (name.Contains("."))
                throw new ArgumentException("Parameter 'name' should not contains period symbol ('.').");

            return NewSchedulingGroup(allNodeJobsGroupName + "." + name, cronSchedule, nodeDataProvider, typeof(TJob), shouldRunOnNode, shouldRescheduleOnNodeStateChange);
        }

        public static JobKey CreateNodeJobKey(Node nodeInfo, NodeSchedulingGroup group)
        {
            return new JobKey(NodeInfoIdSelector(nodeInfo), group.Name);
        }

        private static NodeState[] GetNonExternalNodes(ILocalNodeController service)
        {
            return service.GetAllNodes()
              .Where(n => n.Node is LocalNode || n.Node is DefaultNode)
              .ToArray();
        }

        #endregion Static members

        #region Fields

        private readonly IScheduler _scheduler;
        private readonly ILocalNodeController _nodeControllerService;
        private readonly ISet<NodeSchedulingGroup> _groups = new HashSet<NodeSchedulingGroup>();
        private readonly object _groupsSync = new object();

        #endregion Fields

        #region Constructor

        public NodeScheduleManager(IScheduler scheduler, ILocalNodeController nodeControllerService)
        {
            if (scheduler == null)
                throw new ArgumentNullException("scheduler");

            if (nodeControllerService == null)
                throw new ArgumentNullException("nodeControllerService");

            _scheduler = scheduler;
            _nodeControllerService = nodeControllerService;

            NodeChecker.Init(this, GetNonExternalNodes(_nodeControllerService));
            StartInternalNodeCheckJob();
        }

        #endregion Constructor

        public NodeSchedulingGroup GetNodeSchedulingGroup(string name)
        {
            if (String.IsNullOrWhiteSpace(name))
                throw new ArgumentException("'name' is null or empty string");

            string groupName = allNodeJobsGroupName + "." + name;
            lock (_groupsSync)
            {
                return _groups.FirstOrDefault(g => g.Name == groupName);
            }
        }

        #region Schedule/Unschedule Methods

        public void ScheduleNodeJobs(NodeSchedulingGroup group)
        {
            ScheduleNodeJobs(group, NodeChecker.Nodes);
        }

        private void ScheduleNodeJobs(NodeSchedulingGroup group, params NodeState[] nodes)
        {
            var matchedNodes = nodes.Where(node => group.ShouldRunOnNode(node)).ToArray();

            IJobDetail[] jobs = new IJobDetail[matchedNodes.Length];

            for (int i = 0; i < matchedNodes.Length; i++)
            {
                IJobDetail job = JobBuilder.Create(group.JobType)
                  .WithIdentity(CreateNodeJobKey(matchedNodes[i].Node, group))
                  .UsingJobData(group.NodeDataProvider(matchedNodes[i]))
                  .Build();
                jobs[i] = job;
            }

            lock (_groupsSync)
            {
                _groups.Add(group);

                foreach (var job in jobs)
                {
                    Quartz.ITrigger trigger = TriggerBuilder.Create()
                      .WithIdentity(group.Name, job.Key.Name)
                      .WithCronSchedule(group.CronSchedule)
                      .Build();

                    SchedulerManager.Scheduler.ScheduleJob(job, trigger);
                }
            }
        }

        public void UnscheduleNodeJobs(NodeSchedulingGroup group)
        {
            if (group == null)
                throw new ArgumentNullException("group");

            lock (_groupsSync)
            {
                if (!_groups.Contains(group))
                    throw new InvalidOperationException(String.Format("NodeSchedulingGroup with name {0} is not scheduled.", group.Name));
                _scheduler.DeleteJobs(_scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(group.Name)).ToList());
                _groups.Remove(group);
            }
        }

        private void UnscheduleNodeJobs(params NodeState[] nodes)
        {
            var triggerKeys = nodes.SelectMany(node =>
                _scheduler.GetTriggerKeys(GroupMatcher<TriggerKey>.GroupEquals(NodeInfoIdSelector(node.Node))));
            // Если работа имела только один триггер, то она удаляется из шедулера.
            _scheduler.UnscheduleJobs(triggerKeys.ToArray());
        }

        private void UnscheduleNodeJobs(NodeSchedulingGroup group, params NodeState[] nodes)
        {
            lock (_groupsSync)
            {
                if (!_groups.Contains(group))
                    throw new InvalidOperationException(String.Format("NodeSchedulingGroup with name {0} is not scheduled.", group.Name));
            }
            var nodeKeys = nodes.Where(node => group.ShouldRunOnNode(node))
                .Select(node => NodeInfoIdSelector(node.Node));

            var jobKeys = _scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(group.Name)).Where(jobKey => nodeKeys.Contains(jobKey.Name));
            // Если работа имела только один триггер, то она удаляется из шедулера.
            _scheduler.DeleteJobs(jobKeys.ToArray());
        }

        #endregion Schedule/Unschedule Methods

        private void StartInternalNodeCheckJob()
        {
            IJobDetail job = JobBuilder.Create<NodeChecker>()
              .WithIdentity("NodeChecker")
              .Build();

            Quartz.ITrigger trigger = TriggerBuilder.Create()
              .WithIdentity("NodeChecker")
              .WithCronSchedule(Settings.Default.NodeSchedulerManagerNodeCheckerSchedule)
              .Build();

            SchedulerManager.Scheduler.ScheduleJob(job, trigger);
        }

        private void OnNodeStatesChanged(NodeStatesChangedEventArgs ea)
        {
            Log.InfoFormat("{0} nodes were changed: ({1})", ea.ChangedNodes.Count(), String.Join(", ", ea.ChangedNodes.Select(cn => cn.OldState.Node.Name)));

            UnscheduleNodeJobs(ea.DeletedNodes);

            NodeSchedulingGroup[] groups;
            lock (_groupsSync)
            {
                groups = _groups.ToArray();
            }

            foreach (var group in groups)
            {
                ScheduleNodeJobs(group, ea.AddedNodes);
                var changedNodes = ea.ChangedNodes.Where(cn => group.ShouldRescheduleOnNodeStateChange(cn));
                if (changedNodes.Any())
                {
                    Log.InfoFormat("Nodes to unschedule: ({0})", String.Join(", ", changedNodes.Select(cn => cn.OldState.Node.Name)));
                    UnscheduleNodeJobs(group, changedNodes.Select(cn => cn.OldState).ToArray());
                    ScheduleNodeJobs(group, changedNodes.Select(cn => cn.NewState).ToArray());
                }
            }
        }
    }
}