﻿using log4net;
using PAPI.Core.Logging;
using Quartz;
using System;
using System.Net;
using LogManager = PAPI.Core.Logging.Log;

namespace Win32Service.Schedule.Jobs
{
    internal class NodeWebRequestJob : IJob
    {
        private static readonly ILog _log = LogManager.CreateLogger("scheduler");

        private static ILog Log { get { return _log; } }

        public void Execute(IJobExecutionContext context)
        {
            NodeWebRequestJobParams requestParams = NodeWebRequestJobParams.FromJobDataMap(context.MergedJobDataMap);

            WebRequest req = WebRequest.Create(requestParams.Url);
            HttpWebResponse resp = null;
            try
            {
                resp = (HttpWebResponse)req.GetResponse();
                if (resp.StatusCode != HttpStatusCode.OK)
                {
                    Log.ErrorFormat("Job '{0}' request to node {1} on url {2} results in status code {3} with following description: {4}",
                      requestParams.JobTitle,
                      requestParams.NodeId,
                      requestParams.Url,
                      resp.StatusCode,
                      resp.StatusDescription);
                }
            }
            catch (Exception e)
            {
                Log.ErrorFormat("Job '{0}' request to node {1} on url {2} results in error: {3}",
                  requestParams.JobTitle,
                  requestParams.NodeId,
                  requestParams.Url,
                  e.ToString());
            }
            finally
            {
                if (resp != null)
                    resp.Close();
            }
        }
    }
}