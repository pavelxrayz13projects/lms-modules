﻿using Quartz;
using System;
using System.Collections.Generic;

namespace Win32Service.Schedule.Jobs
{
    internal class NodeWebRequestJobParams
    {
        private string _url;
        private string _nodeId;
        private string _jobTitle;

        public NodeWebRequestJobParams(string url, string nodeId, string jobTitle)
        {
            if (url == null)
                throw new ArgumentNullException("url");
            if (nodeId == null)
                throw new ArgumentNullException("nodeId");
            if (jobTitle == null)
                throw new ArgumentNullException("jobTitle");

            _url = url;
            _nodeId = nodeId;
            _jobTitle = jobTitle;
        }

        public string Url
        {
            get { return _url; }
        }

        public string NodeId
        {
            get { return _nodeId; }
        }

        public string JobTitle
        {
            get { return _jobTitle; }
        }

        public static NodeWebRequestJobParams FromJobDataMap(JobDataMap dataMap)
        {
            return new NodeWebRequestJobParams(dataMap.GetString("Url"), dataMap.GetString("NodeId"), dataMap.GetString("JobTitle"));
        }

        public JobDataMap ToJobDataMap()
        {
            IDictionary<string, object> map = new Dictionary<string, object> { { "Url", Url }, { "NodeId", NodeId }, { "JobTitle", JobTitle } };
            return new JobDataMap(map);
        }
    }
}