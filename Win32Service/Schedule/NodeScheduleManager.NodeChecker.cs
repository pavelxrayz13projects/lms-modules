﻿using log4net;
using Olimp.Domain.Catalogue.Nodes;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using LogManager = PAPI.Core.Logging.Log;

namespace Win32Service.Schedule
{
    public partial class NodeScheduleManager
    {
        #region Nested class NodeChecker

        private class NodeChecker : IJob
        {
            private static NodeScheduleManager _parent;
            private static NodeState[] _nodes;
            private static readonly object _nodesSync = new object();
            private static readonly ILog _log = LogManager.CreateLogger("scheduler");

            private static ILog Log { get { return _log; } }

            internal static NodeScheduleManager Parent
            {
                get
                {
                    if (_parent == null)
                        throw new InvalidOperationException("Class NodeChecker hasn't been inited. Use Init() method first.");

                    return _parent;
                }
            }

            internal static NodeState[] Nodes
            {
                get
                {
                    if (_nodes == null)
                        throw new InvalidOperationException("Class NodeChecker hasn't been inited. Use Init() method first.");

                    lock (_nodesSync)
                    {
                        return (NodeState[])_nodes.Clone();
                    }
                }
                private set
                {
                    if (value == null)
                        throw new ArgumentNullException("value");
                    lock (_nodesSync)
                    {
                        _nodes = value;
                    }
                }
            }

            internal static void Init(NodeScheduleManager parent, NodeState[] nodes)
            {
                if (parent == null)
                    throw new ArgumentNullException("parent");

                _parent = parent;
                Nodes = nodes;
            }

            public void Execute(IJobExecutionContext context)
            {
                try
                {
                    UpdateNodeStateInfos();
                }
                catch (Exception e)
                {
                    _log.Error(e.ToString());
                }
            }

            private void UpdateNodeStateInfos()
            {
                NodeState[] newNodes = NodeScheduleManager.GetNonExternalNodes(Parent._nodeControllerService);
                NodeState[] oldNodes = Nodes;
                LocalNodeStateInfoEqualityByIdComparer nodeIdComparer = new LocalNodeStateInfoEqualityByIdComparer();
                LocalNodeStateInfoEqualityComparer nodeComparer = new LocalNodeStateInfoEqualityComparer();
                NodeState[] addedNodes = newNodes.Except(oldNodes, nodeIdComparer).ToArray();
                NodeState[] deletedNodes = oldNodes.Except(newNodes, nodeIdComparer).ToArray();

                var changedNodes = newNodes.Join(oldNodes,
                        newNode => NodeScheduleManager.NodeInfoIdSelector(newNode.Node),
                        oldNode => NodeScheduleManager.NodeInfoIdSelector(oldNode.Node),
                        (newNode, oldNode) => new LocalNodeStateInfoChanged(oldNode, newNode))
                    .Where(i => !nodeComparer.Equals(i.OldState, i.NewState));

                if (addedNodes.Any() || deletedNodes.Any() || changedNodes.Any())
                {
                    NodeStatesChangedEventArgs ea = new NodeStatesChangedEventArgs(addedNodes, deletedNodes, changedNodes.ToArray());
                    Parent.OnNodeStatesChanged(ea);
                }
                Nodes = newNodes;
            }
        }

        #endregion Nested class NodeChecker

        #region Nested class LocalNodeStateInfoChanged

        public class LocalNodeStateInfoChanged
        {
            public readonly NodeState _oldState;
            public readonly NodeState _newState;

            public LocalNodeStateInfoChanged(NodeState oldState, NodeState newState)
            {
                if (oldState == null)
                    throw new ArgumentNullException("oldState");
                if (newState == null)
                    throw new ArgumentNullException("newState");

                _oldState = oldState;
                _newState = newState;
            }

            public NodeState OldState { get { return _oldState; } }

            public NodeState NewState { get { return _newState; } }

            public bool IsActiveChanged
            {
                get { return !Equals(NewState.IsActive, OldState.IsActive); }
            }

            public bool IsAliveChanged
            {
                get { return !Equals(NewState.IsAlive, OldState.IsAlive); }
            }

            public bool IsPortChanged
            {
                get { return !Equals(NewState.Node.Port, OldState.Node.Port); }
            }
        }

        #endregion Nested class LocalNodeStateInfoChanged

        #region Nested class NodeStateChangesEventArgs

        private class NodeStatesChangedEventArgs : EventArgs
        {
            private readonly NodeState[] _addedNodes;
            private readonly NodeState[] _deletedNodes;
            private readonly LocalNodeStateInfoChanged[] _changedNodes;

            public NodeStatesChangedEventArgs(NodeState[] addedNodes, NodeState[] deletedNodes, LocalNodeStateInfoChanged[] changedNodes)
            {
                if (addedNodes == null)
                    throw new ArgumentNullException("addedNodes");
                if (deletedNodes == null)
                    throw new ArgumentNullException("deletedNodes");
                if (changedNodes == null)
                    throw new ArgumentNullException("changedNodes");

                _addedNodes = addedNodes;
                _deletedNodes = deletedNodes;
                _changedNodes = changedNodes;
            }

            public NodeState[] AddedNodes
            {
                get { return _addedNodes; }
            }

            public NodeState[] DeletedNodes
            {
                get { return _deletedNodes; }
            }

            public LocalNodeStateInfoChanged[] ChangedNodes
            {
                get { return _changedNodes; }
            }
        }

        #endregion Nested class NodeStateChangesEventArgs

        #region Nested class LocalNodeStateInfoEqualityByIdComparer

        private class LocalNodeStateInfoEqualityByIdComparer : IEqualityComparer<NodeState>
        {
            public bool Equals(NodeState x, NodeState y)
            {
                if (x == y)
                    return true;

                if (x == null || y == null)
                    return false;

                if (x.Node == y.Node)
                    return true;

                if (x.Node == null || y.Node == null)
                    return false;

                return Equals(NodeScheduleManager.NodeInfoIdSelector(x.Node), NodeScheduleManager.NodeInfoIdSelector(y.Node));
            }

            public int GetHashCode(NodeState obj)
            {
                if (obj == null || obj.Node == null)
                    return 0;

                object id = NodeScheduleManager.NodeInfoIdSelector(obj.Node);
                if (id == null)
                    return -1;

                return id.GetHashCode();
            }
        }

        #endregion Nested class LocalNodeStateInfoEqualityByIdComparer

        #region Nested class LocalNodeStateInfoEqualityComparer

        private class LocalNodeStateInfoEqualityComparer : IEqualityComparer<NodeState>
        {
            private readonly LocalNodeStateInfoEqualityByIdComparer _nodeIdComparer = new LocalNodeStateInfoEqualityByIdComparer();
            private readonly ServiceStack.Text.JsonSerializer<NodeState> _serializer = new ServiceStack.Text.JsonSerializer<NodeState>();

            public bool Equals(NodeState x, NodeState y)
            {
                if (!_nodeIdComparer.Equals(x, y))
                    return false;

                string xstr = _serializer.SerializeToString(x);
                string ystr = _serializer.SerializeToString(y);
                return xstr == ystr;
            }

            public int GetHashCode(NodeState obj)
            {
                if (obj == null || obj.Node == null)
                    return 0;

                object id = NodeScheduleManager.NodeInfoIdSelector(obj.Node);
                if (id == null)
                    return -1;

                return id.GetHashCode();
            }
        }

        #endregion Nested class LocalNodeStateInfoEqualityComparer
    }
}