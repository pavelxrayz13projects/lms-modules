﻿using Quartz;
using Quartz.Impl;
using System;

namespace Win32Service.Schedule
{
    internal class SchedulerManager
    {
        private static IScheduler _scheduler;

        public static IScheduler Scheduler
        {
            get
            {
                if (_scheduler == null)
                    throw new InvalidOperationException("Remote scheduler is not initialized. Use Init() method.");
                return _scheduler;
            }
        }

        internal static void Init()
        {
            ISchedulerFactory sf = new StdSchedulerFactory();
            // Initialization goes here using App.config
            _scheduler = sf.GetScheduler();
        }

        internal static void Start()
        {
            Scheduler.Start();
        }

        internal static void Stop()
        {
            Scheduler.Shutdown();
        }
    }
}