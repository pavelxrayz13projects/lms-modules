﻿using log4net;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Win32Service
{
    public class ErrorTolerantBehavior : IEndpointBehavior
    {
        private ILog _log;

        public ErrorTolerantBehavior(ILog log)
        {
            _log = log;
        }

        #region IEndpointBehavior Members

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        { }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        { }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            endpointDispatcher.ChannelDispatcher.ErrorHandlers.Add(new LogErrorHandler(_log));
        }

        public void Validate(ServiceEndpoint endpoint)
        { }

        #endregion IEndpointBehavior Members
    }
}