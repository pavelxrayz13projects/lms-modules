using log4net;
using Olimp.Core;
using PAPI.Core;
using PAPI.Core.Logging;
using PAPI.Core.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace Win32Service
{
    using Funq;
    using Olimp.Core.Activation.HardwareBinding;
    using Olimp.Core.Backup;
    using Olimp.Core.Security;
    using Olimp.Core.Watchdog;
    using Olimp.Domain.Catalogue.OrmLite;
    using Olimp.Domain.Catalogue.OrmLite.Repositories;
    using Olimp.Domain.Common.Nodes;
    using Olimp.Domain.Common.Properties;
    using Olimp.LocalNodes.Core.Activation;
    using Olimp.LocalNodes.Core.Cluster;
    using Olimp.LocalNodes.Core.Runtime;
    using Olimp.LocalNodes.Core.Services.Implementations;
    using PAPI.Core.Data;
    using PAPI.Core.Security;
    using Quartz;
    using ServiceStack.OrmLite;
    using System.Configuration;
    using System.Web;
    using System.Web.Configuration;
    using Win32Service.Properties;
    using Win32Service.Schedule;
    using Win32Service.Schedule.Jobs;
    using A = Olimp.Core.Activation;
    using M = System.ServiceModel;
    using MD = System.ServiceModel.Description;
    using S = Olimp.LocalNodes.Contracts.LocalNodeController;

    public class Runner : ServiceModuleRunner
    {
        public const int WatchdogThreadStartDelay = 10000;
        public const int WatchdogThreadTickDelay = 3000;
        public const int Port = 9089;

        public static ILog ServiceLog { get; private set; }

        private static Funq.Container _container;

        public static Funq.Container Container { get { return _container; } }

        static Runner()
        {
            ServiceLog = Log.CreateLogger("service");
            _container = new Funq.Container();
        }

        private AutoResetEvent _watchdogEvent;
        private AutoResetEvent _watchdogDelayEvent;
        private bool _watchdogTerminating;

        private Thread _serviceThread;
        private ManualResetEvent _serviceEvent;

        private Thread _watchdogThread;

        private DefaultLocalNodeController —ontrollerService
        {
            get { return Container.Resolve<DefaultLocalNodeController>(); }
        }

        protected override void ServiceStopped(IModule module, IDictionary<string, string> moduleParams)
        {
            ServiceLog.Info("Stopping service");

            _watchdogTerminating = true;
            _watchdogDelayEvent.Set();
            _watchdogEvent.WaitOne();

            —ontrollerService.StopAllNodes();
            _serviceEvent.Set();
            Schedule.SchedulerManager.Stop();

            Thread.Sleep(100);

            _watchdogThread.Interrupt();
            _serviceThread.Interrupt();
        }

        private void InteropThread(object port)
        {
            var baseAddress = new Uri(OlimpPaths.OlimpServiceUri);
            var serviceHost = new M.ServiceHost(Container.Resolve<FacadeService>(), baseAddress);

            try
            {
                serviceHost.AddServiceEndpoint(typeof(S.ILocalNodeControllerService), new M.BasicHttpBinding(), OlimpPaths.OlimpClusterServiceName)
                    .Behaviors.Add(new ErrorTolerantBehavior(ServiceLog));

                serviceHost.AddServiceEndpoint(typeof(A.IActivationService), new M.BasicHttpBinding(), OlimpPaths.OlimpActivationServiceName)
                    .Behaviors.Add(new ErrorTolerantBehavior(ServiceLog));

                serviceHost.AddServiceEndpoint(typeof(IWatchdogServiceContract), new M.BasicHttpBinding(), OlimpPaths.OlimpWatchdogServiceName)
                    .Behaviors.Add(new ErrorTolerantBehavior(ServiceLog));

                serviceHost.Description.Behaviors.Add(
                    new MD.ServiceMetadataBehavior { HttpGetEnabled = true });

                ServiceLog.Info("Starting service on endpoint " + OlimpPaths.OlimpServiceUri);
                serviceHost.Open();

                _serviceEvent.WaitOne();

                //gently disposing host's channel dispatchers ignoring all errors to avoid unhandled exception in threads caused by serviceHost.Abort();
                try
                {
                    var dispatchers = serviceHost.ChannelDispatchers.ToList();
                    foreach (var d in dispatchers)
                    {
                        try
                        {
                            d.Abort();
                        }
                        catch { }
                    }
                }
                catch { }

                ServiceLog.Info("Stopped");
            }
            catch (Exception ee)
            {
                ServiceLog.Error(ee);
            }
            finally
            {
                try
                {
                    serviceHost.Abort();
                }
                catch { }
            }
        }

        private void WatchdogThread(object parameter)
        {
            var wd = (IWatchdog)parameter;

            Thread.Sleep(WatchdogThreadStartDelay);

            while (!_watchdogTerminating)
            {
                _watchdogDelayEvent.WaitOne(WatchdogThreadTickDelay);
                if (_watchdogTerminating)
                    break;

                wd.Watch();
            }

            _watchdogEvent.Set();
        }

        /*private void StartWebService()
        {
            AppServiceHost serviceHost = new AppServiceHost("SyncService", "services", typeof(SyncService).Assembly);
            serviceHost.RegisterRoutes(
              new Dictionary<Type, string> { { typeof(SyncFinishedAttempts), "/sync/attempts" } });
            ServiceStack.Logging.LogManager.LogFactory = new SingletonLogFactory(ServiceLog);
            try
            {
                serviceHost.Init();
                serviceHost.Start(String.Format("http://{0}:{1}/", Settings.Default.WebServiceHostAddress, Settings.Default.WebServiceHostPort));
                _serviceEvent.WaitOne();
                ServiceLog.Info("Stopped");
            }
            catch
            {
                serviceHost.Stop();
                throw;
            }
            finally
            {
                serviceHost.Dispose();
            }
        }*/

        protected override void ServiceStarted(IModule module, IDictionary<string, string> moduleParams)
        {
            _watchdogThread = new Thread(this.WatchdogThread) { IsBackground = true };
            _watchdogThread.Start(Container.Resolve<IWatchdog>());

            —ontrollerService.StartAllNodes();

            _serviceThread = new Thread(this.InteropThread) { IsBackground = false };
            _serviceThread.Start(Port);

            //StartWebService();

            Schedule.SchedulerManager.Start();

            if (Settings.Default.SynchronizationServiceOn)
            {
                ServiceLog.Info("Initializing synchronization service");
                Thread t = new Thread(delegate()
                  {
                      Thread.Sleep(Settings.Default.RunnerDelayNodeJobsToStartMilliseconds);
                      //StartNodePingJobs();
                      StartDefaultNodePortChangedHandlerJob();
                      StartAttemptsSyncJobs();
                  }) { IsBackground = true };
                t.Start();
            }
        }

        protected virtual ILocalNodeAvailabilityChecker CreateAvailabilityChecker()
        {
            var webConfigDir = Path.Combine(PlatformPaths.Default.Modules, "Olimp");
            var fileMap = new ExeConfigurationFileMap
            {
                ExeConfigFilename = Directory.GetFiles(webConfigDir, "?eb.?onfig", SearchOption.TopDirectoryOnly).FirstOrDefault()
            };

            var webConfig = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);

            var webConfigHandlers = webConfig.GetSection("system.web/httpHandlers") as HttpHandlersSection;
            if (webConfigHandlers != null)
            {
                foreach (HttpHandlerAction h in webConfigHandlers.Handlers)
                {
                    var type = Type.GetType(h.Type);
                    if (Type.Equals(type, typeof(WatchdogHttpHandler)))
                    {
                        var method = h.Verb == "*" ? "POST" : h.Verb;

                        return new WatchdogLocalNodeAvailabilityChecker(h.Path, method);
                    }
                }
            }

            return new DefaultLocalNodeAvailabilityChecker();
        }

        protected override void ServiceInit(IModule module, IDictionary<string, string> moduleParams)
        {
            _serviceEvent = new ManualResetEvent(false);
            _watchdogEvent = new AutoResetEvent(false);
            _watchdogDelayEvent = new AutoResetEvent(false);

            Schedule.SchedulerManager.Init();

            RegisterInstances();
        }

        private void RegisterInstances()
        {
            var dbConnectionFactoryFactory = new SqliteConnectionFactoryFactory();
            Container.Register<IDbConnectionFactory>(dbConnectionFactoryFactory.Create());

            Container.Register<ILocalNodeAvailabilityChecker>(CreateAvailabilityChecker());
            Container.Register<IConnectionStringProviderFactory>(new SqliteConnectionStringProviderFactory("Olimp"));
            Container.Register<IConnectionStringProvider>(container =>
                container.Resolve<IConnectionStringProviderFactory>().CreateProvider(NodesSettings.Default.DefaultNodeName));

            //var registryPath = Path.Combine(PlatformPaths.Default.Modules, "Olimp", "startup.xml");
            //Container.Register<INodesRepository>(new XmlNodeRepository(registryPath, false));
            Container.RegisterAs<OrmLiteNodeRepository, INodesRepository>();

            Container.RegisterAs<FileSystemDistributionInfoProviderFactory, IDistributionInfoProviderFactory>();
            Container.RegisterAs<FileSystemExclusiveActivationCheckerFactory, IExclusiveActivationCheckerFactory>();

            Container.Register<IExclusiveActivationChecker>(container => container.Resolve<IExclusiveActivationCheckerFactory>().Create("Olimp"));
            Container.Register<IDistributionInfoProvider>(container => container.Resolve<IDistributionInfoProviderFactory>().Create("Olimp"));

            Container.RegisterAs<RoughLocalNodeProcessManager, ILocalNodeProcessManager>();
            Container.RegisterAs<OlimpHardwareIdProvider, IHardwareIdProvider>();

            Container.RegisterAs<LocalNodeRuntimeManager, ILocalNodeRuntimeManager>();
            Container.RegisterAs<A.StartupRegistryActivationKeyProvider, A.IActivationKeyProvider>();
            Container.RegisterAs<A.OlimpActivationKeyInfoProviderFactory, A.IActivationKeyInfoProviderFactory>();

            Container.Register<A.IActivationKeyInfoProvider>(
              container => container.Resolve<A.IActivationKeyInfoProviderFactory>().Create(Platform.DefaultNode.Name));

            Container.RegisterAs<DefaultLocalNodeQuotaDistributor, ILocalNodeQuotaDistributor>();

            Container.RegisterAs<LocalNodeInitializationValidator, INodeActivationValidator>();
            Container.Register<IUsersQuotaProvider>(container =>
                new DefaultUsersQuotaProvider(
                    Platform.DefaultNode.Name,
                    container.Resolve<A.IActivationKeyProvider>(),
                    container.Resolve<A.IActivationKeyInfoProviderFactory>()));

            Container.RegisterAs<DefaultLocalNodeActivator, ILocalNodeActivator>();

            Container.RegisterAs<DefaultBackupManager, IBackupManager>();

            Container.RegisterAutoWired<DefaultLocalNodeController>().ReusedWithin(Funq.ReuseScope.Container);
            Container.Register<ILocalNodeController>(container => container.Resolve<DefaultLocalNodeController>());
            Container.RegisterAs<DefaultLocalNodeControllerService, S.ILocalNodeControllerService>();

            Container.RegisterAs<A.OlimpActivator, A.IActivator>();
            Container.RegisterAs<A.InternetActivationConfigurationUpdater, A.IInternetActivationConfigurationUpdater>();
            Container.RegisterAs<A.SqliteActivationAttemptsCounter, A.IActivationAttemptsCounter>();
            Container.RegisterAs<A.StartupRegistryActivationService, A.IActivationService>()
                .ReusedWithin(ReuseScope.Container);
            Container.RegisterAutoWired<FacadeService>();
            Container.Register<IScheduler>(Schedule.SchedulerManager.Scheduler);
            Container.RegisterAutoWired<NodeScheduleManager>();

            Container.RegisterAs<WatchdogServiceInstance, IWatchdogServiceContract>();
            Container.Register<IWatchdog>(container => new DefaultWatchdog(
                container.Resolve<ILocalNodeController>(), container.Resolve<ILocalNodeRuntimeManager>(), container.Resolve<INodesRepository>(), ServiceLog, "/!/watchdog"));
        }

        private string BuildCallbackServicesUrl(Node targetNode, bool useTargetNodeId, string service)
        {
            var identifier = useTargetNodeId
                ? targetNode.Id.ToString()
                : targetNode.Name;

            Node defaultNode = —ontrollerService.GetAllNodes()
                .First(n => Node.IsDefaultNode(n.Node)).Node;

            var uriBuilder = new UriBuilder(defaultNode.GetUrl(true));
            uriBuilder.Path = String.Join("/", uriBuilder.Path.TrimEnd('/'), service, identifier);
            uriBuilder.Query = "url=" + HttpUtility.UrlEncode(targetNode.GetUrl(true));

            return uriBuilder.ToString();
        }

        private void StartDefaultNodePortChangedHandlerJob()
        {
            var nodeScheduleManager = Container.Resolve<NodeScheduleManager>();
            var group = NodeScheduleManager.CreateNodeJobGroup<EmptyJob>(
              "DefaultNodePortChangedHandler",
              "0 * * * * ?",
              nodeState => new JobDataMap(),
              nodeState => Node.IsDefaultNode(nodeState.Node),
              nodeStateChanged =>
              {
                  if (nodeStateChanged.IsPortChanged)
                  {
                      var scheduleManager = Container.Resolve<NodeScheduleManager>();
                      var syncAttemptsGroup = scheduleManager.GetNodeSchedulingGroup("syncattempts");
                      scheduleManager.UnscheduleNodeJobs(syncAttemptsGroup);
                      scheduleManager.ScheduleNodeJobs(syncAttemptsGroup);
                  }
                  return false;
              }
              );
            nodeScheduleManager.ScheduleNodeJobs(group);
        }

        private void StartNodePingJobs()
        {
            var nodeScheduleManager = Container.Resolve<NodeScheduleManager>();
            var group = NodeScheduleManager.CreateNodeJobGroup<NodeWebRequestJob>(
              "ping",
              "0 * * * * ?",
              nodeState => new NodeWebRequestJobParams(this.BuildCallbackServicesUrl(nodeState, false, "Services/Ping"), nodeState.Name, "Ping")
                  .ToJobDataMap(),
              nodeState => nodeState.IsActive,
              nodeStateChanged => nodeStateChanged.IsActiveChanged || nodeStateChanged.IsPortChanged
              );
            nodeScheduleManager.ScheduleNodeJobs(group);
        }

        private void StartAttemptsSyncJobs()
        {
            var nodeScheduleManager = Container.Resolve<NodeScheduleManager>();
            var group = NodeScheduleManager.CreateNodeJobGroup<NodeWebRequestJob>(
              "syncattempts",
              "0 */1 * * * ?",
              nodeState => new NodeWebRequestJobParams(this.BuildCallbackServicesUrl(nodeState, true, "Services/Sync/Attempts"), nodeState.Name, "SyncAttempts")
                  .ToJobDataMap(),
              nodeState => nodeState.IsActive && nodeState.Node.Id != Platform.DefaultNode.Id,
              nodeStateChanged => nodeStateChanged.IsActiveChanged || nodeStateChanged.IsPortChanged
              );
            nodeScheduleManager.ScheduleNodeJobs(group);
        }
    }
}