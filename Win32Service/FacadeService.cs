﻿using Olimp.Core.Activation;
using Olimp.Core.Watchdog;
using Olimp.LocalNodes.Contracts.LocalNodeController;
using Olimp.LocalNodes.Contracts.LocalNodeController.DataContracts;
using Olimp.LocalNodes.Contracts.LocalNodeController.DataContracts.Nodes;
using Olimp.LocalNodes.Contracts.LocalNodeController.DataContracts.Results;
using System;
using System.ServiceModel;

namespace Win32Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class FacadeService : ILocalNodeControllerService, IActivationService, IWatchdogServiceContract
    {
        private ILocalNodeControllerService _controller;
        private IActivationService _activation;
        private IWatchdogServiceContract _wdServiceContract;

        public FacadeService(ILocalNodeControllerService controller, IActivationService activation, IWatchdogServiceContract wdServiceContract)
        {
            if (controller == null)
                throw new ArgumentNullException("controller");

            if (activation == null)
                throw new ArgumentNullException("activation");

            if (wdServiceContract == null)
                throw new ArgumentNullException("wdServiceContract");

            _controller = controller;
            _activation = activation;
            _wdServiceContract = wdServiceContract;
        }

        #region ILocalNodeControllerService Members

        LocalNodeResult ILocalNodeControllerService.Activate(string node)
        {
            return _controller.Activate(node);
        }

        LocalNodeResult ILocalNodeControllerService.Deactivate(string node)
        {
            return _controller.Deactivate(node);
        }

        NodeStateContract[] ILocalNodeControllerService.GetAllNodes()
        {
            return _controller.GetAllNodes();
        }

        LocalNodeResult ILocalNodeControllerService.RestoreBackup(string nodeName, bool forAllNodes, string backupFilePath)
        {
            return _controller.RestoreBackup(nodeName, forAllNodes, backupFilePath);
        }

        LocalNodeResult ILocalNodeControllerService.CreateBackup(string nodeName, bool forAllNodes)
        {
            return _controller.CreateBackup(nodeName, forAllNodes);
        }

        LocalNodeResult ILocalNodeControllerService.UpdateNodeInfo(NodeInfoContract node)
        {
            return _controller.UpdateNodeInfo(node);
        }

        #endregion ILocalNodeControllerService Members

        #region IActivationService Members

        bool IActivationService.Activate(string activationKey)
        {
            return _activation.Activate(activationKey);
        }

        string IActivationService.GetActivationKey()
        {
            return _activation.GetActivationKey();
        }

        #endregion IActivationService Members

        #region IWatchdogServiceContract Members

        void IWatchdogServiceContract.Register(string name, bool isPaused)
        {
            _wdServiceContract.Register(name, isPaused);
        }

        void IWatchdogServiceContract.Unregister(string name)
        {
            _wdServiceContract.Unregister(name);
        }

        void IWatchdogServiceContract.Pause(string name, bool isPaused)
        {
            _wdServiceContract.Pause(name, isPaused);
        }

        #endregion IWatchdogServiceContract Members
    }
}