﻿using log4net;
using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace Win32Service
{
    public class LogErrorHandler : IErrorHandler
    {
        private ILog _log;

        public LogErrorHandler(ILog log)
        {
            _log = log;
        }

        #region IErrorHandler Members

        public bool HandleError(Exception error)
        {
            try
            {
                _log.Error(error);
            }
            catch { }

            return false;
        }

        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        { }

        #endregion IErrorHandler Members
    }
}