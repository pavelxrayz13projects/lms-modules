﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage"  MasterPageFile="~/Views/Shared/MaterialsManagement.master" %>

<asp:Content ContentPlaceHolderID="Olimp_MaterialsManagement_Content" runat="server">
	
    <% Html.RenderPartial("MaterialsManagementPage", new MaterialsPageViewModel("Courses", "Установленные курсы")); %>

</asp:Content>