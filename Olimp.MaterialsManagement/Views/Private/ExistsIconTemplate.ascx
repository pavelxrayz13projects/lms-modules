﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/html" id="exists-icon">
	<div style="margin-left: 50%;">
		{{ var cls = ko.utils.unwrapObservable(it.$data.Exists) ? "icon icon-success" : "icon icon-fail"; }}
		{{ var mrgnTop = ko.utils.unwrapObservable(it.$data.Exists) ? 8 : 5; }}
		<div style="margin-left: -8px; margin-top: -{{= mrgnTop}}px;" class="{{= cls }}"></div>
	</div>
</script>