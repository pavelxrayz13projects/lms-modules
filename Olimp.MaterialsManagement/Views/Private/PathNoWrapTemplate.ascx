﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/html" id="path-no-wrap">
	<span style="white-space: nowrap; color:#585A5E;">{{= ko.utils.unwrapObservable(it.$data.Path) }}</span>
</script>
