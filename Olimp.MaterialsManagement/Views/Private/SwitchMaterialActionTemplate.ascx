﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<script type="text/javascript">
	function switchOn() {
		$.post('<%= Url.Action("SwitchOn", ViewData["controller"]) %>', { id: this.Id() }, function(json) {
			json.result && $('#materials-table').data('viewModel').reload()
		}, 'json')
		
		return false;
	}
	function switchOff(o) {
		var self = this;
		$.post('<%= Url.Action("SwitchOff", ViewData["controller"]) %>', { id: this.Id() }, function(json) {
			json.result && self.IsActive(false);
		}, 'json')
		
		return false;
	}
</script>

<script type="text/html" id="switch-material-action">
	{{? ko.utils.unwrapObservable(it.$data.Exists) && !ko.utils.unwrapObservable(it.$data.IsActive) }}	
		<a class="icon icon-turnon -has-dot-binding" title="Включить" href="#"
			data-dot-bind="click: switchOn"
			data-dot-context="{{= it.$$contextId }}">
		</a>
	{{?? ko.utils.unwrapObservable(it.$data.IsActive) }}
		<a class="icon icon-turnoff -has-dot-binding" title="Отключить" href="#"
			data-dot-bind="click: switchOff"
			data-dot-context="{{= it.$$contextId }}">
		</a>
	{{?}}
</script>		
