﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MaterialsPageViewModel>" %>

<% Html.RenderPartial("~/Olimp.MaterialsManagement/Views/Private/PathNoWrapTemplate.ascx"); %>
<% Html.RenderPartial("~/Olimp.MaterialsManagement/Views/Private/UidNoWrapTemplate.ascx"); %>
<% Html.RenderPartial("~/Olimp.MaterialsManagement/Views/Private/ActiveIconTemplate.ascx"); %>
<% Html.RenderPartial("~/Olimp.MaterialsManagement/Views/Private/ExistsIconTemplate.ascx"); %>
<% Html.RenderPartial("~/Olimp.MaterialsManagement/Views/Private/SwitchMaterialActionTemplate.ascx", 
       new ViewDataDictionary { { "controller", Model.Controller } }); %>

<h1><%= Model.Header %></h1>
<div class="block" >
    <% Html.Table(
	    new TableOptions {
		    SelectUrl = Url.Action("GetMaterials"),	
			BodyTemplateEngine = TemplateEngines.DoT,			
			DefaultSortColumn = "Name",				
			Paging = false,
			Columns = new[] { 
			    new ColumnOptions("IsActive", "Активность") { TemplateId = "active-icon" },					
				new ColumnOptions("Exists", "Наличие") { TemplateId = "exists-icon" },
				new ColumnOptions("Name", "Наименование"),
				new ColumnOptions("Timestamp", "Дата"),
				new ColumnOptions("Path", "Путь") { TemplateId = "path-no-wrap" },										
				new ColumnOptions("UniqueId", "Идентификатор") { TemplateId = "uid-no-wrap" } },
            Actions = new RowAction[] { new TemplateAction("switch-material-action") } 
        }, new { id = "materials-table" }); %>
</div>