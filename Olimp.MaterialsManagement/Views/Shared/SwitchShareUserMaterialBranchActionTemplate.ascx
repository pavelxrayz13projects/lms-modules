﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ShareUserBranchViewModel>" %>


<script type="text/javascript">
    function toggleShare() {
        var self = this, oldState = self.IsShared();
        
        if (typeof self.InProgress === 'undefined')
            self.InProgress = false;

        if (self.InProgress === true)
            return false;
        
        self.InProgress = true;
        $.post('<%= Url.Action("Share", Model.Controller) %>', { branchId: self.Id(), state: !self.IsShared() }, function(newState) {
            self.IsShared(newState === true);           
            self.InProgress = false;
            if (oldState != self.IsShared())
                $.Olimp.showSuccess('Общий доступ к учебному материалу ' + (self.IsShared() ? 'включен' : 'отключен'));
        }, 'json').fail(function () {
            self.InProgress = false;
        });

        return false;
    }	
</script>
    
<script type="text/html" id="share-action">
{{? ko.utils.unwrapObservable(it.$data.IsShared) }}	
	<a class="icon icon-globe-active action -has-dot-binding" title="Общий доступ включен. Нажмите, чтобы отключить." href="#"
		data-dot-bind="click: toggleShare"
		data-dot-context="{{= it.$$contextId }}">
	</a>
{{??}}
	<a class="icon icon-globe-inactive action -has-dot-binding" title="Общий доступ отключен. Нажмите, чтобы включить." href="#"
		data-dot-bind="click: toggleShare"
		data-dot-context="{{= it.$$contextId }}">
	</a>
{{?}}
</script>
