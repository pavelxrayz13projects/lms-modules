using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using PAPI.Core;
using PAPI.Core.Web.Mvc;
using PAPI.Core.Data;
using Olimp.UI.ViewModels;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Core.Security;
using Olimp.Core;
using Olimp.Domain.LearningCenter.Auth;
using Olimp.Domain.LearningCenter.Caching;
using Olimp.Core.Mvc.Security;

namespace Olimp.MaterialsManagement.Controllers
{
	[HandleError]
	[ErrorOnUnauthorizedAjaxRequest(Order=1)]
    [AuthorizeForAdmin(Roles = "MaterialsAdmin", Order = 2)]
	public class NormativesController : MaterialsController<Normative>
	{
		protected override void CleanCache ()
		{
			NormativeCache.BlameTree();
		}
		
		[NonAction]
		protected override bool MaterialExists(string path)
		{			
			return System.IO.File.Exists(Path.Combine(OlimpPaths.Default.Normatives, path));
		}
	}
}

