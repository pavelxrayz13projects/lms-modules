using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Entities;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Web.Controllers.Materials;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.MaterialsManagement.Controllers
{
    [HandleError]
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Admin.Materials.Switch, Order = 2)]
    public class CoursesController : MaterialsController<Course>
    {
        public CoursesController(ICourseRepository courseRepository)
            : base(courseRepository)
        { }

        protected override SwitchMaterialResult Switch(int id, bool state)
        {
            var res = base.Switch(id, state);

            using (var uow = UnitOfWork.CreateWrite())
            {
                var profiles = Repository.Of<ProfileCourse>()
                    .Where(c => c.MaterialId == res.MaterialUniqueId)
                    .Select(c => c.Profile)
                    .Distinct();

                var now = DateTime.Now;
                foreach (var p in profiles)
                    p.CourseLastReswitched = now;

                uow.Commit();
            }

            return res;
        }

        /*protected override void CleanCache ()
        {
            CourseTreeCache.BlameAll();
        }
        */
    }
}