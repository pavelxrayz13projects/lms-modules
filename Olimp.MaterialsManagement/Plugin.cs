using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.MaterialsManagement.Controllers;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.MaterialsManagement.Plugin))]

namespace Olimp.MaterialsManagement
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        { }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(CoursesController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (this.Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var materialsModule = PContext.ExecutingModule as OlimpMaterialsModule;
            if (materialsModule == null)
                throw new InvalidOperationException("Executing module is not OlimpMaterialsModule");

            var connFactoryFactory = new SqliteConnectionFactoryFactory();
            var connFactory = connFactoryFactory.Create();

            var nodeRepository = new OrmLiteNodeRepository(connFactory);
            var courseRepository = new OrmLiteCourseRepository(this.Context.NodeData, connFactory, materialsModule.Materials.CourseRepository, nodeRepository);

            binder.Controller<CoursesController>(b => b
                .Bind<ICourseRepository>(() => courseRepository));
        }
    }
}