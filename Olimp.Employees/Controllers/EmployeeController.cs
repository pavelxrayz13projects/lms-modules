using Olimp.Core;
using Olimp.Core.Mvc.Security;
using Olimp.Core.Security;
using Olimp.Domain.Catalogue.Ldap;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter.DataServices;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Domain.LearningCenter.Services;
using Olimp.Employees.ViewModels;
using Olimp.Ldap;
using Olimp.UI.LearningCenter;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using PAPI.Core.Data.Entities;
using PAPI.Core.Data.Specifications;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Employees.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    public class EmployeeController : PController
    {
        private readonly IAppointmentService _appointmentService;
        private readonly ICompanyService _companyService;
        private readonly IGroupService _groupService;
        private readonly IPasswordGenerator _passwordGenerator;
        private readonly IEmployeeBulkLoader _bulkLoader;

        private readonly ILdapAuthSchemeRepository _authSchemesRepository;
        private readonly LdapEmployeeBulkLoader _ldapBulkLoader;

        public EmployeeController(
            IAppointmentService appointmentService,
            ICompanyService companyService,
            IGroupService groupService,
            IPasswordGenerator passwordGenerator,
            IEmployeeBulkLoader bulkLoader,
            ILdapAuthSchemeRepository authSchemesRepository,
            LdapEmployeeBulkLoader ldapBulkLoader)
        {
            if (authSchemesRepository == null)
                throw new ArgumentNullException("authSchemesRepository");

            if (ldapBulkLoader == null)
                throw new ArgumentNullException("ldapBulkLoader");

            _appointmentService = appointmentService;
            _companyService = companyService;
            _groupService = groupService;
            _passwordGenerator = passwordGenerator;
            _bulkLoader = bulkLoader;

            _authSchemesRepository = authSchemesRepository;
            _ldapBulkLoader = ldapBulkLoader;
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.Registration.Employees, Order = 2)]
        public ActionResult LookupAppointment(string filter, string selectedItems)
        {
            using (UnitOfWork.CreateRead())
            {
                var appointmentArray = selectedItems.Split('#');
                var appointments = _appointmentService.GetActiveAppointments();
                appointments = appointments.Where(a => a.Name.Contains(filter) && !appointmentArray.Contains(a.Name))
                        .OrderBy(a => a.Name);

                return Json(appointments.Select(a => new { label = a.Name, optionValue = a.Name }));
            }
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.Registration.Employees + "," + Permissions.Reports.Generate, Order = 2)]
        public ActionResult LookupCompany(string filter)
        {
            using (UnitOfWork.CreateRead())
            {
                var companies = _companyService.GetActiveCompanies()
                                               .Where(c => c.Name.Contains(filter))
                                               .OrderBy(c => c.Name)
                                               .Select(c => c.Name);

                return Json(companies);
            }
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.Registration.Employees, Order = 2)]
        public ActionResult LookupProfile(string filter, string selectedItems)
        {
            using (UnitOfWork.CreateRead())
            {
                var proilesArray = selectedItems.Split(',');
                var profiles = Repository.Of<Profile>()
                                         .Where(p => p.Name.Contains(filter) && !proilesArray.Contains(p.Name))
                                         .OrderBy(p => p.Name).AsEnumerable();

                return Json(profiles.Select(p => new { label = p.Name, optionValue = p.Name }));
            }
        }

        [HttpGet]
        [AuthorizeForAdmin(Roles = Permissions.Registration.Employees, Order = 2)]
        public ActionResult Index(EmployeeIndexViewModel model)
        {
            using (UnitOfWork.CreateRead())
            {
                var schemes = _authSchemesRepository.GetAll()
                    .Where(s => s.IsEnabled && s.IsPublic)
                    .OrderBy(s => s.Name)
                    .Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() })
                    .ToList();

                return View(new EmployeeIndexViewModel
                {
                    AuthSchemeId = model.AuthSchemeId,
                    GroupId = model.GroupId,
                    Groups = _groupService.GetActiveGroupsList(),
                    SynchronizeSuccess = model.SynchronizeSuccess,
                    SynchronizeMessage = model.SynchronizeMessage,
                    LdapSchemes = schemes,
                    AuthSchemes = Enumerable.Repeat(new SelectListItem { Text = I18N.Employee.DefaultAuthScheme, Value = "-1" }, 1)
                        .Concat(schemes)
                        .ToList(),
                    UILocale = new SelectList(
                        new[] { CultureInfo.GetCultureInfo("ru-RU"), CultureInfo.GetCultureInfo("en-US") },
                        "Name",
                        "NativeName")
                });
            }
        }

        [HttpGet]
        [AuthorizeForAdmin(Roles = Permissions.Registration.Employees, Order = 2)]
        public ActionResult Print(EmployeeIndexViewModel model)
        {
            var viewModel = new EmployeePrintViewModel
            {
                GroupName = I18N.Employee.AllGroups
            };

            Specification<Employee> condition = new EntityIsActive<Employee>();

            using (UnitOfWork.CreateRead())
            {
                var groupId = model.GroupId.GetValueOrDefault();
                if (model.GroupId.GetValueOrDefault() > 0)
                {
                    viewModel.GroupName = Repository.Of<Group>().LoadById(model.GroupId).Name;
                    condition &= new ProxySpecification<Employee>(e => e.Group.Id == groupId);
                }

                viewModel.Employees = Repository.Of<Employee, Guid>()
                                                .Where(condition)
                                                .Where(e => !e.IsAnonymous)
                                                .OrderBy(e => e.Surname)
                                                .ThenBy(e => e.Name)
                                                .ThenBy(e => e.GivenName);

                return View(viewModel);
            }
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.Registration.Employees, Order = 2)]
        public ActionResult GetEmployees(EmployeeTableViewModel model)
        {
            model.SortingHandlers.Add("FullName", new SortEmployeesByFullNameBuilder());
            model.SortingHandlers.Add("GroupName", new SortEmployeesByGroupBuilder());
            model.SortingHandlers.Add("ProfileName", new SortEmployeesByProfileBuilder());

            Specification<Employee> condition =
                new EntityIsActive<Employee>() &
                new ProxySpecification<Employee>(e => !e.IsAnonymous);

            var groupId = model.GroupId.GetValueOrDefault();
            if (groupId > 0)
                condition &= new ProxySpecification<Employee>(e => e.Group.Id == groupId);

            var authSchemeId = model.AuthSchemeId.GetValueOrDefault();
            if (authSchemeId == -1)
                condition &= new ProxySpecification<Employee>(e => e.LdapSchemeId == null);
            else if (authSchemeId > 0)
                condition &= new ProxySpecification<Employee>(e => e.LdapSchemeId == authSchemeId);

            if (!String.IsNullOrEmpty(model.Filter))
                condition &= new ProxySpecification<Employee>(e =>
                    (e.Surname + " " + e.Name + (e.GivenName != null ? " " + e.GivenName : "")).Contains(model.Filter) ||
                    e.Number.Contains(model.Filter));

            using (UnitOfWork.CreateRead())
            {
                return Json(
                    Repository.Of<Employee, Guid>()
                        .Where(condition)
                        .GetTableData(model, e => new
                        {
                            e.Id,
                            e.Login,
                            e.Password,
                            e.Surname,
                            e.Name,
                            e.Number,
                            e.Email,
                            e.UILocale,
                            e.DoNotInheritIndirectProfiles,
                            e.KnowledgeControlReason,
                            GivenName = e.GivenName ?? "",
                            CompanyName = Convert.ToString(e.Company),
                            ProfilesList = e.ProfilesToString(),
                            GroupId = e.Group != null ? e.Group.Id : 0,
                            GroupName = Convert.ToString(e.Group),
                            IsExternal = e.LdapSchemeId > 0,
                            IsReadOnly = e.LdapSchemeId > 0
                        }));
            }
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.Registration.Employees, Order = 2)]
        public ActionResult GetEmployee(Guid? id, int? groupId)
        {
            using (UnitOfWork.CreateRead())
            {
                var group = Repository.Of<Group>().GetById(groupId);
                var e = Repository.Of<Employee, Guid>().GetById(id) ?? new Employee { Group = group ?? new Group() };

                return Json(new
                {
                    e.Id,
                    Login = e.Login ?? string.Empty,
                    Password = e.Password ?? string.Empty,
                    Surname = e.Surname ?? string.Empty,
                    Name = e.Name ?? string.Empty,
                    GivenName = e.GivenName ?? string.Empty,
                    Number = e.Number ?? string.Empty,
                    Email = e.Email ?? string.Empty,
                    UILocale = e.UILocale ?? string.Empty,
                    e.DoNotInheritIndirectProfiles,
                    e.KnowledgeControlReason,
                    AppointmentNames = String.Join("#", e.Appointments.Where(a => a.IsActive).Select(a => a.Name)),
                    ProfilesList = e.ProfilesToString(),
                    CompanyName = Convert.ToString(e.Company) ?? string.Empty,
                    GroupId = e.Group.Id,
                    GroupName = e.Group.Name,
                    IsExternal = e.LdapSchemeId.GetValueOrDefault() > 0,
                    IsReadOnly = e.LdapSchemeId.GetValueOrDefault() > 0
                });
            }
        }

        [NonAction]
        [AuthorizeForAdmin(Roles = Permissions.Registration.Employees, Order = 2)]
        private static EmployeeValidationState ValidateEmployee(EmployeeEditViewModel viewModel)
        {
            var number = viewModel.Employee.Number;

            if (String.IsNullOrWhiteSpace(number))
                return EmployeeValidationState.Valid;
            
            var companyName = viewModel.CompanyName;
            var id = viewModel.Employee.Id;

            var employeeId = Repository.Of<Employee, Guid>()
                                       .Where(new EntityIsActive<Employee>())
                                       .Where(e => e.Company.Name == companyName && e.Number == number)
                                       .Select(e => e.Id)
                                       .FirstOrDefault();

            return employeeId == Guid.Empty || employeeId == id
                ? EmployeeValidationState.Valid
                : EmployeeValidationState.DuplicateNumberInCompany;
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.Registration.Employees, Order = 2)]
        public ActionResult Edit(EmployeeEditViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return null;

            var appointmentRepository = Repository.Of<Appointment>();
            var profileRepo = Repository.Of<Profile>();
            var employeeRepository = Repository.Of<Employee, Guid>();
            var employee = viewModel.Employee;

            var isNew = employee.Id == Guid.Empty;

            using (var uow = UnitOfWork.CreateWrite())
            {
                if(ValidateEmployee(viewModel) == EmployeeValidationState.DuplicateNumberInCompany)
                    return new StatusCodeResultWithText(500, I18N.Employee.DuplicateNumberInCompany);

                if (!isNew)
                {
                    viewModel.Employee = employeeRepository.LoadById(employee.Id);

                    UpdateModel(viewModel);

                    employee = viewModel.Employee;
                }
                else
                {
                    employee.Id = Guid.NewGuid();
                }

                if (!employee.ImportedFromLdap())
                {

                    if (!string.IsNullOrWhiteSpace(viewModel.CompanyName))
                    {
                        employee.Company = Repository.Of<Company>().FirstOrDefault(c => c.Name == viewModel.CompanyName)
                                           ?? new Company {Name = viewModel.CompanyName};
                    }
                    else
                    {
                        employee.Company = null;
                    }

                    if (String.IsNullOrEmpty(viewModel.Employee.Password))
                        employee.Password = _passwordGenerator.Generate(8);

                    employee.Appointments.Clear();

                    if (!String.IsNullOrWhiteSpace(viewModel.AppointmentNames))
                    {
                        var appointmentArray = viewModel.AppointmentNames.Split('#');
                        foreach (var appointmentName in appointmentArray.Where(e => !String.IsNullOrWhiteSpace(e)))
                        {
                            var appointment = appointmentRepository.FirstOrDefault(a => a.IsActive && a.Name == appointmentName)
                                ?? new Appointment { Name = appointmentName };
                            employee.AddAppointment(appointment);
                        }
                    }
                }

                if (isNew)
                    employee.SetSurnamePrefixLogin(employeeRepository);

                employee.SetGroup(Repository.Of<Group>().GetById(viewModel.GroupId));

                employee.Profiles.Clear();

                if (!String.IsNullOrWhiteSpace(viewModel.ProfilesList))
                {
                    var profileArray = viewModel.ProfilesList.Split(',');
                    foreach (var profileName in profileArray.Where(e => !String.IsNullOrWhiteSpace(e)))
                    {
                        var profile = profileRepo.FirstOrDefault(a => a.Name == profileName);
                        if (profile != null)
                            employee.AddProfile(profile);
                    }
                }

                employee.ChangeInProfileEntitiesDate = DateTime.Now;

                employeeRepository.Save(employee, true);

                uow.Commit();
            }

            //TODO: GetEmployee shouldn't be in new UOW
            return isNew
                ? null
                : GetEmployee(employee.Id, null);
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.Registration.Employees, Order = 2)]
        public ActionResult Delete(Guid id)
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                Repository.Of<Employee, Guid>()
                    .DeleteById(id);

                uow.Commit();
            }

            return null;
        }

        [HttpPost]
        public ActionResult MassDelete(IEnumerable<Guid> objects)
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                foreach (var e in Repository.Of<Employee, Guid>().Where(e => objects.Contains(e.Id)))
                    e.IsActive = false;
                uow.Commit();
            }
            return null;
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.Registration.Employees, Order = 2)]
        public ActionResult FromFile(EmployeeFromFileViewModel model)
        {
            if (!ModelState.IsValid || !model.GroupId.HasValue)
                return null;

            using (var uow = UnitOfWork.CreateWrite())
            {
                var group = Repository.Of<Group>().GetById(model.GroupId);

                using (var stream = model.CsvFile.InputStream)
                    _bulkLoader.Load(stream, group);

                uow.Commit();
            }

            return Json(new { status = "success" }, "text/plain");
        }

        [HttpPost]
        public ActionResult SyncFromLdap(EmployeeFromLdapViewModel model)
        {
            var scheme = _authSchemesRepository.GetById(model.AuthSchemeId.Value);

            using (var uow = UnitOfWork.CreateWrite())
            {
                var group = Repository.Of<Group>().GetById(model.GroupId.Value);

                var success = true;
                string message;
                try
                {
                    message = _ldapBulkLoader.Synchronize(scheme, group);
                    uow.Commit();
                }
                catch (Exception e)
                {
                    OlimpApplication.Logger.Error(e.ToString());
                    success = false;
                    message = e.Message;
                }

                return RedirectToAction("Index", new
                {
                    model.GroupId, 
                    model.AuthSchemeId,
                    SynchronizeSuccess = success,
                    SynchronizeMessage = message
                });
            }
        }
    }
}