using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Core.Security;
using Olimp.Domain.Catalogue.Ldap;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Ldap;
using Olimp.Domain.LearningCenter.DataServices;
using Olimp.Domain.LearningCenter.Services;
using Olimp.Employees.Controllers;
using Olimp.Ldap;
using PAPI.Core;
using PAPI.Core.Initialization;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Employees.Plugin))]

namespace Olimp.Employees
{
    public class Plugin : PluginModule
    {
        private IModuleContext _context;

        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
            _context = context;
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(EmployeeController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (_context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var connFactoryFactory = new PerHttpRequestConnectionFactoryFactory(
                new SqliteConnectionFactoryFactory());
            var connFactory = connFactoryFactory.Create();

            var authSchemesRepository = new OrmLiteLdapAuthSchemeRepository(connFactory);

            binder.Controller<EmployeeController>(b => b
                .Bind<IAppointmentService, AppointmentService>()
                .Bind<ICompanyService, CompanyService>()
                .Bind<IGroupService, GroupService>()
                .Bind<IPasswordGenerator, CommonPasswordGenerator>()
                .Bind<IEmployeeBulkLoader>(
                    () => new CsvEmployeeBulkLoader(new CommonPasswordGenerator()),
                    Behavior.Singleton)
                .Bind<ILdapAuthSchemeRepository>(() => authSchemesRepository)
                .Bind<LdapEmployeeBulkLoader>(() => new LdapEmployeeBulkLoader(PContext.ExecutingModule.Database.RepositoryFactory)));
        }
    }
}