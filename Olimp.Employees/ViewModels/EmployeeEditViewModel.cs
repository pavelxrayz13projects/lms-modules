using Olimp.Core.ComponentModel;
using Olimp.Domain.LearningCenter.Entities;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Olimp.Employees.ViewModels
{
    public class EmployeeEditViewModel
    {
        public IEnumerable<SelectListItem> Groups { get; set; }

        public SelectList UILocale { get; set; }

        public int GroupId { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.Employee), DisplayNameResourceName = "Appointment")]
        public string AppointmentNames { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Employee), DisplayNameResourceName = "Profiles")]
        public string ProfilesList { get; set; }

        public int? CompanyId { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.Employee), DisplayNameResourceName = "Company")]
        public string CompanyName { get; set; }

        public Employee Employee { get; set; }
    }
}