using Olimp.UI.ViewModels;

namespace Olimp.Employees.ViewModels
{
    public class EmployeeTableViewModel : TableViewModel
    {
        public int? AuthSchemeId { get; set; }

        public int? GroupId { get; set; }

        public string Filter { get; set; }
    }
}