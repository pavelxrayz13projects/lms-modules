using Olimp.Domain.LearningCenter.Entities;
using System.Collections.Generic;

namespace Olimp.Employees.ViewModels
{
    public class EmployeePrintViewModel
    {
        public string GroupName { get; set; }

        public IEnumerable<Employee> Employees { get; set; }
    }
}