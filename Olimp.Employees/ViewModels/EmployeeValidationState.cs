namespace Olimp.Employees.ViewModels
{
    public enum EmployeeValidationState
    {
        Valid,
        DuplicateNumberInCompany,
        DuplicateFullNameInGroup
    }
}