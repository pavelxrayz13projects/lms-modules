﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Olimp.Employees.ViewModels
{
    public class EmployeeFromLdapViewModel
    {
        public int? GroupId { get; set; }

        public int? AuthSchemeId { get; set; }

        public IEnumerable<SelectListItem> Groups { get; set; }

        public IEnumerable<SelectListItem> AuthSchemes { get; set; }
    }
}