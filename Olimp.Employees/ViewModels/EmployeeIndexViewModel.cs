using System.Collections.Generic;
using System.Web.Mvc;

namespace Olimp.Employees.ViewModels
{
    public class EmployeeIndexViewModel
    {
        public int? GroupId { get; set; }

        public int? AuthSchemeId { get; set; }

        public bool? SynchronizeSuccess { get; set; }
        
        public string SynchronizeMessage { get; set; }

        public string Filter { get; set; }

        public IEnumerable<SelectListItem> Groups { get; set; }

        public IEnumerable<SelectListItem> AuthSchemes { get; set; }

        public IEnumerable<SelectListItem> LdapSchemes { get; set; }

        public SelectList UILocale { get; set; }
    }
}