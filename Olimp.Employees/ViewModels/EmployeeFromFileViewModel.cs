using Olimp.Core.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Olimp.Employees.ViewModels
{
    public class EmployeeFromFileViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Employee), DisplayNameResourceName = "FromFile")]
        public HttpPostedFileBase CsvFile { get; set; }

        public int? GroupId { get; set; }

        public IEnumerable<SelectListItem> Groups { get; set; }
    }
}