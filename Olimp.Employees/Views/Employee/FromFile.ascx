﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<EmployeeFromFileViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Employees.I18N" %>

<% Html.EnableClientValidation(); %>

<% using(Html.BeginForm("FromFile", "Employee", FormMethod.Post, new { 
	id= "employee-csv-from", target = "employee-csv-fake-frame", enctype = "multipart/form-data"  })) { %>
		
	<table class="form-table">
		<tr class="first">
			<th><%= Html.LabelFor(m => m.GroupId, I18N.Employee.Groups) %></th>
			<td><%= Html.DropDownListFor(m => m.GroupId, Model.Groups) %></td>
		</tr>
		<tr class="last">
			<th><%= Html.LabelFor(m => m.CsvFile) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.CsvFile) %></div>
				<div><%= Html.TextBoxFor(m => m.CsvFile, new { type="file" }) %></div>
			</td>
		</tr>
	</table>
<% } %>

<iframe id="employee-csv-fake-frame" name="employee-csv-fake-frame" style="display: none"></iframe>  
