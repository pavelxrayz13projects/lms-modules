﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<EmployeeEditViewModel>" %>

<% Html.EnableClientValidation(); %>
	
<% using(Html.BeginForm("Edit", "Employee", FormMethod.Post, new { id = "employee-edit-form" })) { %>
	
	<%= Html.HiddenFor(m => m.Employee.Id, new Dictionary<string, object> { 
		{ "data-bind", "value: Id" } }) %>
	
	<%= Html.HiddenFor(m => m.Employee.Login, new Dictionary<string, object> { 
		{ "data-bind", "value: Login" } }) %>

	<%= Html.HiddenFor(m => m.Employee.Password, new Dictionary<string, object> { 
		{ "data-bind", "value: Password" } }) %>
					
	<table class="form-table">
		<tr class="first">
			<th><%= Html.LabelFor(m => m.Employee.Surname) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Employee.Surname) %></div>
				<%= Html.TextBoxFor(m => m.Employee.Surname, new { data_bind = "value: Surname, attr: { readonly: IsReadOnly }" }) %>
			</td>
		</tr>
		<tr>
			<th><%= Html.LabelFor(m => m.Employee.Name) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Employee.Name) %></div>
				<%= Html.TextBoxFor(m => m.Employee.Name, new { data_bind = "value: Name, attr: { readonly: IsReadOnly }" }) %>
			</td>
		</tr>
		<tr>
			<th><%= Html.LabelFor(m => m.Employee.GivenName) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Employee.GivenName) %></div>
				<%= Html.TextBoxFor(m => m.Employee.GivenName, new { data_bind = "value: GivenName, attr: { readonly: IsReadOnly }" }) %>
			</td>
		</tr>
		<tr>
			<th><%= Html.LabelFor(m => m.CompanyName) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.CompanyName) %></div>
				<div>
					<%= Html.TextBoxFor(m => m.CompanyName, new { 
						id = "company-name",
						style = "width: 130px;",
						data_bind = "value: CompanyName, enable: !IsExternal()" }) %>
				</div>
			</td>
		</tr>
		<tr>
			<th><%= Html.LabelFor(m => m.AppointmentNames) %><a id="appointment-tooltip" href="#" title="Для выбора элементов можно использовать стрелки клавиатуры и клавишу Enter. При нажатие Enter создается должность с введенным текстом.">(?)</a></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.AppointmentNames) %></div>	
					<%= Html.TextBoxFor(m => m.AppointmentNames, new { 
						id = "appointment-names",
						style = "width: 130px;",
						data_bind = "value: AppointmentNames, enable: !IsExternal()" }) %>
			</td>
		</tr>
		<tr>
			<th><%= Html.LabelFor(m => m.ProfilesList) %><a id="profile-tooltip" href="#" title="Для выбора элементов можно использовать стрелки клавиатуры и клавишу Enter.">(?)</a></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ProfilesList) %></div>	
					<%= Html.TextBoxFor(m => m.ProfilesList, new { 
						id = "profiles",
						style = "width: 130px;",
						data_bind = "value: ProfilesList" }) %>
			</td>
		</tr>
		<tr>
			<th><%= Html.LabelFor(m => m.Employee.Number) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Employee.Number) %></div>
				<%= Html.TextBoxFor(m => m.Employee.Number, new { data_bind = "value: Number, enable: !IsExternal()" }) %>
			</td>
		</tr>
		<tr>
			<th><%= Html.LabelFor(m => m.Employee.KnowledgeControlReason) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Employee.KnowledgeControlReason) %></div>
				<%= Html.DropDownListFor(m => m.Employee.KnowledgeControlReason, new Dictionary<string, object> { 
					{ "id", "control-reason" },
					{ "data-bind", "value: KnowledgeControlReason" } }) %>
			</td>
		</tr>
		<tr>
			<th><%= Olimp.I18N.Domain.Employee.Group %></th>
			<td><%= Html.DropDownListFor(m => m.GroupId, Model.Groups, new {  id = "select-group", data_bind = "value: GroupId" }) %></td>
		</tr>
		<tr>
			<th><%= Html.LabelFor(m => m.Employee.Email) %></th>
			<td><%= Html.TextBoxFor(m => m.Employee.Email, new { data_bind = "value: Email, enable: !IsExternal()" }) %></td>
		</tr>
        <tr>
			<th style="white-space:normal;"><%= Html.LabelFor(m => m.Employee.DoNotInheritIndirectProfiles) %></th>
			<td class="checkbox">
                <%= Html.CheckBoxFor(m => m.Employee.DoNotInheritIndirectProfiles, new { data_bind = "checked: DoNotInheritIndirectProfiles" }) %>				
			</td>
		</tr>
		<tr class="last">
			<th><%= Html.LabelFor(m => m.Employee.UILocale) %></th>
			<td>
                <%= Html.DropDownListFor(m => m.Employee.UILocale, Model.UILocale, new { id = "ui-locale", data_bind = "value: UILocale" }) %>				
			</td>
		</tr>        
	</table>
<% } %>	
