﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<EmployeeIndexViewModel>"  MasterPageFile="~/Views/Shared/Register.master" %>
<%@ Import Namespace="I18N=Olimp.Employees.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Register_Content" runat="server">

	<div class="print-block" style="height: 19px;">
		<a href="<%= Url.Action("Print", "Employee", new { GroupId = Model.GroupId }) %>" target="_blank" class="right">
			<span><%= Olimp.I18N.PlatformShared.PrintLink %></span>
			<div class="icon icon-print"></div>
		</a>	
	</div>
	
	<div id="edit-dialog">
		<% Html.RenderPartial("Edit", new EmployeeEditViewModel { Groups = Model.Groups, UILocale = Model.UILocale }); %>
	</div>	
	
	<div id="from-file-dialog">
		<% Html.RenderPartial("FromFile", new EmployeeFromFileViewModel { GroupId = Model.GroupId, Groups = Model.Groups }); %>
	</div>

    <div id="from-ldap-dialog">
		<% Html.RenderPartial("FromLdap", new EmployeeFromLdapViewModel { 
            GroupId = Model.GroupId, 
            Groups = Model.Groups,
            AuthSchemeId = Model.AuthSchemeId,
            AuthSchemes = Model.LdapSchemes
     }); %>
	</div>
				
	<script type="text/html" id="full-name-template">
		<span data-bind="text: Surname"></span>&nbsp;<span data-bind="text: Name"></span>&nbsp;<span data-bind="text: GivenName"></span>
	</script>
    <% using(Html.BeginForm("Index", "Employee", FormMethod.Get, new { id="group-filter" })) { %>	
      
        <h1><%= I18N.Employee.Employees %></h1>
	    <div class='block'>		
            <div id="filter-block">		
		        <table class="form-table">
                    <tr class="first">
				        <th><%= Html.LabelFor(m => m.GroupId, I18N.Employee.Groups) %></th>
				        <td>
					        <%= Html.DropDownListFor(m => m.GroupId, Model.Groups, I18N.Employee.AllGroups, new { id = "group-select" }) %>
				        </td>
			        </tr>
                    <tr>
				        <th><%= Html.LabelFor(m => m.AuthSchemeId, I18N.Employee.AuthSchemes) %></th>
				        <td>
					        <%= Html.DropDownListFor(m => m.AuthSchemeId, Model.AuthSchemes, I18N.Employee.AllAuthSchemes, new { id = "auth-scheme-select" }) %>
				        </td>
			        </tr>
			        <tr>
				        <th>Фильтр</th>
				        <td>
					        <%= Html.TextBoxFor(m => m.Filter, new Dictionary<string, object> {  
						        { "data-bind", "value: filter, valueUpdate: 'keyup'" }
					        }) %>
				        </td>
			        </tr>
		        </table>
            </div>
					
		    <% Html.Table(
			    new TableOptions {
				    SelectUrl = Url.Action("GetEmployees", new { 
                        GroupId = Model.GroupId.GetValueOrDefault(), 
                        AuthSchemeId = Model.AuthSchemeId.GetValueOrDefault() 
                    }),
				    Adding = new AddingOptions { 
					    Text = I18N.Employee.Add,
					    LoadModelUrl = Url.Action("GetEmployee", new { groupId = Model.GroupId })
				    },
				    DefaultSortColumn = "FullName",
				    PagingAllowAll = true,
                    RowsHighlighting = true,
                Checkboxes = true,
				    Columns = new[] { 
					    new ColumnOptions("Login", Olimp.I18N.Domain.Employee.Login), 
					    new ColumnOptions("Password", Olimp.I18N.Domain.Employee.Password),
					    new ColumnOptions("FullName", Olimp.I18N.Domain.Employee.FullName) { TemplateId = "full-name-template" },
					    new ColumnOptions("Number", Olimp.I18N.Domain.Employee.Number),
					    new ColumnOptions("ProfilesList", I18N.Employee.Profiles),
					    new ColumnOptions("GroupName", Olimp.I18N.Domain.Employee.Group)
				    },
				    Actions = new RowAction[] { 
					    new EditAction("edit-dialog", new DialogOptions { Title = I18N.Employee.Edit, Width = "600px" }) {
						    LoadModelUrl = Url.Action("GetEmployee")
					    },
					    new DeleteAction(Url.Action("Delete"), I18N.Employee.ConfirmDelete)	},
				    TableActions = new TableAction[] {
					    new FileUploadAction("from-file-dialog", "employee-csv-fake-frame", I18N.Employee.FromFile,
						    new DialogOptions { Title = I18N.Employee.FromFileTitle }),
                        new CustomTableAction("openFromLdapDialog", null, I18N.Employee.FromLdap, null),
                        new MassDeleteAction(Url.Action("MassDelete"), I18N.Employee.ConfirmMassDelete, Olimp.I18N.PlatformShared.Delete)	
                        {
                            MessageIfNothingSelected = I18N.Employee.NoEmployeeSelected,
                            SuccessMessage = I18N.Employee.EmployeesSuccessfullyDeleted
                        }
				    }
			    },
		    new { id = "employees-table" }); %>
	    </div>
    <% } %>
            
    <script type="text/javascript">
        $(function () {
            <% if (Model.SynchronizeSuccess.GetValueOrDefault()) {%>
                  $.Olimp.showSuccess('<%= Model.SynchronizeMessage %>');
            <% } else if (!string.IsNullOrWhiteSpace(Model.SynchronizeMessage)) {%>
                  $.Olimp.showError('<%= Model.SynchronizeMessage %>');
            <% } %>
        });
    </script>

	<script type="text/javascript">
	    function openFromLdapDialog() {
	        $('#from-ldap-dialog').olimpsyncformdialog('open');
	    }

	    function disableButton(button, isDisabled) {
	        $(button).parent().olimpbutton('option', 'disabled', isDisabled);
	    }

	    $(function () {
	        $("#filter-block").collapsibleBlock({ headerText: "<%= I18N.Employee.FilterParams %>", initialyExpanded: true });

	        $('#from-file-dialog select, #from-ldap-dialog select').combobox();

	        $('#from-ldap-dialog').olimpsyncformdialog({
	            width: 480,
	            resizable: false,
	            title: '<%= I18N.Employee.FromLdapTitle %>',
	            saveText: '<%= Olimp.I18N.PlatformShared.Save %>',
	            cancelText: '<%= Olimp.I18N.PlatformShared.Cancel %>',
	        });
	        
	        var viewModel = { filter: ko.observable('') },
	            tableModel = $('#employees-table').data('viewModel');
		
	        viewModel.throttledFilter = ko.computed(viewModel.filter)
	            .extend({ throttle: 400 });
		
	        viewModel.throttledFilter.subscribe(function(value) {
	            tableModel.requestData({ 
	                Filter: value, 
	                __callback: function() {
	                    $.Olimp.closeWarnings();
	                    if (!this.rows().length) {
	                        $.Olimp.showWarning('По заданным параметрам не найдено ни одного работника');
	                    }
	                } 
	            });
	        }, viewModel)
		
	        ko.applyBindings(viewModel, $('#filter-block')[0]);
		
	        $('#group-select, #auth-scheme-select')
	            .change(function() { $('#group-filter').submit(); })
	            .combobox();
		
	        $.fn.multiselect.initValidation();

	        $('#edit-dialog').bind('afterApply', function () {
	            $(this).find('form').validate().settings.ignore = [];
	            $.Olimp.Tooltip.Init($('#appointment-tooltip, #profile-tooltip'), "tooltip");
	            $('#control-reason, #select-group, #ui-locale').combobox();
	            $('#appointment-names').multiselect({
	                url: '<%= Url.Action("LookupAppointment") %>',
	                separator: '#'
	            });
	            $('#profiles').multiselect({
	                createOnEnter: false,
	                url: '<%= Url.Action("LookupProfile") %>'
	            });
	            $('#company-name').combobox({ 
	                source: function (request, response) {
	                    $.post('<%= Url.Action("LookupCompany") %>', { filter: request.term }, response);
	                } });
	        });
	    })
	</script>
</asp:Content>