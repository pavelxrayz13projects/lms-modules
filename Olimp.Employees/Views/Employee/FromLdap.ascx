﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<EmployeeFromLdapViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Employees.I18N" %>

<% Html.EnableClientValidation(); %>

<% using(Html.BeginForm("SyncFromLdap", "Employee", FormMethod.Post, new { id= "employee-ldap-from" })) { %>
		
	<table class="form-table">
		<tr class="first">
			<th><%= Html.LabelFor(m => m.GroupId, I18N.Employee.Groups) %></th>
			<td><%= Html.DropDownListFor(m => m.GroupId, Model.Groups) %></td>
		</tr>
        <tr class="last">
		    <th><%= Html.LabelFor(m => m.AuthSchemeId, I18N.Employee.AuthSchemes) %></th>
		    <td><%= Html.DropDownListFor(m => m.AuthSchemeId, Model.AuthSchemes) %></td>
        </tr>		
	</table>
<% } %>
