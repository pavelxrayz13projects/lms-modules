﻿ <%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<EmployeePrintViewModel>"  MasterPageFile="~/Views/Shared/Popup.master" %>

<%-- TODO: Same as Archive->Print --%>
<asp:Content ContentPlaceHolderID="Olimp_Css" runat="server">
	<style type="text/css">	
		* { font-family: Arial; font-size: x-small; font-weight: normal; }	
		body { padding: 20px 10px; }
		table { width: 100%; border-collapse: collapse; border: solid 1px black; border-bottom: 0; border-right: 0; margin: 10px 0; }
		table tr { border-bottom: solid 1px black; }
		table tr th,
		table tr td { border-right: solid 1px black; padding: 3px; }
		.separated-line { margin: 20px 0; }
		
		@media print {
			button { display: none; }
		}
	</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Body" runat="server">

	<h1>Наименование группы: <%= Model.GroupName %></h1>
	
	<table>
		<thead>
			<tr>
				<th>№</th>
				<th><%= Olimp.I18N.Domain.Employee.FullName %></th>
				<th><%= Olimp.I18N.Domain.Employee.Login %></th>
				<th><%= Olimp.I18N.Domain.Employee.Password %></th>
				<th><%= Olimp.I18N.Domain.Employee.Email %></th>
				<th><%= Olimp.I18N.Domain.Employee.Company %></th>
				<th><%= Olimp.I18N.Domain.Employee.Number %></th>
				<th><%= Olimp.I18N.Domain.Employee.KnowledgeControlReason %></th>
				<th><%= Olimp.I18N.Domain.Employee.Profile %></th>
			</tr>
		</thead>
	<% foreach(var o in Model.Employees.Select((e, i) => new { Employee = e, Number = i + 1 })) { %>
		<tr>
			<td><%= o.Number %></td>
			<td><%= o.Employee %></td>
			<td><%= o.Employee.Login %></td>
			<td><%= o.Employee.Password %></td>
			<td><%= o.Employee.Email %></td>
			<td><%= o.Employee.Company %></td>
			<td><%= o.Employee.Number %></td>
			<td><%= Html.EnumValueDisplayName(o.Employee.KnowledgeControlReason) %></td>
			<td><%= o.Employee.ProfilesToString() %></td>
		</tr>
	<% } %>
	</table>
	
	<div class="separated-line">
		<button onclick="window.print()">Печать</button>
	</div>
	
</asp:Content>
