using Olimp.Companies.Controllers;
using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Domain.LearningCenter.DataServices;
using PAPI.Core;
using PAPI.Core.Initialization;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Companies.Plugin))]

namespace Olimp.Companies
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        { }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(CompanyController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            binder.Controller<CompanyController>(b => b
                .Bind<ICompanyService, CompanyService>());
        }
    }
}