﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage"  MasterPageFile="~/Views/Shared/Register.master" %>
<%@ Import Namespace="I18N=Olimp.Companies.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Register_Content" runat="server">
	<h1><%= I18N.Company.Companies %></h1>		
	
	<div id="edit-dialog">	
		<% Html.RenderPartial("Edit"); %>
	</div>
	
	<div class="block">	
		<% Html.Table(
			new TableOptions {
				SelectUrl = Url.Action("GetCompanies"),
				Adding = new AddingOptions { 
					Text = I18N.Company.Add,
                    New = new { Id = 0, Name = "", ProfilesList = "" }
				},
				PagingAllowAll = true,
				Columns = new[] { 
					new ColumnOptions("Name", Olimp.I18N.Domain.Company.Name),
                    new ColumnOptions("ProfilesList", I18N.Company.Profiles)},
				Actions = new RowAction[] { 
					new EditAction("edit-dialog", new DialogOptions { Title = I18N.Company.Edit, Width = "600px" }),
					new DeleteAction(Url.Action("Delete"), I18N.Company.ConfirmDelete)	}
			}); %>
	</div>
    <script type="text/javascript">
        $('#edit-dialog').css("overflow", "hidden").bind('afterApply', function () {
            $.Olimp.Tooltip.Init($(".profile-tooltip"), "tooltip");
            $("#profiles").multiselect({
                createOnEnter:false,
                url: '<%= Url.Action("LookupProfile") %>'
		        });
         });
    </script>
</asp:Content>
