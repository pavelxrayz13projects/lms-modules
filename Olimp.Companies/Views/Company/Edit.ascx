﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<CompanyEditViewModel>" %>

<% Html.EnableClientValidation(); %>
	
<% using(Html.BeginForm("Edit", "Company")) { %>
	
	<%= Html.HiddenFor(m => m.Company.Id, new Dictionary<string, object> { 
		{ "data-bind", "value: Id" } }) %>
	
	<table class="form-table">
		<tr class="first">
			<th><%= Html.LabelFor(m => m.Company.Name) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Company.Name) %></div>
				<%= Html.TextBoxFor(m => m.Company.Name, new Dictionary<string, object> { 
					{ "data-bind", "value: Name" } }) %>
			</td>
		</tr>
		<tr class="last">
			<th><%= Html.LabelFor(m => m.ProfilesList) %><a class="profile-tooltip" href="#" title="<%=Olimp.I18N.Domain.Profile.Tooltip %>">(?)</a></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ProfilesList) %></div>	
					<%= Html.TextBoxFor(m => m.ProfilesList, new Dictionary<string, object> { 
						{ "id", "profiles" },
						{ "data-bind", "value: ProfilesList" } }) %>
			</td>
		</tr>
	</table>
<% } %>			


	