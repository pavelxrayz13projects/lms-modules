using Olimp.Companies.ViewModels;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter.DataServices;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Companies.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Registration.Companies, Order = 2)]
    public class CompanyController : PController
    {
        private ICompanyService _companyService;

        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        [HttpGet]
        public ActionResult Index() { return View(); }

        [HttpPost]
        public ActionResult GetCompanies(TableViewModel model)
        {
            using (UnitOfWork.CreateRead())
            {
                return Json(
                    _companyService.GetActiveCompanies()
                        .GetTableData(model, c => new
                        {
                            c.Id,
                            c.Name,
                            ProfilesList = c.ProfilesToString()
                        }));
            }
        }

        [HttpPost]
        public ActionResult Edit(CompanyEditViewModel model)
        {
            if (!ModelState.IsValid)
                return null;

            var profileRepo = Repository.Of<Profile>();
            var employeeRepo = Repository.Of<Employee, Guid>();
            var companyRepo = Repository.Of<Company>();
            var isNew = model.Company.Id == 0;

            using (var uow = UnitOfWork.CreateWrite())
            {
                if (!isNew)
                {
                    model.Company = companyRepo.LoadById(model.Company.Id);
                    this.UpdateModel(model);
                }

                string[] profileArray = String.IsNullOrWhiteSpace(model.ProfilesList) ? new string[0] : model.ProfilesList.Split(',');
                var profilesChanged = !profileArray.OrderBy(pn => pn)
                    .SequenceEqual(model.Company.Profiles != null ?
                        (IEnumerable<string>)model.Company.Profiles.Select(p => p.Name).OrderBy(pn => pn) :
                        new List<string>());
                if (profilesChanged)
                {
                    // Update Employees ChangeInProfileEntitiesDate
                    foreach (var employee in employeeRepo.Where(em => em.Company.Id == model.Company.Id))
                        employee.ChangeInProfileEntitiesDate = DateTime.Now;
                    // Update Company Profiles
                    if (model.Company.Profiles != null) model.Company.Profiles.Clear();
                    foreach (var profileName in profileArray)
                    {
                        if (String.IsNullOrWhiteSpace(profileName))
                            continue;
                        var profile = profileRepo.FirstOrDefault(a => a.Name == profileName);
                        if (profile != null)
                            model.Company.AddProfile(profile);
                    }
                }
                companyRepo.Save(model.Company);
                uow.Commit();
            }

            return isNew
                ? null
                : Json(new
                {
                    model.Company.Id,
                    model.Company.Name,
                    ProfilesList = model.Company.ProfilesToString()
                });
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var companyRepo = Repository.Of<Company>();
                var employeeRepo = Repository.Of<Employee, Guid>();
                // Update Employees ChangeInProfileEntitiesDate
                foreach (var employee in employeeRepo.Where(em => em.Company.Id == id))
                    employee.ChangeInProfileEntitiesDate = DateTime.Now;
                companyRepo.GetById(id).Profiles.Clear();
                companyRepo.DeleteById(id);

                uow.Commit();
            }

            return null;
        }

        [HttpPost]
        public ActionResult LookupProfile(string filter, string selectedItems)
        {
            using (UnitOfWork.CreateRead())
            {
                var proilesArray = selectedItems.Split(',');
                var profiles = Repository.Of<Profile>()
                        .Where(p => p.Name.Contains(filter) && !proilesArray.Contains(p.Name))
                        .OrderBy(p => p.Name).AsEnumerable();

                return Json(profiles.Select(p => new { label = p.Name, optionValue = p.Name }));
            }
        }
    }
}