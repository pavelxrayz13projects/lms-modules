using Olimp.Core.ComponentModel;
using Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Companies.ViewModels
{
    public class CompanyEditViewModel
    {
        public Company Company { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Company), DisplayNameResourceName = "Profiles")]
        public string ProfilesList { get; set; }
    }
}