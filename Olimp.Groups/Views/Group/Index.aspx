﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<IndexViewModel>"  MasterPageFile="~/Views/Shared/Register.master" %>
<%@ Import Namespace="I18N=Olimp.Groups.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Register_Content" runat="server">

	<h1><%= I18N.Group.Groups %></h1>		
	
	<div id="edit-dialog" data-afterapply="function() { $('.period-picker').datepicker({ changeMonth: true, changeYear: true }) }">	
		<% Html.RenderAction("Edit"); %>
	</div>
			
	<script type="text/html" id="group-name-template">
		<span data-bind="text: Name"></span><span>&nbsp;(</span><a data-bind="text: EmployeesCount, attr: { href: '<%= Url.Action("Index", "Employee") %>?GroupId=' + Id() }"></a><span>)</span>
	</script>
	
	<div class="block">	
		<% Html.Table(
			new TableOptions {
				SelectUrl = Url.Action("GetGroups"),
				Adding = new AddingOptions { 
					Text = I18N.Group.Add,
					New = new { 
						Id = 0, 
						Name = "", 
						Prefix = "", 
						Description = "", 
                        ProfilesList = "",
						ExamBeginDate = DateTime.Today.ToShortDateString(), 
						ExamEndDate = DateTime.Today.ToShortDateString(),
                        ExamSettingsId = 0 
					}
				},
				DefaultSortColumn = "Name",
				PagingAllowAll = true,
                Checkboxes = true,
                RowsHighlighting = true,
				Columns = new[] { 
					new ColumnOptions("Name", Olimp.I18N.Domain.Group.Name) { TemplateId = "group-name-template" }, 
					new ColumnOptions("Prefix", Olimp.I18N.Domain.Group.Prefix),
                    new ColumnOptions("ProfilesList", I18N.Group.Profiles),
                    new ColumnOptions("ExamSettingsName", I18N.Group.ExamSettings),
					new ColumnOptions("ExamBeginDate", I18N.Group.DateBegin),
					new ColumnOptions("ExamEndDate", I18N.Group.DateEnd),
					new ColumnOptions("Description", Olimp.I18N.Domain.Group.Description)
				},
				Actions = new RowAction[] { 
					new EditAction("edit-dialog", new DialogOptions { Title = I18N.Group.Edit, Width = "630px" }),
					new DeleteAction(Url.Action("Delete"), I18N.Group.ConfirmDelete)	
                },
                TableActions = new TableAction[] { 
					new MassDeleteAction(Url.Action("MassDelete"), I18N.Group.ConfirmMassDelete, Olimp.I18N.PlatformShared.Delete)	
                    {
                        MessageIfNothingSelected = I18N.Group.NoGroupSelected,
                        SuccessMessage = I18N.Group.GroupsSuccessfulyDeleted
                    }
                }
			}); %>
	</div>	
	
	<script type="text/javascript">
	$(function() {		
	    $('#edit-dialog').bind('afterApply', function() { 
	        $(this).find('select').combobox();
	        $.Olimp.Tooltip.Init($(".profile-tooltip"), "tooltip");
	        $("#profiles").multiselect({
	            createOnEnter: false,
	            url: '<%= Url.Action("LookupProfile") %>'
            });
	    });
	})
	</script>
</asp:Content>
