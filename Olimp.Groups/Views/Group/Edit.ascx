﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<GroupEditViewModel>" %>

<% Html.EnableClientValidation(); %>
    
<% using(Html.BeginForm("Save", "Group")) { %>
    
    <!-- ko if: Id -->
    <%= Html.Hidden("id", null, new Dictionary<string, object> { 
        { "data-bind", "value: Id" } }) %>
    <!-- /ko -->	

    <table class="form-table">
        <tr class="first">
            <th><%= Html.LabelFor(m => m.Group.Name) %></th>
            <td>				
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Group.Name) %></div>
                <%= Html.TextBoxFor(m => m.Group.Name, new Dictionary<string, object> { 
                    { "data-bind", "value: Name" } }) %>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.GroupPrefix) %></th>
            <td>	
                <%= Html.HiddenFor(m => m.Group.Prefix, new Dictionary<string, object> { 
                    { "data-bind", "value: Prefix" } }) %>
            
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.GroupPrefix) %></div>
                <%= Html.TextBoxFor(m => m.GroupPrefix, new Dictionary<string, object> { 
                    { "data-bind", "value: Prefix" } }) %>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.Group.Description) %></th>
            <td>				
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Group.Description) %></div>
                <textarea name="Group.Description" data-bind="value: Description" 
                    data-val="true" data-val-required="<%: String.Format(Olimp.I18N.PlatformShared.RequiredInvalid, Olimp.I18N.Domain.Group.Description) %>"></textarea>
                <%--
                    <%= Html.TextAreaFor(m => m.Group.Description, new Dictionary<string, object> { 
                    { "data-bind", "value: Description" } }) %>
                --%>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.ExamSettingsId) %></th>
            <td>				
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ExamSettingsId) %></div>	
                    <%= Html.DropDownListFor(m => m.ExamSettingsId, Model.AllExamSettings, new { data_bind = "value: ExamSettingsId" }) %>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.ProfilesList) %><a class="profile-tooltip" href="#" title="<%=Olimp.I18N.Domain.Profile.Tooltip %>">(?)</a></th>
            <td>				
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ProfilesList) %></div>	
                    <%= Html.TextBoxFor(m => m.ProfilesList, new Dictionary<string, object> { 
                        { "id", "profiles" },
                        { "data-bind", "value: ProfilesList" } }) %>
            </td>
        </tr>
        <tr class="last">
            <th>Период проведения аттестации</th>
            <td class="date-span">				
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Group.ExamBeginDate) %></div>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Group.ExamEndDate) %></div>
                <%= Html.LabelFor(m => m.Group.ExamBeginDate) %>
                <%= Html.TextBoxFor(m => m.Group.ExamBeginDate, new Dictionary<string, object> { 
                        { "class", "period-picker" },
                        { "data-bind", "value: ExamBeginDate" }
                }) %>
                <%= Html.LabelFor(m => m.Group.ExamEndDate) %>
                <%= Html.TextBoxFor(m => m.Group.ExamEndDate, new Dictionary<string, object> { 
                        { "class", "period-picker" },
                        { "data-bind", "value: ExamEndDate" }
                }) %>
            </td>
        </tr>		
    </table>	
<% } %>	

