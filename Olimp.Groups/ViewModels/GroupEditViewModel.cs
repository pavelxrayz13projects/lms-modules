using Olimp.Core.ComponentModel;
using Olimp.Domain.LearningCenter.Entities;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.Groups.ViewModels
{
    using System.Collections.Generic;
    using I18N = Olimp.I18N.Domain;

    public class GroupEditViewModel
    {
        public Group Group { get; set; }

        [Remote("PrefixExists", "Group", "Admin", AdditionalFields = "id", ErrorMessageResourceType = typeof(I18N.Group), ErrorMessageResourceName = "PrefixExists")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Group), DisplayNameResourceName = "Prefix")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [RegularExpression(@"\S+", ErrorMessageResourceType = typeof(Groups.I18N.Group), ErrorMessageResourceName = "DisallowWhiteSpace")]
        [StringLength(30, ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "StringLengthInvalid")]
        public string GroupPrefix { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.Groups.I18N.Group), DisplayNameResourceName = "ExamSettings")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int ExamSettingsId { get; set; }

        public IEnumerable<SelectListItem> AllExamSettings { get; set; }

        public bool IsInternetVersion { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Groups.I18N.Group), DisplayNameResourceName = "Profiles")]
        public string ProfilesList { get; set; }
    }
}