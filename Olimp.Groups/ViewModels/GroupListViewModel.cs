using Olimp.Domain.LearningCenter.Entities;
using System.Collections.Generic;

namespace Olimp.Groups.ViewModels
{
    public class GroupListViewModel
    {
        public IEnumerable<Group> Groups { get; set; }
    }
}