using Olimp.Configuration;
using Olimp.Core;
using Olimp.Core.Mvc.Security;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Groups.ViewModels;
using Olimp.UI.LearningCenter;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core;
using PAPI.Core.Data;
using PAPI.Core.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Groups.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Registration.Groups, Order = 2)]
    public class GroupController : PController
    {
        public GroupController()
        {
        }

        private bool IsInternetVersion
        {
            get
            {
                var olimpModule = (OlimpModule)PContext.ExecutingModule;
                var activationKeyInfo = olimpModule.Activation.GetActivationKeyInfo();

                return activationKeyInfo != null && activationKeyInfo.IsInternetVersion;
            }
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new IndexViewModel { IsInternetVersion = this.IsInternetVersion });
        }

        [HttpGet]
        public ActionResult PrefixExists(string groupPrefix)
        {
            using (UnitOfWork.Create())
            {
                var id = Request.QueryString["id"];

                int groupId;
                if (String.IsNullOrEmpty(id) || !int.TryParse(id, out groupId))
                    groupId = 0;

                return Json(
                    !Repository.Of<Group>().Any(g => g.Prefix == groupPrefix && g.IsActive && g.Id != groupId),
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetGroups(TableViewModel model)
        {
            model.SortingHandlers.Add("ProfileName", new SortGroupsByProfileBuilder());
            model.SortingHandlers.Add("ExamSettingsName", new SortGroupsByExamSettingsBuilder());

            using (UnitOfWork.CreateRead())
            {
                return Json(Repository.Of<Group>()
                    .Where(new EntityIsActive<Group>())
                    .GetTableData(model, g => new
                    {
                        g.Id,
                        g.Name,
                        g.Prefix,
                        g.Description,
                        g.EmployeesCount,
                        ExamSettingsId = g.ExamSettings.Id,
                        ExamSettingsName = g.ExamSettings.Name,
                        ProfilesList = g.ProfilesToString(),
                        ExamBeginDate = g.ExamBeginDate.ToShortDateString(),
                        ExamEndDate = g.ExamEndDate.ToShortDateString()
                    }));
            }
        }

        [HttpGet]
        public ActionResult Edit()
        {
            using (UnitOfWork.CreateRead())
            {
                return PartialView(new GroupEditViewModel
                {
                    IsInternetVersion = IsInternetVersion,
                    AllExamSettings = Repository.Of<ExamSettings>()
                        .Where(e => e.IsActive)
                        .OrderBy(e => e.Name)
                        .Select(e => new SelectListItem { Text = e.Name, Value = e.Id.ToString() })
                        .ToList()
                });
            }
        }

        [HttpPost]
        public ActionResult Save(int? id)
        {
            var repo = Repository.Of<Group>();
            var profileRepo = Repository.Of<Profile>();
            var employeeRepo = Repository.Of<Employee, Guid>();
            var examSettingsRepo = Repository.Of<ExamSettings>();

            var model = new GroupEditViewModel();

            using (var uow = UnitOfWork.CreateWrite())
            {
                if (id.HasValue)
                    model.Group = repo.LoadById(id);

                if (!TryUpdateModel(model))
                {
                    PAPI.Core.Diagnostics.Debug.PrintModelState(this);
                    return null;
                }

                string[] profileArray = String.IsNullOrWhiteSpace(model.ProfilesList) ? new string[0] : model.ProfilesList.Split(',');
                var profilesChanged = !profileArray.OrderBy(pn => pn)
                    .SequenceEqual(model.Group.Profiles != null ?
                        (IEnumerable<string>)model.Group.Profiles.Select(p => p.Name).OrderBy(pn => pn) :
                        new List<string>());

                if (profilesChanged)
                {
                    // Update Employees ChangeInProfileEntitiesDate
                    foreach (var employee in employeeRepo.Where(em => em.Company.Id == model.Group.Id))
                        employee.ChangeInProfileEntitiesDate = DateTime.Now;
                    // Update Group Profiles
                    if (model.Group.Profiles != null) model.Group.Profiles.Clear();
                    foreach (var profileName in profileArray)
                    {
                        if (String.IsNullOrWhiteSpace(profileName))
                            continue;
                        var profile = profileRepo.FirstOrDefault(a => a.Name == profileName);
                        if (profile != null)
                            model.Group.AddProfile(profile);
                    }
                }

                model.Group.ExamSettings = examSettingsRepo.GetById(model.ExamSettingsId);

                repo.Save(model.Group);
                uow.Commit();
            }

            return Json(new
            {
                model.Group.Id,
                model.Group.Name,
                model.Group.Prefix,
                model.Group.Description,
                model.ExamSettingsId,
                ExamSettingsName = model.Group.ExamSettings.Name,
                ProfilesList = model.Group.ProfilesToString(),
                ExamBeginDate = model.Group.ExamBeginDate.ToShortDateString(),
                ExamEndDate = model.Group.ExamEndDate.ToShortDateString()
            });
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (var uow = UnitOfWork.CreateRead())
            {
                var defaultGroupId = Config.Get<OlimpConfiguration>().DefaultGroup;
                if (id == defaultGroupId)
                    return new StatusCodeResultWithText(500, I18N.Group.CannotDeleteDefaultGroup);

                uow.UpgradeToWritable();

                DeleteGroupByIdAndSetChangeInProfileEntitiesDateForEmployees(
                    id, defaultGroupId, Repository.Of<Group>(), Repository.Of<Employee, Guid>());

                uow.Commit();
            }

            return null;
        }

        [HttpPost]
        public ActionResult MassDelete(ICollection<int> objects)
        {
            var groupRepo = Repository.Of<Group>();
            var employeeRepo = Repository.Of<Employee, Guid>();

            var removed = new List<int>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                var defaultGroupId = Config.Get<OlimpConfiguration>().DefaultGroup;

                foreach (var id in objects)
                {
                    if (DeleteGroupByIdAndSetChangeInProfileEntitiesDateForEmployees(id, defaultGroupId, groupRepo, employeeRepo))
                        removed.Add(id);
                }

                uow.Commit();
            }

            return Json(new { removed });
        }

        private static bool DeleteGroupByIdAndSetChangeInProfileEntitiesDateForEmployees(
            int id, int defaultGroupId, IRepository<Group, int> groupRepository, IRepository<Employee, Guid> employeeRepository)
        {
            if (id == defaultGroupId)
                return false;

            foreach (var e in employeeRepository.Where(em => em.Group.Id == id))
                e.ChangeInProfileEntitiesDate = DateTime.Now;

            groupRepository.DeleteById(id);

            return true;
        }

        [HttpPost]
        public ActionResult LookupProfile(string filter, string selectedItems)
        {
            using (UnitOfWork.CreateRead())
            {
                var proilesArray = selectedItems.Split(',');
                var profiles = Repository.Of<Profile>()
                        .Where(p => p.Name.Contains(filter) && !proilesArray.Contains(p.Name))
                        .OrderBy(p => p.Name).AsEnumerable();

                return Json(profiles.Select(p => new { label = p.Name, optionValue = p.Name }));
            }
        }
    }
}