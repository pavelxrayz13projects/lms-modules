﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage"  MasterPageFile="~/Views/Shared/Register.master" %>
<%@ Import Namespace="I18N=Olimp.Appointments.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Register_Content" runat="server">
	<h1><%= I18N.Appointment.Appointments %></h1>		

	<div id="edit-dialog">	
		<% Html.RenderPartial("Edit"); %>
	</div>
	
	<div class="block">	
		<% Html.Table(
			new TableOptions {
				SelectUrl = Url.Action("GetAppointments"),
				Adding = new AddingOptions { 
					Text = I18N.Appointment.Add,
                    New = new { Id = 0, Name = "", ProfilesList = "" }
				},
				DefaultSortColumn = "Name",
				PagingAllowAll = true,
				Columns = new[] { 
					new ColumnOptions("Name", Olimp.I18N.Domain.Appointment.Name),
                    new ColumnOptions("ProfilesList", I18N.Appointment.Profiles)},
				Actions = new RowAction[] { 
					new EditAction("edit-dialog", new DialogOptions { Title = I18N.Appointment.Edit, Width = "590px" }),
					new DeleteAction(Url.Action("Delete"), I18N.Appointment.ConfirmDelete)	}
			}); %>
	</div>
    <script type="text/javascript">
        $('#edit-dialog').css("overflow", "hidden").bind('afterApply', function () {
            $.Olimp.Tooltip.Init($(".profile-tooltip"), "tooltip");
            $("#profiles").multiselect({
                createOnEnter: false,
                url: '<%= Url.Action("LookupProfile") %>'
            });
        });
    </script>
</asp:Content>