﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<AppointmentEditViewModel>" %>

<% Html.EnableClientValidation(); %>
	
<% using(Html.BeginForm("Edit", "Appointment")) { %>
	
	<%= Html.HiddenFor(m => m.Appointment.Id, new Dictionary<string, object> { 
		{ "data-bind", "value: Id" } }) %>
	
	<table class="form-table">
		<tr class="first">
			<th><%= Html.LabelFor(m => m.Appointment.Name) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Appointment.Name) %></div>
				<%= Html.TextBoxFor(m => m.Appointment.Name, new Dictionary<string, object> { 
					{ "data-bind", "value: Name" } }) %>
			</td>
		</tr>
		<tr class="last">
			<th><%= Html.LabelFor(m => m.ProfilesList) %><a class="profile-tooltip" href="#" title="<%=Olimp.I18N.Domain.Profile.Tooltip %>">(?)</a></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ProfilesList) %></div>	
					<%= Html.TextBoxFor(m => m.ProfilesList, new Dictionary<string, object> { 
						{ "id", "profiles" },
						{ "data-bind", "value: ProfilesList" } }) %>
			</td>
		</tr>
	</table>
	
<% } %>


	