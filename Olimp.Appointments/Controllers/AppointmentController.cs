using Olimp.Appointments.ViewModels;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter.DataServices;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Appointments.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Registration.Positions, Order = 2)]
    public class AppointmentController : PController
    {
        private IAppointmentService _appointmentService;

        public AppointmentController(IAppointmentService appointmentService)
        {
            _appointmentService = appointmentService;
        }

        [HttpGet]
        public ActionResult Index() { return View(); }

        [HttpPost]
        public ActionResult GetAppointments(TableViewModel model)
        {
            using (UnitOfWork.CreateRead())
            {
                return Json(
                    _appointmentService.GetActiveAppointments()
                        .GetTableData(model, a => new
                        {
                            a.Id,
                            a.Name,
                            ProfilesList = a.ProfilesToString()
                        }));
            }
        }

        [HttpPost]
        public ActionResult Edit(AppointmentEditViewModel model)
        {
            if (!ModelState.IsValid)
                return null;

            var appointmentRepo = Repository.Of<Appointment>();
            var profileRepo = Repository.Of<Profile>();
            var employeeRepo = Repository.Of<Employee, Guid>();
            var isNew = model.Appointment.Id == 0;

            using (var uow = UnitOfWork.CreateWrite())
            {
                if (!isNew)
                {
                    model.Appointment = appointmentRepo.LoadById(model.Appointment.Id);
                    this.UpdateModel(model);
                }

                string[] profileArray = String.IsNullOrWhiteSpace(model.ProfilesList) ? new string[0] : model.ProfilesList.Split(',');
                var profilesChanged = !profileArray.OrderBy(pn => pn)
                    .SequenceEqual(model.Appointment.Profiles != null ?
                        (IEnumerable<string>)model.Appointment.Profiles.Select(p => p.Name).OrderBy(pn => pn) :
                        new List<string>());
                if (profilesChanged)
                {
                    // Update Employees ChangeInProfileEntitiesDate
                    foreach (var employee in employeeRepo.Where(em => em.Appointments.Select(ap => ap.Id).Contains(model.Appointment.Id)))
                        employee.ChangeInProfileEntitiesDate = DateTime.Now;
                    // Update Appointment Profiles
                    if (model.Appointment.Profiles != null) model.Appointment.Profiles.Clear();
                    foreach (var profileName in profileArray)
                    {
                        if (String.IsNullOrWhiteSpace(profileName))
                            continue;
                        var profile = profileRepo.FirstOrDefault(a => a.Name == profileName);
                        if (profile != null)
                            model.Appointment.AddProfile(profile);
                    }
                }

                appointmentRepo.Save(model.Appointment);
                uow.Commit();
            }
            return isNew
                ? null
                : Json(new
                {
                    model.Appointment.Id,
                    model.Appointment.Name,
                    ProfilesList = String.Join(",", model.Appointment.Profiles.Select(p => p.Name))
                });
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (var uow = UnitOfWork.CreateWrite())
            {
                var appointmentRepo = Repository.Of<Appointment>();
                var employeeRepo = Repository.Of<Employee, Guid>();
                // Update Employees ChangeInProfileEntitiesDate
                foreach (var employee in employeeRepo.Where(em => em.Appointments.Select(ap => ap.Id).Contains(id)))
                    employee.ChangeInProfileEntitiesDate = DateTime.Now;
                appointmentRepo.GetById(id).Profiles.Clear();
                appointmentRepo.DeleteById(id);

                uow.Commit();
            }

            return null;
        }

        [HttpPost]
        public ActionResult LookupProfile(string filter, string selectedItems)
        {
            using (UnitOfWork.CreateRead())
            {
                var proilesArray = selectedItems.Split(',');
                var profiles = Repository.Of<Profile>()
                        .Where(p => p.Name.Contains(filter) && !proilesArray.Contains(p.Name))
                        .OrderBy(p => p.Name).AsEnumerable();

                return Json(profiles.Select(p => new { label = p.Name, optionValue = p.Name }));
            }
        }
    }
}