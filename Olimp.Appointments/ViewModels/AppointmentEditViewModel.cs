using Olimp.Core.ComponentModel;
using Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Appointments.ViewModels
{
    public class AppointmentEditViewModel
    {
        public Appointment Appointment { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Appointment), DisplayNameResourceName = "Profiles")]
        public string ProfilesList { get; set; }
    }
}