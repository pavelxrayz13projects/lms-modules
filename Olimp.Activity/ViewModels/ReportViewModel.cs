using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Olimp.Activity.ViewModels
{
    public class ReportViewModel
    {
        public int? CompanyId { get; set; }

        public int? CommissionId { get; set; }

        public string Company { get; set; }

        public DateTime? Date { get; set; }

        public string Number { get; set; }

        public bool AllowGenerateReports { get; set; }

        public bool AllowWrite { get; set; }

        public IEnumerable<SelectListItem> Commissions { get; set; }

        public IEnumerable<SelectListItem> Companies { get; set; }
    }
}