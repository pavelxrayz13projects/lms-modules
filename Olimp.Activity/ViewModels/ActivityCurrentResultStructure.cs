using System;

namespace Olimp.Activity.ViewModels
{
	public class ActivityCurrentResultStructure
	{
		public int Number {get; set;}
		public string Question {get; set;}
		public Guid QuestionId {get; set;}
		public string Answers {get; set;}
		public bool Correct {get; set;}			
	}
}


