﻿using Olimp.Domain.LearningCenter.Entities;
using Olimp.UI.ViewModels;

namespace Olimp.Activity.ViewModels
{
    public class CommissionViewModel : TableViewModel
    {
        public int CommissionId { get; set; }

        public Commission Commission { get; set; }

        public string Commissions { get; set; }
    }
}