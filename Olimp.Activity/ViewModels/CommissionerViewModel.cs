﻿using Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Activity.ViewModels
{
    public class CommissionerViewModel
    {
        public Commissioner Commissioner { get; set; }

        public int CommissionId { get; set; }
    }
}