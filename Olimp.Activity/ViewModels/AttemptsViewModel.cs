using Olimp.UI.ViewModels;
using System;

namespace Olimp.Activity.ViewModels
{
    public class AttemptsViewModel : TableViewModel
    {
        public string FullName { get; set; }

        public string Number { get; set; }

        public string Company { get; set; }

        public string Material { get; set; }

        public DateTime? DateBegin { get; set; }

        public DateTime? DateEnd { get; set; }
    }
}