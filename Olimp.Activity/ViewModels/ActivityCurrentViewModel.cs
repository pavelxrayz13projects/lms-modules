using System.Collections.Generic;
using System.Web.Mvc;

namespace Olimp.Activity.ViewModels
{
    public class ActivityCurrentViewModel
    {
        public IEnumerable<SelectListItem> Profiles { get; set; }
    }
}