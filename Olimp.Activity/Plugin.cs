using Olimp.Activity.Controllers;
using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Exam.Archive;
using Olimp.Domain.Exam.Attempts;
using Olimp.Domain.Exam.NHibernate.Archive;
using Olimp.Domain.Exam.NHibernate.Attempts;
using PAPI.Core;
using PAPI.Core.Initialization;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Activity.Plugin))]

namespace Olimp.Activity
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        { }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(ActivityController),
                typeof(ReportController),
                typeof(CommissionController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (this.Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var connFactoryFactory = new PerHttpRequestConnectionFactoryFactory(
                new SqliteConnectionFactoryFactory());

            var connFactory = connFactoryFactory.Create();

            var nodeRepository = new OrmLiteNodeRepository(connFactory);

            var attemptRepository = new NHibernateAttemptRepository(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory,
                nodeRepository);
            var archiveLookup = new NHibernateArchiveLookup(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory);

            binder.Controller<ReportController>(b => b
                 .Bind<IAttemptRepository>(() => attemptRepository)
                 .Bind<IArchiveLookup>(() => archiveLookup));

            binder.Controller<ActivityController>(b => b
                .Bind<IAttemptRepository>(() => attemptRepository));
        }
    }
}