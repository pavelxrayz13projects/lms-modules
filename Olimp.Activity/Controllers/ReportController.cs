using Olimp.Activity.ViewModels;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Common;
using Olimp.Domain.Exam.Archive;
using Olimp.Domain.Exam.Attempts;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Olimp.Activity.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.KnowledgeControl.Courses.Read, Order = 2)]
    public class ReportController : PController
    {
        private IAttemptRepository _attemptRepository;
        private IArchiveLookup _archiveLookup;

        public ReportController(
            IAttemptRepository attemptRepository,
            IArchiveLookup archiveLookup)
        {
            if (attemptRepository == null)
                throw new ArgumentNullException("attemptRepository");
            if (archiveLookup == null)
                throw new ArgumentNullException("archiveLookup");

            _attemptRepository = attemptRepository;
            _archiveLookup = archiveLookup;
        }

        [HttpGet]
        public ActionResult Report()
        {
            using (UnitOfWork.CreateRead())
            {
                return View(new ReportViewModel
                {
                    Date = DateTime.Today,
                    AllowGenerateReports = Roles.IsUserInRole(Permissions.Reports.Generate) || Roles.IsUserInRole(Permissions.Reports.ManageProtocolTemplates),
                    AllowWrite = Roles.IsUserInRole(Permissions.KnowledgeControl.Courses.Write),
                    Commissions = Repository.Of<Commission>().OrderBy(c => c.Name)
                        .Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString() })
                });
            }
        }

        [HttpGet]
        public ActionResult Library() { return View(); }

        [HttpPost]
        public ActionResult GetAttempts(AttemptsViewModel model)
        {
            var filter = new AttemptsFilter
            {
                AttemptTypes = new[] { Domain.Exam.AttemptType.Exam },
                IsFinished = true,
                DateBegin = model.DateBegin,
                DateEnd = model.DateEnd,
                EmployeeCompany = model.Company,
                EmployeeFullName = model.FullName,
                EmployeeNumber = model.Number,
                MaterialFullName = model.Material
            };

            var paging = new GroupingPagingValues
            {
                GroupByColumn = "EmployeeFullNameAndCompany",
                PageNumber = model.CurrentPage,
                PageSize = model.PageSize,
                SortAsc = model.SortAsc,
                SortByColumn = model.SortColumn
            };

            var results = _attemptRepository.Filter(filter, paging);
            var json = new
            {
                rowsCount = results.TotalCount,
                groups = results.Groups.Select(g => new
                {
                    name = g.GroupName,
                    rows = g.Entities.Select(e => new
                    {
                        Id = e.AttemptId,
                        e.MaterialFullName,
                        Status = e.Status.Replace(" ", "&nbsp;"),
                        StartTime = e.StartTime.ToShortDateString(),
                        e.TicketNumber,
                        ExamAreas = e.ExamAreas != null ? string.Join(", ", e.ExamAreas) : ""
                    })
                })
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LookupMaterial(string filter)
        {
            return Json(_archiveLookup.LookupCourses(filter, 20));
        }

        [HttpPost]
        public ActionResult LookupEmployeeName(string filter)
        {
            return Json(_archiveLookup.LookupEmployeesNames(filter, 20));
        }

        [HttpPost]
        public ActionResult LookupEmployeeNumber(string filter)
        {
            return Json(_archiveLookup.LookupEmployeesNumbers(filter, 20));
        }

        [HttpPost]
        public ActionResult LookupCompany(string filter)
        {
            return Json(_archiveLookup.LookupCompanies(filter, 20));
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.KnowledgeControl.Courses.Write, Order = 2)]
        public ActionResult Delete(ICollection<Guid> objects)
        {
            if (objects != null && objects.Any())
                _attemptRepository.Delete(objects);

            return null;
        }
    }
}