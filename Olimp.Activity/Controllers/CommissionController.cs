﻿using Olimp.Activity.ViewModels;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using ServiceStack.Text;
using System;
using System.Linq;
using System.Web.Mvc;
using CR = Olimp.I18N.Domain.Commissioner;

namespace Olimp.Activity.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Registration.Commissions, Order = 2)]
    public class CommissionController : PController
    {
        public ActionResult Index(int? commissionId)
        {
            var repo = Repository.Of<Commission>();

            using (UnitOfWork.CreateRead())
            {
                var commission = repo.GetById(commissionId);
                var commissions = repo.OrderBy(c => c.Name).Select(c => new { c.Name, c.Id });
                if (commission == null)
                {
                    var firstCommission = commissions.FirstOrDefault();
                    if (firstCommission != null)
                        commissionId = firstCommission.Id;
                }

                JsConfig.ExcludeTypeInfo = true;
                return View(new CommissionViewModel
                {
                    CommissionId = commissionId.GetValueOrDefault(),
                    Commissions = JsonSerializer.SerializeToString(commissions),
                    Commission = commission
                });
            }
        }

        [HttpPost]
        public ActionResult GetCommissioners(CommissionViewModel model)
        {
            using (UnitOfWork.CreateRead())
            {
                var commissioners = Repository.Of<Commissioner>()
                    .Where(c => c.Commission.Id == model.CommissionId);

                return Json(commissioners.GetTableData(model, c => new
                {
                    c.Id,
                    c.FullName,
                    c.Position,
                    c.IsChairman,
                    Role = c.Role ?? "",
                    CommissionId = c.Commission.Id
                }));
            }
        }

        [HttpPost]
        public ActionResult EditCommission(int? commissionId)
        {
            if (!ModelState.IsValid)
                return null;

            var repo = Repository.Of<Commission>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                var id = commissionId.GetValueOrDefault();
                var viewModel = new CommissionViewModel
                {
                    Commission = id > 0
                        ? repo.LoadById(id)
                        : new Commission()
                };

                this.UpdateModel(viewModel);

                repo.Save(viewModel.Commission);

                uow.Commit();

                commissionId = viewModel.Commission.Id;
            }

            return RedirectToAction("Index", new { commissionId = commissionId });
        }

        [HttpPost]
        public ActionResult EditCommissioner(int? id, int commissionId)
        {
            if (!ModelState.IsValid)
                return null;

            var commissionerRepo = Repository.Of<Commissioner>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                var model = new CommissionerViewModel
                {
                    Commissioner = commissionerRepo.GetById(id)
                        ?? new Commissioner()
                };

                var isChairman = model.Commissioner.IsChairman;

                this.UpdateModel(model);

                if (model.Commissioner.Id == 0)
                    model.Commissioner.Commission = Repository.Of<Commission>().LoadById(commissionId);

                if (!isChairman && (model.Commissioner.IsChairman || (model.Commissioner.Role ?? "").Trim().Equals(Olimp.I18N.Domain.Commissioner.ChairmanRole, StringComparison.OrdinalIgnoreCase)))
                {
                    model.Commissioner.IsChairman = true;
                    var other = model.Commissioner.Commission.Commissioners
                        .Where(o => !o.Equals(model.Commissioner));

                    foreach (var o in other)
                    {
                        if (o.IsChairman || (o.Role ?? "").Trim().Equals(Olimp.I18N.Domain.Commissioner.ChairmanRole, StringComparison.OrdinalIgnoreCase))
                            o.Role = "";

                        o.IsChairman = false;
                    }

                    model.Commissioner.Role = Olimp.I18N.Domain.Commissioner.ChairmanRole;
                }

                commissionerRepo.Save(model.Commissioner);

                uow.Commit();
            }

            return null;
        }

        [HttpPost]
        public ActionResult DeleteCommissioner(int id)
        {
            var repo = Repository.Of<Commissioner>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                var commissioner = repo.GetById(id);
                commissioner.Commission.Commissioners.Remove(commissioner);
                commissioner.Commission = null;

                repo.Delete(commissioner);

                uow.Commit();
            }

            return null;
        }

        [HttpPost]
        public ActionResult DeleteCommission(int commissionId)
        {
            var commissionRepo = Repository.Of<Commission>();
            var commissionerRepo = Repository.Of<Commissioner>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                var commission = commissionRepo.GetById(commissionId);
                foreach (var c in commission.Commissioners.ToList())
                {
                    commission.Commissioners.Remove(c);
                    c.Commission = null;

                    commissionerRepo.Delete(c);
                }

                commissionRepo.Delete(commission);

                uow.Commit();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult LookupRole(string filter)
        {
            using (UnitOfWork.CreateRead())
            {
                var roles = Repository.Of<Commissioner>()
                    .Where(c => c.Role.Contains(filter))
                    .OrderBy(c => c.Role)
                    .Select(c => c.Role)
                    .Distinct()
                    .AsEnumerable()
                    .Union(new[] { CR.ChairmanRole, CR.ViceChairmanRole, CR.MemberRole })
                    .OrderBy(r => r);

                return Json(roles);
            }
        }
    }
}