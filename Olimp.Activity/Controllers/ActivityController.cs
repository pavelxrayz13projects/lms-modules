using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Common;
using Olimp.Domain.Exam.Attempts;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Activity.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.KnowledgeControl.Courses.Read, Order = 2)]
    public class ActivityController : PController
    {
        private IAttemptRepository _attemptRepository;

        public ActivityController(IAttemptRepository attemptRepository)
        {
            if (attemptRepository == null)
                throw new ArgumentNullException("attemptRepository");

            _attemptRepository = attemptRepository;
        }

        public ActionResult Current()
        {
            return View();
        }

        public ActionResult GetAttempts(TableViewModel model)
        {
            var filter = new AttemptsFilter
            {
                AttemptTypes = new[] { Domain.Exam.AttemptType.Exam },
                DateBegin = DateTime.Today,
                DateEnd = DateTime.Today
            };

            var paging = new GroupingPagingValues
            {
                GroupByColumn = "EmployeeFullNameAndCompany",
                PageNumber = model.CurrentPage,
                PageSize = model.PageSize,
                SortAsc = model.SortAsc,
                SortByColumn = model.SortColumn
            };

            var results = _attemptRepository.Filter(filter, paging);
            var json = new
            {
                rowsCount = results.TotalCount,
                groups = results.Groups.Select(g => new
                {
                    name = g.GroupName,
                    rows = g.Entities.Select(e => new
                    {
                        Id = e.AttemptId,
                        MaterialFullName = e.MaterialFullName,
                        Status = e.Status.Replace(" ", "&nbsp;"),
                        StartTime = e.StartTime.ToShortDateString(),
                        TicketNumber = e.TicketNumber
                    })
                })
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }
    }
}