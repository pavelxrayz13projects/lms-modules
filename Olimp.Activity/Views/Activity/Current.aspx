﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<object>"  MasterPageFile="~/Views/Shared/Monitoring.master" %>
<%@ Import Namespace="I18N=Olimp.Activity.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Monitoring_Content" runat="server">
	<h1>Информация по текущей активности</h1>
	
	<% Html.RenderPartial("AttemptPassedTemplate"); %>
	<% Html.RenderPartial("AttemptPassedTemplate_doT"); %>		
		 
	<% Html.Table(
		new TableOptions {
			SelectUrl = Url.Action("GetAttempts"),
			ReloadInterval = 20000,
			BodyTemplateEngine = TemplateEngines.DoT,
			PagingDefaultSize = 50,
			PagingAllowAll = true,
			GroupBy = "EmployeeFullNameAndCompany",
            RowsHighlighting = true,
			Columns = new[] { 
				new ColumnOptions("MaterialFullName", I18N.Current.Course),
				new ColumnOptions("TicketNumber", I18N.Current.TicketNumber),
				new ColumnOptions("Status", I18N.Current.Passed),
			},
            Actions = new RowAction[] { 
                new TemplateAction("attempt-results-action-doT"),
                new TemplateAction("attempt-print-action-doT") 
            }
		}, new { id = "search-results" }); %> 
		
	<% Html.RenderPartial("AttemptResultsActionTemplate_doT"); %>	
	
    <script type="text/javascript">
	window.Archive.reloadDialog = 20000;
	$(function() { $('#search-form .form-table select').combobox(); })
	</script>	

</asp:Content>