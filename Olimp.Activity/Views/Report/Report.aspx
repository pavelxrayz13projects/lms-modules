﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<ReportViewModel>"  MasterPageFile="~/Views/Shared/Monitoring.master" %>
<%@ Import Namespace="I18N=Olimp.Activity.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Monitoring_Content" runat="server">

    <% Html.EnableClientValidation(); %>

    <% Html.RenderPartial("AttemptPassedTemplate"); %>	
    <% Html.RenderPartial("AttemptPassedTemplate_doT"); %>	
    <% Html.RenderPartial("AttemptResultsActionTemplate_doT"); %>

    <% using (Html.BeginForm("Generate", "ReportGenerator", FormMethod.Post, new { id = "protocol-form" })) { %>
        <% if (Model.AllowGenerateReports) { %>
            <div id="report-block">
                <h1>Формирование протокола (<a href="#" class="link-button" data-bind="text: toggleProtocolLinkText, click: toggleProtocolCollapse"></a>)</h1>
                <div class="block">
                    <table class="form-table" data-bind="visible: protocolExpanded">	
                        <tr class="first">	
                            <th><%= I18N.Report.ProtocolDate %></th> 
                            <td>
                                <div>
                                    <%= Html.TextBoxFor(m => m.Date, new Dictionary<string, object> { 
                                        { "id", "protocolDate" },
                                        { "data-bind", "value: protocolDate" }
                                    }) %>
                                </div>
                                <div></div>
                            </td>
                        </tr>
                        <tr>	
                            <th><%= I18N.Report.ProtocolNumber %></th> 
                            <td>
                                <div><%= Html.TextBoxFor(m => m.Number) %></div> 
                                <div><%= Html.ValidationMessageFor(m => m.Number) %></div> 
                            </td>
                        </tr>
                        <tr>	
                            <th>Организация</th> 
                            <td><%= Html.TextBoxFor(m => m.Company, new { id = "protocol-company" }) %></td>
                        </tr>
                        <tr>	
                            <th>Комиссия</th> 
                            <td>
                                <%= Html.DropDownListFor(m => m.CommissionId, Model.Commissions, "Выберите комиссию...", new { id = "protocol-commission" }) %>
                            </td>
                        </tr>
                        <tr>	
                            <th><%= I18N.Report.ProtocolTemplate %></th> 
                            <td>
                                <% Html.RenderAction("InPlace", "ProtocolManager"); %>
                                <%--<%= Html.DropDownListFor(m => m.ReportId, Model.Protocols, new { id = "protocol-template" }) %>--%>
                            </td>
                        </tr>
                        <tr class="last">
                            <td colspan="2">
                                <button type="submit">Сформировать</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        <% } %>	
        <h1>Результаты экзаменов</h1>
        <div class="block">
            <div id="search-block">
                <table class="form-table">
                    <tr class="first">
                        <th>Поиск</th>
                        <td>
                            <select id="search-options" data-bind="value: searchBy, options: searchByOptions"></select></td>
                    </tr>
                    <tr>
                        <th>ФИО</th>
                        <td>
                            <input id="employee-name" type="text" data-bind="value: searchData.FullName" /></td>
                    </tr>
                    <tr>
                        <th>Табельный номер</th>
                        <td>
                            <input id="employee-number" type="text" data-bind="value: searchData.Number" /></td>
                    </tr>
                    <tr>
                        <th>Организация</th>
                        <td>
                            <input id="company-name" type="text" data-bind="value: searchData.Company" /></td>
                    </tr>
                    <tr data-bind="visible: selectCourseVisible">
                        <th>Курс</th>
                        <td>
                            <input id="material-name" type="text" data-bind="value: searchData.Material" /></td>
                    </tr>
                    <tr id="period">
                        <th>Период</th>
                        <td class="date-span">
                            <span>начало</span>
                            <input type="text" data-bind="value: searchData.DateBegin" />
                            <span>окончание</span>
                            <input type="text" data-bind="value: searchData.DateEnd" />
                        </td>
                    </tr>
                    <tr class="last">
                        <td colspan="2">
                            <button id="search" type="button" data-bind="click: applyFilter"><%= I18N.Report.Search %></button>
                            <button type="button" data-bind="click: clearFilter"><%= I18N.Report.Clear %></button>
                        </td>
                    </tr>
                </table>
            </div>
            <% Html.Table(
                new TableOptions {					
                    SelectUrl = Url.Action("GetAttempts"),
                    BodyTemplateEngine = TemplateEngines.DoT,
                    GroupBy = "EmployeeFullNameAndCompany",
                    PagingAllowAll = true,
                    PagingDefaultSize = 50,
                    Checkboxes = true,
                    RowsHighlighting = true,					
                    CheckboxesName = "Attempts",
                    Columns = new[] { 
                        new ColumnOptions("StartTime", I18N.Report.DateStart),
                        new ColumnOptions("MaterialFullName", I18N.Current.Course),
                        new ColumnOptions("TicketNumber", I18N.Current.TicketNumber),
                        new ColumnOptions("ExamAreas", I18N.Current.ExamAreas),
                        new ColumnOptions("Status", I18N.Current.Passed)
                    },
                    Actions = new RowAction[] { 
                        new TemplateAction("attempt-results-action-doT"),
                        new TemplateAction("attempt-print-action-doT") 
                    },
                    TableActions = new TableAction[] { 
                    new MassDeleteAction(Url.Action("Delete"), I18N.Report.ConfirmDelete, Olimp.I18N.PlatformShared.Delete) { Disabled = !Model.AllowWrite }	}
                }, new { id = "search-results" }); %>
        </div>
    <% } %>

    <script type="text/javascript">
    $(function () { 
        $("#search-block").collapsibleBlock({ headerText: "<%= I18N.Report.ResultsSearch %>" });
        $('#search-block button, #report-block button').olimpbutton();
        $('#period input, #protocolDate').datepicker({ changeMonth: true, changeYear: true });
        
        var searchResultsModel = $('#search-results').data('viewModel');

        var protocolModel = { 			
            protocolDate: '<%= Model.Date != null ? Model.Date.Value.ToShortDateString() : "" %>',
            protocolExpanded: ko.observable(false),
            toggleProtocolCollapse: function() {
                this.protocolExpanded(!this.protocolExpanded());
                return false;
            }
        };
        protocolModel.toggleProtocolLinkText = ko.computed(function () {
            return this.protocolExpanded() ? 'скрыть' : 'показать';
        }, protocolModel);
        
        var searchModel = {
            searchByOptions: ['по результатам экзаменов', 'по пользователям'],
            searchBy: ko.observable(),
            searchData: {
                FullName: ko.observable(),
                Number: ko.observable(),
                Company: ko.observable(),
                Material: ko.observable(),
                DateBegin: ko.observable(),
                DateEnd: ko.observable(),
                __callback: function () {
                    if (!this.groups().length) {
                        $.Olimp.showWarning('По заданным параметрам не найдено ни одного результата ');
                    }
                }
            },
            applyFilter: function () {
                var data = ko.mapping.toJS(this.searchData);
                searchResultsModel.requestData(data);
            },
            clearFilter: function () {
                this.searchData.FullName("");
                this.searchData.Number("");
                this.searchData.Company("");
                this.searchData.Material("");
                this.searchData.DateBegin("");
                this.searchData.DateEnd("");
                this.searchBy(this.searchByOptions[0]);
                searchResultsModel.requestData({});
            }
        };
        searchModel.selectCourseVisible = ko.computed(function () {
            return this.searchBy() == this.searchByOptions[0];
        }, searchModel);
                        
        ko.applyBindings(searchModel, $('#search-block')[0]);
        if ($('#report-block').length > 0)
            ko.applyBindings(protocolModel, $('#report-block')[0]);
        
        $('#search-options').combobox();
        $('#protocol-commission').combobox();
        $('#protocol-company').combobox({
            source: function (request, response) {
                $.post('<%= Url.Action("LookupCompany", "Employee") %>', { filter: request.term }, response);
            }
        });
        $('#company-name').combobox({
            source: function (request, response) {
                $.post('<%= Url.Action("LookupCompany") %>', { filter: request.term }, response);
            }
        });
        $('#material-name').combobox({
            source: function (request, response) {
                $.post('<%= Url.Action("LookupMaterial") %>', { filter: request.term }, response);
            }
        });
        $('#employee-name').combobox({
            minLength: 2,
            dropDown: false,
            source: function (request, response) {
                $.post('<%= Url.Action("LookupEmployeeName") %>', { filter: request.term }, response);
            }
        });
        $('#employee-number').combobox({
            minLength: 2,
            dropDown: false,
            source: function (request, response) {
                $.post('<%= Url.Action("LookupEmployeeNumber") %>', { filter: request.term }, response);
            }
        });
    });
    </script>
</asp:Content>