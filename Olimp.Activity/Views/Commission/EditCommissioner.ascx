﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<CommissionerViewModel>" %>

<% using(Html.BeginForm("EditCommissioner", "Commission")) { %>
	<%= Html.Hidden("id", 0, new { data_bind = "value: Id" })%>

    <%= Html.HiddenFor(m => m.CommissionId, new { data_bind = "value: CommissionId" })%>

	<table class="form-table">
		<tr class="first">
			<th><%= Html.LabelFor(m => m.Commissioner.FullName)%></th>
			<td>
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Commissioner.FullName) %></div>
				<%= Html.TextBoxFor(m => m.Commissioner.FullName, new Dictionary<string, object> {
					{ "data-bind", "value: FullName" }
				})%>
			</td>
		</tr>
        <tr>
			<th><%= Html.LabelFor(m => m.Commissioner.Position)%></th>
			<td>
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Commissioner.Position) %></div>
				<%= Html.TextBoxFor(m => m.Commissioner.Position, new { data_bind = "value: Position" })%>
			</td>
		</tr>
        <tr>
            <th>Председатель</th>
            <td class="checkbox">
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Commissioner.IsChairman) %></div>
                <%=  Html.CheckBoxFor(m => m.Commissioner.IsChairman, new { id = "is-chairman", data_bind = "checked: IsChairman" })%>
            </td>
        </tr>
        <tr class="last">
			<th><%= Html.LabelFor(m => m.Commissioner.Role)%></th>
			<td>
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Commissioner.Role) %></div>
				<%= Html.TextBoxFor(m => m.Commissioner.Role, new { id = "commissioner-role", data_bind = "value: Role" })%>
			</td>
		</tr>		
	</table>
<% } %>


	