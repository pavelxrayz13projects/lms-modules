﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<CommissionViewModel>" %>

<% using(Html.BeginForm("EditCommission", "Commission")) { %>
	<%= Html.HiddenFor(m => m.CommissionId, new Dictionary<string, object> {
		{ "data-bind", "value: currentEditFormCommission().Id" }
	})%>

	<table class="form-table">
		<tr class="first last">
			<th><%= Html.LabelFor(m => m.Commission.Name)%></th>
			<td>
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Commission.Name) %></div>
				<%= Html.TextBoxFor(m => m.Commission.Name, new Dictionary<string, object> {
					{ "data-bind", "value: currentEditFormCommission().Name" }
				})%>
			</td>
		</tr>
	</table>
<% } %>


	