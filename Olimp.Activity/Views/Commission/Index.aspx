﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<CommissionViewModel>"  MasterPageFile="~/Views/Shared/Register.master" %>
<%@ Import Namespace="I18N=Olimp.Activity.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Register_Js" runat="server">
    <% Html.EnableClientValidation(); %>

	<script type="text/javascript">	
	    var defaultCommission = { Id: 0, Name: '' },
	        commissionsMapping = {
	            create: function (o) { return o.data; },
	            key: function (o) { return ko.utils.unwrapObservable(o.Id) }
	        },
            viewModel = window.viewModel = {	        
                commissions: ko.mapping.fromJS(<%= Model.Commissions %>, commissionsMapping),                
                currentCommission: ko.observable(),
	            currentEditFormCommission: ko.observable(defaultCommission),
                deleteCommission: function () { 
                    if (this.currentCommission() && confirm('Текущая комиссия и все ее члены будут удалены. Продолжить?'))
                        $('#delete-commission-form').submit() 
                },
	            addCommission: function () {
	                this.currentEditFormCommission(defaultCommission);

	                $('#commission-edit-dialog').olimpsyncformdialog('open');
	            },
	            renameCommission: function () {
	                if (!this.currentCommission())
	                    return;

	                this.currentEditFormCommission(this.currentCommission());

	                $('#commission-edit-dialog').olimpsyncformdialog('open');
	            }	        
            },
	        currentCommissionIndex = viewModel.commissions.mappedIndexOf({ Id: <%= Model.CommissionId %> });
    
	    viewModel.currentCommission(viewModel.commissions()[currentCommissionIndex]);
	    viewModel.currentCommissionId = ko.computed(function() {
	        var current = this.currentCommission();
	        return current ? current.Id : 0;
	    }, viewModel);

	    viewModel.renameCommissionClass = ko.computed(function() {
	        return this.currentCommission() ? 'icon icon-pencil-active' : 'icon icon-pencil-inactive';
	    }, viewModel)	    

	    viewModel.deleteCommissionClass = ko.computed(function() {
	        return this.currentCommission() ? 'icon icon-delete-active' : 'icon icon-delete-inactive';
	    }, viewModel)           
	            	    
	    $(function () {
	        ko.applyBindings(viewModel, $('#commission-block')[0]);
	        viewModel.currentCommissionId.subscribe(function() {
	            $('#switch-commission-form').submit()
	        })

	        $('#commission-edit-dialog').olimpsyncformdialog({ title: 'Комиссия' })
                
	        $('#edit-dialog').bind('afterApply', function() {
	            var self = $(this),
	                textBox = self.find('#commissioner-role');

                self.find('#commissioner-role').combobox({ 
	                source: function (request, response) {
	                    $.post('<%= Url.Action("LookupRole") %>', { filter: request.term }, response)
	                } 
                });

	            var isChairman = self.find('#is-chairman').click(function() {
	                var roleName = '<%= Olimp.I18N.Domain.Commissioner.ChairmanRole %>';

	                if ($(this).prop('checked')) {
	                    textBox.val(roleName).combobox('option', 'disabled', true);
	                } else {
	                    textBox.val('').combobox('option', 'disabled', false);
	                }
	            }).prop('checked');

	            textBox.combobox('option', 'disabled', isChairman);
	        }).bind('afterSubmit', function (ev, data) {
                $('#commissioners-table').data('viewModel').reload()
	        });

	        $('#current-commission').combobox();
	    });
	</script>	
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Register_Content" runat="server">
	<h1>Комиссии</h1>

    <div id="commission-block">
        <% using (Html.BeginForm("DeleteCommission", "Commission", FormMethod.Post, new { id="delete-commission-form" })) { %>
            <%= Html.HiddenFor(m => m.CommissionId, new Dictionary<string, object> { { "data-bind", "value: currentCommissionId" } }) %>
        <% } %>

        <% using (Html.BeginForm("Index", "Commission", FormMethod.Get, new { id="switch-commission-form", @class="block" })) { %>
            <%= Html.HiddenFor(m => m.CommissionId, new Dictionary<string, object> { { "data-bind", "attr: { 'value': currentCommissionId }" } }) %>

		    <table class="form-table" style="margin-bottom: 6px;">
			    <tr class="first last">		
				    <th><label>Выберите комиссию:</label></th>
				    <td>
                        <table style="border-collapse: collapse; width: 100%;">
                            <tr>
                                <td>
                                    <select id="current-commission" data-bind="
                                        options: commissions, 
                                        optionsText: 'Name',                                         
                                        value: currentCommission">
                                    </select>
                                </td>
                                <td style="width: 1px;">
                                    <a data-bind="click: addCommission"
                                        href="#" 
                                        class="action icon icon-add" 
                                        style="margin-left:10px;" 
                                        title="Добавить новую комиссию">
                                    </a>
                                </td>
                                <td style="width: 1px;">
                                    <a data-bind="click: renameCommission, attr: { 'class': renameCommissionClass }"
                                        href="#" 
                                        style="margin-left:10px;" 
                                        title="Переименовать комиссию">
                                    </a>
                                </td>
                                <td style="width: 1px;">
                                    <a data-bind="click: deleteCommission, attr: { 'class': deleteCommissionClass }"
                                        href="#" 
                                        style="margin-left:10px;" 
                                        title="Удалить комиссию">
                                    </a>
                                </td>
                            </tr>
                        </table>					
				    </td>
			    </tr>						
		    </table>		
	    <% } %>

        <div id ="commission-edit-dialog" style="display:none;">
        <% Html.RenderPartial("EditCommission"); %>
        </div>  
    </div>          
			
    <div id="commissioners-block">
        <h1>Члены комиссии</h1>
        <div class="block">	
	        <% Html.Table(
		        new TableOptions {
			        SelectUrl = Url.Action("GetCommissioners", new { CommissionId = Model.CommissionId }),                                
			        Paging = false,
			        Adding = new AddingOptions { 
				        Text = "Добавить",
				        New = new { Id = 0, FullName = "", Position = "", IsChairman = false, Role = "", CommissionId = Model.CommissionId }
			        },
			        Columns = new[] { 
				        new ColumnOptions("FullName", Olimp.I18N.Domain.Commissioner.FullName),
				        new ColumnOptions("Position", Olimp.I18N.Domain.Commissioner.Position),				
                        new ColumnOptions("Role", Olimp.I18N.Domain.Commissioner.Role)
			        },
			        Actions = new RowAction[] { 
				        new EditAction("edit-dialog", new DialogOptions { Title = "Член комиссии" }),
				        new DeleteAction(Url.Action("DeleteCommissioner"), "Член комиссии будет удален. Продолжить?")	}
            }, new { id = "commissioners-table" }); %>
        </div> 
    </div>
    
    <div id="edit-dialog">	
	<% Html.RenderPartial("EditCommissioner", new CommissionerViewModel { 
        CommissionId = Model.CommissionId 
    }); %>
    </div>  

    <script>
        $(function(){
            ko.computed(function() {
                $('#commissioners-block').css('display', this.currentCommission() ? 'block' : 'none');
            }, window.viewModel);
        })
    </script>
</asp:Content>