﻿using Olimp.Core;
using System;
using System.Reflection;
using System.Web.Mvc;

namespace Olimp.Web.Controllers
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class InternalFeatureAttribute : ActionMethodSelectorAttribute
    {
        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        {
            return OlimpApplication.InternalFeaturesMode;
        }
    }
}