﻿using Olimp.Configuration;
using Olimp.Domain.LearningCenter;
using PAPI.Core;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

using RegType = Olimp.Domain.LearningCenter.NotPersisted.RegistrationType;

namespace Olimp.Web.Controllers.Registration
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class RegistrationTypeAttribute : ActionMethodSelectorAttribute
    {
        private RegType[] _types;

        public RegistrationTypeAttribute(params RegType[] registrationTypes)
        {
            _types = registrationTypes;
        }

        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        {
            IUnitOfWork uow = null;
            IUnitOfWorkFactory _unitOfWork = PContext.ExecutingModule.Database.UnitOfWorkFactory;

            if (_unitOfWork != null)
                uow = _unitOfWork.CreateRead();
            try
            {
                var config = Config.Get<OlimpConfiguration>();
                return _types.Contains(config.RegistrationType);
            }
            finally
            {
                if (uow != null)
                    uow.Dispose();
            }
        }
    }
}