﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Olimp.Web.Controllers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public abstract class AllowAttribute : ActionFilterAttribute
    {
        public string RedirectTo { get; set; }

        public abstract bool IsAllowed(ActionExecutingContext filterContext);

        public string Message { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!this.IsAllowed(filterContext))
            {
                if (String.IsNullOrWhiteSpace(this.RedirectTo))
                {
                    throw new HttpException(403, this.Message);
                }
                else
                {
                    filterContext.Result = new RedirectResult(this.RedirectTo);
                }
            }
        }
    }
}