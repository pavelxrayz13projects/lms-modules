﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Olimp.Web.Controllers.ComponentModel
{
    public class IsDateTimeAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            DateTime dateTime;
            var str = Convert.ToString(value);

            return DateTime.TryParse(str, out dateTime);
        }
    }
}