﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.ComponentModel
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RequiredIfBoolAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly string _otherProperty;

        public RequiredIfBoolAttribute(string otherProperty)
        {
            _otherProperty = otherProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var property = validationContext.ObjectType.GetProperty(this._otherProperty);
            var errorMessage = this.FormatErrorMessage(validationContext.DisplayName);

            if (property == null || property.PropertyType != typeof(bool))
            {
                throw new ArgumentException("Compared property is not bool.");
            }

            var boolProperty = (bool)property.GetValue(validationContext.ObjectInstance, null);
            if (boolProperty)
                return null;
            if (value == null)
                return new ValidationResult(errorMessage);
            var str = value as string;
            if (str != null && str.Trim().Length == 0)
                return new ValidationResult(errorMessage);

            return null;
        }

        #region IClientValidatable implementation

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = this.FormatErrorMessage(metadata.DisplayName),
                ValidationType = "requiredifbool"
            };
            rule.ValidationParameters.Add("boolproperty", this._otherProperty);
            yield return rule;
        }

        #endregion IClientValidatable implementation
    }
}