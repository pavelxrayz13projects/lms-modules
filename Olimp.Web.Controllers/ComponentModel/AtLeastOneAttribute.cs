﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.ComponentModel
{
    [AttributeUsage(AttributeTargets.Property)]
    public class AtLeastOneAttribute : ValidationAttribute, IClientValidatable
    {
        public override bool IsValid(object value)
        {
            var collection = value as IEnumerable;
            if (collection == null)
                return false;

            return collection.GetEnumerator().MoveNext();
        }

        #region IClientValidatable implementation

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = this.ErrorMessage,
                ValidationType = "atleastone"
            };
        }

        #endregion IClientValidatable implementation
    }
}