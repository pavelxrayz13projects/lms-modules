﻿namespace Olimp.Web.Controllers.Nodes
{
    public interface IOlimpLinkProvider
    {
        string Get(string hostName);
    }
}