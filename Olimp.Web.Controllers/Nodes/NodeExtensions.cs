﻿using Olimp.Domain.Common.Nodes;
using System;
using System.Web;

namespace Olimp.Web.Controllers.Nodes
{
    public static class NodeExtensions
    {
        private static string GetNodeUrl(Uri requestUri, Node node)
        {
            if (requestUri == null)
                throw new ArgumentNullException("requestUri");

            if (node == null)
                throw new ArgumentNullException("node");

            return node.GetUrl(requestUri.Host, false);
        }

        public static string GetNodeUrl(this HttpRequest request, Node node)
        {
            return GetNodeUrl(request.Url, node);
        }

        public static string GetNodeUrl(this HttpRequestBase request, Node node)
        {
            return GetNodeUrl(request.Url, node);
        }
    }
}