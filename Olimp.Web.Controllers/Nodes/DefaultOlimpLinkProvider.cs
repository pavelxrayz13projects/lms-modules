﻿using Olimp.Domain.Common.Nodes;
using PAPI.Core;
using System;

namespace Olimp.Web.Controllers.Nodes
{
    public class DefaultOlimpLinkProvider : IOlimpLinkProvider
    {
        private readonly NodeData _currentNode;
        private readonly INodesRepository _nodesRepository;

        public DefaultOlimpLinkProvider(NodeData currentNode, INodesRepository nodesRepository)
        {
            if (currentNode == null)
                throw new ArgumentNullException("currentNode");
            if (nodesRepository == null)
                throw new ArgumentNullException("nodesRepository");

            _currentNode = currentNode;
            _nodesRepository = nodesRepository;
        }

        public string Get(string hostName)
        {
            if (String.IsNullOrWhiteSpace(hostName))
                throw new ArgumentException("'hostName' is null or empty string");

            var node = _nodesRepository.GetById(_currentNode.Id);
            return node == null
                ? hostName
                : node.GetUrl(hostName, false);
        }
    }
}