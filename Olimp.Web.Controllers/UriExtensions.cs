﻿using System;

namespace Olimp.Web.Controllers
{
    public static class UriExtensions
    {
        public static string GetRequestUrlBase(this Uri requestUrl)
        {
            var builder = new UriBuilder { Scheme = requestUrl.Scheme, Host = requestUrl.Host, Port = requestUrl.Port };
            return builder.Uri.ToString();
        }
    }
}