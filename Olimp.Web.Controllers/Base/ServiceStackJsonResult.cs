﻿using ServiceStack.Text;
using System;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Base
{
    //TODO: fix content-type
    public class ServiceStackJsonResult : JsonResult
    {
        public ServiceStackJsonResult()
            : base()
        { }

        #region implemented abstract members of System.Web.Mvc.ActionResult

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            var httpMethod = context.HttpContext.Request.HttpMethod;
            if (this.JsonRequestBehavior == JsonRequestBehavior.DenyGet
                && String.Equals(httpMethod, "GET", StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("Use JsonRequestBehavior.AllowGet to allow Json by Get requests");
            }

            var response = context.HttpContext.Response;

            response.ContentType = String.IsNullOrEmpty(this.ContentType)
                ? "application/json"
                : this.ContentType;

            if (this.ContentEncoding != null)
                response.ContentEncoding = this.ContentEncoding;

            if (this.Data != null)
            {
                JsConfig.ExcludeTypeInfo = true;
                JsonSerializer.SerializeToWriter(this.Data, response.Output);
            }
        }

        #endregion implemented abstract members of System.Web.Mvc.ActionResult
    }
}