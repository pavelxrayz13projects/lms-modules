﻿using System.Web.Mvc;

namespace Olimp.Web.Controllers.Base
{
    internal class SimpleViewDataContainer : IViewDataContainer
    {
        public SimpleViewDataContainer()
        {
            this.ViewData = new ViewDataDictionary();
        }

        #region IViewDataContainer Members

        public ViewDataDictionary ViewData { get; set; }

        #endregion IViewDataContainer Members
    }
}