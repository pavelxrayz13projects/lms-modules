﻿using System;
using System.Web.Mvc;
using Olimp.Core.Mvc.Security;
using Olimp.Exceptions;

namespace Olimp.Web.Controllers.Base
{
    public abstract class HandleGenericExceptionAttributeBase : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;

            while (exception != null)
            {
                if (exception.GetType() == GetTypeOfException())
                    break;

                exception = exception.InnerException;
            }

            if (exception != null)
            {
                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;

                var isAjaxRequested = filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
                if (isAjaxRequested)
                    filterContext.Result = new StatusCodeResultWithText(500, exception.Message);
                else
                    throw new OlimpHttpException(4, exception.Message);
            }
            else
            {
                base.OnException(filterContext);
            }
        }

        protected abstract Type GetTypeOfException();
    }
}