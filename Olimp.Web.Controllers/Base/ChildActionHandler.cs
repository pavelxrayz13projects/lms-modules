﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Olimp.Web.Controllers.Base
{
    internal class ChildActionHandler : MvcHandler
    {
        public ChildActionHandler(RequestContext context)
            : base(context)
        { }

        protected override void AddVersionHeader(HttpContextBase httpContext)
        { }
    }
}