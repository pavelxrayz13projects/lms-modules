﻿using Olimp.Configuration;
using Olimp.Domain.LearningCenter;
using PAPI.Core;
using PAPI.Core.Data;
using ServiceStack.Text;
using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace Olimp.Web.Controllers.Base
{
    public abstract class PController : Controller
    {
        public PController(IUnitOfWorkFactory unitOfWorkFactory)
            : base()
        {
            this.UnitOfWork = unitOfWorkFactory;
        }

        public PController()
            : this(PContext.ExecutingModule.Database.UnitOfWorkFactory)
        { }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new ServiceStackJsonResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }

        protected virtual string JsCallback(string functionName)
        {
            return this.JsCallback(functionName, false);
        }

        protected virtual string JsCallback(string functionName, bool runInIFrame)
        {
            if (runInIFrame && !functionName.StartsWith("window.parent."))
                functionName = "window.parent." + functionName;

            return String.Format(@"<script type=""text/javascript"">{0}()</script>", functionName);
        }

        protected FileResult File(Action<Stream> writeStream, string contentType, string downloadName)
        {
            return new LambdaFileResult(writeStream, contentType)
            {
                FileDownloadName = downloadName
            };
        }

        protected JavaScriptResult JsonpContent(string jsonpCallback, string content)
        {
            var value = HttpUtility.JavaScriptStringEncode(content, true);

            return new JavaScriptResult
            {
                Script = String.Format("{0}({1});", jsonpCallback, value)
            };
        }

        protected JavaScriptResult Jsonp(string jsonpCallback, object json)
        {
            JsConfig.IncludeTypeInfo = false;
            var jsonString = JsonSerializer.SerializeToString(json);

            return new JavaScriptResult
            {
                Script = String.Format("{0}({1});", jsonpCallback, jsonString)
            };
        }

        protected JavaScriptResult JsonpAction(string jsonpCallback, string actionName)
        {
            return JsonpAction(jsonpCallback, html => html.Action(actionName));
        }

        protected JavaScriptResult JsonpAction(string jsonpCallback, string actionName, object routeValues)
        {
            return JsonpAction(jsonpCallback, html => html.Action(actionName, routeValues));
        }

        protected JavaScriptResult JsonpAction(string jsonpCallback, string actionName, RouteValueDictionary routeValues)
        {
            return JsonpAction(jsonpCallback, html => html.Action(actionName, routeValues));
        }

        protected JavaScriptResult JsonpAction(string jsonpCallback, string actionName, string controllerName)
        {
            return JsonpAction(jsonpCallback, html => html.Action(actionName, controllerName));
        }

        protected JavaScriptResult JsonpAction(string jsonpCallback, string actionName, string controllerName, object routeValues)
        {
            return JsonpAction(jsonpCallback, html => html.Action(actionName, controllerName, routeValues));
        }

        protected JavaScriptResult JsonpAction(string jsonpCallback, string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            return JsonpAction(jsonpCallback, html => html.Action(actionName, controllerName, routeValues));
        }

        protected JavaScriptResult JsonpPartialView(string jsonpCallback, string partialViewName = null, object model = null)
        {
            if (String.IsNullOrWhiteSpace(partialViewName))
                partialViewName = this.ControllerContext.RouteData.GetRequiredString("action");

            return JsonpView(jsonpCallback, model, engines => engines.FindPartialView(this.ControllerContext, partialViewName));
        }

        protected JavaScriptResult JsonpPartialView(string jsonpCallback, object model = null)
        {
            return JsonpPartialView(jsonpCallback, null, model);
        }

        protected JavaScriptResult JsonpView(string jsonpCallback, string viewName, string masterName, object model = null)
        {
            if (String.IsNullOrWhiteSpace(viewName))
                viewName = this.ControllerContext.RouteData.GetRequiredString("action");

            return JsonpView(jsonpCallback, model, engines => engines.FindView(this.ControllerContext, viewName, masterName ?? String.Empty));
        }

        protected JavaScriptResult JsonpView(string jsonpCallback, string viewName, object model = null)
        {
            return JsonpView(jsonpCallback, viewName, null, model);
        }

        protected JavaScriptResult JsonpView(string jsonpCallback, object model = null)
        {
            return JsonpView(jsonpCallback, null, null, model);
        }

        private JavaScriptResult JsonpView(string jsonpCallback, object model, Func<ViewEngineCollection, ViewEngineResult> viewResultProvider)
        {
            var viewData = new ViewDataDictionary(this.ViewData) { Model = model };

            using (var sw = new StringWriter())
            {
                var viewResult = viewResultProvider.Invoke(ViewEngines.Engines);
                var viewContext = new ViewContext(this.ControllerContext, viewResult.View, viewData, this.TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(this.ControllerContext, viewResult.View);

                var content = sw.GetStringBuilder().ToString();

                return JsonpContent(jsonpCallback, content);
            }
        }

        private JavaScriptResult JsonpAction(string jsonpCallback, Func<HtmlHelper, MvcHtmlString> actionExecutor)
        {
            var htmlHelper = new HtmlHelper(new ViewContext(), new SimpleViewDataContainer());
            var result = actionExecutor.Invoke(htmlHelper);

            return JsonpContent(jsonpCallback, result.ToString());
        }

        public IUnitOfWorkFactory UnitOfWork { get; private set; }

        protected virtual void SetCulture(string name)
        {
            Thread.CurrentThread.CurrentCulture =
                Thread.CurrentThread.CurrentUICulture =
                    new CultureInfo(name);
        }

        protected virtual string ReadLocaleFromConfig()
        {
            return Config.Get<OlimpConfiguration>().UILocale;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            var locale = ReadLocaleFromConfig();
            SetCulture(locale);
        }
    }
}