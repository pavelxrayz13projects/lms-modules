﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Base
{
    public class LambdaFileResult : FileResult
    {
        private Action<Stream> _writer;

        public LambdaFileResult(Action<Stream> writer, string contentType)
            : base(contentType)
        {
            if (writer == null)
                throw new ArgumentNullException("writer");

            _writer = writer;
        }

        protected override void WriteFile(HttpResponseBase response)
        {
            _writer.Invoke(response.OutputStream);
        }
    }
}