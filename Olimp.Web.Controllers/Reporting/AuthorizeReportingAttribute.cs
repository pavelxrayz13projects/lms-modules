﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Olimp.Web.Controllers.Reporting
{
    public class AuthorizeReportingAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var allowManagePermission = ((ReportManagerControllerBase)filterContext.Controller).AllowManagePermission;
            if (!this.Roles.Contains(allowManagePermission))
                this.Roles += (String.IsNullOrWhiteSpace(this.Roles) ? "" : ",") + allowManagePermission;
            base.OnAuthorization(filterContext);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var currentUrl = HttpUtility.UrlEncode(
                filterContext.RequestContext.HttpContext.Request.RawUrl);

            var routeDictionary = new RouteValueDictionary()
			{
				{ "ReturnUrl", currentUrl }
			};

            filterContext.Result = new RedirectToRouteResult("AuthAdmin", routeDictionary);
        }
    }
}