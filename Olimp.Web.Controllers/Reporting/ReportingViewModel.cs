﻿using Olimp.Reporting;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Reporting
{
    public class ReportingViewModel
    {
        public ReportType Type { get; set; }

        public int ReportId { get; set; }

        public IEnumerable<SelectListItem> ReportTemplates { get; set; }
    }
}