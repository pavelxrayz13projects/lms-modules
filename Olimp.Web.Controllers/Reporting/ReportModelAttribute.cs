﻿using System;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Reporting
{
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
    public class ReportModelAttribute : CustomModelBinderAttribute
    {
        public string GuidParam { get; set; }

        #region implemented abstract members of System.Web.Mvc.CustomModelBinderAttribute

        public override IModelBinder GetBinder()
        {
            return new ReportModelBinder(this.GuidParam);
        }

        #endregion implemented abstract members of System.Web.Mvc.CustomModelBinderAttribute
    }
}