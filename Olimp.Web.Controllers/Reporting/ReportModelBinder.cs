﻿using System;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Reporting
{
    public class ReportModelBinder : IModelBinder
    {
        private string _guidParam;
        private IModelBinder _binder;

        public ReportModelBinder(string guidParam)
        {
            _binder = new DefaultModelBinder();
            _guidParam = guidParam;
        }

        #region IModelBinder implementation

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var controller = controllerContext.Controller as ReportGeneratorController;
            if (controller == null)
                throw new InvalidOperationException("Can be used only with ReportGeneratorController");

            var result = controllerContext.Controller.ValueProvider.GetValue(_guidParam);
            if (result == null || String.IsNullOrWhiteSpace(result.AttemptedValue))
                return null;

            var guid = new Guid(result.AttemptedValue);
            var provider = controller.GetReportProvider(guid);
            if (provider == null)
                return null;

            bindingContext = new ModelBindingContext(bindingContext);
            bindingContext.ModelMetadata = ModelMetadataProviders.Current
                .GetMetadataForType(null, provider.ModelType);

            return _binder.BindModel(controllerContext, bindingContext);
        }

        #endregion IModelBinder implementation
    }
}