using Olimp.Core.ComponentModel;
using Olimp.Reporting.Entities;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Olimp.Web.Controllers.Reporting
{
    public class TemplateUploadViewModel
    {
        public Report Template { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Reporting), DisplayNameResourceName = "File")]
        public HttpPostedFileBase XmlFile { get; set; }
    }
}