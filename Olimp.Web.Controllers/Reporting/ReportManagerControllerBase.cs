﻿using Olimp.Core;
using Olimp.Domain.Catalogue.Security;
using Olimp.Reporting;
using Olimp.Reporting.Entities;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core;
using PAPI.Core.Data;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Olimp.Web.Controllers.Reporting
{
    public abstract class ReportManagerControllerBase : PController
    {
        private IReportEngine _reportEngine;

        //TODO: Should be ctor injection
        public ReportManagerControllerBase()
        {
            _reportEngine = PContext.ExecutingModule.Kernel.Resolver.Resolve<IReportEngine>();
            if (_reportEngine == null)
                throw new InvalidOperationException("IReportEngine is not set");
        }

        protected abstract ReportType ReportType { get; }

        public abstract string AllowManagePermission { get; }

        [AuthorizeReportingAttribute(Roles = Permissions.Reports.Generate, Order = 2)]
        public virtual ActionResult Index()
        {
            return PartialView("~/Olimp.ReportManager/Views/ReportManager/Index.ascx", new TemplatesListViewModel
            {
                Type = this.ReportType,
                AllowManage = Roles.IsUserInRole(this.AllowManagePermission)
            });
        }

        [ChildActionOnly]
        [AuthorizeReportingAttribute(Roles = Permissions.Reports.Generate, Order = 2)]
        public ActionResult InPlace(string id)
        {
            using (UnitOfWork.CreateRead())
            {
                var templates = Repository.Of<Report>()
                    .Where(r => r.ReportType == this.ReportType)
                    .OrderBy(t => t.Name)
                    .Select(t => new SelectListItem { Text = t.Name, Value = t.Guid.ToString() })
                    .ToList();

                return PartialView("~/Olimp.ReportManager/Views/ReportManager/InPlace.ascx", new InPlaceViewModel
                {
                    Id = id,
                    Type = this.ReportType,
                    AllowManage = Roles.IsUserInRole(this.AllowManagePermission),
                    ReportTemplates = templates
                });
            }
        }

        [HttpPost]
        [AuthorizeReportingAttribute(Roles = Permissions.Reports.Generate, Order = 2)]
        public virtual ActionResult GetTemplates(TableViewModel model)
        {
            using (UnitOfWork.CreateRead())
            {
                return Json(Repository.Of<Report>()
                    .Where(r => r.ReportType == this.ReportType)
                    .GetTableData(model, r => new { r.Id, r.Name, r.Guid }));
            }
        }

        [HttpGet]
        [AuthorizeReportingAttribute(Order = 2)]
        public virtual ActionResult Upload()
        {
            return PartialView("~/Olimp.ReportManager/Views/ReportManager/Upload.ascx");
        }

        [HttpPost]
        [AuthorizeReportingAttribute(Order = 2)]
        [HandleIFrameErrorAttribute]
        public virtual ActionResult Upload(TemplateUploadViewModel model)
        {
            if (!ModelState.IsValid)
                throw new InvalidOperationException();

            var report = _reportEngine.TryAddNewTemplate(model.XmlFile.InputStream, this.ReportType);
            if (report == null)
            {
                return Json(new
                {
                    status = new
                    {
                        errorMessage = I18N.Reporting.IncorrectTemplateFormat
                    }
                }, "text/plain");
            }

            report.Name = model.Template.Name;

            using (var uow = UnitOfWork.CreateWrite())
            {
                Repository.Of<Report>().Save(report);

                uow.Commit();

                return Json(new
                {
                    status = "success",
                    report = new
                    {
                        id = report.Guid,
                        name = report.Name
                    }
                }, "text/plain");
            }

            //JsCallback("Olimp.Callbacks.TemplateXmlOkCallback", true);
        }

        [HttpPost]
        [AuthorizeReportingAttribute(Order = 2)]
        public virtual ActionResult Delete(int id)
        {
            var repo = Repository.Of<Report>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                var report = repo.GetById(id);

                repo.Delete(report);

                uow.Commit();
            }

            return null;
        }

        [HttpPost]
        [AuthorizeReportingAttribute(Order = 2)]
        public virtual ActionResult DeleteInPlace(Guid id)
        {
            var repo = Repository.Of<Report>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                var report = repo.SingleOrDefault(r => r.Guid == id);

                repo.Delete(report);

                uow.Commit();
            }

            return null;
        }

        [HttpGet]
        [AuthorizeReportingAttribute(Roles = Permissions.Reports.Generate, Order = 2)]
        public virtual ActionResult Download(int id)
        {
            using (UnitOfWork.CreateRead())
            {
                var template = Repository.Of<Report>().GetById(id);
                var source = Path.Combine(OlimpPaths.Default.Reports, template.TemplatePath);
                if (!System.IO.File.Exists(source))
                    return null;

                var file = System.IO.File.Open(source, FileMode.Open, FileAccess.Read, FileShare.Read);
                return File(file, "text/xml", "template.xml");
            }
        }

        [HttpGet]
        [AuthorizeReportingAttribute(Roles = Permissions.Reports.Generate, Order = 2)]
        public virtual ActionResult DownloadInPlace(Guid id)
        {
            using (UnitOfWork.CreateRead())
            {
                var template = Repository.Of<Report>().FirstOrDefault(r => r.Guid == id);

                return this.GetReportFile(template);
            }
        }

        [NonAction]
        [AuthorizeReportingAttribute(Roles = Permissions.Reports.Generate, Order = 2)]
        protected virtual ActionResult GetReportFile(Report template)
        {
            if (template == null)
                return null;

            var source = Path.Combine(OlimpPaths.Default.Reports, template.TemplatePath);
            if (!System.IO.File.Exists(source))
                return null;

            var file = System.IO.File.Open(source, FileMode.Open, FileAccess.Read, FileShare.Read);
            return File(file, "text/xml", "template.xml");
        }
    }
}