﻿namespace Olimp.Web.Controllers.Reporting
{
    public class InPlaceViewModel : ReportingViewModel
    {
        public string Id { get; set; }

        public bool AllowManage { get; set; }
    }
}