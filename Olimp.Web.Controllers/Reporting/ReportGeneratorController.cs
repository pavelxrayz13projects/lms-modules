﻿using Olimp.Reporting;
using Olimp.Web.Controllers.Base;
using System;
using System.IO;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Reporting
{
    public class ReportGeneratorController : PController
    {
        private IReportEngine _reportEngine;

        public ReportGeneratorController(IReportEngine reportEngine)
        {
            if (reportEngine == null)
                throw new ArgumentNullException("reportEngine");

            _reportEngine = reportEngine;
        }

        [NonAction]
        public bool HasModel(Guid guid)
        {
            var provider = this.GetReportProvider(guid);
            if (provider == null)
                return false;

            return provider.Presenter != null;
        }

        [NonAction]
        public void Generate(Guid guid, object model, Stream stream)
        {
            var provider = this.GetReportProvider(guid);

            if (provider != null)
                provider.GenerateToStream(model, stream);
        }

        [NonAction]
        public IReportProvider GetReportProvider(Guid guid)
        {
            return _reportEngine.GetReportProvider(guid);
        }

        [ChildActionOnly]
        public virtual ReportModelResult ReportModel(Guid guid)
        {
            var provider = this.GetReportProvider(guid);
            if (provider == null)
                throw new ArgumentException("Report " + guid.ToString() + " wasn't found");

            return new ReportModelResult(provider.Presenter);
        }
    }
}