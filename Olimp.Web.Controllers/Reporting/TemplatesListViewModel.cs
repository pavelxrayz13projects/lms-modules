using Olimp.Reporting;
using Olimp.Reporting.Entities;
using System.Collections.Generic;

namespace Olimp.Web.Controllers.Reporting
{
    public class TemplatesListViewModel
    {
        public bool AllowManage { get; set; }

        public ReportType Type { get; set; }

        public IEnumerable<Report> Reports { get; set; }
    }
}