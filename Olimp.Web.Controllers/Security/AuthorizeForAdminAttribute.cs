﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Olimp.Web.Controllers.Security
{
    public class AuthorizeForAdminAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var action = filterContext.ActionDescriptor;
            if (action.IsDefined(typeof(AllowAnonymousAttribute), true)) return;

            var controller = action.ControllerDescriptor;
            if (controller.IsDefined(typeof(AllowAnonymousAttribute), true)) return;

            base.OnAuthorization(filterContext);
        }


        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var currentUrl = HttpUtility.UrlEncode(
                filterContext.RequestContext.HttpContext.Request.RawUrl);
            var routeDictionary = new RouteValueDictionary
            {
                { "ReturnUrl", currentUrl }
            };

            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
                routeDictionary.Add("ErrorMessage", I18N.Auth.UserNotInRoleAuthError);

            filterContext.Result = new RedirectToRouteResult("AuthAdmin", routeDictionary);
        }
    }
}