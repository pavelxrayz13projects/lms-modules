﻿using System;
using System.Security;
using Olimp.Web.Controllers.Base;

namespace Olimp.Web.Controllers.Security
{
    public class HandleSecurityExceptionAttribute : HandleGenericExceptionAttributeBase
    {
        protected override Type GetTypeOfException()
        {
            return typeof (SecurityException);
        }
    }
}
