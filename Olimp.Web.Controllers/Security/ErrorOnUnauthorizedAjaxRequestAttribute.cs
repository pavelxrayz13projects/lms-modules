﻿using Olimp.Core.Mvc.Security;
using System;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class ErrorOnUnauthorizedAjaxRequestAttribute : ActionFilterAttribute, IAuthorizationFilter
    {
        #region IAuthorizationFilter implementation

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.Headers["X-Requested-With"] != "XMLHttpRequest")
                return;

            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new StatusCodeResultWithText(500, Olimp.I18N.Core.Messages.UnauthorizedAjax); //should be 401, but in this case strange behavior...
            }
        }

        #endregion IAuthorizationFilter implementation
    }
}