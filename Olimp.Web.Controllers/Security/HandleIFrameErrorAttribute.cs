﻿using Olimp.Core.Mvc.Security;
using System;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class HandleIFrameErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            var isIFrameRequested = filterContext.HttpContext.Request.Form["X-Requested-With"] == "IFrame";
            if (isIFrameRequested)
            {
                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
                filterContext.Result = new StatusCodeResultWithText(500, filterContext.Exception.Message);
            }
            else
            {
                base.OnException(filterContext);
            }
        }
    }
}