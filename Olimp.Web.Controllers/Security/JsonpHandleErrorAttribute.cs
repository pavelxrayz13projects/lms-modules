﻿using System;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Olimp.Web.Controllers.Security
{
    public class JsonpHandleErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null || filterContext.Exception == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            filterContext.HttpContext.Response.StatusCode = 200;
            var serializer = new JavaScriptSerializer();
            var message = serializer.Serialize(filterContext.Exception.Message);

            filterContext.Result = new JavaScriptResult
            {
                Script = String.Format("alert('{0}');", message)
            };
            filterContext.ExceptionHandled = true;
        }
    }
}