﻿namespace Olimp.Web.Controllers.Materials
{
    public class ShareUserBranchViewModel
    {
        public ShareUserBranchViewModel(string controller)
        {
            this.Controller = controller;
        }

        public string Controller { get; private set; }
    }
}