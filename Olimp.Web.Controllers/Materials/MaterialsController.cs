﻿using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Entities;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Materials
{
    public abstract class MaterialsController<T> : PController
        where T : MaterialBase
    {
        private IMaterialRepository<T> _materialRepo;

        public MaterialsController(IMaterialRepository<T> materialRepo)
        {
            if (materialRepo == null)
                throw new ArgumentNullException("materialRepo");

            _materialRepo = materialRepo;
        }

        public ActionResult Index()
        {
            return View();
        }

        [NonAction]
        protected virtual SwitchMaterialResult Switch(int id, bool state)
        {
            return _materialRepo.Switch(id, state);
        }

        [HttpPost]
        public ActionResult SwitchOff(int id)
        {
            this.Switch(id, false);

            return Json(new { result = true });
        }

        [HttpPost]
        public ActionResult SwitchOn(int id)
        {
            this.Switch(id, true);

            return Json(new { result = true });
        }

        [HttpPost]
        public ActionResult GetMaterials(TableViewModel model)
        {
            var data = _materialRepo.GetAvailableWithState().Select(s => new
            {
                Id = s.Material.Id,
                Name = Convert.ToString(s.Material),
                Path = s.Material.FileName,
                Exists = s.FileExists,
                UniqueId = String.Join(".", s.Material.MaterialId, s.Material.Version),
                Timestamp = s.Material.DateIssued.ToShortDateString(),
                IsActive = s.IsActive
            });

            return Json(data.GetTableData(model, r => r));
        }
    }
}