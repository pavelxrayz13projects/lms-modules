﻿namespace Olimp.Web.Controllers.Materials
{
    public class MaterialsPageViewModel
    {
        public MaterialsPageViewModel(string controller, string header)
        {
            this.Controller = controller;
            this.Header = header;
        }

        public string Controller { get; private set; }

        public string Header { get; private set; }
    }
}