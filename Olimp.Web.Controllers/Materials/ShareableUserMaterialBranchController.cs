﻿using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Entities;
using Olimp.Web.Controllers.Base;
using System;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Materials
{
    public class ShareableUserMaterialBranchController<T> : PController
        where T : UserMaterialBranch
    {
        private IShareableMaterialBranchRepository<T> _branchRepository;

        public ShareableUserMaterialBranchController(IShareableMaterialBranchRepository<T> branchRepository)
        {
            if (branchRepository == null)
                throw new ArgumentNullException("branchRepository");

            _branchRepository = branchRepository;
        }

        public virtual ActionResult Share(int branchId, bool state)
        {
            var branch = _branchRepository.GetById(branchId);

            _branchRepository.Share(branch, state);

            return Json(state);
        }
    }
}