﻿using System;
using Olimp.Web.Controllers.Base;

namespace Olimp.Web.Controllers.Users
{
    public class HandleCannotDeleteUserException : HandleGenericExceptionAttributeBase
    {
        protected override Type GetTypeOfException()
        {
            return typeof (CannotDeleteUserException);
        }
    }
}
