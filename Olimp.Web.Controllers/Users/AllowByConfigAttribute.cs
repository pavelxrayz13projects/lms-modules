﻿using Olimp.Configuration;
using Olimp.Web.Controllers.Base;
using PAPI.Core.Data;
using System;
using System.Reflection;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Users
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class AllowByConfigAttribute : AllowAttribute
    {
        public AllowByConfigAttribute(Type configurationContainer)
        {
            this.ConfigurationContainer = configurationContainer;
        }

        public Type ConfigurationContainer { get; private set; }

        public string FieldName { get; set; }

        public override bool IsAllowed(ActionExecutingContext filterContext)
        {
            IUnitOfWork uow = null;
            var controller = filterContext.Controller as PController;
            if (controller != null)
                uow = controller.UnitOfWork.CreateRead();

            try
            {
                if (this.ConfigurationContainer == null || String.IsNullOrWhiteSpace(this.FieldName))
                    return false;

                var configObject = Config.Get(this.ConfigurationContainer);
                if (configObject == null)
                    return false;

                var property = this.ConfigurationContainer.GetProperty(this.FieldName,
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);

                if (property == null || property.PropertyType != typeof(bool))
                    return false;

                return (bool)property.GetValue(configObject, null);
            }
            finally
            {
                if (uow != null)
                    uow.Dispose();
            }
        }
    }
}