﻿using Olimp.Domain.LearningCenter.Entities;
using Olimp.Web.Controllers.Base;
using PAPI.Core.Data;
using System;
using System.Web;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Users
{
    public abstract class UserController : PController
    {
        public const int UserAuthorizing = -9000;

        private Guid? _employeeId;
        private Lazy<Employee> _employee;

        public UserController()
        {
            _employee = new Lazy<Employee>(this.GetEmployee);
        }

        private Employee GetEmployee()
        {
            Employee employee = null;
            if (this.EmployeeId != Guid.Empty)
            {
                using (UnitOfWork.CreateRead())
                {
                    employee = Repository.Of<Employee, Guid>().GetById(this.EmployeeId);
                }
            }
            if (employee == null)
                return null;

            return employee;
        }

        protected abstract string UserCookieName { get; }

        protected abstract string UserCookiePath { get; }

        public virtual void DeleteUserCookie()
        {
            this.Response.Cookies.Add(new HttpCookie(this.UserCookieName)
            {
                Expires = DateTime.Today.AddYears(-10),
                Path = this.UserCookiePath
            });
        }

        public virtual Guid EmployeeId
        {
            get
            {
                if (_employeeId.HasValue)
                    return _employeeId.Value;

                Guid employeeId;
                var cookie = this.Request.Cookies[this.UserCookieName];
                var result = cookie != null ? cookie.Values["employeeId"] : null;
                if (result == null || !Guid.TryParse(result, out employeeId))
                    _employeeId = default(Guid);
                else
                    _employeeId = employeeId;

                return _employeeId.Value;
            }
        }

        public virtual bool IsAuthorizing { get { return false; } }

        public Employee Employee { get { return _employee.Value; } }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            var employee = this.Employee;
            if (employee != null && !String.IsNullOrWhiteSpace(employee.UILocale))
                SetCulture(employee.UILocale);
        }
    }
}