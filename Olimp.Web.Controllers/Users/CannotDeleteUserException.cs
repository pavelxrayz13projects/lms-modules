﻿using System;

namespace Olimp.Web.Controllers.Users
{
    public class CannotDeleteUserException : Exception
    {
        public CannotDeleteUserException(string message)
            :base(message)
        {
        }
    }
}
