﻿using Olimp.Configuration;
using Olimp.Domain.LearningCenter;
using PAPI.Core.Data;
using System;
using System.Reflection;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Users
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class AllowByConfigurationAttribute : AllowAttribute
    {
        public AllowByConfigurationAttribute()
        {
            this.Message = Olimp.I18N.Core.Messages.DisallowedByConfigSettings;
        }

        public string FieldName { get; set; }

        public override bool IsAllowed(ActionExecutingContext filterContext)
        {
            var controller = filterContext.Controller as UserController;
            if (controller == null || controller.IsAuthorizing)
                return true;

            IUnitOfWork uow = null;
            IUnitOfWorkFactory _unitOfWork = controller.UnitOfWork;

            if (_unitOfWork != null)
                uow = _unitOfWork.CreateRead();
            try
            {
                var config = Config.Get<OlimpConfiguration>();
                var configProperty = config.GetType().GetProperty(this.FieldName,
                    BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);

                if (configProperty == null || configProperty.PropertyType != typeof(bool))
                    return true;

                return (bool)configProperty.GetValue(config, null);
            }
            finally
            {
                if (uow != null)
                    uow.Dispose();
            }
        }

        private void DeleteCookie(ControllerBase controller)
        {
            var userController = controller as UserController;
            if (userController == null)
                return;

            userController.DeleteUserCookie();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                base.OnActionExecuting(filterContext);
            }
            catch
            {
                this.DeleteCookie(filterContext.Controller);
                throw;
            }

            if (filterContext.Result != null)
                this.DeleteCookie(filterContext.Controller);
        }
    }
}