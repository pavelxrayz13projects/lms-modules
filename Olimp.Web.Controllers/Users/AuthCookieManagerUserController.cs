﻿using Olimp.Domain.Exam.Auth;

namespace Olimp.Web.Controllers.Users
{
    public class AuthCookieManagerUserController : UserController
    {
        protected override string UserCookieName
        {
            get { return AuthCookieManager.GetNameByUrl(Request["returnUrl"] ?? (string)ViewData["returnUrl"] ?? "userCookie"); }
        }

        protected override string UserCookiePath
        {
            get { return AuthCookieManager.GetPathByUrl(Request["returnUrl"] ?? (string)ViewData["returnUrl"] ?? "/"); }
        }
    }
}