﻿using Olimp.Web.Controllers.Users;
using System;
using System.Web;
using System.Web.Mvc;

namespace Olimp.Web.Controllers.Learning
{
    public abstract class IntegrationUserController : UserController, IIntegrationContextContainer
    {
        private IntegrationContext _integrationContext;

        [NonAction]
        protected virtual bool KeepIntegrationContext(IntegrationContext ctx)
        {
            return true;
        }

        public IntegrationContext IntegrationContext
        {
            get
            {
                if (_integrationContext == null)
                {
                    _integrationContext = new IntegrationContext();

                    var integrationCookie = this.Request.Cookies[this.IntegrationCookieName];
                    if (integrationCookie != null)
                    {
                        Uri referer;
                        if (Uri.TryCreate(integrationCookie.Values["referer"], UriKind.Absolute, out referer))
                        {
                            var ctx = new IntegrationContext(referer);
                            foreach (string key in integrationCookie.Values)
                                ctx[key] = integrationCookie.Values[key];

                            if (!this.KeepIntegrationContext(ctx))
                                ctx.IntegrationState = IntegrationContextState.ContextInvalid;

                            _integrationContext = ctx;
                        }
                        else
                        {
                            _integrationContext.IntegrationState = IntegrationContextState.NoReferer;
                        }
                    }
                }

                return _integrationContext;
            }
        }

        protected virtual string IntegrationCookieName { get { return "IntegrationContext"; } }

        protected virtual string IntegrationCookiePath { get { return "/"; } }

        protected virtual bool ExitIntegrationModeOnReturn { get { return false; } }

        [NonAction]
        protected virtual DateTime GetIntegrationCookieExpiration(DateTime current)
        {
            return current.AddDays(2);
        }

        [NonAction]
        protected virtual HttpCookie CreateIntegrationCookie(IntegrationContext context)
        {
            this.EnsureIntegrationContext(context);

            var cookie = new HttpCookie(this.IntegrationCookieName)
            {
                Path = this.IntegrationCookiePath,
                Expires = this.GetIntegrationCookieExpiration(DateTime.Now)
            };

            foreach (var pair in context)
                cookie.Values.Add(pair.Key, pair.Value);

            cookie.Values["referer"] = context.IntegrationReferer.ToString();

            return cookie;
        }

        [NonAction]
        protected void EnterIntegrationMode(string referer, object data = null)
        {
            var refererUri = new Uri(referer, UriKind.Absolute);

            var ctx = new IntegrationContext(refererUri);

            var dataParams = HtmlHelper.AnonymousObjectToHtmlAttributes(data);
            foreach (var pair in dataParams)
                ctx[pair.Key] = Convert.ToString(pair.Value);

            var cookie = this.CreateIntegrationCookie(ctx);
            if (cookie != null)
                this.Response.Cookies.Add(cookie);

            _integrationContext = ctx;
        }

        [NonAction]
        protected void ExitIntegrationMode()
        {
            if (this.IntegrationContext.IntegrationState == IntegrationContextState.NoIntegrationCookie)
                return;

            if (this.Request.Cookies[this.IntegrationCookieName] != null)
            {
                var cookie = new HttpCookie(this.IntegrationCookieName)
                {
                    Path = this.IntegrationCookiePath,
                    Expires = new DateTime(2000, 1, 1)
                };

                this.Response.Cookies.Add(cookie);
            }

            _integrationContext = new IntegrationContext();
        }

        [NonAction]
        private void EnsureIntegrationContext(IntegrationContext ctx)
        {
            if (ctx == null)
                throw new ArgumentNullException("ctx");

            switch (ctx.IntegrationState)
            {
                case IntegrationContextState.NoReferer:
                    throw new InvalidOperationException("Integration context contains no referer");
                case IntegrationContextState.NoIntegrationCookie:
                    throw new InvalidOperationException("Integration cookie is missing");
                case IntegrationContextState.ContextInvalid:
                    throw new InvalidOperationException("Integration context is invalid");
            }
        }

        [HttpPost]
        public virtual ActionResult ReturnToRefereingSystem()
        {
            this.EnsureIntegrationContext(this.IntegrationContext);
            var referer = this.IntegrationContext.IntegrationReferer;

            if (this.ExitIntegrationModeOnReturn)
                this.ExitIntegrationMode();

            return Redirect(referer.ToString());
        }
    }
}