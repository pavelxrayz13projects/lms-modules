﻿namespace Olimp.Web.Controllers.Learning
{
    public enum IntegrationContextState
    {
        NoIntegrationCookie,
        NoReferer,
        ContextInvalid,
        InIntegrationContext
    }
}