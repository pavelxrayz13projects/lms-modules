﻿namespace Olimp.Web.Controllers.Learning
{
    internal interface IIntegrationContextContainer
    {
        IntegrationContext IntegrationContext { get; }
    }
}