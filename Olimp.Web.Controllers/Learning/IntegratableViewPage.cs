﻿using Olimp.Core.Mvc;

namespace Olimp.Web.Controllers.Learning
{
    public class IntegratableViewPage<TModel> : OlimpViewPage<TModel>, IIntegrationContextContainer
    {
        private IntegrationContext _integrationContext;

        public IntegrationContext IntegrationContext
        {
            get
            {
                if (_integrationContext == null)
                {
                    var integrationUserController = this.ViewContext.Controller as IntegrationUserController;

                    _integrationContext = integrationUserController != null && integrationUserController.IntegrationContext != null
                        ? integrationUserController.IntegrationContext
                        : new IntegrationContext();
                }

                return _integrationContext;
            }
        }
    }
}