﻿using System;
using System.Web;

namespace Olimp.Web.Controllers.Learning.Exam
{
    public static class ExamAndPrepareScormIntegrationHelper
    {
        public static int GetProfileIdFromCookisOrUrl(HttpRequestBase request)
        {
            const string profileIdParamName = "profileId";
            const string integrationContextCookiName = "IntegrationContext";

            var profileIdString = HttpUtility.ParseQueryString(request.Url.AbsoluteUri).Get(profileIdParamName);

            if (String.IsNullOrWhiteSpace(profileIdString))
            {
                var cookie = request.Cookies.Get(integrationContextCookiName);

                if (cookie == null)
                    return 0;

                profileIdString = HttpUtility.ParseQueryString(cookie.Value).Get(profileIdParamName);
            }

            int profileId;
            return !int.TryParse(profileIdString, out profileId) ? 0 : profileId;
        }
    }
}