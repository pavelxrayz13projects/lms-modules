﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace Olimp.Web.Controllers.Learning
{
    public static class IntegrationHtmlExtensions
    {
        public static void ReturnToReferringSystemButton(this HtmlHelper html, string buttonText, object routeValues = null, object htmlAttributes = null)
        {
            var attrs = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes ?? new Object());
            attrs["type"] = "submit";

            var routeDictionary = new RouteValueDictionary(routeValues);
            var action = (string)(routeDictionary["action"] = routeDictionary["action"] ?? "ReturnToRefereingSystem");
            var controller = (string)(routeDictionary["controller"] = routeDictionary["controller"] ?? html.ViewContext.RouteData.Values["controller"]);

            using (html.BeginForm(action, controller, routeDictionary, FormMethod.Post, new Dictionary<string, object> { { "style", "display: inline" } }))
            {
                var buttonBuilder = new TagBuilder("button");
                buttonBuilder.MergeAttributes(attrs);
                buttonBuilder.SetInnerText(buttonText);

                html.ViewContext.Writer.Write(buttonBuilder);
            }
        }
    }
}