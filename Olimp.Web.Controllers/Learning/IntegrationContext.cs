﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Olimp.Web.Controllers.Learning
{
    public class IntegrationContext : IEnumerable<KeyValuePair<string, string>>
    {
        private IDictionary<string, string> _data;

        public IntegrationContext()
        {
            this.IntegrationState = IntegrationContextState.NoIntegrationCookie;

            _data = new Dictionary<string, string>();
        }

        public IntegrationContext(Uri referer)
            : this()
        {
            if (referer == null)
                throw new ArgumentNullException("referer");

            this.IntegrationReferer = referer;
            this.IntegrationState = IntegrationContextState.InIntegrationContext;
        }

        public string this[string key]
        {
            get
            {
                string value;
                if (_data.TryGetValue(key, out value))
                    return value;

                return null;
            }
            set
            {
                _data[key] = value;
            }
        }

        public Uri IntegrationReferer { get; private set; }

        public IntegrationContextState IntegrationState { get; internal set; }

        #region IEnumerable<KeyValuePair<string,string>> Members

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        #endregion IEnumerable<KeyValuePair<string,string>> Members

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion IEnumerable Members
    }
}