﻿using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Domain.LearningCenter.NotPersisted;
using Olimp.Profile.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using ServiceStack.Text;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.Profile.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.ExamSettings, Order = 2)]
    public class ExamSettingsController : PController
    {
        [NonAction]
        private static void SelectTimeRestrictionBySpecifier(ExamSettingsViewModel model)
        {
            int examTimeLimit;

            switch (model.ExamSettings.ExamTimeRestriction)
            {
                case ExamTimeLimit.TimeForQuestion:
                    if (String.IsNullOrWhiteSpace(model.QuestionTimeLimit) ||
                        !int.TryParse(model.QuestionTimeLimit, out examTimeLimit))
                    {
                        examTimeLimit = 1;
                    }
                    break;

                case ExamTimeLimit.TimeForTicket:
                    if (String.IsNullOrWhiteSpace(model.ExamTimeLimit) ||
                        !int.TryParse(model.ExamTimeLimit, out examTimeLimit))
                    {
                        examTimeLimit = 20;
                    }
                    break;

                default:
                    examTimeLimit = 1;
                    break;
            }

            model.ExamSettings.ExamTimeLimit = examTimeLimit;
        }

        [NonAction]
        private static void GetTimeRestrictionBySpecifier(ExamSettingsViewModel model)
        {
            model.ExamTimeLimit = null;
            model.QuestionTimeLimit = null;

            if (!model.ExamSettings.ExamTimeLimit.HasValue)
                return;

            var timeLimit = model.ExamSettings.ExamTimeLimit.Value.ToString();

            switch (model.ExamSettings.ExamTimeRestriction)
            {
                case ExamTimeLimit.TimeForQuestion:
                    if (!String.IsNullOrWhiteSpace(timeLimit))
                        model.QuestionTimeLimit = timeLimit;
                    break;

                case ExamTimeLimit.TimeForTicket:
                    if (!String.IsNullOrWhiteSpace(timeLimit))
                        model.ExamTimeLimit = timeLimit;
                    break;
            }
        }

        [HttpGet]
        public ActionResult Index(int? examSettingsId)
        {
            var repo = Repository.Of<ExamSettings>();

            using (UnitOfWork.CreateRead())
            {
                var examSettings = repo.GetById(examSettingsId);
                var examSettingsList = repo.Where(p => p.IsActive).OrderBy(p => p.Name)
                    .ToList();

                examSettings = examSettings ?? examSettingsList.FirstOrDefault();
                if (examSettings != null)
                    examSettingsId = examSettings.Id;

                var viewModel = new ExamSettingsViewModel
                {
                    ExamSettingsId = examSettingsId.GetValueOrDefault(),
                    ExamSettingsString = JsonSerializer.SerializeToString(
                        examSettingsList.Select(p => new {p.Name, p.Id})),
                    ExamSettings = examSettings
                };

                GetTimeRestrictionBySpecifier(viewModel);

                JsConfig.ExcludeTypeInfo = true;
                return View(viewModel);
            }
        }

        [HttpPost]
        public ActionResult EditExamSettings(int? id)
        {
            if (!ModelState.IsValid)
                return null;

            var repo = Repository.Of<ExamSettings>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                var examSettingsId = id.GetValueOrDefault();
                var viewModel = new ExamSettingsViewModel
                {
                    ExamSettings = examSettingsId > 0
                        ? repo.LoadById(examSettingsId)
                        : new ExamSettings()
                };

                this.UpdateModel(viewModel);

                SelectTimeRestrictionBySpecifier(viewModel);

                repo.Save(viewModel.ExamSettings);

                uow.Commit();

                id = viewModel.ExamSettings.Id;
            }

            return RedirectToAction("Index", new { examSettingsId = id });
        }

        [HttpPost]
        public ActionResult DeleteExamSettings(int examSettingsId)
        {
            var repo = Repository.Of<ExamSettings>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                var examSettings = repo.GetById(examSettingsId);

                repo.Delete(examSettings);

                uow.Commit();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Save(int? examSettingsId)
        {
            var repo = Repository.Of<ExamSettings>();

            using (var uow = UnitOfWork.CreateWrite())
            {
                var id = examSettingsId.GetValueOrDefault();
                var viewModel = new ExamSettingsViewModel
                {
                    ExamSettings = id > 0
                        ? repo.LoadById(id)
                        : new ExamSettings()
                };

                this.UpdateModel(viewModel);

                repo.Save(viewModel.ExamSettings);

                uow.Commit();

                examSettingsId = viewModel.ExamSettings.Id;
            }

            return RedirectToAction("Index", new { examSettingsId });
        }
    }
}