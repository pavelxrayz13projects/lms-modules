﻿using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Profile.ViewModels;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using System;
using System.Web.Mvc;

namespace Olimp.Profile.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Profiles, Order = 2)]
    public class AdditionalEventController : PController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetAdditionalEvents(TableViewModel model)
        {
            using (UnitOfWork.Create())
            {
                var events = Repository.Of<AdditionalEvent>();

                return Json(events.GetTableData(model, c => new { c.Id, c.Name }));
            }
        }

        [HttpPost]
        public ActionResult Edit(AdditionalEventEditViewModel model)
        {
            if (!ModelState.IsValid)
                return null;

            var repo = Repository.Of<AdditionalEvent>();

            using (var uow = UnitOfWork.Create())
            {
                AdditionalEvent evt = null;
                if (model.AdditionalEvent.Id > 0)
                    evt = repo.GetById(model.AdditionalEvent.Id);

                if (evt == null)
                    evt = new AdditionalEvent();

                evt.Name = model.AdditionalEvent.Name;

                repo.Save(evt);

                uow.Commit();
            }

            return null;
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (var uow = UnitOfWork.Create())
            {
                var repo = Repository.Of<AdditionalEvent>();
                var additionalEvent = repo.GetById(id);
                foreach (var profile in additionalEvent.Profiles)
                {
                    profile.AdditionalEventsLastModified = DateTime.Now;
                }
                additionalEvent.Profiles.Clear();
                repo.DeleteById(id);

                uow.Commit();
            }

            return null;
        }
    }
}