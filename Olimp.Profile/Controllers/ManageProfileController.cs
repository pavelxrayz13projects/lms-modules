﻿using Olimp.Configuration;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Exam.NHibernate.Schema;
using Olimp.Domain.LearningCenter;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Profile.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core.Data;
using PAPI.Core.Data.Entities;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Model = Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Profile.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Profiles, Order = 2)]
    public class ManageProfileController : PController
    {
        private readonly IExamAreasListProvider _examAreasListProvider;
        private readonly IMaterialCodesWithExamAreasProvider _materialCodesWithExamAreasProvider;
        private readonly IPersonalCatalogueProvider _personalCatalogueProvider;

        public ManageProfileController(
            IExamAreasListProvider examAreasListProvider,
            IMaterialCodesWithExamAreasProvider materialCodesWithExamAreasProvider,
            IPersonalCatalogueProvider personalCatalogueProvider)
        {
            if (examAreasListProvider == null)
                throw new ArgumentNullException("examAreasListProvider");

            if (materialCodesWithExamAreasProvider == null)
                throw new ArgumentNullException("materialCodesWithExamAreasProvider");

            if (personalCatalogueProvider == null)
                throw new ArgumentNullException("personalCatalogueProvider");

            _examAreasListProvider = examAreasListProvider;
            _materialCodesWithExamAreasProvider = materialCodesWithExamAreasProvider;
            _personalCatalogueProvider = personalCatalogueProvider;
        }

        [HttpGet]
        public ActionResult Index()
        {
            using (UnitOfWork.CreateRead())
            {
                var profiles = Repository.Of<Model.Profile>().OrderBy(p => p.Name).ToList();

                var additionalEvents = Repository.Of<AdditionalEvent>()
                    .OrderBy(p => p.Name)
                    .Select(a => new {label = a.Name, optionValue = a.Name})
                    .ToList();

                var appointments = Repository.Of<Appointment>()
                    .OrderBy(a => a.Name)
                    .Select(a => new { label = a.Name, optionValue = a.Name })
                    .ToList();

                var companies = Repository.Of<Company>()
                    .OrderBy(c => c.Name)
                    .Select(c => new { label = c.Name, optionValue = c.Name })
                    .ToList();

                var groups = Repository.Of<Group>()
                    .OrderBy(g => g.Name)
                    .Select(g => new { label = g.Name, optionValue = g.Name })
                    .ToList();

                var examAreas = _examAreasListProvider
                    .GetAreas()
                    .Select(g => new
                    {
                        label = string.Format("{0}. {1}", g.Code, g.Name),
                        optionValue = g.Code
                    })
                    .ToList();

                JsConfig.ExcludeTypeInfo = true;

                return View(new ManageProfileViewModel
                {
                    AreaCodeToMaterialCodeMap =
                        JsonSerializer.SerializeToString(
                            _materialCodesWithExamAreasProvider.GetAllMaterialCodesWithBranchesInfo()),
                    AllAdditionalEvents = JsonSerializer.SerializeToString(additionalEvents),
                    AreaAutoFill = Config.Get<OlimpConfiguration>().AreaAutoFill,
                    AllAppointments = JsonSerializer.SerializeToString(appointments),
                    AllCompanies = JsonSerializer.SerializeToString(companies),
                    AllGroups = JsonSerializer.SerializeToString(groups),
                    AllExamAreas = JsonSerializer.SerializeToString(examAreas),
                    Profiles = JsonSerializer.SerializeToString(profiles.Select(p => new
                    {
                        p.Id,
                        p.Name,
                        AlternativeName = p.AlternativeName ?? string.Empty,
                        p.Periodicity,
                        AdditionalEvents = p.AdditionalEvents.Select(a => new {label = a.Name, optionValue = a.Name}),
                        Courses = p.Courses.Select(c => c.MaterialId),
                        Companies = p.Companies.Select(a => new {label = a.Name, optionValue = a.Name}),
                        Groups = p.Groups.Select(g => new {label = g.Name, optionValue = g.Name}),
                        ExamAreas = _examAreasListProvider.GetAreas().Where(ea => p.ExamAreas.Contains(ea.Code))
                            .Select((ea => new
                            {
                                label = string.Format("{0}. {1}", ea.Code, ea.Name),
                                optionValue = ea.Code
                            }))
                    }))
                });
            }
        }

        [HttpPost]
        public ActionResult EditProfile(int? profileId)
        {
            if (!ModelState.IsValid)
                return null;

            var repo = Repository.Of<Model.Profile>();

            try
            {
                using (var uow = UnitOfWork.CreateWrite())
                {
                    var id = profileId.GetValueOrDefault();
                    var viewModel = new ManageProfileViewModel
                    {
                        Profile = id > 0 ? repo.LoadById(id) : new Model.Profile()
                    };

                    UpdateModel(viewModel);

                    repo.Save(viewModel.Profile);

                    uow.Commit();

                    return Json(new
                    {
                        successes = true,
                        message = I18N.Profile.ProfileSaved,
                        profile = new {viewModel.Profile.Id, viewModel.Profile.Name}
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new {successes = false, message = e.Message});
            }
        }

        [HttpPost]
        public ActionResult DeleteProfile(int profileId)
        {
            var profileRepo = Repository.Of<Model.Profile>();
            var companyRepo = Repository.Of<Company>();
            var appointmentRepo = Repository.Of<Appointment>();
            var profileResultRepo = Repository.Of<ProfileResultSchema>();
            var additionalEventResultRepo = Repository.Of<AdditionalEventResultSchema>();

            try
            {
                using (var uow = UnitOfWork.CreateWrite())
                {
                    var profile = profileRepo.GetById(profileId);

                    // additional events
                    foreach (var additionalEvent in profile.AdditionalEvents)
                        additionalEvent.Profiles.Remove(profile);
                    // group
                    foreach (var group in profile.Groups)
                        group.Profiles.Remove(profile);
                    // company
                    foreach (var company in companyRepo.Where(c => c.Profiles.Contains(profile)))
                        company.Profiles.Remove(profile);
                    // employee
                    foreach (var employee in profile.Employees)
                        employee.Profiles.Remove(profile);
                    // appointment
                    foreach (var appointment in appointmentRepo.Where(a => a.Profiles.Contains(profile)))
                        appointment.Profiles.Remove(profile);
                    // profile result
                    foreach (var profileResult in profileResultRepo.Where(pr => pr.IsTemporary && pr.ConcreteProfileId == profileId))
                        profileResultRepo.Delete(profileResult);
                    // additional event result
                    foreach (var eventResult in additionalEventResultRepo.Where(pr => pr.IsTemporary && pr.ConcreteProfileId == profileId))
                        additionalEventResultRepo.Delete(eventResult);

                    profile.AdditionalEvents.Clear();
                    profile.Groups.Clear();
                    profile.Employees.Clear();
                    profile.Appointments.Clear();
                    profile.Companies.Clear();
                    profile.ExamAreas.ToList().Clear();

                    profileRepo.DeleteById(profileId);

                    uow.Commit();
                }

                return Json(new {successes = true, message = I18N.Profile.ProfileDeleted});
            }
            catch (Exception e)
            {
                return Json(new { successes = false, message = e.Message });
            }
        }

        [HttpPost]
        public ActionResult Save(int id)
        {
            var model = new ProfileFormViewModel();
            var profileCourseRepo = Repository.Of<ProfileCourse>();
            var profileRepo = Repository.Of<Model.Profile>();
            var additionalEventRepo = Repository.Of<AdditionalEvent>();
            var appointmentRepo = Repository.Of<Appointment>();
            var companyRepo = Repository.Of<Company>();
            var groupRepo = Repository.Of<Group>();

            try
            {
                using (var uow = UnitOfWork.CreateWrite())
                {
                    model.CurrentProfile = profileRepo.GetById(id);
                    model.CurrentProfile.ExamArea = null;

                    UpdateModel(model);

                    var config = Config.Get<OlimpConfiguration>();
                    config.AreaAutoFill = model.AreaAutoFill;
                    Config.Set<OlimpConfiguration>(config);

                    model.CurrentProfile.Periodicity = model.Periodicity;

                    var materials = Deserializ(model.Materials).OrderBy(m => m).ToList();

                    var materialsChanged = !model.CurrentProfile.Courses
                        .Select(c => c.MaterialId)
                        .OrderBy(m => m)
                        .SequenceEqual(materials);

                    if (materialsChanged)
                    {
                        foreach (var c in model.CurrentProfile.Courses.ToList())
                        {
                            model.CurrentProfile.Courses.Remove(c);
                            c.Profile = null;
                            profileCourseRepo.Delete(c);
                        }
                        model.CurrentProfile.ChangeInCoursesDate = DateTime.Now;

                        if (model.Materials != null)
                            model.CurrentProfile.SelectCourses(materials);
                    }

                    foreach (var adEvent in model.CurrentProfile.AdditionalEvents)
                        adEvent.Profiles.Remove(model.CurrentProfile);
                    model.CurrentProfile.AdditionalEvents.Clear();

                    MergeDependencyEntities(
                        additionalEventRepo,
                        model.AdditionalEvents,
                        e => model.CurrentProfile.AddAdditionalEvent(e));

                    foreach (var appointment in model.CurrentProfile.Appointments)
                        appointment.Profiles.Remove(model.CurrentProfile);
                    model.CurrentProfile.Appointments.Clear();

                    MergeDependencyEntities(
                        appointmentRepo, 
                        model.Appointments,
                        e => model.CurrentProfile.AddAppointment(e));

                    foreach(var company in model.CurrentProfile.Companies)
                        company.Profiles.Remove(model.CurrentProfile);
                    model.CurrentProfile.Companies.Clear();

                    MergeDependencyEntities(
                        companyRepo, 
                        model.Companies,
                        e => model.CurrentProfile.AddCompany(e));

                    foreach (var group in model.CurrentProfile.Groups)
                        group.Profiles.Remove(model.CurrentProfile);
                    model.CurrentProfile.Groups.Clear();

                    MergeDependencyEntities(
                        groupRepo, 
                        model.Groups,
                        e => model.CurrentProfile.AddGroup(e));

                    model.CurrentProfile.ExamArea = null;

                    var examAreas = "";
                    if (!String.IsNullOrWhiteSpace(model.ExamAreasString))
                    {
                        var examAreasArray = model.ExamAreasString.Split('#');
                        examAreas = JsonSerializer.SerializeToString(examAreasArray);
                    }

                    model.CurrentProfile.ExamArea = examAreas;

                    profileRepo.Save(model.CurrentProfile);

                    uow.Commit();
                }

                return Json(new
                {
                    successes = true, 
                    message = I18N.Profile.ProfileSaved,
                    TreeData = _personalCatalogueProvider.GetPersonalExamCourses(id)
                });
            }
            catch (Exception e)
            {
                return Json(new { successes = false, message = e.Message });
            }
        }

        [HttpPost]
        public ActionResult GetTree(string materials)
        {
            return Json(_personalCatalogueProvider.GetPersonalCourses(Deserializ(materials)));
        }

        private static ICollection<Guid> Deserializ(string materials)
        {
            return string.IsNullOrWhiteSpace(materials)
                ? new List<Guid>()
                : materials.Split(new[] { '#' }, StringSplitOptions.RemoveEmptyEntries).Select(Guid.Parse).ToList();
        }

        private static void MergeDependencyEntities<TEntity, TId>(
            IRepository<TEntity, TId> entityRepo,
            string entityAsPlainText,
            Action<TEntity> addEntityToCurrentProfile)
            where TEntity : class, IEntity<TId>, INamedEntity, new()
        {
            if (!String.IsNullOrWhiteSpace(entityAsPlainText))
            {
                var entitiesArray = entityAsPlainText.Split('#');
                foreach (var entityName in entitiesArray)
                {
                    if (String.IsNullOrWhiteSpace(entityName))
                        continue;
                    var entity = entityRepo.FirstOrDefault(a => a.Name == entityName) ??
                                 new TEntity { Name = entityName };

                    var e = entity as IPermanent;
                    if (e != null)
                        e.IsActive = true;

                    addEntityToCurrentProfile(entity);
                }
            }
        }
    }
}