﻿using Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Profile.ViewModels
{
    public class AdditionalEventEditViewModel
    {
        public AdditionalEvent AdditionalEvent { get; set; }
    }
}