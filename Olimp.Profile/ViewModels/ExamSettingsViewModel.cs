﻿using Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Profile.ViewModels
{
    public class ExamSettingsViewModel
    {
        public string ExamSettingsString { get; set; }

        public int ExamSettingsId { get; set; }

        public string ExamTimeLimit { get; set; }

        public string QuestionTimeLimit { get; set; }

        public ExamSettings ExamSettings { get; set; }
    }
}