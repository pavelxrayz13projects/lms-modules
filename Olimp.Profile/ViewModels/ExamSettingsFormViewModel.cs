﻿using Olimp.Core.ComponentModel;
using Olimp.Domain.LearningCenter.Entities;
using System.ComponentModel.DataAnnotations;

namespace Olimp.Profile.ViewModels
{
    public class ExamSettingsFormViewModel
    {
        public ExamSettings ExamSettings { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.Profile), DisplayNameResourceName = "ExamTimeLimit")]
        [RegularExpression("^([1-9]|[1-9][0-9]|[1-9][0-9][0-9]|1[0-3][0-9][0-9]|1[0-4][0-3][0-9]|1440)$", ErrorMessageResourceType = typeof(Olimp.I18N.Domain.Profile), ErrorMessageResourceName = "ExamTimeLimitInvalid")]
        public string ExamTimeLimit { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.Profile), DisplayNameResourceName = "ExamTimeLimit")]
        [RegularExpression("^([1-9]|[1-5][0-9]|60)$", ErrorMessageResourceType = typeof(Olimp.I18N.Domain.Profile), ErrorMessageResourceName = "QuestionTimeLimitInvalid")]
        public string QuestionTimeLimit { get; set; }
    }
}