﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Olimp.Core.ComponentModel;
using Model = Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Profile.ViewModels
{
    public class ManageProfileViewModel
    {
        public string Profiles { get; set; }

        public int ProfileId { get; set; }

        public Model.Profile Profile { get; set; }

        public bool ExamAreasAvailable { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.Config), DisplayNameResourceName = "AreaAutoFill")]
        public bool AreaAutoFill { get; set; }

        public string AreaCodeToMaterialCodeMap { get; set; }

        public IEnumerable<SelectListItem> ExamAreas { get; set; }

        public ICollection<Guid> Materials { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.AdditionalEvent), DisplayNameResourceName = "AdditionalEvents")]
        public string AdditionalEvents { get; set; }

        public string Appointments { get; set; }

        public string Companies { get; set; }

        public string Groups { get; set; }

        public string ExamAreasString { get; set; }

        public string AllAdditionalEvents { get; set; }

        public string AllAppointments { get; set; }

        public string AllCompanies { get; set; }

        public string AllGroups { get; set; }

        public string AllExamAreas { get; set; }
    }
}