using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Olimp.Profile.ViewModels
{
    using Core.ComponentModel;
    using Domain.LearningCenter.Entities;
    using System.ComponentModel.DataAnnotations;

    public class ProfileFormViewModel
    {
        public Profile CurrentProfile { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.AdditionalEvent), DisplayNameResourceName = "AdditionalEvents")]
        public string AdditionalEvents { get; set; }

        public bool ExamAreasAvailable { get; set; }

        public string Appointments { get; set; }

        public string Companies { get; set; }

        public string Groups { get; set; }

        public string ExamAreasString { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.Config), DisplayNameResourceName = "AreaAutoFill")]
        public bool AreaAutoFill { get; set; }

        public IEnumerable<string> SelectedExamAreas { get; set; }

        public string AreaCodeToMaterialCodeMap { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.Profile), DisplayNameResourceName = "Periodicity")]
        [AnyTypeRegEx("^[0-9]+$", ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "NumberInvalid")]
        [Range(0, 2400, ErrorMessageResourceType = typeof(I18N.Profile), ErrorMessageResourceName = "PeriodicityTooLarge")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public virtual int Periodicity { get; set; }

        public IEnumerable<SelectListItem> ExamAreas { get; set; }

        public string Materials { get; set; }
    }
}