using System;
using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Catalogue;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Exam.NHibernate.Catalogue;
using Olimp.Profile.Controllers;
using PAPI.Core;
using PAPI.Core.Initialization;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Profile.Plugin))]

namespace Olimp.Profile
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(ManageProfileController),
                typeof(AdditionalEventController),
                typeof(ExamSettingsController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization)) return;
            
            var materialsModule = PContext.ExecutingModule as OlimpMaterialsModule;
            if (materialsModule == null)
                throw new InvalidOperationException("Executing module is not OlimpMaterialsModule");

            var nodeData = materialsModule.Context.NodeData;

            var connFactoryFactory = new PerHttpRequestConnectionFactoryFactory(
                new SqliteConnectionFactoryFactory());
            var connFactory = connFactoryFactory.Create();

            var examAreasListProvider = new CachedExamAreasListProvider(
                new XmlExamAreasListProvider(OlimpPaths.Default.ExamAreasListFile));

            var materialCodesWithExamAreasProvider = new MaterialCodesWithExamAreasProvider(connFactory);

            var branchesProvider = new NHibernateProfileBranchesProvider(
                PContext.ExecutingModule.Database.RepositoryFactory,
                PContext.ExecutingModule.Database.UnitOfWorkFactory);

            var personalCatalogueProvider = new OrmLitePersonalCatalogueProvider(
                nodeData,
                connFactory,
                new DefaultCatalogueTreeBuilder(),
                branchesProvider);

            binder.Controller<ManageProfileController>(b => b
                  .Bind<IExamAreasListProvider>(() => examAreasListProvider)
                  .Bind<IMaterialCodesWithExamAreasProvider>(() => materialCodesWithExamAreasProvider)
                  .Bind<IPersonalCatalogueProvider>(() => personalCatalogueProvider));
        }
    }
}