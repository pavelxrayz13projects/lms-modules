﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<ExamSettingsViewModel>"  MasterPageFile="~/Views/Shared/Profile.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Olimp_Profile_Js" runat="server">
    <% Html.EnableClientValidation(); %>

    <script type="text/javascript">	
        var defaultExamSettings = { Id: 0, Name: '' },
            examSettingsMapping = {
                create: function (o) { return o.data; },
                key: function (o) { return ko.utils.unwrapObservable(o.Id) }
            },
            viewModel = {	        
                examSettings: ko.mapping.fromJS(<%= Model.ExamSettingsString %>, examSettingsMapping),                
                currentExamSettings: ko.observable(),
                currentEditFormExamSettings: ko.observable(defaultExamSettings),
                deleteExamSettings: function () { 
                    if (this.currentExamSettings() && confirm('Текущие настройки экзамена будут удалены. Продолжить?'))
                        $('#delete-exam-settings-form').submit() 
                },
                addExamSettings: function () {
                    this.currentEditFormExamSettings(defaultExamSettings);

                    $('#exam-settings-edit-dialog').olimpsyncformdialog('open');
                },
                renameExamSettings: function () {
                    if (!this.currentExamSettings())
                        return;

                    this.currentEditFormExamSettings(this.currentExamSettings());

                    $('#exam-settings-edit-dialog').olimpsyncformdialog('open');
                }	        
            },
            currentExamSettingsIndex = viewModel.examSettings.mappedIndexOf({ Id: <%= Model.ExamSettingsId %> });
    
        viewModel.currentExamSettings(viewModel.examSettings()[currentExamSettingsIndex]);
        viewModel.currentExamSettingsId = ko.computed(function() {
            var current = this.currentExamSettings();
            return current ? current.Id : 0;
        }, viewModel);

        viewModel.renameExamSettingsClass = ko.computed(function() {
            return this.currentExamSettings() ? 'icon icon-pencil-active' : 'icon icon-pencil-inactive';
        }, viewModel)	    

        viewModel.deleteExamSettingsClass = ko.computed(function() {
            return this.currentExamSettings() ? 'icon icon-delete-active' : 'icon icon-delete-inactive';
        }, viewModel)
                
        ko.computed(function() {
            $('#exam-settings-block').css('display', this.currentExamSettings() ? 'block' : 'none');
        }, viewModel);
                
        $(function () {
            ko.applyBindings(viewModel, $('#exam-settings-block')[0]);
            viewModel.currentExamSettingsId.subscribe(function() {
                $('#switch-exam-settings-form').submit()
            })

            $('#exam-settings-edit-dialog').olimpsyncformdialog({ title: 'Настройки экзамена' })
            $('#current-exam-settings').combobox();
        });
    </script>	
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Olimp_Profile_Content" runat="server">
    <h1>Выбор настроек экзамена</h1>

    <div id="exam-settings-block">
        <% using (Html.BeginForm("DeleteExamSettings", "ExamSettings", FormMethod.Post, new { id="delete-exam-settings-form" })) { %>
            <%= Html.HiddenFor(m => m.ExamSettingsId, new Dictionary<string, object> { { "data-bind", "value: currentExamSettingsId" } }) %>
        <% } %>

        <% using (Html.BeginForm("Index", "ExamSettings", FormMethod.Get, new { id="switch-exam-settings-form", @class="block" })) { %>
            <%= Html.HiddenFor(m => m.ExamSettingsId, new Dictionary<string, object> { { "data-bind", "value: currentExamSettingsId" } }) %>

            <table class="form-table" style="margin-bottom: 6px;">
                <tr class="first last">		
                    <th><label>Выберите настройки экзамена:</label></th>
                    <td>
                        <table style="border-collapse: collapse; width: 100%;">
                            <tr>
                                <td>
                                    <select id="current-exam-settings" data-bind="
                                        options: examSettings, 
                                        optionsText: 'Name',                                         
                                        value: currentExamSettings">
                                    </select>
                                </td>
                                <td style="width: 1px;">
                                    <a data-bind="click: addExamSettings"
                                        href="#" 
                                        class="action icon icon-add" 
                                        style="margin-left:10px;" 
                                        title="Добавить новые  настройки экзамена">
                                    </a>
                                </td>
                                <td style="width: 1px;">
                                    <a data-bind="click: renameExamSettings, attr: { 'class': renameExamSettingsClass }"
                                        href="#" 
                                        style="margin-left:10px;" 
                                        title="Переименовать настройки экзамена">
                                    </a>
                                </td>
                                <td style="width: 1px;">
                                    <a data-bind="click: deleteExamSettings, attr: { 'class': deleteExamSettingsClass }"
                                        href="#" 
                                        style="margin-left:10px;" 
                                        title="Удалить настройки экзамена">
                                    </a>
                                </td>
                            </tr>
                        </table>					
                    </td>
                </tr>						
            </table>		
        <% } %>

        <div id ="exam-settings-edit-dialog" style="display:none;">
            <% Html.RenderPartial("EditExamSettings"); %>
        </div>  
    </div>          
        
    <% if (Model.ExamSettings != null) { %>	
        <div id="exam-settings-manage-block">        
            <% using (Html.BeginForm("EditExamSettings", "ExamSettings", FormMethod.Post, new { id = "exam-settings-form" })) { %>			
                <div id="exam-settings-params">	
                    <% Html.RenderPartial("ExamSettingsForm", new ExamSettingsFormViewModel
                    {
                        ExamSettings = Model.ExamSettings,
                        ExamTimeLimit = Model.ExamTimeLimit,
                        QuestionTimeLimit = Model.QuestionTimeLimit
                    }); %>		
                </div>			
            <% } %>        
        </div>
    <% } %>
</asp:Content>