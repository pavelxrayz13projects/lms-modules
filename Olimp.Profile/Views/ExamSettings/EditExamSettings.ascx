﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<ExamSettingsViewModel>" %>

<% using (Html.BeginForm("Save", "ExamSettings"))
   { %>
	<%= Html.HiddenFor(m => m.ExamSettingsId, new Dictionary<string, object> {
		{ "data-bind", "value: currentEditFormExamSettings().Id" }
	})%>

	<table class="form-table">
		<tr class="first last">
			<th><%= Html.LabelFor(m => m.ExamSettings.Name)%></th>
			<td>
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ExamSettings.Name) %></div>
				<%= Html.TextBoxFor(m => m.ExamSettings.Name, new Dictionary<string, object> {
					{ "data-bind", "value: currentEditFormExamSettings().Name" }
				})%>
			</td>
		</tr>
	</table>
<% } %>