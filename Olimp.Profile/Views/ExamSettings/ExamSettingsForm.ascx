﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<ExamSettingsFormViewModel>" %>

<%= Html.Hidden("id", Model.ExamSettings.Id) %>
<%= Html.HiddenFor(m => m.ExamSettings.Name) %>

<h1>Ответственный за экзамен</h1>
<div class="block">
    <table class="form-table">	
        <tr class="first last">
            <th><%= Html.LabelFor(m => m.ExamSettings.ResponsibleForExam) %></th>
            <td>
                <div><%= Html.TextBoxFor(m => m.ExamSettings.ResponsibleForExam) %></div>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ExamSettings.ResponsibleForExam) %></div>
            </td>
        </tr>
    </table>
</div>	

<h1>Режим временного ограничения на экзамен</h1>
<div id="time-mode" class="block">
    <table class="form-table">
        <tr data-bind="attr: { 'class': examTimeRestrictionClass }">
            <th><%= Html.LabelFor(m => m.ExamSettings.ExamTimeRestriction) %></th>
            <td>
                <%= Html.DropDownListFor(m => m.ExamSettings.ExamTimeRestriction, new Dictionary<string, object> {
                    { "data-bind", "value: examTimeRestriction" }
                }) %>
            </td>
        </tr>
        <tr class="last" data-bind="visible: showTimeLimit">
            <th><%= Html.LabelFor(m => m.ExamTimeLimit) %></th>
            <td>
                <div style="display: none" data-bind="visible: !showQuestionLimit()">
                    <div><%= Html.TextBoxFor(m => m.ExamTimeLimit) %></div>
                    <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ExamTimeLimit) %></div>
                </div>
                
                <div style="display: none" data-bind="visible: showQuestionLimit">
                    <div><%= Html.TextBoxFor(m => m.QuestionTimeLimit) %></div>
                    <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.QuestionTimeLimit) %></div>
                </div>
            </td>
        </tr>
    </table>
</div>			
    
<h1>Настройки интерфейса</h1>
<div class="block">
    <table class="form-table">	
        <tr class="first">
            <th><%= Html.LabelFor(m => m.ExamSettings.AllowNavigateQuestions) %></th>
            <td class="checkbox"><%= Html.CheckBoxFor(m => m.ExamSettings.AllowNavigateQuestions) %></td>
        </tr>
        <tr class="last">
            <th><%= Html.LabelFor(m => m.ExamSettings.RandomAnswersOrder) %></th>
            <td class="checkbox"><%= Html.CheckBoxFor(m => m.ExamSettings.RandomAnswersOrder) %></td>
        </tr>
    </table>
</div>	
<button type="submit"><%= Olimp.I18N.PlatformShared.Save %></button>

<script type="text/javascript">
    $(function () {
        var viewModel = {
            examTimeRestriction: ko.observable('<%= Model.ExamSettings.ExamTimeRestriction %>')
        };
        viewModel.showTimeLimit = ko.computed(function () {
            return this.examTimeRestriction() != '<%= ExamTimeLimit.NoLimit %>';
        }, viewModel);
        viewModel.showQuestionLimit = ko.computed(function() {
            return this.examTimeRestriction() == '<%= ExamTimeLimit.TimeForQuestion %>';
        }, viewModel);
        viewModel.examTimeRestrictionClass = ko.computed(function () {
            return this.showTimeLimit() ? 'first' : 'first last';
        }, viewModel);

        ko.applyBindings(viewModel, $('#time-mode')[0]);
        $('button').olimpbutton();
    });
</script>


