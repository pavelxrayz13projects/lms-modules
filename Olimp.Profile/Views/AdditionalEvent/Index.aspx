﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage"  MasterPageFile="~/Views/Shared/Profile.master" %>
<%@ Import Namespace="I18N=Olimp.Profile.I18N" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Olimp_Profile_Content" runat="server">
	
	<h1><%= I18N.AdditionalEvent.AdditionalEvents %></h1>		

	<div id="edit-dialog" style="overflow:hidden;">	
		<% Html.RenderPartial("Edit"); %>
	</div>
	
	<div class="block">	
		<% Html.Table(
			new TableOptions {
                SelectUrl = Url.Action("GetAdditionalEvents"),
				Adding = new AddingOptions { 
					Text = Olimp.I18N.PlatformShared.Add,					
					New = new { Id = 0, Name = "" }
				},		
				Columns = new[] { 
					new ColumnOptions("Name", Olimp.I18N.Domain.AdditionalEvent.Name) },
				Actions = new RowAction[] { 
					new EditAction("edit-dialog", new DialogOptions { Title = I18N.AdditionalEvent.Create, Width = "620px" }),
					new DeleteAction(Url.Action("Delete"), I18N.AdditionalEvent.ConfirmDelete)	}
			}); %>
	</div>
	
</asp:Content>
