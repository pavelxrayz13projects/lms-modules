﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<AdditionalEventEditViewModel>" %>

<% Html.EnableClientValidation(); %>
	
<% using(Html.BeginForm("Edit", "AdditionalEvent")) { %>
	
	<%= Html.HiddenFor(m => m.AdditionalEvent.Id, new Dictionary<string, object> { 
		{ "data-bind", "value: Id" } }) %>
	
	<table class="form-table">
		<tr class="first last">
			<th><%= Html.LabelFor(m => m.AdditionalEvent.Name) %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.AdditionalEvent.Name) %></div>
				<%= Html.TextBoxFor(m => m.AdditionalEvent.Name, new Dictionary<string, object> { 
					{ "data-bind", "value: Name" } }) %>
			</td>
		</tr>
	</table>
<% } %>			


	