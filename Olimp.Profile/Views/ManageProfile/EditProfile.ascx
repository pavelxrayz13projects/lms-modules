﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<ManageProfileViewModel>" %>

<% using(Html.BeginForm("EditProfile", "ManageProfile")) { %>
	<%= Html.HiddenFor(m => m.ProfileId)%>

	<table class="form-table">
		<tr class="first last">
			<th><%= Html.LabelFor(m => m.Profile.Name)%></th>
			<td>
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Profile.Name) %></div>
				<%= Html.TextBoxFor(m => m.Profile.Name, new Dictionary<string, object> {{ "data-bind", "value: newProfile().Name" }
				})%>
			</td>
		</tr>
	</table>
<% } %>