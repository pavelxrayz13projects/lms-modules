﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<ProfileFormViewModel>" %>
<%@ Import Namespace="I18N=Olimp.Profile.I18N" %>

<%= Html.Hidden("id", Model.CurrentProfile.Id) %>
<%= Html.HiddenFor(m => m.CurrentProfile.Name) %>
<h1>Настройки профиля</h1>
<div class="block">
    <table class="form-table">
        <tr class="first">
            <th><%= Html.LabelFor(m => m.CurrentProfile.AlternativeName) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.CurrentProfile.AlternativeName) %></div>
                <div><%= Html.TextBoxFor(m => m.CurrentProfile.AlternativeName) %></div>
            </td>
        </tr>

        <% if (Model.ExamAreasAvailable) { %>
        <tr>
            <th style="white-space: normal; width: 200px"><%= Html.LabelFor(m => m.AreaAutoFill) %></th>
            <td  class="checkbox"><%= Html.CheckBoxFor(m => m.AreaAutoFill, new{ id = "area-autofill-checkbox"}) %></td>
        </tr>
        <tr id="exam-areas-block">
            <th>
                <span><%= Html.LabelFor(m => m.CurrentProfile.ExamArea) %>
                    (<span id="exam-areas-count"></span>)
                </span>
            </th>
            <td>
                <div><%= Html.DropDownListFor(m => m.SelectedExamAreas, Model.ExamAreas, new { id = "exam-areas-list", multiple = "multiple"})%></div>
            </td>
        </tr>
        <% } %>
        <tr>
            <th><%= Html.LabelFor(m => m.Periodicity) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Periodicity) %></div>
                <div><%= Html.TextBoxFor(m => m.Periodicity) %></div>
            </td>
        </tr>
        <tr class="last">
            <th style="vertical-align: top; padding-top: 6px;"><%= Html.LabelFor(m => m.AdditionalEvents) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.AdditionalEvents) %></div>
                <div><%= Html.TextBoxFor(m => m.AdditionalEvents, new Dictionary<string, object> { 
                        { "id", "additional-events" }}) %></div>
            </td>
        </tr>
    </table>
</div>		
<h1><%= I18N.Profile.SelectCourses %></h1>
<div style="margin-bottom: 65px;" class="block">
    <% Html.Catalogue(
        new CatalogueOptions {
            SelectUrl = Url.Action("GetCourses", new { profileId = Model.CurrentProfile.Id }),
            CheckBoxesName = "Materials",
            CheckBoxesValue = "guid",
            InitialExpanded = false,
            TemplateEngine = TemplateEngines.DoT,
            SelectedMaterials = Model.Materials });
    %>
</div>	

<div class="button-float-container">
    <button type="submit"><%= Olimp.I18N.PlatformShared.Save %></button>
</div>

<script type="text/javascript">
    $(function () {
        <% if (Model.ExamAreasAvailable)
           { %>
        var areaCodeToMaterialCodeMap = $.parseJSON('<%= Model.AreaCodeToMaterialCodeMap %>'),
            examAreasList = $('#exam-areas-list');

        examAreasList.multiselect({
            createOnEnter: false,
            separator: "#",
            validateAndSendToServerWhenIsDisabled: true,
            menuSourceComparer: function (a, b) {
                var aa = String(a.optionValue).split("."),
                    bb = String(b.optionValue).split("."),
                    minimal = Math.min(aa.length, bb.length);

                for (var i = 0; i < minimal; ++i) {
                    var valA = parseInt(aa[i]) || aa[i].toLowerCase(),
                        valB = parseInt(bb[i]) || bb[i].toLowerCase();

                    if (valA < valB) {
                        return -1;
                    } else if (valA > valB) {
                        return 1;
                    }
                }
                return 0;
            }
        });

        var autofillCheckbox = $("#area-autofill-checkbox"),
            examAreasMultiselect = examAreasList.data("multiselect");

        if (autofillCheckbox.prop("checked")) {
            examAreasMultiselect._setOption("disabled", true);
        }

        function setTextIfListIsEmpty() {
            var ulContainer = $("#exam-areas-block .olimp-multiselect-list-container");
            ulContainer.find(".ul-container-text").remove();
            if (ulContainer.find("ul>li").length == 0) {
                ulContainer.prepend($("<span>").addClass("ul-container-text").text("<%=I18N.Profile.NoSelectedElements%>"));
            }
        }

        setTextIfListIsEmpty();

        function getAreaCodeForMaterialCode(materialCode, branchId) {
            if (areaCodeToMaterialCodeMap.hasOwnProperty(materialCode)) {
                var areas = areaCodeToMaterialCodeMap[materialCode];
                for (var i = 0; i < areas.length; i++) {
                    if (areas[i].BranchId == branchId)
                        return areas[i].Area;
                }
            }

            return null;
        }

        function addListItemForSelectedMaterial(material) {
            var areaCode = getAreaCodeForMaterialCode(material.code, material.id),
                checked = !!material.selected && material.selected(),
                areaLabel = examAreasMultiselect.getElementOptionTextByValue(areaCode);

            if (!areaCode & !areaLabel) {
                return;
            }
            var item = {
                label: areaLabel,
                optionValue: areaCode
            };
            if (checked) {
                examAreasMultiselect.createListElementAndRemoveFromSource(item);
            } else {
                examAreasMultiselect.deleteListElementForSelect(item);
            }
        }

        autofillCheckbox.click(function () {
            if (autofillCheckbox.prop("checked")) {
                examAreasMultiselect.removeAllSelectedOptions();
                examAreasMultiselect._setOption("disabled", true);
                $.each(ko.dataFor($("#Materials")[0]).getSelectedMaterials(),function(i, element) {
                    addListItemForSelectedMaterial(element);
                });
            } else {
                examAreasMultiselect._setOption("disabled", false);
            }
        });

        $("#Materials").on("click material-checkbox-changed", ".checker", function (event) {
            if (!autofillCheckbox.prop("checked")) {
                return;
            }
            addListItemForSelectedMaterial(ko.dataFor(event.target));
        });

        function setExamAreasCount() {
            var ulContainerChildCount = $("#exam-areas-block .olimp-multiselect-list").children().length;
            $("#exam-areas-count").text(ulContainerChildCount);
        }

        setExamAreasCount();

        $("#exam-areas-block").on("multiselect-elements-changed", ".olimp-multiselect-list", setTextIfListIsEmpty);
        $("#exam-areas-block").on("multiselect-elements-changed", ".olimp-multiselect-list", setExamAreasCount);
        <% } %>

        $('button').olimpbutton();
        $("#additional-events").multiselect({
            url: '<%= Url.Action("LookupAdditionalEvent") %>'
        });
    });
</script>


