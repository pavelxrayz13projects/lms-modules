﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<ManageProfileViewModel>"  MasterPageFile="~/Views/Shared/Profile.master" %>
<%@ Import Namespace="I18N=Olimp.Profile.I18N" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Olimp_Profile_Css" runat="server">		
</asp:Content>	
<asp:Content ID="Content2" ContentPlaceHolderID="Olimp_Profile_Js" runat="server">
    <% Html.EnableClientValidation(); %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Olimp_Profile_Content" runat="server">
    <style>
        #main {
            position: absolute;
            width: 100%;
            bottom: 0;
            top: 140px;
            padding: 0;
        }

        #profile-block {
            padding: 20px 20px 0 18px; 
            height: 100%;
            box-sizing: border-box; 
            min-width: 700px
        }

        #profile-block input { width: 100% !important }

        #profile-block td.checkbox input { width: auto !important }

        .left-content {
            float: left; 
            width: 293px; 
            height: 100%
        }

        .right-content {
            float: right; 
            width: 316px; 
            height: 100%
        }

        #select-courses {
            font-size: inherit; 
            text-decoration: none !important;
        }

        #select-courses span {
            white-space: NOWRAP; 
            margin-left: 20px;
        }
    </style>
    
    <div id="profile-block">
        <div class="left-content">
            <table class="form-table" style="height: 100%">
                <tr class="last" style="height: 30px">
                    <th><h1><%= I18N.Profile.Select %></h1></th>
                </tr>
                <tr style="height: 30px; border-top: solid 1px #e7e7e7"> 
                    <td>
                        <!-- TODO: wrap this as control -->
                        <table class="search-box-wrapper">
                            <tbody>
                                <tr>
                                    <td>
                                        <input data-bind="value: searchingProfileFilter, valueUpdate:'afterkeydown'"/>
                                    </td>
                                    <td class="search-box-button-wrap">
                                        <div data-bind="click: manuallySearchProfiles" class="icon icon-search search-box-button-icon"></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr class="last" style="height: 30px">
                    <td style="width: 1px;">
                         <a data-bind="click: openEditProfileDialog" href="#" class="action icon icon-add icon-with-label">
                             <span><%= I18N.Profile.AddProfile %></span>
                         </a>
                     </td>
                </tr> 
                <tr class="last"> 
                     <td style="vertical-align: top;">
                         <ul id="profiles-list" class="list" data-bind="foreach: profiles" style="overflow: hidden; overflow-y: scroll; height: 90%; margin: 0">
                            <li data-bind="click: $parent.currentProfile, css: { 'selected-profile': $data === $parent.currentProfile() }">
                                <span data-bind="text: Name"></span>
                            </li>
                         </ul>
                     </td>
                </tr>
            </table>
            
            <div id ="profile-edit-dialog" style="display:none;">
                <% Html.RenderPartial("EditProfile"); %>
            </div>
        </div>
       
        <div class="right-content">
            <table class="form-table" style=" margin-top: 35px;">
                <tr class="last">
                    <td class="profile-dependecy-entities-buttons">
                        <button data-bind="click: selectExamAreasButton, css: { 'selected-button': isSelectedExamAreasButton }" style="margin-right: 3px">
                            <span class="icon-profile profile-examareas"></span>
                        </button>
                        <button data-bind="click: selectOrganizationsButton, css: { 'selected-button': isSelectedOrganizationsButton }" style="margin-right: 3px">
                            <span class="icon-profile profile-organizations"></span>
                        </button>
                        <button data-bind="click: selectGroupsButton, css: { 'selected-button': isSelectedGroupsButton }" style="margin-right: 3px">
                            <span class="icon-profile profile-groups"></span>
                        </button>
                        <button data-bind="click: selectAppointmentsButton, css: { 'selected-button': isSelectedAppointmentsButton }" style="margin-right: 3px">
                            <span class="icon-profile profile-appointments"></span>
                        </button>
                        <button data-bind="click: selectAdditionalEventsButton, css: { 'selected-button': isSelectedAdditionalEventsButton }">
                            <span class="icon-profile profile-additional-events"></span>
                        </button>
                    </td>
                </tr>
                <tr>
                    <th><h1 data-bind="text: dependencyEntityLabel"></h1></th>
                </tr>
            </table>

            <div data-bind="visible: isSelectedAppointmentsButton">
                <div style="margin-top: 12px; font-size: 13px">
                    <input id="appointments-list"/>
                </div>
            </div>

            <div data-bind="visible: isSelectedOrganizationsButton">
                <div style="margin-top: 12px; font-size: 13px">
                    <input id="companies-list"/>
                </div>
            </div>
            
            <div data-bind="visible: isSelectedExamAreasButton">
                <div style="margin-top: 12px; font-size: 13px">
                    <input id="examareas-list"/>
                </div>
            </div>

            <div data-bind="visible: isSelectedGroupsButton">
                <div style="margin-top: 12px; font-size: 13px">
                    <input id="groups-list"/>
                </div>
            </div>

            <div data-bind="visible: isSelectedAdditionalEventsButton">
                <div style="margin-top: 12px; font-size: 13px">
                    <input id="additional-events"/>
                </div>
            </div>
        </div>

        <div style="display: inline-block; width: 46%; margin-left: 18px; margin-right: 18px; min-width: 595px;">
            <div id="profile-settings">
                <table class="form-table">
                    <tr class="last" style="height: 30px">
                        <th><h1><%= I18N.Profile.ProfileSettings %></h1></th>
                    </tr>
                     
                    <tr style="height: 30px; border-top: solid 1px #e7e7e7">
                        <th><%= Html.LabelFor(m => m.Profile.Name) %></th> 
                        <td><input type="text" data-bind="value: currentEditProfile().Name"></td>
                    </tr>
                     
                    <tr style="height: 30px">
                        <th><%= Html.LabelFor(m => m.Profile.AlternativeName) %></th> 
                        <td><input type="text" data-bind="value: currentEditProfile().AlternativeName"></td>
                    </tr>
                     
                    <tr style="height: 30px">
                        <th><%= Html.LabelFor(m => m.Profile.Periodicity) %></th> 
                        <td><input type="text" data-bind="value: currentEditProfile().Periodicity"></td> 
                    </tr>                    
                    
                    <tr style="height: 30px">
                        <th style="white-space: normal; width: 200px"><%= Html.LabelFor(m => m.AreaAutoFill) %></th>
                        <td  class="checkbox"><%= Html.CheckBoxFor(
                            m => m.AreaAutoFill, 
                            new
                            {
                                id = "area-autofill-checkbox",
                                data_bind = "checked: areaAutoFill"
                            
                            }) %></td>
                    </tr>
                </table>
            </div>
 
            <table class="form-table">
                <tr>
                    <th><h1 style="margin-top: 10px; margin-bottom: 1px;"><%= I18N.Profile.SelectedCourses %></h1></th>
                </tr>
                <tr class="last" style="height: 30px">
                    <td style="width: 1px;">
                         <a id="select-courses" href="#" class="action icon icon-pencil-active">
                             <span><%= I18N.Profile.ProfilesListChanges %></span>
                         </a>
                     </td>
                </tr>
                <tr class="last">
                  <td>
                       <% Html.Catalogue(new CatalogueOptions
                          {
                               Id = "courses-tree-display", 
                               CheckBoxesValue = "guid", 
                               DeferredInitializationScriptRendering = true, 
                               ShowCheckBoxes = false, 
                               IsCreateMaterialsTreeViewModelAndApplyBindings = false, 
                               IsRenderTemplateEngine = false
                          }); %>
                  </td>
                </tr>
            </table>
        </div>
        
        <div class="button-float-container" style="padding: 0 0 0 20px">
            <div style="border-top: solid 1px #c7cad3;padding: 10px 0 12px 0;margin-right: 40px;">
                <button data-bind="click: save"><%= Olimp.I18N.PlatformShared.Save %></button>
                <button data-bind="click: remove"><%= Olimp.Profile.I18N.Profile.DeleteProfile %></button>
            </div>
        </div>
    </div>
    
    <div id="tree-dialog">
        <% Html.Catalogue(new CatalogueOptions
           {
                Id = "courses-tree", 
                CheckBoxesValue = "guid", 
                DeferredInitializationScriptRendering = true
           }); %>
    </div>

    <% Html.CatalogueInitializationScript("courses-tree"); %>
    <% Html.CatalogueInitializationScript("courses-tree-display"); %>
	
    <script type="text/javascript">
        function getBy(collection, id) {
            return ko.utils.arrayFirst(collection, function(e) {
                return e.Id === id;
            });
        }

        function isNullOrWhitespace(input) {
            if (input == null) return true;
            return input.replace(/\s/gi, '').length < 1;
        }

        function fillContent(stringToSplit, arrayToFill) {
            $.each(stringToSplit.split('#'), function(i, e) {
                arrayToFill.push({ label: e, optionValue: e });
            });
        };

        function getListMultiselect(propertyOfModel, idOfList, optionalMultiselectArgument) {
            optionalMultiselectArgument = optionalMultiselectArgument || { separator: "#" };
            var groupsList = propertyOfModel;
            var groupsListSelect = $(idOfList);

            $.each(groupsList, function(i, e) {
                groupsListSelect.append($('<option></option>').attr('value', e.optionValue).text(e.label));
            });

            groupsListSelect.multiselect(optionalMultiselectArgument);

            return groupsListSelect.data("multiselect");
        };

        function getListMultiselect(sourceElements, idOfList, optionalMultiselectArgument) {
            optionalMultiselectArgument = optionalMultiselectArgument || { separator: "#" };
            optionalMultiselectArgument.data = sourceElements;
            var groupsListSelect = $(idOfList);

            groupsListSelect.multiselect(optionalMultiselectArgument);

            return groupsListSelect.data("multiselect");
        };

        function Profile() {
            this.Id = 0;
            this.Name = '';
            this.AlternativeName = '';
            this.Periodicity = 0;
            this.AdditionalEvents = '';
            this.Appointments = '';
            this.Companies = '';
            this.Groups = '';
            this.ExamAreas = '';
        }

        $(function() {
            var additionalEventsMultiselect = getListMultiselect(<%= Model.AllAdditionalEvents %>, '#additional-events');
            var appointmentsListMultiselect = getListMultiselect(<%= Model.AllAppointments %>, '#appointments-list');
            var companiesListMultiselect = getListMultiselect(<%= Model.AllCompanies %>, '#companies-list');
            var groupsListMultiselect = getListMultiselect(<%= Model.AllGroups %>, '#groups-list', {
                separator: "#",
                createOnEnter: false
            });
            var allExamAreas = <%= Model.AllExamAreas %>;
            var examAreasListMultiselect = getListMultiselect(allExamAreas, '#examareas-list', {
                separator: "#",
                createOnEnter: false,
                menuSourceComparer: function(a, b) {
                    var aa = String(a.optionValue).split("."),
                        bb = String(b.optionValue).split("."),
                        minimal = 3;

                    for (var i = 0; i < minimal; ++i) {
                        var valA = parseInt(aa[i]) || aa[i].toLowerCase(),
                            valB = parseInt(bb[i]) || bb[i].toLowerCase();

                        if (valA < valB) {
                            return -1;
                        } else if (valA > valB) {
                            return 1;
                        }
                    }
                    return 0;
                }
            });

            var areaCodeToMaterialCodeMap = $.parseJSON('<%= Model.AreaCodeToMaterialCodeMap %>');
            function getAreaCodeForMaterialCode(materialCode, branchId) {
                if (areaCodeToMaterialCodeMap.hasOwnProperty(materialCode)) {
                    var areas = areaCodeToMaterialCodeMap[materialCode];
                    for (var i = 0; i < areas.length; i++) {
                        if (areas[i].BranchId == branchId)
                            return areas[i].Area;
                    }
                }

                return null;
            }

            function addListItemForSelectedMaterial(material, examAreasMultiselect) {
                var areaCode = getAreaCodeForMaterialCode(material.code, material.id),
                    checked = !!material.selected && material.selected(),
                    item = null;
                
                for (var i = 0, len = allExamAreas.length; i < len; i++) {
                    if (allExamAreas[i].optionValue === areaCode) {
                        item = allExamAreas[i];
                        break;
                    }
                }

                if (!areaCode & !item) 
                    return;

                if (checked) {
                    examAreasMultiselect.createSelectedListElementForInput(item);
                } else {
                    examAreasMultiselect.deleteListElementForInput(item);
                }
            }

            var autofillCheckbox = $("#area-autofill-checkbox"),
                materialsElement = $("#courses-tree");

            autofillCheckbox.click(function () {
                if (autofillCheckbox.prop("checked")) {
                    examAreasListMultiselect.removeAllSelectedOptions();
                    examAreasListMultiselect._setOption("disabled", true);
                    $.each(ko.dataFor(materialsElement[0]).getSelectedMaterials(),function(i, element) {
                        addListItemForSelectedMaterial(element, examAreasListMultiselect);
                    });
                } else {
                    examAreasListMultiselect._setOption("disabled", false);
                }
            });
            if (autofillCheckbox.prop("checked")) {
                examAreasListMultiselect._setOption("disabled", true);
            }
            materialsElement.on("click material-checkbox-changed", ".checker", function (event) {
                if (!autofillCheckbox.prop("checked")) {
                    return;
                }
                addListItemForSelectedMaterial(ko.dataFor(event.target), examAreasListMultiselect);
            });

            var profileMapping = {
                    create: function(o) {
                        var e = o.data;
                        e.Name = ko.observable(e.Name);
                        return e;
                    },
                    key: function(o) {
                        return ko.utils.unwrapObservable(o.Id);
                    }
                },
                dependencyEntityLabels = {
                    examAreas: '<%= Olimp.Profile.I18N.Profile.ExamArea %>',
                    organizations: '<%= Olimp.Profile.I18N.Profile.Organization %>',
                    groups: '<%= Olimp.Profile.I18N.Profile.Group %>',
                    appointments: '<%= Olimp.Profile.I18N.Profile.Appointment %>',
                    additionalEvents: '<%= Olimp.Profile.I18N.Profile.AdditionalEvents %>'
                },
                coursesTreeDisplayElement = $('#courses-tree-display'),
                viewModel = new MaterialsTree(coursesTreeDisplayElement, null, { id: 'courses-tree-display', checkboxes: false });

            viewModel.profiles = ko.mapping.fromJS(<%= Model.Profiles %>, profileMapping);
            viewModel.areaAutoFill =  ko.observable(!!(<%= Model.AreaAutoFill ? 1 : 0 %>));
            viewModel.currentProfile = ko.observable();
            viewModel.newProfile = ko.observable(new Profile());
            viewModel.currentEditProfile = ko.observable(new Profile());
            viewModel.isSelectedExamAreasButton = ko.observable(true);
            viewModel.isSelectedOrganizationsButton = ko.observable(false);
            viewModel.isSelectedGroupsButton = ko.observable(false);
            viewModel.isSelectedAppointmentsButton = ko.observable(false);
            viewModel.isSelectedAdditionalEventsButton = ko.observable(false);
            viewModel.dependencyEntityLabel = ko.observable(dependencyEntityLabels.examAreas);
            viewModel.searchingProfileFilter = ko.observable("");

            viewModel.openEditProfileDialog = function() {
                this.newProfile(new Profile());

                $('#profile-edit-dialog').olimpsyncformdialog('open');
            };

            viewModel.save = function() {
                var self = this;
                var currentEditProfile = self.currentEditProfile();

                if (isNullOrWhitespace(currentEditProfile.Name)) {
                    $.Olimp.showError('<%= Olimp.Profile.I18N.Profile.ProfileValidationMessage %>');
                    return;
                }

                var selectedAdditionalEvents = additionalEventsMultiselect.getAllSelectedElementsAsPlainTextWithSeporators();
                var selectedAppointments = appointmentsListMultiselect.getAllSelectedElementsAsPlainTextWithSeporators();
                var selectedCompanies = companiesListMultiselect.getAllSelectedElementsAsPlainTextWithSeporators();
                var selectedGroups = groupsListMultiselect.getAllSelectedElementsAsPlainTextWithSeporators();
                var selectedExamAreas = examAreasListMultiselect.getInputValueAsText();

                var data = {
                    'id': currentEditProfile.Id,
                    'CurrentProfile.Name': currentEditProfile.Name,
                    'CurrentProfile.AlternativeName': currentEditProfile.AlternativeName,
                    'Periodicity': currentEditProfile.Periodicity,
                    'AdditionalEvents': selectedAdditionalEvents,
                    'Materials': currentEditProfile.Courses.join('#'),
                    'Companies': selectedCompanies,
                    'Groups': selectedGroups,
                    'ExamAreasString': selectedExamAreas,
                    'AreaAutoFill': self.areaAutoFill()
                };

                $.post('<%= Url.Action("Save", new {controller = "ManageProfile"}) %>', data, function(responce) {
                    if (responce.successes) {
                        var profile = getBy(self.profiles(), currentEditProfile.Id);

                        profile.Name(currentEditProfile.Name);
                        profile.AlternativeName = currentEditProfile.AlternativeName;
                        profile.Periodicity = currentEditProfile.Periodicity;
                        profile.Courses = currentEditProfile.Courses;
                        profile.AdditionalEvents = [];
                        profile.Appointments = [];
                        profile.Companies = [];
                        profile.ExamAreas = [];
                        profile.Groups = [];

                        fillContent(selectedAdditionalEvents, profile.AdditionalEvents);
                        fillContent(selectedAppointments, profile.Appointments);
                        fillContent(selectedCompanies, profile.Companies);
                        fillContent(selectedExamAreas, profile.ExamAreas);
                        fillContent(selectedGroups, profile.Groups);

                        $.Olimp.showSuccess(responce.message);
                    } else {
                        $.Olimp.showError(responce.message);
                    }
                }).fail(function(error) {
                    $.Olimp.showError(error);
                });
            };

            viewModel.remove = function() {
                if (!confirm('<%= Olimp.Profile.I18N.Profile.ConfirmationMessageToDeleteProfile %>'))
                    return;

                var self = this,
                    currentProfile = self.currentProfile();

                if (!currentProfile) {
                    return;
                }

                $.post('<%= Url.Action("DeleteProfile", new {controller = "ManageProfile"}) %>', { ProfileId: currentProfile.Id }, function(responce) {
                    if (responce.successes) {
                        viewModel.profiles.remove(getBy(self.profiles(), currentProfile.Id));
                        viewModel.currentEditProfile(new Profile());

                        additionalEventsMultiselect.removeAllSelectedOptions();
                        appointmentsListMultiselect.removeAllSelectedOptions();
                        companiesListMultiselect.removeAllSelectedOptions();
                        groupsListMultiselect.removeAllSelectedOptions();
                        examAreasListMultiselect.removeAllSelectedOptions();

                        viewModel.selected([]);
                        viewModel.updateTree({ nodes: [] });

                        $.Olimp.showSuccess(responce.message);
                    } else {
                        $.Olimp.showError(responce.message);
                    }
                }).fail(function(error) {
                    $.Olimp.showError(error);
                });
            };

            viewModel.selectExamAreasButton = function() {
                this.isSelectedExamAreasButton(true);
                this.dependencyEntityLabel(dependencyEntityLabels.examAreas);

                this.isSelectedOrganizationsButton(false);
                this.isSelectedGroupsButton(false);
                this.isSelectedAppointmentsButton(false);
                this.isSelectedAdditionalEventsButton(false);
            };

            viewModel.selectOrganizationsButton = function() {
                this.isSelectedOrganizationsButton(true);
                this.dependencyEntityLabel(dependencyEntityLabels.organizations);

                this.isSelectedExamAreasButton(false);
                this.isSelectedGroupsButton(false);
                this.isSelectedAppointmentsButton(false);
                this.isSelectedAdditionalEventsButton(false);
            };

            viewModel.selectGroupsButton = function() {
                this.isSelectedGroupsButton(true);
                this.dependencyEntityLabel(dependencyEntityLabels.groups);

                this.isSelectedExamAreasButton(false);
                this.isSelectedOrganizationsButton(false);
                this.isSelectedAppointmentsButton(false);
                this.isSelectedAdditionalEventsButton(false);
            };

            viewModel.selectAppointmentsButton = function() {
                this.isSelectedAppointmentsButton(true);
                this.dependencyEntityLabel(dependencyEntityLabels.appointments);

                this.isSelectedExamAreasButton(false);
                this.isSelectedOrganizationsButton(false);
                this.isSelectedGroupsButton(false);
                this.isSelectedAdditionalEventsButton(false);
            };

            viewModel.selectAdditionalEventsButton = function() {
                this.isSelectedAdditionalEventsButton(true);
                this.dependencyEntityLabel(dependencyEntityLabels.additionalEvents);

                this.isSelectedExamAreasButton(false);
                this.isSelectedOrganizationsButton(false);
                this.isSelectedGroupsButton(false);
                this.isSelectedAppointmentsButton(false);
            };

            viewModel.searchProfiles = function(str) {
                var actualProfilesList = $("#profiles-list").find("li span").get();
                if (typeof str === "object")
                    str = '';

                $.each(actualProfilesList, function(i, e) {
                    var entry = $(e),
                        newText = entry.text().replace("<span>", '').replace("</span>", '');

                    entry.html(newText);

                    var startIndexOfSubstring = entry.text().toLowerCase().indexOf(str.toLowerCase()),
                        isVisible = startIndexOfSubstring >= 0;

                    if (!isVisible) {
                        entry.parent().css("display", "none");
                    } else {
                        entry.parent().removeAttr("style");
                    }

                    if (str && isVisible) {
                        var originalText = entry.text().substr(startIndexOfSubstring, str.length);
                        newText = entry.text().replace(originalText, "<span class='search-selection'>" + originalText + "</span>");
                        entry.html(newText);
                    }
                });
            };

            viewModel.manuallySearchProfiles = function() {
                this.searchProfiles(this.searchingProfileFilter());
            };

            coursesTreeDisplayElement.data('viewModel', viewModel);

            var p = ko.utils.arrayFirst(viewModel.profiles(), function(e) {
                return e.Id === <%= Model.ProfileId %>;
            });

            if (p) {
                currentProfileIndex = p.Id;
            } else {
                currentProfileIndex = 0;
            }

            viewModel.currentProfileId = ko.computed(function() {
                var current = this.currentProfile();
                return current ? current.Id : 0;
            }, viewModel);

            viewModel.renameProfileClass = ko.computed(function() {
                return this.currentProfile() ? 'icon icon-pencil-active' : 'icon icon-pencil-inactive';
            }, viewModel);

            viewModel.deleteProfileClass = ko.computed(function() {
                return this.currentProfile() ? 'icon icon-delete-active' : 'icon icon-delete-inactive';
            }, viewModel);

            ko.computed(function() {
                $('#profile-block').css('display', this.currentProfile() ? 'block' : 'none');
            }, viewModel);

            ko.applyBindings(viewModel, $('#profile-block')[0]);

            viewModel.currentProfileId.subscribe(function() {
                var e = viewModel.currentProfile();

                viewModel.currentEditProfile({
                    Id: e.Id,
                    Name: e.Name(),
                    AlternativeName: e.AlternativeName,
                    Periodicity: !!e.Periodicity ? e.Periodicity : 0,
                    Appointments: e.Appointments,
                    Companies: e.Companies,
                    Courses: e.Courses,
                    Groups: e.Groups,
                    ExamAreas: e.ExamAreas
                });

                additionalEventsMultiselect.clear();

                if (e.AdditionalEvents && e.AdditionalEvents != '') {
                    $.each(e.AdditionalEvents, function(i, item) {
                        additionalEventsMultiselect.createSelectedListElementForInput(item);
                    });
                }

                appointmentsListMultiselect.clear();

                if (e.Appointments && e.Appointments != '') {
                    $.each(e.Appointments, function(i, item) {
                        appointmentsListMultiselect.createSelectedListElementForInput(item);
                    });
                }

                companiesListMultiselect.clear();

                if (e.Companies && e.Companies != '') {
                    $.each(e.Companies, function(i, item) {
                        companiesListMultiselect.createSelectedListElementForInput(item);
                    });
                }

                groupsListMultiselect.clear();

                if (e.Groups && e.Groups != '') {
                    $.each(e.Groups, function(i, item) {
                        groupsListMultiselect.createSelectedListElementForInput(item);
                    });
                }

                examAreasListMultiselect.clear();

                if (e.ExamAreas && e.ExamAreas != '') {
                    $.each(e.ExamAreas, function(i, item) {
                        examAreasListMultiselect.createSelectedListElementForInput(item);
                    });
                }

                var data = { 'materials': e.Courses ? e.Courses.join('#') : '' };

                $.post('<%= Url.Action("GetTree", new {controller = "ManageProfile"}) %>', data, function(responce) {
                    viewModel.selected(e.Courses);
                    viewModel.updateTree(responce);
                });

                $('#select-courses').click(function() {
                    if (viewModel.currentEditProfile().Id === 0)
                        return;

                    $.post('/Admin/Info/GetCourse', function(json) {
                        var treeModel = $('#courses-tree').data('viewModel'),
                            e = viewModel.currentEditProfile();

                        treeModel.selected(e.Courses ? e.Courses : []);
                        treeModel.updateTree(json);

                        $('#tree-dialog').olimpdialog('open');
                    }, 'json')
                });

                $('#tree-dialog').olimpdialog({
                    autoOpen: false,
                    draggable: false,
                    resizable: false,
                    modal: true,
                    width: '70%',
                    fixedHeight: 0.7,
                    title: 'Выбор курсов',
                    buttons: {
                        'Выбрать': function() {
                            var self = this,
                                materials = $('#courses-tree').data('viewModel').getSelectedMaterials(),
                                profile = viewModel.currentEditProfile();

                            profile.Courses = $.map(materials, function(e) {
                                return e.guid;
                            });

                            var data = { 'materials': profile.Courses ? profile.Courses.join('#') : '' };

                            $.post('<%= Url.Action("GetTree", new {controller = "ManageProfile"}) %>', data, function(responce) {
                                viewModel.selected(profile.Courses);
                                viewModel.updateTree(responce);
                                $(self).olimpdialog('close');
                            });
                        },
                        'Отмена': function() {
                            $(this).olimpdialog('close');
                        }
                    }
                });
            });

            viewModel.currentEditProfile.subscribe(function() {
                var buttons = $('.button-float-container :button'),
                    inputs = $("#profile-settings :input");

                if (viewModel.currentEditProfile().Id === 0) {
                    inputs.attr("disabled", true);
                    buttons.attr("disabled", true);
                    buttons.addClass('ui-olimpbutton-disabled ui-state-disabled');
                } else {
                    inputs.attr("disabled", false);
                    buttons.attr("disabled", false);
                    buttons.removeClass('ui-olimpbutton-disabled ui-state-disabled');
                }
            });

            viewModel.currentProfile(viewModel.profiles()[currentProfileIndex]);

            viewModel.searchingProfileFilter.subscribe(function(newValue) {
                viewModel.searchProfiles(newValue);
            });

            $('.button-float-container :button').olimpbutton();
            $('.left-content :button').olimpbutton();

            $('#profile-edit-dialog').olimpsyncformdialog({
                title: 'Профиль',
                buttons: {
                    'Сохранить': function() {
                        var newProfile = viewModel.newProfile();

                        if (isNullOrWhitespace(newProfile.Name)) {
                            $.Olimp.showError('<%= Olimp.Profile.I18N.Profile.ProfileValidationMessage %>');
                            return;
                        }

                        var data = { 'Profile.Name': newProfile.Name };

                        $.post('<%= Url.Action("EditProfile", new {controller = "ManageProfile"}) %>', data, function(responce) {
                            if (responce.successes) {
                                responce.profile.Name = ko.observable(responce.profile.Name);
                                viewModel.profiles.splice(0, 0, responce.profile);
                                viewModel.newProfile(new Profile());
                                $.Olimp.showSuccess(responce.message);
                            } else {
                                $.Olimp.showError(responce.message);
                            }
                        }).fail(function(error) {
                            $.Olimp.showError(error);
                        });
                    }
                }
            });
        });
    </script>
</asp:Content>