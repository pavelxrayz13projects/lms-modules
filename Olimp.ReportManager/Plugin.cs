using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.ReportManager.Controllers;
using PAPI.Core;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.ReportManager.Plugin))]

namespace Olimp.ReportManager
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        { }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(ReportManagerController),
                typeof(ProtocolManagerController));
        }
    }
}