﻿(function($, undefined) {
    function onSelectChanged (table) {
        var id = $(this).val(),
            download = table.find('.inplace-download'),
            del = table.find('.inplace-delete');
            
        if (id && id.length)
            download.attr('href', '/Admin/ReportManager/DownloadInPlace/' + id)
                .parent().show();
        else {
            del.parent().hide();
            download.parent().hide();
        }
    }

    $.widget("ui.inplacereportmanager", {

        options: {
            allowManage: false,
            uploadDialogContent: '',
            uploadDialogTitle: '',
            uploadButtonText: '',
            cancelButtonText: '',
            confirmDeleteText: '',
            deleteUrl: ''
        },

        _dialogOptions: {
            autoOpen: false, 
            draggable: false, 
            resizable: false, 
            modal: true,
            width: '580px',
            beforeClose: function () { return $(this).data('in-progress') !== true },
            close: function () {
                $(this).parent().find('.olimp-dialog-buttonpane button')
                    .olimpbutton('option', 'disabled', false)
            },
            buttons: {}
        },

        _create: function() {
            var self = this,
                table = this.element.find('.inplace-wrapper');

            var select = table.find('select').combobox()
                .change(function () { onSelectChanged.apply(this, [table]) });
            onSelectChanged.apply(select[0], [ table ]);

            if (this.options.allowManage) {
                var uploadDialog = $('<div />').html(this.options.uploadDialogContent).hide().appendTo('body'),
                    form = uploadDialog.find('form'),
                    iframe = uploadDialog.find('iframe');

                $.validator.unobtrusive.parse(uploadDialog[0])

                this._dialogOptions.title = this.options.uploadDialogTitle;
                this._dialogOptions.buttons[this.options.uploadButtonText] = function () {
                    if (!form.valid())
                        return false;

                    uploadDialog.data('in-progress', true)
                        .parent().find('.olimp-dialog-buttonpane button')
                        .olimpbutton('option', 'disabled', true)

                    form.submit(); 
                };
                this._dialogOptions.buttons[this.options.cancelButtonText] = function () { uploadDialog.olimpdialog('close') };

                uploadDialog.olimpdialog(this._dialogOptions);

                iframe.load(function () {
                    var doc = $.browser.msie
                            ? document.frames[iframe[0].name].document
                            : iframe[0].contentDocument,
                        content = doc.body.innerText || doc.body.textContent,
                        text;

                    try {
                        text = $.parseJSON(content);
                    } catch (e) {                        
                        $.Olimp.showError(content);
                    }

                    if (text) {
                        if (text.status == "success") {
                            select.combobox('prependOption', { label: text.report.name, value: text.report.id })
                                .combobox('select', text.report.id)
                        } else if (text.status && text.status.errorMessage) {
                            $.Olimp.showError(text.status.errorMessage);
                        }
                    }

                    uploadDialog.data('in-progress', false);
                    uploadDialog.olimpdialog('close');
                })

                table.find('.inplace-upload').click(function () {
                    form[0].reset();
                    uploadDialog.olimpdialog('open')
                    return false;
                })

                table.find('.inplace-delete').click(function () {
                    if (!confirm(self.options.confirmDeleteText))
                        return false;

                    var id = select.val();
                    $.post(self.options.deleteUrl, { id: id }, function () {
                        select.combobox('removeOption', id);
                    });
                    return false;
                })
            }
        },

        destroy: function() {		
            $.Widget.prototype.destroy.call(this);
        }
    });

})(jQuery);