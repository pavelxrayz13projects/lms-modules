﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<InPlaceViewModel>" %>
<%@ Import Namespace="I18N=Olimp.ReportManager.I18N" %>

<% var id = String.IsNullOrWhiteSpace(Model.Id) ? "inplace-" + Guid.NewGuid().ToString() : Model.Id; %>

<div id="<%= id %>">
    <table class="inplace-wrapper" style="width: 100%; border-collapse: collapse; border: 0;">
        <tr>
            <td>
                <%= Html.DropDownListFor(m => m.ReportId, Model.ReportTemplates) %>
            </td>
            <td style="width: 20px;">
                <a title="Выгрузить шаблон" style="float:right;" class="icon icon-sheet-small inplace-download"></a>
            </td>
            <% if (Model.AllowManage) { %>
            <td style="width: 20px;">
                <a title="Загрузить шаблон" style="float:right;" href="#" class="action icon icon-add inplace-upload"></a>
            </td>
            <td style="width: 20px;">
                <a title="Удалить" href="#" style="float:right;" class="action icon icon-delete-active inplace-delete"></a>
            </td>
            <% } %>
        </tr>
    </table>
</div>

<% Platform.RenderExtensions("/olimp/report-manager/in-place/js"); %>
<script>
    $(function () {
        $('#<%= id %>').inplacereportmanager({
            allowManage: <%= Model.AllowManage.ToString().ToLower() %>,
            uploadDialogContent:            
                <% if (Model.AllowManage) { %>
                    '<%= Html.Action("Upload", new {type = Model.Type}).ToString().Replace("\n", "") %>'
                <% } else { %>
                    ''
                <% } %>,
            uploadDialogTitle: '<%= I18N.Templates.UploadTitle %>',
            confirmDeleteText: '<%= I18N.Templates.ConfirmDelete %>',
            deleteUrl: '<%= Url.Action("DeleteInPlace") %>',
            uploadButtonText: '<%= Olimp.I18N.PlatformShared.Save %>',
            cancelButtonText: '<%= Olimp.I18N.PlatformShared.Cancel %>'
        });
    })
</script>

