﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<TemplateUploadViewModel>" %>

<% Html.EnableClientValidation(); %>

<% using(Html.BeginForm("Upload", (string) ViewContext.RouteData.Values["controller"], FormMethod.Post, new { 
	id= "template-xml-from", target = "template-xml-fake-frame", enctype = "multipart/form-data"  })) { %>
	
    <input type="hidden" name="X-Requested-With" value="IFrame" />
	
	<table class="form-table">
		<tr class="first">
			<th><%= Html.LabelFor(m => m.Template.Name) %></th>
			<td>
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Template.Name) %></div>
				<div><%= Html.TextBoxFor(m => m.Template.Name) %></div>
				
			</td>
		</tr>
		<tr class="last">
			<th><%= Html.LabelFor(m => m.XmlFile) %></th>
			<td>
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.XmlFile) %></div>
				<div><%= Html.TextBoxFor(m => m.XmlFile, new { type="file" }) %></div>
			</td>
		</tr>
	</table>

<% } %>

<iframe id="template-xml-fake-frame" name="template-xml-fake-frame" style="display: none"></iframe>