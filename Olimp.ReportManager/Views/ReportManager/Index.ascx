﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<TemplatesListViewModel>" %>
<%@ Import Namespace="I18N=Olimp.ReportManager.I18N" %>

<h1><%= Templates.Header %></h1>

<script type="text/html" id="download-action">
	<a class="icon icon-sheet-small action" title="Выгрузить шаблон" data-bind="
		attr: { href: '<%= Url.Action("Download") %>/' + $data.Id() }">
	</a>
</script>

<% var nameColumnOptions = new ColumnOptions("Name", Olimp.I18N.Domain.Report.Name); %>
<% if (Model.Type == Olimp.Reporting.ReportType.Report) { %>
	<% nameColumnOptions.TemplateId = "template-name"; %>		
	
	<script type="text/javascript">
	function generateReport() {
		var div = $('<div />').appendTo('body'), self = this;
			
		div.load('<%= Url.Action("Model", "ReportGenerator") %>', 'reportId=' + self.Guid(), function(text, status) {			
			if (status == 'error')
				return;
	
			div.olimpdialog({ 
				title: self.Name(),
				draggable: false, 
				resizable: false, 
				modal: true,
				width: '700px',
				buttons: {
					'<%= I18N.Templates.Generate %>': function() { div.find('form:first').submit() },
					'<%= Olimp.I18N.PlatformShared.Cancel %>': function() { div.olimpdialog('close') }
				},
				close: function() { div.olimpdialog('destroy').remove() }	
			});
		})
	}
	</script>
		
	<script type="text/html" id="template-name">
		<a href="#" data-bind="text: Name, click: generateReport"></a>
	</script>	
<% } %>

<% if (Model.AllowManage) { %>
<div id="upload-dialog">
	<% Html.RenderAction("Upload", new { type = Model.Type }); %>	
</div>	
<% } %>
		
<div class="block">	
	<% Html.Table(
		new TableOptions {
			SelectUrl = Url.Action("GetTemplates", new { type = Model.Type }),
			DefaultSortColumn = "Name",
			PagingDefaultSize = 20,
			Columns = new[] { nameColumnOptions },
			TableActions = new TableAction[] { 
				new FileUploadAction("upload-dialog", "template-xml-fake-frame", Templates.Add,
					new DialogOptions { Title = I18N.Templates.UploadTitle, Width = "580px" }) { Disabled = !Model.AllowManage } 
			},
			Actions = new RowAction[] { 
				new TemplateAction("download-action"),
				new DeleteAction(Url.Action("Delete"), Templates.ConfirmDelete) { Disabled = !Model.AllowManage }	}
		}); %>
</div>
