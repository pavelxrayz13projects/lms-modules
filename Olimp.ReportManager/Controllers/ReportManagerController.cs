using Olimp.Domain.Catalogue.Security;
using Olimp.Reporting;
using Olimp.Web.Controllers.Reporting;
using Olimp.Web.Controllers.Security;

namespace Olimp.ReportManager.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    public class ReportManagerController : ReportManagerControllerBase
    {
        protected override ReportType ReportType { get { return Reporting.ReportType.Report; } }

        public override string AllowManagePermission { get { return Permissions.Reports.ManageReportTemplates; } }
    }
}