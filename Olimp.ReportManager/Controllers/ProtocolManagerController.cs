﻿using Olimp.Domain.Catalogue.Security;
using Olimp.Reporting;
using Olimp.Web.Controllers.Reporting;
using Olimp.Web.Controllers.Security;

namespace Olimp.ReportManager.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    public class ProtocolManagerController : ReportManagerControllerBase
    {
        protected override ReportType ReportType { get { return Reporting.ReportType.Protocol; } }

        public override string AllowManagePermission { get { return Permissions.Reports.ManageProtocolTemplates; } }
    }
}