using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.ReportGenerator.Controllers;
using PAPI.Core;
using PAPI.Core.Initialization;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.ReportGenerator.Plugin))]

namespace Olimp.ReportGenerator
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(ReportGeneratorController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            base.SetupKernel(binder);

            var rootKernel = PContext.ExecutingModule.Kernel.Resolver;
            binder.Controller<ReportGeneratorController>(b => b
                .Bind<Olimp.Reporting.IReportEngine>((() => rootKernel.Resolve<Olimp.Reporting.IReportEngine>())));
        }
    }
}