using System.Linq;
using System.Security;
using Olimp.Core.Web;
using Olimp.Domain.Catalogue.Security;
using Olimp.ReportGenerator.ViewModels;
using Olimp.Reporting;
using Olimp.Web.Controllers.Reporting;
using Olimp.Web.Controllers.Security;
using System;
using System.Web.Mvc;

namespace Olimp.ReportGenerator.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Reports.Generate + "," + Permissions.Reports.ManageReportTemplates, Order = 2)]
    public class ReportGeneratorController : Olimp.Web.Controllers.Reporting.ReportGeneratorController
    {
        public ReportGeneratorController(IReportEngine engine)
            : base(engine)
        {
        }

        [HttpGet]
        public ActionResult Model(Guid reportId)
        {
            var currentUserPermissions = RolesHelper.GetCurrentUserRoles(ControllerContext);
            if (!currentUserPermissions.Contains(Permissions.Reports.Generate))
                throw new SecurityException(Olimp.Web.Controllers.I18N.Auth.UserNotInRoleAuthError);

            if (!HasModel(reportId))
                return RedirectToAction("Generate", new { reportId = reportId });

            return PartialView(new ReportGeneratorViewModel { Guid = reportId });
        }

        [HttpPost]
        public ActionResult Generate(
            Guid reportId,
            [ReportModel(GuidParam = "reportId")] object model)
        {
            var extension = Request.Browser.Browser == "Chrome"
                ? ".xls"
                : ".xml";

            Generate(reportId, model, Response.OutputStream);
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=report" + extension);

            return null;
        }
    }
}