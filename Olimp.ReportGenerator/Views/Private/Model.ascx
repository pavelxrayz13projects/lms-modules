﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<ReportGeneratorViewModel>" %>

<% Html.EnableClientValidation(); %>
	
<% using (Html.BeginForm(
	"Generate", 
	"ReportGenerator", 
	FormMethod.Post, 
	new { id = String.Format("Report-{0}", Model.Guid) })) { %>
	
	<%= Html.Hidden("reportId", Model.Guid) %>
	<% Html.RenderAction("ReportModel", new { guid = Model.Guid }); %>

<% } %>	
