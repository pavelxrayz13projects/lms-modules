﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<SplashViewModel>" MasterPageFile="~/Views/Shared/Popup.master" %>
<%@ Import Namespace="Olimp.I18N.Core" %>
<%@ Import Namespace="I18N=Olimp.Splash.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Head" runat="server">
    <title>Олимп</title>
    <meta name="olimp.distribution-id"	content="<%: Model.DistributionId %>" />
    <meta name="olimp.distribution-owner" content="<%: Model.DistributionOwner %>" />
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Css" runat="server">
    <% Platform.RenderExtensions("/olimp/splash/layout/css"); %>
    <style>
    .ui-autocomplete .ui-menu-item a { font-weight: normal; font-family: Arial; font-size: 16px; }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Body" runat="server">
    <div id="header">
        <div id="node-chooser">
            <div id="current-node" data-bind="click: openNodesMenu" title = "<%= I18N.Splash.ChangeNode %>">
                <table>
                    <tr>
                        <td><div class="splash-icon place-pointer-icon"></div></td>
                        <td id="current-node-text"><div data-bind="text: selectedNode() ? selectedNode().Text : ''"></div></td>
                        <td><div class="splash-icon show-nodes-icon"></div></td>
                    </tr>
                </table>
            </div>
            <a  href="<%= Url.Action("Index", new { area="Admin", controller="Settings" }) %>" 
                title = "<%= I18N.Splash.SystemManagement %>" 
                class="splash-icon settings-icon" 
                data-bind="click: linkRedirect"></a>
        </div>
        <div id="nodes-menu">
            <div id="nodes-list" data-bind="foreach: nodes">
                <div class="node-element" data-bind="text: Text, click: $root.selectNode,css: { selected: Id == ($root.selectedNode() ? $root.selectedNode().Id : '') }"></div>
            </div>
            <div id="nodes-menu-closer" data-bind="click: closeNodesMenu" title = "<%= I18N.Splash.Close %>">
                <table>
                    <tr>
                        <td><div class="splash-icon close-icon"></div></td>
                        <td class="close-text"><%= I18N.Splash.Close %></td>
                    </tr>
                </table>
            </div>
                
        </div>
    </div>

    <div id="cluster-splash-factoy">
        <div class="picture"></div>
    </div>

    <center id="cluster-splash-wrapper">
        <div class="system-logo">
            <img class="logo" src="<%= Url.Content("~/Content/Images/big-logo.png") %>" alt="<%= Olimp.I18N.Shared.Title %>" />
        </div>

        <div id="nodes-menu-wrapper" class="cluster-menu">
            <% Html.RenderAction("SectionsList", new { format = "html" }); %>
        </div>
    </center>

    <div id="overlay" class="olimp-overlay"></div>

    <script>
        $(function () {
            function viewModel() {
                var self = this,
                    nodesMapping = {
                        create: function(o) { return o.data; },
                        key: function(o) { return ko.utils.unwrapObservable(o.Id); }
                    };
                self.nodes = ko.mapping.fromJS(<%= Model.Nodes %>, nodesMapping);
                self.selectedNode = ko.observable(<%= Model.SelectedNode %>);
                self.selectNode = function(data) {
                    self.selectedNode(data);
                    var action = '<%= Url.Action("SectionsList", "Splash", new { format = "jsonp-html" })%>&callback=?',
                        url = data.Url + action,
                        wrapper = $('#nodes-menu-wrapper'),
                        timeout = setTimeout(function() { $.Olimp.showError('<%= I18N.Splash.NodeHasntRespond %>'); }, 5000);

                    $.getJSON(url, function(html) {
                        clearTimeout(timeout);
                        wrapper.html(html);
                    });
                };
                self.openNodesMenu = function() {
                    var nodesMenu = $('#nodes-menu');
                    if (!nodesMenu.is(":visible")) {
                        $("#overlay").show();
                        nodesMenu.show();
                        $('#current-node').addClass('cursor-default');
                        $('.settings-icon').addClass('cursor-default');
                        $("#current-node .show-nodes-icon").addClass('selected');
                    }
                };
                self.closeNodesMenu = function() {
                    $('#nodes-menu').hide(600);
                    $('#current-node').removeClass('cursor-default');
                    $('.settings-icon').removeClass('cursor-default');
                    $("#current-node .show-nodes-icon").removeClass('selected');
                    $("#overlay").hide();
                };
                self.linkRedirect = function(item, event) {
                    var link = $(event.target);
                    if (!$('#nodes-menu').is(":visible") && $(event.target).is("a")) {
                        var nodeurl = self.selectedNode() ? self.selectedNode().Url.slice(0, -1) : '';
                        window.location.href = nodeurl + link.attr("href");
                    }
                };
            }
            ko.applyBindings(new viewModel(), $('#header')[0]);
        })
    </script>
</asp:Content>