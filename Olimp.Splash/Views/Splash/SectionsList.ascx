﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SectionsListViewModel>" %>

<div id="nodes-menu-container">
    <% foreach(var s in Model.Sections) { %>
        <% var icon = !String.IsNullOrWhiteSpace(s.IconClass) ? s.IconClass : "section-icon-none"; %>

        <a href="<%= s.Url %>" class="node-section-link" title="<%= s.Text %>">
            <div class="node-section-icon <%= icon %>"></div>
            <div class="node-section-text"><%= s.Text.ToLower() %></div>
        </a>
    <% } %>
    <div style="clear:both;"></div>
</div>