using Olimp.Configuration;
using Olimp.Core.Extensibility;
using Olimp.Domain.LearningCenter;
using Olimp.Splash.Models;
using Olimp.UI;
using PAPI.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;

namespace Olimp.Splash
{
    public class ModulesList : ExtensionContainer<DictionaryExtension>, IRenderable
    {
        private PropertyResolver _propertyResolver;

        public ModulesList(PropertyResolver propertyResolver)
        {
            _propertyResolver = propertyResolver ?? new PropertyResolver();
        }

        public ModulesList()
            : this(null)
        { }

        public override DictionaryExtension CreateExtension(string extensionType, IModule module)
        {
            return new DictionaryExtension(module);
        }

        protected virtual RouteValueDictionary GetRoutes(DictionaryExtension item)
        {
            var routes = new RouteValueDictionary();
            foreach (var p in item.Parameters)
            {
                if (p.Key.Equals("text", StringComparison.InvariantCultureIgnoreCase) ||
                    p.Key.Equals("if", StringComparison.InvariantCultureIgnoreCase) ||
                    p.Key.Equals("icon", StringComparison.InvariantCultureIgnoreCase) ||
                    p.Key.Equals("priority", StringComparison.InvariantCultureIgnoreCase))
                    continue;

                routes.Add(p.Key, p.Value);
            }

            return routes;
        }

        protected virtual bool CheckConstraint(DictionaryExtension item)
        {
            string condition;

            if (!item.Parameters.TryGetValue("if", out condition))
                return true;

            var configProperty = typeof(OlimpConfiguration).GetProperty(condition,
                BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);

            if (configProperty != null)
            {
                if (configProperty.PropertyType != typeof(bool))
                    return true;

                var config = Config.Get<OlimpConfiguration>();
                return (bool)configProperty.GetValue(config, null);
            }

            return true;
        }

        protected virtual int GetPriority(DictionaryExtension item)
        {
            string priorityString;
            int priority;
            if (!item.Parameters.TryGetValue("priority", out priorityString) || !int.TryParse(priorityString, out priority))
                return 0;

            return priority;
        }

        public IEnumerable<Section> GetOrderedSections(Func<string, string, string, RouteValueDictionary, string, Section> sectionFactory)
        {
            foreach (var item in this.Items.OrderBy(p => this.GetPriority(p)))
            {
                if (!this.CheckConstraint(item))
                    continue;

                var text = item.Parameters["text"];
                _propertyResolver.TryResolveI18NProperty(ref text, item.Extender);

                var action = item.Parameters["action"];
                var controller = item.Parameters["controller"];

                string iconClass;
                if (!item.Parameters.TryGetValue("icon", out iconClass))
                    iconClass = null;

                var route = this.GetRoutes(item);

                yield return sectionFactory.Invoke(text, action, controller, route, iconClass);
            }
        }

        #region IRenderable implementation

        public virtual void Render(ViewContext context, IViewDataContainer viewDataContainer, RouteCollection routeCollection, IDictionary<string, object> args)
        {
            object sectionFactoryObject;
            if (!args.TryGetValue("SectionFactory", out sectionFactoryObject))
                throw new InvalidOperationException("SectionFactory atgument was not specified");

            var sectionFactory = sectionFactoryObject as Func<string, string, string, RouteValueDictionary, string, Section>;
            if (sectionFactory == null)
                throw new InvalidOperationException("SectionFactory atgument is invalid");

            var helper = new HtmlHelper(context, viewDataContainer, routeCollection);

            context.Writer.Write("<div id=\"areas\">");

            foreach (var s in this.GetOrderedSections(sectionFactory))
            {
                context.Writer.Write("<div class=\"area\">");
                context.Writer.Write(@"<a href=""{1}"">{0}</a>", s.Text, s.Url);
                context.Writer.Write("</div>");
            }

            context.Writer.Write("</div>");
        }

        #endregion IRenderable implementation
    }
}