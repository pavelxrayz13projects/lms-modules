﻿namespace Olimp.Splash.Models
{
    public class Section
    {
        public string Url { get; set; }

        public string Text { get; set; }

        public string IconClass { get; set; }
    }
}