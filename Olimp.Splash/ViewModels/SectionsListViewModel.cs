﻿using Olimp.Splash.Models;
using System.Collections.Generic;

namespace Olimp.Splash.ViewModels
{
    public class SectionsListViewModel
    {
        public IList<Section> Sections { get; set; }
    }
}