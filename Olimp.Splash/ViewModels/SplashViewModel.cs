using Olimp.Splash.Models;
using System;
using System.Web.Routing;

namespace Olimp.Splash.ViewModels
{
    public class SplashViewModel
    {
        public bool GodMode { get; set; }

        public bool InternalFeaturesMode { get; set; }

        public string DistributionId { get; set; }

        public string DistributionOwner { get; set; }

        public string Nodes { get; set; }

        public string SelectedNode { get; set; }

        public Func<string, string, string, RouteValueDictionary, string, Section> SectionFactory { get; set; }
    }
}