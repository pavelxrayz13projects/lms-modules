using Olimp.Core;
using Olimp.Core.Extensibility;
using Olimp.Core.Routing;
using Olimp.Domain.LearningCenter;
using Olimp.LocalNodes.Core.Cluster;
using Olimp.LocalNodes.Core.Services.Clients;
using Olimp.Splash.Controllers;
using Olimp.Web.Controllers.Nodes;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.Splash.Plugin))]

namespace Olimp.Splash
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["*"].RegisterControllers(typeof(SplashController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization)) return;

            var olimpModule = PContext.ExecutingModule as LearningCenterModule;
            if (olimpModule == null)
                throw new InvalidOperationException("Olimp.Main only compatible with LearningCenterModule");

            var extender = olimpModule.Plugins.GetExtender();
            var nodeData = olimpModule.Context.NodeData;

            var olimpLinkProvider = new DefaultOlimpLinkProvider(nodeData, olimpModule.LocalCluster.GetStartupRegistry());
            var localNodeController = new WebLocalNodeControllerServiceClient(OlimpPaths.OlimpClusterServiceUri);

            binder.Controller<SplashController>(b => b
                  .Bind<NodeData>(() => nodeData)
                  .Bind<IExtender>(() => extender)
                  .Bind<ILocalNodeController>(() => localNodeController)
                  .Bind<IOlimpLinkProvider>(() => olimpLinkProvider));
        }
    }
}