using Olimp.Core;
using Olimp.Core.Extensibility;
using Olimp.Domain.Common.Nodes;
using Olimp.LocalNodes.Core.Cluster;
using Olimp.Splash.Models;
using Olimp.Splash.ViewModels;
using Olimp.Web.Controllers.Nodes;
using Olimp.Web.Controllers.Users;
using PAPI.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using ServiceStack.Text;

namespace Olimp.Splash.Controllers
{
    public class SplashController : AuthCookieManagerUserController
    {
        private readonly NodeData _currentNode;
        private readonly IExtender _extender;
        private readonly ILocalNodeController _nodeController;
        private readonly IOlimpLinkProvider _olimpLinkProvider;

        private string _customHostName;
        private UrlHelper _urlHelper;
        private Uri _hostUri;

        public SplashController(
            NodeData currentNode,
            IExtender extender,
            ILocalNodeController nodeController,
            IOlimpLinkProvider olimpLinkProvider)
        {
            if (currentNode == null)
                throw new ArgumentNullException("currentNode");

            if (extender == null)
                throw new ArgumentNullException("extender");

            if (nodeController == null)
                throw new ArgumentNullException("nodeRegistry");

            if (olimpLinkProvider == null)
                throw new ArgumentNullException("olimpLinkProvider");

            _currentNode = currentNode;
            _extender = extender;
            _nodeController = nodeController;
            _olimpLinkProvider = olimpLinkProvider;
        }

        [NonAction]
        private string GetNodeText(Node node)
        {
            if (Node.IsDefaultNode(node))
                return I18N.Splash.DefaultNodeTitle;

            var activatable = node as IActivatableNode;
            if (activatable != null && !String.IsNullOrWhiteSpace(activatable.DistributionLabel))
                return activatable.DistributionLabel;

            return node.Name;
        }

        private Section CreateSection(string text, string action, string controller, RouteValueDictionary values = null, string iconClass = null)
        {
            _urlHelper = _urlHelper ?? new UrlHelper(this.ControllerContext.RequestContext);
            _hostUri = _hostUri ?? new Uri(_olimpLinkProvider.Get(Request.Url.Host), UriKind.Absolute);

            var relativeUrl = _urlHelper.Action(action, controller, values);

            var link = new Uri(_hostUri, new Uri(relativeUrl, UriKind.Relative));

            return new Section { Text = text, Url = link.ToString(), IconClass = iconClass };
        }

        public ActionResult Index(bool? local)
        {
            var info = PContext.ExecutingModule.Context.DistributionInfoProvider.Get();
            var model = new SplashViewModel
            {
                SectionFactory = this.CreateSection,
                GodMode = OlimpApplication.GodMode,
                InternalFeaturesMode = OlimpApplication.InternalFeaturesMode
            };

            if (info != null)
            {
                model.DistributionId = BitConverter.ToString(info.GetDistributionIdBytes());
                model.DistributionOwner = info.CompanyName;
            }

            var activeNodes = _nodeController.GetAllNodes()
                .Where(n => n.IsActive && n.IsAlive)
                .OrderByDescending(n => Node.IsDefaultNode(n.Node))
                .ThenBy(n => n.Node.Name)
                .Select(n => new {n.Node.Id, Text = GetNodeText(n.Node), Url = Request.GetNodeUrl(n.Node)}).ToList();
            JsConfig.ExcludeTypeInfo = true;

            model.Nodes = JsonSerializer.SerializeToString(activeNodes);
            model.SelectedNode =
                JsonSerializer.SerializeToString(activeNodes.FirstOrDefault(n => n.Id == _currentNode.Id));

            return View("ClusterIndex", model);
        }

        public ActionResult SectionsList(string callback = null, string format = "html")
        {
            var modulesList = _extender.GetContainer("/splash/layout/moduleslist") as ModulesList;

            var sections = modulesList != null
                ? modulesList.GetOrderedSections(this.CreateSection).ToList()
                : new List<Section>();

            var model = new SectionsListViewModel { Sections = sections };

            switch (format)
            {
                case "html": break;
                case "jsonp-html":
                    if (!String.IsNullOrWhiteSpace(callback))
                        return JsonpPartialView(callback, model);
                    break;

                default:
                    throw new InvalidOperationException("Unknown format " + format);
            }

            return PartialView(model);
        }
    }
}