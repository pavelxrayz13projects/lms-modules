﻿using log4net;
using System;

using LogManager = PAPI.Core.Logging.Log;

namespace Olimp.Logic.Converters
{
    internal sealed class ThreadStaticConversionContext : IConversionContext
    {
        private static readonly ILog _log = LogManager.CreateLogger("logic");

        private static ILog Log { get { return _log; } }

        [ThreadStatic]
        private static ThreadStaticConversionContext _current;

        public static ThreadStaticConversionContext Current
        {
            get
            {
                if (_current == null)
                    throw new InvalidOperationException(
                      "ConversionContext has not been initialized. Use one of CreateThreadStaticContext factory methods first.");
                if (_current._isDisposed)
                    throw new InvalidOperationException("ConversionContext is disposed and cannot be used.");
                return _current;
            }
        }

        internal static ThreadStaticConversionContext Create()
        {
            if (_current != null && !_current._isDisposed)
                throw new InvalidOperationException(
                  String.Format("Cannot create ConversionContext - another ConversionContext (Id = {0}) exists and it's not been disposed yet.", _current.Id));

            _current = new ThreadStaticConversionContext();
            return _current;
        }

        private readonly Funq.Container _container;
        private readonly Guid _id;
        private bool _isDisposed = false;

        private ThreadStaticConversionContext()
        {
            _id = Guid.NewGuid();
            _container = new Funq.Container();
            Log.DebugFormat("ConversionContext with Id = {0} has just been created.", Id);
        }

        public Funq.Container Container
        {
            get
            {
                if (_isDisposed)
                    throw new InvalidOperationException("Context is disposed.");

                return _container;
            }
        }

        public Guid Id
        {
            get { return _id; }
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _isDisposed = true;
            if (_container != null)
            {
                _container.Dispose();
                Log.DebugFormat("ConversionContext with Id = {0} has just been disposed.", Id);
            }
        }
    }
}