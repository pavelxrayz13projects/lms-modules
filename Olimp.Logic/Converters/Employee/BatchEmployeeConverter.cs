﻿using PAPI.Core.Data;
using System;
using Contracts = Olimp.Service.Contract.SyncService.Employee;
using Entities = Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Logic.Converters.Employee
{
    public class BatchEmployeeConverter : BatchConverter<Contracts.Employee, Entities.Employee>
    {
        static BatchEmployeeConverter()
        {
            EmployeeMapper.CreateMap();
        }

        public BatchEmployeeConverter(IRepositoryFactory repositoryFactory)
            : base(EmployeeMapper.CreateThreadStaticContext<CompanyConverter, GroupConverter, EmployeeConverter>(repositoryFactory))
        {
            if (repositoryFactory == null)
                throw new ArgumentNullException("repositoryFactory");

            Context.Container.Register<IRepositoryFactory>(repositoryFactory);

            RegisterDebugCounters(Context.Container);
        }

        protected override IConverter<Contracts.Employee, Entities.Employee> GetRootConverter()
        {
            return Context.Container.Resolve<IConverter<Contracts.Employee, Entities.Employee>>();
        }

        protected override void BeforeBatchConvert()
        {
            base.BeforeBatchConvert();
            ClearDebugCounters();
        }

        protected override void AfterBatchConvert()
        {
            base.AfterBatchConvert();
            LogDebugCounters();
        }

        protected static void RegisterDebugCounters(Funq.Container container)
        {
#if DEBUG
            container.Register<IStatisticsCounters>(new Counters());
#endif
        }

        protected void ClearDebugCounters()
        {
#if DEBUG
            var counters = Context.Container.TryResolve<IStatisticsCounters>();
            if (counters != null)
                counters.Clear();
#endif
        }

        protected void LogDebugCounters()
        {
#if DEBUG
            var counters = Context.Container.TryResolve<IStatisticsCounters>();
            if (counters != null)
                Log.Debug(counters.ToString());
#endif
        }
    }
}