﻿using AutoMapper;
using Olimp.Domain.LearningCenter.NotPersisted;
using Olimp.Logic.Properties;
using PAPI.Core.Data;
using System;
using System.Linq;
using Contracts = Olimp.Service.Contract.SyncService.Employee;
using Entities = Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Logic.Converters.Employee
{
    public static class EmployeeMapper
    {
        public static void CreateMap()
        {
            Mapper.CreateMap<Contracts.Employee, Entities.Employee>()
                .ConstructUsing(ConstructEmployee)
                .ForMember(e => e.Profiles, opt => opt.Ignore())
                .ForMember(e => e.KnowledgeControlReason, opt => opt.Ignore())
                .ForMember(e => e.Login, opt => opt.Ignore())
                .ForMember(e => e.Password, opt => opt.Ignore())
                .ForMember(e => e.UILocale, opt => opt.Ignore())
                .ForMember(e => e.Email, opt => opt.Ignore())
                .ForMember(e => e.Appointments, opt => opt.Ignore())
                .ForMember(e => e.DoNotInheritIndirectProfiles, opt => opt.Ignore())
                .ForMember(e => e.GivenName, opt => opt.MapFrom(e => e.ThirdName))
                .ForMember(e => e.Company, opt => opt.ResolveUsing(ResolveAndConvert<Contracts.Employee, Entities.Company>))
                .ForMember(e => e.Group, opt => opt.ResolveUsing(ResolveAndConvert<Contracts.Employee, Entities.Group>))
                .ForMember(e => e.DateCreated, opt => opt.ResolveUsing(e => DateTime.Now))
                .ForMember(e => e.ChangeInProfileEntitiesDate, opt => opt.ResolveUsing(e => DateTime.Now))
                .ForMember(e => e.IsActive, opt => opt.UseValue(true))
                .ForMember(e => e.IsAnonymous, opt => opt.UseValue(EmployeeConverterSettings.Default.CreateAnonymousEmployee))
                //.AfterMap(FetchEmployeeAppointmentsAndFields)
                .AfterMap(AddEmployeeToGroup);
        }

        private static Entities.Employee ConstructEmployee(ResolutionContext ctx)
        {
            var employeeRepo = GetRepository<Entities.Employee, Guid>();

            var source = (Contracts.Employee)ctx.SourceValue;
            var dest = employeeRepo.GetById(source.Id);

            return dest ?? new Entities.Employee
            {
                KnowledgeControlReason = KnowledgeControlReason.Primary
            };
        }

        /*
        private static void FetchEmployeeAppointmentsAndFields(Contracts.Employee source, Entities.Employee dest)
        {
            var positionsToAdd = new HashSet<string>(source.Positions ?? new string[0], StringComparer.OrdinalIgnoreCase);

            foreach (var a in dest.Appointments.ToList())
            {
                if (!positionsToAdd.Contains(a.Name))
                {
                    dest.Appointments.Remove(a);
                    a.Employees.Remove(dest);
                }
                else
                {
                    a.IsPrivate = false;
                    a.IsActive = true;

                    positionsToAdd.Remove(a.Name);
                }
            }

            var positions = positionsToAdd.ToArray();
            var existingAppointments = GetRepository<Entities.Appointment, int>()
                .Where(a => positions.Contains(a.Name));

            foreach (var a in existingAppointments)
            {
                dest.Appointments.Add(a);
                a.Employees.Add(dest);

                a.IsPrivate = false;
                a.IsActive = true;

                positionsToAdd.Remove(a.Name);
            }

            var appointmentsToCreate = positionsToAdd
                .Select(p => new Entities.Appointment { IsActive = true, IsPrivate = false, Name = p });

            foreach (var a in appointmentsToCreate)
            {
                dest.Appointments.Add(a);
                a.Employees.Add(dest);
            }
        }
        */

        private static void AddEmployeeToGroup(Contracts.Employee source, Entities.Employee dest)
        {
            if (dest.Group != null && !dest.Group.Employees.Any(e => e.Equals(dest)))
                dest.Group.Employees.Add(dest);
        }

        #region Methods

        public static IConversionContext CreateThreadStaticContext<TCompanyConverter, TGroupConverter, TEmployeeConverter>(IRepositoryFactory repoFactory)
            where TCompanyConverter : IConverter<Contracts.Employee, Entities.Company>
            where TGroupConverter : IConverter<Contracts.Employee, Entities.Group>
            where TEmployeeConverter : IConverter<Contracts.Employee, Entities.Employee>
        {
            var context = CreateThreadStaticContext(repoFactory);
            context.Container.RegisterAs<TCompanyConverter, IConverter<Contracts.Employee, Entities.Company>>();
            context.Container.RegisterAs<TGroupConverter, IConverter<Contracts.Employee, Entities.Group>>();
            context.Container.RegisterAs<TEmployeeConverter, IConverter<Contracts.Employee, Entities.Employee>>();

            return context;
        }

        public static IConversionContext CreateThreadStaticContext<TCompanyConverter>(
            IRepositoryFactory repoFactory,
            IConverter<Contracts.Employee, Entities.Company> companyConverter,
            IConverter<Contracts.Employee, Entities.Group> groupConverter,
            IConverter<Contracts.Employee, Entities.Employee> employeeConverter)
        {
            var context = CreateThreadStaticContext(repoFactory);
            context.Container.Register<IConverter<Contracts.Employee, Entities.Company>>(companyConverter);
            context.Container.Register<IConverter<Contracts.Employee, Entities.Group>>(groupConverter);
            context.Container.Register<IConverter<Contracts.Employee, Entities.Employee>>(employeeConverter);

            return context;
        }

        private static IConversionContext CreateThreadStaticContext(IRepositoryFactory repoFactory)
        {
            if (repoFactory == null)
                throw new ArgumentNullException("repoFactory");

            var context = ThreadStaticConversionContext.Create();

            context.Container.Register<IRepository<Entities.Employee, Guid>>(repoFactory.CreateFor<Entities.Employee, Guid>());
            context.Container.Register<IRepository<Entities.Appointment, int>>(repoFactory.CreateFor<Entities.Appointment, int>());

            return context;
        }

        internal static IRepository<TEntity, TId> GetRepository<TEntity, TId>()
            where TEntity : class, IEntity<TId>
        {
            return ThreadStaticConversionContext.Current.Container.Resolve<IRepository<TEntity, TId>>();
        }

        internal static TDestination ResolveAndConvert<TSource, TDestination>(TSource source)
        {
            var converter = ThreadStaticConversionContext.Current.Container
                .Resolve<IConverter<TSource, TDestination>>();

            return converter.Convert(source);
        }

        #endregion Methods
    }
}