﻿using PAPI.Core.Data;
using System;
using System.Linq.Expressions;
using Contracts = Olimp.Service.Contract.SyncService.Employee;
using Entities = Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Logic.Converters.Employee
{
    public class CompanyConverter : CachedEntityConverter<Contracts.Employee, string, Entities.Company, int>
    {
        public CompanyConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        {
        }

        public override string ExtractKey(Contracts.Employee source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            if (String.IsNullOrWhiteSpace(source.Company))
                return "Olimp.Company.Null";

            return source.Company;
        }

        protected override Expression<Func<Entities.Company, bool>> GetEntitySearchFilter(Contracts.Employee source)
        {
            if (String.IsNullOrWhiteSpace(source.Company))
                return company => false;

            return company => company.Name == source.Company;
        }

        protected override Entities.Company CreateNewEntity(Contracts.Employee source)
        {
            if (String.IsNullOrWhiteSpace(source.Company))
                return null;

            return new Entities.Company { Name = source.Company };
        }
    }
}