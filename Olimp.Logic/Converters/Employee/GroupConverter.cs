﻿using Olimp.Logic.Properties;
using PAPI.Core.Data;
using System;
using System.Linq.Expressions;
using Contracts = Olimp.Service.Contract.SyncService.Employee;
using Entities = Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Logic.Converters.Employee
{
    public class GroupConverter : CachedEntityConverter<Contracts.Employee, string, Entities.Group, int>
    {
        public GroupConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        {
        }

        public override string ExtractKey(Contracts.Employee source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            if (String.IsNullOrWhiteSpace(source.Group))
                return "Olimp.Group.Null";

            return source.Group;
        }

        protected override Expression<Func<Entities.Group, bool>> GetEntitySearchFilter(Contracts.Employee source)
        {
            if (String.IsNullOrWhiteSpace(source.Group))
                return group => false;

            return group => group.Name == source.Group;
        }

        protected override Entities.Group CreateNewEntity(Contracts.Employee source)
        {
            if (String.IsNullOrWhiteSpace(source.Group))
                return null;

            var now = DateTime.Now;
            var groupRepo = this.CreateRepository();

            return new Entities.Group
            {
                Prefix = GroupPrefixHelper.GetPrefix(groupRepo, source.Group),
                Description = EmployeeConverterSettings.Default.DefaultEmployeeGroupDescription ?? String.Empty,
                ExamBeginDate = now,
                ExamEndDate = now.AddYears(5),
                Name = source.Group,
                IsActive = true,
                ExamSettings = new Entities.ExamSettings
                {
                    Name = source.Group
                }
            };
        }
    }
}