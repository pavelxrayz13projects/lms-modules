﻿using PAPI.Core.Data;
using System;
using System.Linq;
using Contracts = Olimp.Service.Contract.SyncService.Employee;
using Entities = Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Logic.Converters.Employee
{
    public class EmployeeConverter : DomainEntityConverter<Contracts.Employee, Entities.Employee, Guid>
    {
        public EmployeeConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        { }

        protected override void SaveToRepository(Entities.Employee result)
        {
            var repository = this.CreateRepository();

            var id = result.Id;
            var forceSave = !repository.Any(e => e.Id == id);

            repository.Save(result, forceSave);
        }
    }
}