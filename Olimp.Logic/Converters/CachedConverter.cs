﻿using log4net;
using PAPI.Core.Data;
using System;
using System.Collections.Generic;
using LogManager = PAPI.Core.Logging.Log;

namespace Olimp.Logic.Converters
{
    public abstract class CachedEntityConverter<TSource, TSourceKey, TDestination, TDestinatinationKey>
      : DomainEntityConverter<TSource, TDestination, TDestinatinationKey>, IConverter<TSource, TDestination>
      where TDestination : class, IEntity<TDestinatinationKey>
    {
        private static readonly ILog _log = LogManager.CreateLogger("logic");

        private static ILog Log { get { return _log; } }

        private readonly IDictionary<TSourceKey, TDestination> _cache = new Dictionary<TSourceKey, TDestination>();

        private readonly string _cacheHitEntityCounterName;

        public CachedEntityConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        {
            _cacheHitEntityCounterName = GetType().FullName + ".CacheHit";
        }

        public IDictionary<TSourceKey, TDestination> Cache
        {
            get { return _cache; }
        }

        public abstract TSourceKey ExtractKey(TSource source);

        public override TDestination Convert(TSource source)
        {
            try
            {
                if (source == null)
                    return ConvertNull();

                return ConvertCore(source);
            }
            catch (Exception e)
            {
                Log.ErrorFormat("Cannot convert object: {0}", e.ToString());
                throw;
            }
        }

        protected virtual TDestination ConvertCore(TSource source)
        {
            TSourceKey key = ExtractKey(source);
            TDestination result;
            if (Cache.TryGetValue(key, out result))
            {
                IncrementDebugCounter(_cacheHitEntityCounterName);
                return result;
            }

            result = base.Convert(source);
            Cache.Add(key, result);
            return result;
        }

        public virtual TDestination ConvertNull()
        {
            return default(TDestination);
        }
    }
}