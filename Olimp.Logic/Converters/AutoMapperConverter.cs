﻿using AutoMapper;

namespace Olimp.Logic.Converters
{
    public class AutoMapperConverter<TSource, TDestination> : IConverter<TSource, TDestination>
    {
        public virtual TDestination Convert(TSource source)
        {
            return Mapper.Map<TSource, TDestination>(source);
        }
    }
}