﻿using System;

namespace Olimp.Logic.Converters
{
    public interface IConversionContext : IDisposable
    {
        Guid Id { get; }

        Funq.Container Container { get; }
    }
}