﻿namespace Olimp.Logic.Converters
{
    public interface IConverter<TSource, TDestination>
    {
        TDestination Convert(TSource source);
    }
}