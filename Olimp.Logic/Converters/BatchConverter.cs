﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using LogManager = PAPI.Core.Logging.Log;

namespace Olimp.Logic.Converters
{
    public abstract class BatchConverter<TSource, TDestination> : IDisposable
    {
        private static readonly ILog _log = LogManager.CreateLogger("logic");

        protected ILog Log { get { return _log; } }

        private readonly IConversionContext _context;
        private bool _isDisposed = false;

        protected BatchConverter(IConversionContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            _context = context;
        }

        protected IConversionContext Context
        {
            get { return _context; }
        }

        protected IConverter<TSource, TDestination> RootConverter
        {
            get
            {
                var rootConverter = GetRootConverter();
                if (rootConverter == null)
                    throw new InvalidOperationException("Instance GetRootConverter method returned null.");

                return rootConverter;
            }
        }

        protected abstract IConverter<TSource, TDestination> GetRootConverter();

        public virtual IList<TDestination> ConvertAll(IList<TSource> sources)
        {
            if (sources == null)
                throw new ArgumentNullException("sources");

            BeforeBatchConvert();
            var result = sources.Select(i => RootConverter.Convert(i)).ToList();
            AfterBatchConvert();

            return result;
        }

        protected virtual void BeforeBatchConvert()
        {
        }

        protected virtual void AfterBatchConvert()
        {
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _isDisposed = true;
            if (_context != null)
            {
                _context.Dispose();
            }
        }
    }
}