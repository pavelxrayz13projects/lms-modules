﻿using PAPI.Core.Data;
using System;
using System.Linq.Expressions;
using Contracts = Olimp.Service.Contract.SyncService.Attempts;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Logic.Converters.Attempts
{
    public class ExamResponsibleConverter : CachedEntityConverter<Contracts.Attempt, string, Entities.CachedExamResponsibleSchema, int>
    {
        public ExamResponsibleConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        {
        }

        public override string ExtractKey(Contracts.Attempt source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            return source.ExamResponsible;
        }

        protected override Expression<Func<Entities.CachedExamResponsibleSchema, bool>> GetEntitySearchFilter(Contracts.Attempt source)
        {
            return responsible => responsible.FullName == source.ExamResponsible;
        }

        protected override Entities.CachedExamResponsibleSchema CreateNewEntity(Contracts.Attempt source)
        {
            return new Entities.CachedExamResponsibleSchema { FullName = source.ExamResponsible };
        }
    }
}