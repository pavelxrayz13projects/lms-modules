﻿using PAPI.Core.Data;
using System;
using Contracts = Olimp.Service.Contract.SyncService.Attempts;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Logic.Converters.Attempts
{
    public class AttemptConverter : DomainEntityConverter<Contracts.Attempt, Entities.ExamAttemptSchema, Guid>
    {
        private Entities.ForeignNodeSiteSchema _site;

        public AttemptConverter(IRepositoryFactory repositoryFactory, Entities.ForeignNodeSiteSchema site)
            : base(repositoryFactory)
        {
            if (site == null)
                throw new ArgumentNullException("site");

            _site = site;
        }

        protected override bool ForceSaveToRepository { get { return true; } }

        protected override Entities.ExamAttemptSchema CreateNewEntity(Contracts.Attempt source)
        {
            var entity = base.CreateNewEntity(source);
            entity.SiteMapped = _site;

            return entity;
        }
    }
}