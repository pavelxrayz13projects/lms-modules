﻿using PAPI.Core.Data;
using System;
using System.Linq.Expressions;
using Contracts = Olimp.Service.Contract.SyncService.Attempts;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Logic.Converters.Attempts
{
    public class TicketConverter : CachedEntityConverter<Contracts.Attempt, string, Entities.CachedTicketSchema, int>
    {
        public TicketConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        {
        }

        public override string ExtractKey(Contracts.Attempt source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            return String.Format("Olimp.Ticket.{0}.{1}.{2}",
                source.Ticket.Number, source.Ticket.UniqueId, source.Ticket.QuestionsCount);
        }

        protected override Expression<Func<Entities.CachedTicketSchema, bool>> GetEntitySearchFilter(Contracts.Attempt source)
        {
            return ticket => ticket.Number == source.Ticket.Number && ticket.UniqueId == source.Ticket.UniqueId;
        }

        protected override Entities.CachedTicketSchema CreateNewEntity(Contracts.Attempt source)
        {
            return new Entities.CachedTicketSchema
            {
                Number = source.Ticket.Number,
                UniqueId = source.Ticket.UniqueId,
                QuestionsCount = source.Ticket.QuestionsCount
            };
        }
    }
}