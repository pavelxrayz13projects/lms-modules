﻿using PAPI.Core.Data;
using System;
using System.Linq;
using System.Linq.Expressions;
using Contracts = Olimp.Service.Contract.SyncService.Attempts;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Logic.Converters.Attempts
{
    public class QuestionConverter : CachedEntityConverter<Contracts.QuestionTask, string, Entities.CachedQuestionSchema, int>
    {
        public QuestionConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        {
        }

        public override string ExtractKey(Contracts.QuestionTask source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            return String.Format("Olimp.Question({0})", source.Question.UniqueId);
        }

        protected override Expression<Func<Entities.CachedQuestionSchema, bool>> GetEntitySearchFilter(Contracts.QuestionTask source)
        {
            var uniqueId = source.Question.Topic.Course.UniqueId;
            var version = source.Question.Topic.Course.Version;
            var checksum = source.Question.Topic.Course.Checksum;
            var fileName = source.Question.Topic.Course.FileName;

            return question => question.UniqueId == source.Question.UniqueId
                && question.TopicMapped.CourseMapped.UniqueId == uniqueId
                && question.TopicMapped.CourseMapped.Version == version
                && question.TopicMapped.CourseMapped.Checksum == checksum
                && question.TopicMapped.CourseMapped.FileName == fileName;
        }

        protected override Entities.CachedQuestionSchema CreateNewEntity(Contracts.QuestionTask source)
        {
            var topic = AttemptsMapper.ResolveAndConvert<Contracts.Question, Entities.CachedTopicSchema>(source.Question);

            var question = new Entities.CachedQuestionSchema
            {
                AllowMultiple = source.Question.AllowMultiple,
                Text = source.Question.Text,
                UniqueId = source.Question.UniqueId,
                TopicMapped = topic
            };

            question.AnswersMapped = source.Question.Answers.Select(a => new Entities.CachedAnswerSchema
            {
                IsCorrect = a.IsCorrect,
                Number = a.Number,
                Text = a.Text,
                QuestionMapped = question
            }).ToList();

            return question;
        }
    }
}