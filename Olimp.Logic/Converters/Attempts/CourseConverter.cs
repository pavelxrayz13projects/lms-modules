﻿using PAPI.Core.Data;
using System;
using Contracts = Olimp.Service.Contract.SyncService.Attempts;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Logic.Converters.Attempts
{
    public class CourseConverter : CachedEntityConverter<Contracts.Course, string, Entities.CachedCourseSchema, int>
    {
        public CourseConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        {
        }

        public override string ExtractKey(Contracts.Course source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            return String.Format("Olimp.Course({0},{1},{2},{3})",
                source.UniqueId, source.Version, source.Checksum, source.FileName);
        }

        protected override Entities.CachedCourseSchema CreateNewEntity(Contracts.Course source)
        {
            return new Entities.CachedCourseSchema
            {
                Checksum = source.Checksum,
                Code = source.Code,
                FileName = source.FileName,
                IsCommercial = source.IsCommercial,
                MaxErrors = source.MaxErrors,
                Name = source.Name,
                UniqueId = source.UniqueId,
                Version = source.Version
            };
        }

        protected override System.Linq.Expressions.Expression<Func<Entities.CachedCourseSchema, bool>> GetEntitySearchFilter(Contracts.Course source)
        {
            return course => course.UniqueId == source.UniqueId
                && course.Version == source.Version
                && course.Checksum == source.Checksum
                && course.FileName == source.FileName;
        }
    }
}