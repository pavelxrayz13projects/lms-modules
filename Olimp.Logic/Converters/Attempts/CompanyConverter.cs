﻿using PAPI.Core.Data;
using System;
using System.Linq.Expressions;
using Contracts = Olimp.Service.Contract.SyncService.Attempts;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Logic.Converters.Attempts
{
    public class CompanyConverter : CachedEntityConverter<Contracts.Employee, string, Entities.CachedCompanySchema, int>
    {
        public CompanyConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        {
        }

        public override string ExtractKey(Contracts.Employee source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            if (String.IsNullOrWhiteSpace(source.Company))
                return "Olimp.Company.Null";

            return source.Company;
        }

        protected override Expression<Func<Entities.CachedCompanySchema, bool>> GetEntitySearchFilter(Contracts.Employee source)
        {
            if (String.IsNullOrWhiteSpace(source.Company))
                return company => false;

            return company => company.Name == source.Company;
        }

        protected override Entities.CachedCompanySchema CreateNewEntity(Contracts.Employee source)
        {
            if (String.IsNullOrWhiteSpace(source.Company))
                return null;

            return new Entities.CachedCompanySchema { Name = source.Company };
        }
    }
}