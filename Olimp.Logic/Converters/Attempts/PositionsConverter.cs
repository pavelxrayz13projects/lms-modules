﻿using PAPI.Core.Data;
using System;
using System.Linq.Expressions;
using Contracts = Olimp.Service.Contract.SyncService.Attempts;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Logic.Converters.Attempts
{
    public class PositionsConverter : CachedEntityConverter<Contracts.Position, string, Entities.CachedEmployeePositionSchema, int>
    {
        public PositionsConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        {
        }

        public override string ExtractKey(Contracts.Position source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            if (String.IsNullOrWhiteSpace(source.Name))
                return "Olimp.Positions.Null";

            return source.Name;
        }

        protected override Expression<Func<Entities.CachedEmployeePositionSchema, bool>> GetEntitySearchFilter(Contracts.Position source)
        {
            if (String.IsNullOrWhiteSpace(source.Name))
                return positions => false;

            return position => position.Name == source.Name;
        }

        protected override Entities.CachedEmployeePositionSchema CreateNewEntity(Contracts.Position source)
        {
            if (String.IsNullOrWhiteSpace(source.Name))
                return null;

            return new Entities.CachedEmployeePositionSchema { Name = source.Name };
        }
    }
}