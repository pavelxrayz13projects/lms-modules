﻿using PAPI.Core.Data;
using System;
using System.Linq.Expressions;
using Contracts = Olimp.Service.Contract.SyncService.Attempts;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Logic.Converters.Attempts
{
    public class GroupConverter : CachedEntityConverter<Contracts.Attempt, string, Entities.CachedGroupSchema, int>
    {
        public GroupConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        {
        }

        public override string ExtractKey(Contracts.Attempt source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            if (source.Group == null)
                return "Olimp.Group.Null";

            return String.Format("Olimp.Group.{0}.{1}.{2}.{3}",
                source.Group.ExamPeriodBegin, source.Group.ExamPeriodEnd, source.Group.Name, source.Group.Description);
        }

        protected override Expression<Func<Entities.CachedGroupSchema, bool>> GetEntitySearchFilter(Contracts.Attempt source)
        {
            if (source.Group == null)
                return group => false;

            return group => group.Name == source.Group.Name
                && group.Prefix == source.Group.Prefix
                && group.ExamPeriodBegin == source.Group.ExamPeriodBegin
                && group.ExamPeriodEnd == source.Group.ExamPeriodEnd
                && group.Description == source.Group.Description;
        }

        protected override Entities.CachedGroupSchema CreateNewEntity(Contracts.Attempt source)
        {
            if (source.Group == null)
                return null;

            return new Entities.CachedGroupSchema
            {
                Prefix = source.Group.Prefix,
                Name = source.Group.Name,
                Description = source.Group.Description,
                ExamPeriodBegin = source.Group.ExamPeriodBegin,
                ExamPeriodEnd = source.Group.ExamPeriodEnd
            };
        }
    }
}