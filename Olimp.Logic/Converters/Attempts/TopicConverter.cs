﻿using PAPI.Core.Data;
using System;
using System.Linq.Expressions;
using Contracts = Olimp.Service.Contract.SyncService.Attempts;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Logic.Converters.Attempts
{
    public class TopicConverter : CachedEntityConverter<Contracts.Question, string, Entities.CachedTopicSchema, int>
    {
        public TopicConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        {
        }

        public override string ExtractKey(Contracts.Question source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            return String.Format("Olimp.Topic({0},{1})",
                source.Topic.Name, source.Topic.UniqueId);
        }

        protected override Expression<Func<Entities.CachedTopicSchema, bool>> GetEntitySearchFilter(Contracts.Question source)
        {
            return topic => topic.Name == source.Topic.Name && topic.UniqueId == source.Topic.UniqueId;
        }

        protected override Entities.CachedTopicSchema CreateNewEntity(Contracts.Question source)
        {
            var course = AttemptsMapper.ResolveAndConvert<Contracts.Course, Entities.CachedCourseSchema>(source.Topic.Course);

            return new Entities.CachedTopicSchema
            {
                Description = source.Topic.Description,
                DocumentsCount = source.Topic.DocumentsCount,
                Name = source.Topic.Name,
                QuestionsCount = source.Topic.QuestionsCount,
                ScormsCount = source.Topic.ScormsCount,
                UniqueId = source.Topic.UniqueId,
                CourseMapped = course
            };
        }
    }
}