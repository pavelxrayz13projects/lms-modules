﻿using AutoMapper;
using Olimp.Domain.Exam;
using Olimp.Domain.Exam.Entities;
using System;
using Contracts = Olimp.Service.Contract.SyncService.Attempts;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Logic.Converters.Attempts
{
    public static class AttemptsMapper
    {
        public static void CreateMap()
        {
            #region Attempt

            Mapper.CreateMap<Contracts.Attempt, Entities.ExamAttemptSchema>()
                .ForMember(a => a.AttemptTypeMapped, opt => opt.Ignore())
                .ForMember(a => a.SiteMapped, opt => opt.Ignore())
                .ForMember(a => a.IsDeleted, opt => opt.Ignore())
                .ForMember(a => a.TasksCount, opt => opt.MapFrom(a => a.Tasks.Count))
                .ForMember(a => a.TasksMapped, opt => opt.MapFrom(a => a.Tasks))
                .ForMember(
                    a => a.Settings,
                    opt => opt.ResolveUsing(a => new AttemptSettings
                    {
                        AllowNavigateQuestions = a.AllowNavigateQuestions,
                        LoginType = (LoginType)a.LoginType,
                        RegistrationType = (RegistrationType)a.RegistrationType,
                        ShuffleAnswers = a.ShuffleAnswers,
                        TimeLimit = a.TimeLimit,
                        TimeMode = (ExamTimeLimit)a.TimeMode
                    }))
                .ForMember(
                    a => a.GroupMapped,
                    opt => opt.ResolveUsing(ResolveAndConvert<Contracts.Attempt, Entities.CachedGroupSchema>))
                .ForMember(
                    a => a.TicketMapped,
                    opt => opt.ResolveUsing(ResolveAndConvert<Contracts.Attempt, Entities.CachedTicketSchema>))
                .ForMember(
                    a => a.ResponsiblePersoneMapped,
                    opt => opt.ResolveUsing(ResolveAndConvert<Contracts.Attempt, Entities.CachedExamResponsibleSchema>))
                .ForMember(
                    a => a.EmployeeMapped,
                    opt => opt.ResolveUsing(ResolveAndConvert<Contracts.Attempt, Entities.CachedEmployeeSchema>))
                .ForMember(
                    a => a.TestMapped,
                    opt => opt.ResolveUsing(a => ResolveAndConvert<Contracts.Course, Entities.CachedCourseSchema>(a.Course)))
                .AfterMap(FixChildToParentReferences);

            #endregion Attempt

            #region QuestionTask

            Mapper.CreateMap<Contracts.QuestionTask, Entities.AttemptQuestionTaskSchema>()
                .ForMember(qt => qt.Id, opt => opt.Ignore())
                .ForMember(qt => qt.AttemptMapped, opt => opt.Ignore())
                .ForMember(qt => qt.AnswerTimestamp, opt => opt.MapFrom(qt => qt.AsnwerTimestamp))
                .ForMember(qt => qt.QuestionMapped, opt => opt.ResolveUsing(ResolveAndConvert<Contracts.QuestionTask, Entities.CachedQuestionSchema>))
                .ForMember(qt => qt.GivenAnswerNumbersMapped, opt => opt.ResolveUsing(qt => String.Join(",", qt.GivenAnswerNumbers)));

            #endregion QuestionTask
        }

        #region Question

        private static void FixChildToParentReferences(Contracts.Question source, Entities.CachedQuestionSchema destination)
        {
            foreach (var a in destination.AnswersMapped)
                a.QuestionMapped = destination;
        }

        private static void FixChildToParentReferences(Contracts.Attempt source, Entities.ExamAttemptSchema destination)
        {
            foreach (var t in destination.TasksMapped)
                t.AttemptMapped = destination;
        }

        #endregion Question

        #region Methods

        public static IConversionContext CreateThreadStaticContext<
            TAttemptConverter, TCourseConverter, TEmployeeConverter, TPositionsConverter, TCompanyConverter,
            TTicketConverter, TTopicConverter, TGroupConverter, TResponsibleConverter, TQuestionConverter>()

            where TAttemptConverter : IConverter<Contracts.Attempt, Entities.ExamAttemptSchema>
            where TCourseConverter : IConverter<Contracts.Course, Entities.CachedCourseSchema>
            where TEmployeeConverter : IConverter<Contracts.Attempt, Entities.CachedEmployeeSchema>
            where TPositionsConverter : IConverter<Contracts.Position, Entities.CachedEmployeePositionSchema>
            where TCompanyConverter : IConverter<Contracts.Employee, Entities.CachedCompanySchema>
            where TTicketConverter : IConverter<Contracts.Attempt, Entities.CachedTicketSchema>
            where TTopicConverter : IConverter<Contracts.Question, Entities.CachedTopicSchema>
            where TGroupConverter : IConverter<Contracts.Attempt, Entities.CachedGroupSchema>
            where TResponsibleConverter : IConverter<Contracts.Attempt, Entities.CachedExamResponsibleSchema>
            where TQuestionConverter : IConverter<Contracts.QuestionTask, Entities.CachedQuestionSchema>
        {
            var context = ThreadStaticConversionContext.Create();

            context.Container.RegisterAs<TAttemptConverter, IConverter<Contracts.Attempt, Entities.ExamAttemptSchema>>();
            context.Container.RegisterAs<TCourseConverter, IConverter<Contracts.Course, Entities.CachedCourseSchema>>();
            context.Container.RegisterAs<TEmployeeConverter, IConverter<Contracts.Attempt, Entities.CachedEmployeeSchema>>();
            context.Container.RegisterAs<TPositionsConverter, IConverter<Contracts.Position, Entities.CachedEmployeePositionSchema>>();
            context.Container.RegisterAs<TCompanyConverter, IConverter<Contracts.Employee, Entities.CachedCompanySchema>>();
            context.Container.RegisterAs<TTicketConverter, IConverter<Contracts.Attempt, Entities.CachedTicketSchema>>();
            context.Container.RegisterAs<TTopicConverter, IConverter<Contracts.Question, Entities.CachedTopicSchema>>();
            context.Container.RegisterAs<TGroupConverter, IConverter<Contracts.Attempt, Entities.CachedGroupSchema>>();
            context.Container.RegisterAs<TResponsibleConverter, IConverter<Contracts.Attempt, Entities.CachedExamResponsibleSchema>>();
            context.Container.RegisterAs<TQuestionConverter, IConverter<Contracts.QuestionTask, Entities.CachedQuestionSchema>>();

            return context;
        }

        public static IConversionContext CreateThreadStaticContext(
            IConverter<Contracts.Attempt, Entities.ExamAttemptSchema> attemptConverter,
            IConverter<Contracts.Course, Entities.CachedCourseSchema> courseConverter,
            IConverter<Contracts.Attempt, Entities.CachedEmployeeSchema> employeeConverter,
            IConverter<Contracts.Position, Entities.CachedEmployeePositionSchema> positionsConverter,
            IConverter<Contracts.Employee, Entities.CachedCompanySchema> companyConverter,
            IConverter<Contracts.Attempt, Entities.CachedTicketSchema> ticketConverter,
            IConverter<Contracts.Question, Entities.CachedTopicSchema> topicConverter,
            IConverter<Contracts.Attempt, Entities.CachedGroupSchema> groupConverter,
            IConverter<Contracts.Attempt, Entities.CachedExamResponsibleSchema> responsibleConverter,
            IConverter<Contracts.QuestionTask, Entities.CachedQuestionSchema> questionConverter)
        {
            if (attemptConverter == null)
                throw new ArgumentNullException("attemptConverter");
            if (courseConverter == null)
                throw new ArgumentNullException("courseConverter");
            if (employeeConverter == null)
                throw new ArgumentNullException("employeeConverter");
            if (positionsConverter == null)
                throw new ArgumentNullException("positionsConverter");
            if (companyConverter == null)
                throw new ArgumentNullException("companyConverter");
            if (ticketConverter == null)
                throw new ArgumentNullException("ticketConverter");
            if (topicConverter == null)
                throw new ArgumentNullException("topicConverter");
            if (groupConverter == null)
                throw new ArgumentNullException("groupConverter");
            if (responsibleConverter == null)
                throw new ArgumentNullException("responsibleConverter");
            if (questionConverter == null)
                throw new ArgumentNullException("questionConverter");

            var context = ThreadStaticConversionContext.Create();

            context.Container.Register<IConverter<Contracts.Attempt, Entities.ExamAttemptSchema>>(attemptConverter);
            context.Container.Register<IConverter<Contracts.Course, Entities.CachedCourseSchema>>(courseConverter);
            context.Container.Register<IConverter<Contracts.Attempt, Entities.CachedEmployeeSchema>>(employeeConverter);
            context.Container.Register<IConverter<Contracts.Position, Entities.CachedEmployeePositionSchema>>(positionsConverter);
            context.Container.Register<IConverter<Contracts.Employee, Entities.CachedCompanySchema>>(companyConverter);
            context.Container.Register<IConverter<Contracts.Attempt, Entities.CachedTicketSchema>>(ticketConverter);
            context.Container.Register<IConverter<Contracts.Question, Entities.CachedTopicSchema>>(topicConverter);
            context.Container.Register<IConverter<Contracts.Attempt, Entities.CachedGroupSchema>>(groupConverter);
            context.Container.Register<IConverter<Contracts.Attempt, Entities.CachedExamResponsibleSchema>>(responsibleConverter);
            context.Container.Register<IConverter<Contracts.QuestionTask, Entities.CachedQuestionSchema>>(questionConverter);

            return context;
        }

        internal static TDestination ResolveAndConvert<TSource, TDestination>(TSource source)
        {
            var converter = ThreadStaticConversionContext.Current.Container
                .Resolve<IConverter<TSource, TDestination>>();

            return converter.Convert(source);
        }

        #endregion Methods
    }
}