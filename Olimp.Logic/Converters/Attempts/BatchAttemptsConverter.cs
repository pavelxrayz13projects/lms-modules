﻿using PAPI.Core.Data;
using System;
using Contracts = Olimp.Service.Contract.SyncService.Attempts;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Logic.Converters.Attempts
{
    public class BatchAttemptsConverter : BatchConverter<Contracts.Attempt, Entities.ExamAttemptSchema>
    {
        static BatchAttemptsConverter()
        {
            AttemptsMapper.CreateMap();
        }

        public BatchAttemptsConverter(IRepositoryFactory repositoryFactory, Entities.ForeignNodeSiteSchema site)
            : base(AttemptsMapper.CreateThreadStaticContext<
                  AttemptConverter, CourseConverter, EmployeeConverter, PositionsConverter, CompanyConverter,
                  TicketConverter, TopicConverter, GroupConverter, ExamResponsibleConverter, QuestionConverter>())
        {
            if (repositoryFactory == null)
                throw new ArgumentNullException("repositoryFactory");

            if (site == null)
                throw new ArgumentNullException("site");

            Context.Container.Register<IRepositoryFactory>(repositoryFactory);
            Context.Container.Register<Entities.ForeignNodeSiteSchema>(site);

            RegisterDebugCounters(Context.Container);
        }

        protected override IConverter<Contracts.Attempt, Entities.ExamAttemptSchema> GetRootConverter()
        {
            return Context.Container.Resolve<IConverter<Contracts.Attempt, Entities.ExamAttemptSchema>>();
        }

        protected override void BeforeBatchConvert()
        {
            base.BeforeBatchConvert();
            ClearDebugCounters();
        }

        protected override void AfterBatchConvert()
        {
            base.AfterBatchConvert();
            LogDebugCounters();
        }

        protected static void RegisterDebugCounters(Funq.Container container)
        {
#if DEBUG
            container.Register<IStatisticsCounters>(new Counters());
#endif
        }

        protected void ClearDebugCounters()
        {
#if DEBUG
            var counters = Context.Container.TryResolve<IStatisticsCounters>();
            if (counters != null)
                counters.Clear();
#endif
        }

        protected void LogDebugCounters()
        {
#if DEBUG
            var counters = Context.Container.TryResolve<IStatisticsCounters>();
            if (counters != null)
                Log.Debug(counters.ToString());
#endif
        }
    }
}