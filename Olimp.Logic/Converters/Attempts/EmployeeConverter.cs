﻿using Olimp.Logic.Properties;
using PAPI.Core.Data;
using System;
using System.Linq;
using System.Linq.Expressions;
using Contracts = Olimp.Service.Contract.SyncService.Attempts;
using Entities = Olimp.Domain.Exam.NHibernate.Schema;

namespace Olimp.Logic.Converters.Attempts
{
    public class EmployeeConverter : CachedEntityConverter<Contracts.Attempt, string, Entities.CachedEmployeeSchema, int>
    {
        public EmployeeConverter(IRepositoryFactory repositoryFactory)
            : base(repositoryFactory)
        {
        }

        public override string ExtractKey(Contracts.Attempt source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            return String.Format("Olimp.Employee.CompanyId={0}.{1}.{2}.{3}.{4}",
                source.Employee.Company,
                source.Employee.Surname,
                source.Employee.Name,
                source.Employee.ThirdName,
                source.Employee.Positions != null
                    ? String.Join(".", source.Employee.Positions.Select(p => p.Name))
                    : "Null");
        }

        private string GetEmployeePositions(Contracts.Employee employee)
        {
            if (employee.Positions == null || !employee.Positions.Any())
                return null;

            var positions = employee.Positions.Where(a => !String.IsNullOrWhiteSpace(a.Name))
                .Select(a => a.Name);

            return String.Join(", ", positions);
        }

        protected override Expression<Func<Entities.CachedEmployeeSchema, bool>> GetEntitySearchFilter(Contracts.Attempt source)
        {
            var sourceEmployee = source.Employee;

            if (!String.IsNullOrWhiteSpace(sourceEmployee.Company))
            {
                var positions = this.GetEmployeePositions(sourceEmployee);

                return employee => employee.Surname == sourceEmployee.Surname
                    && employee.Name == sourceEmployee.Name
                    && employee.ThirdName == sourceEmployee.ThirdName
                    && employee.CompanyMapped.Name == sourceEmployee.Company
                    && employee.Positions == positions
                    && employee.IsAnonymous == EmployeeConverterSettings.Default.CreateAnonymousEmployee;
            }
            else
            {
                return employee => employee.Surname == sourceEmployee.Surname
                    && employee.Name == sourceEmployee.Name
                    && employee.ThirdName == sourceEmployee.ThirdName
                    && employee.IsAnonymous == EmployeeConverterSettings.Default.CreateAnonymousEmployee;
            }
        }

        protected override Entities.CachedEmployeeSchema CreateNewEntity(Contracts.Attempt source)
        {
            var company = AttemptsMapper.ResolveAndConvert<Contracts.Employee, Entities.CachedCompanySchema>(source.Employee);
            var positions = this.GetEmployeePositions(source.Employee);

            var employee = new Entities.CachedEmployeeSchema
            {
                CompanyMapped = company,
                Name = source.Employee.Name,
                Number = source.Employee.Number,
                Positions = positions,
                Surname = source.Employee.Surname,
                ThirdName = source.Employee.ThirdName,
                IsAnonymous = EmployeeConverterSettings.Default.CreateAnonymousEmployee
            };

            if (!String.IsNullOrWhiteSpace(positions))
            {
                foreach (var p in source.Employee.Positions)
                {
                    var position = AttemptsMapper.ResolveAndConvert<Contracts.Position, Entities.CachedEmployeePositionSchema>(p);
                    employee.PositionsListMapped.Add(position);
                }
            }

            return employee;
        }
    }
}