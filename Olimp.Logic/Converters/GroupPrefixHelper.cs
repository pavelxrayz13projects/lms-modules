﻿using PAPI.Core.Data;
using System;
using System.Linq;
using System.Text;
using Entities = Olimp.Domain.LearningCenter.Entities;

namespace Olimp.Logic.Converters
{
    internal static class GroupPrefixHelper
    {
        public static string BuildPrefix(string name, int length)
        {
            if (name.Length == length)
                return name;

            var result = new StringBuilder(length);

            var parts = name.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var maxLength = parts.Max(p => p.Length);

            var currentPart = 0;
            var currentIndex = 0;
            while (result.Length < length)
            {
                var part = parts[currentPart];
                if (currentIndex < part.Length)
                    result.Append(part[currentIndex].ToString().ToUpper());

                if (++currentPart >= parts.Length)
                {
                    if (++currentIndex >= maxLength)
                        break;

                    currentPart = 0;
                }
            }

            return result.ToString();
        }

        public static string GetPrefix(IRepository<Entities.Group, int> repo, string name)
        {
            string prefix;

            var prefixLength = 1;

            do
            {
                if (prefixLength <= name.Length)
                    prefix = BuildPrefix(name, prefixLength++);
                else
                    prefix = String.Format("{0}-{1}", name, prefixLength++ - name.Length);
            }
            while (repo.Any(g => g.Prefix == prefix));

            return prefix;
        }
    }
}