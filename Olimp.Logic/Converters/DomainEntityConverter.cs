﻿using PAPI.Core.Data;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Olimp.Logic.Converters
{
    public class DomainEntityConverter<TSource, TDestination, TDestinatinationKey> : AutoMapperConverter<TSource, TDestination>
      where TDestination : class, IEntity<TDestinatinationKey>
    {
        private readonly string _newEntityCounterName;
        private readonly string _loadedEntityCounterName;

        private IRepositoryFactory _repositoryFactory;
        private IRepository<TDestination, TDestinatinationKey> _repository;

        public DomainEntityConverter(IRepositoryFactory repositoryFactory)
        {
            if (repositoryFactory == null)
                throw new ArgumentNullException("repositoryFactory");

            _repositoryFactory = repositoryFactory;

            _newEntityCounterName = GetType().FullName + ".New";
            _loadedEntityCounterName = GetType().FullName + ".Loaded";
        }

        public IRepositoryFactory RepositoryFactory
        {
            get { return _repositoryFactory; }
        }

        public override TDestination Convert(TSource source)
        {
            if (source == null)
                return default(TDestination);

            TDestination result = GetFromRepository(source);

            if (result == null)
            {
                result = CreateNewEntity(source);
                if (result != null)
                {
                    SaveToRepository(result);
                    IncrementDebugCounter(_newEntityCounterName);
                }
            }
            else
            {
                IncrementDebugCounter(_loadedEntityCounterName);
            }

            return result;
        }

        protected TDestination GetFromRepository(TSource source)
        {
            TDestination result;
            var repository = CreateRepository();
            result = repository.Where(GetEntitySearchFilter(source)).FirstOrDefault();
            return result;
        }

        protected virtual bool ForceSaveToRepository { get { return false; } }

        protected virtual void SaveToRepository(TDestination result)
        {
            var repository = CreateRepository();
            repository.Save(result, this.ForceSaveToRepository);
        }

        protected virtual TDestination CreateNewEntity(TSource source)
        {
            return base.Convert(source);
        }

        protected virtual IRepository<TDestination, TDestinatinationKey> CreateRepository()
        {
            if (_repository == null)
                _repository = RepositoryFactory.CreateFor<TDestination, TDestinatinationKey>();

            return _repository;
        }

        protected virtual Expression<Func<TDestination, bool>> GetEntitySearchFilter(TSource source)
        {
            return i => false;
        }

        protected void IncrementDebugCounter(string counterName)
        {
#if DEBUG
            var counters = ThreadStaticConversionContext.Current.Container.TryResolve<IStatisticsCounters>();
            if (counters != null)
                counters.Increment(counterName);
#endif
        }
    }
}