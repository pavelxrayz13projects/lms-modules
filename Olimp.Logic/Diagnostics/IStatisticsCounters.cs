﻿using System.Collections.Generic;

namespace Olimp.Logic.Diagnostics
{
    public interface IStatisticsCounters
    {
        void Clear();

        int Decrement(string counterName);

        int Increment(string counterName);

        string ToString();

        IDictionary<string, int> Values { get; }
    }
}