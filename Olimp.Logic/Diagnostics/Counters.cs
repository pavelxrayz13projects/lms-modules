﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Olimp.Logic.Diagnostics
{
    public sealed class Counters : Olimp.Logic.Diagnostics.IStatisticsCounters
    {
        private readonly Dictionary<string, Counter> _data = new Dictionary<string, Counter>();
        private readonly object syncRoot = new object();

        public int Increment(string counterName)
        {
            return GetOrCreateCounter(counterName).Increment();
        }

        public int Decrement(string counterName)
        {
            return GetOrCreateCounter(counterName).Decrement();
        }

        public IDictionary<string, int> Values
        {
            get
            {
                lock (syncRoot)
                {
                    Dictionary<string, int> result = new Dictionary<string, int>(_data.Count);
                    foreach (var entry in _data)
                    {
                        result[entry.Key] = entry.Value;
                    }

                    return result;
                }
            }
        }

        public void Clear()
        {
            lock (syncRoot)
            {
                _data.Clear();
            }
        }

        public override string ToString()
        {
            IDictionary<string, int> values = Values;
            StringBuilder sb = new StringBuilder("Current counter values:");
            sb.AppendLine();
            foreach (var item in values)
            {
                sb.AppendFormat("{0}: {1}", item.Key, item.Value);
                sb.AppendLine();
            }
            return sb.ToString();
        }

        private Counter GetOrCreateCounter(string counterName)
        {
            if (String.IsNullOrWhiteSpace(counterName))
                throw new ArgumentException("counterName is null or whitespace.");

            Counter result;
            lock (syncRoot)
            {
                if (!_data.TryGetValue(counterName, out result))
                {
                    result = new Counter();
                    _data.Add(counterName, result);
                }
            }
            return result;
        }

        private class Counter
        {
            private volatile int _count;

            public int Count
            {
                get { return _count; }
            }

            public int Increment()
            {
                return Interlocked.Increment(ref _count);
            }

            public int Decrement()
            {
                return Interlocked.Decrement(ref _count);
            }

            public static implicit operator int(Counter counter)
            {
                return counter._count;
            }
        }
    }
}