using log4net;
using Olimp.Core;
using PAPI.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Update
{
    public class Runner : IModuleRunner
    {
        private static readonly ILog _log = PAPI.Core.Logging.Log.CreateUtilityLogger("olimp.update");

        public void Run(IModule module, IDictionary<string, string> moduleParams)
        {
            var flags = ModuleLoadFlags.LoadForDatabaseInitialization;

            string moduleName;
            if (!moduleParams.TryGetValue("moduleName", out moduleName))
                moduleName = "Olimp";

            var nodeData = ModuleFactory.GetNodeData(moduleParams);

            var targetModule = PModule.Load(moduleName, nodeData ?? Platform.DefaultNode, flags);
            if (targetModule == null)
            {
                _log.ErrorFormat("Could not find module {0}.", moduleName);
                Environment.ExitCode = -1;
                return;
            }

            var updatableModule = targetModule as IUpdatableModule;
            if (updatableModule != null)
            {
                try
                {
                    var updater = updatableModule.GetUpdater();
                    if (updater != null)
                        updater.Update(targetModule.Database.UnitOfWorkFactory, targetModule.Database.GetSessionManager(), _log);

                    if (targetModule.Context.NodeData.Name == Platform.DefaultNode.Name)
                    {
                        var nodes = GetAllNodesForDefaultNode(targetModule);
                        if (nodes != null && nodes.Any())
                        {
                            foreach (var node in nodes)
                            {
                                var processArgs = String.Format("Update /moduleName:Olimp /nodeId:{0} /node:{1}", node.Id, node.Name);
                                var process = Process.Start(new ProcessStartInfo(PlatformPaths.Default.ExecutablePath, processArgs)
                                  {
                                      UseShellExecute = true
                                  });
                                process.WaitForExit();
                            }
                        }

                        CleanupUpdateFolder();
                    }
                }
                catch (Exception e)
                {
                    _log.ErrorFormat("Failed to update system. Terminating. Error: {0}", e);
                    Environment.ExitCode = -1;
                    return;
                }
            }

            Environment.ExitCode = 0;
        }

        private static ICollection<NodeData> GetAllNodesForDefaultNode(IModule currentModule)
        {
            var clusterContextContainer = currentModule as Olimp.Core.Cluster.Local.ILocalClusterContextContainer;
            if (clusterContextContainer != null)
            {
                var cluster = clusterContextContainer.LocalCluster;
                var startupRegistry = cluster.GetStartupRegistry();
                return startupRegistry.GetAll()
                    .Where(n => n.Name != Platform.DefaultNode.Name)
                    .Select(n => new NodeData(n.Id, n.Name))
                    .ToList();
            }

            return null;
        }

        private static void CleanupUpdateFolder()
        {
            foreach (var file in Directory.GetFiles(PlatformPaths.Default.Update))
                File.Delete(file);
        }
    }
}