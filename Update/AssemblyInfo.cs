using PAPI.Core;
using System.Reflection;

[assembly: AssemblyTitle("Update")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: EncryptedAssemblyReference("Olimp.Core, Version=1.0.0.0, Culture=neutral, PublicKeyToken=bd83c7d5d855788a")]
[assembly: EncryptedAssemblyReference("Olimp.Domain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=bd83c7d5d855788a")]
[assembly: EncryptedAssemblyReference("Olimp.Courses, Version=1.0.0.0, Culture=neutral, PublicKeyToken=bd83c7d5d855788a")]