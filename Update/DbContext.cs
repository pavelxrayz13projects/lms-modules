using System;
using System.IO;
using PAPI.Core.Data;
using PAPI.Core;
using Olimp.Core.Data;
using PAPI.Core.Data.NHibernate;
using Olimp.Domain.LearningCenter;
using Olimp.Courses;
using Olimp.Configuration;
using Olimp.Courses.Security;
using PAPI.Core.Security;
using PAPI.Core.Data.Entities;
using Olimp.Core;

namespace Update
{
	public class DbContext
	{
		private ISessionManager _sessionManager;
		
		private DbContext (string moduleName, NodeData nodeData)
		{
			var flags = ModuleLoadFlags.LoadForDatabaseInitialization;
			this.Module = PModule.Load("Olimp", nodeData, flags);

			_sessionManager = this.Module.Database.GetSessionManager();
			
			var distribIdBytes = this.Module.Context.DistributionInfoProvider.Get().GetDistributionIdBytes();
			var cryptoService = new CryptoService(distribIdBytes);
			
			this.CatalogueLoader = new EccCatalogueLoader(cryptoService);
			
			this.UnitOfWork = this.Module.Database.UnitOfWorkFactory;
			this.Repository = this.Module.Database.RepositoryFactory;
			this.UpdateContext = new CatalogueUpdateContext(
				(OlimpModule) this.Module,	
				((OlimpMaterialsModule) this.Module).Materials.CourseRepository,
				((OlimpMaterialsModule) this.Module).Materials.NormativeRepository,
				_sessionManager,
				this.Repository);
		}
		
		public ICatalogueLoader CatalogueLoader { get; private set; }
		
		public IModule Module { get; private set ;}
		
		public IUnitOfWorkFactory UnitOfWork { get; private set; }
		
		public IRepositoryFactory Repository { get; private set; }
		
		public CatalogueUpdateContext UpdateContext { get; private set; }
		
		public T GetConfig<T>()
			where T : class
		{
			return Config.Get<T>(_sessionManager);
		}
		
		public void SetConfig<T>(T settings)
			where T : class
		{
			Config.Set<T>(settings, _sessionManager);
		}
		
		public static DbContext OfModule(string moduleName, NodeData nodeData)
		{
			return new DbContext(moduleName, nodeData);
		}
	}
}

