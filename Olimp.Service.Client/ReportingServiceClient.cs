﻿using Olimp.Service.Contract.ReportingService;
using PAPI.Core.ServiceStack.ServiceClient;
using System.IO;

namespace Olimp.Service.Client
{
    public class ReportingServiceClient : ServiceClientBase, IReportingService
    {
        public ReportingServiceClient(string baseUri)
            : base(baseUri, "reports/generate")
        {
        }

        #region IReportingService Members

        public Stream Post(GenerateReport request)
        {
            return this.MakeRequest(request, (uri, r) => this.Post<Stream>(uri, r));
        }

        #endregion IReportingService Members
    }
}