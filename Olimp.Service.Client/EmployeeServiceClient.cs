﻿using Olimp.Service.Contract.EmployeeService.Exists;
using Olimp.Service.Contract.EmployeeService.Position;
using PAPI.Core.ServiceStack.ServiceClient;

namespace Olimp.Service.Client
{
    public class EmployeeServiceClient : ServiceClientBase, IEmployeeExists, IEmployeeAddPosition, IEmployeeRemovePositions
    {
        public EmployeeServiceClient(string baseUri)
            : base(baseUri, "employees")
        {
        }

        #region IEmployeeExists Members

        public EmployeeExistsResponse Get(EmployeeExists request)
        {
            return this.MakeRequest(request, (uri, r) =>
                this.Get<EmployeeExistsResponse>(this.FormatUri(uri + "/exists/{EmployeeId}", r)));
        }

        #endregion IEmployeeExists Members

        #region IEmployeeRemovePositions Members

        public RemoveEmployeePositionsResponse Post(RemoveEmployeePositions request)
        {
            return this.MakeRequest(request, (uri, r) =>
                this.Post<RemoveEmployeePositionsResponse>(uri + "/position/remove", r));
        }

        #endregion IEmployeeRemovePositions Members

        #region IEmployeeAddPosition Members

        public AddEmployeePositionResponse Post(AddEmployeePosition request)
        {
            return this.MakeRequest(request, (uri, r) =>
                this.Post<AddEmployeePositionResponse>(uri + "/position/add", r));
        }

        #endregion IEmployeeAddPosition Members
    }
}