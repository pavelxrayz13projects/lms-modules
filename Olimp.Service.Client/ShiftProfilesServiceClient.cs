﻿using Olimp.Service.Contract.KnowledgeControl.Shift;
using PAPI.Core.ServiceStack.ServiceClient;

namespace Olimp.Service.Client
{
    public class ShiftProfilesServiceClient : ServiceClientBase, IShiftProfileById, IShiftProfilesByPosition, IShiftProfileResultById
    {
        public ShiftProfilesServiceClient(string baseUri)
            : base(baseUri, "knowledge-control/shift")
        {
        }

        #region IShiftKnowledgeControl Members

        public ShiftProfileByIdResponse Post(ShiftProfileById request)
        {
            return this.MakeRequest(request, (uri, r) => this.Post<ShiftProfileByIdResponse>(uri + "/profile", r));
        }

        #endregion IShiftKnowledgeControl Members

        #region IShiftProfilesByPosition Members

        public ShiftProfilesByPositionResponse Post(ShiftProfilesByPosition request)
        {
            return this.MakeRequest(request, (uri, r) => this.Post<ShiftProfilesByPositionResponse>(uri + "/position", r));
        }

        #endregion IShiftProfilesByPosition Members

        #region IShiftProfileResultById Members

        public ShiftProfileResultByIdResponse Post(ShiftProfileResultById request)
        {
            return this.MakeRequest(request, (uri, r) => this.Post<ShiftProfileResultByIdResponse>(uri + "/profile-result", r));
        }

        #endregion IShiftProfileResultById Members
    }
}