﻿using Olimp.Service.Contract.SyncService.Employee;
using PAPI.Core.ServiceStack.ServiceClient;

namespace Olimp.Service.Client
{
    public class SyncEmployeeServiceClient : ServiceClientBase, ISyncEmployee
    {
        public SyncEmployeeServiceClient(string baseUri)
            : base(baseUri, "sync/employee")
        {
        }

        #region ISyncEmployee Members

        public SyncEmployeeResponse Post(SyncEmployee request)
        {
            return this.MakeRequest(request, (uri, r) => this.Post<SyncEmployeeResponse>(uri, r));
        }

        #endregion ISyncEmployee Members
    }
}