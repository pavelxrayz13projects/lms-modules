﻿using Olimp.Service.Contract.PingService;
using PAPI.Core.ServiceStack.ServiceClient;

namespace Olimp.Service.Client
{
    public class PingServiceClient : ServiceClientBase, IPingService
    {
        public PingServiceClient(string baseUri)
            : base(baseUri, "ping")
        {
        }

        #region IPingService Members

        public PingResponse Post(PingRequest request)
        {
            return this.MakeRequest(request, (uri, r) => this.Post<PingResponse>(uri, r));
        }

        #endregion IPingService Members
    }
}