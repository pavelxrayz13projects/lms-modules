﻿using Olimp.Service.Contract.KnowledgeControl.Journal;
using PAPI.Core.ServiceStack.ServiceClient;

namespace Olimp.Service.Client
{
    public class JournalServiceClient : ServiceClientBase, IJournalService, IJournalDeleteService
    {
        public JournalServiceClient(string baseUri)
            : base(baseUri, "knowledge-control/journal")
        {
        }

        #region IJournalService Members

        public JournalDataResponse Post(JournalData request)
        {
            return this.MakeRequest(request, (uri, r) => this.Post<JournalDataResponse>(uri + "/filter", r));
        }

        #endregion IJournalService Members

        #region IJournalDeleteService Members

        public DeleteFromJournalResponse Post(DeleteFromJournal request)
        {
            return this.MakeRequest(request, (uri, r) => this.Post<DeleteFromJournalResponse>(uri + "/delete", r));
        }

        #endregion IJournalDeleteService Members
    }
}