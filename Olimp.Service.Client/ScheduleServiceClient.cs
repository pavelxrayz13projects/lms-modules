﻿using Olimp.Service.Contract.KnowledgeControl.Schedule.FilterSchedule;
using Olimp.Service.Contract.KnowledgeControl.Schedule.ForEmployee;
using PAPI.Core.ServiceStack.ServiceClient;

namespace Olimp.Service.Client
{
    public class ScheduleServiceClient : ServiceClientBase, IKnowledgeControlForEmployees, IFilterSchedule, ICountKnowledgeControlForEmployees, ICountFilterSchedule
    {
        public ScheduleServiceClient(string baseUri)
            : base(baseUri, "knowledge-control")
        {
        }

        #region IKnowledgeControlForEmployee Members

        public KnowledgeControlForEmployeesResponse Post(GetKnowledgeControlForEmployees request)
        {
            return this.MakeRequest(request, (uri, r) =>
                this.Post<KnowledgeControlForEmployeesResponse>(uri + "/employees/schedule-for", r));
        }

        #endregion IKnowledgeControlForEmployee Members

        #region IFilterSchedule Members

        public FilterScheduleResponse Post(FilterSchedule request)
        {
            return this.MakeRequest(request, (uri, r) => this.Post<FilterScheduleResponse>(uri + "/schedule/filter", r));
        }

        #endregion IFilterSchedule Members

        #region ICountKnowledgeControlForEmployees Members

        public CountKnowledgeControlForEmployeesResponse Post(CountKnowledgeControlForEmployees request)
        {
            return this.MakeRequest(request, (uri, r) =>
                this.Post<CountKnowledgeControlForEmployeesResponse>(uri + "/employees/count-schedule-for", r));
        }

        #endregion ICountKnowledgeControlForEmployees Members

        #region ICountFilterSchedule Members

        public CountFilterScheduleResponse Post(CountFilterSchedule request)
        {
            return this.MakeRequest(request, (uri, r) => this.Post<CountFilterScheduleResponse>(uri + "/schedule/count-filter", r));
        }

        #endregion ICountFilterSchedule Members
    }
}