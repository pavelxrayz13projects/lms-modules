﻿using Olimp.Service.Contract.SyncService.Attempts;
using PAPI.Core.ServiceStack.ServiceClient;

namespace Olimp.Service.Client
{
    public class SyncAttemptsServiceClient : ServiceClientBase, ISyncAttempts
    {
        public SyncAttemptsServiceClient(string baseUri)
            : base(baseUri, "sync/attempts")
        {
        }

        #region ISyncAttempts Members

        public SyncFinishedAttemptsResponse Post(SyncFinishedAttempts request)
        {
            return this.MakeRequest(request, (uri, r) => this.Post<SyncFinishedAttemptsResponse>(uri, r));
        }

        #endregion ISyncAttempts Members
    }
}