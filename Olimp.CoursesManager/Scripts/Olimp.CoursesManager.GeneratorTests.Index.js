﻿$.Olimp = $.Olimp || {};
$.Olimp.CoursesManager = $.Olimp.CoursesManager || {};
$.Olimp.CoursesManager.GeneratorTests = $.Olimp.CoursesManager.GeneratorTests || {};
$.Olimp.CoursesManager.GeneratorTests.Index = {

    init: function(deleteTicketsUrl, genetateTicketsUrl) {
        this.deleteTicketsUrl = deleteTicketsUrl;
        this.genetateTicketsUrl = genetateTicketsUrl;
    },

    deleteTickets: function  () {
		if (!confirm('Тест будет деактивирован. Продолжить?'))
			return;
		
        var self = this;
        $.post($.Olimp.CoursesManager.GeneratorTests.Index.deleteTicketsUrl, { id: this.Id() }, function() {
            self.CurrentTicketsCount(0);
            $.Olimp.showSuccess('Тест деактивирован');
        });
    },
		
    generateTickets: function  () {
        var self = this;
        
        if (typeof self.InProgress === 'undefined')
            self.InProgress = false;

        if (self.InProgress === true || ((self.CurrentTicketsCount() > 0) && !confirm('Текущая версия теста будет заменена новой. Продолжить?')))
            return;

        self.InProgress =  true;
        $.post($.Olimp.CoursesManager.GeneratorTests.Index.genetateTicketsUrl, { id: this.Id() }, function() {
            self.CurrentTicketsCount(self.TicketsAmount());
            self.CurrentVersionsCount(self.CurrentVersionsCount() + 1);
            self.InProgress = false;
            $.Olimp.showSuccess('Генерация билетов завершена');
        }).fail(function () {
            self.InProgress = false;
        });
    }
};