﻿$.Olimp = $.Olimp || {};
$.Olimp.CoursesManager = $.Olimp.CoursesManager || {}
$.Olimp.CoursesManager.Info = $.Olimp.CoursesManager.Info || {}
$.Olimp.CoursesManager.Info.TreeInfo = {
    init: function(canDeleteTickets, questionInfoUrl, getTicketInfoUrl, getCourseInfoUrl, deleteTicketUrl) {
        this.canDeleteTickets = canDeleteTickets;
        this.questionInfoUrl = questionInfoUrl;
        this.getTicketInfoUrl = getTicketInfoUrl;
        this.getCourseInfoUrl = getCourseInfoUrl;
        this.deleteTicketUrl = deleteTicketUrl;
    },

    showCourseInfo: function(branchId) { $('#course-info-dialog').data('viewModel').branchId(branchId) },

    showQuestionInfo: function () {
        var branchId = $('#course-info-dialog').data('viewModel').branchId(),
            questionId = this.Id(),
            dialog = $('#question-info-dialog');
		
        dialog.load($.Olimp.CoursesManager.Info.TreeInfo.questionInfoUrl, { materialId: branchId, questionId: questionId }, function () { dialog.olimpdialog('open') });
    },

    createViewModel: function (courseInfoDialog) {
        var options = this,
            viewModel = {
                selectedTicketTitle: ko.observable(''),
                allowDeleteTickets: ko.observable(false),
                courseName: ko.observable(''),
                tickets: ko.observableArray([]),
                ticketSize: ko.observable(),
                maxErrors: ko.observable()
            },
            selectedTicket = ko.observable(),
            branchId = ko.observable();

        viewModel.selectedTicket = ko.computed({
            read: function () { return selectedTicket() },
            write: function (value) {
                var self = this;
                if (!self.branchId() || !value)
                    return;

                $.post(options.getTicketInfoUrl, { branchId: self.branchId(), ticketId: value.id() }, function (tableData) {
                    $('#ticket-questions').data('viewModel').setRows(tableData)
                    self.selectedTicketTitle(value.title())

                    selectedTicket(value);

                    courseInfoDialog.olimpdialog('option', 'position', 'center').olimpdialog('open');
                },
                'json')
            },
            owner: viewModel
        }).extend({ throttle: 100 });

        viewModel.branchId = ko.computed({
            read: function () { return branchId(); },
            write: function (value) {
                var self = this;
                branchId(value);
                $.post(options.getCourseInfoUrl, { id: value }, function (json) { ko.mapping.fromJS(json, {}, self) }, 'json');
            },
            owner: viewModel
        })

        return viewModel;
    }
}

		
$(function() {
    var dialog = $('#course-info-dialog'),
        viewModel = $.Olimp.CoursesManager.Info.TreeInfo.createViewModel(dialog),
        courseInfoNode = $('#course-info');

    ko.applyBindings(viewModel, courseInfoNode[0])		
    courseInfoNode.find('select').combobox();
		
    if ($.Olimp.CoursesManager.Info.TreeInfo.canDeleteTickets) {
        ko.applyBindings({
            allowDeleteTickets: ko.computed(function() { return viewModel.allowDeleteTickets() }),
            deleteTicket: function() {
                if (!confirm('Билет будет удален. Продолжить?')) return;
		
                $.post($.Olimp.CoursesManager.Info.TreeInfo.deleteTicketUrl, 
                    { branchId: viewModel.branchId(), ticketId: viewModel.selectedTicket().id() },
                    function() { 
                        viewModel.tickets.remove(viewModel.selectedTicket()) 
                        if (!viewModel.tickets().length) {		
                            viewModel.selectedTicket(null);
                            viewModel.selectedTicketTitle('');
                            viewModel.allowDeleteTickets(false);
                            $('#ticket-questions').data('viewModel').setRows({ rows: [], rowsCount: 0 })							
                        }
                    })
            },
            pretifyButton: function() { $('#ticket-actions button').olimpbutton() }
        }, $('#ticket-actions')[0]);
    }
		
	dialog.data('viewModel', viewModel)
	    .olimpdialog({ autoOpen: false, draggable: false, resizable: false, modal: true, width: '70%', fixedHeight: 0.9, title: 'Информация по курсу' });	
		
    $('#question-info-dialog').olimpdialog({ 
        autoOpen: false, modal: true, resizable: false, draggable: false, width: '60%', title: 'Информация о вопросе' });
})
