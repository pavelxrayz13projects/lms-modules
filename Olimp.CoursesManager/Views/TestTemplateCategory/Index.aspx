﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage"  MasterPageFile="~/Views/Shared/Register.master" %>
<%@ Import Namespace="I18N=Olimp.CoursesManager.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Register_Content" runat="server">
	<h1><%= I18N.Courses.TestCategories %></h1>		
	
	<div id="edit-dialog">	
		<% Html.RenderPartial("Edit"); %>
	</div>
	
	<div class="block">	
		<% Html.Table(
			new TableOptions {
                SelectUrl = Url.Action("GetCategories"),
				Adding = new AddingOptions {
                    Text = I18N.Courses.AddCategory,
					New = new { Id = 0, Name = "" }
				},
				DefaultSortColumn = "Name",
				PagingAllowAll = true,
				Columns = new[] { 
					new ColumnOptions("Name", Olimp.I18N.Domain.TestTemplate.Category) },
				Actions = new RowAction[] { 
					new EditAction("edit-dialog", new DialogOptions { Title = Olimp.I18N.PlatformShared.Edit, Width = "600px" }),
					new DeleteAction(Url.Action("Delete"), I18N.Courses.DeleteCategory)	}
			}); %>
	</div>
</asp:Content>

