﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<TestTemplateCategoryViewModel>" %>

<% Html.EnableClientValidation(); %>
	
<% using(Html.BeginForm("Edit", "TestTemplateCategory")) { %>
	
	<%= Html.HiddenFor(m => m.Category.Id, new { data_bind = "value: Id" } ) %>
	
	<table class="form-table">
		<tr class="first last">
			<th><%= Olimp.CoursesManager.I18N.Courses.CategoryName %></th>
			<td>				
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Category.Name) %></div>
                <%= Html.TextBoxFor(m => m.Category.Name, new { data_bind = "value: Name" } ) %>
			</td>
		</tr>
	</table>
<% } %>	