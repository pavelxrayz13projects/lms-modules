﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<div id="tree-dialog">
	<% Html.Catalogue(new CatalogueOptions { Id = "courses-tree", CheckBoxesValue = "guid", DeferredInitializationScriptRendering = true }); %>
</div>

<% Html.CatalogueInitializationScript("courses-tree"); %>

<script type="text/javascript">
$(function() {
	$('#tree-dialog').olimpdialog({ 
        autoOpen: false, 
		draggable: false, 
		resizable: false, 
		modal: true, 
		width: '70%', 			
		fixedHeight: 0.7,
		title: 'Выбор курсов',
		buttons: {
		    'Выбрать': function() {
			    var materials = $('#courses-tree').data('viewModel').getSelectedMaterials(),
				    rows = $('#materials-distribution').data('viewModel').rows(),
					defaultPercent = Math.floor(100 / materials.length),
					lastPercent = 100 - (defaultPercent * (materials.length - 1)),
					lastId = materials[materials.length - 1].guid;
	
		    	$('#materials-distribution').data('viewModel').setRows({
				    rowsCount: materials.length,
                    rows: $.map(materials, function(m) { return { 
                    	Id: m.guid, EntryName: $.trim(((m.code || '') + ' ' + m.name)), Percent: m.guid == lastId ? lastPercent : defaultPercent } 
                    }) 
		    	});
	
				$(this).olimpdialog('close');
            },
			'Отмена': function() { $(this).olimpdialog('close') }
        }
    });
})
</script>