﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TestTemplateEditViewModel>" %>

<script id="percent-template" type="text/html">
	<input type="hidden" data-bind="attr: { 
		name: 'MaterialsDistribution[' + $data._index() + '].Key',
		value: Id }" />
	<input class="percentage" type="text" data-bind="attr: { 
		name: 'MaterialsDistribution[' + $data._index() + '].Value' },
		value: Percent" />
</script>


<% using(Html.BeginForm("Save", "GeneratorTests")) { %>
	<%= Html.Hidden("id", "0", new Dictionary<string, object> { { "data-bind", "value: Id" } }) %>

	<table class="form-table">
		<tr class="first">						
			<th><%= Html.LabelFor(m => m.Name) %></th>
			<td>
				<%= Html.TextBoxFor(m => m.Name, new Dictionary<string, object> {
					{ "data-bind", "value: Name" }
				}) %>
				<div class="validation-message">
				<%= Html.ValidationMessageFor(m => m.Name) %>
				</div>
			</td>
		</tr>
        <tr>						
			<th><%= Html.LabelFor(m => m.CategoryName) %></th>
			<td>
				<%= Html.TextBoxFor(m => m.CategoryName, new { id = "category-name", data_bind = "value: CategoryName" } 
                ) %>										
                <div class="validation-message">
				    <%= Html.ValidationMessageFor(m => m.CategoryName) %>
				</div>
			</td>
		</tr>
		<tr>						
			<th><%= Html.LabelFor(m => m.TicketsAmount) %></th>
			<td>
				<%= Html.TextBoxFor(m => m.TicketsAmount, new Dictionary<string, object> {
					{ "data-bind", "value: TicketsAmount"}
				}) %>
				<div class="validation-message">
				<%= Html.ValidationMessageFor(m => m.TicketsAmount) %>
				</div>
			</td>
		</tr>	
		<tr>						
			<th><%= Html.LabelFor(m => m.QuestionsInTicket) %></th>
			<td>
				<%= Html.TextBoxFor(m => m.QuestionsInTicket, new Dictionary<string, object> {
					{ "data-bind", "value: QuestionsInTicket"}
				}) %>
				<div class="validation-message">
				<%= Html.ValidationMessageFor(m => m.QuestionsInTicket) %>
				</div>
			</td>
		</tr>
		<tr>						
			<th><%= Html.LabelFor(m => m.AllowedMistakesCount) %></th>
			<td>
				<%= Html.TextBoxFor(m => m.AllowedMistakesCount, new Dictionary<string, object> {
					{ "data-bind", "value: AllowedMistakesCount"}
				}) %>
				<div class="validation-message">
				<%= Html.ValidationMessageFor(m => m.AllowedMistakesCount) %>
				</div>
			</td>
		</tr>	
		<tr>
			<td colspan="2">
				<button id="select-courses" type="button">Выбрать курсы</button>
			</td>
		</tr>
	</table>	

	<div id="percentage-validation" class="validation-message"></div>
	
	<div style="margin-top: 6px;" id="materials-distribution" data-bind="
		table: { paging: false, sorting: false, bodyTemplateEngine: 'DoT'},
		tableColumns: [
			{ name: 'EntryName', caption: 'Наименование курса' },
			{ name: 'Percent', caption: 'Процент вопросов', templateId: 'percent-template' }],
		tableData: Distribution">
	</div>
<% } %>