﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<GeneratorTestsIndexViewModel>"  MasterPageFile="~/Views/Shared/Courses.master" %>

<asp:Content ContentPlaceHolderID="Olimp_Courses_Js" runat="server">
	<% Html.EnableClientValidation(); %>
	<% Platform.RenderExtensions("/olimp/courses-manager/generator-tests/index/layout/js"); %>

	<script type="text/javascript">	
	    $.Olimp.CoursesManager.GeneratorTests.Index.init('<%= Url.Action("DeleteTickets") %>', '<%= Url.Action("GenerateTickets") %>');
	</script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Courses_Content" runat="server">
    <% if(Model.AllowGenerateUserTests) { %>
	    <div id="edit-dialog">
		    <% Html.RenderPartial("Edit", new TestTemplateEditViewModel()); %>
	    </div>
    <% } %>

    <script type="text/javascript">
        $(function () {
            $('#edit-dialog').bind('afterApply', function () {
                $('#select-courses').olimpbutton().click(function () {
                    $.post('<%= Url.Action("GetCourse", "Info") %>', function (json) {
                        var rows = $('#materials-distribution').data('viewModel').rows(),
                            treeModel = $('#courses-tree').data('viewModel');

                        treeModel.selected($.map(rows, function (m) { return m.Id() }))
                        treeModel.updateTree(json);

                        $('#tree-dialog').olimpdialog('open');
                    }, 'json')
                })
                $('#category-name').combobox({
                    source: function(request, response) {
                        $.post('<%= Url.Action("LookupCategories") %>', { filter: request.term }, response);
                    }
                });
            })
        })
    </script>
	
	<% Html.RenderPartial("CoursesTree"); %>
	
	<script id="delete-tickets-action" type="text/html">
		<a href="#" class="icon icon-trash action -has-dot-binding" title="Деактивировать тест" 
            data-dot-bind="click:  $.Olimp.CoursesManager.GeneratorTests.Index.deleteTickets"
            data-dot-context="{{= it.$$contextId }}">
		</a>
	</script>
	
	<script id="generate-tickets-action" type="text/html">
		<a href="#" class="icon icon-generate-3 action -has-dot-binding" title="Сгенерировать билеты" 
            data-dot-bind="click:  $.Olimp.CoursesManager.GeneratorTests.Index.generateTickets"
            data-dot-context="{{= it.$$contextId }}">
		</a>
	</script>	

    <% Html.RenderPartial("SwitchShareUserMaterialBranchActionTemplate", new ShareUserBranchViewModel("GeneratorTests"));  %>	
	
	<h1>Генератор тестов</h1>	
	<div class="block">
        <% 
            var actions = new List<RowAction>();
            if (Model.AllowGenerateUserTests)
            {
                actions.Add(new EditAction("edit-dialog", new DialogOptions {Title = "Наcтройки генерации билетов", Width = "80%", FixedHeight = 0.8})
                {
                    LoadModelUrl = Url.Action("GetTemplate")
                });
                actions.Add(new TemplateAction("generate-tickets-action"));
            }
            actions.Add(new TemplateAction("share-action"));
            if (Model.AllowGenerateUserTests)
            {
                actions.Add(new TemplateAction("delete-tickets-action"));
                actions.Add(new DeleteAction(Url.Action("DeleteTemplate"), "Шаблон и тест будут удалены. Продолжить?"));
            }
        %>	
		<% Html.Table(
			new TableOptions {
                BodyTemplateEngine = TemplateEngines.DoT,
				SelectUrl = Url.Action("GetTemplates"),
				Adding = new AddingOptions {
                    Disabled = !Model.AllowGenerateUserTests,
					Text = "Создать тест",
                    New = new { Id = 0, Name = "", CategoryName = "", AllowedMistakesCount = 1, TicketsAmount = 10, QuestionsInTicket = 5, IsShared = false, Distribution = new { rows = new object[0] } }
				},
				Columns = new[] { 
					new ColumnOptions("Name", Olimp.I18N.Domain.TestTemplate.Name),
                    new ColumnOptions("CategoryName", Courses.Category),
                    new ColumnOptions("CurrentVersionsCount", Olimp.I18N.Domain.TestTemplate.CurrentVersions),
					new ColumnOptions("CurrentTicketsCount", Olimp.I18N.Domain.TestTemplate.CurrentTickets),                    
					new ColumnOptions("TicketsAmount", Olimp.I18N.Domain.TestTemplate.TicketsAmount),
					new ColumnOptions("TicketSize", Olimp.I18N.Domain.TestTemplate.TicketSize),					
					new ColumnOptions("MaxErrors", Olimp.I18N.Domain.TestTemplate.MaxErrors)
				},
                Actions = actions.ToArray()
			}); %>
	</div>	
</asp:Content>