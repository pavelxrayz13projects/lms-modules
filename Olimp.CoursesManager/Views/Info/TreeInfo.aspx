﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<InfoViewModel>"  MasterPageFile="~/Views/Shared/Courses.master" %>

<asp:Content ContentPlaceHolderID="Olimp_Courses_Js" runat="server">
    <% Platform.RenderExtensions("/olimp/courses-manager/info/tree-info/layout/js"); %>
    <script>
        $.Olimp.CoursesManager.Info.TreeInfo.init(
            <%= Model.CanDeleteTickets.ToString().ToLower() %>,
            '<%= Url.Action("QuestionInfo", "Archive") %>',
            '<%= Url.Action("GetTicketInfo", new { controller = "Info" }) %>',
            '<%= Url.Action("GetCourseInfo", new { controller = "Info" }) %>',
            '<%= Url.Action("DeleteTicket", new { controller = "Info" }) %>');
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Olimp_Courses_Content" runat="server">
		
	<h1>Информация о курсах</h1>	
	<div class="block">
	<% Html.Catalogue(	
		new CatalogueOptions { 
			SelectUrl = Url.Action("GetCourse"),
			TemplateEngine = TemplateEngines.DoT,
			ShowCheckBoxes = false,
            MaterialLink = "javascript:$.Olimp.CoursesManager.Info.TreeInfo.showCourseInfo({id});"
		}); 
	%>
	</div>
	
    <% Html.RenderPartial("CourseInfoTemplates"); %>	

	<div id="course-info-dialog">
	<% Html.RenderPartial("CourseInfo"); %>
	</div>
	
    <% Html.TableInitializationScript("ticket-questions"); %>

	<div id="question-info-dialog"></div>

</asp:Content>