﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<InfoViewModel>" %>

<div id="course-info">
	<h1 data-bind="text: courseName"></h1>	
	<div class="block">
		<table class="form-table">
			<tr class="first">		
				<th>Количество билетов</th>
				<td data-bind="text: tickets().length"></td>
			</tr>		
			<tr>		
				<th>Количество вопросов в билете</th>
				<td data-bind="text: ticketSize"></td>
			</tr>	
			<tr>		
				<th>Количество допустимых ошибок</th>
				<td data-bind="text: maxErrors"></td>
			</tr>
			<tr class='last'>		
				<th>Билеты</th>
				<td>
					<select data-bind="
						options: tickets, 
						optionsText: 'title', 
						value: selectedTicket">
					</select>
				</td>
			</tr>			
		</table>
	</div>
        
	<h1 data-bind="text: selectedTicketTitle"></h1>	    
</div>

<div class="block">
	<% Html.Table(new TableOptions {
        DeferredInitializationScriptRendering = true,
		Sorting = false,
		Paging = false,
		Columns = new[] { 
			new ColumnOptions("Number", Olimp.I18N.Domain.Attempt.Number),
			new ColumnOptions("Text", Olimp.I18N.Domain.Attempt.Question) },
		Actions = new RowAction[] { new TemplateAction("question-info-action") },
	}, new { id = "ticket-questions" }); %>
</div>

<% if (Model.CanDeleteTickets) { %>
    <div id="ticket-actions" data-bind="if: allowDeleteTickets">
        <div data-bind="template: {
            name: 'delete-button-template',
            afterRender: pretifyButton }">
    	</div>
    </div>
<% } %>




