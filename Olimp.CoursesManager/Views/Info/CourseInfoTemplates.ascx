﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<InfoViewModel>" %>

<% Html.RenderPartial("AnswerResultTemplate"); %>

<script id="question-info-action" type="text/html">
    <a href="#" class="icon icon-info action" data-bind="click: $.Olimp.CoursesManager.Info.TreeInfo.showQuestionInfo" title="Просмотреть вопрос"></a>
</script>

<% if (Model.CanDeleteTickets) { %>
    <script id="delete-button-template" type="text/html">
	    <button data-bind="click: deleteTicket">Удалить билет</button>
    </script>	   
<% } %>