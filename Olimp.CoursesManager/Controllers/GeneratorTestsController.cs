using Olimp.Core.Mvc.Security;
using Olimp.Courses.Generation.Tickets;
using Olimp.CoursesManager.ViewModels;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Entities;
using Olimp.Domain.Catalogue.Security;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Materials;
using Olimp.Web.Controllers.Security;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Olimp.CoursesManager.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.UserTests.Generate + "," + Permissions.LocalCluster.UserCourses.Share, Order = 2)]
    [CheckActivation]
    public class GeneratorTestsController : ShareableUserMaterialBranchController<GeneratedByUserMaterialBranch>
    {
        private readonly IGeneratedByUserBranchRepository _branchRepository;
        private readonly IDirectoryRepository<UserMaterialCategory, int> _categoryRepository;

        public GeneratorTestsController(
            IGeneratedByUserBranchRepository branchRepository,
            IDirectoryRepository<UserMaterialCategory, int> categoryRepository)
            : base(branchRepository)
        {
            if (branchRepository == null) throw new ArgumentNullException("branchRepository");
            if (categoryRepository == null) throw new ArgumentNullException("categoryRepository");

            _branchRepository = branchRepository;
            _categoryRepository = categoryRepository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new GeneratorTestsIndexViewModel { AllowGenerateUserTests = Roles.IsUserInRole(Permissions.UserTests.Generate) });
        }

        public ActionResult GetTemplate(int id)
        {
            var branch = _branchRepository.GetById(id);
            var entries = _branchRepository.GetTestEntries(id);

            var userCourse = branch.Material as UserCourse;
            var ticketsCount = userCourse != null
                ? userCourse.TicketsCount
                : 0;

            var tableViewModel = new TableViewModel();

            return Json(new
            {
                branch.Id,
                branch.Name,
                CategoryName = branch.Category != null ? branch.Category.Name : string.Empty,
                branch.QuestionsInTicket,
                branch.TicketsAmount,
                branch.AllowedMistakesCount,
                IsShared = branch.IsSharedGlobally,
                CurrentTicketsCount = ticketsCount,
                CurrentVersionsCount = branch.GeneratedVersionsCount,
                Distribution = entries.GetTableData(
                    tableViewModel, p => new { p.EntryName, p.Percent, Id = p.UniqueId })
            });
        }

        public ActionResult GetTemplates(TableViewModel model)
        {
            var data = _branchRepository.GetAll().Select(tt => new
            {
                tt.Id,
                tt.Name,
                CategoryName = tt.Category != null ? tt.Category.Name : string.Empty,
                TicketSize = tt.QuestionsInTicket,
                tt.TicketsAmount,
                IsShared = tt.IsSharedGlobally,
                MaxErrors = tt.AllowedMistakesCount,
                CurrentTicketsCount = tt.Material != null
                    ? ((Course)tt.Material).TicketsCount
                    : 0,
                CurrentVersionsCount = tt.GeneratedVersionsCount
            }).GetTableData(model, t => t);

            return Json(data);
        }

        [HttpPost]
        public ActionResult Save(int? id)
        {
            var model = new TestTemplateEditViewModel();

            UpdateModel(model);

            var templateId = id.GetValueOrDefault();
            if (templateId > 0)
                _branchRepository.Update(templateId, model.Name, model.CategoryName, model, model.MaterialsDistribution);
            else
                _branchRepository.Include(model.Name, model.CategoryName, model, model.MaterialsDistribution);

            return null;
        }

        [HttpPost]
        public ActionResult LookupCategories(string filter)
        {
            return Json(_categoryRepository.Lookup(filter).OrderBy(e => e.Name).Select(e => e.Name));
        }

        [HttpPost]
        public ActionResult DeleteTemplate(int id)
        {
            _branchRepository.DeleteById(id);

            //TODO: Caching
            //if (blame)
            //	CourseTreeCache.BlameAll();

            return null;
        }

        public ActionResult DeleteTickets(int id)
        {
            _branchRepository.DeleteTickets(id);

            /*CourseTreeCache.BlameAll();*/

            return null;
        }

        public ActionResult GenerateTickets(int id)
        {
            try
            {
                _branchRepository.GenerateTickets(id);
            }
            catch (NotEnoughQuestionsException)
            {
                return new StatusCodeResultWithText(500, I18N.Courses.NotEnoughQuestions);
            }

            //TODO: Reset cache
            //CourseTreeCache.BlameAll();

            return null;
        }
    }
}