﻿using Olimp.CoursesManager.ViewModels;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Entities;
using Olimp.Domain.Catalogue.Security;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using System.Web.Mvc;

namespace Olimp.CoursesManager.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.UserTests.ManageCategories, Order = 2)]
    public class TestTemplateCategoryController : PController
    {
        private readonly IDirectoryRepository<UserMaterialCategory, int> _repository;

        public TestTemplateCategoryController(IDirectoryRepository<UserMaterialCategory, int> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetCategories(TableViewModel model)
        {
            var categories =
                _repository.GetSortedPage(
                    model.PageSize.GetValueOrDefault(),
                    model.CurrentPage.GetValueOrDefault(),
                    model.SortColumn,
                    model.SortAsc.HasValue && !model.SortAsc.Value);

            return Json(new
                            {
                                rows = categories.Entities,
                                rowsCount = categories.TotalCount
                            });
        }

        [HttpPost]
        public ActionResult Edit(TestTemplateCategoryViewModel model)
        {
            if (!ModelState.IsValid) return null;

            if (model.Category.Id > 0) _repository.Rename(model.Category.Id, model.Category.Name);
            else _repository.GetOrCreate(model.Category.Name);

            return null;
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            _repository.DeleteById(id);
            return null;
        }
    }
}