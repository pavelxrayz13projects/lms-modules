using System.Security;
using Olimp.Core.Mvc.Security;
using Olimp.Core.Web;
using Olimp.Courses.Extensions;
using Olimp.CoursesManager.ViewModels;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Catalogue.Tickets;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Olimp.CoursesManager.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Courses.TreeInfo, Order = 2)]
    [CheckActivation]
    public class InfoController : PController
    {
        private IWholeCatalogueProviderFactory _catalogueProviderFactory;
        private ICourseRepository _coursesRepository;
        private ITicketsInfoProvider _ticketsInfoProvider;
        private ITicketRepositoryFactory _ticketRepositoryFactory;

        public InfoController(
            IWholeCatalogueProviderFactory catalogueProviderFactory,
            ICourseRepository coursesRepository,
            ITicketsInfoProvider ticketsInfoProvider,
            ITicketRepositoryFactory ticketRepositoryFactory)
        {
            if (catalogueProviderFactory == null)
                throw new ArgumentNullException("catalogueProviderFactory");

            if (coursesRepository == null)
                throw new ArgumentNullException("coursesRepository");

            if (ticketsInfoProvider == null)
                throw new ArgumentNullException("ticketsInfoProvider");

            if (ticketRepositoryFactory == null)
                throw new ArgumentNullException("ticketRepositoryFactory");

            _catalogueProviderFactory = catalogueProviderFactory;
            _coursesRepository = coursesRepository;
            _ticketsInfoProvider = ticketsInfoProvider;
            _ticketRepositoryFactory = ticketRepositoryFactory;
        }

        [HttpGet]
        public ActionResult TreeInfo()
        {
            return View(new InfoViewModel
            {
                CanDeleteTickets = Roles.IsUserInRole(
                    Permissions.Courses.DeleteTickets)
            });
        }

        public ActionResult GetCourseInfo(int id)
        {
            var currentUserPermissions = RolesHelper.GetCurrentUserRoles(ControllerContext);

            if(!currentUserPermissions.Contains(Permissions.Courses.ViewTickets))
                throw new SecurityException(I18N.Courses.UserNotInRoleAuthError);

            var course = _coursesRepository.GetByBranchId(id);
            var info = _ticketsInfoProvider.GetTicketsInfo(course);

            return Json(new
            {
                allowDeleteTickets = info.AllowDeleteTickets,
                courseName = String.Join(" ", course.Code, course.Name),
                ticketSize = info.QuestionsInTicket,
                maxErrors = info.AllowedMistakesCount,
                tickets = info.Tickets.Select(t => new { id = t.UniqueId, title = t.Title })
            });
        }

        public ActionResult GetTicketInfo(int branchId, Guid ticketId)
        {
            var course = _coursesRepository.GetByBranchId(branchId);
            var ticketRepostitory = _ticketRepositoryFactory.Create(course);
            var ticket = ticketRepostitory.GetById(ticketId);

            return Json(ticket.Questions.GetTableData(new TableViewModel(), q => new
            {
                Id = q.UniqueId,
                Number = q.Number,
                Text = q.GetTextWithFixedCourseImageLinks(branchId)
            }));
        }

        public ActionResult MaterialTickets(MaterialTicketsInfoViewModel model)
        {
            return PartialView(model);
        }

        [AuthorizeForAdmin(Roles = Permissions.Courses.DeleteTickets, Order = 2)]
        public ActionResult DeleteTicket(Guid ticketId, int branchId)
        {
            var course = _coursesRepository.GetByBranchId(branchId);
            var ticketRepostitory = _ticketRepositoryFactory.Create(course);

            ticketRepostitory.DeleteById(ticketId);

            return null;
        }

        public ActionResult GetCourse()
        {
            var catalogueProvider = _catalogueProviderFactory.Create(
                CatalogueLockingPolicy.NoTickets, PContext.ExecutingModule.Context.NodeData.Id);

            return Json(catalogueProvider.GetWholeCatalogue(MaterialType.Course), JsonRequestBehavior.AllowGet);
            //TODO: Caching
            //return CourseTreeCache.GetCourseTree(false, "text/html");
        }
    }
}