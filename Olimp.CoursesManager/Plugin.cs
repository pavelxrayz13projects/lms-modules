using Olimp.Core;
using Olimp.Core.Routing;
using Olimp.Courses.Generation.Generators.Course;
using Olimp.Courses.Generation.Tickets;
using Olimp.Courses.Security;
using Olimp.CoursesManager;
using Olimp.CoursesManager.Controllers;
using Olimp.Domain.Catalogue;
using Olimp.Domain.Catalogue.Catalogue;
using Olimp.Domain.Catalogue.Entities;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Catalogue;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Catalogue.OrmLite.Security;
using Olimp.Domain.Catalogue.Tickets;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.IO;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Plugin))]

namespace Olimp.CoursesManager
{
    public class Plugin : PluginModule
    {
        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(GeneratorTestsController),
                typeof(InfoController),
                typeof(TestTemplateCategoryController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (Context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization)) return;

            var materialsModule = PContext.ExecutingModule as OlimpMaterialsModule;
            if (materialsModule == null)
                throw new InvalidOperationException("Executing module is not OlimpMaterialsModule");

            var distributionInfo = materialsModule.Context.DistributionInfoProvider.Get();
            if (distributionInfo == null) throw new FileNotFoundException("Distribution info file wasn't found");

            var cryptoService = new CryptoService(distributionInfo.GetDistributionIdBytes());

            var nodeData = materialsModule.Context.NodeData;

            var connFactoryFactory = new PerHttpRequestConnectionFactoryFactory(new SqliteConnectionFactoryFactory());
            var connFactory = connFactoryFactory.Create();

            var catalogueTreeBuilder = new DefaultCatalogueTreeBuilder();

            var catalogueProviderFactory = new OrmLiteWholeCatalogueProviderFactory(connFactory, catalogueTreeBuilder);

            var nodeRepository = new OrmLiteNodeRepository(connFactory);
            var courseRepository = new OrmLiteCourseRepository(nodeData, connFactory, materialsModule.Materials.CourseRepository, nodeRepository);

            var courseService = new EcpCourseService(cryptoService, materialsModule.Materials.CourseRepository);
            var ticketRepositoryFactory = new OrmLiteTicketRepositoryFactory(courseRepository, materialsModule.Materials.CourseRepository, courseService);

            var ticketInfoProvider = new OrmLiteTicketsInfoProvider(connFactoryFactory, materialsModule.Materials.CourseRepository, ticketRepositoryFactory);

            binder.Controller<InfoController>(b => b
                .Bind<IWholeCatalogueProviderFactory>(() => catalogueProviderFactory)
                .Bind<ICourseRepository>(() => courseRepository)
                .Bind<ITicketsInfoProvider>(() => ticketInfoProvider)
                .Bind<ITicketRepositoryFactory>(() => ticketRepositoryFactory));

            var predefinedBranchesProvider = new OrmLitePredefinedCatalogueBranchesProvider(connFactory);
            var containerRepository = new OrmLiteContainerBranchRepository(Context.NodeData, connFactory, predefinedBranchesProvider);

            var activationNumberProvider = new OlimpModuleActivationNumberProvider(materialsModule);
            var ticketGeneratorFactory = new AggregationTicketGeneratorFactory();
            var testGenerator = new OrmLiteTestGenerator(
                nodeData,
                courseService,
                materialsModule.Materials.CourseRepository,
                activationNumberProvider,
                ticketGeneratorFactory);

            var userMaterialCategoryRepository = new OrmLiteUserMaterialCategoryRepository(connFactory, Context.NodeData);

            var branchRepository = new PermissionInsuredGeneratedByUserBranchRepository(
                new OrmLiteGeneratedByUserBranchRepository(
                    nodeData, connFactory, nodeRepository, containerRepository, userMaterialCategoryRepository, courseRepository, testGenerator),
                new RoleProviderPermissionChecker());

            var categoryRepository = new OrmLiteUserMaterialCategoryRepository(connFactory, nodeData);

            binder.Controller<GeneratorTestsController>(b => b
                .Bind<IGeneratedByUserBranchRepository>(() => branchRepository)
                .Bind<IDirectoryRepository<UserMaterialCategory, int>>(() => categoryRepository));

            binder.Controller<TestTemplateCategoryController>(
                b => b.Bind<IDirectoryRepository<UserMaterialCategory, int>>(() => categoryRepository));
        }
    }
}