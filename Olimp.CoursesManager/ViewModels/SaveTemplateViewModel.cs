using System.Collections.Generic;

namespace Olimp.CoursesManager.ViewModels
{
    public class SaveTemplateViewModel
    {
        public SaveTemplateViewModel()
        {
            this.MaterialsDistribution = new Dictionary<int, double>();
        }

        public int Id { get; set; }

        public string TemplateName { get; set; }

        public string TestName { get; set; }

        public int TicketsAmount { get; set; }

        public int TicketSize { get; set; }

        public int MaxErrors { get; set; }

        public Dictionary<int, double> MaterialsDistribution { get; set; }
    }
}