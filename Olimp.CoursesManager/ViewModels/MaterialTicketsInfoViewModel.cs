namespace Olimp.CoursesManager.ViewModels
{
    public class MaterialTicketsInfoViewModel
    {
        public int? Page { get; set; }

        public int MaterialId { get; set; }
    }
}