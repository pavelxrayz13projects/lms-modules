using Olimp.Courses.Model;
using System;

namespace Olimp.CoursesManager.ViewModels
{
    public class ContentShowQuestionViewModel
    {
        public CourseQuestion Question { get; set; }

        public Guid QuestionId { get; set; }

        public int MaterialId { get; set; }
    }
}