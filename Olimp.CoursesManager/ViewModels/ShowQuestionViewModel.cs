using System;

namespace Olimp.CoursesManager.ViewModels
{
    public class ShowQuestionViewModel
    {
        public int MaterialId { get; set; }

        public Guid QuestionId { get; set; }
    }
}