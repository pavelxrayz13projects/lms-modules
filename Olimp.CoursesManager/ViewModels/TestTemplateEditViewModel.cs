using Olimp.Core.ComponentModel;
using Olimp.Domain.Catalogue.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Olimp.CoursesManager.ViewModels
{
    public class TestTemplateEditViewModel : ITicketOptions
    {
        public TestTemplateEditViewModel()
        {
            this.MaterialsDistribution = new Dictionary<Guid, double>();
        }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.TestTemplate), DisplayNameResourceName = "Name")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public string Name { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.Courses), DisplayNameResourceName = "Category")]
        public string CategoryName { get; set; }

        public IDictionary<Guid, double> MaterialsDistribution { get; set; }

        #region ITicketOptions Members

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.TestTemplate), DisplayNameResourceName = "MaxErrors")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int AllowedMistakesCount { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.TestTemplate), DisplayNameResourceName = "TicketSize")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int QuestionsInTicket { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.TestTemplate), DisplayNameResourceName = "TicketsAmount")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        public int TicketsAmount { get; set; }

        #endregion ITicketOptions Members
    }
}