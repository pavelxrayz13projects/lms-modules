﻿using Olimp.Domain.Catalogue.Entities;

namespace Olimp.CoursesManager.ViewModels
{
    public class TestTemplateCategoryViewModel
    {
        public UserMaterialCategory Category { get; set; }
    }
}