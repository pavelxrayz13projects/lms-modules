﻿using System.Collections.Generic;

namespace Olimp.Service.Contract.ReportingService
{
    public class GenerateReport
    {
        public string ReportFileName { get; set; }

        public Dictionary<string, string[]> InputValues { get; set; }
    }
}