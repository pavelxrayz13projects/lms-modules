﻿using System.IO;

namespace Olimp.Service.Contract.ReportingService
{
    public interface IReportingService
    {
        Stream Post(GenerateReport request);
    }
}