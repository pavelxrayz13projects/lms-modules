﻿using System;

namespace Olimp.Service.Contract.SyncService.Employee
{
    public class Employee
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string ThirdName { get; set; }

        public string Number { get; set; }

        public string Company { get; set; }

        public string Group { get; set; }

        //public string[] Positions { get; set; }
    }
}