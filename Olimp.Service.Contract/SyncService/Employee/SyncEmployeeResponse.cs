﻿namespace Olimp.Service.Contract.SyncService.Employee
{
    public class SyncEmployeeResponse
    {
        public EmployeeSyncState Result { get; set; }
    }
}