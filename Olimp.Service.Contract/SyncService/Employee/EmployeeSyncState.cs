﻿namespace Olimp.Service.Contract.SyncService.Employee
{
    public enum EmployeeSyncState
    {
        Unknown = 0,
        Created = 1,
        Updated = 2
    }
}