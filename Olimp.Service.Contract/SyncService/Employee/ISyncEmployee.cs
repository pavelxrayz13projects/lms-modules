﻿namespace Olimp.Service.Contract.SyncService.Employee
{
    public interface ISyncEmployee
    {
        SyncEmployeeResponse Post(SyncEmployee request);
    }
}