﻿using System;

namespace Olimp.Service.Contract.SyncService.Attempts
{
    public class QuestionTask
    {
        public bool IsCorrect { get; set; }

        public bool IsAnswered { get; set; }

        public int Order { get; set; }

        public DateTime? AsnwerTimestamp { get; set; }

        public string AnswerText { get; set; }

        public int[] GivenAnswerNumbers { get; set; }

        public Question Question { get; set; }
    }
}