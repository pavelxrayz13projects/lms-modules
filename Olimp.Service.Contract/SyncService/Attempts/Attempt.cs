﻿using System;
using System.Collections.Generic;

namespace Olimp.Service.Contract.SyncService.Attempts
{
    public class Attempt
    {
        public Guid Id { get; set; }

        #region Attributes

        public bool IsFinished { get; set; }

        public bool IsPassed { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? FinishTime { get; set; }

        public int MistakesCount { get; set; }

        public Guid ConcreteEmployeeId { get; set; }

        public bool AllowNavigateQuestions { get; set; }

        public bool ShuffleAnswers { get; set; }

        public int? TimeLimit { get; set; }

        public string ExamResponsible { get; set; }

        public string TestArea { get; set; }

        #region Enums

        public int AttemptType { get; set; }

        public int RegistrationType { get; set; }

        public int LoginType { get; set; }

        public int TimeMode { get; set; }

        public int? KnowledgeControlReason { get; set; }

        #endregion Enums

        #endregion Attributes

        #region References

        public ICollection<QuestionTask> Tasks { get; set; }

        public Group Group { get; set; }

        public Course Course { get; set; }

        public Employee Employee { get; set; }

        public Ticket Ticket { get; set; }

        #endregion References
    }
}