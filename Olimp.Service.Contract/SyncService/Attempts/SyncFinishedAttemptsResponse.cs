﻿using System.Collections.Generic;

namespace Olimp.Service.Contract.SyncService.Attempts
{
    public class SyncFinishedAttemptsResponse
    {
        public IList<Attempt> Attempts { get; set; }

        public bool MoreItemsAvailable { get; set; }
    }
}