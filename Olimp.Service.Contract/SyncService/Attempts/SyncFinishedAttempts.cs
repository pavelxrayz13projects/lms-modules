﻿using System;

namespace Olimp.Service.Contract.SyncService.Attempts
{
    public class SyncFinishedAttempts
    {
        public DateTime? LastSyncAttemptFinishedTime { get; set; }

        public int MaxCountToReturn { get; set; }
    }
}