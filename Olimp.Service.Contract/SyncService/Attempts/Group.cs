﻿using System;

namespace Olimp.Service.Contract.SyncService.Attempts
{
    public class Group
    {
        public int Id { get; set; }

        public string Prefix { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime ExamPeriodBegin { get; set; }

        public DateTime ExamPeriodEnd { get; set; }
    }
}