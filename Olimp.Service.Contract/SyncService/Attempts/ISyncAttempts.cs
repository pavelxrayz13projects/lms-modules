﻿namespace Olimp.Service.Contract.SyncService.Attempts
{
    public interface ISyncAttempts
    {
        SyncFinishedAttemptsResponse Post(SyncFinishedAttempts request);
    }
}