﻿using System;

namespace Olimp.Service.Contract.SyncService.Attempts
{
    public class Topic
    {
        public int Id { get; set; }

        public Guid UniqueId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int ScormsCount { get; set; }

        public int DocumentsCount { get; set; }

        public int QuestionsCount { get; set; }

        public Course Course { get; set; }
    }
}