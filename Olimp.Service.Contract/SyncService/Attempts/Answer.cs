﻿namespace Olimp.Service.Contract.SyncService.Attempts
{
    public class Answer
    {
        public int Id { get; set; }

        public bool IsCorrect { get; set; }

        public int Number { get; set; }

        public string Text { get; set; }
    }
}