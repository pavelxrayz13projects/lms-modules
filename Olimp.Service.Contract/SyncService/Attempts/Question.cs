﻿using System;
using System.Collections.Generic;

namespace Olimp.Service.Contract.SyncService.Attempts
{
    public class Question
    {
        public int Id { get; set; }

        public Guid UniqueId { get; set; }

        public bool AllowMultiple { get; set; }

        public string Text { get; set; }

        public Topic Topic { get; set; }

        public ICollection<Answer> Answers { get; set; }
    }
}