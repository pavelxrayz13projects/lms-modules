﻿using System;

namespace Olimp.Service.Contract.SyncService.Attempts
{
    public class Ticket
    {
        public int Id { get; set; }

        public int Number { get; set; }

        public int QuestionsCount { get; set; }

        public Guid UniqueId { get; set; }
    }
}