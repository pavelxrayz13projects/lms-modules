﻿namespace Olimp.Service.Contract.SyncService.Attempts
{
    public class Employee
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string ThirdName { get; set; }

        public string Number { get; set; }

        public string Company { get; set; }

        public Position[] Positions { get; set; }
    }
}