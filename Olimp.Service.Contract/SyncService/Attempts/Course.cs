﻿using System;

namespace Olimp.Service.Contract.SyncService.Attempts
{
    public class Course
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string FileName { get; set; }

        public string Name { get; set; }

        public Guid UniqueId { get; set; }

        public int Version { get; set; }

        public string Checksum { get; set; }

        public int MaxErrors { get; set; }

        public bool IsCommercial { get; set; }
    }
}