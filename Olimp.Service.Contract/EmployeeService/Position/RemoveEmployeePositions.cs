﻿using System;

namespace Olimp.Service.Contract.EmployeeService.Position
{
    public class RemoveEmployeePositions
    {
        public string[] Positions { get; set; }

        public Guid EmployeeId { get; set; }
    }
}