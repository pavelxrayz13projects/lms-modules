﻿namespace Olimp.Service.Contract.EmployeeService.Position
{
    public class AddEmployeePositionResponse
    {
        public EmployeePositionState Result { get; set; }
    }
}