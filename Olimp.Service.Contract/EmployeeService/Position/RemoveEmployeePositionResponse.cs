﻿namespace Olimp.Service.Contract.EmployeeService.Position
{
    public class RemoveEmployeePositionsResponse
    {
        public EmployeePositionState[] Results { get; set; }
    }
}