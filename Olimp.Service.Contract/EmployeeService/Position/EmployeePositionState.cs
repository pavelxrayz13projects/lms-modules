﻿namespace Olimp.Service.Contract.EmployeeService.Position
{
    public enum EmployeePositionState
    {
        Added,
        Removed,
        Ignored
    }
}