﻿using System;

namespace Olimp.Service.Contract.EmployeeService.Position
{
    public class AddEmployeePosition
    {
        public string Position { get; set; }

        public Guid EmployeeId { get; set; }
    }
}