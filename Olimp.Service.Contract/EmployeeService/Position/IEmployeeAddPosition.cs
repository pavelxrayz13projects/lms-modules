﻿namespace Olimp.Service.Contract.EmployeeService.Position
{
    public interface IEmployeeAddPosition
    {
        AddEmployeePositionResponse Post(AddEmployeePosition request);
    }
}