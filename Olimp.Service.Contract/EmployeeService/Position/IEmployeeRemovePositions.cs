﻿namespace Olimp.Service.Contract.EmployeeService.Position
{
    public interface IEmployeeRemovePositions
    {
        RemoveEmployeePositionsResponse Post(RemoveEmployeePositions request);
    }
}