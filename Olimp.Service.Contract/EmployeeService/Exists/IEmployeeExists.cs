﻿namespace Olimp.Service.Contract.EmployeeService.Exists
{
    public interface IEmployeeExists
    {
        EmployeeExistsResponse Get(EmployeeExists request);
    }
}