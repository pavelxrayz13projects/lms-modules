﻿using System;

namespace Olimp.Service.Contract.EmployeeService.Exists
{
    public class EmployeeExists
    {
        public Guid EmployeeId { get; set; }
    }
}