﻿namespace Olimp.Service.Contract.EmployeeService.Exists
{
    public class EmployeeExistsResponse
    {
        public bool Exists { get; set; }
    }
}