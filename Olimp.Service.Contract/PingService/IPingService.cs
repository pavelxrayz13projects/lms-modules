﻿namespace Olimp.Service.Contract.PingService
{
    public interface IPingService
    {
        PingResponse Post(PingRequest request);
    }
}