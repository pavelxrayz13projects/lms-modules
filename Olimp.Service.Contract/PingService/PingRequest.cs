﻿namespace Olimp.Service.Contract.PingService
{
    public class PingRequest
    {
        public string PingMessage { get; set; }
    }
}