﻿using System;

namespace Olimp.Service.Contract.PingService
{
    public class PingResponse
    {
        public string PingReply { get; set; }

        public DateTime ReplyTime { get; set; }
    }
}