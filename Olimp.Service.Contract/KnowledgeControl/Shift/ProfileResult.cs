﻿namespace Olimp.Service.Contract.KnowledgeControl.Shift
{
    public class ProfileResult
    {
        public int ProfileResultId { get; set; }

        public bool IsPrimary { get; set; }
    }
}