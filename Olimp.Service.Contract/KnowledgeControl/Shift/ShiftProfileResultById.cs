﻿namespace Olimp.Service.Contract.KnowledgeControl.Shift
{
    public class ShiftProfileResultById : ShiftProfilesBase
    {
        public int ProfileResultId { get; set; }

        public bool AutoUpdateToNewerProfileResult { get; set; }
    }
}