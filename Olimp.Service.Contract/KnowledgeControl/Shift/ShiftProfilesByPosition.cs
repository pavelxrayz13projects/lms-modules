﻿namespace Olimp.Service.Contract.KnowledgeControl.Shift
{
    public class ShiftProfilesByPosition : ShiftProfilesBase
    {
        public string Position { get; set; }
    }
}