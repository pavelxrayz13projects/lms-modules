﻿namespace Olimp.Service.Contract.KnowledgeControl.Shift
{
    public class ShiftProfilesByPositionResponse
    {
        public ProfileResult[] ProfileResults { get; set; }
    }
}