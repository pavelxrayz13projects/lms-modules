﻿namespace Olimp.Service.Contract.KnowledgeControl.Shift
{
    public class ShiftProfileResponseBase
    {
        public ProfileResult ProfileResult { get; set; }
    }
}