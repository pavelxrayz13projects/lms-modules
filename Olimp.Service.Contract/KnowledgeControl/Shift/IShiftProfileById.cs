﻿namespace Olimp.Service.Contract.KnowledgeControl.Shift
{
    public interface IShiftProfileById
    {
        ShiftProfileByIdResponse Post(ShiftProfileById request);
    }
}