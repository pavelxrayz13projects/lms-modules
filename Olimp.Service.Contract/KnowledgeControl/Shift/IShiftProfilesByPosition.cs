﻿namespace Olimp.Service.Contract.KnowledgeControl.Shift
{
    public interface IShiftProfilesByPosition
    {
        ShiftProfilesByPositionResponse Post(ShiftProfilesByPosition request);
    }
}