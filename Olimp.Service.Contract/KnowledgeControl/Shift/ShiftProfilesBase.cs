﻿using System;

namespace Olimp.Service.Contract.KnowledgeControl.Shift
{
    public abstract class ShiftProfilesBase
    {
        public Guid EmployeeId { get; set; }

        public DateTime NextDate { get; set; }
    }
}