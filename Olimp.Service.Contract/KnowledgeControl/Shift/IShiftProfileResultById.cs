﻿namespace Olimp.Service.Contract.KnowledgeControl.Shift
{
    public interface IShiftProfileResultById
    {
        ShiftProfileResultByIdResponse Post(ShiftProfileResultById request);
    }
}