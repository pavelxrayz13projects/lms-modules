﻿namespace Olimp.Service.Contract.KnowledgeControl.Shift
{
    public class ShiftProfileById : ShiftProfilesBase
    {
        public int ProfileId { get; set; }
    }
}