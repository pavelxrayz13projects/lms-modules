﻿using System;

namespace Olimp.Service.Contract.KnowledgeControl.Journal
{
    public class JournalRecord
    {
        public int ProfileResultId { get; set; }

        public Employee Employee { get; set; }

        public DateTime DatePassed { get; set; }

        public string KnowledgeControlTitle { get; set; }
    }
}