﻿namespace Olimp.Service.Contract.KnowledgeControl.Journal
{
    public class JournalData
    {
        public Filter Filter { get; set; }

        public Page Page { get; set; }
    }
}