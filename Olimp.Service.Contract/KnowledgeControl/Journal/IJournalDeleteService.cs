﻿namespace Olimp.Service.Contract.KnowledgeControl.Journal
{
    public interface IJournalDeleteService
    {
        DeleteFromJournalResponse Post(DeleteFromJournal dto);
    }
}