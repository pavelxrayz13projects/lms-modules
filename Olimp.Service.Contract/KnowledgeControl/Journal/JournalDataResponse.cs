﻿namespace Olimp.Service.Contract.KnowledgeControl.Journal
{
    public class JournalDataResponse
    {
        public JournalRecord[] Records { get; set; }
    }
}