﻿namespace Olimp.Service.Contract.KnowledgeControl.Journal
{
    public enum DeletionResult
    {
        Skipped,
        Deleted,
        Failed
    }
}