﻿using System;

namespace Olimp.Service.Contract.KnowledgeControl.Journal
{
    public class Filter
    {
        public Guid[] Employees { get; set; }

        public string EmployeeFullNameFilter { get; set; }

        public string[] Positions { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }
}