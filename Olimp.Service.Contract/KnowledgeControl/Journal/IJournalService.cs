﻿namespace Olimp.Service.Contract.KnowledgeControl.Journal
{
    public interface IJournalService
    {
        JournalDataResponse Post(JournalData request);
    }
}