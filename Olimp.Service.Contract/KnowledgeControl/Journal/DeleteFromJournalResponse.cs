﻿namespace Olimp.Service.Contract.KnowledgeControl.Journal
{
    public class DeleteFromJournalResponse
    {
        public DeletionResult[] DeletionResults { get; set; }
    }
}