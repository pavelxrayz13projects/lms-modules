﻿namespace Olimp.Service.Contract.KnowledgeControl
{
    public class Page
    {
        public int? PageNumber { get; set; }

        public int? PageSize { get; set; }

        public bool? SortAsc { get; set; }

        public string SortByColumn { get; set; }
    }
}