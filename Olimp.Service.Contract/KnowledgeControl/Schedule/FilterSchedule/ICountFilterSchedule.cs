﻿namespace Olimp.Service.Contract.KnowledgeControl.Schedule.FilterSchedule
{
    public interface ICountFilterSchedule
    {
        CountFilterScheduleResponse Post(CountFilterSchedule request);
    }
}