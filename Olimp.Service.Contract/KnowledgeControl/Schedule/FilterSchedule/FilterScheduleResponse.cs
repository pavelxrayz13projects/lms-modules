﻿namespace Olimp.Service.Contract.KnowledgeControl.Schedule.FilterSchedule
{
    public class FilterScheduleResponse
    {
        public ScheduleRecord[] FilteredSchedule { get; set; }
    }
}