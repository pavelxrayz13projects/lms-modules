﻿namespace Olimp.Service.Contract.KnowledgeControl.Schedule.FilterSchedule
{
    public class Profile
    {
        public int Id { get; set; }

        public string Title { get; set; }
    }
}