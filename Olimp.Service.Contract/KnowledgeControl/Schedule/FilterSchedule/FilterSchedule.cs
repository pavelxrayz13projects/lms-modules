﻿namespace Olimp.Service.Contract.KnowledgeControl.Schedule.FilterSchedule
{
    public class FilterSchedule
    {
        public Filter Filter { get; set; }

        public Page Page { get; set; }
    }
}