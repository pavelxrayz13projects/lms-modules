﻿namespace Olimp.Service.Contract.KnowledgeControl.Schedule.FilterSchedule
{
    public interface IFilterSchedule
    {
        FilterScheduleResponse Post(FilterSchedule request);
    }
}