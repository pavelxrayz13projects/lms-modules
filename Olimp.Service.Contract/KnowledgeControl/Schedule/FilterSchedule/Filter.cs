﻿using System;

namespace Olimp.Service.Contract.KnowledgeControl.Schedule.FilterSchedule
{
    public class Filter
    {
        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string[] Positions { get; set; }

        public Guid[] Employees { get; set; }

        public string EmployeeFullNameFilter { get; set; }
    }
}