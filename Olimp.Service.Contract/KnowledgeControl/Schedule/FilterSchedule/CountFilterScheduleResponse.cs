﻿namespace Olimp.Service.Contract.KnowledgeControl.Schedule.FilterSchedule
{
    public class CountFilterScheduleResponse
    {
        public int Count { get; set; }
    }
}