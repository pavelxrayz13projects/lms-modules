﻿using System;

namespace Olimp.Service.Contract.KnowledgeControl.Schedule.FilterSchedule
{
    public class ScheduleRecord
    {
        public Employee Employee { get; set; }

        public Profile Profile { get; set; }

        public DateTime? PreviousDate { get; set; }

        public DateTime NextDate { get; set; }
    }
}