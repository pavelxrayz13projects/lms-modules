﻿namespace Olimp.Service.Contract.KnowledgeControl.Schedule.ForEmployee
{
    public interface IKnowledgeControlForEmployees
    {
        KnowledgeControlForEmployeesResponse Post(GetKnowledgeControlForEmployees request);
    }
}