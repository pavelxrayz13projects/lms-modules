﻿using System;

namespace Olimp.Service.Contract.KnowledgeControl.Schedule.ForEmployee
{
    public class GetKnowledgeControlForEmployees
    {
        public string Referer { get; set; }

        public Guid[] Employees { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public bool WithTicketsOnly { get; set; }
    }
}