﻿namespace Olimp.Service.Contract.KnowledgeControl.Schedule.ForEmployee
{
    public class CountKnowledgeControlForEmployeesResponse
    {
        public int Count { get; set; }
    }
}