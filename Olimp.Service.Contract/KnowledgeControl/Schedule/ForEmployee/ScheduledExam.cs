﻿namespace Olimp.Service.Contract.KnowledgeControl.Schedule.ForEmployee
{
    public class ScheduledExam
    {
        public string FullName { get; set; }

        public string TestUrl { get; set; }

        public string TestUrlRequestMethod { get; set; }

        public bool HasTickets { get; set; }

        public bool IsFinished { get; set; }

        public bool IsPassed { get; set; }
    }
}