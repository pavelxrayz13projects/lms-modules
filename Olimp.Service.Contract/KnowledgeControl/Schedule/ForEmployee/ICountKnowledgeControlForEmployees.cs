﻿namespace Olimp.Service.Contract.KnowledgeControl.Schedule.ForEmployee
{
    public interface ICountKnowledgeControlForEmployees
    {
        CountKnowledgeControlForEmployeesResponse Post(CountKnowledgeControlForEmployees request);
    }
}