﻿using System.Collections.Generic;

namespace Olimp.Service.Contract.KnowledgeControl.Schedule.ForEmployee
{
    public class KnowledgeControlForEmployeesResponse
    {
        public Dictionary<Employee, ScheduledExam[]> EmployeesHavingExams { get; set; }
    }
}