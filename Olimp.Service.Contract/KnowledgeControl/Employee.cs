﻿using System;

namespace Olimp.Service.Contract.KnowledgeControl
{
    public class Employee
    {
        public Guid Id { get; set; }

        public string FullName { get; set; }
    }
}