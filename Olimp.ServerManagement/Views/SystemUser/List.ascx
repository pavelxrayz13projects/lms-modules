﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<SystemUserEditViewModel>" %>
<%@ Import Namespace="I18N=Olimp.ServerManagement.I18N" %>

<div id="system-user-edit-dialog">	
    <% Html.RenderPartial("~/Olimp.ServerManagement/Views/SystemUser/Edit.ascx"); %> 
</div>		

<% Html.Table(
        new TableOptions {
            SelectUrl = Url.Action("GetSystemUsers", "SystemUser"),
            RowLocked = "IsLocked",
            ManualLoad = true,
            Adding = new AddingOptions { 
                Text = I18N.ServerManagement.Add,
                New = new {
                    Id = 0, 
                    Login = "", 
                    Password = "",
                    ConfirmPassword = "",
                    IsBlocked = false,
                    IsGlobal = false,
                    Roles = new string[0],
                    ExcludedPermissions = new string[0],
                    Nodes = new[] { Model.GlobalModel.CurrentNodeId }
                }
            },
            Columns = new[] { 
                new ColumnOptions("Login", I18N.SystemUser.Login),
                new ColumnOptions("RolesString", "Роли"),
                new ColumnOptions("NodesString", "Узлы") 
            },
            Actions = new RowAction[] { 
                new EditAction("system-user-edit-dialog", new DialogOptions { Title = I18N.SystemUser.Edit, Width = "580px" }),
                new DeleteAction(Url.Action("Delete", "SystemUser"), I18N.SystemUser.ConfirmDelete)	}
        }, new { id = "system-users-table" }); %>
    
<script type="text/javascript">
    $.fn.multiselect.initValidation();
    $.validator.unobtrusive.adapters.add(
        'requiredifbool',
        ['boolProperty'],
        function (options) {
            options.rules['requiredifbool'] = options.params;
            options.messages['requiredifbool'] = options.message;
        }
    );
    $.validator.addMethod('requiredifbool', function (value, element, params) {
        var checked = $('input:hidden[name=' + params.boolProperty + ']:checked').length > 0;
        if (!checked && !value)
            return false;

        return true;
    }, '');

    $(function () {
        $("#add-system-user").olimpbutton();

        $('#system-user-edit-dialog').bind('afterApply', function () {
            $(".multiselect").multiselect();

            var isGlobalCb = $('#is-global-cb');

            function enableDisableNodesList() {
                var isGlobal = isGlobalCb.prop('checked');

                $('#select-nodes').multiselect("option", "disabled", isGlobal);
            }

            isGlobalCb.click(enableDisableNodesList);
            enableDisableNodesList();
        });
    });
</script>