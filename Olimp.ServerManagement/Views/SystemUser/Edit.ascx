﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<SystemUserEditViewModel>" %>
<%@ Import Namespace="I18N=Olimp.ServerManagement.I18N" %>

<% Html.EnableClientValidation(); %>
    
<% using(Html.BeginForm("Edit", "SystemUser")) { %>
    
    <%= Html.HiddenFor(m => m.Id, new { data_bind = "value: Id" }) %>

    <table class="form-table">
        <tr class="first">
            <th><%= Html.LabelFor(m => m.Login) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Login) %></div>
                <%= Html.TextBoxFor(m => m.Login, new { data_bind = "value: Login" }) %>
            </td>
        </tr>	
        <tr>
            <th><%= Html.LabelFor(m => m.Password) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Password) %></div>
                <%= Html.TextBoxFor(m => m.Password, new { data_bind = "value: Password", type = "password" }) %>
            </td>
        </tr>	
        <tr>
            <th><%= Html.LabelFor(m => m.ConfirmPassword) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ConfirmPassword) %></div>
                <%= Html.TextBoxFor(m => m.ConfirmPassword, new { data_bind = "value: ConfirmPassword", type = "password" }) %>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.IsBlocked, "Запретить вход") %></th>
            <td class="checkbox">
                <%= Html.CheckBoxFor(m => m.IsBlocked, new { data_bind = "checked: IsBlocked" }) %>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.Roles) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Roles) %></div>
                <%= Html.ListBoxFor(m => m.Roles, Model.GlobalModel.AllRoles, new { data_bind = "selectedOptions: Roles", @class = "multiselect" }) %>                
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.ExcludedPermissions) %></th>
             <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ExcludedPermissions) %></div>
                <%= Html.ListBoxFor(m => m.ExcludedPermissions, Model.GlobalModel.AllPermissions, new { data_bind = "selectedOptions: ExcludedPermissions", @class = "multiselect" }) %>                
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.IsGlobal) %></th>
            <td class="checkbox">
                <% 
                    var isGlobalAttrs = new Dictionary<string, object> { 
                        { "data-bind", "checked: IsGlobal" },
                        { "id", "is-global-cb" } };

                    if (!Model.GlobalModel.CanManageGlobal)
                        isGlobalAttrs.Add("disabled", "disabled");
                %>

                <%= Html.CheckBoxFor(m => m.IsGlobal, isGlobalAttrs) %>
            </td>
        </tr>
        <tr class="last">
            <th><%= Html.LabelFor(m => m.Nodes) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Nodes) %></div>
                <%= Html.ListBoxFor(m => m.Nodes, Model.GlobalModel.AllNodes, new { data_bind = "selectedOptions: Nodes", @class = "multiselect", id = "select-nodes" }) %>
            </td>
        </tr>
    </table>
<% } %>