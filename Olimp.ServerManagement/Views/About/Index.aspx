﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<AboutViewModel>"  MasterPageFile="~/Views/Shared/SimpleLayout.master" %>
<%@ Import Namespace="I18N=Olimp.ServerManagement.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_Content" runat="server">

	<h1><%= I18N.About.AboutTitle %> (<a href="#" class="link-button" id="sysinfo-link">скрыть</a>)</h1>	

	<div class="block">
		<table class="form-table" id="sysinfo">
			<tr class="first">	 
				<th>Версия программного комплекса:</th>
				<td><%= Model.ProgrammVersion %></td>
			</tr>
            
            <% if (Model.ShowServerInfo) { %>
			<tr>	
				<th>Путь установки:</th>
				<td><%= Model.OlimpPath %></td>
			</tr>
            <% } %>

            <% if (Model.ShowClientInfo) { %>
            <tr>
                <th>Идентификатор узла:</th>
                <td><%= Model.NodeName %></td>
            </tr>
            <% } %>

            <% if (Model.ShowServerInfo) { %>
			<tr>	
				<th>Операционная система:</th>
				<td><%= Model.SystemName %></td>
			</tr>
            <% } %>

            <% if (Model.ShowClientInfo) { %>
			<tr>	
				<th>Текущее время на сервере:</th>
				<td><%= Model.ServerNow %></td>
			</tr>
			<tr>	
				<th>Версия обозревателя:</th>
				<td id="user-agent"></td>
			</tr>
			<noscript>
				<tr>	
					<th>Javascript:</th>
					<td>Отключен</td>
				</tr>
			</noscript>
            <% } %>

			<tr>	
				<th>Обладатель лицензии:</th>
				<td>
                    <%= !String.IsNullOrWhiteSpace(Model.NodeCompanyName) 
                        ? Model.NodeCompanyName 
                        : Model.CompanyName %>
				</td>
			</tr>

            <% if (Model.ShowServerInfo) { %>
			<tr>	
				<th>Идентификатор дистрибутива:</th>
				<td><%= Model.DistributionId %></td>
			</tr>
			<tr>	
				<th>Ключ привязки:</th>
				<td><%= Model.BindingKey %></td>
			</tr>
            <% } %>

			<tr>	
				<th>Активация:</th>
				<td>
					<% if (Model.ActivationState == ActivationState.Exclusive) { %>
						<span>Эксклюзивный режим</span>
					<% } else if (Model.IsTrial && Model.ActivationState == ActivationState.OK) { %>
						<span>Пробный период</span>
					<% } else if (Model.IsTrial && Model.ActivationState == ActivationState.Expired) { %>
						<span>Пробный период истек</span>
					<% } else if (Model.ActivationState == ActivationState.OK) { %>
						<span>Выполнена</span>
					<% } else { %>
						<span>Не выполнена</span>
					<% } %>				
				</td>
			</tr>	

            <% if (Model.IsTrial) { %>
            <tr>	
				<th>Окончание пробного периода:</th>
				<td><%= Model.LicenseExpires %></td>
			</tr>
            <% } %>

			<% if (Model.ShowServerInfo && Model.ActivationState != ActivationState.Exclusive) { %>
			<tr>	
				<th>Номер активации:</th>
				<td><%= Model.ActivationNumber %></td>
			</tr>
			<tr>	
				<th>Последняя активация:</th>
				<td><%= Model.LastActivation %></td>
			</tr>
			<tr>	
				<th>Количество попыток активации:</th>
				<td><%= Model.ActivationAttempt %></td>
			</tr>			
			<% } %>

            <% if (Model.ShowClientInfo) { %>
			<tr>	
				<th>Количество рабочих мест:</th>
				<td>
					<%= Model.MaxUsers > -1 ? Model.MaxUsers.ToString() : "Не ограничено" %>
				</td>
			</tr>		            
			<tr<%= !Model.ShowServerInfo ? " class=\"last\"" : "" %>>	
				<th>Использовано рабочих мест:</th>
				<td id="consumed-users-count">0</td>
			</tr>
            <% } %>
            <% if (Model.ShowServerInfo) { %>
			<tr>	
				<th>Последнее обновление:</th>
				<td><%= Model.LastUpdate %></td>
			</tr>
			<tr class="last">	 
				<th>Последняя резервная копия:</th>
				<td><%= Model.LastBackup %></td>
			</tr>
            <% } %>
		</table>
	</div>
	
    <% if (Model.ShowServerInfo) { %>
	<script type="text/html" id="path-no-wrap">
		<span style="white-space: nowrap">{{= ko.utils.unwrapObservable(it.$data.Path) }}</span>
	</script>
			
	<script type="text/html" id="uid-no-wrap">
		<span style="white-space: nowrap">{{= ko.utils.unwrapObservable(it.$data.UniqueId) }}</span>
	</script>
			
	<script type="text/html" id="active-icon">
		<div style="margin-left: 50%;">
			{{? ko.utils.unwrapObservable(it.$data.IsActive) }}			
			<div style="margin-left: -8px;" class="icon icon-success"></div>			
			{{??}}
			<div style="margin-left: -8px;" class="icon icon-fail"></div>
			{{?}}
		</div>
	</script>
			
	<script type="text/html" id="exists-icon">
		<div style="margin-left: 50%;">
			{{? ko.utils.unwrapObservable(it.$data.Exists) }}
			<div style="margin-left: -8px;" class="icon icon-success"></div>
			{{??}}
			{{?}}
		</div>
	</script>
			
	<h1>Использованные рабочие места (<a href="#" class="link-button" id="user-slots-table-link">показать</a>)</h1>
	<div class="block">
		<div id="user-slots-table" style="display: none;">
		<% Html.Table(
			new TableOptions {
				SelectUrl = Url.Action("GetSlots"),
				ReloadInterval = 10000,
				OnReload = "function() { $('#consumed-users-count').text($('#user-slots-table .table:first').data('viewModel').rowsCount()) }",
				DefaultSortColumn = "IP",							
				BodyTemplateEngine = TemplateEngines.DoT,							
				Paging = false,				
				Columns = new[] { 
					new ColumnOptions("IP", "IP-адрес"),					
					new ColumnOptions("BrowserInfo", "Браузер"),
					new ColumnOptions("Token", "Идентификатор")					
				} }); %>
		</div>
	</div>

    <% } %>
			
	<script type="text/javascript">
	$(function() {	
		$("#user-agent").text(window.navigator.userAgent);
		
		$('#sysinfo-link').click(function() {
			var block = $('#sysinfo');
		
			if (block.is(':visible')) {
				$(this).text('показать')
				block.hide();
			} else {
				$(this).text('скрыть')				
				block.show();
			}
		
			return false;
		})
		<% if (Model.ShowServerInfo) { %>
		$('#user-slots-table-link').click(function() {
			var block = $('#user-slots-table');				
		
			if (block.is(':visible')) {
				$(this).text('показать')
				block.hide();
			} else {
				$(this).text('скрыть')				
				block.show();
			}
		
			return false;
		})
        <% } %>
	});
	</script>	
</asp:Content>