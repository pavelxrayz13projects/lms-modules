﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<AuthSchemeViewModel>" MasterPageFile="~/Views/Shared/ServerManagement.master" %>
<%@ Import Namespace="I18N=Olimp.ServerManagement.I18N" %>
<asp:Content ContentPlaceHolderID="Olimp_ServerManagement_Js" runat="server" >
    <% Html.EnableClientValidation(); %>
    
    <script type="text/javascript">
        $(function () {
            <% if (Model.SaveSuccess.GetValueOrDefault()) {%>
                $.Olimp.showSuccess('<%= Model.SaveMessage %>');
            <% } else if (!string.IsNullOrWhiteSpace(Model.SaveMessage)) {%>
                $.Olimp.showError('<%= Model.SaveMessage %>');
            <% } %>
        });
    </script>

    <script type="text/javascript">
        var defaultAuthScheme = {
                Id: 0, 
                Name: '' , 
                ServerName:'',
                ServerPort: 636,
                BaseDN: '',
                Login: '',
                Password: '',
                UseSsl: true
            },
            authSchemesMapping = {
                create: function (o) { return o.data; },
                key: function (o) { return ko.utils.unwrapObservable(o.Id); }
            },
            viewModel = {
                authSchemes: ko.mapping.fromJS(<%= Model.AuthSchemeString %>, authSchemesMapping),
                currentAuthScheme: ko.observable(),
                currentEditFormAuthScheme: ko.observable(defaultAuthScheme),
                deleteAuthScheme: function () {
                    if (this.currentAuthScheme() && confirm('<%= I18N.LdapAuthSchemes.DeleteConfirm %>'))
                        $('#delete-auth-scheme-form').submit();
                },
                addAuthScheme: function () {
                    this.currentEditFormAuthScheme(defaultAuthScheme);

                    $('#auth-scheme-edit-dialog').olimpsyncformdialog('open');
                },
                renameAuthScheme: function () {
                    if (!this.currentAuthScheme())
                        return;

                    this.currentEditFormAuthScheme(this.currentAuthScheme());

                    $('#auth-scheme-rename-dialog').olimpsyncformdialog('open');
                }
            },
            currentAuthSchemeIndex = viewModel.authSchemes.mappedIndexOf({ Id: <%= Model.Id %> });
    
        viewModel.currentAuthScheme(viewModel.authSchemes()[currentAuthSchemeIndex]);
        viewModel.currentAuthSchemeId = ko.computed(function() {
            var current = this.currentAuthScheme();
            return current ? current.Id : 0;
        }, viewModel);

        viewModel.renameAuthSchemeClass = ko.computed(function() {
            return this.currentAuthScheme() ? 'icon icon-pencil-active' : 'icon icon-pencil-inactive';
        }, viewModel);

        viewModel.deleteAuthSchemeClass = ko.computed(function() {
            return this.currentAuthScheme() ? 'icon icon-delete-active' : 'icon icon-delete-inactive';
        }, viewModel);
               
                
        $(function () {
            ko.applyBindings(viewModel, $('#auth-scheme-block')[0]);
            viewModel.currentAuthSchemeId.subscribe(function() {
                $('#switch-auth-scheme-form').submit();
            });

            $('#auth-scheme-edit-dialog').olimpsyncformdialog({ title: '<%= I18N.LdapAuthSchemes.AuthScheme %>', width: 650 });
            $('#auth-scheme-rename-dialog').olimpsyncformdialog({ title: '<%= I18N.LdapAuthSchemes.AuthScheme %>', width: 650 });
            $('#current-auth-scheme').combobox();
        });
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Olimp_ServerManagement_Content" runat="server">
    <h1><%= I18N.LdapAuthSchemes.AuthSchemeTitle %></h1>

    <div id="auth-scheme-block">
        <% using (Html.BeginForm("DeleteAuthScheme", "AuthScheme", FormMethod.Post, new { id="delete-auth-scheme-form" }))
           { %>
            <%= Html.Hidden("authSchemeId", 0, new { data_bind = "value: currentAuthSchemeId" })%>
        <% } %>

        <% using (Html.BeginForm("Index", "AuthScheme", FormMethod.Get, new { id = "switch-auth-scheme-form", @class = "block" }))
           { %>
            <%= Html.Hidden("authSchemeId", 0, new { data_bind = "value: currentAuthSchemeId" })%>
            <table class="form-table" style="margin-bottom: 6px;">
                <tr class="first last">
                    <th><label><%= I18N.LdapAuthSchemes.ChooseAuthScheme %></label></th>
                    <td>
                        <table style="border-collapse: collapse; width: 100%;">
                            <tr>
                                <td>
                                    <select id="current-auth-scheme" data-bind="
                                        options: authSchemes,
                                        optionsText: 'Name',
                                        value: currentAuthScheme">
                                    </select>
                                </td>
                                <td style="width: 1px;">
                                    <a data-bind="click: addAuthScheme"
                                        href="#" 
                                        class="action icon icon-add" 
                                        style="margin-left:10px;" 
                                        title="<%= I18N.LdapAuthSchemes.AddAuthScheme %>">
                                    </a>
                                </td>
                                <td style="width: 1px;">
                                    <a data-bind="click: renameAuthScheme, attr: { 'class': renameAuthSchemeClass }"
                                        href="#" 
                                        style="margin-left:10px;" 
                                        title="<%= I18N.LdapAuthSchemes.EditAuthScheme %>">
                                    </a>
                                </td>
                                <td style="width: 1px;">
                                    <a data-bind="click: deleteAuthScheme, attr: { 'class': deleteAuthSchemeClass }"
                                        href="#" 
                                        style="margin-left:10px;" 
                                        title="<%= I18N.LdapAuthSchemes.DeleteAuthScheme %>">
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        <% } %>

        <div id ="auth-scheme-edit-dialog" style="display:none">
            <% Html.RenderPartial("EditAuthScheme"); %>
        </div>

        <div id ="auth-scheme-rename-dialog" style="display:none">
            <% Html.RenderPartial("RenameAuthScheme"); %>
        </div>
    </div>
    <% if (Model.Id > 0)
       { %>
        <div id="auth-scheme-manage-block">
            <% using (Html.BeginForm("Save", "AuthScheme", FormMethod.Post, new {id = "auth-scheme-form"}))
               { %>
                <div id="auth-scheme-params">
                    <% Html.RenderPartial("AuthSchemeForm"); %>
                </div>
            <% } %>
        </div>
    <% } %>
</asp:Content>