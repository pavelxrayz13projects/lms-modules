﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<AuthSchemeViewModel>" %>
<%@ Import Namespace="I18N=Olimp.ServerManagement.I18N" %>

<% using (Html.BeginForm("Save", "AuthScheme", FormMethod.Post, new {id = "auth-scheme-dialog-form"}))
    {%>
    <%= Html.HiddenFor(m => m.Id, new { data_bind = "value: currentEditFormAuthScheme().Id" })%>

    <table class="form-table">
        <tr class="first">
            <th><%= Html.LabelFor(m => m.Name)%></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Name) %></div>
                <%= Html.TextBoxFor(m => m.Name, new { data_bind = "value: currentEditFormAuthScheme().Name" })%>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.ServerName)%></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ServerName) %></div>
                <%= Html.TextBoxFor(m => m.ServerName, new { data_bind = "value: currentEditFormAuthScheme().ServerName" })%>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.ServerPort)%></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ServerPort) %></div>
                <%= Html.TextBoxFor(m => m.ServerPort, new { data_bind = "value: currentEditFormAuthScheme().ServerPort" })%>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.UseSsl)%></th>
            <td class="checkbox">
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.UseSsl) %></div>
                <%= Html.CheckBoxFor(m => m.UseSsl, new { data_bind = "value: currentEditFormAuthScheme().UseSsl" })%>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.BaseDN)%></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.BaseDN) %></div>
                <%= Html.TextBoxFor(m => m.BaseDN, new
                    {
                        data_bind = "value: currentEditFormAuthScheme().BaseDN",
                        placeholder = I18N.LdapAuthSchemes.BaseDNPlaceholder
                    })%>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.Login)%></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Login) %></div>
                <%= Html.TextBoxFor(m => m.Login, new
                    {
                        data_bind = "value: currentEditFormAuthScheme().Login",
                        placeholder = I18N.LdapAuthSchemes.LoginPlaceholder
                    })%>
            </td>
        </tr>
        <tr class="last">
            <th><%= Html.LabelFor(m => m.Password)%></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Password) %></div>
                <%= Html.TextBoxFor(m => m.Password, new {
                        data_bind = "value: currentEditFormAuthScheme().Password",
                        type = "password"
                    })%>
            </td>
        </tr>
    </table>
<% } %>