﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<AuthSchemeViewModel>" %>
<%@ Import Namespace="I18N=Olimp.ServerManagement.I18N" %>
<%= Html.HiddenFor(m => m.Id) %>
<%= Html.HiddenFor(m => m.Name) %>

<h1><%= I18N.LdapAuthSchemes.BaseSettings %></h1>
<div class="block">
    <table class="form-table">
        <tr class="first">
            <th><%= Html.LabelFor(m => m.ServerName) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ServerName) %></div>
                <div><%= Html.TextBoxFor(m => m.ServerName) %></div>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.ServerPort) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ServerPort) %></div>
                <div><%= Html.TextBoxFor(m => m.ServerPort) %></div>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.UseSsl) %></th>
            <td class="checkbox">
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.UseSsl) %></div>
                <div><%= Html.CheckBoxFor(m => m.UseSsl) %></div>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.BaseDN) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.BaseDN) %></div>
                <div>
                    <%= Html.TextBoxFor(m => m.BaseDN, new { placeholder = I18N.LdapAuthSchemes.BaseDNPlaceholder }) %>
                </div>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.Login) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Login) %></div>
                <div>
                    <%= Html.TextBoxFor(m => m.Login, new
                         {
                             placeholder = I18N.LdapAuthSchemes.LoginPlaceholder
                         }) %>
                </div>
            </td>
        </tr>
        <tr class="last">
            <th><%= Html.LabelFor(m => m.Password) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Password) %></div>
                <div><%= Html.TextBoxFor(m => m.Password, new { type = "password" })%></div>
            </td>
        </tr>
    </table>
</div>

<h1><%= I18N.LdapAuthSchemes.FieldsCorrespondence %></h1>
<div class="block">
    <table class="form-table">
        <tr class="first">
            <th><%= Html.LabelFor(m => m.LoginField) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.LoginField) %></div>
                <div><%= Html.TextBoxFor(m => m.LoginField) %></div>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.NumberField) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.NumberField) %></div>
                <div><%= Html.TextBoxFor(m => m.NumberField) %></div>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.FullNameField) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.FullNameField) %></div>
                <div><%= Html.TextBoxFor(m => m.FullNameField) %></div>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.EmailField) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.EmailField) %></div>
                <div><%= Html.TextBoxFor(m => m.EmailField) %></div>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.CompanyField) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.CompanyField) %></div>
                <div><%= Html.TextBoxFor(m => m.CompanyField) %></div>
            </td>
        </tr>
        <tr>
            <th><%= Html.LabelFor(m => m.ProfessionField) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ProfessionField) %></div>
                <div><%= Html.TextBoxFor(m => m.ProfessionField) %></div>
            </td>
        </tr>
        <tr class="last">
            <th><%= Html.LabelFor(m => m.LocaleField) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.LocaleField) %></div>
                <div><%= Html.TextBoxFor(m => m.LocaleField) %></div>
            </td>
        </tr>
    </table>
</div>

<h1><%= I18N.LdapAuthSchemes.FilterSettings %></h1>
<div class="block">
    <table class="form-table">
        <tr class="first">
            <th><%= Html.LabelFor(m => m.ImportOnlyActiveUsers) %></th>
            <td class="checkbox"><%= Html.CheckBoxFor(m => m.ImportOnlyActiveUsers) %></td>
        </tr>
        <tr class="last">
            <th><%= Html.LabelFor(m => m.GroupDN) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.GroupDN) %></div>
                <div>
                    <%= Html.TextBoxFor(m => m.GroupDN, new
                         {
                             placeholder = I18N.LdapAuthSchemes.GroupDNPlaceholder
                         }) %>
                </div>
            </td>
        </tr>
    </table>
</div>

<h1><%= I18N.LdapAuthSchemes.ServiceSettings %></h1>
<div class="block">
    <table class="form-table">
        <tr class="first">
            <th><%= Html.LabelFor(m => m.IsPublic) %></th>
            <td class="checkbox"><%= Html.CheckBoxFor(m => m.IsPublic) %></td>
        </tr>
        <tr class="last">
            <th><%= Html.LabelFor(m => m.IsEnabled) %></th>
            <td class="checkbox"><%= Html.CheckBoxFor(m => m.IsEnabled) %></td>
        </tr>
    </table>
</div>

<button type="submit"><%= Olimp.I18N.PlatformShared.Save %></button>

<script type="text/javascript">
    $(function () {
        $('button').olimpbutton();
    });
</script>