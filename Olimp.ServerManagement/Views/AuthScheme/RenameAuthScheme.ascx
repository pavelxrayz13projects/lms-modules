﻿<%@ Control Language="C#" Inherits="OlimpViewUserControl<AuthSchemeViewModel>" %>

<% using (Html.BeginForm("Rename", "AuthScheme", FormMethod.Post, new {id = "auth-scheme-rename-dialog-form"}))
    {%>
    <%= Html.HiddenFor(m => m.Id, new { data_bind = "value: currentEditFormAuthScheme().Id" })%>

    <table class="form-table">
        <tr class="first last">
            <th><%= Html.LabelFor(m => m.Name)%></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.Name) %></div>
                <%= Html.TextBoxFor(m => m.Name, new { data_bind = "value: currentEditFormAuthScheme().Name" })%>
            </td>
        </tr>
    </table>
<% } %>