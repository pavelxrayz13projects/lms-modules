﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<LdapPermissionRulesModel>" %>
<%@ Import Namespace="I18N=Olimp.ServerManagement.I18N" %>

<div id="ldap-permission-rules-edit-dialog">
    <% if(Model.CanManageLdapPermissions) { 
           Html.RenderPartial("~/Olimp.ServerManagement/Views/LdapPermissionRules/Edit.ascx", new LdapRuleEditViewModel()
           {
               GlobalModel = Model.GlobalModel
           });
       } %>	
</div>	

<% Html.Table(
        new TableOptions
        {
            SelectUrl = Url.Action("GetLdapPermissionRules", "LdapPermissionRules"),
            OmitTemplates = true,
            ManualLoad = true,
            Adding = new AddingOptions
            {
                Disabled = !Model.CanManageLdapPermissions,
                Text = I18N.ServerManagement.Add,
                New = new
                {
                    Id = 0,
                    LdapGroupDN = "",
                    RoleId = "",
                    NodeId = "",
                    ExcludedPermissions = new string[0],
                }
            },
            Columns = new[] 
            { 
                new ColumnOptions("RoleName", Olimp.I18N.Domain.LdapPermissionRule.Role),
                new ColumnOptions("NodeName", Olimp.I18N.Domain.LdapPermissionRule.Node),
                new ColumnOptions("LdapGroupDNWithSpaces", Olimp.I18N.Domain.LdapPermissionRule.LdapGroupDN) 
            },
            Actions = new RowAction[] 
            { 
                new EditAction("ldap-permission-rules-edit-dialog", new DialogOptions { Title = I18N.LdapPermissionRules.Editing, Width = "580px" })
                {
                    Disabled = !Model.CanManageLdapPermissions
                },
                new DeleteAction(Url.Action("Delete", "LdapPermissionRules"), I18N.SystemUser.ConfirmDelete)
                {
                    Disabled = !Model.CanManageLdapPermissions
                }	
            }
        }, new { id = "ldap-rules-table" }); %>

<script type="text/javascript">
    $(function () {
        $('#ldap-permission-rules-edit-dialog').bind('afterApply', function () {
            $(".multiselect").multiselect();
        });
    });
</script>