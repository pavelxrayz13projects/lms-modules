﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<LdapRuleEditViewModel>" %>
<%@ Import Namespace="I18N=Olimp.ServerManagement.I18N" %>

<% Html.EnableClientValidation(); %>
    
<% using(Html.BeginForm("Edit", "LdapPermissionRules")) { %>
    
    <%= Html.HiddenFor(m => m.Id, new Dictionary<string, object> { 
        { "data-bind", "value: Id" } }) %>

    <table class="form-table">
        <tr class="first">
            <th><%= Html.LabelFor(m => m.RoleId) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.RoleId) %></div>
                <%= Html.DropDownListFor(m => m.RoleId, Model.GlobalModel.AllRoles, new { data_bind = "value: RoleId" }) %>                
            </td>
        </tr> 
        <tr>
            <th><%= Html.LabelFor(m => m.ExcludedPermissions) %></th>
             <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.ExcludedPermissions) %></div>
                <%= Html.ListBoxFor(m => m.ExcludedPermissions, Model.GlobalModel.AllPermissions, new { data_bind = "selectedOptions: ExcludedPermissions", @class = "multiselect" }) %>                
            </td>
        </tr>       
        <tr>
            <th><%= Html.LabelFor(m => m.NodeId) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.NodeId) %></div>
                <%= Html.DropDownListFor(m => m.NodeId, Model.GlobalModel.AllNodes, I18N.LdapPermissionRules.AllNodes, new { data_bind = "value: NodeId" }) %>
            </td>
        </tr>	
        <tr class="last">
            <th><%= Html.LabelFor(m => m.LdapGroupDN) %></th>
            <td>
                <div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.LdapGroupDN) %></div>
                <%= Html.TextBoxFor(m => m.LdapGroupDN, new { data_bind = "value: LdapGroupDN" }) %>
            </td>
        </tr>	
    </table>
<% } %>