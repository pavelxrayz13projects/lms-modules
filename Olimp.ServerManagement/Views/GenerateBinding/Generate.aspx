﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<ActivationViewModel>" MasterPageFile="~/Views/Shared/ServerManagement.master" %>

<asp:Content ContentPlaceHolderID="Olimp_ServerManagement_Content" runat="server">

    <h1>Активация</h1>

    <div class="block">
        <table class="form-table">
            <tr class="first">
                <th>Ключ привязки</th>
                <td data-bind="text: bindingKey"></td>
            </tr>
            <tr data-bind="attr: { 'class': activationStateClass }">
                <th>Состояние активации</th>
                <td data-bind="text: activationState"></td>
            </tr>
            <tr data-bind="visible: informationFieldsVisible">
                <th>Номер активации</th>
                <td data-bind="text: activationNumber"></td>
            </tr>
			<tr data-bind="visible: expirationFieldVisible">
                <th>Окончание обслуживания</th>
                <td data-bind="text: licenceExpires"></td>
            </tr>
            <tr data-bind="attr: { 'class': maxUsersClass }, visible: isActivated">
                <th>Число пользователей</th>
                <td data-bind="text: maxUsersString"></td>
            </tr>
            <tr data-bind="attr: { 'class': activationTypeClass }, visible: activationFormVisible">
                <th>
                    <!-- ko ifnot: isActivated -->
                    Тип активации<!-- /ko -->
                    <!-- ko if: isActivated -->
                    Реактивация<!-- /ko -->
                </th>
                <td class="table">
                    <table class="form-table">
                        <tr class="first">
                            <td style="width: 1px;" class="checkbox">
                                <input id="by-phone" type="radio" name="activation-type" value="phone"
                                    data-bind="checked: activationType" />
                            </td>
                            <td>
                                <label for="by-phone">По электронной почте</label>
                            </td>
                        </tr>
                        <tr class="last">
                            <td style="width: 1px;" class="checkbox">
                                <input id="by-internet" disabled="disabled" type="radio" name="activation-type" value="internet"
                                    data-bind="checked: activationType" />
                            </td>
                            <td>
                                <label for="by-internet">По интернету</label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr data-bind="visible: byInternet" class="last">
                <th>Шаг 1</th>
                <td>
                    <div>Активация будет осуществлена в автоматическом режиме. Для активации нажмите кнопку "Активация".</div>
                    <div style="margin-top: 24px;">
                        <button data-bind="click: activate">Активация</button>
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript">
        $(function () {
            var viewModel = {
                bindingKey: ko.observable('<%= Model.BindingKey %>'),
                isTrial: ko.observable(<%= Model.IsTrial.ToString().ToLower() %>),
                state: ko.observable('<%= Model.ActivationState %>'),
                maxUsers: ko.observable(<%= Model.MaxUsers %>),
                activationNumber: ko.observable(<%= Model.ActivationNumber %>),
                licenceExpires: ko.observable('<%= Model.LicenceExpires %>'),
                activationType: ko.observable(),
                activationKey: ko.observable(),
                activate: function () {

                    var intervalId = 0;
                    var self = this;
                    var handle_result = function (json) {
                        if (json.activationIsInProgress == true)
                            return false;

                        if (intervalId != 0) {
                            clearInterval(intervalId);
                            intervalId = 0;
                        } else {
                            return true;
                        }

                        $.Olimp.hideLoader();
                        $('#activateByPhoneBtn').removeAttr("disabled");

                        ko.mapping.fromJS(json.data, {}, self);

                        if (self.state() == '<%= ActivationState.OK %>') {
                            self.activationType(null);
                            alert('Активация выполнена успешно');
                        } else {
                            alert('Активация не удалась');
                        }
                        return true;
                    };

                    if (this.byPhone()) {
                        $('#activateByPhoneBtn').attr("disabled", "disabled");
                        $.Olimp.showLoader();
                        $.ajax({
                            url: '<%= Url.Action("ActivateByPhone") %>',
                            data: { key: this.activationKey() },
                            type: 'POST',
                            global: false
                        });
                        intervalId = setInterval(
                            function () {
                                $.ajax({
                                    url: '<%= Url.Action("GetState", "GenerateBinding", new { area="Admin" }) %>',
                                    success: handle_result,
                                    dataType: 'json',
                                    type: 'POST',
                                    global: false
                                });
                            }, 2000);
                    } else if (this.byInternet()) {
                        $.post('<%= Url.Action("ActivateByInternet") %>', success, 'json')
                    }
                }
            };

            viewModel.maxUsersString = ko.computed(function () {
                var users = this.maxUsers();
                if (users > 0)
                    return users;

                return 'Не ограничено';
            }, viewModel);

            viewModel.isExclusive = ko.computed(function () { return this.state() == '<%= ActivationState.Exclusive %>' }, viewModel)
            viewModel.isActivated = ko.computed(function () { return this.state() == '<%= ActivationState.OK %>' }, viewModel);
            viewModel.informationFieldsVisible = ko.computed(function () {
                var activated = this.isActivated(),
                    exclusive = this.isExclusive();

                return activated && !exclusive;
            }, viewModel)
		viewModel.expirationFieldVisible = ko.computed(function () {
		    var infoVisible = this.informationFieldsVisible(),
                isTrial = this.isTrial();

		    return infoVisible && isTrial;
		}, viewModel)
            viewModel.activationFormVisible = ko.computed(function () { return !this.isExclusive() }, viewModel)
            viewModel.activationState = ko.computed(function () {
                var state = this.state();
                if (this.isExclusive())
                    return 'Эксклюзивный режим'

                if (state == '<%= ActivationState.Expired %>')
                    return 'Пробный период истек';

                return state == '<%= ActivationState.OK %>'
                    ? (this.isTrial() ? 'Пробный период' : 'Активация выполнена')
                    : 'Активация не выполнена';
            }, viewModel)
            viewModel.byPhone = ko.computed(function () { return this.activationType() === 'phone' }, viewModel);
            viewModel.byInternet = ko.computed(function () { return this.activationType() === 'internet' }, viewModel);
            viewModel.activationTypeClass = ko.computed(function () {
                return this.byPhone() || this.byInternet() ? '' : 'last';
            }, viewModel);
            viewModel.activationStateClass = ko.computed(function () {
                return this.informationFieldsVisible() ? '' : 'last';
            }, viewModel);
            viewModel.maxUsersClass = ko.computed(function () { return !this.isExclusive() ? '' : 'last'; }, viewModel)

            ko.applyBindings(viewModel);

            $('button').olimpbutton();
        });
    </script>
</asp:Content>