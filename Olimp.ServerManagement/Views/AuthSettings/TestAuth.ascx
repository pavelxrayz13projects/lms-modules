﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<AuthSettingsViewModel>" %>
<%@ Import Namespace="I18N=Olimp.ServerManagement.I18N" %>

<% Html.EnableClientValidation(); %>

<div class="tip" style="margin-bottom: 10px;"><%= I18N.AuthSettings.TestAuthDialogTip %></div>
	
<% using (Html.BeginForm("SetAuthMethod", "AuthSettings")) { %>
	
	<%= Html.HiddenFor(m => m.LdapSchemeId, new { id = "current-scheme-id" }) %>

	<table class="form-table">
		<tr class="first">
			<th><%= Html.LabelFor(m => m.TestLogin) %></th>
			<td>
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.TestLogin) %></div>
				<%= Html.TextBoxFor(m => m.TestLogin) %>
			</td>
		</tr>	
		<tr class="last">
			<th><%= Html.LabelFor(m => m.TestPassword) %></th>
			<td>
				<div class="olimp-validation-error"><%= Html.ValidationMessageFor(m => m.TestPassword) %></div>
				<%= Html.TextBoxFor(m => m.TestPassword, new { type = "password" }) %>
			</td>
		</tr>	
	</table>
<% } %>			


	