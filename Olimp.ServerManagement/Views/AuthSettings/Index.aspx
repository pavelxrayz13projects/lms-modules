﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage<AuthSettingsViewModel>"  MasterPageFile="~/Views/Shared/ServerManagement.master" %>
<%@ Import Namespace="I18N=Olimp.ServerManagement.I18N" %>

<asp:Content ContentPlaceHolderID="Olimp_ServerManagement_Content" runat="server">

    <style>
        .current-auth-type { font-weight: bold; font-style: italic; }
    </style>

    <div id="test-auth-dialog">
        <% Html.RenderPartial("TestAuth"); %>
    </div>

    <h1>Авторизация</h1>
    <div class="block">
        <table class="form-table">            
            <tbody id="type-selector-section">
                <tr class="first">
                    <th style="width: 170px;"><%= I18N.AuthSettings.AuthMethod %></th>
                    <td class="table">
                        <table class="form-table">
                            <tr class="first">
                                <td style="width: 1px;" class="checkbox">
                                    <input id="auth-type-integrated" type="radio" name="auth-type" value="integrated" data-bind="checked: authType" />
                                </td>
                                <td>
                                    <label for="auth-type-integrated"><%= I18N.AuthSettings.ModeIntegrated %></label>
                                    <span class="current-auth-type" data-bind="visible: currentIsIntegrated">(<%= I18N.AuthSettings.Current %>)</span>
                                </td>
                            </tr>
                            <tr class="last">
                                <td style="width: 1px;" class="checkbox">
                                    <input id="auth-type-ldap" type="radio" name="auth-type" value="ldap" data-bind="checked: authType, enable: isLdapSwithDisable" />
                                </td>
                                <td>
                                    <label for="auth-type-ldap"><%= I18N.AuthSettings.ModeLdap %></label>
                                    <span class="current-auth-type" data-bind="visible: currentIsLdap, text: currentLdapSchemeText"></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="row-for-auth-type-ldap" style="display: none;">
                    <th><%= I18N.AuthSettings.AuthScheme %></th>
                    <td>
                        <div id="ldap-scheme-validation-error" class="olimp-validation-error">
                            <span class="field-validation-valid">
                                <span for="select-ldap-scheme" data-bind="visible: !isLdapSchemeIdValid()">
                                    <%= I18N.LdapAuthSchemes.LdapSchemeNotSelectedError %>
                                </span>
                            </span>
                        </div>

                        <%= Html.DropDownListFor(m => m.LdapSchemeId, Model.AllSchemes, new { 
                            data_bind = "value: ldapSchemeId",
                            id = "select-ldap-scheme"
                        }) %>
                    </td>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <td>
                        <button id="do-change-auth-method" type="button" data-bind="click: testAuthorize, enable: isLdapSwithDisable, attr: {'class': applySelectedMethodButtonClass}">
                            <%= I18N.AuthSettings.ApplySelectedMethod %>
                        </button>                        
                    </td>
                </tr>
            </tbody>
            <tbody id="section-for-auth-type-integrated" style="display: none">
                <tr class="last">
                    <th style="vertical-align: top;"><%= I18N.AuthSettings.ManageAccounts %></th>
                    <td class="table">                    
                        <% Html.RenderPartial("~/Olimp.ServerManagement/Views/SystemUser/List.ascx", new SystemUserEditViewModel() { GlobalModel = Model }); %>
                    </td>
                </tr>
            </tbody>
            <tbody>                
                <tr class="row-for-auth-type-ldap last" style="display: none">
                    <th style="vertical-align: top;"><%= I18N.AuthSettings.ManageRoles %></th>
                    <td class="table">
                        <% Html.RenderPartial("~/Olimp.ServerManagement/Views/LdapPermissionRules/List.ascx", new LdapPermissionRulesModel() {
                               GlobalModel = Model, 
                               CanManageLdapPermissions = Model.CanChangeLdapPermissionRules
                           }); %>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>       

    <script type="text/ecmascript">
        $(function () {
            <% if (!string.IsNullOrWhiteSpace(Model.DisplayText)) {%>
                    $.Olimp.showSuccess('<%= Model.DisplayText %>');
            <% } %>

            $('#do-change-auth-method').olimpbutton();

            var testAuthDialog = $('#test-auth-dialog').olimpdialog({
                width: 480,
                resizable: false,
                modal: true,
                autoOpen: false,
                title: '<%= I18N.AuthSettings.TestAuthDialogTitle %>',
                close: function () { $(this).find('form')[0].reset(); },
                buttons: {
                    '<%= I18N.AuthSettings.Apply %>': function () {
                        var form = $(this).find('form');
                        if (form.valid()) {
                            $.post(form.attr("action"), form.serialize(), function (e) {
                                if (e.authValid === true) {
                                    var text = encodeURIComponent(e.message),
                                        url;

                                    if (window.location.href.indexOf('?') === -1) {
                                        url = window.location.href + '?DisplayText=' + text;
                                    } else {
                                        url = window.location.href + '&DisplayText=' + text;
                                    }

                                    window.location.assign(url);
                                }
                                else {
                                    $.Olimp.showError(e.message);
                                }
                            });
                        }
                        return false;
                    },
                    '<%= Olimp.I18N.PlatformShared.Cancel %>': function() { $(this).olimpdialog('close') }
                }
            });
            
            var canManageGlobal = '<%= Model.CanManageGlobal %>' === "True" ? true : false;
            //var canChangeLdapPermissionRules = '<%= Model.CanChangeLdapPermissionRules %>' === "True" ? true : false;
            
            var viewModel = {
                authType: ko.observable(),
                currentAuthType: ko.observable(),
                ldapSchemeId: ko.observable(<%= Model.LdapSchemeId.GetValueOrDefault() %>),
                isLdapSwithDisable: ko.observable(canManageGlobal),
                applySelectedMethodButtonClass: ko.observable(canManageGlobal ?
                    'olimp-button' : 'olimp-button ui-olimpbutton-disabled ui-state-disabled'),
                setCurrentAuthType: function (type) {                    
                    this.currentAuthType(type);
                    this.authType(type);
                },
                testAuthorize: function () {
                    if (!this.isLdapSchemeIdValid())
                        return;

                    $('#current-scheme-id').val(this.currentLdapSchemeId());
                    testAuthDialog.olimpdialog('open');
                }
            };

            viewModel.authType.subscribe(function (value) {
                var table;

                if (value === 'ldap') {
                    table = $('#ldap-rules-table');
                    
                    $('#section-for-auth-type-integrated').hide();
                    $('.row-for-auth-type-ldap').show();
                } else if (value === 'integrated') {
                    table = $('#system-users-table');

                    $('#section-for-auth-type-integrated').show();
                    $('.row-for-auth-type-ldap').hide();
                }

                if (table && table.data('tableDataHasBeenLoaded') !== true)
                    table.data('tableDataHasBeenLoaded', true).data('viewModel').load();
            })

            viewModel.currentIsIntegrated = ko.computed(function () {
                return this.currentAuthType() === 'integrated';
            }, viewModel);

            viewModel.currentIsLdap = ko.computed(function () {
                return this.currentAuthType() === 'ldap';
            }, viewModel);

            viewModel.currentLdapSchemeId = ko.computed(function () {
                var selectedSchemeId = this.ldapSchemeId();

                return this.authType() !== 'integrated'
                    ? selectedSchemeId
                    : null;
            }, viewModel);
                        
            viewModel.currentLdapSchemeText = ko.computed(function () {
                var text = $('#select-ldap-scheme option[value=' + this.ldapSchemeId() + ']').text();
                return '(' + '<%= I18N.AuthSettings.Current %>' + ', ' + text + ')';
            }, viewModel);
            
            viewModel.isLdapSchemeIdValid = ko.computed(function () {
                if (this.authType() === 'integrated')
                    return true;

                return this.authType() === 'ldap' && !!this.currentLdapSchemeId();
            }, viewModel);

            ko.applyBindings(viewModel, $('#type-selector-section')[0]);            

            viewModel.setCurrentAuthType('<%= Model.LdapSchemeId.HasValue ? "ldap" : "integrated" %>');
        })
    </script>
</asp:Content>