﻿<%@ Page Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewPage"  MasterPageFile="~/Views/Shared/Profile.master" %>

<asp:Content ContentPlaceHolderID="Olimp_Profile_Content" runat="server">

	<% using (Html.BeginForm("Save", "ModeRegistrations", FormMethod.Post, new { id = "mode-registrations-form" })){ %>     

		<h1>Режим регистрации</h1>	

		<div id="moderegistrationform">	
			<% Html.RenderAction("ModeRegistrationForm"); %>
		</div>	
	<% } %>	
</asp:Content>