﻿<%@ Control Language="C#" Inherits="Olimp.Core.Mvc.OlimpViewUserControl<ModeRegistrationsViewModel>" %>
<% 	
	var strSelectedId = "";
	if(Model.ExamGroups != null)
	{
		foreach(var group in Model.ExamGroups) 
			strSelectedId += "-" + group + "-";		  						
	}
	var DetermineGroupsByExamPeriod = Model.Config.DetermineGroupsByExamPeriod;
%> 
<div class='block'>		
	<table class="form-table" id="config-registration-type">		
		<tr data-bind="attr: { 'class': firstRowClass }">
			<th><%= Html.LabelFor(m => m.Config.RegistrationType) %></th>
			<td>
				<% if (!Model.InternetVersion) { %>
					<%= Html.DropDownListFor(m => m.Config.RegistrationType, Model.RegistrationTypes , new Dictionary<string, object> {
						{ "data-bind", "value: registrationType" }
					})%>				
				<% } else { %>
					<%= Html.HiddenFor(m => m.Config.RegistrationType) %>
					<%= Olimp.I18N.Domain.RegistrationType.PreRegistration %>			
				<% } %>
			</td>
		</tr>	
		<tr data-bind="visible: isPreRegistration, attr: { 'class': lastRowClass }">
			<th><%= Html.LabelFor(m => m.Config.LoginType) %></th>
			<td>
                <%= Html.DropDownListFor(m => m.Config.LoginType, new Dictionary<string, object> {
                        { "data-bind", "value: loginType" }
                    })%>
			</td>
		</tr>	
        <tr data-bind="visible: fioRegistration">
			<th><%= Html.LabelFor(m => m.Config.RequireGivenName) %></th>
			<td class="checkbox"><%= Html.CheckBoxFor(m => m.Config.RequireGivenName) %></td>
		</tr>
		<tr data-bind="visible: fioRegistration, attr: { 'class': isExpandedRegistrationClass }">
			<th><%= Html.LabelFor(m => m.ExpandedRegistration) %></th>
			<td class="checkbox">
                <%= Html.CheckBoxFor(m => m.ExpandedRegistration, new Dictionary<string, object> { 
                    { "data-bind", "checked: isExpandedRegistration" }
                })%>
			</td>
		</tr>				
        <tr data-bind="visible: showInheritProfiles">
			<th style="white-space: normal;"><%= Html.LabelFor(m => m.Config.InheritIndirectProfiles) %></th>
			<td class="checkbox"><%= Html.CheckBoxFor(m => m.Config.InheritIndirectProfiles) %></td>
		</tr>
        <tr class="row-for-auth-type-ldap" data-bind="visible: isExpandedAuthMethod">
            <th style="width: 170px;"><%= Olimp.ServerManagement.I18N.AuthSettings.AuthMethod %></th>        
            <td class="table">
                <table class="form-table">
                    <tr class="first">
                        <td style="width: 1px;" class="checkbox">
                            <input id="auth-type-integrated" type="radio" name="auth-type" value="integrated" data-bind="checked: authType" />
                        </td>
                        <td>
                            <label for="auth-type-integrated"><%= Olimp.ServerManagement.I18N.AuthSettings.ModeIntegrated %></label>
                            <span class="current-auth-type" data-bind="visible: currentIsIntegrated">(<%= Olimp.ServerManagement.I18N.AuthSettings.Current %>)</span>
                        </td>
                    </tr>
                    <tr class="last">
                        <td style="width: 1px;" class="checkbox">
                            <input id="auth-type-ldap" type="radio" name="auth-type" value="ldap" data-bind="checked: authType" />
                        </td>
                        <td>
                            <label for="auth-type-ldap"><%= Olimp.ServerManagement.I18N.AuthSettings.ModeLdap %></label>
                            <span class="current-auth-type" data-bind="visible: currentIsLdap, text: currentLdapSchemeText"></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="row-for-auth-schemes-ldap last" style="display: none;" data-bind="visible: isExpandedAuthSchemes">
            <th><%= Olimp.ServerManagement.I18N.AuthSettings.AuthScheme %></th>
            <td>
                <div id="ldap-scheme-validation-error" class="olimp-validation-error">
                    <span class="field-validation-valid">
                        <span for="select-ldap-scheme" data-bind="visible: !isLdapSchemeIdValid()">
                            <%= Olimp.ServerManagement.I18N.LdapAuthSchemes.LdapSchemeNotSelectedError %>
                        </span>
                    </span>
                </div>
                 <%= Html.DropDownListFor(m => m.LdapSchemeId, Model.AllSchemes, new { 
                            data_bind = "value: ldapSchemeId, enable: isExpandedAuthSchemes",
                            id = "select-ldap-scheme"
                     })
                 %>
            </td>
        </tr>
	</table>
</div>

<h1>Язык интерфейса</h1>
<div class="block">
	<table class="form-table">
		<tr class="first last">
			<th><%= Html.LabelFor(m => m.Config.UILocale) %></th>
			<td><%= Html.DropDownListFor(m => m.Config.UILocale, Model.AllowedLocales) %></td>
		</tr>
	</table>
</div>	

<h1>Настройки основного входа</h1>
<div class="block">
	<table class="form-table">	
		<tr class="first">
			<th><%= Html.LabelFor(m => m.Config.AllowExamPreparation) %></th>
			<td class="checkbox"><%= Html.CheckBoxFor(m => m.Config.AllowExamPreparation) %></td>
		</tr>
		<tr class="last">
			<th><%= Html.LabelFor(m => m.Config.AllowExam) %></th>
			<td class="checkbox"><%= Html.CheckBoxFor(m => m.Config.AllowExam) %></td>
		</tr>
	</table>
</div>

<h1>Основная группа</h1>
<div class="block">
	<table class="form-table">	
		<tr class="first last">
			<th><%= Html.LabelFor(m => m.Config.DefaultGroup) %></th>
			<td>
			    <%= Html.DropDownListFor(m => m.Config.DefaultGroup, Model.Groups, new Dictionary<string, object> {
                        {"class", "combobox"},
			            {"data-bind", "disable: isPreRegistration"}
			        }) 
                %>
			</td>
		</tr>
	</table>
</div>		

<h1>Группы пользователей для экзамена</h1>
<div class="block">		
	<table class="form-table" data-bind="attr: {disabled: !isPreRegistration()}">
	
	<% var groups = new Queue<SelectListItem>(Model.Groups); %>	
	<tr class="<%= (!groups.Any() ? "first last" : "first") %>">
		<th class="bold">
			<label style="font-weight: bold" for="checkbox-config-group">
				<%= Olimp.I18N.Domain.Config.DetermineGroupsByExamPeriod %>
			</label>
		</th>
		<td class="checkbox">
			<%= Html.CheckBoxFor(m => m.Config.DetermineGroupsByExamPeriod, new {
			        id="checkbox-config-group",
                    data_bind="disable: !isPreRegistration(), checked: configGroupIsCheck"
			    }) 
            %>
		</td>	
	</tr>
		
	<% while(groups.Any()) { %>		
		<% var group = groups.Dequeue(); %>
		<% var isChecked = strSelectedId.IndexOf("-" + group.Value + "-") != -1; %>
		
		<tr <%= (!groups.Any() ? "class=\"last\"" : "") %>>
  			<th><label for="exam-group-<%= group.Value %>"><%= group.Text %></label></th>
			<td class="checkbox">
				<input id="exam-group-<%= group.Value %>" 
                    data-bind="disable: examGroupsDisable"
					class="CheckGroup" 
					type="checkbox" 
					name="ExamGroups" 
					value="<%= group.Value %>"  <%= (isChecked ? "checked=\"checked\"" : "") %> />
			</td>
		</tr>
	<% } %>
	</table>	
</div>

<script>
    $(function () {
        <% if (Model.SaveSuccess == true) {%>
                $.Olimp.showSuccess('<%= Model.SaveMessage %>');
        <% } else if (Model.SaveSuccess == false) {%>
                $.Olimp.showError('<%= Model.SaveMessage %>');
        <% } %>
    })
</script>

<button id="save-mode" data-bind="click: save"><%= Olimp.I18N.PlatformShared.Save %></button>

<script type="text/javascript">	

$(function() {	
	$("#save-mode").olimpbutton();

	var regType = ('<%= Model.Config.RegistrationType %>' == 'ByFullNameExpanded') ? 'ByFullName' : '<%= Model.Config.RegistrationType %>';

    var viewModel = {
        authType: ko.observable(),
        loginType: ko.observable('<%= Model.Config.LoginType %>'),
        currentAuthType: ko.observable(),
        ldapSchemeId: ko.observable(<%= Model.LdapSchemeId.GetValueOrDefault() %>),
        setCurrentAuthType: function (type) {                    
            this.currentAuthType(type);
            this.authType(type);
        },
	    registrationType: ko.observable(regType),
	    isExpandedRegistration: ko.observable(<%= Model.ExpandedRegistration.ToString().ToLower() %>),
        configGroupIsCheck: ko.observable(<%= DetermineGroupsByExamPeriod ? "true" : "false" %>),
        save : function() {
            if (!this.isLdapSchemeIdValid())
                return;

            var form = $('#mode-registrations-form');
            form.submit();
        }
    };

	viewModel.isPreRegistration = ko.computed(function() {
	     return this.registrationType() === 'PreRegistration';
	}, viewModel);

	viewModel.isAnonymous = ko.computed(function() {
	     return this.registrationType() === 'Anonymous';
	}, viewModel);

	viewModel.isLdapAuthType = ko.computed(function() {
	    return this.authType() === 'ldap';
	}, viewModel);

	viewModel.firstRowClass = ko.computed(function() {
	     return !this.isAnonymous() ? 'first' : 'first last';
	}, viewModel);
    
	viewModel.fioRegistration = ko.computed(function() { 
	    var isAnonymous = this.isAnonymous();
	
	    return !this.isPreRegistration() && !isAnonymous;
	}, viewModel);

	viewModel.showInheritProfiles = ko.computed(function() {
	    var isExpanded = this.isExpandedRegistration();

	    return this.fioRegistration() && isExpanded;
	}, viewModel);
    	
	viewModel.isExpandedRegistrationClass = ko.computed(function() {
	     return this.isExpandedRegistration() ? '' : 'last';
	}, viewModel);

	viewModel.examGroupsDisable = ko.computed(function () {
	    return !this.isPreRegistration() ? true : this.configGroupIsCheck();
	}, viewModel);

	viewModel.currentIsIntegrated = ko.computed(function () {
	    return this.currentAuthType() === 'integrated';
	}, viewModel);

	viewModel.currentIsLdap = ko.computed(function () {
	    return this.currentAuthType() === 'ldap';
	}, viewModel);

    viewModel.currentLdapSchemeText = ko.computed(function() {
        var text = $('#select-ldap-scheme option[value=' + this.ldapSchemeId() + ']').text();
        return '(' + '<%= Olimp.ServerManagement.I18N.AuthSettings.Current %>' + ', ' + text + ')';
    }, viewModel);

    viewModel.isPassworgLoginType = ko.computed(function() {
        return this.loginType() === "Password";
    }, viewModel);

    viewModel.isExpandedAuthMethod = ko.computed(function() {
        return this.isPreRegistration() && this.isPassworgLoginType();
    }, viewModel);

    viewModel.isExpandedAuthSchemes = ko.computed(function() {
        return this.isExpandedAuthMethod() && this.isLdapAuthType();
    }, viewModel);

    viewModel.lastRowClass = ko.computed(function () {
        return !this.isExpandedAuthSchemes() ? 'last' : '';
    }, viewModel);

    viewModel.authType.subscribe(function () {
        var e = $('.row-for-auth-schemes-ldap');

        if (!viewModel.isExpandedAuthSchemes()) {
            e.removeClass('last');
            $('.row-for-auth-type-ldap').addClass('last');
        } else {
            e.addClass('last');
            $('.row-for-auth-type-ldap').removeClass('last');
        }
    });

    viewModel.isLdapSchemeIdValid = ko.computed(function () {
        if (this.authType() === 'integrated')
            return true;

        return this.authType() === 'ldap' && !!this.currentLdapSchemeId();
    }, viewModel);

    viewModel.currentLdapSchemeId = ko.computed(function () {
        var selectedSchemeId = this.ldapSchemeId();

        return this.authType() !== 'integrated'
            ? selectedSchemeId
            : null;
    }, viewModel);

    ko.applyBindings(viewModel, $('config-registration-type')[0]);

    viewModel.setCurrentAuthType('<%= Model.LdapSchemeId.HasValue ? "ldap" : "integrated" %>');
	
	$('select').combobox();
});
</script>				