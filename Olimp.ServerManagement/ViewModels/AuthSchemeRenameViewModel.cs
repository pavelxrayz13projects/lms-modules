﻿namespace Olimp.ServerManagement.ViewModels
{
    public class AuthSchemeRenameViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}