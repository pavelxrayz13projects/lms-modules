using System.Collections.Generic;
using Olimp.Core.ComponentModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace Olimp.ServerManagement.ViewModels
{
    public class LdapRuleEditViewModel
    {
        public int? Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.LdapPermissionRule), DisplayNameResourceName = "Role")]
        public string RoleId { get; set; }

        public string RoleName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.LdapPermissionRule), DisplayNameResourceName = "LdapGroupDN")]
        public string LdapGroupDN { get; set; }

        public string LdapGroupDNWithSpaces { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.LdapPermissionRule), DisplayNameResourceName = "Node")]
        public Guid? NodeId { get; set; }

        public string NodeName { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.SystemUser), DisplayNameResourceName = "ExcludedPermissions")]
        public IEnumerable<string> ExcludedPermissions { get; set; }

        public AuthSettingsViewModel GlobalModel { get; set; }
    }
}