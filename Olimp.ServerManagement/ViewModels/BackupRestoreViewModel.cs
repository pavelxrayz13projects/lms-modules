using Olimp.Core.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.ServerManagement.ViewModels
{
    public class BackupRestoreViewModel : BackupViewModelBase
    {
        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.ServerManagement.I18N.Backup), DisplayNameResourceName = "ForAllNodesRestore")]
        public override bool ForAllNodes { get; set; }

        public bool CanRestoreForAllNodes { get; set; }

        public IEnumerable<SelectListItem> BackupFiles { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.ServerManagement.I18N.ServerManagement), ErrorMessageResourceName = "BackupNotSelected")]
        public string SelectedBackup { get; set; }

        public bool CanRestore { get; set; }
    }
}