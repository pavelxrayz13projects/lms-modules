using Olimp.Domain.LearningCenter;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.ServerManagement.ViewModels
{
    public class ModeRegistrationsViewModel
    {
        public int? LdapSchemeId { get; set; }

        public bool InternetVersion { get; set; }

        public bool? SaveSuccess { get; set; }

        public string SaveMessage { get; set; }

        public OlimpConfiguration Config { get; set; }

        [Display(ResourceType = typeof(I18N.ModeRegistrations), Name = "ExpandedRegistration")]
        public bool ExpandedRegistration { get; set; }

        public IList<SelectListItem> AllSchemes { get; set; }

        public SelectList AllowedLocales { get; set; }

        public SelectList Profiles { get; set; }

        public SelectList ExamSettings { get; set; }

        public SelectList RegistrationTypes { get; set; }

        public IEnumerable<int> ExamGroups { get; set; }

        public IEnumerable<SelectListItem> Groups { get; set; }
    }
}