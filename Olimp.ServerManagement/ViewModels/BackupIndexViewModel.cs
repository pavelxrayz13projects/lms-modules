﻿using Olimp.Core.ComponentModel;

namespace Olimp.ServerManagement.ViewModels
{
    public class BackupIndexViewModel : BackupViewModelBase
    {
        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.ServerManagement.I18N.Backup), DisplayNameResourceName = "ForAllNodesBackup")]
        public override bool ForAllNodes { get; set; }

        public bool CanBackup { get; set; }

        public bool CanBackupForAllNodes { get; set; }

        public bool CanRestore { get; set; }

        public bool CanRestoreForAllNodes { get; set; }
    }
}