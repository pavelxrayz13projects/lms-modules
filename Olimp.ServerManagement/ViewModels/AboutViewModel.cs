using Olimp.Core.Activation;

namespace Olimp.ServerManagement.ViewModels
{
    public class AboutViewModel
    {
        public string ServerNow { get; set; }

        public bool ShowServerInfo { get; set; }

        public bool ShowClientInfo { get; set; }

        public string NodeName { get; set; }

        public string OlimpPath { get; set; }

        public string SystemName { get; set; }

        public string ProgrammVersion { get; set; }

        public string CompanyName { get; set; }

        public string DistributionId { get; set; }

        public string BindingKey { get; set; }

        public int ActivationAttempt { get; set; }

        public int ActivationNumber { get; set; }

        public int MaxUsers { get; set; }

        public string NodeCompanyName { get; set; }

        public string LastActivation { get; set; }

        public string LicenseExpires { get; set; }

        public ActivationState ActivationState { get; set; }

        public bool IsTrial { get; set; }

        public string LastUpdate { get; set; }

        public string LastBackup { get; set; }
    }
}