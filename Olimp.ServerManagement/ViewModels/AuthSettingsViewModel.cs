﻿using Olimp.Core.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.ServerManagement.ViewModels
{
    public class AuthSettingsViewModel
    {
        public int? LdapSchemeId { get; set; }

        public string DisplayText { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.AuthSettings), DisplayNameResourceName = "TestLogin")]
        public string TestLogin { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.AuthSettings), DisplayNameResourceName = "TestPassword")]
        public string TestPassword { get; set; }

        public IList<SelectListItem> AllSchemes { get; set; }

        public IEnumerable<SelectListItem> AllRoles { get; set; }

        public IEnumerable<SelectListItem> AllNodes { get; set; }

        public IEnumerable<SelectListItem> AllPermissions { get; set; } 

        public string CurrentNodeId { get; set; }

        public bool CanManageGlobal { get; set; }

        public bool CanChangeLdapPermissionRules { get; set; }
    }
}