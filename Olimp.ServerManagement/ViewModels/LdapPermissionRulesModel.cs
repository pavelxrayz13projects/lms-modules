﻿namespace Olimp.ServerManagement.ViewModels
{
    public class LdapPermissionRulesModel
    {
        public bool CanManageLdapPermissions { get; set; }

        public AuthSettingsViewModel GlobalModel { get; set; }
    }
}