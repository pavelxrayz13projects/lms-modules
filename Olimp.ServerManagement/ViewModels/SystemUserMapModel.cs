﻿using System;
using System.Collections.Generic;

namespace Olimp.ServerManagement.ViewModels
{
    public class SystemUserMapModel
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public bool IsBlocked { get; set; }

        public bool IsGlobal { get; set; }

        public bool IsLocked { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public IEnumerable<string> Roles { get; set; }

        public IEnumerable<string> ExcludedPermissions { get; set; }

        public string RolesString { get; set; }

        public IEnumerable<Guid> Nodes { get; set; }

        public string NodesString { get; set; }
    }
}