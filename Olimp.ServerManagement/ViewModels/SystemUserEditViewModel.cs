using Olimp.Core.ComponentModel;
using Olimp.Web.Controllers.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Olimp.ServerManagement.ViewModels
{
    public class SystemUserEditViewModel
    {
        public int? Id { get; set; }

        [Remote("ValidateLoginIsUnique", "SystemUser", "Admin", AdditionalFields = "Id", HttpMethod = "POST", ErrorMessageResourceType = typeof(I18N.SystemUser), ErrorMessageResourceName = "ValidationErrorUserLoginIsNotUnique")]
        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.SystemUser), DisplayNameResourceName = "Login")]
        public string Login { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.ServerManagement.I18N.SystemUser), DisplayNameResourceName = "OnEveryNode")]
        public bool IsGlobal { get; set; }

        public bool IsBlocked { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.SystemUser), DisplayNameResourceName = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [Compare("Password", ErrorMessageResourceType = typeof(Olimp.I18N.Domain.SystemUser), ErrorMessageResourceName = "PasswordsNotMatch")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.SystemUser), DisplayNameResourceName = "ConfirmPassword")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.SystemUser), DisplayNameResourceName = "Roles")]
        public IEnumerable<string> Roles { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.SystemUser), DisplayNameResourceName = "ExcludedPermissions")]
        public IEnumerable<string> ExcludedPermissions { get; set; }
        
        [RequiredIfBool("IsGlobal", ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(Olimp.I18N.Domain.SystemUser), DisplayNameResourceName = "Nodes")]
        public IEnumerable<Guid> Nodes { get; set; }

        public AuthSettingsViewModel GlobalModel { get; set; }
    }
}