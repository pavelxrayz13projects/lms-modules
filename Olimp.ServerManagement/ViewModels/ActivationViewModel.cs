using Olimp.Core.Activation;
using System.Runtime.Serialization;

namespace Olimp.ServerManagement.ViewModels
{
    [DataContract]
    public class ActivationViewModel
    {
        [DataMember(Name = "isTrial")]
        public bool IsTrial { get; set; }

        [DataMember(Name = "state")]
        public ActivationState ActivationState { get; set; }

        [DataMember(Name = "licenceExpires")]
        public string LicenceExpires { get; set; }

        [DataMember(Name = "maxUsers")]
        public int MaxUsers { get; set; }

        [DataMember(Name = "activationNumber")]
        public int ActivationNumber { get; set; }

        [DataMember(Name = "bindingKey")]
        public string BindingKey { get; set; }
    }
}