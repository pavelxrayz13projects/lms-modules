﻿using Olimp.Core.ComponentModel;
using Olimp.Domain.Catalogue.Entities;
using System.ComponentModel.DataAnnotations;

namespace Olimp.ServerManagement.ViewModels
{
    public class AuthSchemeViewModel : LdapAuthScheme
    {
        public AuthSchemeViewModel()
        {
            LoginField = "sAMAccountName";
            FullNameField = "name";
            CompanyField = "department";
            ProfessionField = "title";
            EmailField = "mail";
            IsPublic = true;
            IsEnabled = true;
            UseSsl = true;
            ImportOnlyActiveUsers = true;
        }

        public AuthSchemeViewModel(LdapAuthScheme authScheme)
        {
            Id = authScheme.Id;
            Name = authScheme.Name;

            ServerName = authScheme.ServerName;
            ServerPort = authScheme.ServerPort;
            BaseDN = authScheme.BaseDN;
            Password = authScheme.Password;
            Login = authScheme.Login;

            GroupDN = authScheme.GroupDN;
            LoginField = authScheme.LoginField;
            FullNameField = authScheme.FullNameField;
            IsLockedField = authScheme.IsLockedField;
            CompanyField = authScheme.CompanyField;
            ProfessionField = authScheme.ProfessionField;
            EmailField = authScheme.EmailField;
            NumberField = authScheme.NumberField;
            LocaleField = authScheme.LocaleField;

            IsPublic = authScheme.IsPublic;
            IsEnabled = authScheme.IsEnabled;

            UseSsl = authScheme.UseSsl;
            ImportOnlyActiveUsers = authScheme.ImportOnlyActiveUsers;
        }

        public string AuthSchemeString { get; set; }

        public sealed override int Id { get; set; }

        public bool? SaveSuccess { get; set; }

        public string SaveMessage { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "Name")]
        public sealed override string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "ServerName")]
        public sealed override string ServerName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [AnyTypeRegEx("^[0-9]+$", ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "NumberInvalid")]
        [Range(0, 65535, ErrorMessageResourceType = typeof(I18N.LdapAuthSchemes), ErrorMessageResourceName = "ServerPortTooLarge")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "ServerPort")]
        public sealed override int ServerPort { get; set; }

        [Required(ErrorMessageResourceType = typeof(Olimp.I18N.PlatformShared), ErrorMessageResourceName = "RequiredInvalid")]
        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "BaseDN")]
        public sealed override string BaseDN { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "GroupDN")]
        public sealed override string GroupDN { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "Login")]
        public sealed override string Login { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "Password")]
        public sealed override string Password { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "LoginField")]
        public sealed override string LoginField { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "FullNameField")]
        public sealed override string FullNameField { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "CompanyField")]
        public sealed override string CompanyField { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "ProfessionField")]
        public sealed override string ProfessionField { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "IsPublic")]
        public sealed override bool IsPublic { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "IsEnabled")]
        public sealed override bool IsEnabled { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "EmailField")]
        public sealed override string EmailField { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "NumberField")]
        public sealed override string NumberField { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "LocaleField")]
        public sealed override string LocaleField { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "UseSsl")]
        public sealed override bool UseSsl { get; set; }

        [LocalizedDisplayName(DisplayNameResourceType = typeof(I18N.LdapAuthSchemes), DisplayNameResourceName = "ImportOnlyActiveUsers")]
        public sealed override bool ImportOnlyActiveUsers { get; set; }
    }
}