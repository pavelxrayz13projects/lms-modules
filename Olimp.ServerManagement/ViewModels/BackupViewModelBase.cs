﻿namespace Olimp.ServerManagement.ViewModels
{
    public class BackupViewModelBase
    {
        public virtual bool ForAllNodes { get; set; }
    }
}