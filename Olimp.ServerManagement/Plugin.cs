using Olimp.Core;
using Olimp.Core.Activation;
using Olimp.Core.Activation.HardwareBinding;
using Olimp.Core.Routing;
using Olimp.Domain.Catalogue.Ldap;
using Olimp.Domain.Catalogue.OrmLite;
using Olimp.Domain.Catalogue.OrmLite.Ldap;
using Olimp.Domain.Catalogue.OrmLite.Repositories;
using Olimp.Domain.Catalogue.OrmLite.Security;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Common.Nodes;
using Olimp.Domain.Common.Security;
using Olimp.Domain.LearningCenter;
using Olimp.LocalNodes.Core.Cluster;
using Olimp.LocalNodes.Core.Services.Clients;
using Olimp.ServerManagement.Controllers;
using PAPI.Core;
using PAPI.Core.Initialization;
using System;
using System.Web.Routing;

[assembly: Bind("Module", Context = typeof(ModuleFactory), TargetType = typeof(Olimp.ServerManagement.Plugin))]

namespace Olimp.ServerManagement
{
    public class Plugin : PluginModule
    {
        public static bool ActivationLinkVisible
        {
            get { return PContext.ExecutingModule.Context.NodeData.Name.Equals(Platform.DefaultNode.Name); }
        }

        private IModuleContext _context;

        public Plugin(IModuleContext context, IModuleKernel kernel)
            : base(context, kernel)
        {
            _context = context;
        }

        public override void RegisterRoutes(OlimpAreaCollection areas, RouteCollection routes)
        {
            areas["Admin"].RegisterControllers(
                typeof(AboutController),
                typeof(BackupController),
                typeof(GenerateBindingController),
                typeof(ModeRegistrationsController),
                typeof(SystemUserController),
                typeof(LdapPermissionRulesController),
                typeof(AuthSchemeController),
                typeof(AuthSettingsController));
        }

        public override void SetupKernel(IBindingBuilder binder)
        {
            if (_context.Flags.HasFlag(ModuleLoadFlags.LoadForDatabaseInitialization))
                return;

            var rolesProvider = new IntegratedRolesProvider();
            var userFactory = new DefaultUserFactory();

            var connFactoryFactory = new SqliteConnectionFactoryFactory();
            var connFactory = connFactoryFactory.Create();

            var nodesRepository = new OrmLiteNodeRepository(connFactory);

            var permissionChecker = new RoleProviderPermissionChecker();
            var userRepository = new PermissionInsuredUserRepository(
                this.Context.NodeData,
                new OrmLiteUserRepository(this.Context.NodeData, connFactory, nodesRepository, rolesProvider),
                permissionChecker);

            binder.Controller<SystemUserController>(b => b
                .Bind<IRolesProvider>(() => rolesProvider)
                .Bind<IUserFactory>(() => userFactory)
                .Bind<IUserRepository>(() => userRepository)
                .Bind<INodesRepository>(() => nodesRepository));

            var module = PContext.ExecutingModule as LearningCenterModule;
            if (module == null)
                throw new InvalidOperationException("Executing module is not LearningCenterModule");

            var startupRegistry = module.LocalCluster.GetStartupRegistry();
            var hardwareIdProvider = new OlimpHardwareIdProvider();

            var bindingKeyGenerator = new OlimpBindingKeyGenerator(
                module.Context.DistributionInfoProvider,
                module.Database,
                hardwareIdProvider);

            binder.Controller<AboutController>(b => b
                .Bind<INodesRepository>(() => startupRegistry)
                .Bind<IBindingKeyGenerator>(() => bindingKeyGenerator)
            );

            binder.Controller<GenerateBindingController>(b => b
                .Bind<IActivationService>(() => new WebActivationServiceClient(OlimpPaths.OlimpActivationServiceUri))
                .Bind<IBindingKeyGenerator>(() => bindingKeyGenerator)
            );

            var defaultLocalNodeController = new DefaultLocalNodeControllerServiceClient(OlimpPaths.OlimpClusterServiceUri);

            binder.Controller<BackupController>(b => b
                .Bind<ILocalNodeController>(() => defaultLocalNodeController));

            var ldapRulesRepository = new OrmLiteLdapPermissionRuleRepository(connFactory, nodesRepository, rolesProvider);

            binder.Controller<LdapPermissionRulesController>(b => b
                .Bind<ILdapPermissionRulesRepository>(() => ldapRulesRepository)
                .Bind<IRolesProvider>(() => rolesProvider)
                .Bind<INodesRepository>(() => nodesRepository));

            var authSchemasRepository = new OrmLiteLdapAuthSchemeRepository(connFactory);

            binder.Controller<AuthSchemeController>(b => b
                .Bind<ILdapAuthSchemeRepository>(() => authSchemasRepository));

            binder.Controller<AuthSettingsController>(b => b
                .Bind<IUserRepository>(() => userRepository)
                .Bind<IRolesProvider>(() => rolesProvider)
                .Bind<INodesRepository>(() => nodesRepository)
                .Bind<ILdapAuthSchemeRepository>(() => authSchemasRepository)
                .Bind<ILdapPermissionRulesRepository>(() => ldapRulesRepository));

            binder.Controller<ModeRegistrationsController>(b => b.Bind<ILdapAuthSchemeRepository>(() => authSchemasRepository));
        }
    }
}