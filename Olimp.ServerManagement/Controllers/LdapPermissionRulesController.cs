﻿using System.Web.Security;
using Olimp.Core;
using Olimp.Core.Mvc.Security;
using Olimp.Core.Web;
using Olimp.Domain.Catalogue.Entities;
using Olimp.Domain.Catalogue.Ldap;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Common.Nodes;
using Olimp.Domain.Common.Security;
using Olimp.ServerManagement.ViewModels;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Olimp.ServerManagement.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    public class LdapPermissionRulesController : PController
    {
        private readonly ILdapPermissionRulesRepository _rulesRepository;
        private readonly IRolesProvider _rolesProvider;
        private readonly INodesRepository _nodesRepository;

        public LdapPermissionRulesController(ILdapPermissionRulesRepository rulesRepository, IRolesProvider rolesProvider, INodesRepository nodesRepository)
        {
            if (rolesProvider == null)
                throw new ArgumentNullException("rolesProvider");

            if (rulesRepository == null)
                throw new ArgumentNullException("rulesRepository");

            if (nodesRepository == null)
                throw new ArgumentNullException("nodesRepository");

            _rulesRepository = rulesRepository;
            _rolesProvider = rolesProvider;
            _nodesRepository = nodesRepository;
        }

        [NonAction]
        private static LdapRuleEditViewModel MapLdapPermissionRule(LdapPermissionRule rule)
        {
            var model = new LdapRuleEditViewModel
            {
                Id = rule.Id,
                LdapGroupDN = rule.LdapGroupDN,
                LdapGroupDNWithSpaces = (rule.LdapGroupDN ?? "").Replace(",", ", "),
                ExcludedPermissions = rule.ExcludedPermissions
            };

            if (rule.Node != null)
            {
                model.NodeId = rule.Node.Id;
                model.NodeName = rule.Node.Name;
            }
            else
            {
                model.NodeName = I18N.LdapPermissionRules.AllNodes;
            }

            if (rule.Role != null)
            {
                model.RoleId = rule.Role.Id;
                model.RoleName = rule.Role.Title;
            }

            return model;
        }

        [HttpPost]
        //TODO: Optimize paging
        public ActionResult GetLdapPermissionRules(TableViewModel model)
        {
            var rules = _rulesRepository.GetAll().Select(MapLdapPermissionRule);

            return Json(rules.GetTableData(model, r => new
            {
                r.Id,
                r.RoleId,
                r.RoleName,
                r.LdapGroupDN,
                r.LdapGroupDNWithSpaces,
                NodeId = Convert.ToString(r.NodeId),
                r.NodeName,
                r.GlobalModel,
                r.ExcludedPermissions
            }));
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.Admin.Ldap.ManageLdapPermissionRules, Order = 2)]
        public ActionResult Edit(LdapRuleEditViewModel model)
        {
            if (!TryValidateModel(model))
            {
                PAPI.Core.Diagnostics.Debug.PrintModelState(this);
                return new StatusCodeResultWithText((int)HttpStatusCode.BadRequest, "The model is invalid.");
            }

            var ruleId = model.Id.GetValueOrDefault();
            var rule = new LdapPermissionRule
            {
                Id = ruleId,
                LdapGroupDN = model.LdapGroupDN,
                Node = model.NodeId.HasValue
                    ? _nodesRepository.GetById(model.NodeId.Value)
                    : null,
                Role = _rolesProvider.GetById(model.RoleId)
            };

            rule.ExcludedPermissions.Clear();

            if (model.ExcludedPermissions != null)
            {
                foreach (var permission in model.ExcludedPermissions)
                    rule.ExcludedPermissions.Add(permission);
            }

            if (ruleId > 0)
                _rulesRepository.Update(rule);
            else
                rule.Id = _rulesRepository.Add(rule);

            return Json(MapLdapPermissionRule(rule));
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.Admin.Ldap.ManageLdapPermissionRules, Order = 2)]
        public ActionResult Delete(int id)
        {
            _rulesRepository.DeleteById(id);

            return null;
        }
    }
}