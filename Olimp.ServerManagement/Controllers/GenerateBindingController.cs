using Olimp.Core;
using Olimp.Core.Activation;
using Olimp.Core.Security;
using Olimp.Domain.Catalogue.Security;
using Olimp.ServerManagement.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core;
using System;
using System.Web.Mvc;

namespace Olimp.ServerManagement
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Admin.Activation, Order = 2)]
    public class GenerateBindingController : PController
    {
        private static volatile bool _activationIsInProgress = false;
        private static readonly object _activationSyncObj = new object();

        public bool ActivationIsInProgress
        {
            get { return _activationIsInProgress; }
        }

        private IActivationService _activationService;
        private IBindingKeyGenerator _bindingKeyGenerator;

        public GenerateBindingController(
            IActivationService activationService, IBindingKeyGenerator bindingKeyGenerator)
        {
            if (activationService == null)
                throw new ArgumentNullException("activationService");

            if (bindingKeyGenerator == null)
                throw new ArgumentNullException("bindingKeyGenerator");

            _activationService = activationService;
            _bindingKeyGenerator = bindingKeyGenerator;
        }

        [HttpGet]
        public ActionResult Generate()
        {
            return View(GetCurrentState());
        }

        [HttpPost]
        public ActionResult GetState()
        {
            if (ActivationIsInProgress)
                return Json(new { activationIsInProgress = true });

            ActivationViewModel model = GetCurrentState();
            return Json(new { activationIsInProgress = false, data = model });
        }

        [NonAction]
        private ActivationViewModel GetCurrentState()
        {
            var olimpModule = (OlimpModule)PContext.ExecutingModule;
            var keyInfo = olimpModule.Activation.GetActivationKeyInfo();

            using (UnitOfWork.CreateRead())
            {
                return new ActivationViewModel
                {
                    LicenceExpires = keyInfo != null
                      ? keyInfo.LicenceExpires.ToShortDateString()
                      : "-",
                    MaxUsers = keyInfo != null ? keyInfo.MaxUsers : 0,
                    IsTrial = keyInfo != null && keyInfo.Flags.HasFlag(OlimpActivationKeyFlags.IsTrial),
                    ActivationNumber = keyInfo != null ? keyInfo.ActivationNumber : 0,
                    ActivationState = olimpModule.Activation.GetActivationManager().EnsureActivation(),
                    BindingKey = ReadableKey.ToReadableKey(_bindingKeyGenerator.Generate(), 4)
                };
            }
        }

        [HttpPost]
        //TODO: RewriteActivationLogic
        public void ActivateByPhone(string key)
        {
            lock (_activationSyncObj)
            {
                try
                {
                    // ��������� �� ��������� �������� ���� ��� ������� ������������ ���� (����� ������������������� � false).
                    _activationIsInProgress = true;
                    var olimpModule = (OlimpModule)PContext.ExecutingModule;
                    olimpModule.Activation.GetActivationManager().TryActivate(key);
                }
                finally
                {
                    _activationIsInProgress = false;
                }
            }
        }

        [HttpPost]
        public void ActivateByInternet(string key)
        {
            this.ActivateByPhone("3MKB-NVBN-N02J-FN3P-94KD-XJC6RA");
        }
    }
}