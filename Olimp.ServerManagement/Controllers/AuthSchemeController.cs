﻿using Olimp.Domain.Catalogue.Ldap;
using Olimp.Domain.Catalogue.Security;
using Olimp.ServerManagement.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using ServiceStack.Text;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.ServerManagement.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Admin.Ldap.ManageLdapSchemas, Order = 2)]
    public class AuthSchemeController : PController
    {
        private readonly ILdapAuthSchemeRepository _authSchemesRepository;

        public AuthSchemeController(ILdapAuthSchemeRepository authSchemesRepository)
        {
            if (authSchemesRepository == null)
                throw new ArgumentNullException("authSchemesRepository");

            _authSchemesRepository = authSchemesRepository;
        }

        [HttpGet]
        public ActionResult Index(int? authSchemeId, bool? saveSuccess, string saveMessage)
        {
            var allSchemes = _authSchemesRepository.GetAll()
                .OrderBy(e => e.Name)
                .ToList();

            var authScheme = authSchemeId.HasValue
                ? allSchemes.Find(e => e.Id == authSchemeId.Value)
                : allSchemes.FirstOrDefault();

            var viewmodel = authScheme != null
                ? new AuthSchemeViewModel(authScheme)
                : new AuthSchemeViewModel();

            JsConfig.ExcludeTypeInfo = true;

            var schemesList = allSchemes.Select(e => new { e.Id, e.Name });
            viewmodel.AuthSchemeString = JsonSerializer.SerializeToString(schemesList);

            viewmodel.SaveSuccess = saveSuccess;
            viewmodel.SaveMessage = saveMessage;

            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult DeleteAuthScheme(int authSchemeId)
        {
            _authSchemesRepository.DeleteById(authSchemeId);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Save(AuthSchemeViewModel model)
        {
            if (!ModelState.IsValid)
                return null;

            var id = model.Id;

            var message = I18N.LdapAuthSchemes.SaveSuccess;
            var success = true;
            try
            {
                if (id > 0)
                    _authSchemesRepository.Update(model);
                else
                    id = _authSchemesRepository.Add(model);
            }
            catch (Exception e)
            {
                success = false;
                message = e.Message;
            }

            return RedirectToAction("Index", new
            {
                authSchemeId = id, 
                SaveSuccess = success, 
                SaveMessage = message
            });
        }

        [HttpPost]
        public ActionResult Rename(AuthSchemeRenameViewModel model)
        {
            if (!ModelState.IsValid)
                return null;

            _authSchemesRepository.Rename(model.Id, model.Name);

            return RedirectToAction("Index", new { authSchemeId = model.Id });
        }
    }
}