using Olimp.Configuration;
using Olimp.Core.Activation;
using Olimp.Core.Mvc.Security;
using Olimp.Core.Security;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Common.Nodes;
using Olimp.Domain.LearningCenter;
using Olimp.ServerManagement.ViewModels;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core;
using System;
using System.Web.Mvc;
using System.Web.Security;

namespace Olimp.ServerManagement.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.About.Client + "," + Permissions.About.Server, Order = 2)]
    public class AboutController : PController
    {
        private INodesRepository _nodeRegistry;
        private IBindingKeyGenerator _bindingKeyGenerator;

        public AboutController(
            INodesRepository nodeRegistry,
            IBindingKeyGenerator bindingKeyGenerator)
        {
            if (nodeRegistry == null)
                throw new ArgumentNullException("nodeRegistry");

            if (bindingKeyGenerator == null)
                throw new ArgumentNullException("bindingKeyGenerator");

            _nodeRegistry = nodeRegistry;
            _bindingKeyGenerator = bindingKeyGenerator;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var os = Environment.OSVersion;
            var distributionInfo = PContext.ExecutingModule.Context.DistributionInfoProvider.Get();

            var olimpModule = (LearningCenterModule)PContext.ExecutingModule;
            var keyInfo = olimpModule.Activation.GetActivationKeyInfo();

            ActivationState state;
            string readableBindingKey;
            BaseConfiguration config;

            using (UnitOfWork.CreateRead())
            {
                config = Config.Get<BaseConfiguration>();
                state = olimpModule.Activation.GetActivationManager().EnsureActivation();
                readableBindingKey = ReadableKey.ToReadableKey(_bindingKeyGenerator.Generate(), 4);
            }

            var now = DateTime.Now;
            TimeSpan? utcOffset = null;
            try
            {
                utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(now);
            }
            catch { }

            var viewmodel = new AboutViewModel
            {
                ShowServerInfo = Roles.IsUserInRole(Permissions.About.Server),
                ShowClientInfo = Roles.IsUserInRole(Permissions.About.Client),

                NodeName = olimpModule.Context.NodeData.Name,
                OlimpPath = PlatformPaths.Default.Root,
                SystemName = String.Format("{0} ({1})", os.VersionString, os.Version),
                ProgrammVersion = olimpModule.Context.Assembly.GetName()
                    .Version.ToString(),
                ActivationAttempt = config.ActivationAttempt,
                ActivationNumber = keyInfo != null
                    ? keyInfo.ActivationNumber
                    : 0,
                LicenseExpires = (keyInfo == null || keyInfo.LicenceExpires == default(DateTime))
                    ? "-"
                    : keyInfo.LicenceExpires.ToShortDateString(),
                MaxUsers = olimpModule.Activation.UsersQuotaProvider.MaxUsers,
                ActivationState = state,
                IsTrial = keyInfo != null && keyInfo.Flags.HasFlag(OlimpActivationKeyFlags.IsTrial),
                BindingKey = readableBindingKey,
                LastBackup = config.LastBackup == default(DateTime) ? "-" : config.LastBackup.ToShortDateString(),
                LastUpdate = config.LastUpdate == default(DateTime) ? "-" : config.LastUpdate.ToShortDateString(),
                LastActivation = config.LastActivation == default(DateTime) ? "-" : config.LastActivation.ToShortDateString(),
                ServerNow = utcOffset.HasValue
                    ? String.Format("{0} (UTC{1}{2:hh\\:mm})", now.ToUniversalTime(), utcOffset.Value >= TimeSpan.Zero ? "+" : "-", utcOffset.Value)
                    : String.Format("{0} UTC", now.ToUniversalTime())
            };

            if (!viewmodel.NodeName.Equals(Platform.DefaultNode.Name, StringComparison.OrdinalIgnoreCase))
            {
                var nodeInfo = _nodeRegistry.GetByName(viewmodel.NodeName) as LocalNode;
                if (nodeInfo != null)
                    viewmodel.NodeCompanyName = nodeInfo.DistributionLabel;
            }

            if (distributionInfo != null)
            {
                viewmodel.DistributionId = BitConverter.ToString(distributionInfo.GetDistributionIdBytes());
                viewmodel.CompanyName = distributionInfo.CompanyName;
            }

            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult GetSlots(TableViewModel model)
        {
            return Json(LimitUsersAttribute.GetConsumedSlots().GetTableData(model, r => r));
        }
    }
}