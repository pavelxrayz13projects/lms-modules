using Olimp.Core;
using Olimp.Domain.Catalogue.Security;
using Olimp.LocalNodes.Core.Cluster;
using Olimp.ServerManagement.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Olimp.ServerManagement.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    public class BackupController : PController
    {
        private readonly ILocalNodeController _localNodeController;

        public BackupController(ILocalNodeController localNodeController)
        {
            if (localNodeController == null)
                throw new ArgumentNullException("localNodeController");

            _localNodeController = localNodeController;
        }

        [NonAction]
        private static string GetNodeName()
        {
            var paths = OlimpPaths.OfExecutingModule();

            var nodeName = paths.PlatformPaths.NodeName;

            //TODO: Remove this crutch
            string externalNode;
            if (PContext.ExecutingModuleParams.TryGetValue("externalNode", out externalNode) && !String.IsNullOrWhiteSpace(externalNode))
            {
                if (!String.Equals(nodeName, Platform.DefaultNode.Name, StringComparison.OrdinalIgnoreCase))
                    throw new NotSupportedException("Sub nodes of external node aren't supported");

                nodeName = externalNode;
            }

            return nodeName;
        }

        [HttpGet]
        [AuthorizeForAdmin(
            Roles = Permissions.Admin.Backup.CreateLocal + "," +
                    Permissions.Admin.Backup.RestoreLocal,
            Order = 2)]
        public ActionResult Index()
        {
            return View(new BackupIndexViewModel
            {
                CanBackup = Roles.IsUserInRole(Permissions.Admin.Backup.CreateLocal),
                CanBackupForAllNodes = Roles.IsUserInRole(Permissions.Admin.Backup.CreateForAllNodes),
                CanRestore = Roles.IsUserInRole(Permissions.Admin.Backup.RestoreLocal),
                CanRestoreForAllNodes = Roles.IsUserInRole(Permissions.Admin.Backup.RestoreForAllNodes)
            });
        }

        [HttpPost]
        [AuthorizeForAdmin(
            Roles = Permissions.Admin.Backup.CreateLocal + "," +
                Permissions.Admin.Backup.CreateForAllNodes,
            Order = 2)]
        public ActionResult Backup(BackupViewModelBase model)
        {
            model.ForAllNodes = true;
            //model.ForAllNodes = model.ForAllNodes && Roles.IsUserInRole(Permissions.Admin.Backup.CreateForAllNodes);

            var nodeName = GetNodeName();

            _localNodeController.CreateBackup(nodeName, model.ForAllNodes);

            return Json(new { });
        }

        [NonAction]
        private static string BackupFileNameToReadableName(string backupName)
        {
            var rawName = Path.GetFileNameWithoutExtension(backupName);

            string readableName;
            if (backupName.StartsWith("local-"))
            {
                readableName = String.Join(" ",
                    I18N.Backup.Node,
                    PContext.ExecutingModule.Context.NodeData.Name);

                rawName = rawName.Substring(6);
            }
            else if (backupName.StartsWith("full-"))
            {
                readableName = I18N.Backup.Complete;
                rawName = rawName.Substring(5);
            }
            else
            {
                return null;
            }

            DateTime backupDate;
            if (!DateTime.TryParseExact(rawName, "yyyy-M-d_H-m-s", null, DateTimeStyles.AssumeUniversal, out backupDate))
                return null;

            return String.Format("{0} ({1:F})", readableName, backupDate.ToLocalTime());
        }

        [HttpGet]
        [ChildActionOnly]
        public ActionResult Restore()
        {
            var path = OlimpPaths.OfExecutingModule().Backup;

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var files = Directory.GetFiles(path, "*.zip", SearchOption.TopDirectoryOnly)
                .Select(p => new FileInfo(p))
                .OrderBy(i => i.LastWriteTime)
                .Select(i => i.Name);

            var canRestoreForAllNodes = Roles.IsUserInRole(Permissions.Admin.Backup.RestoreForAllNodes);
            if (!canRestoreForAllNodes)
                files = files.Where(name => name.StartsWith("local-"));

            return PartialView(new BackupRestoreViewModel
            {
                //CanRestoreForAllNodes = Roles.IsUserInRole(Permissions.Admin.Backup.RestoreForAllNodes),
                ForAllNodes = true,
                CanRestoreForAllNodes = false,
                CanRestore = canRestoreForAllNodes, //WARNING: http://svnsrv/redmine/issues/6054
                BackupFiles = files.Select(n => new SelectListItem { Text = BackupFileNameToReadableName(n), Value = n })
                    .Where(i => i.Text != null)
            });
        }

        [HttpPost]
        [AuthorizeForAdmin(
            Roles = Permissions.Admin.Backup.RestoreLocal + "," +
                Permissions.Admin.Backup.RestoreForAllNodes,
            Order = 2)]
        public ActionResult Restore(BackupRestoreViewModel model)
        {
            if (string.IsNullOrEmpty(model.SelectedBackup))
                return RedirectToAction("Index");

            model.ForAllNodes = model.ForAllNodes && Roles.IsUserInRole(Permissions.Admin.Backup.RestoreForAllNodes);

            var paths = OlimpPaths.OfExecutingModule();
            var fullPath = Path.Combine(paths.Backup, model.SelectedBackup);

            var nodeName = GetNodeName();

            _localNodeController.RestoreBackup(nodeName, model.ForAllNodes, fullPath);

            return null;
        }
    }
}