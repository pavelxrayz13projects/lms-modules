using Olimp.Configuration;
using Olimp.Core;
using Olimp.Domain.Catalogue.Entities;
using Olimp.Domain.Catalogue.Ldap;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.LearningCenter;
using Olimp.Domain.LearningCenter.Entities;
using Olimp.Domain.LearningCenter.NotPersisted;
using Olimp.Ldap;
using Olimp.ServerManagement.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core;
using PAPI.Core.Data;
using PAPI.Core.Data.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace Olimp.ServerManagement.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.ExamSettings, Order = 2)]
    public class ModeRegistrationsController : PController
    {
        private readonly ILdapAuthSchemeRepository _schemesRepository;

        public ModeRegistrationsController(ILdapAuthSchemeRepository schemesRepository)
        {
            if (schemesRepository == null)
                throw new ArgumentNullException("schemesRepository");

            _schemesRepository = schemesRepository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        private bool IsInternetVersion
        {
            get
            {
                var olimpModule = (OlimpModule)PContext.ExecutingModule;
                var activationKeyInfo = olimpModule.Activation.GetActivationKeyInfo();

                return activationKeyInfo != null && activationKeyInfo.IsInternetVersion;
            }
        }

        [HttpGet]
        public ActionResult ModeRegistrationForm(bool? saveSuccess, string saveMessage)
        {
            var currentLdapScheme = LdapScheme.CurrentSchemeForEmployers;

            using (UnitOfWork.CreateRead())
            {
                var config = Config.Get<OlimpConfiguration>();
                var selectRegistrationType = new SelectList(
                    Enum.GetValues(typeof(RegistrationType))
                        .Cast<RegistrationType>()
                        .Where(v => v != RegistrationType.ByFullNameExpanded)
                        .Select(v => new
                        {
                            value = v,
                            text = ((DisplayAttribute)((typeof(RegistrationType).GetMember(v.ToString()))[0].GetCustomAttributes(typeof(DisplayAttribute), false))[0])
                                .GetName()
                        }),
                    "value",
                    "text");

                var viewModel = new ModeRegistrationsViewModel
                {
                    Config = config,
                    RegistrationTypes = selectRegistrationType,
                    ExpandedRegistration = (config.RegistrationType == RegistrationType.ByFullNameExpanded),
                    ExamGroups = config.ExamGroups,
                    InternetVersion = this.IsInternetVersion,
                    AllowedLocales = new SelectList(
                        new[] { CultureInfo.GetCultureInfo("ru-RU"), CultureInfo.GetCultureInfo("en-US") },
                        "Name",
                        "NativeName"),
                    Groups = Repository.Of<Group>()
                        .Where(new EntityIsActive<Group>())
                        .Select(g => new SelectListItem { Text = g.Name, Value = g.Id.ToString() })
                        .ToList(),
                    LdapSchemeId = currentLdapScheme != null ? currentLdapScheme.Id : (int?)null,
                    AllSchemes = _schemesRepository.GetAll()
                        .Where(s => s.IsEnabled)
                        .Select(n => new SelectListItem { Text = n.Name, Value = n.Id.ToString() })
                        .ToList(),
                    SaveSuccess = saveSuccess,
                    SaveMessage = saveMessage
                };

                return PartialView(viewModel);
            }
        }

        [HttpPost]
        public ActionResult Save(ModeRegistrationsViewModel model)
        {
            var success = true;
            var message = I18N.ModeRegistrations.SaveLoginSettingsMessage;

            using (var uow = UnitOfWork.CreateWrite())
            {
                model.Config = Config.Get<OlimpConfiguration>();

                this.UpdateModel(model);

                if (model.ExamGroups == null && !model.Config.DetermineGroupsByExamPeriod)
                    model.Config.DetermineGroupsByExamPeriod = true;

                if (this.IsInternetVersion)
                    model.Config.RegistrationType = RegistrationType.PreRegistration;
                else if (model.Config.RegistrationType == RegistrationType.ByFullName && model.ExpandedRegistration)
                    model.Config.RegistrationType = RegistrationType.ByFullNameExpanded;

                model.Config.ExamGroups = model.ExamGroups ?? Enumerable.Empty<int>();

                try
                {
                    Config.Set<OlimpConfiguration>(model.Config);

                    LdapAuthScheme scheme = null;

                    if (model.LdapSchemeId.HasValue)
                        scheme = _schemesRepository.GetById(model.LdapSchemeId.Value);

                    _schemesRepository.SetCurrentFromEmployers(scheme);
                    LdapScheme.CurrentSchemeForEmployers = scheme;

                    uow.Commit();
                }
                catch (Exception e)
                {
                    success = false;
                    message = e.Message;
                }
            }

            return RedirectToAction("Index", new { SaveSuccess = success, SaveMessage = message });
        }
    }
}