﻿using System.Globalization;
using Olimp.Core;
using Olimp.Core.Web;
using Olimp.Domain.Catalogue.Entities;
using Olimp.Domain.Catalogue.Ldap;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Catalogue.Security.IntegratedRoles;
using Olimp.Domain.Common.Nodes;
using Olimp.Domain.Common.Security;
using Olimp.Ldap;
using Olimp.Ldap.EntryMapping;
using Olimp.Ldap.Exceptions;
using Olimp.ServerManagement.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using PAPI.Core;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using RolesI18N = Olimp.Domain.Catalogue.I18N.Roles;

namespace Olimp.ServerManagement.Controllers
{
    [AuthorizeForAdmin(
        Roles = Permissions.Admin.Users.ManageLocal + "," +
                Permissions.Admin.Users.ManageGlobal + "," +
                Permissions.Admin.Ldap.ManageLdapPermissionRules + "," +
                Permissions.Admin.AuthScheme.Switch,
        Order = 2)]
    public class AuthSettingsController : PController
    {
        private readonly IUserRepository _userRepository;
        private readonly IRolesProvider _rolesProvider;
        private readonly INodesRepository _nodesRepository;
        private readonly ILdapAuthSchemeRepository _schemesRepository;
        private readonly ILdapPermissionRulesRepository _permissionRulesRepository;

        public AuthSettingsController(
            IUserRepository userRepository,
            IRolesProvider rolesProvider,
            INodesRepository nodesRepository,
            ILdapAuthSchemeRepository schemesRepository,
            ILdapPermissionRulesRepository permissionRulesRepository)
        {
            if (userRepository == null)
                throw new ArgumentNullException("userRepository");

            if (rolesProvider == null)
                throw new ArgumentNullException("rolesProvider");

            if (nodesRepository == null)
                throw new ArgumentNullException("nodesRepository");

            if (schemesRepository == null)
                throw new ArgumentNullException("schemesRepository");

            if (permissionRulesRepository == null)
                throw new ArgumentNullException("permissionRulesRepository");

            _userRepository = userRepository;
            _rolesProvider = rolesProvider;
            _nodesRepository = nodesRepository;
            _schemesRepository = schemesRepository;
            _permissionRulesRepository = permissionRulesRepository;
        }

        [HttpGet]
        public ActionResult Index(string displayText)
        {
            var currentNode = PContext.ExecutingModule.Context.NodeData;
            var currentUserPermissions = RolesHelper.GetCurrentUserRoles(ControllerContext);

            var canManageGlobal = currentUserPermissions.Contains(Permissions.Admin.Users.ManageGlobal);

            var currentLdapScheme = LdapScheme.CurrentSchemeForSystemUsers;

            var roles = _rolesProvider.GetAll().ToList();

            var permissions = roles.SelectMany(r => r.Permissions).Distinct().Select(p => new SelectListItem
            {
                Text = Permissions.DisplayName.GetDisplayName(p),
                Value = p
            });

            var model = new AuthSettingsViewModel
            {
                AllRoles = roles
                    .Where(r => r.CanBeAddedWithPermissions(currentUserPermissions))
                    .Select(r => new SelectListItem { Text = r.Title, Value = r.Id }),
                AllPermissions = permissions.Where(p => p.Text != Domain.Catalogue.I18N.Permissions.NotUsed).OrderBy(p => p.Text),
                AllNodes = canManageGlobal
                    ? _nodesRepository.GetAll()
                        .Select(n => new SelectListItem { Text = n.Name, Value = n.Id.ToString("N") })
                    : new[] { new SelectListItem { Text = currentNode.Name, Value = currentNode.Id.ToString("N") } },
                AllSchemes = _schemesRepository.GetAll()
                    .OrderBy(s => s.Name)
                    .Where(s => s.IsEnabled)
                    .Select(n => new SelectListItem { Text = n.Name, Value = n.Id.ToString(CultureInfo.InvariantCulture) })
                    .ToList(),
                LdapSchemeId = currentLdapScheme != null
                    ? currentLdapScheme.Id
                    : (int?)null,
                CurrentNodeId = currentNode.Id.ToString("N"),
                CanManageGlobal = canManageGlobal,
                CanChangeLdapPermissionRules = 
                    currentUserPermissions.Contains(Permissions.Admin.Ldap.ManageLdapPermissionRules),
                DisplayText = displayText
            };

            return View(model);
        }

        [HttpPost]
        [AuthorizeForAdmin(Roles = Permissions.Admin.AuthScheme.Switch)]
        public ActionResult SetAuthMethod(AuthSettingsViewModel model)
        {
            bool authValid;
            string message = null;

            LdapAuthScheme scheme = null;

            if (model.LdapSchemeId.HasValue)
            {
                scheme = _schemesRepository.GetById(model.LdapSchemeId.Value);
                authValid = TestAuthScheme(scheme, model.TestLogin, model.TestPassword, out message);
            }
            else
            {
                authValid = TestOlimpUser(model.TestLogin, model.TestPassword);
            }

            if (!authValid)
            {
                return Json(new
                {
                    authValid = false,
                    message = I18N.AuthSettings.CredentialsCheckFailed + message
                });
            }

            _schemesRepository.SetCurrentFromSystemUsers(scheme);
            LdapScheme.CurrentSchemeForSystemUsers = scheme;

            FormsAuthentication.SignOut();
            FormsAuthentication.SetAuthCookie(model.TestLogin, false);

            return Json(new
            {
                authValid = true,
                message = I18N.AuthSettings.ApplySelectedMethodMessage + message
            });
        }

        private bool TestOlimpUser(string login, string password)
        {
            var isAuthorized = false;

            var user = _userRepository.GetByLogin(login, false);
            if (user != null && user.PasswordEquals(password))
            {
                isAuthorized = GlobalAuthManagerRole.IsContainedByPermissionSet(
                    user.GetPermissions().ToArray());
            }

            return isAuthorized;
        }

        [NonAction]
        private bool TestAuthScheme(LdapAuthScheme scheme, string login, string password, out string message)
        {
            if (scheme == null)
                throw new ArgumentNullException("scheme");

            message = null;
            var accessor = new LdapUsersAccessor(scheme);
            LdapUserEntryBaseAttributes ldapEntryAttrs;

            try
            {
                if (!accessor.TryAuthMappedUser(
                    login,
                    password,
                    LdapEmployeeConverter.LdapUserEntryBaseAttributesMapper.CreateLdapEntryMapper(scheme),
                    out ldapEntryAttrs))
                {
                    message = I18N.AuthSettings.InvalidCredentialsError;
                    return false;
                }
            }
            catch (Exception ee)
            {
                if (ee is LdapConnectException || ee is LdapBindException)
                    message = I18N.AuthSettings.ErrorFailedToConnectLdap;

                OlimpApplication.Logger.Error(ee.ToString());
                return false;
            }

            var groups = accessor.GetGroupsByUserAttributes(ldapEntryAttrs, true);
            if (groups.Length == 0)
            {
                message = !String.IsNullOrWhiteSpace(scheme.Login)
                    ? String.Format(I18N.AuthSettings.ErrorUserhasNoGroups, login, scheme.Login)
                    : String.Format(I18N.AuthSettings.ErrorUserhasNoGroupsByAnonymous, login);
                return false;
            }

            var roles = _permissionRulesRepository.IntersectWithLdapGroups(Platform.DefaultNode.Id, groups);
            var permissions = roles.SelectMany(r => r.Permissions).ToArray();

            if (!GlobalAuthManagerRole.IsContainedByPermissionSet(permissions))
            {
                message = String.Format(I18N.AuthSettings.ErrorInappropriatePermissions, login, RolesI18N.ClusterAdmin);
                return false;
            }

            return true;
        }
    }
}