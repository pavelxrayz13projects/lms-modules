﻿using System.Resources;
using System.Security;
using Olimp.Core;
using Olimp.Core.Mvc.Security;
using Olimp.Core.Web;
using Olimp.Domain.Catalogue.Entities;
using Olimp.Domain.Catalogue.Security;
using Olimp.Domain.Common.Nodes;
using Olimp.Domain.Common.Security;
using Olimp.ServerManagement.ViewModels;
using Olimp.UI.ViewModels;
using Olimp.Web.Controllers.Base;
using Olimp.Web.Controllers.Security;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using Olimp.Web.Controllers.Users;

namespace Olimp.ServerManagement.Controllers
{
    [ErrorOnUnauthorizedAjaxRequest(Order = 1)]
    [AuthorizeForAdmin(Roles = Permissions.Admin.Users.ManageLocal + "," + Permissions.Admin.Users.ManageGlobal, Order = 2)]
    public class SystemUserController : PController
    {
        private const string PasswordPlaceholder = "··········";

        private readonly IRolesProvider _rolesProvider;
        private readonly IUserRepository _userRepository;
        private readonly INodesRepository _nodesRepository;
        private readonly IUserFactory _userFactory;

        //TODO: refactor dependency graph
        public SystemUserController(IRolesProvider rolesProvider, IUserFactory userFactory, IUserRepository userRepository, INodesRepository nodesRepository)
        {
            if (rolesProvider == null)
                throw new ArgumentNullException("rolesProvider");

            if (userFactory == null)
                throw new ArgumentNullException("userFactory");

            if (userRepository == null)
                throw new ArgumentNullException("userRepository");

            if (nodesRepository == null)
                throw new ArgumentNullException("nodesRepository");

            _rolesProvider = rolesProvider;
            _userRepository = userRepository;
            _userFactory = userFactory;
            _nodesRepository = nodesRepository;
        }

        [NonAction]
        private string GetUserRolesString(User u)
        {
            if (u.IsBlocked)
                return I18N.SystemUser.LoginRestricted;

            return String.Join(", ", u.Roles.OrderBy(r => r.Title).Select(r => r.Title));
        }

        [NonAction]
        private string GetUserNodesString(User u)
        {
            if (u.IsGlobal)
                return I18N.SystemUser.OnEveryNode;

            return String.Join(", ", u.Nodes.OrderBy(n => n.Name).Select(n => n.Name));
        }

        [NonAction]
        private SystemUserMapModel MapUser(User u, bool canManageGlobal)
        {
            return new SystemUserMapModel
            {
                Id = u.Id,
                Login = u.Login,
                IsBlocked = u.IsBlocked,
                IsGlobal = u.IsGlobal,
                IsLocked = !canManageGlobal &&
                    (u.IsGlobal || u.Nodes.Count > 1 || u.HasPermission(Permissions.Admin.Users.ManageGlobal)),
                Password = PasswordPlaceholder,
                ConfirmPassword = PasswordPlaceholder,
                Roles = u.Roles.Select(r => r.Id),
                ExcludedPermissions = u.ExcludedPermissions,
                RolesString = this.GetUserRolesString(u),
                Nodes = u.Nodes.Select(n => n.Id),
                NodesString = this.GetUserNodesString(u)
            };
        }

        [HttpPost]
        //TODO: Optimize paging
        public ActionResult GetSystemUsers(TableViewModel model)
        {
            var canManageGlobal = Roles.IsUserInRole(Permissions.Admin.Users.ManageGlobal);
            var users = _userRepository.GetAll().Select(u => this.MapUser(u, canManageGlobal));

            return Json(users.GetTableData(model, u => u));
        }

        [HttpPost]
        public ActionResult Edit(SystemUserEditViewModel model)
        {
            var currentUserPermissions = RolesHelper.GetCurrentUserRoles(ControllerContext);

            if (!TryValidateModel(model))
            {
                PAPI.Core.Diagnostics.Debug.PrintModelState(this);
                return new StatusCodeResultWithText((int)HttpStatusCode.BadRequest, "The model is invalid.");
            }

            if (!DoValidateLoginIsUnique(model.Login, model.Id))
            {
                return new StatusCodeResultWithText(
                  (int)HttpStatusCode.BadRequest,
                  I18N.SystemUser.ValidationErrorUserLoginIsNotUnique);
            }

            if (model.Login == "sys" && model.ExcludedPermissions != null && model.ExcludedPermissions.Any())
                return new StatusCodeResultWithText((int)HttpStatusCode.BadRequest, I18N.SystemUser.ImpossibleApplyExcludedPermissionsForSys);

            var userId = model.Id.GetValueOrDefault();
            var user = userId > 0
                ? _userRepository.GetById(userId)
                : _userFactory.Create(model.Login, model.Password, model.IsGlobal);

            user.Login = model.Login;
            user.IsGlobal = model.IsGlobal;
            user.IsBlocked = model.IsBlocked;

            if (model.Password != PasswordPlaceholder)
                user.ChangePassword(model.Password);

            user.Roles.Clear();
            var roles = _rolesProvider.GetMany(model.Roles.ToArray())
                .Where(r => r.CanBeAddedWithPermissions(currentUserPermissions));

            foreach (var r in roles)
                user.Roles.Add(r);

            user.Nodes.Clear();
            var nodes = _nodesRepository.GetMany(model.Nodes == null ? new Guid[0] : model.Nodes.ToArray());
            foreach (var n in nodes)
                user.Nodes.Add(n);

            user.ExcludedPermissions.Clear();

            if (model.ExcludedPermissions != null)
            {
                foreach (var permission in model.ExcludedPermissions)
                    user.ExcludedPermissions.Add(permission);
            }

            if (userId > 0)
                _userRepository.UpdateGivenVersion(user);
            else
                user.Id = _userRepository.Add(user);

            if (user.Login == User.Identity.Name)
                Roles.DeleteCookie();

            return Json(MapUser(user, true));
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var user = _userRepository.GetById(id);
            if (user.Login == this.User.Identity.Name)
                throw new CannotDeleteUserException(I18N.SystemUser.CannotDeleteUser);

            _userRepository.DeleteGivenVersion(user);
            return null;
        }

        [NonAction]
        private bool DoValidateLoginIsUnique(string login, int? userId)
        {
            var user = _userRepository.GetByLogin(login, false);

            return user == null || (userId.HasValue && userId.Value == user.Id);
        }

        [HttpPost]
        public ActionResult ValidateLoginIsUnique(string login, int? id)
        {
            Olimp.Core.OlimpApplication.Logger.InfoFormat("Validate system user login: {0} (id = {1})", login, id);
            return Json(DoValidateLoginIsUnique(login, id), JsonRequestBehavior.AllowGet);
        }
    }
}